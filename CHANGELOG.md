# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2.0.8189] - 2022-06-03
* Use Value Tuples to implement GetHashCode().

## [2.0.8125] - 2022-03-31
* Pass tests in project reference mode.

## [2.0.8119] - 2022-03-25
* Use the new Json application settings base class.

## [2.0.8110] - 2022-03-16
* Use the ?. operator, without making a copy of the delegate, 
to check if a delegate is non-null and invoke it in a thread-safe way.
* Fixes Contrib unit tests. 
* Uses new standard libraries and package build conditions.
* Packaged.

## [2.0.8097] - 2022-03-03
* Match minor revision to Dapper release versions.
* Prepared for packaging.
* Use project/package reference condition.

## [2.2.8069] - 2022-02-03
* Targeting Visual Studio 2022, C# 10 and .NET 6.0.
* Update NuGet packages.
* Remove unused references. 
* Update build version.
* Display version file when updating build version.

## [2.2.7844] - 2021-06-23
Fork of Dapper 2.0.90.

## [2.2.7759] - 2021-03-29
Converted to NET Standard 2.0
Fork of Dapper 2.0.82.

## [2.2.7759] - 2021-03-29
Converted to NET Standard 2.0
Fork of Dapper 2.0.82.

## [2.2.7607] - 2020-10-29
Converted to C#

## [2.2.7597] - 2020-10-19
### Entity
Adds persistence state. 

## [2.2.7422] - 2020-04-27
### Entity
Adds generic entity base.

## [2.2.7345] - 2019-02-10
Uses readme.md version (2.0.204) as the current version.

## [2.2.7209] - 2019-09-27
Uses NUGET version (2.2.203) as the current version.

## [1.60.7175] - 2019-08-24
Updated to version 1.60


## [1.50.6792] - 2018-08-06
Created

\(C\) 2017 Stack Exchange. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
[2.2.7844] - 2021-06-23 - Merged branch build onto Main.
```
[2.0.8189]: https://bitbucket.org/davidhary/dn.dapper/src/main/

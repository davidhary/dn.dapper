# Dapper Libraries

A simple object mapper for .Net Data Connection

* [Requirements](#Requirements)
* [Source Code](#Source-Code)
* [Database Servers, Drivers and Packages](#Packages)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Repository Owner](#Repository-Owner)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Requirements"></a>
## Requirements

### SQL Server CE
SQL Server CE 64 bits revision 4.0 must be installed. SQL Server CE Files must be linked from 
```
C:\Program Files\Microsoft SQL Server Compact Edition\v4.0\Private\amd64
```
whereas the .NET assembly must be referenced from
```
C:\Windows\assembly\GAC_MSIL\System.Data.SqlServerCe\4.0.0.0__89845dcd8080cc91\System.Data.SqlServerCe.dll
```

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [Enums Libraries] - .NET Standard Enums libraries
* [Json Libraries] - .NET Standard Json libraries
* [Std Libraries] - .NET Standard Libraries
* [Tracing Libraries] - .NET Standard Tracing libraries
* [Units Amounts] - Units and Amounts
* [Connection UI] - Port of Microsofot Connection UI Libraries
* [ISR Dapper] - Dapper Libraries
```
git clone git@bitbucket.org:davidhary/Arebis.UnitsAmounts.git
git clone git@bitbucket.org:davidhary/dn.enums.git
git clone git@bitbucket.org:davidhary/dn.json.git
git clone git@bitbucket.org:davidhary/dn.std.git
git clone git@bitbucket.org:davidhary/dn.tracing.git
git clone git@bitbucket.org:davidhary/connectionui.git
git clone git@bitbucket.org:davidhary/dn.dapper.git
```

Clone the repositories into the following relative path(s) (parents of the .git folder):
```
%vslib%\core\std
%vslib%\core\UnitsAmounts
%vslib%\data\ConnectionUI
%vslib%\data\dapper
```

where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

#### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

<a name="Packages"></a>
## Database Servers, Drivers and Packages
The following database servers, drivers and packages are associated with this repository:
* [SQL Server Express]
* [SQL Server Compact]
* [SQLite]

### SQLite
* The  SQLite dynamic link libraries are linked to C:\Apps\SQLite. 
This was down due to update conflicts between the SQLite NUGET packages and the SQLite installation package.
It seems that the SQLite packages are more up to date. 
* The Interop library is marked to be copied to the output folder, which  might have been resolved with version 113.

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio]
* [Jarte RTF Editor]
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Try Convert]
* [Funduc Search and Replace]

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [Sam Saffron, Marc Gravell, Nick Craver](https://stackexchange.github.io/Dapper/)
* [ATE Coder]

<a name="Acknowledgments"></a>
## Acknowledgments
* [Dapper]
* [Its all a remix] -- we are but a spec on the shoulders of giants
* [John Simmons] - outlaw programmer
* [Stack overflow] - Stack overflow

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Dapper]  

<a name="Closed-software"></a>
### Closed software
None

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[Dapper]: https://github.com/StackExchange/Dapper
[ISR Dapper] : https://www.bitbucket.org/davidhary/dn.dapper
[Enums Libraries]: https://www.bitbucket.org/davidhary/dn.enums
[Json Libraries]: https://www.bitbucket.org/davidhary/dn.json
[Std Libraries]: https://www.bitbucket.org/davidhary/dn.std
[Tracing Libraries]: https://www.bitbucket.org/davidhary/dn.tracing
[Units Amounts]: https://bitbucket.org/davidhary/Arebis.UnitsAmounts
[Connection UI]: https://bitbucket.org/davidhary/dn.connetionui

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Atomineer Code Documentation]: https://www.atomineerutils.com/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
[Jarte RTF Editor]: https://www.jarte.com/ 
[Try Convert]: https://github.com/dotnet/try-convert
[Visual Studio]: https://www.visualstudio.com/
[WiX Toolset]: https://www.wixtoolset.org/


[SQL Server Express]: https://www.microsoft.com/en-us/sql-server/sql-server-downloads
[SQL Server Compact]: http://www.microsoft.com/sqlserver/en/us/editions/2012-editions/compact.aspx
[SQLite]: https://system.data.sqlite.org/index.html/doc/trunk/www/downloads.wiki

[Sam Saffron, Marc Gravell, Nick Craver]: https://stackexchange.github.io/Dapper/

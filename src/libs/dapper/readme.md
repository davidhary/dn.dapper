# About

The isr.Dapper package is a fork of [Dapper].

[Dapper] is a .Net library providing a high performance Micro-ORM 
supporting SQL Server, MySQL, SQLite, SqlCE, Firebird etc.

# How to Use

See [Dapper].

# Key Features

See [Dapper].

# Main Types

The main types provided by this library are:

See [Dapper].

# Modifications

This fork includes the following modifications of the 
[Dapper] project:

* Targets were set and .NET Standard 2.0 and 2.1;
* Code was moved to the Dapper and Mapper folders;
* This fork avoids the package conflicts of the official package
by targeting .NET Standard, which occur in MS Test projects
that target .NET 4.8 and 6.0.

# Feedback

Dapper is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Dapper Repository].

[Dapper Repository]: https://bitbucket.org/davidhary/dn.dapper
[Dapper]: https://github.com/DapperLib/Dapper

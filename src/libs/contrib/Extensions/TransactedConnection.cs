﻿using System;
using System.Data;

namespace Dapper
{
    /// <summary>   A transacted connection. </summary>
    ///
    public sealed class TransactedConnection : IDbConnection
    {

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="connection">   The connection. </param>
        /// <param name="transaction">  The transaction. </param>

        public TransactedConnection(IDbConnection connection, IDbTransaction transaction)
        {
            Connection = connection;
            Transaction = transaction;
        }

        #region " ISR "

        /// <summary>   Gets the connection. </summary>
        /// <value> The connection. </value>
        public IDbConnection Connection { get; }
        /// <summary>   Gets the transaction. </summary>
        /// <value> The transaction. </value>
        public IDbTransaction Transaction { get; }

        #endregion

        /// <summary>   Gets or sets the string used to open a database. </summary>
        ///
        /// <value> A string containing connection settings. </value>

        public string ConnectionString
        {
            get { return Connection.ConnectionString; }
            set { Connection.ConnectionString = value; }
        }

        /// <summary>
        /// Gets the time to wait while trying to establish a connection before terminating the attempt
        /// and generating an error.
        /// </summary>
        ///
        /// <value>
        /// The time (in seconds) to wait for a connection to open. The default value is 15 seconds.
        /// </value>

        public int ConnectionTimeout => Connection.ConnectionTimeout;

        /// <summary>
        /// Gets the name of the current database or the database to be used after a connection is opened.
        /// </summary>
        ///
        /// <value>
        /// The name of the current database or the name of the database to be used once a connection is
        /// open. The default value is an empty string.
        /// </value>

        public string Database => Connection.Database;

        /// <summary>   Gets the current state of the connection. </summary>
        ///
        /// <value> One of the <see cref="T:System.Data.ConnectionState" /> values. </value>

        public ConnectionState State => Connection.State;

        /// <summary>
        /// Begins a database transaction with the specified <see cref="T:System.Data.IsolationLevel" />
        /// value.
        /// </summary>
        ///
        /// <param name="il">   One of the <see cref="T:System.Data.IsolationLevel" /> values. </param>
        ///
        /// <returns>   An object representing the new transaction. </returns>

        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            throw new NotImplementedException();
        }

        /// <summary>   Begins a database transaction. </summary>
        ///
        /// <returns>   An object representing the new transaction. </returns>

        public IDbTransaction BeginTransaction() => Transaction;

        /// <summary>   Changes the current database for an open Connection object. </summary>
        ///
        /// <param name="databaseName"> The name of the database to use in place of the current database. </param>

        public void ChangeDatabase(string databaseName) => Connection.ChangeDatabase(databaseName);

        /// <summary>   Closes the connection to the database. </summary>
        public void Close() => Connection.Close();

        /// <summary>   Creates and returns a Command object associated with the connection. </summary>
        ///
        /// <returns>   A Command object associated with the connection. </returns>

        public IDbCommand CreateCommand()
        {
            // The command inherits the "current" transaction.
            var command = Connection.CreateCommand();
            command.Transaction = Transaction;
            return command;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        public void Dispose() => Connection.Dispose();

        /// <summary>
        /// Opens a database connection with the settings specified by the ConnectionString property of
        /// the provider-specific Connection object.
        /// </summary>

        public void Open() => Connection.Open();
    }
}

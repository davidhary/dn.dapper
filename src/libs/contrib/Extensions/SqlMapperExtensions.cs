﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Dapper.Contrib.Extensions
{
    public static partial class SqlMapperExtensions
    {

        #region " Entities "

        /// <summary>
        /// Delete all entities in the table related to the type T.
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="connection">Open SqlConnection</param>
        /// <param name="transaction">The transaction to run under, null (the default) if none</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout</param>
        /// <returns> number of deleted records </returns>
        public static int DeleteAllEntities<T>(this IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null) where T : class
        {
            var type = typeof(T);
            var name = GetTableName(type);
            var statement = $"delete from {name}";
            return connection.Execute(statement, null, transaction, commandTimeout);
        }

        /// <summary>
        /// Delete all entities in the table related to the type T asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">Type of entity</typeparam>
        /// <param name="connection">Open SqlConnection</param>
        /// <param name="transaction">The transaction to run under, null (the default) if none</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout</param>
        /// <returns> number of deleted records </returns>
        public static async Task<int> DeleteAllEntitiesAsync<T>(this IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null) where T : class
        {
            var type = typeof(T);
            var statement = "DELETE FROM " + GetTableName(type);
            return await connection.ExecuteAsync(statement, null, transaction, commandTimeout).ConfigureAwait(false);
        }

        /// <summary>   Gets type table name. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="type"> The type. </param>
        /// <returns>   The type table name. </returns>
        public static string GetTypeTableName(Type type)
        {
            string name;
            //NOTE: This as dynamic trick falls back to handle both our own Table-attribute as well as the one in EntityFramework 
            var tableAttrName =
                type.GetCustomAttribute<TableAttribute>(false)?.Name
                ?? (type.GetCustomAttributes(false).FirstOrDefault(attr => attr.GetType().Name == "TableAttribute") as dynamic)?.Name;

            if (tableAttrName != null)
            {
                name = tableAttrName;
            }
            else
            {
                // @ISR removed plurals: 
                name = type.Name; // + "s";
                if (type.IsInterface && name.StartsWith("I"))
                    name = name.Substring(1);

                // @ISR removed suffix entity
                if (name.EndsWith("Entity"))
                    name = name.Substring(0, name.Length - 6);

#if (false)
                    // original code
                    name = type.Name + "s";
                    if (type.IsInterface && name.StartsWith("I"))
                        name = name.Substring(1);
#endif
            }
            return name;
        }

        #endregion

    }
}

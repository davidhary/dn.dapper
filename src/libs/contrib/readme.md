# About

isr.Dapper.Contrib is a fork of [Dapper.Contrib].

[Dapper.Contrib] contains a number of helper methods for inserting, 
getting, updating and deleting records using Dapper.

# How to Use

See [Dapper.Contrib] 

# Key Features

See [Dapper.Contrib] 

# Main Types

See [Dapper.Contrib] 

# Modifications

This fork includes the following modifications of [Dapper.Contrib], 
which the developers rejected:

* Drops the plural suffix from table names;
* Allows fetching records from tables consisting of only primary keys.
* Allows using a _TransactConnect_ _IDBConnection_.

# Feedback

isr.Dapper.Contrib is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Dapper Repository].

[Dapper Repository]: https://bitbucket.org/davidhary/dn.dapper
[Dapper.Contrib]: https://github.com/DapperLib/Dapper.Contrib


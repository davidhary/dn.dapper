﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Dapper
{
    /// <summary>   A SQL builder. </summary>
    /// <remarks>   David, 2021-06-08. </remarks>
    public class SqlBuilder
    {
        private readonly Dictionary<string, Clauses> _data = new Dictionary<string, Clauses>();
        private int _seq;

        private class Clause
        {
            public string Sql { get; set; }
            public object Parameters { get; set; }
            public bool IsInclusive { get; set; }
        }

        private class Clauses : List<Clause>
        {
            private readonly string _joiner, _prefix, _postfix;

            public Clauses(string joiner, string prefix = "", string postfix = "")
            {
                _joiner = joiner;
                _prefix = prefix;
                _postfix = postfix;
            }

            public string ResolveClauses(DynamicParameters p)
            {
                foreach (var item in this)
                {
                    p.AddDynamicParams(item.Parameters);
                }
                return this.Any(a => a.IsInclusive)
                    ? _prefix +
                      string.Join(_joiner,
                          this.Where(a => !a.IsInclusive)
                              .Select(c => c.Sql)
                              .Union(new[]
                              {
                                  " ( " +
                                  string.Join(" OR ", this.Where(a => a.IsInclusive).Select(c => c.Sql).ToArray()) +
                                  " ) "
                              }).ToArray()) + _postfix
                    : _prefix + string.Join(_joiner, this.Select(c => c.Sql).ToArray()) + _postfix;
            }
        }

        /// <summary>   A template. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        public class Template
        {
            private readonly string _sql;
            private readonly SqlBuilder _builder;
            private readonly object _initParams;
            private int _dataSeq = -1; // Unresolved

            /// <summary>   Constructor. </summary>
            /// <remarks>   David, 2021-06-08. </remarks>
            /// <param name="builder">      The builder. </param>
            /// <param name="sql">          The SQL. </param>
            /// <param name="parameters">   Options for controlling the operation. </param>
            public Template(SqlBuilder builder, string sql, dynamic parameters)
            {
                _initParams = parameters;
                _sql = sql;
                _builder = builder;
            }

            private static readonly Regex _regex = new Regex(@"\/\*\*.+?\*\*\/", RegexOptions.Compiled | RegexOptions.Multiline);

            private void ResolveSql()
            {
                if (_dataSeq != _builder._seq)
                {
                    var p = new DynamicParameters(_initParams);

                    rawSql = _sql;

                    foreach (var pair in _builder._data)
                    {
                        rawSql = rawSql.Replace("/**" + pair.Key + "**/", pair.Value.ResolveClauses(p));
                    }
                    parameters = p;

                    // replace all that is left with empty
                    rawSql = _regex.Replace(rawSql, "");

                    _dataSeq = _builder._seq;
                }
            }

            private string rawSql;
            private object parameters;

            /// <summary>   Gets the raw SQL. </summary>
            /// <value> The raw SQL. </value>
            public string RawSql
            {
                get { ResolveSql(); return rawSql; }
            }

            /// <summary>   Gets options for controlling the operation. </summary>
            /// <value> The parameters. </value>
            public object Parameters
            {
                get { ResolveSql(); return parameters; }
            }
        }

        /// <summary>   Adds a template to 'parameters'. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A Template. </returns>
        public Template AddTemplate(string sql, dynamic parameters = null) =>
            new Template(this, sql, parameters);

        /// <summary>   Adds a clause. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   Options for controlling the operation. </param>
        /// <param name="joiner">       The joiner. </param>
        /// <param name="prefix">       (Optional) The prefix. </param>
        /// <param name="postfix">      (Optional) The postfix. </param>
        /// <param name="isInclusive">  (Optional) True if is inclusive, false if not. </param>
        /// <returns>   A SqlBuilder. </returns>
        protected SqlBuilder AddClause(string name, string sql, object parameters, string joiner, string prefix = "", string postfix = "", bool isInclusive = false)
        {
            if (!_data.TryGetValue(name, out Clauses clauses))
            {
                clauses = new Clauses(joiner, prefix, postfix);
                _data[name] = clauses;
            }
            clauses.Add(new Clause { Sql = sql, Parameters = parameters, IsInclusive = isInclusive });
            _seq++;
            return this;
        }

        /// <summary>   Intersects. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder Intersect(string sql, dynamic parameters = null) =>
            AddClause("intersect", sql, parameters, "\nINTERSECT\n ", "\n ", "\n", false);

        /// <summary>   Inner join. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder InnerJoin(string sql, dynamic parameters = null) =>
            AddClause("innerjoin", sql, parameters, "\nINNER JOIN ", "\nINNER JOIN ", "\n", false);

        /// <summary>   Left join. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder LeftJoin(string sql, dynamic parameters = null) =>
            AddClause("leftjoin", sql, parameters, "\nLEFT JOIN ", "\nLEFT JOIN ", "\n", false);

        /// <summary>   Right join. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder RightJoin(string sql, dynamic parameters = null) =>
            AddClause("rightjoin", sql, parameters, "\nRIGHT JOIN ", "\nRIGHT JOIN ", "\n", false);

        /// <summary>   Wheres. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder Where(string sql, dynamic parameters = null) =>
            AddClause("where", sql, parameters, " AND ", "WHERE ", "\n", false);

        /// <summary>   Or where. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder OrWhere(string sql, dynamic parameters = null) =>
            AddClause("where", sql, parameters, " OR ", "WHERE ", "\n", true);

        /// <summary>   Order by. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder OrderBy(string sql, dynamic parameters = null) =>
            AddClause("orderby", sql, parameters, " , ", "ORDER BY ", "\n", false);

        /// <summary>   Selects. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder Select(string sql, dynamic parameters = null) =>
            AddClause("select", sql, parameters, " , ", "", "\n", false);

        /// <summary>   Adds the parameters. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="parameters">   Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder AddParameters(dynamic parameters) =>
            AddClause("--parameters", "", parameters, "", "", "", false);

        /// <summary>   Joins. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder Join(string sql, dynamic parameters = null) =>
            AddClause("join", sql, parameters, "\nJOIN ", "\nJOIN ", "\n", false);

        /// <summary>   Group by. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder GroupBy(string sql, dynamic parameters = null) =>
            AddClause("groupby", sql, parameters, " , ", "\nGROUP BY ", "\n", false);

        /// <summary>   Having. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder Having(string sql, dynamic parameters = null) =>
            AddClause("having", sql, parameters, "\nAND ", "HAVING ", "\n", false);

        /// <summary>   Sets. </summary>
        /// <remarks>   David, 2021-06-08. </remarks>
        /// <param name="sql">          The SQL. </param>
        /// <param name="parameters">   (Optional) Options for controlling the operation. </param>
        /// <returns>   A SqlBuilder. </returns>
        public SqlBuilder Set(string sql, dynamic parameters = null) =>
             AddClause("set", sql, parameters, " , ", "SET ", "\n", false);

    }
}

using System;

namespace isr.Dapper.Entities.TrimExtensions
{
    /// <summary> Includes 'Trim' extensions for <see cref="String">String</see>. </summary>
    /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-04-09, 1.1.3386 </para></remarks>
    public static class TrimExtensionMethods
    {

        #region " END OF LINE "

        /// <summary> Trim end line. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> A delimited string of values. </param>
        /// <returns> A String. </returns>
        public static string TrimEndLine( this string value )
        {
            return string.IsNullOrWhiteSpace( value ) ? string.Empty : value.TrimEnd( Environment.NewLine.ToCharArray() );
        }

        #endregion

        #region " SCPI UNIT "

        /// <summary>
        /// Remove unit characters from SCPI data. Some instruments append units to the end of the
        /// fetched values. This method removes alpha characters as well as the number sign which
        /// instruments append to the reading number.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> A delimited string of values. </param>
        /// <returns> The values striped from units. </returns>
        public static string TrimUnits( this string value )
        {
            return value.TrimUnits( "," );
        }

        /// <summary>
        /// Remove unit characters from SCPI data. Some instruments append units to the end of the
        /// fetched values. This method removes alpha characters as well as the number sign which
        /// instruments append to the reading number.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value">     A delimited string of values. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <returns> The values striped from units. </returns>
        public static string TrimUnits( this string value, string delimiter )
        {
            const string unitCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#";
            var dataBuilder = new System.Text.StringBuilder();
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                if ( value.Contains( delimiter ) )
                {
                    foreach ( string dataElement in value.Split( ',' ) )
                    {
                        if ( dataBuilder.Length > 0 )
                        {
                            _ = dataBuilder.Append( "," );
                        }

                        _ = dataBuilder.Append( dataElement.TrimEnd( unitCharacters.ToCharArray() ) );
                    }
                }
                else
                {
                    _ = dataBuilder.Append( value.TrimEnd( unitCharacters.ToCharArray() ) );
                }
            }

            return dataBuilder.ToString();
        }

        #endregion

        #region

        /// <summary>
        /// A string extension method that cleans the given string replacing multiple spaces with one
        /// space.
        /// </summary>
        /// <remarks>   David, 2020-10-03. </remarks>
        /// <param name="value">    A delimited string of values. </param>
        /// <returns>   A string. </returns>
        public static string Clean( this string value )
        {
            return System.Text.RegularExpressions.Regex.Replace( value, @"\s+", " " );
        }

        #endregion

    }
}

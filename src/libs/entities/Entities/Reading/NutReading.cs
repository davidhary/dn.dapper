using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Nut Reading builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class NutReadingBuilder : OneToManyRealBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( NutReadingNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = NutBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( NutNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = ReadingTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ReadingTypeNub.Id );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( NutReadingEntity.NutAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( NutReadingEntity.ReadingTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( NutReadingEntity.Amount );

        #region " SINGLETON "

        private static readonly Lazy<NutReadingBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static NutReadingBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="NutReadingEntity"/> <see cref="IOneToManyReal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "NutReading" )]
    public class NutReadingNub : OneToManyRealNub, IOneToManyReal
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public NutReadingNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyReal CreateNew()
        {
            return new NutReadingNub();
        }
    }

    /// <summary>
    /// The <see cref="NutReadingEntity"/> stores nut Readings.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class NutReadingEntity : EntityBase<IOneToManyReal, NutReadingNub>, IOneToManyReal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public NutReadingEntity() : this( new NutReadingNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public NutReadingEntity( IOneToManyReal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public NutReadingEntity( IOneToManyReal cache, IOneToManyReal store ) : base( new NutReadingNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public NutReadingEntity( NutReadingEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.NutReadingBuilder.TableName, nameof( IOneToManyReal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyReal CreateNew()
        {
            return new NutReadingNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyReal CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyRealNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="NutReadingEntity"/> interface value. </param>
        public override void CopyFrom( IOneToManyReal value )
        {
            OneToManyRealNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.NutEntity"/>Value interface. </param>
        public override void UpdateCache( IOneToManyReal value )
        {
            // first make the copy to notify of any property change.
            OneToManyRealNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="nutAutoId">        Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="readingTypeId">    Identifies the <see cref="Dapper.Entities.ReadingTypeEntity"/> </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int nutAutoId, int readingTypeId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{NutReadingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutReadingNub.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            _ = sqlBuilder.Where( $"{nameof( NutReadingNub.SecondaryId )} = @SecondaryId", new { SecondaryId = readingTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<NutReadingNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="nutAutoId">        Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="readingTypeId"> Identifies the <see cref="Dapper.Entities.ReadingTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int nutAutoId, int readingTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, nutAutoId, readingTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToManyReal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="nutAutoId">        Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="readingTypeId">    Type of the <see cref="Dapper.Entities.ReadingTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int nutAutoId, int readingTypeId )
        {
            return connection.Delete( new NutReadingNub() { PrimaryId = nutAutoId, SecondaryId = readingTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.NutReadingEntity"/>'s.
        /// </summary>
        /// <value>
        /// the <see cref="Dapper.Entities.NutReadingEntity"/>'s.
        /// </value>
        public IEnumerable<NutReadingEntity> NutReadings { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<NutReadingEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToManyReal>() ) : Populate( connection.GetAll<NutReadingNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.NutReadings = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( NutReadingEntity.NutReadings ) );
            return this.NutReadings?.Any() == true ? this.NutReadings.Count() : 0;
        }

        /// <summary>   Count Nut <see cref="NutReadingEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.NutReadingBuilder.TableName}] WHERE {nameof( NutReadingNub.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<NutReadingEntity> FetchEntities( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.NutReadingBuilder.TableName}] WHERE {nameof( NutReadingNub.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<NutReadingNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Nut <see cref="NutReadingEntity"/>'s by Nut Auto Id. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<NutReadingNub> FetchNubs( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{NutReadingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutReadingEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<NutReadingNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of <see cref="NutReadingEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.NutEntity"/>Value nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<NutReadingEntity> Populate( IEnumerable<NutReadingNub> nubs )
        {
            var l = new List<NutReadingEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( NutReadingNub nub in nubs )
                    l.Add( new NutReadingEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of <see cref="NutReadingEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.NutEntity"/>Value interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<NutReadingEntity> Populate( IEnumerable<IOneToManyReal> interfaces )
        {
            var l = new List<NutReadingEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new NutReadingNub();
                foreach ( IOneToManyReal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new NutReadingEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Nut <see cref="NutReadingEntity"/>'s; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="nutAutoId">        Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="readingTypeId">    Type of the <see cref="Dapper.Entities.ReadingTypeEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int nutAutoId, int readingTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{NutReadingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutReadingNub.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            _ = sqlBuilder.Where( $"{nameof( NutReadingNub.SecondaryId )} = @SecondaryId", new { SecondaryId = readingTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches <see cref="NutReadingEntity"/>'s by unique Index; expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="nutAutoId">        Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="readingTypeId">    Identifies the
        ///                                 <see cref="Dapper.Entities.ReadingTypeEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<NutReadingNub> FetchEntities( System.Data.IDbConnection connection, int nutAutoId, int readingTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{NutReadingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutReadingNub.PrimaryId )} = @primaryId", new { primaryId = nutAutoId } );
            _ = sqlBuilder.Where( $"{nameof( NutReadingNub.SecondaryId )} = @SecondaryId", new { SecondaryId = readingTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<NutReadingNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.NutEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="nutAutoId">        Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="readingTypeId">    Identifies the
        ///                                 <see cref="Dapper.Entities.ReadingTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int nutAutoId, int readingTypeId )
        {
            return 1 == CountEntities( connection, nutAutoId, readingTypeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count all <see cref="NutReadingEntity"/>'s; expects 0 or 1. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of values. </returns>
        public int CountNutReadings( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{NutReadingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutReadingNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches all the <see cref="NutReadingEntity"/>'s associated with this
        /// entity; Should fetch up to 1. expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<NutReadingNub> FetchNutReadings( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{NutReadingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutReadingNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<NutReadingNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.NutEntity"/> Entity. </summary>
        /// <value> the <see cref="Dapper.Entities.NutEntity"/> Entity. </value>
        public NutEntity NutEntity { get; private set; }

        /// <summary>   Fetches a Nut entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchNutEntity( System.Data.IDbConnection connection )
        {
            this.NutEntity = new NutEntity();
            return this.NutEntity.FetchUsingKey( connection, this.PrimaryId );
        }

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.ReadingTypeEntity"/>.
        /// </summary>
        /// <value> the <see cref="Dapper.Entities.NutEntity"/> value type entity. </value>
        public ReadingTypeEntity ReadingTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="ReadingTypeEntity"/> Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchReadingTypeEntity( System.Data.IDbConnection connection )
        {
            this.ReadingTypeEntity = new ReadingTypeEntity();
            return this.ReadingTypeEntity.FetchUsingKey( connection, this.PrimaryId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( NutReadingEntity.NutAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.NutEntity"/> record.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NutEntity"/> record. </value>
        public int NutAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( NutReadingEntity.ReadingTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.ReadingTypeEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ReadingTypeEntity"/>. </value>
        public int ReadingTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>
        /// Gets or sets a reading value assigned to the <see cref="Dapper.Entities.NutEntity"/> for the
        /// specific <see cref="Dapper.Entities.ReadingTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The reading value assigned to the <see cref="Dapper.Entities.NutEntity"/> for the specific
        /// <see cref="Dapper.Entities.ReadingTypeEntity"/>.
        /// </value>
        public double Amount
        {
            get => this.ICache.Amount;

            set {
                if ( !object.Equals( this.Amount, value ) )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   A nut reading selector. </summary>
    /// <remarks>   David, 2020-06-29. </remarks>
    public struct NutReadingSelector : IEquatable<NutReadingSelector>
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="nutReading">   The <see cref="NutReadingEntity"/>. </param>
        public NutReadingSelector( NutReadingEntity nutReading ) : this( nutReading.NutAutoId, nutReading.ReadingTypeId )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="nutAutoId">        Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="readingTypeId">    Identifies the <see cref="ReadingTypeEntity"/>. </param>
        public NutReadingSelector( int nutAutoId, int readingTypeId )
        {
            this.NutAutoId = nutAutoId;
            this.ReadingTypeId = readingTypeId;
        }

        /// <summary>   Gets or sets the id of the nut automatic. </summary>
        /// <value> The identifier of the nut automatic. </value>
        public int NutAutoId { get; set; }

        /// <summary>   Gets or sets the id of the Reading Type. </summary>
        /// <value> The identifier of the Reading Type. </value>
        public int ReadingTypeId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( NutReadingSelector ) obj );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( NutReadingSelector other )
        {
            return this.NutAutoId == other.NutAutoId && this.ReadingTypeId == other.ReadingTypeId;
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.NutAutoId, this.ReadingTypeId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( NutReadingSelector left, NutReadingSelector right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( NutReadingSelector left, NutReadingSelector right )
        {
            return !(left == right);
        }
    }

    /// <summary>   Collection of <see cref="NutReadingEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class NutReadingEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToManyReal, NutReadingNub, NutReadingEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( NutReadingEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( NutReadingEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<NutReadingEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( NutReadingEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return NutReadingBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection <see cref="Dapper.Entities.NutEntity"/>-Unique <see cref="NutReadingEntity"/>.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class NutUniqueNutReadingEntityCollection : NutReadingEntityCollection, Std.Primitives.IGetterSetter<double>
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="element">      The <see cref="ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="NomTypeEntity"/>. </param>
        /// <param name="uut">          The <see cref="UutEntity"/>. </param>
        public NutUniqueNutReadingEntityCollection( int nutAutoId, ElementEntity element, int nomTypeId, UutEntity uut ) : base()
        {
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
            this.MetaReading = new MetaReading( this, element, nomTypeId, uut );
            this.NutAutoId = nutAutoId;
        }


        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( NutReadingEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.ReadingTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.ReadingTypeId );
            this.NotifyPropertyChanged( ReadingTypeEntity.EntityLookupDictionary()[entity.ReadingTypeId].Label );
        }

        /// <summary>
        /// Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>
        /// Gets the reading value of the given <see cref="ReadingTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        double? IGetterSetter<double>.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Sets value for the given <see cref="ReadingTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        double IGetterSetter<double>.Setter( double value, string name )
        {
            return this.SetterThis( value, name );
        }


        /// <summary>
        /// Gets the reading value of the given <see cref="ReadingTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        protected double? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Sets the value for the given <see cref="ReadingTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        private double SetterThis( double value, string name )
        {
            int key = this.ToKey( name );
            if ( !int.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>
        /// Sets the value for the given <see cref="ReadingTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        protected double Setter( double value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.MetaReading"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.MetaReading"/>. </value>
        public MetaReading MetaReading { get; private set; }

        #endregion

        #region " TRAIT SELECTION "

        /// <summary>   Queries if collection contains 'ReadingType' key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="readingTypeId">    Identifies the <see cref="ReadingTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int readingTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( readingTypeId );
        }

        /// <summary>
        /// Converts a name to a key using the
        /// <see cref="ReadingTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
        /// extend the element Nominal Traits beyond the values of the enumeration type that is used to
        /// populate this table.
        /// </summary>
        /// <remarks>   David, 2020-05-24. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return ReadingTypeEntity.KeyLookupDictionary()[name];
        }

        /// <summary>   Sets the Meta Reading value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Double. </returns>
        public double? Setter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                this.Setter( this.ToKey( name ), value.Value );
                this.NotifyPropertyChanged( name );
            }

            return value;
        }

        /// <summary>
        /// Gets the <see cref="NutReadingEntity"/> associated with the specified type.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="readingTypeId">    Identifies the <see cref="ReadingTypeEntity"/>. </param>
        /// <returns>   An <see cref="NutReadingEntity"/>. </returns>
        public NutReadingEntity Entity( int readingTypeId )
        {
            return this.ContainsKey( readingTypeId ) ? this[this._PrimaryKeyDictionary[readingTypeId]] : new NutReadingEntity();
        }

        /// <summary>   Gets the reading value of the given identity. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="readingTypeId">    Identifies the <see cref="ReadingTypeEntity"/>. </param>
        /// <returns>   A Double? </returns>
        public double? Getter( int readingTypeId )
        {
            return this.ContainsKey( readingTypeId ) ? this[this._PrimaryKeyDictionary[readingTypeId]].Amount : new double?();
        }

        /// <summary>   Set the specified element value. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="readingTypeId">    Identifies the <see cref="ReadingTypeEntity"/>. </param>
        /// <param name="value">            The value. </param>
        public void Setter( int readingTypeId, double value )
        {
            if ( this.ContainsKey( readingTypeId ) )
            {
                this[this._PrimaryKeyDictionary[readingTypeId]].Amount = value;
            }
            else
            {
                this.Add( new NutReadingEntity() { NutAutoId = NutAutoId, ReadingTypeId = readingTypeId, Amount = value } );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NutEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NutEntity"/>. </value>
        public int NutAutoId { get; private set; }

        #endregion

    }
}

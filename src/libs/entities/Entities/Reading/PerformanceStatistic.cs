
using System;
using System.Linq;

namespace isr.Dapper.Entities
{
    /// <summary>   A performance statistic. </summary>
    /// <remarks>   David, 2021-04-27. </remarks>
    public struct PerformanceStatistic
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-05-10. </remarks>
        /// <param name="sampleSource">     The sample source. </param>
        /// <param name="ensembleCount">    The number of sample sets. </param>
        /// <param name="sampleCount">      The number of samples. </param>
        /// <param name="outlierCount">     The number of outliers. </param>
        /// <param name="mean">             The mean value. </param>
        /// <param name="sigma">            The standard deviation. </param>
        public PerformanceStatistic( SampleSource sampleSource, int ensembleCount, int sampleCount, int outlierCount, double mean, double sigma )
        {
            this.SampleSource = sampleSource;
            this.EnsembleCount = ensembleCount;
            this.SampleCount = sampleCount;
            this.OutlierCount = outlierCount;
            this.Mean = mean;
            this.Sigma = sigma;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="sampleSource">     The sample source. </param>
        /// <param name="ensembleCount">    The number of ensembles. </param>
        /// <param name="sampleTrait">      The sample trait. </param>
        public PerformanceStatistic( SampleSource sampleSource, int ensembleCount, SampleTrait sampleTrait )
        {
            this.SampleSource = sampleSource;
            this.EnsembleCount = ensembleCount;
            this.SampleCount = sampleTrait.SampleCount.GetValueOrDefault( 0 );
            this.OutlierCount = sampleTrait.OutlierCount.GetValueOrDefault( 0 );
            this.Mean = sampleTrait.Mean.GetValueOrDefault( 0 );
            this.Sigma = sampleTrait.Sigma.GetValueOrDefault( 0 );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="ensembleStat"> The ensemble stat. </param>
        /// <param name="singleStat">   The single stat. </param>
        /// <param name="minimumCount"> The minimum number of samples (outliers included) for adding the single statistics. </param>
        public PerformanceStatistic( PerformanceStatistic ensembleStat, PerformanceStatistic singleStat, int minimumCount )
        {
            this.SampleSource = SampleSource.Historical;
            this.Mean = ensembleStat.Mean;
            this.Sigma = ensembleStat.Sigma;
            this.SampleCount = ensembleStat.SampleCount;
            this.OutlierCount = ensembleStat.OutlierCount;
            this.EnsembleCount = ensembleStat.EnsembleCount;
            if ( minimumCount <= (singleStat.OutlierCount + singleStat.SampleCount) )
            {
                this.SampleSource = SampleSource.Historical;
                int ensampleSampleCount = ensembleStat.SampleCount + singleStat.SampleCount;
                this.Mean = (ensembleStat.SampleCount * ensembleStat.Mean + singleStat.SampleCount * singleStat.Mean) / ensampleSampleCount;
                this.Sigma = Math.Sqrt( ((ensembleStat.SampleCount * ensembleStat.Sigma * ensembleStat.Sigma)
                                        + (singleStat.SampleCount * singleStat.Sigma * singleStat.Sigma)) / ensampleSampleCount );
                this.SampleCount = ensampleSampleCount;
                this.OutlierCount = ensembleStat.OutlierCount + singleStat.OutlierCount;
                this.EnsembleCount = ensembleStat.EnsembleCount + singleStat.EnsembleCount;
            }
        }

        /// <summary>   Gets the empty value for the specific sample source. </summary>
        /// <value> The empty value. </value>
        public static PerformanceStatistic Empty( SampleSource sampleSource ) => new( sampleSource, 0, 0, 0, 0, 0 );

        /// <summary>   Gets the sample source. </summary>
        /// <value> The sample source. </value>
        public SampleSource SampleSource { get; }

        /// <summary>   Gets the number of ensembles. </summary>
        /// <value> The number of ensembles. </value>
        public int EnsembleCount { get; }

        /// <summary>   Gets the number of samples. </summary>
        /// <value> The number of samples. </value>
        public int SampleCount { get; }

        /// <summary>   Gets the number of outliers. </summary>
        /// <value> The number of outliers. </value>
        public int OutlierCount { get; }

        /// <summary>   Gets the mean. </summary>
        /// <value> The mean value. </value>
        public double Mean { get; }

        /// <summary>   Gets the sigma. </summary>
        /// <value> The sigma. </value>
        public double Sigma { get; }

        /// <summary>   Tests if two Performance Statistic objects are considered equal. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="left">     Performance statistics to be compared. </param>
        /// <param name="right">    Performance statistics to be compared. </param>
        /// <returns>   True if the objects are considered equal, false if they are not. </returns>
        public static bool Equals( PerformanceStatistic left, PerformanceStatistic right )
        {
            return left.SampleSource == right.SampleSource && left.EnsembleCount == right.EnsembleCount
                && left.SampleCount == right.SampleCount && left.OutlierCount == right.OutlierCount
                && left.Mean == right.Mean && left.Sigma == right.Sigma;
        }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return (obj is PerformanceStatistic statistics) && PerformanceStatistic.Equals( this, statistics );
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.SampleSource, this.EnsembleCount, this.SampleCount, this.OutlierCount, this.Mean, this.Sigma ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( PerformanceStatistic left, PerformanceStatistic right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( PerformanceStatistic left, PerformanceStatistic right )
        {
            return !(left == right);
        }
    }

    /// <summary>   List of performance statistic bindings. </summary>
    /// <remarks>   David, 2021-04-27. </remarks>
    public class PerformanceStatisticBindingList : isr.Std.BindingLists.InvokingBindingList<PerformanceStatistic>
    {

        /// <summary>   Simulates some statistics. </summary>
        /// <remarks>   David, 2021-05-04. </remarks>
        private void SimulateInternal()
        {
            this.Clear();
            Random sampleRng = new( DateTime.Now.Second );
            int sampleCount = sampleRng.Next( 10, 100 );
            int ensableCount = sampleRng.Next( 6, 10 );
            double mean = 125 + 10 * sampleRng.NextDouble();
            double sigma = 3 + 3 * (sampleRng.NextDouble() - 0.5);
            int outlierCount = Math.Max( 0, sampleRng.Next( -3, 3 ) );

            // fetch the current lot statistics and add the single ensemble type statistics.
            this.SessionStatistics = new PerformanceStatistic( SampleSource.Session, 1, sampleCount, outlierCount, mean, sigma );
            this.Add( this.SessionStatistics );

            PerformanceStatistic historicalStats = PerformanceStatistic.Empty( SampleSource.Historical );
            PerformanceStatistic singleStats;
            for ( int i = 0; i < ensableCount; i++ )
            {
                sampleCount = sampleRng.Next( 10, 100 );
                mean = sampleRng.Next( 125, 135 );
                sigma = sampleRng.Next( 4, 8 );
                outlierCount = Math.Max( 0, sampleRng.Next( -3, 3 ) );

                singleStats = new PerformanceStatistic( SampleSource.Session, 1, sampleCount, outlierCount, mean, sigma );
                // update the reading statistics.
                historicalStats = new PerformanceStatistic( historicalStats, singleStats, ensableCount );
            }
            this.HistoricalStatistics = historicalStats;
            this.Add( historicalStats );
        }

        /// <summary>   Simulates this object. </summary>
        /// <remarks>   David, 2021-05-25. </remarks>
        public void Simulate()
        {
            bool raiseListChangedEventsCache = this.RaiseListChangedEvents;
            try
            {
                this.RaiseListChangedEvents = false;
                this.SimulateInternal();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raiseListChangedEventsCache;
                if ( this.RaiseListChangedEvents )
                    this.ResetBindings();
            }
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2021-05-10. </remarks>
        public void Initialize()
        {
            this.Clear();
            this.SessionStatistics = PerformanceStatistic.Empty( SampleSource.Session );
            this.Add( this.SessionStatistics );
            this.HistoricalStatistics = PerformanceStatistic.Empty( SampleSource.Historical );
            this.Add( this.HistoricalStatistics );
        }

        /// <summary>   Gets or sets the session statistics. </summary>
        /// <value> The session statistics. </value>
        public PerformanceStatistic SessionStatistics { get; private set; }

        /// <summary>   Gets or sets the historical statistics. </summary>
        /// <value> The historical statistics. </value>
        public PerformanceStatistic HistoricalStatistics { get; private set; }

        /// <summary>
        /// Populates the statistics with the current lot statistics and the lost statistics for the
        /// sample session collected past the <paramref name="historicalReferenceDate"/>
        /// for the primary platform meter entity.
        /// </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="sampleTrait">              The sample trait. </param>
        /// <param name="partAutoId">               Identifies the part. </param>
        /// <param name="lotAutoId">                Identifies the lot. </param>
        /// <param name="platformMeter">            The platform meter. </param>
        /// <param name="historicalReferenceDate">  The historical reference date. </param>
        /// <param name="minimumCount">             The minimum count (outliers plus sample) for adding single statistics to the ensemble. </param>
        private void PopulateInternal( System.Data.IDbConnection connection, SampleTrait sampleTrait,
                              int partAutoId, int lotAutoId, PlatformMeter platformMeter, DateTime historicalReferenceDate, int minimumCount )
        {
            this.Clear();
            this.Add( new PerformanceStatistic( SampleSource.Session, 1, sampleTrait ) );
#if false
            // use the current lot statistics and add the single ensemble type statistics.
            SampleTrait sampleTrait = lot.NutReadingEntitiesCollection.SelectSampleTrait(platformMeter);
            // select the part associated with the lot
            PartEntity part = PartLotEntity.FetchEntitiesByLot( connection, lot.AutoId ).FirstOrDefault().PartEntity;
            part.FetchElements( connection, new int[] { platformMeter.MeterId } );
#endif
            PerformanceStatistic singleStats = new( SampleSource.Session, 1, sampleTrait );
            PerformanceStatistic historicalStats = PerformanceStatistic.Empty( SampleSource.Historical );
            // update the reading statistics.
            historicalStats = new PerformanceStatistic( historicalStats, singleStats, minimumCount );

            // scan all the lots.
            foreach ( LotEntity l in PartLotEntity.FetchLots( connection, partAutoId ) )
            {
                if ( l.AutoId != lotAutoId && l.Timestamp > historicalReferenceDate )
                {

                    foreach ( SessionEntity session in LotSessionEntity.FetchOrderedSessions( connection, l.AutoId ).Reverse() )
                    {
                        if ( session.Timestamp > historicalReferenceDate )
                        {
                            _ = session.FetchTraits( connection );
                            if ( session.Traits.SessionTrait.SessionType == ( int ) SessionType.Sample )
                            {
                                _ = session.FetchPlatformMeterSampleTraits( connection, platformMeter );
                                sampleTrait = session.SampleTraits.SelectSampleTrait( platformMeter );
                                singleStats = new PerformanceStatistic( SampleSource.Session, 1, sampleTrait );

                                // update the reading statistics.
                                historicalStats = new PerformanceStatistic( historicalStats, singleStats, minimumCount );
                            }
                        }
                    }
                }
            }
            this.Add( historicalStats );
        }

        /// <summary>   Populates. </summary>
        /// <remarks>   David, 2021-05-25. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="sampleTrait">              The sample trait. </param>
        /// <param name="partAutoId">               Identifies the part. </param>
        /// <param name="lotAutoId">                Identifies the lot. </param>
        /// <param name="platformMeter">            The platform meter. </param>
        /// <param name="historicalReferenceDate">  The historical reference date. </param>
        /// <param name="minimumCount">             The minimum count (outliers plus sample) for adding
        ///                                         single statistics to the ensemble. </param>
        public void Populate( System.Data.IDbConnection connection, SampleTrait sampleTrait,
                              int partAutoId, int lotAutoId, PlatformMeter platformMeter, DateTime historicalReferenceDate, int minimumCount )
        {
            bool raiseListChangedEventsCache = this.RaiseListChangedEvents;
            try
            {
                this.RaiseListChangedEvents = false;
                this.PopulateInternal( connection, sampleTrait, partAutoId, lotAutoId, platformMeter, historicalReferenceDate, minimumCount );
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raiseListChangedEventsCache;
                if ( this.RaiseListChangedEvents )
                    this.ResetBindings();
            }
        }


    }

    /// <summary>   Values that represent sample sources. </summary>
    /// <remarks>   David, 2021-04-27. </remarks>
    public enum SampleSource
    {
        /// <summary>   An enum constant representing the Session option. </summary>
        Session,

        /// <summary>   An enum constant representing the historical option. </summary>
        Historical
    }
}


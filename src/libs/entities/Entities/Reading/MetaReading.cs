using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// The Meta Reading class properties map to records of the Nut Reading table, which are assembled in
    /// the <see cref="NutReadingEntitiesCollection"/> and includes part specifications values sufficient for
    /// binning the associated Nominal Under Test (NUT).
    /// </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class MetaReading : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSetter"> The getter setter. </param>
        /// <param name="element">      The <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="uut">          The <see cref="Dapper.Entities.UutEntity"/>. </param>
        public MetaReading( Std.Primitives.IGetterSetter<double> getterSetter, ElementEntity element, int nomTypeId, UutEntity uut ) : base()
        {
            this.GetterSetter = getterSetter;
            this.ElementLabel = element.ElementLabel;
            this.ElementType = ( ElementType ) element.ElementTypeId;
            this.ElementOrdinalNumber = element.OrdinalNumber;
            this.NomType = ( NomType ) nomTypeId;
            this.UutNumber = uut.UutNumber;
            this.MeterId = uut.Traits.UutTrait.MeterId;
            this.ElementAutoId = element.AutoId;
            var trait = element.NomTraitsCollection[new MeterNomTypeSelector( this.MeterId, nomTypeId )].NomTrait;
            this.NominalValue = trait.NominalValue;
            this.GuardedSpecificationRange = trait.GuardedSpecificationRange;
            this.OverflowRange = trait.OverflowRange;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<double> GetterSetter { get; set; }

        /// <summary>   Gets a Meta Reading value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Double. </returns>
        public double? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets a Meta Reading value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Double. </returns>
        public double? Setter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }

            return value;
        }

        #endregion

        #region " IDENTIFIERS "

        /// <summary>   Identifies the element. </summary>
        private int _ElementId;

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int ElementAutoId
        {
            get => this._ElementId;

            set {
                if ( value != this.ElementAutoId )
                {
                    this._ElementId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _MeterId;

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId
        {
            get => this._MeterId;

            set {
                if ( value != this.MeterId )
                {
                    this._MeterId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private NomType _NomType;

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public NomType NomType
        {
            get => this._NomType;

            set {
                if ( value != this.NomType )
                {
                    this._NomType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " DERIVED TRAITS "

        private MeterEntity _Meter;

        /// <summary>   Gets the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public MeterEntity Meter
        {
            get {
                if ( !(this._Meter?.IsClean()).GetValueOrDefault( false ) )
                {
                    this._Meter = MeterEntity.EntityLookupDictionary()[this.MeterId];
                }

                return this._Meter;
            }
        }

        private MeterModelEntity _MeterModel;

        /// <summary>   Gets the Meter Model entity. </summary>
        /// <value> The Meter Model entity. </value>
        public MeterModelEntity MeterModel
        {
            get {
                if ( !(this._MeterModel?.IsClean()).GetValueOrDefault( false ) )
                {
                    this._MeterModel = MeterModelEntity.EntityLookupDictionary()[this.Meter.MeterModelId];
                }

                return this._MeterModel;
            }
        }

        #endregion

        #region " UUT and ELEMENT PROPERTIES "

        /// <summary>   The uut number. </summary>
        private int _UutNumber;

        /// <summary>   Gets or sets the uut number. </summary>
        /// <value> The uut number. </value>
        public int UutNumber
        {
            get => this._UutNumber;

            set {
                if ( !Equals( value, this.UutNumber ) )
                {
                    this._UutNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The element label. </summary>
        private string _ElementLabel;

        /// <summary>   Gets or sets the Label of the Element. </summary>
        /// <value> The Label of the Element. </value>
        public string ElementLabel
        {
            get => this._ElementLabel;

            set {
                if ( !string.Equals( value, this.ElementLabel ) )
                {
                    this._ElementLabel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Type of the element. </summary>
        private ElementType _ElementType;

        /// <summary>   Gets or sets the type of the Element. </summary>
        /// <value> The type of the Element. </value>
        public ElementType ElementType
        {
            get => this._ElementType;

            set {
                if ( value != this.ElementType )
                {
                    this._ElementType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The element ordinal number. </summary>
        private int _ElementOrdinalNumber;

        /// <summary>   Gets or sets the Element Ordinal number. </summary>
        /// <value> The ElementOrdinal number. </value>
        public int ElementOrdinalNumber
        {
            get => this._ElementOrdinalNumber;

            set {
                if ( !Equals( value, this.ElementOrdinalNumber ) )
                {
                    this._ElementOrdinalNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " NOMINAL VALUES "

        private double? _NominalValue;

        /// <summary>   Gets or sets the Nominal Value. </summary>
        /// <value> The Nominal Value. </value>
        public double? NominalValue
        {
            get => this._NominalValue;

            set {
                if ( !Nullable.Equals( value, this._NominalValue ) )
                {
                    this._NominalValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The overflow range. </summary>
        private Std.Primitives.RangeR _OverflowRange;

        /// <summary>   Gets or sets the overflow range. </summary>
        /// <value> The overflow range. </value>
        public Std.Primitives.RangeR OverflowRange
        {
            get => this._OverflowRange;

            set {
                if ( value != this.OverflowRange )
                {
                    this._OverflowRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The guarded specification range. </summary>
        private Std.Primitives.RangeR _GuardedSpecificationRange;

        /// <summary>   Gets or sets the Guarded Specification range. </summary>
        /// <value> The Guarded Specification range. </value>
        public Std.Primitives.RangeR GuardedSpecificationRange
        {
            get => this._GuardedSpecificationRange;

            set {
                if ( value != this.GuardedSpecificationRange )
                {
                    this._GuardedSpecificationRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " READING VALUES "

        /// <summary>   Gets or sets the Reading Amount. </summary>
        /// <value> The Reading Amount. </value>
        public double? Amount
        {
            get => this.Getter();

            set {
                _ = this.Setter( value );
                this._Delta = this.EstimateDelta();
            }
        }

        /// <summary>   Gets or sets the Reading Status. </summary>
        /// <value> The Reading Status. </value>
        public int? Status
        {
            get => ( int? ) this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the Reading Bin Number. </summary>
        /// <value> The Reading Bin Number. </value>
        public int? BinNumber
        {
            get => ( int? ) this.Getter();

            set => this.Setter( value );
        }

        #endregion

        #region " DERIVED VALUES "

        /// <summary>   Estimate delta. </summary>
        /// <remarks>   David, 2020-06-30. </remarks>
        /// <returns>   A Double? </returns>
        private double? EstimateDelta()
        {
            var readingValue = this.Amount;
            return (readingValue.HasValue && this.NominalValue.HasValue && this.NominalValue != 0) == true ? readingValue / this.NominalValue - 1 : new double?();
        }

        private double? _Delta;

        /// <summary>   Gets the Reading Delta. </summary>
        /// <value> The Reading Delta. </value>
        public double? Delta
        {
            get {
                if ( !this._Delta.HasValue )
                {
                    this._Delta = this.EstimateDelta();
                }

                return this._Delta;
            }
        }

        #endregion

    }
}

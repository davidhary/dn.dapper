// Description: The Unit Under test or UUT, represents an actual physical parts that gets tested
// using a specific meter. Each UUT includes a set of Nominal Under Test (NUT) elements.
// 
// During data collection, each NUT (nominal under test) gets a set of values, each called a
// Reading. These readings are assembled into a collection of Nut Reading Entities, which include a
// measured or evaluated value (Amount), a measurement status word, and a bin number assigned to
// the Reading based on the nominal traits of the NUT element.
// 
// The Reading class assembles the Amounts of the Nut Reading Entities (e.g., Amount, Status, and
// Bin Number) for each NUT.



using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// A builder for a Natural UUT based on the <see cref="IKeyForeignNaturalTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class UutBuilder : KeyForeignNaturalTimeBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( UutNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the foreign table . </summary>
        /// <value> The name of the foreign table. </value>
        public override string ForeignTableName { get; set; } = UutTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <value> The name of the foreign table key. </value>
        public override string ForeignTableKeyName { get; set; } = nameof( UutTypeNub.Id );

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( UutEntity.AutoId );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( UutEntity.UutTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( UutEntity.UutNumber );

        /// <summary>   Gets or sets the name of the timestamp field. </summary>
        /// <value> The name of the timestamp field. </value>
        public override string TimestampFieldName { get; set; } = nameof( UutEntity.Timestamp );

        #region " SINGLETON "

        private static readonly Lazy<UutBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static UutBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the natural Uut table based on the
    /// <see cref="IKeyForeignNaturalTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "Uut" )]
    public class UutNub : KeyForeignNaturalTimeNub, IKeyForeignNaturalTime
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public UutNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignNaturalTime CreateNew()
        {
            return new UutNub();
        }
    }


    /// <summary>
    /// The natural Uut Entity based on the <see cref="IKeyForeignNaturalTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public partial class UutEntity : EntityBase<IKeyForeignNaturalTime, UutNub>, IKeyForeignNaturalTime
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public UutEntity() : this( new UutNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public UutEntity( IKeyForeignNaturalTime value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public UutEntity( IKeyForeignNaturalTime cache, IKeyForeignNaturalTime store ) : base( new UutNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public UutEntity( UutEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.UutBuilder.TableName, nameof( IKeyForeignNaturalTime ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignNaturalTime CreateNew()
        {
            return new UutNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyForeignNaturalTime CreateCopy()
        {
            var destination = this.CreateNew();
            KeyForeignNaturalTimeNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyForeignNaturalTime value )
        {
            KeyForeignNaturalTimeNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Uut interface. </param>
        public override void UpdateCache( IKeyForeignNaturalTime value )
        {
            // first make the copy to notify of any property change.
            KeyForeignNaturalTimeNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The Uut table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyForeignNaturalTime>( key ) : connection.Get<UutNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.UutNumber );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutNumber">    The uut number. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int uutNumber )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, uutNumber ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyForeignNaturalTime entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            bool fetched = UniqueIndexOptions.Amount == (UutBuilder.Instance.QueryUniqueIndexOptions( connection ) & UniqueIndexOptions.Amount) ? this.FetchUsingUniqueIndex( connection, entity.Amount ) : this.FetchUsingKey( connection, entity.AutoId );
            if ( fetched )
            {
                // update the existing record from the specified entity.
                entity.AutoId = this.AutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new UutNub() { AutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Uut entities. </summary>
        /// <value> The Uut entities. </value>
        public IEnumerable<UutEntity> Uuts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<UutEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyForeignNaturalTime>() ) : Populate( connection.GetAll<UutNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Uuts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( UutEntity.Uuts ) );
            return this.Uuts?.Any() == true ? this.Uuts.Count() : 0;
        }

        /// <summary>   Populates a list of Uut entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The Uut nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<UutEntity> Populate( IEnumerable<UutNub> nubs )
        {
            var l = new List<UutEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( UutNub nub in nubs )
                    l.Add( new UutEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Uut entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The Uut interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<UutEntity> Populate( IEnumerable<IKeyForeignNaturalTime> interfaces )
        {
            var l = new List<UutEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new UutNub();
                foreach ( IKeyForeignNaturalTime iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new UutEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutNumber">    The uut number. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int uutNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{UutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutNub.Amount )} = @UutNumber", new { uutNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutNumber">    The uut number. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntitiesAlternative( System.Data.IDbConnection connection, int uutNumber )
        {
            var selectSql = new System.Text.StringBuilder( $"SELECT * FROM [{UutBuilder.TableName}]" );
            _ = selectSql.Append( $"WHERE (@{nameof( UutNub.Amount )} IS NULL OR {nameof( UutNub.Amount )} = @UutNumber)" );
            _ = selectSql.Append( "; " );
            return connection.ExecuteScalar<int>( selectSql.ToString(), new { uutNumber } );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutNumber">    The uut number. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<UutNub> FetchNubs( System.Data.IDbConnection connection, int uutNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{UutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutNub.Amount )} = @UutNumber", new { uutNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<UutNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the uut exists. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutNumber">    The uut number. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int uutNumber )
        {
            return 1 == CountEntities( connection, uutNumber );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Uut. </summary>
        /// <value> Identifies the Uut. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="UutTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="UutTypeEntity"/>. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( UutEntity.UutTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="UutTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="UutTypeEntity"/>. </value>
        public int UutTypeId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }


        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        public int Amount
        {
            get => this.ICache.Amount;

            set {
                if ( this.Amount != value )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( UutEntity.UutNumber ) );
                }
            }
        }

        /// <summary>   Gets or sets the uut number. </summary>
        /// <value> The uut number. </value>
        public int UutNumber
        {
            get => this.Amount;

            set => this.Amount = value;
        }

        /// <summary>   Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTime.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " TIMESTAMP "

        /// <summary>   Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        /// <summary>   The timestamp caption format. </summary>
        private string _TimestampCaptionFormat;

        /// <summary>   Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._TimestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._TimestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.Timestamp.ToString( this.TimestampCaptionFormat );

        #endregion

    }

    /// <summary>   Collection of uut entities. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class UutEntityCollection : EntityKeyedCollection<int, IKeyForeignNaturalTime, UutNub, UutEntity>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public UutEntityCollection() : base()
        {
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override int GetKeyForItem( UutEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.AutoId;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( UutEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<UutEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( UutEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Fetches nut readings. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of nut reading entities. </returns>
        public int FetchNutReadings( System.Data.IDbConnection connection )
        {
            int count = 0;
            foreach ( UutEntity uut in this )
                count += uut.FetchNutReadings( connection );
            return count;
        }

        /// <summary>   Initializes the collection of nut reading entity collection. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        public void InitializeNutReadingEntitiesCollection( IEnumerable<PlatformMeter> platformMeters )
        {
            foreach ( UutEntity uut in this )
                uut.InitializeNutReadingEntitiesCollection( platformMeters );
        }

        /// <summary>   Fetches the Nuts. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <param name="connection">   The connection. </param>
        public void FetchNuts( System.Data.IDbConnection connection )
        {
            foreach ( UutEntity uut in this )
                _ = uut.FetchNuts( connection );
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            throw new NotImplementedException();
        }
    }

}

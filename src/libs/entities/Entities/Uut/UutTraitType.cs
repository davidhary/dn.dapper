using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Uut Trait Type builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class UutTraitTypeBuilder : NominalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( UutTraitTypeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>
        /// Inserts or ignores the records described by the <see cref="UutTraitType"/> enumeration type.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public override int InsertIgnoreDefaultRecords( System.Data.IDbConnection connection )
        {
            return this.InsertIgnore( connection, typeof( UutTraitType ), new int[] { ( int ) UutTraitType.None } );
        }

        #region " SINGLETON "

        private static readonly Lazy<UutTraitTypeBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static UutTraitTypeBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="UutTraitType"/> table based on the
    /// <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "UutTraitType" )]
    public class UutTraitTypeNub : NominalNub, INominal
    {

        /// <summary>   Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        public UutTraitTypeNub() : base()
        {
        }

        /// <summary>   Creates the new. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <returns>   The new. </returns>
        public override INominal CreateNew()
        {
            return new UutTraitTypeNub();
        }
    }


    /// <summary>
    /// The <see cref="UutTraitTypeEntity"/>. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class UutTraitTypeEntity : EntityBase<INominal, UutTraitTypeNub>, INominal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public UutTraitTypeEntity() : this( new UutTraitTypeNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public UutTraitTypeEntity( INominal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public UutTraitTypeEntity( INominal cache, INominal store ) : base( new UutTraitTypeNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public UutTraitTypeEntity( UutTraitTypeEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.UutTraitTypeBuilder.TableName, nameof( INominal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new UutTraitTypeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override INominal CreateCopy()
        {
            var destination = this.CreateNew();
            NominalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( INominal value )
        {
            NominalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The UutValueType interface. </param>
        public override void UpdateCache( INominal value )
        {
            // first make the copy to notify of any property change.
            NominalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The UutValueType table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<INominal>( key ) : connection.Get<UutTraitTypeNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Label );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Uut Trait Type label. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string label )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, label ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, INominal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Label ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new UutTraitTypeNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the UutValueType entities. </summary>
        /// <value> The UutValueType entities. </value>
        public static IEnumerable<UutTraitTypeEntity> UutTraitTypes { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<UutTraitTypeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<INominal>() ) : Populate( connection.GetAll<UutTraitTypeNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            UutTraitTypeEntity.UutTraitTypes = FetchAllEntities( connection, this.UsingNativeTracking );
            return UutTraitTypes?.Any() == true ? UutTraitTypes.Count() : 0;
        }

        /// <summary>   Populates a list of UutValueType entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The UutValueType nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<UutTraitTypeEntity> Populate( IEnumerable<UutTraitTypeNub> nubs )
        {
            var l = new List<UutTraitTypeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( UutTraitTypeNub nub in nubs )
                    l.Add( new UutTraitTypeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of UutValueType entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The UutValueType interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<UutTraitTypeEntity> Populate( IEnumerable<INominal> interfaces )
        {
            var l = new List<UutTraitTypeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new UutTraitTypeNub();
                foreach ( INominal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new UutTraitTypeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, UutTraitTypeEntity> _EntityLookupDictionary;

        /// <summary>   The Uut Trait Type lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A Dictionary(Of Integer, UutValueTypeEntity) </returns>
        public static IDictionary<int, UutTraitTypeEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, UutTraitTypeEntity>();
                foreach ( UutTraitTypeEntity entity in UutTraitTypes )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Dictionary of key lookups. </summary>
        private static IDictionary<string, int> _KeyLookupDictionary;

        /// <summary>   The key lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A Dictionary(Of String, Integer) </returns>
        public static IDictionary<string, int> KeyLookupDictionary()
        {
            if ( !(_KeyLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _KeyLookupDictionary = new Dictionary<string, int>();
                foreach ( UutTraitTypeEntity entity in UutTraitTypes )
                    _KeyLookupDictionary.Add( entity.Label, entity.Id );
            }

            return _KeyLookupDictionary;
        }

        /// <summary>   Query a refetch is required for the shared entities. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if refetch required, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (UutTraitTypes?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) &&
                   (_KeyLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Uut Trait Type label. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.UutTraitTypeBuilder.TableName}] WHERE {nameof( UutTraitTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Uut Trait Type label. </param>
        /// <returns>   An IUutValueType . </returns>
        public static IEnumerable<UutTraitTypeNub> FetchNubs( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.UutTraitTypeBuilder.TableName}] WHERE {nameof( UutTraitTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<UutTraitTypeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the UutValueType exists. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Uut Trait Type label. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string label )
        {
            return 1 == CountEntities( connection, label );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Uut Trait type. </summary>
        /// <value> Identifies the Uut Trait type. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( ( object ) this.Id, ( object ) value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( UutTraitTypeEntity.UutTraitType ) );
                }
            }
        }

        /// <summary>   Gets or sets the type of the uut trait. </summary>
        /// <value> The type of the uut trait. </value>
        public UutTraitType UutTraitType
        {
            get => ( UutTraitType ) this.Id;

            set => this.Id = ( int ) value;
        }

        /// <summary>   Gets or sets the Uut Trait Type label. </summary>
        /// <value> The Uut Trait Type label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the description of the Uut Trait type. </summary>
        /// <value> The Uut Trait Type description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="uutValueTypeId">   The entity unique key. </param>
        /// <returns>   A <see cref="UutTraitTypeEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string details, UutTraitTypeEntity entity) TryFetchUsingKey( System.Data.IDbConnection connection, int uutValueTypeId )
        {
            string activity = string.Empty;
            var result = new UutTraitTypeEntity();
            try
            {
                activity = $"Fetching {nameof( UutTraitTypeEntity )} by {nameof( UutTraitTypeNub.Id )} of {uutValueTypeId}";
                return result.FetchUsingKey( connection, uutValueTypeId ) ? (true, string.Empty, result) : (false, $"Failed {activity}", result);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", result);
            }
        }

        /// <summary>   Try fetch using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uutValueTypeLabel">    The Uut Trait Type label. </param>
        /// <returns>   A <see cref="UutTraitTypeEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string details, UutTraitTypeEntity entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, string uutValueTypeLabel )
        {
            string activity = string.Empty;
            var entity = new UutTraitTypeEntity();
            try
            {
                activity = $"Fetching {nameof( UutTraitTypeEntity )} by {nameof( UutTraitTypeNub.Label )} of {uutValueTypeLabel}";
                return entity.FetchUsingUniqueIndex( connection, uutValueTypeLabel ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, details As String, Entities As IEnumerable(Of UutTraitTypeEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string details, IEnumerable<UutTraitTypeEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new UutTraitTypeEntity();
                activity = $"fetching all {nameof( UutTraitTypeEntity )}'s";
                int elementCount = entity.FetchAllEntities( connection );
                if ( elementCount != EntityLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( UutTraitTypeEntity.EntityLookupDictionary )} count must equal {nameof( UutTraitTypes )} count " );
                }
                else if ( elementCount != KeyLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( UutTraitTypeEntity.KeyLookupDictionary )} count must equal {nameof( UutTraitTypes )} count " );
                }

                return (true, string.Empty, UutTraitTypes);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<UutTraitTypeEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }

    /// <summary>   Values that represent Uut Traits. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public enum UutTraitType
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "<Uut Trait>" )]
        None = 0,

        /// <summary>   An enum constant representing the meter Identifier option. </summary>
        [System.ComponentModel.Description( "Meter Id" )]
        MeterId = 1
    }
}

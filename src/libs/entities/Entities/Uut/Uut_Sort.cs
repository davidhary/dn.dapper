using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   An uut entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class UutEntity
    {

        /// <summary>
        /// Enumerate the sort identities for all the binned Nuts measured for the UUT.
        /// </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <param name="binnings"> The binning entities. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process select sort identities in this
        /// collection.
        /// </returns>
        public IEnumerable<int> SelectUutSortIdentities( ProductUniqueBinningEntityCollection binnings )
        {
            var result = new List<int>();
            foreach ( KeyValuePair<NutUniqueKey, ReadingBinEntity> elementBin in this.Nuts.NutUniqueKeyReadingBinEntityDictionary )
            {
                var sortId = binnings.Getter( elementBin.Key.ElementId, elementBin.Key.NomTypeId, elementBin.Value.Id );
                if ( sortId.HasValue )
                    result.Add( sortId.Value );
            }

            return result;
        }

        /// <summary>   Executes the sort operation. </summary>
        /// <remarks>   David, 2020-07-04. </remarks>
        /// <param name="product">  The product. </param>
        /// <returns>   A ProductSortEntity. </returns>
        public ProductSortEntity DoSort( ProductEntity product )
        {
            return this.DoSort( product.ProductBinnings, product.ProductSorts );
        }

        /// <summary>   Executes the sort operation. </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <param name="binnings"> The binning entities. </param>
        /// <param name="sorts">    The sorts. </param>
        /// <returns>   A ProductSortEntity. </returns>
        public ProductSortEntity DoSort( ProductUniqueBinningEntityCollection binnings, ProductUniqueProductSortEntityCollection sorts )
        {
            var sortedFailedSortEntityDix = new SortedDictionary<int, ProductSortEntity>();
            var sortIdentites = this.SelectUutSortIdentities( binnings );
            var candidateEntity = new ProductSortEntity();
            int candidateBucketBin;
            foreach ( int sortId in sortIdentites )
            {
                candidateEntity = sorts[sortId];
                candidateBucketBin = candidateEntity.ProductSortTraits.ProductSortTrait.BucketBin.Value;
                if ( candidateBucketBin != ( int ) BucketBin.Good )
                {
                    if ( !sortedFailedSortEntityDix.ContainsKey( candidateEntity.SortOrder ) )
                    {
                        sortedFailedSortEntityDix.Add( candidateEntity.SortOrder, candidateEntity );
                    }
                }
            }
            // if any sorts failed, the first item is the sort entity; otherwise, the candidate already is the good sort entity.
            if ( sortedFailedSortEntityDix.Any() )
                candidateEntity = sortedFailedSortEntityDix.First().Value;
            return this.ApplyProductSortEntity( candidateEntity );
        }

        /// <summary>   Applies the product sort entity described by candidateEntity. </summary>
        /// <remarks>   David, 2020-07-08. </remarks>
        /// <param name="candidateEntity">  The candidate entity. </param>
        /// <returns>   A ProductSortEntity. </returns>
        private ProductSortEntity ApplyProductSortEntity( ProductSortEntity candidateEntity )
        {
            if ( (candidateEntity?.IsClean()).GetValueOrDefault( false ) )
            {
                int candidateBucketBin = candidateEntity.ProductSortTraits.ProductSortTrait.BucketBin.Value;
                this._ProductSortEntity = candidateEntity;
                this.BucketBin = BucketBinEntity.ToBucketBin( candidateBucketBin );
                this.UutState = BucketBinEntity.IsPresent( this.BucketBin ) ? UutState.Present : UutState.Absent;
            }
            else
            {
                this.UutState = (this.Nuts?.Any()).GetValueOrDefault( false ) ? UutState.Ready : UutState.Undefined;
                this.BucketBin = BucketBin.Unknown;
                this._ProductSortEntity = null;
            }
            this.NotifyPropertyChanged( nameof( UutEntity.ProductSortEntity ) );
            return candidateEntity;
        }

        /// <summary>   The product sort entity. </summary>
        private ProductSortEntity _ProductSortEntity;

        /// <summary>   Gets or sets the product sort entity. </summary>
        /// <value> The product sort entity. </value>
        public ProductSortEntity ProductSortEntity
        {
            get => this._ProductSortEntity;

            set {
                if ( !Entity.EntityBase<IKeyForeignLabelNatural, ProductSortNub>.Equals( value, this.ProductSortEntity ) )
                {
                    _ = this.ApplyProductSortEntity( value );
                }
            }
        }

        /// <summary>   The bucket bin. </summary>
        private BucketBin _BucketBin;

        /// <summary>   Gets or sets the bucket bin. </summary>
        /// <value> The bucket bin. </value>
        public BucketBin BucketBin
        {
            get => this._BucketBin;

            set {
                if ( value != this.BucketBin )
                {
                    this._BucketBin = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   State of the uut. </summary>
        private UutState _UutState;

        /// <summary>   Gets or sets the state of the uut. </summary>
        /// <value> The uut state. </value>
        public UutState UutState
        {
            get => this._UutState;

            set {
                if ( !object.Equals( value, this.UutState ) )
                {
                    this._UutState = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Stores the sort of a sorted and <see cref="Dapper.Entities.UutState.Present"/> UUT.
        /// </summary>
        /// <remarks>   David, 2020-06-14. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An <see cref="Dapper.Entities.UutProductSortEntity"/>. </returns>
        [CLSCompliant( false )]
        public UutProductSortEntity StoreUutSort( TransactedConnection connection )
        {
            var uutSort = new UutProductSortEntity();
            if ( this.UutState == UutState.Present )
            {
                // move uut to the complete state only if it is present; which means the uut must be sorted first.
                uutSort = new UutProductSortEntity() { UutAutoId = this.AutoId, ProductSortAutoId = this.ProductSortEntity.AutoId };
                try
                {
                    _ = uutSort.Upsert( connection );
                }
                catch ( Exception ex )
                {
                    ex.Data.Add( $"{nameof( UutProductSortEntity.UutAutoId )}.{ex.Data.Count}", uutSort.UutAutoId );
                    ex.Data.Add( $"{nameof( UutProductSortEntity.ProductSortAutoId )}.{ex.Data.Count}", uutSort.ProductSortAutoId );
                    throw ex;
                }

                var (Success, Details) = uutSort.ValidateStoredEntity( $"{nameof( UutProductSortEntity )}" );
                if ( !Success )
                    throw new InvalidOperationException( Details );
                this.UutState = UutState.Complete;
            }

            return uutSort;
        }
    }

    /// <summary>   Values that represent uut states. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public enum UutState
    {

        /// <summary> An enum constant representing the Idle State; uut is not defined. </summary>
        [Description( "Undefined" )]
        Undefined,

        /// <summary> An enum constant representing the Ready State:
        /// UUT has NUT defined and ready to receive readings. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the Measured State:
        /// UUT NUT's have readings. </summary>
        [Description( "Measured" )]
        Measured,

        /// <summary> An enum constant representing the Present State:
        /// UUT was sorted and determined to be present. </summary>
        [Description( "Present" )]
        Present,

        /// <summary> An enum constant representing the Absent State:
        /// UUT was sorted and determined to be absent. </summary>
        [Description( "Absent" )]
        Absent,

        /// <summary> An enum constant representing the Completed State:
        /// UUT and NUT data were saved. </summary>
        [Description( "Complete" )]
        Complete
    }
}

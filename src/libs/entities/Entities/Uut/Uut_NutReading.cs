using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   An uut entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class UutEntity
    {

        /// <summary>   Gets or sets the collection of <see cref="NutReadingEntityCollection"/>. </summary>
        /// <value> The nut readings. </value>
        public NutReadingEntitiesCollection NutReadingEntitiesCollection { get; private set; }

        /// <summary>   Fetches nut readings. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of nut readings. </returns>
        public int FetchNutReadings( System.Data.IDbConnection connection )
        {
            return this.Nuts.FetchNutReadings( connection );
        }

        /// <summary>
        /// Populates the <see cref="Dapper.Entities.UutEntity"/> <see cref="UutEntity.NutReadingEntitiesCollection"/> for all
        /// <see cref="Dapper.Entities.NutEntity"/>'s.
        /// </summary>
        /// <remarks>   David, 2020-06-11. </remarks>
        /// <returns>
        /// The number of elements in the <see cref="UutEntity.NutReadingEntitiesCollection"/>.
        /// </returns>
        public int PopulateNutReadingEntitiesCollection()
        {
            this.NutReadingEntitiesCollection.Clear();
            foreach ( NutEntity nut in this.Nuts )
                this.NutReadingEntitiesCollection.Add( nut.NutReadings );
            return this.NutReadingEntitiesCollection.Count;
        }

        /// <summary>   Initializes the nut reading entities collection. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        /// <param name="platformMeters">   The platform meters. </param>
        public void InitializeNutReadingEntitiesCollection( IEnumerable<PlatformMeter> platformMeters )
        {
            this.NutReadingEntitiesCollection = new NutReadingEntitiesCollection( platformMeters );
            foreach ( var platformMeter in platformMeters )
            {
                if ( this.Traits.UutTrait.MeterId == platformMeter.MeterId )
                    this.Nuts.InitializeNutReadings( this, platformMeter.Elements() );
            }
        }

        /// <summary>   Initializes the collection of nut reading entity collection. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        public void InitializeNutReadingEntitiesCollection( PlatformMeter platformMeter )
        {
            if ( this.Traits.UutTrait.MeterId == platformMeter.MeterId )
            {
                this.NutReadingEntitiesCollection = new NutReadingEntitiesCollection( platformMeter );
                this.Nuts.InitializeNutReadings( this, platformMeter.Elements() );
            }
        }

    }

    /// <summary>
    /// Keyed collection of <see cref="NutUniqueNutReadingEntityCollection"/> keyed by
    /// <see cref="NutMeterNomTypeSelector"/> of <see cref="Dapper.Entities.NutEntity"/> and
    /// <see cref="Dapper.Entities.MeterEntity"/> and
    /// <see cref="Dapper.Entities.NomTypeEntity"/>.
    /// </summary>
    /// <remarks>   David, 2020-06-16. </remarks>
    public class NutReadingEntitiesCollection : KeyedCollection<NutMeterNomTypeSelector, NutUniqueNutReadingEntityCollection>
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-05-18. </remarks>
        public NutReadingEntitiesCollection() : base()
        {
            this.MeterBindingListDictionary = new Dictionary<int, Std.BindingLists.InvokingBindingList<MetaReading>>();
            this.MeterElementNomTypeBindingListDictionary = new Dictionary<MeterElementNomTypeSelector, Std.BindingLists.InvokingBindingList<MetaReading>>();
            this.BindingList = new Std.BindingLists.InvokingBindingList<MetaReading>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        /// <param name="platformMeters">   The platform meters. </param>
        public NutReadingEntitiesCollection( IEnumerable<PlatformMeter> platformMeters ) : this()
        {
            foreach ( PlatformMeter platformMeter in platformMeters )
            {
                this.AddMeter( platformMeter );
            }
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        public NutReadingEntitiesCollection( PlatformMeter platformMeter ) : this()
        {
            this.AddMeter( platformMeter );
        }

        /// <summary>   Adds a meter. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        private void AddMeter( PlatformMeter platformMeter )
        {
            MeterElementNomTypeSelector selector;
            Std.BindingLists.InvokingBindingList<MetaReading> meterBindingList;
            Std.BindingLists.InvokingBindingList<MetaReading> meterElementNomTypeBindingList;
            meterBindingList = new Std.BindingLists.InvokingBindingList<MetaReading>();
            this.MeterBindingListDictionary.Add( platformMeter.MeterId, meterBindingList );
            foreach ( ElementEntity element in platformMeter.Elements() )
            {
                foreach ( NomTypeEntity nomTypeEntity in element.NomTypes )
                {
                    meterElementNomTypeBindingList = new Std.BindingLists.InvokingBindingList<MetaReading>();
                    selector = new MeterElementNomTypeSelector( platformMeter.MeterId, element.AutoId, nomTypeEntity.Id );
                    this.MeterElementNomTypeBindingListDictionary.Add( selector, meterElementNomTypeBindingList );
                }
            }
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-06-22. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override NutMeterNomTypeSelector GetKeyForItem( NutUniqueNutReadingEntityCollection item )
        {
            return new NutMeterNomTypeSelector( item );
        }

        /// <summary>   Query if collection contains this item. </summary>
        /// <remarks>   David, 2021-07-21. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsItem( NutUniqueNutReadingEntityCollection item )
        {
            return this.Contains( new NutMeterNomTypeSelector( item ) );
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( NutUniqueNutReadingEntityCollection item )
        {
            base.Add( item );
            var selector = new MeterElementNomTypeSelector( item.MetaReading );
            this.MeterElementNomTypeBindingListDictionary[selector].Add( item.MetaReading );
            this.MeterBindingListDictionary[item.MetaReading.MeterId].Add( item.MetaReading );
            this.BindingList.Add( item.MetaReading );
        }

        /// <summary>   Select item. </summary>
        /// <remarks>   David, 2021-07-21. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   A NutUniqueNutReadingEntityCollection. </returns>
        public NutUniqueNutReadingEntityCollection SelectItem( NutUniqueNutReadingEntityCollection item )
        {
            var itemSelector = new NutMeterNomTypeSelector( item );
            if ( this.Contains( itemSelector ) )
            {
                return this[itemSelector];
            }
            else
            {
                return null;
            }
        }

        /// <summary>   Add or update. </summary>
        /// <remarks>   David, 2021-07-21. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public virtual void Upadd( NutUniqueNutReadingEntityCollection item )
        {
            var itemSelector = new NutMeterNomTypeSelector( item );
            if ( this.Contains( itemSelector ) )
            {
                this.Remove( itemSelector );
            }
            base.Add( item );
            var selector = new MeterElementNomTypeSelector( item.MetaReading );
            this.MeterElementNomTypeBindingListDictionary[selector].Add( item.MetaReading );
            this.MeterBindingListDictionary[item.MetaReading.MeterId].Add( item.MetaReading );
            this.BindingList.Add( item.MetaReading );
        }



        /// <summary>   Removes the first. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        public void RemoveFirst()
        {
            if ( this.Any() )
            {
                var selector = new MeterElementNomTypeSelector( this.BindingList.First() );
                this.MeterElementNomTypeBindingListDictionary[selector]?.RemoveAt( 0 );
                this.MeterBindingListDictionary[selector.MeterId]?.RemoveAt( 0 );
                this.RemoveAt( 0 );
                this.BindingList.RemoveAt( 0 );
            }
        }

        /// <summary>   Toggles binding list change events. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <param name="enabled">  True to enable, false to disable. </param>
        public void ToggleBindingListChangeEvents( bool enabled )
        {
            this.BindingList.RaiseListChangedEvents = enabled;
            foreach ( Std.BindingLists.InvokingBindingList<MetaReading> bl in this.MeterBindingListDictionary.Values )
            {
                bl.RaiseListChangedEvents = enabled;
                if ( enabled )
                    bl.ResetBindings();
            }

            foreach ( Std.BindingLists.InvokingBindingList<MetaReading> bl in this.MeterElementNomTypeBindingListDictionary.Values )
            {
                bl.RaiseListChangedEvents = enabled;
                if ( enabled )
                    bl.ResetBindings();
            }
        }

        /// <summary>   Adds a range. </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="items">    <see cref="NutReadingEntitiesCollection"/> of items to append to this. </param>
        public virtual void AddRange( NutReadingEntitiesCollection items )
        {
            try
            {
                this.ToggleBindingListChangeEvents( false );
                foreach ( NutUniqueNutReadingEntityCollection item in items )
                    this.Add( item );
            }
            catch
            {
                throw;
            }
            finally
            {
                this.ToggleBindingListChangeEvents( true );
            }
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
            this.BindingList.Clear();
            foreach ( BindingList<MetaReading> bindingList in this.MeterBindingListDictionary.Values )
                bindingList.Clear();
            foreach ( BindingList<MetaReading> bindingList in this.MeterElementNomTypeBindingListDictionary.Values )
                bindingList.Clear();
        }

        /// <summary>   Gets or sets a dictionary of meter identity binding lists. </summary>
        /// <value> A dictionary of meter identity binding lists. </value>
        private IDictionary<int, Std.BindingLists.InvokingBindingList<MetaReading>> MeterBindingListDictionary { get; set; }

        /// <summary>   Selects <see cref="BindingList"/> of <see cref="MetaReading"/>. </summary>
        /// <remarks>   David, 2021-05-13. </remarks>
        /// <param name="meterId">  Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   An <see cref="isr.Std.BindingLists.InvokingBindingList{T}"/>. </returns>
        public Std.BindingLists.InvokingBindingList<MetaReading> SelectBindingList( int meterId )
        {
            return this.MeterBindingListDictionary[meterId];
        }

        /// <summary>   Dictionary of <see cref="BindingList"/> of <see cref="MetaReading"/>. </summary>
        /// <value> A dictionary of meter element nom type binding lists. </value>
        private IDictionary<MeterElementNomTypeSelector, Std.BindingLists.InvokingBindingList<MetaReading>> MeterElementNomTypeBindingListDictionary { get; set; }

        /// <summary>   Selects <see cref="BindingList"/> of <see cref="MetaReading"/>. </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   An <see cref="isr.Std.BindingLists.InvokingBindingList{T}"/>. </returns>
        public Std.BindingLists.InvokingBindingList<MetaReading> SelectBindingList( int meterId, int elementAutoId, int nomTypeId )
        {
            return this.MeterElementNomTypeBindingListDictionary[new MeterElementNomTypeSelector( meterId, elementAutoId, nomTypeId )];
        }

        /// <summary>   Selects <see cref="BindingList"/> of <see cref="MetaReading"/>. </summary>
        /// <remarks>   David, 2021-05-13. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   An <see cref="isr.Std.BindingLists.InvokingBindingList{T}"/>. </returns>
        public Std.BindingLists.InvokingBindingList<MetaReading> SelectBindingList( PlatformMeter platformMeter )
        {
            return this.MeterElementNomTypeBindingListDictionary[new MeterElementNomTypeSelector( platformMeter )];
        }

        /// <summary>   A <see cref="BindingList"/> of <see cref="MetaReading"/>. </summary>
        /// <value> A list of bindings. </value>
        public Std.BindingLists.InvokingBindingList<MetaReading> BindingList { get; private set; }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        [CLSCompliant( false )]
        public void Upsert( TransactedConnection connection )
        {
            foreach ( NutUniqueNutReadingEntityCollection nutReadings in this )
                _ = nutReadings.Upsert( connection );
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        public void Upsert( System.Data.IDbConnection connection )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.Upsert( new TransactedConnection( connection, transaction ) );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.Upsert( transactedConnection );
            }
        }
    }
}

using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{
    /// <summary>   The Uut Trait class holing the Uut trait values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class UutTrait : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSestter">    The getter setter. </param>
        public UutTrait( Std.Primitives.IGetterSetter<int> getterSestter ) : base()
        {
            this.GetterSetter = getterSestter;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<int> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) The name. </param>
        /// <returns>   The trait value. </returns>
        protected int? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void Setter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }
        }

        #endregion

        #region " TRAITS "

        /// <summary>   Gets or sets the id of the meter. </summary>
        /// <value> Identifies the meter. </value>
        public int MeterId
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        #endregion

        #region " DERIVED TRAITS "

        private MeterEntity _Meter;

        /// <summary>   Gets the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public MeterEntity Meter
        {
            get {
                if ( !(this._Meter?.IsClean()).GetValueOrDefault( false ) )
                {
                    this._Meter = MeterEntity.EntityLookupDictionary()[this.MeterId];
                }

                return this._Meter;
            }
        }

        private MeterModelEntity _MeterModel;

        /// <summary>   Gets the Meter Model entity. </summary>
        /// <value> The Meter Model entity. </value>
        public MeterModelEntity MeterModel
        {
            get {
                if ( !(this._MeterModel?.IsClean()).GetValueOrDefault( false ) )
                {
                    this._MeterModel = MeterModelEntity.EntityLookupDictionary()[this.Meter.MeterModelId];
                }

                return this._MeterModel;
            }
        }

        #endregion

    }
}

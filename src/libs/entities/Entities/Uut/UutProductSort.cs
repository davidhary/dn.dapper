using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Uut-Product-Sort builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class UutProductSortBuilder : OneToOneBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( UutProductSortNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = UutBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( UutNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = ProductSortBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ProductSortNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( UutProductSortEntity.UutAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( UutProductSortEntity.ProductSortAutoId );

        #region " SINGLETON "

        private static readonly Lazy<UutProductSortBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static UutProductSortBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Uut Sort Nub based on the <see cref="IOneToOne">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "UutSort" )]
    public class UutProductSortNub : OneToOneNub, IOneToOne
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public UutProductSortNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToOne CreateNew()
        {
            return new UutProductSortNub();
        }
    }

    /// <summary>   Sorts the UUT Product Sort based on the <see cref="IOneToOne"/> interface. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class UutProductSortEntity : EntityBase<IOneToOne, UutProductSortNub>, IOneToOne
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public UutProductSortEntity() : this( new UutProductSortNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public UutProductSortEntity( IOneToOne value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public UutProductSortEntity( IOneToOne cache, IOneToOne store ) : base( new UutProductSortNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public UutProductSortEntity( UutProductSortEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.UutProductSortBuilder.TableName, nameof( IOneToOne ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToOne CreateNew()
        {
            return new UutProductSortNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToOne CreateCopy()
        {
            var destination = this.CreateNew();
            OneToOneNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToOne value )
        {
            OneToOneNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Uut-Sort interface. </param>
        public override void UpdateCache( IOneToOne value )
        {
            // first make the copy to notify of any property change.
            OneToOneNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IOneToOne>( key ) : connection.Get<UutProductSortNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.UutAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.UutAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int uutAutoId )
        {
            return this.FetchUsingKey( connection, uutAutoId );
        }

        /// <summary>
        /// Tries to fetch and existing or insert a new <see cref="ProductSortEntity"/> and update or
        /// insert a new <see cref="UutProductSortEntity"/>.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.UutEntity"/> exists for the specified
        /// <paramref name="uutAutoId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="uutAutoId">        Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="productSortId">    Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainSort( System.Data.IDbConnection connection, int uutAutoId, int productSortId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            this.ProductSortEntity = new ProductSortEntity() { AutoId = productSortId };
            if ( !this.ProductSortEntity.Obtain( connection ) )
            {
                result = (false, $"Failed obtaining {nameof( Dapper.Entities.ProductSortEntity )} with {nameof( Dapper.Entities.ProductSortEntity.AutoId )} of {productSortId}");
            }

            if ( result.Success )
            {
                this.UutAutoId = this.UutEntity.AutoId;
                this.ProductSortAutoId = this.ProductSortEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( UutProductSortEntity.ProductSortEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( UutProductSortEntity )} for [{nameof( Dapper.Entities.UutEntity.AutoId )},{nameof( Dapper.Entities.ProductSortEntity.AutoId )}] of [{uutAutoId},{productSortId}]");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.UutEntity"/> and
        /// <see cref="ProductSortEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="UutProductSortEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="sortId">       Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int uutAutoId, int sortId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.UutEntity is null || this.UutEntity.AutoId != uutAutoId )
            {
                this.UutEntity = new UutEntity() { AutoId = uutAutoId };
                if ( !this.UutEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( Dapper.Entities.UutEntity )} with {nameof( Dapper.Entities.UutEntity.AutoId )} of {uutAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainSort( connection, uutAutoId, sortId );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( UutProductSortEntity.UutEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.UutEntity"/> and
        /// <see cref="ProductSortEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="UutProductSortEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection )
        {
            return this.TryObtain( connection, this.UutAutoId, this.ProductSortAutoId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToOne entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="sortAutoId">   Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int uutAutoId, int sortAutoId )
        {
            return connection.Delete( new UutProductSortNub() { PrimaryId = uutAutoId, SecondaryId = sortAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Uut-Sort entities. </summary>
        /// <value> The Uut-Sort entities. </value>
        public IEnumerable<UutProductSortEntity> UutSorts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<UutProductSortEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToOne>() ) : Populate( connection.GetAll<UutProductSortNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.UutSorts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( UutProductSortEntity.UutSorts ) );
            return this.UutSorts?.Any() == true ? this.UutSorts.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<UutProductSortEntity> Populate( IEnumerable<UutProductSortNub> nubs )
        {
            var l = new List<UutProductSortEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( UutProductSortNub nub in nubs )
                    l.Add( new UutProductSortEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<UutProductSortEntity> Populate( IEnumerable<IOneToOne> interfaces )
        {
            var l = new List<UutProductSortEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new UutProductSortNub();
                foreach ( IOneToOne iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new UutProductSortEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Sort entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{UutProductSortBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<UutProductSortEntity> FetchEntities( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.UutProductSortBuilder.TableName}] WHERE {nameof( UutProductSortNub.PrimaryId )} = @Id", new { Id = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<UutProductSortNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Sort. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sortAutoId">   Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   The total number of entities by Sort. </returns>
        public static int CountEntitiesBySort( System.Data.IDbConnection connection, int sortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{UutProductSortBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Sorts in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sortAutoId">   Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Sorts in this
        /// collection.
        /// </returns>
        public static IEnumerable<UutProductSortEntity> FetchEntitiesBySort( System.Data.IDbConnection connection, int sortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.UutProductSortBuilder.TableName}] WHERE {nameof( UutProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<UutProductSortNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="sortAutoId">   Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int uutAutoId, int sortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{UutProductSortBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            _ = sqlBuilder.Where( $"{nameof( UutProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="sortAutoId">   Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<UutProductSortNub> FetchNubs( System.Data.IDbConnection connection, int uutAutoId, int sortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{UutProductSortBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            _ = sqlBuilder.Where( $"{nameof( UutProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<UutProductSortNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Uut Sort exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="sortAutoId">   Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int uutAutoId, int sortAutoId )
        {
            return 1 == CountEntities( connection, uutAutoId, sortAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Uut entity. </summary>
        /// <value> The Uut entity. </value>
        public UutEntity UutEntity { get; private set; }

        /// <summary>   Fetches Uut Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Uut Entity. </returns>
        public UutEntity FetchUutEntity( System.Data.IDbConnection connection )
        {
            var entity = new UutEntity();
            _ = entity.FetchUsingKey( connection, this.UutAutoId );
            this.UutEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Uuts associated with the specified <paramref name="sortAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sortAutoId">   Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   The total number of Uuts. </returns>
        public static int CountUuts( System.Data.IDbConnection connection, int sortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.UutProductSortBuilder.TableName}] WHERE {nameof( UutProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Uuts associated with the specified <paramref name="sortAutoId"/>; expected a
        /// single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sortAutoId">   Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Uuts in this collection.
        /// </returns>
        public static IEnumerable<UutEntity> FetchUuts( System.Data.IDbConnection connection, int sortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.UutProductSortBuilder.TableName}] WHERE {nameof( UutProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<UutEntity>();
            foreach ( UutProductSortNub nub in connection.Query<UutProductSortNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new UutProductSortEntity( nub );
                l.Add( entity.FetchUutEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Uuts associated with the specified <paramref name="sortAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sortAutoId">   Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteUuts( System.Data.IDbConnection connection, int sortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.UutProductSortBuilder.TableName}] WHERE {nameof( UutProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sortAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the Sort entity. </summary>
        /// <value> The Sort entity. </value>
        public ProductSortEntity ProductSortEntity { get; private set; }

        /// <summary>   Fetches a Sort Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Sort Entity. </returns>
        public ProductSortEntity FetchSortEntity( System.Data.IDbConnection connection )
        {
            var entity = new ProductSortEntity();
            _ = entity.FetchUsingKey( connection, this.ProductSortAutoId );
            this.ProductSortEntity = entity;
            return entity;
        }

        /// <summary>   Fetches a Sort Entity. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The Sort Entity. </returns>
        public static ProductSortEntity FetchSortEntity( System.Data.IDbConnection connection, int uutAutoId )
        {
            var entity = FetchSorts( connection, uutAutoId ).FirstOrDefault();
            if ( (entity?.IsClean()).GetValueOrDefault( false ) )
            {
                _ = entity.FetchTraits( connection );
            }

            return entity;
        }

        /// <summary>   Count sorts. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The total number of sorts. </returns>
        public static int CountSorts( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.UutProductSortBuilder.TableName}] WHERE {nameof( UutProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Sorts in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Sorts in this collection.
        /// </returns>
        public static IEnumerable<ProductSortEntity> FetchSorts( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.UutProductSortBuilder.TableName}] WHERE {nameof( UutProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ProductSortEntity>();
            foreach ( UutProductSortNub nub in connection.Query<UutProductSortNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new UutProductSortEntity( nub );
                l.Add( entity.FetchSortEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Sort related to the specified Uut. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteSorts( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.UutProductSortBuilder.TableName}] WHERE {nameof( UutProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the session uut product sorts in this collection. </summary>
        /// <remarks>   David, 2020-07-09. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the session uut product sorts in this
        /// collection.
        /// </returns>
        public static IEnumerable<UutProductSortEntity> FetchSessionUutProductSorts( System.Data.IDbConnection connection, string selectQuery, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { sessionAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return Populate( connection.Query<UutProductSortEntity>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the session uut product sorts in this collection. </summary>
        /// <remarks>
        /// David, 2020-07-09 Inner join for three tables:
        /// <code>
        /// select * from tableA a
        /// inner join tableB b on a.common = b.common
        /// inner join TableC c on b.common = c.common
        /// </code>
        /// Working query:
        /// <code>
        /// select [UUtSort].* from [UUtSort]
        /// inner join [Uut] on [UutSort].PrimaryId = [Uut].AutoId
        /// inner join [SessionUut] on [Uut].AutoId = [SessionUut].SecondaryId
        /// Where [SessionUut].PrimaryId = 3
        /// </code>.
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the session uut product sorts in this
        /// collection.
        /// </returns>
        public static IEnumerable<UutProductSortEntity> FetchSessionUutProductSorts( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UutProductSort].* From [UutProductSort] Inner Join [UutProductSort] on [Uut].PrimaryId 
            // Inner Join [SessionUut] on [SessionUut].SecondaryId = [Uut].AutoId where [SessionUut].PrimaryId = 2
            _ = queryBuilder.AppendLine( @$"SELECT [{UutProductSortBuilder.TableName}].* FROM [{UutProductSortBuilder.TableName}] " );
            _ = queryBuilder.AppendLine( @$"Inner Join [{UutBuilder.TableName}] " );
            _ = queryBuilder.AppendLine( @$"   ON [{isr.Dapper.Entities.UutProductSortBuilder.TableName}].{nameof( UutProductSortNub.PrimaryId )} = [{UutBuilder.TableName}].{nameof( UutNub.AutoId )}" );
            _ = queryBuilder.AppendLine( @$"Inner Join [{SessionUutBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( @$"   ON [{UutBuilder.TableName}].{nameof( UutNub.AutoId )} = [{SessionUutBuilder.TableName}].{nameof( SessionUutNub.SecondaryId )} " );
            _ = queryBuilder.AppendLine( @$"WHERE [{SessionUutBuilder.TableName}].{nameof( SessionUutNub.PrimaryId )} = @{nameof( sessionAutoId )}" );
            _ = queryBuilder.AppendLine( @$"ORDER BY [{isr.Dapper.Entities.UutProductSortBuilder.TableName}].{nameof( UutProductSortNub.PrimaryId )} ASC; " );
            return FetchSessionUutProductSorts( connection, queryBuilder.ToString(), sessionAutoId );
        }

        /// <summary>   Deletes the unsorted uuts. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="session">      The <see cref="SessionEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteUnsortedUuts( System.Data.IDbConnection connection, SessionEntity session )
        {
            int result;
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        result = DeleteUnsortedUuts( new TransactedConnection( connection, transaction ), session );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                result = DeleteUnsortedUuts( transactedConnection, session );
            }

            return result;
        }

        /// <summary>   Deletes the unsorted uuts. </summary>
        /// <remarks>   David, 2020-07-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="session">      The <see cref="SessionEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        [CLSCompliant( false )]
        public static int DeleteUnsortedUuts( TransactedConnection connection, SessionEntity session )
        {
            if ( session.Uuts is null )
                _ = session.FetchUuts( connection );
            var count = default( int );
            if ( session.Uuts.Any() )
            {
                var uutProductSorts = new UutProductSortCollection();
                uutProductSorts.Populate( FetchSessionUutProductSorts( connection, session.AutoId ) );
                if ( uutProductSorts.Any() )
                {
                    foreach ( UutEntity uut in session.Uuts )
                    {
                        if ( !uutProductSorts.Contains( uut.AutoId ) )
                        {
                            // remove uut from the database if it does not have a sort
                            _ = uut.Delete( connection );
                            count += 1;
                        }
                    }
                }
                else
                {
                    // all UUTs are unsorted
                    foreach ( UutEntity uut in session.Uuts )
                    {
                        _ = uut.Delete( connection );
                        count += 1;
                    }
                }

                if ( count > 0 )
                {
                    // refetch if any uut was removed.
                    _ = session.FetchUuts( connection );
                }
            }
            else
            {
                return count;
            }

            return count;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( UutProductSortEntity.UutAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.UutEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.UutEntity"/>. </value>
        public int UutAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( UutProductSortEntity.ProductSortAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.ProductSortEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductSortEntity"/>. </value>
        public int ProductSortAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }

    /// <summary>   Collection of uut product sorts. </summary>
    /// <remarks>   David, 2020-07-09. </remarks>
    public class UutProductSortCollection : EntityKeyedCollection<int, IOneToOne, UutProductSortNub, UutProductSortEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-07-09. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override int GetKeyForItem( UutProductSortEntity item )
        {
            return item.UutAutoId;
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            throw new NotImplementedException();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-07-09. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<UutProductSortEntity> entities )
        {
            foreach ( UutProductSortEntity entity in entities )
                this.Add( entity );
        }
    }
}

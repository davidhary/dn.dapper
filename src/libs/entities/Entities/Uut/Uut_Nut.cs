using System;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   An uut entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class UutEntity
    {

        /// <summary>   Gets or sets the <see cref="UutUniqueNutEntityCollection">uut Nuts</see>. </summary>
        /// <value> The Uut Nuts entities. </value>
        public UutUniqueNutEntityCollection Nuts { get; private set; }

        /// <summary>   Fetches the <see cref="UutUniqueNutEntityCollection">Uut Nuts</see>. </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The number of <see cref="Dapper.Entities.NutEntity"/>'s in <see cref="UutEntity.Nuts"/>.
        /// </returns>
        public int FetchNuts( System.Data.IDbConnection connection )
        {
            this.Nuts = FetchNuts( connection, this.AutoId );
            return (this.Nuts?.Count).GetValueOrDefault( 0 );
        }

        /// <summary>   Fetches the <see cref="UutUniqueNutEntityCollection">Uut Nuts</see>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The <see cref="UutUniqueNutEntityCollection"/>. </returns>
        public static UutUniqueNutEntityCollection FetchNuts( System.Data.IDbConnection connection, int uutAutoId )
        {
            if ( !ReadingBinEntity.IsEnumerated() )
                _ = ReadingBinEntity.TryFetchAll( connection );
            var nuts = new UutUniqueNutEntityCollection( uutAutoId );
            nuts.Populate( UutNutEntity.FetchOrderedNuts( connection, uutAutoId ) );
            return nuts;
        }

        /// <summary>   The focused element label. </summary>
        private string _FocusedElementLabel;

        /// <summary>   Gets or sets the focused element label. </summary>
        /// <value> The focused element label. </value>
        public string FocusedElementLabel
        {
            get => this._FocusedElementLabel;

            set {
                if ( !string.Equals( value, this.FocusedElementLabel ) )
                {
                    this._FocusedElementLabel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Type of the focused nut nom. </summary>
        private NomType _FocusedNutNomType;

        /// <summary>   Gets or sets the type of the focused nut nom. </summary>
        /// <value> The type of the focused nut nom. </value>
        public NomType FocusedNutNomType
        {
            get => this._FocusedNutNomType;

            set {
                if ( !object.Equals( value, this.FocusedNutNomType ) )
                {
                    this._FocusedNutNomType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The focused nut reading bin. </summary>
        private ReadingBin _FocusedNutReadingBin;

        /// <summary>   Gets or sets the focused nut reading bin. </summary>
        /// <value> The focused nut reading bin. </value>
        public ReadingBin FocusedNutReadingBin
        {
            get => this._FocusedNutReadingBin;

            set {
                if ( !object.Equals( value, this.FocusedNutReadingBin ) )
                {
                    this._FocusedNutReadingBin = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The focused nut amount. </summary>
        private double _FocusedNutAmount;

        /// <summary>   Gets or sets the focused nut amount. </summary>
        /// <value> The focused nut amount. </value>
        public double FocusedNutAmount
        {
            get => this._FocusedNutAmount;

            set {
                if ( !object.Equals( value, this.FocusedNutAmount ) )
                {
                    this._FocusedNutAmount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Adds or updates a nut reading. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="nut">              The <see cref="NutEntity"/>. </param>
        /// <param name="readingValue">     The reading value. </param>
        /// <param name="readingStatus">    The reading status. </param>
        /// <returns>   A NutEntity. </returns>
        public NutEntity UpaddNutReading( NutEntity nut, double readingValue, int readingStatus )
        {
            // at this time we have only a single nominal type.
            nut.NutReadings.MetaReading.Amount = readingValue;
            nut.NutReadings.MetaReading.Status = readingStatus;
            this.FocusedElementLabel = nut.NutReadings.MetaReading.ElementLabel;
            this.FocusedNutNomType = nut.NutReadings.MetaReading.NomType;
            this.FocusedNutAmount = readingValue;
            this.NutReadingEntitiesCollection.Upadd( nut.NutReadings );
            return nut;
        }

        /// <summary>   Adds or updates a nut bin. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <param name="nut">  The <see cref="NutEntity"/>. </param>
        /// <param name="bin">  The <see cref="ReadingBin"/>. </param>
        /// <returns>   A NutEntity. </returns>
        public NutEntity UpaddNutBin( NutEntity nut, ReadingBin bin )
        {
            nut.NutReadings.MetaReading.BinNumber = ( int? ) bin;
            this.Nuts.UpaddReadingBin( nut, ReadingBinEntity.EntityLookupDictionary()[( int ) bin] );
            this.FocusedElementLabel = nut.NutReadings.MetaReading.ElementLabel;
            this.FocusedNutNomType = nut.NutReadings.MetaReading.NomType;
            this.FocusedNutReadingBin = bin;
            return nut;
        }

        /// <summary>   Stores nut reading and bin. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="storeEnabled"> (Optional) True to enable, false to disable the store. </param>
        /// <param name="sortEnabled">  (Optional) True to enable, false to disable the sort. </param>
        [CLSCompliant( false )]
        public void StoreNutReadingsAndBins( TransactedConnection connection, bool storeEnabled = true, bool sortEnabled = true )
        {
            foreach ( NutEntity nut in this.Nuts )
            {
                int binId = sortEnabled ? this.Nuts.NutIdentityReadingBinEntityDictionary[nut.AutoId].Id : 0;
                _ = UutEntity.StoreNutReadingAndBin( connection, nut, binId, storeEnabled );
            }
        }

        /// <summary>   Stores nut reading and bin. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="storeEnabled"> (Optional) True to enable, false to disable the store. </param>
        /// <param name="sortEnabled">  (Optional) True to enable, false to disable the sort. </param>
        public void StoreNutReadingsAndBins( System.Data.IDbConnection connection, bool storeEnabled = true, bool sortEnabled = true )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.StoreNutReadingsAndBins( new TransactedConnection( connection, transaction ), storeEnabled, sortEnabled );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.StoreNutReadingsAndBins( transactedConnection );
            }
        }

        /// <summary>   Stores nut reading and bin. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nut">          The <see cref="NutEntity"/>. </param>
        /// <param name="binId">        Identifies the bin. </param>
        /// <param name="storeEnabled"> (Optional) True to enable, false to disable the store. </param>
        /// <returns>   A NutReadingBinEntity. </returns>
        [CLSCompliant( false )]
        public static NutReadingBinEntity StoreNutReadingAndBin( TransactedConnection connection, NutEntity nut, int binId, bool storeEnabled = true )
        {
            if ( storeEnabled )
            {
                _ = nut.NutReadings.Upsert( connection );
            }
            if ( binId != 0 )
            {
                var nutReadingBin = new NutReadingBinEntity() { NutAutoId = nut.AutoId, ReadingBinId = binId };
                var (Success, Details) = nutReadingBin.TryObtain( connection );
                return !Success ? throw new InvalidOperationException( Details ) : nutReadingBin;
            }
            else
            {
                return new NutReadingBinEntity();
            }
        }

        /// <summary>   Stores nut reading and bin. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="nut">          The <see cref="NutEntity"/>. </param>
        /// <param name="binId">        Identifies the bin. </param>
        /// <returns>   A NutReadingBinEntity. </returns>
        public static NutReadingBinEntity StoreNutReadingAndBin( System.Data.IDbConnection connection, NutEntity nut, int binId )
        {
            NutReadingBinEntity result;
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        result = StoreNutReadingAndBin( new TransactedConnection( connection, transaction ), nut, binId );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                result = StoreNutReadingAndBin( transactedConnection, nut, binId );
            }

            return result;
        }

        /// <summary>   Adds a nut reading. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="nut">          The <see cref="NutEntity"/>. </param>
        /// <param name="amount">       The amount. </param>
        /// <param name="upsert">       True to upsert. </param>
        /// <returns>   A NutEntity. </returns>
        public NutEntity AddNutReading( System.Data.IDbConnection connection, NutEntity nut, double amount, bool upsert )
        {
            // at this time we have only a single nominal type.
            nut.NutReadings.MetaReading.Amount = amount;
            this.FocusedElementLabel = nut.NutReadings.MetaReading.ElementLabel;
            this.FocusedNutNomType = nut.NutReadings.MetaReading.NomType;
            this.FocusedNutAmount = amount;
            if ( upsert )
            {
                _ = nut.NutReadings.Upsert( connection );
            }

            this.NutReadingEntitiesCollection.Add( nut.NutReadings );
            return nut;
        }

        /// <summary>   Adds a nut bin. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="nut">          The <see cref="NutEntity"/>. </param>
        /// <param name="bin">          The <see cref="ReadingBin"/>. </param>
        /// <param name="upsert">       True to upsert. </param>
        /// <returns>   A NutReadingBinEntity. </returns>
        public NutReadingBinEntity AddNutBin( System.Data.IDbConnection connection, NutEntity nut, ReadingBin bin, bool upsert )
        {
            nut.NutReadings.MetaReading.BinNumber = ( int? ) bin;
            this.Nuts.UpaddReadingBin( nut, ReadingBinEntity.EntityLookupDictionary()[( int ) bin] );
            this.FocusedElementLabel = nut.NutReadings.MetaReading.ElementLabel;
            this.FocusedNutNomType = nut.NutReadings.MetaReading.NomType;
            this.FocusedNutReadingBin = bin;
            if ( upsert )
                _ = nut.NutReadings.Upsert( connection );
            return StoreNutBin( connection, nut.AutoId, ( int ) bin );
        }

        /// <summary>   Stores nut bin. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutId">        Identifies the nut. </param>
        /// <param name="binId">        Identifies the bin. </param>
        /// <returns>   A NutReadingBinEntity. </returns>
        public static NutReadingBinEntity StoreNutBin( System.Data.IDbConnection connection, int nutId, int binId )
        {
            var nutReadingBin = new NutReadingBinEntity() { NutAutoId = nutId, ReadingBinId = binId };
            var (Success, Details) = nutReadingBin.TryObtain( connection );
            return !Success ? throw new InvalidOperationException( Details ) : nutReadingBin;
        }
    }
}


namespace isr.Dapper.Entities
{
    /// <summary>   An uut entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class UutEntity
    {

        /// <summary>   Gets or sets the <see cref="UutTraitEntity"/> collection. </summary>
        /// <value> The Traits. </value>
        public UutUniqueTraitEntityCollection Traits { get; private set; }

        /// <summary>
        /// Fetches the <see cref="UutUniqueTraitEntityCollection"/> Natural (Integer) type trait
        /// entity collection.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of Traits. </returns>
        public int FetchTraits( System.Data.IDbConnection connection )
        {
            if ( !UutTraitTypeEntity.IsEnumerated() )
                _ = UutTraitTypeEntity.TryFetchAll( connection );
            this.Traits = new UutUniqueTraitEntityCollection( this.AutoId );
            this.Traits.Populate( UutTraitEntity.FetchEntities( connection, this.AutoId ) );
            return this.Traits.Count;
        }
    }
}

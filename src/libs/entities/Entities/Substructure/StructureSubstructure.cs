using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Structure-Substructure builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class StructureSubstructureBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( StructureSubstructureNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = StructureBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( StructureNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = SubstructureBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( SubstructureNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( StructureSubstructureEntity.StructureAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( StructureSubstructureEntity.SubstructureAutoId );

        #region " SINGLETON "

        private static readonly Lazy<StructureSubstructureBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static StructureSubstructureBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Structure Substructure Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "StructureSubstructure" )]
    public class StructureSubstructureNub : OneToManyNub, IOneToMany
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public StructureSubstructureNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new StructureSubstructureNub();
        }
    }

    /// <summary>
    /// The Structure-Substructure Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class StructureSubstructureEntity : EntityBase<IOneToMany, StructureSubstructureNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public StructureSubstructureEntity() : this( new StructureSubstructureNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Structure-Substructure interface. </param>
        public StructureSubstructureEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public StructureSubstructureEntity( IOneToMany cache, IOneToMany store ) : base( new StructureSubstructureNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public StructureSubstructureEntity( StructureSubstructureEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.StructureSubstructureBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new StructureSubstructureNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Structure-Substructure interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int structureAutoId, int substructureAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, structureAutoId, substructureAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.StructureAutoId, this.SubstructureAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.StructureAutoId, this.SubstructureAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int structureAutoId, int substructureAutoId )
        {
            return this.FetchUsingKey( connection, structureAutoId, substructureAutoId );
        }

        /// <summary>
        /// Attempts to retrieve an existing or insert a new <see cref="StructureSubstructureEntity"/>
        /// from the given data. Specifying new <paramref name="structureNumber"/> adds this Structure;
        /// Specifying a negative
        /// <paramref name="substructureNumber"/> adds a new Substructure with the next Substructure
        /// number for this Structure. Updates the
        /// <see cref="StructureSubstructureEntity.StructureEntity"/> and
        /// <see cref="StructureSubstructureEntity.SubstructureEntity"/>
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureNumber">      The Structure number. </param>
        /// <param name="substructureNumber">   The Substructure number. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int structureNumber, int substructureNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            this.StructureEntity = new StructureEntity() { StructureNumber = structureNumber };
            this.SubstructureEntity = new SubstructureEntity() { SubstructureNumber = substructureNumber };
            if ( this.StructureEntity.Obtain( connection ) )
            {
                if ( 0 < substructureNumber )
                {
                    if ( !this.SubstructureEntity.FetchUsingUniqueIndex( connection ) )
                    {
                        result = (false, $"Failed fetching existing {nameof( Dapper.Entities.SubstructureEntity )} with {nameof( Dapper.Entities.SubstructureEntity.SubstructureNumber )} of {substructureNumber}");
                    }
                }
                else
                {
                    this.SubstructureEntity = FetchLastSubstructure( connection, this.StructureEntity.AutoId );
                    substructureNumber = this.SubstructureEntity.IsClean() ? this.SubstructureEntity.SubstructureNumber + 1 : 1;
                    this.SubstructureEntity = new SubstructureEntity() { SubstructureNumber = substructureNumber };
                    if ( !this.SubstructureEntity.Obtain( connection ) )
                    {
                        result = (false, $"Failed obtaining {nameof( Dapper.Entities.SubstructureEntity )} with {nameof( Dapper.Entities.SubstructureEntity.SubstructureNumber )} of {substructureNumber}");
                    }
                }
            }
            else
            {
                result = (false, $"Failed obtaining {nameof( Dapper.Entities.StructureEntity )} with {nameof( Dapper.Entities.StructureEntity.StructureNumber )} of {structureNumber}");
            }

            if ( result.Success )
            {
                this.StructureAutoId = this.StructureEntity.AutoId;
                this.SubstructureAutoId = this.SubstructureEntity.AutoId;
                if ( !this.Obtain( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( StructureSubstructureEntity )} for [{nameof( Dapper.Entities.StructureEntity.StructureNumber )}, {nameof( Dapper.Entities.SubstructureEntity.SubstructureNumber )}] of [{structureNumber},{substructureNumber}]");
                }
            };
            this.NotifyPropertyChanged( nameof( StructureSubstructureEntity.SubstructureEntity ) );
            this.NotifyPropertyChanged( nameof( StructureSubstructureEntity.StructureEntity ) );
            return result;
        }


        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int structureAutoId, int substructureAutoId )
        {
            return connection.Delete( new StructureSubstructureNub() { PrimaryId = structureAutoId, SecondaryId = substructureAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Structure-Substructure entities. </summary>
        /// <value> The Structure-Substructure entities. </value>
        public IEnumerable<StructureSubstructureEntity> StructureSubstructures { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<StructureSubstructureEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<StructureSubstructureNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.StructureSubstructures = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( StructureSubstructureEntity.StructureSubstructures ) );
            return this.StructureSubstructures?.Any() == true ? this.StructureSubstructures.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<StructureSubstructureEntity> Populate( IEnumerable<StructureSubstructureNub> nubs )
        {
            var l = new List<StructureSubstructureEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( StructureSubstructureNub nub in nubs )
                    l.Add( new StructureSubstructureEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<StructureSubstructureEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<StructureSubstructureEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new StructureSubstructureNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new StructureSubstructureEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Substructure entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{StructureSubstructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( StructureSubstructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<StructureSubstructureEntity> FetchEntities( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}] WHERE {nameof( StructureSubstructureNub.PrimaryId )} = @Id", new { Id = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<StructureSubstructureNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Substructure. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>   The total number of entities by Substructure. </returns>
        public static int CountEntitiesBySubstructure( System.Data.IDbConnection connection, int substructureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{StructureSubstructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( StructureSubstructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substructureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Substructures in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Substructures in this
        /// collection.
        /// </returns>
        public static IEnumerable<StructureSubstructureEntity> FetchEntitiesBySubstructure( System.Data.IDbConnection connection, int substructureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}] WHERE {nameof( StructureSubstructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substructureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<StructureSubstructureNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int structureAutoId, int substructureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{StructureSubstructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( StructureSubstructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            _ = sqlBuilder.Where( $"{nameof( StructureSubstructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substructureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<StructureSubstructureNub> FetchNubs( System.Data.IDbConnection connection, int structureAutoId, int substructureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{StructureSubstructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( StructureSubstructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            _ = sqlBuilder.Where( $"{nameof( StructureSubstructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substructureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<StructureSubstructureNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Structure Substructure exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int structureAutoId, int substructureAutoId )
        {
            return 1 == CountEntities( connection, structureAutoId, substructureAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Structure entity. </summary>
        /// <value> The Structure entity. </value>
        public StructureEntity StructureEntity { get; private set; }

        /// <summary>   Fetches Structure Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Structure Entity. </returns>
        public StructureEntity FetchStructureEntity( System.Data.IDbConnection connection )
        {
            var entity = new StructureEntity();
            _ = entity.FetchUsingKey( connection, this.StructureAutoId );
            this.StructureEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Structures associated with the specified <paramref name="substructureAutoId"/>;
        /// expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>   The total number of Structures. </returns>
        public static int CountStructures( System.Data.IDbConnection connection, int substructureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}] WHERE {nameof( StructureSubstructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substructureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Structures associated with the specified <paramref name="substructureAutoId"/>;
        /// expected a single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Structures in this collection.
        /// </returns>
        public static IEnumerable<SubstructureEntity> FetchStructures( System.Data.IDbConnection connection, int substructureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}] WHERE {nameof( StructureSubstructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substructureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<SubstructureEntity>();
            foreach ( StructureSubstructureNub nub in connection.Query<StructureSubstructureNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new StructureSubstructureEntity( nub );
                l.Add( entity.FetchSubstructureEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Structures associated with the specified <paramref name="substructureAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="substructureAutoId">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteStructures( System.Data.IDbConnection connection, int substructureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}] WHERE {nameof( StructureSubstructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substructureAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the Substructure entity. </summary>
        /// <value> The Substructure entity. </value>
        public SubstructureEntity SubstructureEntity { get; private set; }

        /// <summary>   Fetches a Substructure Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Substructure Entity. </returns>
        public SubstructureEntity FetchSubstructureEntity( System.Data.IDbConnection connection )
        {
            var entity = new SubstructureEntity();
            _ = entity.FetchUsingKey( connection, this.SubstructureAutoId );
            this.SubstructureEntity = entity;
            return entity;
        }

        /// <summary>   Count substructures. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   The total number of substructures. </returns>
        public static int CountSubstructures( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}] WHERE {nameof( StructureSubstructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Substructures in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Substructures in this collection.
        /// </returns>
        public static IEnumerable<SubstructureEntity> FetchSubstructures( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}] WHERE {nameof( StructureSubstructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<SubstructureEntity>();
            foreach ( StructureSubstructureNub nub in connection.Query<StructureSubstructureNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new StructureSubstructureEntity( nub );
                l.Add( entity.FetchSubstructureEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Substructure related to the specified Structure. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteSubstructures( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}] WHERE {nameof( StructureSubstructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the ordered substructures in this collection. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered substructures in this
        /// collection.
        /// </returns>
        public static IEnumerable<SubstructureEntity> FetchOrderedSubstructures( System.Data.IDbConnection connection, string selectQuery, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = structureAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return SubstructureEntity.Populate( connection.Query<SubstructureNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered substructures in this collection. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered substructures in this
        /// collection.
        /// </returns>
        public static IEnumerable<SubstructureEntity> FetchOrderedSubstructures( System.Data.IDbConnection connection, int structureAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [StructureSubstructure] on [StructureSubstructure].SecondaryId = [Substructure].AutoId where [StructureSubstructure].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{SubstructureBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{SubstructureBuilder.TableName}] Inner Join [{StructureSubstructureBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}].{nameof( StructureSubstructureNub.SecondaryId )} = [{isr.Dapper.Entities.SubstructureBuilder.TableName}].{nameof( SubstructureNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}].{nameof( StructureSubstructureNub.PrimaryId )} = @Id" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.SubstructureBuilder.TableName}].{nameof( SubstructureNub.Amount )} ASC; " );
            return FetchOrderedSubstructures( connection, queryBuilder.ToString(), structureAutoId );
        }

        /// <summary>   Fetches the last Substructure in this collection. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the last Substructures in this
        /// collection.
        /// </returns>
        public static SubstructureEntity FetchFirstSubstructure( System.Data.IDbConnection connection, string selectQuery, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery, new { Id = structureAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<SubstructureNub>( template.RawSql, template.Parameters ).FirstOrDefault();
            return nub is null ? new SubstructureEntity() : new SubstructureEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches the last Substructure in this collection. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the last Substructures in this
        /// collection.
        /// </returns>
        public static SubstructureEntity FetchLastSubstructure( System.Data.IDbConnection connection, int structureAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [StructureSubstructure] on [StructureSubstructure].SecondaryId = [Substructure].AutoId where [StructureSubstructure].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{SubstructureBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{SubstructureBuilder.TableName}] Inner Join [{StructureSubstructureBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}].{nameof( StructureSubstructureNub.SecondaryId )} = [{isr.Dapper.Entities.SubstructureBuilder.TableName}].{nameof( SubstructureNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.StructureSubstructureBuilder.TableName}].{nameof( StructureSubstructureNub.PrimaryId )} = @Id" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.SubstructureBuilder.TableName}].{nameof( SubstructureNub.Amount )} DESC; " );
            return FetchFirstSubstructure( connection, queryBuilder.ToString(), structureAutoId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( StructureSubstructureEntity.StructureAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="StructureEntity"/>. </summary>
        /// <value> Identifies the <see cref="StructureEntity"/>. </value>
        public int StructureAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( StructureSubstructureEntity.SubstructureAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="SubstructureEntity"/>. </summary>
        /// <value> Identifies the <see cref="SubstructureEntity"/>. </value>
        public int SubstructureAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// A builder for a natural Substructure based on the
    /// <see cref="IKeyForeignNaturalTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class SubstructureBuilder : KeyForeignNaturalTimeBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( SubstructureNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the foreign table . </summary>
        /// <value> The name of the foreign table. </value>
        public override string ForeignTableName { get; set; } = SubstructureTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <value> The name of the foreign table key. </value>
        public override string ForeignTableKeyName { get; set; } = nameof( SubstructureTypeNub.Id );

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( SubstructureEntity.AutoId );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( SubstructureEntity.SubstructureTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( SubstructureEntity.SubstructureNumber );

        /// <summary>   Gets or sets the name of the timestamp field. </summary>
        /// <value> The name of the timestamp field. </value>
        public override string TimestampFieldName { get; set; } = nameof( SubstructureEntity.Timestamp );

        #region " SINGLETON "

        private static readonly Lazy<SubstructureBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SubstructureBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Substructure table based on the
    /// <see cref="IKeyForeignNaturalTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "Substructure" )]
    public class SubstructureNub : KeyForeignNaturalTimeNub, IKeyForeignNaturalTime
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SubstructureNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignNaturalTime CreateNew()
        {
            return new SubstructureNub();
        }
    }


    /// <summary>
    /// The Substructure Entity based on the <see cref="IKeyForeignNaturalTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class SubstructureEntity : EntityBase<IKeyForeignNaturalTime, SubstructureNub>, IKeyForeignNaturalTime
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SubstructureEntity() : this( new SubstructureNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Substructure interface. </param>
        public SubstructureEntity( IKeyForeignNaturalTime value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public SubstructureEntity( IKeyForeignNaturalTime cache, IKeyForeignNaturalTime store ) : base( new SubstructureNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SubstructureEntity( SubstructureEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.SubstructureBuilder.TableName, nameof( IKeyForeignNaturalTime ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "


        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignNaturalTime CreateNew()
        {
            return new SubstructureNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyForeignNaturalTime CreateCopy()
        {
            var destination = this.CreateNew();
            KeyForeignNaturalTimeNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyForeignNaturalTime value )
        {
            KeyForeignNaturalTimeNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Substructure interface. </param>
        public override void UpdateCache( IKeyForeignNaturalTime value )
        {
            // first make the copy to notify of any property change.
            KeyForeignNaturalTimeNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The Substructure table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyForeignNaturalTime>( key ) : connection.Get<SubstructureNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.SubstructureNumber );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="substrucutreNumber">   Identifies the <see cref="SubstructureEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int substrucutreNumber )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, substrucutreNumber ).SingleOrDefault();
            return nub is object && this.FetchUsingKey( connection, nub.AutoId );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyForeignNaturalTime entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // update the existing record from the specified entity.
            // insert a new record based on the specified entity values.
            // try fetching an existing record
            bool fetched = UniqueIndexOptions.Amount == (SubstructureBuilder.Instance.QueryUniqueIndexOptions( connection ) & UniqueIndexOptions.Amount) ? this.FetchUsingUniqueIndex( connection, entity.Amount ) : this.FetchUsingKey( connection, entity.AutoId );
            if ( fetched )
            {
                entity.AutoId = this.AutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new SubstructureNub() { AutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Substructure entities. </summary>
        /// <value> The Substructure entities. </value>
        public IEnumerable<SubstructureEntity> Substructures { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SubstructureEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyForeignNaturalTime>() ) : Populate( connection.GetAll<SubstructureNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Substructures = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( SubstructureEntity.Substructures ) );
            return this.Substructures?.Any() == true ? this.Substructures.Count() : 0;
        }

        /// <summary>   Populates a list of Substructure entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The Substructure nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SubstructureEntity> Populate( IEnumerable<SubstructureNub> nubs )
        {
            var l = new List<SubstructureEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( SubstructureNub nub in nubs )
                    l.Add( new SubstructureEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Substructure entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The Substructure interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SubstructureEntity> Populate( IEnumerable<IKeyForeignNaturalTime> interfaces )
        {
            var l = new List<SubstructureEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new SubstructureNub();
                foreach ( IKeyForeignNaturalTime iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new SubstructureEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="substructureNumber">   The substructure number. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int substructureNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SubstructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SubstructureNub.Amount )} = @SubstructureNumber", new { substructureNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="substructureNumber">   The substructure number. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntitiesAlternative( System.Data.IDbConnection connection, int substructureNumber )
        {
            var selectSql = new System.Text.StringBuilder( $"SELECT * FROM [{SubstructureBuilder.TableName}]" );
            _ = selectSql.Append( $"WHERE (@{nameof( SubstructureNub.Amount )} IS NULL OR {nameof( SubstructureNub.Amount )} = @SubstructureNumber)" );
            _ = selectSql.Append( "; " );
            return connection.ExecuteScalar<int>( selectSql.ToString(), new { substructureNumber } );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="substructureNumber">   The substructure number. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SubstructureNub> FetchNubs( System.Data.IDbConnection connection, int substructureNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SubstructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SubstructureNub.Amount )} = @SubstructureNumber", new { substructureNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SubstructureNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the substructure exists. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="substructureNumber">   The substructure number. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int substructureNumber )
        {
            return 1 == CountEntities( connection, substructureNumber );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="SubstructureEntity"/>. </summary>
        /// <value> Identifies the <see cref="SubstructureEntity"/>. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="SubstructureEntity"/> type. </summary>
        /// <value> Identifies the <see cref="SubstructureEntity"/> type. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SubstructureEntity.SubstructureTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="SubstructureEntity"/> type. </summary>
        /// <value> Identifies the <see cref="SubstructureEntity"/> type. </value>
        public int SubstructureTypeId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        public int Amount
        {
            get => this.ICache.Amount;

            set {
                if ( this.Amount != value )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SubstructureEntity.SubstructureNumber ) );
                }
            }
        }

        /// <summary>   Gets or sets the substructure number. </summary>
        /// <value> The structure number. </value>
        public int SubstructureNumber
        {
            get => this.Amount;

            set => this.Amount = value;
        }

        /// <summary>   Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTime.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " FIELDS: CUSTOM "

        /// <summary>   Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        /// <summary>   The timestamp caption format. </summary>
        private string _TimestampCaptionFormat;

        /// <summary>   Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._TimestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._TimestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.Timestamp.ToString( this.TimestampCaptionFormat );

        #endregion

    }
}

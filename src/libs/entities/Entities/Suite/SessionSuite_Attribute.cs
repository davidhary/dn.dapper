
namespace isr.Dapper.Entities
{
    /// <summary>   A session suite entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class SessionSuiteEntity
    {

        /// <summary>   Gets or sets the Traits. </summary>
        /// <value> The Traits. </value>
        public SessionSuiteUniqueTraitEntityCollection Traits { get; private set; }

        /// <summary>
        /// Fetches the <see cref="SessionSuiteUniqueTraitEntityCollection"/>.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        public void FetchTraits( System.Data.IDbConnection connection )
        {
            if ( !SessionTraitTypeEntity.IsEnumerated() )
                _ = ProductSortTraitTypeEntity.TryFetchAll( connection );
            this.Traits = new SessionSuiteUniqueTraitEntityCollection( this.SessionSuiteAutoId );
            this.Traits.Populate( SessionSuiteTraitEntity.FetchEntities( connection, this.SessionSuiteAutoId ) );
        }
    }
}

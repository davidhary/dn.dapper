using System;

namespace isr.Dapper.Entities
{
    /// <summary>   An meter element nominal type selector. </summary>
    /// <remarks>   David, 2020-06-16. </remarks>
    public struct MeterElementNomTypeSelector : IEquatable<MeterElementNomTypeSelector>
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="meterId">          Identifies the <see cref="MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="NomTypeEntity"/>. </param>
        public MeterElementNomTypeSelector( int meterId, int elementAutoId, int nomTypeId )
        {
            this.MeterId = meterId;
            this.ElementAutoId = elementAutoId;
            this.NomTypeId = nomTypeId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-05-13. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        public MeterElementNomTypeSelector( PlatformMeter platformMeter )
        {
            this.MeterId = platformMeter.MeterId;
            this.ElementAutoId = platformMeter.PrimaryElementId;
            this.NomTypeId = ( int ) platformMeter.PrimaryNominalType;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="nominals"> The nominals. </param>
        public MeterElementNomTypeSelector( PartNomEntityCollection nominals ) : this( nominals.MeterId, nominals.ElementAutoId, nominals.NomTypeId )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="sampleTrait">  The sample trait. </param>
        public MeterElementNomTypeSelector( SampleTrait sampleTrait ) : this( sampleTrait.MeterId, sampleTrait.ElementAutoId, ( int ) sampleTrait.NomType )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="metaReading">   The meta reading. </param>
        public MeterElementNomTypeSelector( MetaReading metaReading ) : this( metaReading.MeterId, metaReading.ElementAutoId, ( int ) metaReading.NomType )
        {
        }

        /// <summary>   Gets or sets the id of the <see cref="MeterEntity"/>. </summary>
        /// <value> The identifier of the <see cref="MeterEntity"/>. </value>
        public int MeterId { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="ElementEntity"/>. </summary>
        /// <value> The identifier of the <see cref="ElementEntity"/>. </value>
        public int ElementAutoId { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="NomTypeEntity"/>. </summary>
        /// <value> The identifier of the <see cref="NomTypeEntity"/>. </value>
        public int NomTypeId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( MeterElementNomTypeSelector ) obj );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( MeterElementNomTypeSelector other )
        {
            return this.MeterId == other.MeterId && this.ElementAutoId == other.ElementAutoId && this.NomTypeId == other.NomTypeId;
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.MeterId, this.ElementAutoId, this.NomTypeId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( MeterElementNomTypeSelector left, MeterElementNomTypeSelector right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( MeterElementNomTypeSelector left, MeterElementNomTypeSelector right )
        {
            return !(left == right);
        }
    }

}

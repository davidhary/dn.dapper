using System;

namespace isr.Dapper.Entities
{

    /// <summary>   A nut meter nominal type selector. </summary>
    /// <remarks>   David, 2020-07-01. </remarks>
    public struct NutMeterNomTypeSelector : IEquatable<NutMeterNomTypeSelector>
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="nutReadings">  The nut readings. </param>
        public NutMeterNomTypeSelector( NutUniqueNutReadingEntityCollection nutReadings ) : this( nutReadings.NutAutoId, nutReadings.MetaReading.MeterId, ( int ) nutReadings.MetaReading.NomType )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="nutAutoId">    Identifies the nut automatic. </param>
        /// <param name="meterId">      Identifies the <see cref="MeterEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="NomTypeEntity"/>. </param>
        public NutMeterNomTypeSelector( int nutAutoId, int meterId, int nomTypeId )
        {
            this.NutAutoId = nutAutoId;
            this.MeterId = meterId;
            this.NomTypeId = nomTypeId;
        }

        /// <summary>   Gets or sets the id of the nut automatic. </summary>
        /// <value> The identifier of the nut automatic. </value>
        public int NutAutoId { get; set; }

        /// <summary>   Gets or sets the id of the meter. </summary>
        /// <value> The identifier of the meter. </value>
        public int MeterId { get; set; }

        /// <summary>   Gets or sets the id of the nom type. </summary>
        /// <value> The identifier of the nom type. </value>
        public int NomTypeId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( NutMeterNomTypeSelector ) obj );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( NutMeterNomTypeSelector other )
        {
            return this.MeterId == other.MeterId && this.NutAutoId == other.NutAutoId && this.NomTypeId == other.NomTypeId;
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.MeterId, this.NutAutoId, this.NomTypeId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( NutMeterNomTypeSelector left, NutMeterNomTypeSelector right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( NutMeterNomTypeSelector left, NutMeterNomTypeSelector right )
        {
            return !(left == right);
        }
    }
}

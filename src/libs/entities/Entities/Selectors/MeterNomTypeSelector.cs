using System;

namespace isr.Dapper.Entities
{

    /// <summary>   An meter element nominal type selector. </summary>
    /// <remarks>   David, 2020-06-16. </remarks>
    public struct MeterNomTypeSelector : IEquatable<MeterNomTypeSelector>
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-22. </remarks>
        /// <param name="nominals"> The nominals. </param>
        public MeterNomTypeSelector( PartNomEntityCollection nominals ) : this( nominals.MeterId, nominals.NomTypeId )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="nutReadings">  The nut readings. </param>
        public MeterNomTypeSelector( NutUniqueNutReadingEntityCollection nutReadings ) : this( nutReadings.MetaReading.MeterId, ( int ) nutReadings.MetaReading.NomType )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-22. </remarks>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        public MeterNomTypeSelector( int meterId, int nomTypeId )
        {
            this.MeterId = meterId;
            this.NomTypeId = nomTypeId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-05-14. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        public MeterNomTypeSelector( PlatformMeter platformMeter )
        {
            this.MeterId = platformMeter.MeterId;
            this.NomTypeId = ( int ) platformMeter.PrimaryNominalType;
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public int NomTypeId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( MeterNomTypeSelector ) obj );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( MeterNomTypeSelector other )
        {
            return this.MeterId == other.MeterId && this.NomTypeId == other.NomTypeId;
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.MeterId, this.NomTypeId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( MeterNomTypeSelector left, MeterNomTypeSelector right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( MeterNomTypeSelector left, MeterNomTypeSelector right )
        {
            return !(left == right);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Lot OhmMeterSetup builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class LotOhmMeterSetupBuilder : OneToManyIdBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( LotOhmMeterSetupNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = LotBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( LotNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = MeterBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( MeterNub.Id );

        /// <summary>   Gets or sets the name of the Foreign ID table. </summary>
        /// <value> The name of the Foreign ID table. </value>
        public override string ForeignIdTableName { get; set; } = OhmMeterSetupBuilder.TableName;

        /// <summary>   Gets or sets the name of the Foreign ID table key. </summary>
        /// <value> The name of the Foreign ID table key. </value>
        public override string ForeignIdTableKeyName { get; set; } = nameof( OhmMeterSetupNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( LotOhmMeterSetupEntity.LotAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( LotOhmMeterSetupEntity.MeterId );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( LotOhmMeterSetupEntity.OhmMeterSetupAutoId );

        #region " SINGLETON "

        private static readonly Lazy<LotOhmMeterSetupBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static LotOhmMeterSetupBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="Dapper.Entities.LotEntity"/> OhmMeterSetup table
    /// <see cref="IOneToManyId">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "LotOhmMeterSetup" )]
    public class LotOhmMeterSetupNub : OneToManyIdNub, IOneToManyId
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public LotOhmMeterSetupNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyId CreateNew()
        {
            return new LotOhmMeterSetupNub();
        }
    }

    /// <summary>
    /// The <see cref="Dapper.Entities.LotEntity"/>OhmMeterSetup Entity. Implements access to the database
    /// using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class LotOhmMeterSetupEntity : EntityBase<IOneToManyId, LotOhmMeterSetupNub>, IOneToManyId
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public LotOhmMeterSetupEntity() : this( new LotOhmMeterSetupNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotOhmMeterSetupEntity( IOneToManyId value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public LotOhmMeterSetupEntity( IOneToManyId cache, IOneToManyId store ) : base( new LotOhmMeterSetupNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotOhmMeterSetupEntity( LotOhmMeterSetupEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.LotOhmMeterSetupBuilder.TableName, nameof( IOneToManyId ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyId CreateNew()
        {
            return new LotOhmMeterSetupNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyId CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyIdNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="LotOhmMeterSetupEntity"/> interface value. </param>
        public override void CopyFrom( IOneToManyId value )
        {
            OneToManyIdNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached OhmMeterSetup, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.LotEntity"/>OhmMeterSetup interface. </param>
        public override void UpdateCache( IOneToManyId value )
        {
            // first make the copy to notify of any property change.
            OneToManyIdNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int lotAutoId, int meterId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, lotAutoId, meterId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int lotAutoId, int meterId )
        {
            this.ClearStore();
            return this.FetchUsingKey( connection, lotAutoId, meterId );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToManyId entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int lotAutoId, int meterId )
        {
            return connection.Delete( new LotOhmMeterSetupNub() { PrimaryId = lotAutoId, SecondaryId = meterId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.LotEntity"/>OhmMeterSetup entities. </summary>
        /// <value> The lot ohm meter setups. </value>
        public IEnumerable<LotOhmMeterSetupEntity> LotOhmMeterSetups { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotOhmMeterSetupEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToManyId>() ) : Populate( connection.GetAll<LotOhmMeterSetupNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.LotOhmMeterSetups = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( LotOhmMeterSetupEntity.LotOhmMeterSetups ) );
            return this.LotOhmMeterSetups?.Any() == true ? this.LotOhmMeterSetups.Count() : 0;
        }

        /// <summary>   Count LotOhmMeterSetups. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.LotOhmMeterSetupBuilder.TableName}] WHERE {nameof( LotOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotOhmMeterSetupEntity> FetchEntities( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotOhmMeterSetupBuilder.TableName}] WHERE {nameof( LotOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<LotOhmMeterSetupNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Lot ohm meter setup by Lot auto id; expected single or none. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<LotOhmMeterSetupNub> FetchNubs( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotOhmMeterSetupEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LotOhmMeterSetupNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of LotOhmMeterSetup entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.LotEntity"/>OhmMeterSetup nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotOhmMeterSetupEntity> Populate( IEnumerable<LotOhmMeterSetupNub> nubs )
        {
            var l = new List<LotOhmMeterSetupEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( LotOhmMeterSetupNub nub in nubs )
                    l.Add( new LotOhmMeterSetupEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of LotOhmMeterSetup entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.LotEntity"/>OhmMeterSetup interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotOhmMeterSetupEntity> Populate( IEnumerable<IOneToManyId> interfaces )
        {
            var l = new List<LotOhmMeterSetupEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new LotOhmMeterSetupNub();
                foreach ( IOneToManyId iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new LotOhmMeterSetupEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Lot Traits; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int lotAutoId, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotOhmMeterSetupNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Lot Ohm Meter Setup by Lot Auto ID and Meter Id;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<LotOhmMeterSetupNub> FetchNubs( System.Data.IDbConnection connection, int lotAutoId, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotOhmMeterSetupNub.PrimaryId )} = @primaryId", new { primaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotOhmMeterSetupNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LotOhmMeterSetupNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.LotEntity"/>OhmMeterSetup exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int lotAutoId, int meterId )
        {
            return 1 == CountEntities( connection, lotAutoId, meterId );
        }

        #endregion

        #region " RELATIONS: LOT "

        /// <summary>   Count OhmMeterSetups associated with this Lot. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of OhmMeterSetup OhmMeterSetups. </returns>
        public int CountLotOhmMeterSetups( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Lot OhmMeterSetup OhmMeterSetups by <see cref="LotOhmMeterSetupEntity.LotAutoId"/>;
        /// expected as manger as the number of meters.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<LotOhmMeterSetupNub> FetchLotOhmMeterSetups( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LotOhmMeterSetupNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.LotEntity"/> Entity. </summary>
        /// <value> The lot entity. </value>
        public LotEntity LotEntity { get; private set; }

        /// <summary>   Fetches a Lot Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchLotEntity( System.Data.IDbConnection connection )
        {
            this.LotEntity = new LotEntity();
            return this.LotEntity.FetchUsingKey( connection, this.LotAutoId );
        }

        #endregion

        #region " RELATIONS: METER "

        /// <summary>   The meter entity. </summary>
        private MeterEntity _MeterEntity;

        /// <summary>   Gets the meter entity. </summary>
        /// <value> The meter entity. </value>
        public MeterEntity MeterEntity
        {
            get {
                if ( this._MeterEntity is null )
                {
                    this._MeterEntity = MeterEntity.EntityLookupDictionary()[this.MeterId];
                }

                return this._MeterEntity;
            }
        }

        /// <summary>   Fetches the meter. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchMeterEntity( System.Data.IDbConnection connection )
        {
            if ( !MeterEntity.IsEnumerated() )
                _ = MeterEntity.TryFetchAll( connection );
            this._MeterEntity = MeterEntity.EntityLookupDictionary()[this.MeterId];
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.MeterEntity ) );
            return this._MeterEntity is object;
        }

        #endregion

        #region " RELATIONS: METER MODEL "

        /// <summary>   The Meter Model entity. </summary>
        private MeterModelEntity _MeterModelEntity;

        /// <summary>   Gets the Meter Model entity. </summary>
        /// <value> The meter model entity. </value>
        public MeterModelEntity MeterModelEntity
        {
            get {
                if ( this._MeterModelEntity is null )
                {
                    this._MeterModelEntity = this.MeterEntity.MeterModelEntity;
                }

                return this._MeterModelEntity;
            }
        }

        /// <summary>   Fetches the Meter Model entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchMeterModel( System.Data.IDbConnection connection )
        {
            _ = this.FetchMeterEntity( connection );
            _ = this.MeterEntity.FetchMeterModel( connection );
            this._MeterModelEntity = this.MeterEntity.MeterModelEntity;
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.MeterModelEntity ) );
            return this.MeterModelEntity is object;
        }

        #endregion

        #region " RELATIONS: OHM METER SETUP "

        /// <summary>   Gets or sets the ohm meter setup entity. </summary>
        /// <value> The ohm meter setup entity. </value>
        public OhmMeterSetupEntity OhmMeterSetupEntity { get; private set; }

        /// <summary>   Fetches ohm meter setup entity. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool FetchOhmMeterSetupEntity( System.Data.IDbConnection connection )
        {
            this.OhmMeterSetupEntity = new OhmMeterSetupEntity();
            return this.OhmMeterSetupEntity.FetchUsingKey( connection, this.OhmMeterSetupAutoId );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.OhmMeterSetupEntity"/>'s. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The OhmMeterSetup. </returns>
        public static IEnumerable<OhmMeterSetupEntity> FetchOhmMeterSetups( System.Data.IDbConnection connection, string selectQuery, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { lotAutoId } );
            // Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, new {lotAutoId = lotAutoId})
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return OhmMeterSetupEntity.Populate( connection.Query<OhmMeterSetupNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches <see cref="Dapper.Entities.OhmMeterSetupEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The <see cref="IEnumerable{OhmMeterSetupEntity}"/>. </returns>
        public static IEnumerable<OhmMeterSetupEntity> FetchOhmMeterSetups( System.Data.IDbConnection connection, int lotAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [LotOhmMeterSetup] on [LotOhmMeterSetup].SecondaryId = [OhmMeterSetup].AutoId where [LotOhmMeterSetup].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{OhmMeterSetupBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{OhmMeterSetupBuilder.TableName}] Inner Join [{LotOhmMeterSetupBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.LotOhmMeterSetupBuilder.TableName}].{nameof( LotOhmMeterSetupNub.ForeignId )} = [{isr.Dapper.Entities.OhmMeterSetupBuilder.TableName}].{nameof( OhmMeterSetupNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.LotOhmMeterSetupBuilder.TableName}].{nameof( LotOhmMeterSetupNub.PrimaryId )} = @{nameof( lotAutoId )}; " );
            return FetchOhmMeterSetups( connection, queryBuilder.ToString(), lotAutoId );
        }

        /// <summary>   Adds a Lot ohm meter setup entity. </summary>
        /// <remarks>   David, 2020-06-14. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the Lot identity. </param>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="nominalValue">     The nominal value. </param>
        /// <param name="meterId">          The identifier of the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   An OhmMeterSetupEntity. </returns>
        public static (bool Success, string Details, OhmMeterSetupEntity OhmMeterSetup) AddLotOhmMeterSetupEntity( System.Data.IDbConnection connection,
            int lotAutoId, string toleranceCode, double nominalValue, int meterId )
        {
            var ohmMeterSettingLookup = new OhmMeterSettingLookupEntity();
            var ohmMeterSettingEntity = new OhmMeterSettingEntity();
            var ohmMeterSetupEntity = new OhmMeterSetupEntity();
            var lotOhmMeterSetupEntity = new LotOhmMeterSetupEntity();
            (bool Success, string Details, OhmMeterSetupEntity OhmMeterSetup) result = (true, string.Empty, ohmMeterSetupEntity);
            ohmMeterSettingLookup = OhmMeterSettingLookupEntity.SelectOhmMeterSettingLookupEntity( toleranceCode, meterId, nominalValue );
            (bool Success, string Details) r;
            r = ohmMeterSettingLookup.ValidateStoredEntity( $"Selected {nameof( OhmMeterSettingLookupEntity )} for tolerance '{toleranceCode}' and nominal value of {nominalValue}" );
            if ( !r.Success )
                result = (r.Success, r.Details, ohmMeterSetupEntity);
            if ( result.Success )
            {
                ohmMeterSettingEntity = new OhmMeterSettingEntity() { Id = ohmMeterSettingLookup.OhmMeterSettingId };
                _ = ohmMeterSettingEntity.FetchUsingUniqueIndex( connection );
                r = ohmMeterSettingEntity.ValidateStoredEntity( $"Fetched {nameof( OhmMeterSettingEntity )} with {nameof( OhmMeterSettingLookupEntity.OhmMeterSettingId )} of {ohmMeterSettingLookup.OhmMeterSettingId}" );
                if ( !r.Success )
                    result = (r.Success, r.Details, ohmMeterSetupEntity);
            }

            if ( result.Success )
            {
                ohmMeterSetupEntity.Copy( ohmMeterSettingEntity );
                _ = ohmMeterSetupEntity.Insert( connection );
                r = ohmMeterSetupEntity.ValidateStoredEntity( $"Inserted {nameof( Dapper.Entities.OhmMeterSetupEntity )}" );
                if ( !r.Success )
                    result = (r.Success, r.Details, ohmMeterSetupEntity);
            }

            if ( result.Success )
            {
                lotOhmMeterSetupEntity = new LotOhmMeterSetupEntity() {
                    LotAutoId = lotAutoId,
                    OhmMeterSetupAutoId = ohmMeterSetupEntity.AutoId,
                    MeterId = meterId
                };
                _ = lotOhmMeterSetupEntity.Upsert( connection );
                r = lotOhmMeterSetupEntity.ValidateStoredEntity( $"Upserted {nameof( LotOhmMeterSetupEntity )} " );
                if ( !r.Success )
                    result = (r.Success, r.Details, ohmMeterSetupEntity);
            }

            return result;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( LotOhmMeterSetupEntity.LotAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.LotEntity"/> record.
        /// </summary>
        /// <value> The identifier of the lot automatic. </value>
        public int LotAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( LotOhmMeterSetupEntity.MeterId ) );
                }
            }
        }

        /// <summary>   Gets or sets the identity of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> The identifier of the meter. </value>
        public int MeterId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>   Gets or sets the Three-to-Many referenced table identifier. </summary>
        /// <value> The identifier of Three-to-Many referenced table. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( LotOhmMeterSetupEntity.OhmMeterSetupAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the identity of the <see cref="OhmMeterSetupEntity"/>. </summary>
        /// <value> The identifier of the ohm meter setup automatic. </value>
        public int OhmMeterSetupAutoId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of Lot Trait Natural (Integer-value) entities. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class LotOhmMeterSetupEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToManyId, LotOhmMeterSetupNub, LotOhmMeterSetupEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( LotOhmMeterSetupEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( LotOhmMeterSetupEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all Lots from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<LotOhmMeterSetupEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( LotOhmMeterSetupEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return LotOhmMeterSetupBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of Lot-Unique Lot Trait Natural (Integer-value) Entities unique to the Lot.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class LotUniqueOhmMeterSetupEntityCollection : LotOhmMeterSetupEntityCollection
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public LotUniqueOhmMeterSetupEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        public LotUniqueOhmMeterSetupEntityCollection( int lotAutoId ) : this()
        {
            this.LotAutoId = lotAutoId;
        }

        /// <summary>
        /// Dictionary of unique indexes for selecting the ohm meter setup by meter.
        /// </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>
        /// Dictionary of primary keys for selecting the ohm meter setup by meter id.
        /// </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( LotOhmMeterSetupEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.MeterId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.MeterId );
        }

        /// <summary>
        /// Removes all Lots from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains 'ohmMeterSetupAutoId' key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="meterId">  Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int meterId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( meterId );
        }

        /// <summary>
        /// Gets the setup for the specified <see cref="Dapper.Entities.MeterEntity"/> Id.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="meterId">  Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   A LotOhmMeterSetupEntity. </returns>
        public LotOhmMeterSetupEntity OhmMeterSetup( int meterId )
        {
            return this.ContainsKey( meterId ) ? this[this._PrimaryKeyDictionary[meterId]] : new LotOhmMeterSetupEntity();
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.LotEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.LotEntity"/>. </value>
        public int LotAutoId { get; private set; }
    }
}

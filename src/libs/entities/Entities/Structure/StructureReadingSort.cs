using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Structure-ProductSort builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class StructureProductSortBuilder : OneToOneBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( StructureProductSortNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = StructureBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( StructureNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = ProductSortBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ProductSortNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( StructureProductSortEntity.StructureAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( StructureProductSortEntity.ProductSortAutoId );

        #region " SINGLETON "

        private static readonly Lazy<StructureProductSortBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static StructureProductSortBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Structure ProductSort Nub based on the <see cref="IOneToOne">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "StructureProductSort" )]
    public class StructureProductSortNub : OneToOneNub, IOneToOne
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public StructureProductSortNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToOne CreateNew()
        {
            return new StructureProductSortNub();
        }
    }

    /// <summary>
    /// The Structure-ProductSort Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class StructureProductSortEntity : EntityBase<IOneToOne, StructureProductSortNub>, IOneToOne
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public StructureProductSortEntity() : this( new StructureProductSortNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public StructureProductSortEntity( IOneToOne value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public StructureProductSortEntity( IOneToOne cache, IOneToOne store ) : base( new StructureProductSortNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public StructureProductSortEntity( StructureProductSortEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.StructureProductSortBuilder.TableName, nameof( IOneToOne ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToOne CreateNew()
        {
            return new StructureProductSortNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToOne CreateCopy()
        {
            var destination = this.CreateNew();
            OneToOneNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToOne value )
        {
            OneToOneNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Structure-ProductSort interface. </param>
        public override void UpdateCache( IOneToOne value )
        {
            // first make the copy to notify of any property change.
            OneToOneNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int structureAutoId, int productSortAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, structureAutoId, productSortAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.StructureAutoId, this.ProductSortAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.StructureAutoId, this.ProductSortAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int structureAutoId, int productSortAutoId )
        {
            return this.FetchUsingKey( connection, structureAutoId, productSortAutoId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToOne entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int structureAutoId, int productSortAutoId )
        {
            return connection.Delete( new StructureProductSortNub() { PrimaryId = structureAutoId, SecondaryId = productSortAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Structure-ProductSort entities. </summary>
        /// <value> The Structure-ProductSort entities. </value>
        public IEnumerable<StructureProductSortEntity> StructureProductSorts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<StructureProductSortEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToOne>() ) : Populate( connection.GetAll<StructureProductSortNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.StructureProductSorts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( StructureProductSortEntity.StructureProductSorts ) );
            return this.StructureProductSorts?.Any() == true ? this.StructureProductSorts.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<StructureProductSortEntity> Populate( IEnumerable<StructureProductSortNub> nubs )
        {
            var l = new List<StructureProductSortEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( StructureProductSortNub nub in nubs )
                    l.Add( new StructureProductSortEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<StructureProductSortEntity> Populate( IEnumerable<IOneToOne> interfaces )
        {
            var l = new List<StructureProductSortEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new StructureProductSortNub();
                foreach ( IOneToOne iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new StructureProductSortEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to ProductSort entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{StructureProductSortBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( StructureProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<StructureProductSortEntity> FetchEntities( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.StructureProductSortBuilder.TableName}] WHERE {nameof( StructureProductSortNub.PrimaryId )} = @Id", new { Id = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<StructureProductSortNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by ProductSort. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>   The total number of entities by ProductSort. </returns>
        public static int CountEntitiesByProductSort( System.Data.IDbConnection connection, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{StructureProductSortBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( StructureProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by ProductSorts in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by ProductSorts in this
        /// collection.
        /// </returns>
        public static IEnumerable<StructureProductSortEntity> FetchEntitiesByProductSort( System.Data.IDbConnection connection, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.StructureProductSortBuilder.TableName}] WHERE {nameof( StructureProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<StructureProductSortNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int structureAutoId, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{StructureProductSortBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( StructureProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            _ = sqlBuilder.Where( $"{nameof( StructureProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<StructureProductSortNub> FetchNubs( System.Data.IDbConnection connection, int structureAutoId, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{StructureProductSortBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( StructureProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            _ = sqlBuilder.Where( $"{nameof( StructureProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<StructureProductSortNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Structure ProductSort exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="structureAutoId">      Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int structureAutoId, int productSortAutoId )
        {
            return 1 == CountEntities( connection, structureAutoId, productSortAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Structure entity. </summary>
        /// <value> The Structure entity. </value>
        public StructureEntity StructureEntity { get; private set; }

        /// <summary>   Fetches Structure Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Structure Entity. </returns>
        public StructureEntity FetchStructureEntity( System.Data.IDbConnection connection )
        {
            var entity = new StructureEntity();
            _ = entity.FetchUsingKey( connection, this.StructureAutoId );
            this.StructureEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Structures associated with the specified <paramref name="productSortAutoId"/>; expected
        /// 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>   The total number of Structures. </returns>
        public static int CountStructures( System.Data.IDbConnection connection, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.StructureProductSortBuilder.TableName}] WHERE {nameof( StructureProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Structures associated with the specified <paramref name="productSortAutoId"/>;
        /// expected a single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Structures in this collection.
        /// </returns>
        public static IEnumerable<ProductSortEntity> FetchStructures( System.Data.IDbConnection connection, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.StructureProductSortBuilder.TableName}] WHERE {nameof( StructureProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ProductSortEntity>();
            foreach ( StructureProductSortNub nub in connection.Query<StructureProductSortNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new StructureProductSortEntity( nub );
                l.Add( entity.FetchProductSortEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Structures associated with the specified <paramref name="productSortAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>Sort. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteStructures( System.Data.IDbConnection connection, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.StructureProductSortBuilder.TableName}] WHERE {nameof( StructureProductSortNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the ProductSort entity. </summary>
        /// <value> The ProductSort entity. </value>
        public ProductSortEntity ProductSortEntity { get; private set; }

        /// <summary>   Fetches a ProductSort Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The ProductSort Entity. </returns>
        public ProductSortEntity FetchProductSortEntity( System.Data.IDbConnection connection )
        {
            var entity = new ProductSortEntity();
            _ = entity.FetchUsingKey( connection, this.ProductSortAutoId );
            this.ProductSortEntity = entity;
            return entity;
        }

        /// <summary>   Count product sorts. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   The total number of product sorts. </returns>
        public static int CountProductSorts( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.StructureProductSortBuilder.TableName}] WHERE {nameof( StructureProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the ProductSorts in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ProductSorts in this collection.
        /// </returns>
        public static IEnumerable<ProductSortEntity> FetchProductSorts( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.StructureProductSortBuilder.TableName}] WHERE {nameof( StructureProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ProductSortEntity>();
            foreach ( StructureProductSortNub nub in connection.Query<StructureProductSortNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new StructureProductSortEntity( nub );
                l.Add( entity.FetchProductSortEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all ProductSort related to the specified Structure. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteProductSorts( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.StructureProductSortBuilder.TableName}] WHERE {nameof( StructureProductSortNub.PrimaryId )} = @PrimaryId", new { PrimaryId = structureAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( StructureProductSortEntity.StructureAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( StructureProductSortEntity.ProductSortAutoId ) );
                }
            }
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="StructureEntity"/>. </summary>
        /// <value> Identifies the <see cref="StructureEntity"/>. </value>
        public int StructureAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the ProductSort. </summary>
        /// <value> Identifies the ProductSort. </value>
        public int ProductSortAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

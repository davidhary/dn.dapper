using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Session-Structure builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class SessionStructureBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( SessionStructureNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = SessionBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( SessionNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = StructureBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( StructureNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( SessionStructureEntity.SessionAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( SessionStructureEntity.StructureAutoId );

        #region " SINGLETON "

        private static readonly Lazy<SessionStructureBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SessionStructureBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Session Structure Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "SessionStructure" )]
    public class SessionStructureNub : OneToManyNub, IOneToMany
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SessionStructureNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new SessionStructureNub();
        }
    }

    /// <summary>
    /// The Session-Structure Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class SessionStructureEntity : EntityBase<IOneToMany, SessionStructureNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SessionStructureEntity() : this( new SessionStructureNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SessionStructureEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public SessionStructureEntity( IOneToMany cache, IOneToMany store ) : base( new SessionStructureNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SessionStructureEntity( SessionStructureEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.SessionStructureBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new SessionStructureNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Session-Structure interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int sessionAutoId, int structureAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, sessionAutoId, structureAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.SessionAutoId, this.StructureAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.SessionAutoId, this.StructureAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int sessionAutoId, int structureAutoId )
        {
            return this.FetchUsingKey( connection, sessionAutoId, structureAutoId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int sessionAutoId, int structureAutoId )
        {
            return connection.Delete( new SessionStructureNub() { PrimaryId = sessionAutoId, SecondaryId = structureAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Session-Structure entities. </summary>
        /// <value> The Session-Structure entities. </value>
        public IEnumerable<SessionStructureEntity> SessionStructures { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionStructureEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<SessionStructureNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.SessionStructures = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( SessionStructureEntity.SessionStructures ) );
            return this.SessionStructures?.Any() == true ? this.SessionStructures.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SessionStructureEntity> Populate( IEnumerable<SessionStructureNub> nubs )
        {
            var l = new List<SessionStructureEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( SessionStructureNub nub in nubs )
                    l.Add( new SessionStructureEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SessionStructureEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<SessionStructureEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new SessionStructureNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new SessionStructureEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Structure entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionStructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionStructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionStructureEntity> FetchEntities( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionStructureBuilder.TableName}] WHERE {nameof( SessionStructureNub.PrimaryId )} = @Id", new { Id = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<SessionStructureNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Structure. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   The total number of entities by Structure. </returns>
        public static int CountEntitiesByStructure( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionStructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionStructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Structures in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Structures in this
        /// collection.
        /// </returns>
        public static IEnumerable<SessionStructureEntity> FetchEntitiesByStructure( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionStructureBuilder.TableName}] WHERE {nameof( SessionStructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<SessionStructureNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int sessionAutoId, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionStructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionStructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            _ = sqlBuilder.Where( $"{nameof( SessionStructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionStructureNub> FetchNubs( System.Data.IDbConnection connection, int sessionAutoId, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SessionStructureBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionStructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            _ = sqlBuilder.Where( $"{nameof( SessionStructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SessionStructureNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Session Structure exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int sessionAutoId, int structureAutoId )
        {
            return 1 == CountEntities( connection, sessionAutoId, structureAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Session entity. </summary>
        /// <value> The Session entity. </value>
        public SessionEntity SessionEntity { get; private set; }

        /// <summary>   Fetches Session Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Session Entity. </returns>
        public SessionEntity FetchSessionEntity( System.Data.IDbConnection connection )
        {
            var entity = new SessionEntity();
            _ = entity.FetchUsingKey( connection, this.SessionAutoId );
            this.SessionEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Sessions associated with the specified <paramref name="structureAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   The total number of Sessions. </returns>
        public static int CountSessions( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.SessionStructureBuilder.TableName}] WHERE {nameof( SessionStructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Sessions associated with the specified <paramref name="structureAutoId"/>;
        /// expected a single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Sessions in this collection.
        /// </returns>
        public static IEnumerable<StructureEntity> FetchSessions( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionStructureBuilder.TableName}] WHERE {nameof( SessionStructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<StructureEntity>();
            foreach ( SessionStructureNub nub in connection.Query<SessionStructureNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new SessionStructureEntity( nub );
                l.Add( entity.FetchStructureEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Sessions associated with the specified <paramref name="structureAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteSessions( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.SessionStructureBuilder.TableName}] WHERE {nameof( SessionStructureNub.SecondaryId )} = @SecondaryId", new { SecondaryId = structureAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the Structure entity. </summary>
        /// <value> The Structure entity. </value>
        public StructureEntity StructureEntity { get; private set; }

        /// <summary>   Fetches a Structure Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Structure Entity. </returns>
        public StructureEntity FetchStructureEntity( System.Data.IDbConnection connection )
        {
            var entity = new StructureEntity();
            _ = entity.FetchUsingKey( connection, this.StructureAutoId );
            this.StructureEntity = entity;
            return entity;
        }

        /// <summary>   Count structures. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   The total number of structures. </returns>
        public static int CountStructures( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.SessionStructureBuilder.TableName}] WHERE {nameof( SessionStructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Structures in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Structures in this collection.
        /// </returns>
        public static IEnumerable<StructureEntity> FetchStructures( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionStructureBuilder.TableName}] WHERE {nameof( SessionStructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<StructureEntity>();
            foreach ( SessionStructureNub nub in connection.Query<SessionStructureNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new SessionStructureEntity( nub );
                l.Add( entity.FetchStructureEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Structure related to the specified Session. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteStructures( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.SessionStructureBuilder.TableName}] WHERE {nameof( SessionStructureNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SessionStructureEntity.SessionAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SessionStructureEntity.StructureAutoId ) );
                }
            }
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Session. </summary>
        /// <value> Identifies the Session. </value>
        public int SessionAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the <see cref="StructureEntity"/>. </summary>
        /// <value> Identifies the <see cref="StructureEntity"/>. </value>
        public int StructureAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

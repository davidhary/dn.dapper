using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the Tolerance nub and entity. Includes the fields as kept in the data table.
    /// Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value. <para>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ITolerance
    {

        /// <summary>   Gets or sets the id of the Tolerance. </summary>
        /// <value> Identifies the Tolerance. </value>
        [ExplicitKey]
        string ToleranceCode { get; set; }

        /// <summary>   Gets or sets the lower limit. </summary>
        /// <value> The lower limit. </value>
        double LowerLimit { get; set; }

        /// <summary>   Gets or sets the upper limit. </summary>
        /// <value> The upper limit. </value>
        double UpperLimit { get; set; }

        /// <summary>   Gets or sets the caption. </summary>
        /// <value> The caption. </value>
        string Caption { get; set; }

        /// <summary>   Gets or sets the compound caption. </summary>
        /// <value> The compound caption. </value>
        [Computed]
        string CompoundCaption { get; set; }
    }

    /// <summary>   A tolerance builder. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public sealed class ToleranceBuilder
    {

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ToleranceNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValues( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? InsertValues( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? InsertValues( sqlite ) : 0;
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValues( System.Data.SQLite.SQLiteConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('A', -0.0005, 0.0005,   '±0.05%' ,'A: ±0.05%' ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('B', -0.001, 0.001,     '±0.1%'  ,'B: ±0.1%'  ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('C', -0.0025, 0.0025,   '±0.25%' ,'C: ±0.25%' ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('D', -0.005, 0.005,     '±0.5%'  ,'D: ±0.5%'  ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('F', -0.01, 0.01,       '±1%'    ,'F: ±1%'    ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('G', -0.02, 0.02,       '±2%'    ,'G: ±2%'    ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('H', -0.03, 0.03,       '±3%'    ,'H: ±3%'    ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('J', -0.05, 0.05,       '±5%'    ,'J: ±5%'    ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('K', -0.1, 0.1,         '±10%'   ,'K: ±10%'   ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('M', -0.2, 0.2,         '±20%'   ,'M: ±20%'   ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('P', -0.0015, 0.0015,   '±0.15%' ,'P: ±0.15%' ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('Q', -0.0002, 0.0002,   '±0.02%' ,'Q: ±0.02%' ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('R', -0.002, 0.002,     '±0.2%'  ,'R: ±0.2%'  ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('S', -0.00025, 0.00025, '±0.025%','S: ±0.025%'); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('T', -0.0001, 0.0001,   '±0.01%' ,'T: ±0.01%' ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('U', -0.0003, 0.0003,   '±0.03%' ,'U: ±0.03%' ); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('V', -0.0005, 0.0005,   '±0.005%','V: ±0.005%'); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('Y', -0.00015, 0.00015, '±0.015%','Y: ±0.015%'); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES ('Z', -0.99, 0.99,       '±99%'   ,'Z: ±99%'   ); " );
            return connection.Execute( builder.ToString() );
        }

        /// <summary>   Inserts or ignores the records using an SQL client connection. </summary>
        /// <remarks>   David, 2020-03-14. Supports SQL Client only library. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValuesSqlClient( System.Data.IDbConnection connection )
        {
            return InsertValues( connection as System.Data.SqlClient.SqlConnection );
        }

        /// <summary>   Inserts the values SQL lite described by connection. </summary>
        /// <remarks>   David, 2020-06-18. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValuesSQLite( System.Data.IDbConnection connection )
        {
            return InsertValues( connection as System.Data.SQLite.SQLiteConnection );
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValues( System.Data.SqlClient.SqlConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'A') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'A', -0.0005, 0.0005, N'±0.05%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'B') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'B', -0.001, 0.001, N'±0.1%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'C') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'C', -0.0025, 0.0025, N'±0.25%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'D') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'D', -0.005, 0.005, N'±0.5%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'F') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'F', -0.01, 0.01, N'±1%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'G') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'G', -0.02, 0.02, N'±2%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'H') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'H', -0.03, 0.03, N'±3%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'J') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'J', -0.05, 0.05, N'±5%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'K') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'K', -0.1, 0.1, N'±10%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'M') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'M', -0.2, 0.2, N'±20%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'P') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'P', -0.0015, 0.0015, N'±0.15%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'Q') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'Q', -0.0002, 0.0002, N'±0.02%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'R') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'R', -0.002, 0.002, N'±0.2%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'S') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'S', -0.00025, 0.00025, N'±0.025%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'T') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'T', -0.0001, 0.0001, N'±0.01%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'U') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'U', -0.0003, 0.0003, N'±0.03%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'V') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'V', -0.0005, 0.0005, N'±0.005%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'Y') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'Y', -0.00015, 0.00015, N'±0.015%'); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE [{nameof( ToleranceNub.ToleranceCode )}] = N'Z') INSERT INTO [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([ToleranceCode], [LowerLimit], [UpperLimit], [Caption]) VALUES (N'Z', -0.99, 0.99, N'±99%'); " );
            return connection.Execute( builder.ToString() );
        }

        /// <summary>   Updates the compound caption. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The key. </param>
        /// <param name="value">        The value. </param>
        public static void UpdateCompoundCaption( System.Data.IDbConnection connection, string key, string value )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( $"UPDATE [{TableName}] " );
            _ = queryBuilder.Append( $"SET [{nameof( ToleranceNub.CompoundCaption )}] = '{value}' " );
            _ = queryBuilder.Append( $"WHERE [{nameof( ToleranceNub.ToleranceCode )}] = '{key}'; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
        }

        /// <summary>   Gets the name of the unique label index. </summary>
        /// <value> The name of the unique label index. </value>
        private static string CaptionIndexName => $"UQ_{isr.Dapper.Entities.ToleranceBuilder.TableName}_{nameof( ToleranceNub.Caption )}";

        /// <summary>   The using unique caption. </summary>
        private static bool? _UsingUniqueCaption;

        /// <summary>   Indicates if the entity uses a unique Caption. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool UsingUniqueCaption( System.Data.IDbConnection connection )
        {
            if ( !_UsingUniqueCaption.HasValue )
            {
                _UsingUniqueCaption = connection.IndexExists( CaptionIndexName );
            }

            return _UsingUniqueCaption.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The SQL Query or empty. </returns>
        public static string CreateUniqueIndex( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateUniqueIndex( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateUniqueIndex( sqlite ) : string.Empty;
        }

        /// <summary>   Creates unique index for SQLite database table. </summary>
        /// <remarks>   David, 2020-06-08. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The SQL Script. </returns>
        private static string CreateUniqueIndex( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{isr.Dapper.Entities.ToleranceBuilder.CaptionIndexName}] ON [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([{nameof( ToleranceNub.Caption )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return queryBuilder.ToString();
        }

        /// <summary>   Creates unique index for SQL Server database table. </summary>
        /// <remarks>   David, 2020-06-08. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The SQL Script. </returns>
        private static string CreateUniqueIndex( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.ToleranceBuilder.TableName}]') AND name = N'{isr.Dapper.Entities.ToleranceBuilder.CaptionIndexName}')
            CREATE UNIQUE NONCLUSTERED INDEX [{isr.Dapper.Entities.ToleranceBuilder.CaptionIndexName}] ON [dbo].[{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([{nameof( ToleranceNub.Caption )}] ASC) 
             WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return queryBuilder.ToString();
        }

        /// <summary>   Removes the duplicates described by connection. </summary>
        /// <remarks>   David, 2020-06-08. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of records affected. </returns>
        public static int RemoveDuplicates( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? RemoveDuplicates( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? RemoveDuplicates( sqlite ) : 0;
        }

        /// <summary>   Removes the duplicates described by connection. </summary>
        /// <remarks>   David, 2020-06-08. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of records affected. </returns>
        private static int RemoveDuplicates( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( @$"DELETE FROM [dbo].[{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE {nameof( ToleranceNub.ToleranceCode )} = '@'; " );
            return connection.Execute( queryBuilder.ToString().Clean() );
        }

        /// <summary>   Removes the duplicates described by connection. </summary>
        /// <remarks>   David, 2020-06-08. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of records affected. </returns>
        private static int RemoveDuplicates( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( @$"DELETE FROM [dbo].[{isr.Dapper.Entities.ToleranceBuilder.TableName}] WHERE {nameof( ToleranceNub.ToleranceCode )} = N'@'; " );
            return connection.Execute( queryBuilder.ToString().Clean() );
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Entities.ToleranceBuilder.TableName}] (
                [{nameof( ToleranceNub.ToleranceCode )}] [nchar](1) NOT NULL PRIMARY KEY, 
                [{nameof( ToleranceNub.LowerLimit )}] [float] NOT NULL,
                [{nameof( ToleranceNub.UpperLimit )}] [float] NOT NULL,
                [{nameof( ToleranceNub.Caption )}] [nvarchar](10) NOT NULL,
                [{nameof( ToleranceNub.CompoundCaption )}] [nvarchar](13) NOT NULL); 
            CREATE UNIQUE INDEX [{isr.Dapper.Entities.ToleranceBuilder.CaptionIndexName}] ON [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([{nameof( ToleranceNub.Caption )}]); 
            CREATE TRIGGER [{isr.Dapper.Entities.ToleranceBuilder.TableName}_Tr] AFTER UPDATE OF [{nameof( ToleranceNub.ToleranceCode )}], [{nameof( ToleranceNub.Caption )}] ON [{isr.Dapper.Entities.ToleranceBuilder.TableName}]
            BEGIN
                UPDATE [{isr.Dapper.Entities.ToleranceBuilder.TableName}]
                SET    [{nameof( ToleranceNub.CompoundCaption )}] = (([{nameof( ToleranceNub.ToleranceCode )}]+': ')+[{nameof( ToleranceNub.Caption )}])
                WHERE  [rowId] = [NEW].[RowId];
            END;" );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.ToleranceBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Entities.ToleranceBuilder.TableName}](
                [{nameof( ToleranceNub.ToleranceCode )}] [nchar](1) NOT NULL,
                [{nameof( ToleranceNub.LowerLimit )}] [float] NOT NULL,
                [{nameof( ToleranceNub.UpperLimit )}] [float] NOT NULL,
                [{nameof( ToleranceNub.Caption )}] [nvarchar](10) NOT NULL,
                [{nameof( ToleranceNub.CompoundCaption )}]  AS (([{nameof( ToleranceNub.ToleranceCode )}]+': ')+[{nameof( ToleranceNub.Caption )}]) PERSISTED NOT NULL,
             CONSTRAINT [PK_{isr.Dapper.Entities.ToleranceBuilder.TableName}] PRIMARY KEY CLUSTERED 
            ([{nameof( ToleranceNub.ToleranceCode )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END;
            CREATE UNIQUE INDEX [{isr.Dapper.Entities.ToleranceBuilder.CaptionIndexName}] ON [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([{nameof( ToleranceNub.Caption )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>   Implements the Tolerance table <see cref="ITolerance">interface</see>. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-19 </para>
    /// </remarks>
    [Table( "Tolerance" )]
    public class ToleranceNub : EntityNubBase<ITolerance>, ITolerance
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public ToleranceNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ITolerance CreateNew()
        {
            return new ToleranceNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ITolerance CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( ITolerance value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( ITolerance source, ITolerance destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Caption = source.Caption;
            destination.LowerLimit = source.LowerLimit;
            destination.UpperLimit = source.UpperLimit;
            destination.ToleranceCode = source.ToleranceCode;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as ITolerance );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( ITolerance other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( ITolerance left, ITolerance right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && string.Equals( left.Caption, right.Caption );
                result = result && Equals( left.LowerLimit, right.LowerLimit );
                result = result && Equals( left.UpperLimit, right.UpperLimit );
                result = result && string.Equals( left.ToleranceCode, right.ToleranceCode );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.Caption, this.LowerLimit, this.UpperLimit, this.ToleranceCode ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Tolerance. </summary>
        /// <value> Identifies the Tolerance. </value>
        [ExplicitKey]
        public string ToleranceCode { get; set; }

        /// <summary>   Gets or sets the lower limit. </summary>
        /// <value> The lower limit. </value>
        public double LowerLimit { get; set; }

        /// <summary>   Gets or sets the upper limit. </summary>
        /// <value> The upper limit. </value>
        public double UpperLimit { get; set; }

        /// <summary>   Gets or sets the caption. </summary>
        /// <value> The caption. </value>
        public string Caption { get; set; }

        /// <summary>   Gets or sets the timestamp. </summary>
        /// <value> The timestamp. </value>
        [Computed]
        public string CompoundCaption { get; set; }

        #endregion

    }

    /// <summary>   The Tolerance Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-19 </para>
    /// </remarks>
    public class ToleranceEntity : EntityBase<ITolerance, ToleranceNub>, ITolerance
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public ToleranceEntity() : this( new ToleranceNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ToleranceEntity( ITolerance value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ToleranceEntity( ITolerance cache, ITolerance store ) : base( new ToleranceNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ToleranceEntity( ToleranceEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ToleranceBuilder.TableName, nameof( ITolerance ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ITolerance CreateNew()
        {
            return new ToleranceNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ITolerance CreateCopy()
        {
            var destination = this.CreateNew();
            ToleranceNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( ITolerance value )
        {
            ToleranceNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the Meter Model interface. </param>
        public override void UpdateCache( ITolerance value )
        {
            // first make the copy to notify of any property change.
            ToleranceNub.Copy( value, this );

            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          the Meter Model table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, string key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<ITolerance>( key ) : connection.Get<ToleranceNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.ToleranceCode );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Caption );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-04-28. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="caption">      The tolerance caption. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string caption )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, caption ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Overrides in order to fetch the stored
        /// entity.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ITolerance entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Caption ) )
            {
                // update the existing record from the specified entity.
                entity.ToleranceCode = this.ToleranceCode;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, string key )
        {
            return connection.Delete( new ToleranceNub() { ToleranceCode = key } );
        }

        /// <summary>   Updates the compound caption. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="caption">      The tolerance caption. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool UpdateCompoundCaption( System.Data.IDbConnection connection, string caption )
        {
            ToleranceBuilder.UpdateCompoundCaption( connection, this.ToleranceCode, caption );
            return this.FetchUsingKey( connection );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the tolerance entities. </summary>
        /// <value> The Tolerance entities. </value>
        public static IEnumerable<ToleranceEntity> Tolerances { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ToleranceEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ITolerance>() ) : Populate( connection.GetAll<ToleranceNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            Tolerances = FetchAllEntities( connection, this.UsingNativeTracking );
            return Tolerances?.Any() == true ? Tolerances.Count() : 0;
        }

        /// <summary>   Populates a list of tolerance entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the Meter Model nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ToleranceEntity> Populate( IEnumerable<ToleranceNub> nubs )
        {
            var l = new List<ToleranceEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ToleranceNub nub in nubs )
                    l.Add( new ToleranceEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of tolerance entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the Meter Model interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ToleranceEntity> Populate( IEnumerable<ITolerance> interfaces )
        {
            var l = new List<ToleranceEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ToleranceNub();
                foreach ( ITolerance iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ToleranceEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<string, ToleranceEntity> _EntityLookupDictionary;

        /// <summary>   The tolerance entity lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of Integer, ToleranceEntity) </returns>
        public static IDictionary<string, ToleranceEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<string, ToleranceEntity>();
                foreach ( ToleranceEntity entity in Tolerances )
                    _EntityLookupDictionary.Add( entity.ToleranceCode, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Dictionary of key lookups. </summary>
        private static IDictionary<string, string> _KeyLookupDictionary;

        /// <summary>   The key lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of String, Integer) </returns>
        public static IDictionary<string, string> KeyLookupDictionary()
        {
            if ( !(_KeyLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _KeyLookupDictionary = new Dictionary<string, string>();
                foreach ( ToleranceEntity entity in Tolerances )
                    _KeyLookupDictionary.Add( entity.Caption, entity.ToleranceCode );
            }

            return _KeyLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (Tolerances?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) &&
                   (_KeyLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="caption">      The tolerance caption. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string caption )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ToleranceBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ToleranceNub.Caption )} = @Caption", new { caption } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="caption">      The tolerance caption number. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ToleranceNub> FetchNubs( System.Data.IDbConnection connection, string caption )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ToleranceBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ToleranceNub.Caption )} = @Caption", new { caption } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ToleranceNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Tolerance exists. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="caption">      The tolerance caption. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string caption )
        {
            return 1 == CountEntities( connection, caption );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Tolerance. </summary>
        /// <value> Identifies the Tolerance. </value>
        public string ToleranceCode
        {
            get => this.ICache.ToleranceCode;

            set {
                if ( !object.Equals( this.ToleranceCode, value ) )
                {
                    this.ICache.ToleranceCode = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the lower limit. </summary>
        /// <value> The lower limit. </value>
        public double LowerLimit
        {
            get => this.ICache.LowerLimit;

            set {
                if ( !object.Equals( this.LowerLimit, value ) )
                {
                    this.ICache.LowerLimit = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the upper limit. </summary>
        /// <value> The upper limit. </value>
        public double UpperLimit
        {
            get => this.ICache.UpperLimit;

            set {
                if ( this.UpperLimit != value )
                {
                    this.ICache.UpperLimit = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the caption. </summary>
        /// <value> The caption. </value>
        public string Caption
        {
            get => this.ICache.Caption;

            set {
                if ( !string.Equals( this.Caption, value ) )
                {
                    this.ICache.Caption = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ToleranceEntity.Identity ) );
                }
            }
        }

        /// <summary>   Gets or sets the timestamp. </summary>
        /// <value> The timestamp. </value>
        public string CompoundCaption
        {
            get => this.ICache.CompoundCaption;

            set {
                if ( !string.Equals( this.CompoundCaption, value ) )
                {
                    this.ICache.CompoundCaption = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " CUSTOM FIELDS "

        /// <summary>   Gets the identity. </summary>
        /// <value> The identity. </value>
        public string Identity => $"{this.Caption}";

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="toleranceCode">    The entity unique key. </param>
        /// <returns>   The (Success As Boolean, Details As String, Entity As ToleranceEntity) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, ToleranceEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, string toleranceCode )
        {
            string activity = string.Empty;
            var entity = new ToleranceEntity();
            bool success = true;
            string details = string.Empty;
            try
            {
                activity = $"Fetching {nameof( ToleranceEntity )} by {nameof( ToleranceNub.ToleranceCode )} of {toleranceCode}";
                if ( !entity.FetchUsingKey( connection, toleranceCode ) )
                {
                    details = $"Failed {activity}";
                    success = false;
                }
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                details = $"Exception {activity};. {ex}";

                success = false;
            }

            return (success, details, entity);
        }

        /// <summary>   Try fetch using unique index. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="toleranceCaption"> The tolerance caption. </param>
        /// <returns>   The (Success As Boolean, Details As String, Entity As ToleranceEntity) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, ToleranceEntity Entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, string toleranceCaption )
        {
            string activity = string.Empty;
            var entity = new ToleranceEntity();
            bool success = true;
            string details = string.Empty;
            try
            {
                activity = $"Fetching {nameof( ToleranceEntity )} by {nameof( ToleranceNub.Caption )} of {toleranceCaption}";
                if ( !entity.FetchUsingUniqueIndex( connection, toleranceCaption ) )
                {
                    details = $"Failed {activity}";
                    success = false;
                }
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                details = $"Exception {activity};. {ex}";

                success = false;
            }

            return (success, details, entity);
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entities As IEnumerable(Of ToleranceEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, IEnumerable<ToleranceEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new ToleranceEntity();
                activity = $"fetching all {nameof( ToleranceEntity )}s";
                int elementCount = entity.FetchAllEntities( connection );
                if ( elementCount != EntityLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( ToleranceEntity.EntityLookupDictionary )} count must equal {nameof( Tolerances )} count " );
                }
                else if ( elementCount != KeyLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( ToleranceEntity.KeyLookupDictionary )} count must equal {nameof( Tolerances )} count " );
                }

                return (true, string.Empty, Tolerances);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<ToleranceEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Bucket Bin builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class BucketBinBuilder : NominalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( BucketBinNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>
        /// Inserts or ignore the records as described by the <see cref="BucketBin"/> enumeration type.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public override int InsertIgnoreDefaultRecords( System.Data.IDbConnection connection )
        {
            return this.InsertIgnore( connection, typeof( BucketBin ), Array.Empty<int>() );
        }

        #region " SINGLETON "

        private static readonly Lazy<BucketBinBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static BucketBinBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the BucketBin table based on the <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "BucketBin" )]
    public class BucketBinNub : NominalNub, INominal
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public BucketBinNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new BucketBinNub();
        }
    }

    /// <summary>   The BucketBin Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class BucketBinEntity : EntityBase<INominal, BucketBinNub>, INominal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public BucketBinEntity() : this( new BucketBinNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public BucketBinEntity( INominal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public BucketBinEntity( INominal cache, INominal store ) : base( new BucketBinNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public BucketBinEntity( BucketBinEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.BucketBinBuilder.TableName, nameof( INominal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new BucketBinNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override INominal CreateCopy()
        {
            var destination = this.CreateNew();
            NominalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( INominal value )
        {
            NominalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The BucketBin interface. </param>
        public override void UpdateCache( INominal value )
        {
            // first make the copy to notify of any property change.
            NominalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The BucketBin table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<INominal>( key ) : connection.Get<BucketBinNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Label );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Bucket Bin label. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string label )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, label ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, INominal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Label ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new BucketBinNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the BucketBin entities. </summary>
        /// <value> The BucketBin entities. </value>
        public static IEnumerable<BucketBinEntity> BucketBins { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<BucketBinEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<INominal>() ) : Populate( connection.GetAll<BucketBinNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            BucketBins = FetchAllEntities( connection, this.UsingNativeTracking );
            return BucketBins?.Any() == true ? BucketBins.Count() : 0;
        }

        /// <summary>   Populates a list of BucketBin entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The BucketBin nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<BucketBinEntity> Populate( IEnumerable<BucketBinNub> nubs )
        {
            var l = new List<BucketBinEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( BucketBinNub nub in nubs )
                    l.Add( new BucketBinEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of BucketBin entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The BucketBin interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<BucketBinEntity> Populate( IEnumerable<INominal> interfaces )
        {
            var l = new List<BucketBinEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new BucketBinNub();
                foreach ( INominal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new BucketBinEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, BucketBinEntity> _EntityLookupDictionary;

        /// <summary>   The Bucket Bin Lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of Integer, BucketBinEntity) </returns>
        public static IDictionary<int, BucketBinEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, BucketBinEntity>();
                foreach ( BucketBinEntity entity in BucketBins )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Dictionary of key lookups. </summary>
        private static IDictionary<string, int> _KeyLookupDictionary;

        /// <summary>   The key lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of String, Integer) </returns>
        public static IDictionary<string, int> KeyLookupDictionary()
        {
            if ( !(_KeyLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _KeyLookupDictionary = new Dictionary<string, int>();
                foreach ( BucketBinEntity entity in BucketBins )
                    _KeyLookupDictionary.Add( entity.Label, entity.Id );
            }

            return _KeyLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (BucketBins?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) &&
                   (_KeyLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Bucket Bin label. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.BucketBinBuilder.TableName}] WHERE {nameof( BucketBinNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Bucket Bin label. </param>
        /// <returns>   An IBucketBin . </returns>
        public static IEnumerable<BucketBinNub> FetchNubs( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.BucketBinBuilder.TableName}] WHERE {nameof( BucketBinNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<BucketBinNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the BucketBin exists. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Bucket Bin label. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string label )
        {
            return 1 == CountEntities( connection, label );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Bucket Bin. </summary>
        /// <value> Identifies the Bucket Bin. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( ( object ) this.Id, ( object ) value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( BucketBinEntity.BucketBin ) );
                }
            }
        }

        /// <summary>   Gets or sets the BucketBin. </summary>
        /// <value> The BucketBin. </value>
        public BucketBin BucketBin
        {
            get => ( BucketBin ) this.Id;

            set => this.Id = ( int ) value;
        }

        /// <summary>   Gets or sets the Bucket Bin label. </summary>
        /// <value> The Bucket Bin label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the description of the Bucket Bin. </summary>
        /// <value> The Bucket Bin description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="bucketBinId">  The entity unique key. </param>
        /// <returns>   The (Success As Boolean, Details As String, Entity As BucketBinEntity) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, BucketBinEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int bucketBinId )
        {
            string activity = string.Empty;
            var entity = new BucketBinEntity();
            bool success = true;
            string details = string.Empty;
            try
            {
                activity = $"Fetching {nameof( BucketBinEntity )} by {nameof( BucketBinNub.Id )} of {bucketBinId}";
                if ( !entity.FetchUsingKey( connection, bucketBinId ) )
                {
                    details = $"Failed {activity}";
                    success = false;
                }
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                details = $"Exception {activity};. {ex}";
                success = false;
            }

            return (success, details, entity);
        }

        /// <summary>   Try fetch using unique index. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="bucketBinLabel">   The Bucket bin label. </param>
        /// <returns>   The (Success As Boolean, Details As String, Entity As BucketBinEntity) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, BucketBinEntity Entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, string bucketBinLabel )
        {
            string activity = string.Empty;
            var entity = new BucketBinEntity();
            bool success = true;
            string details = string.Empty;
            try
            {
                activity = $"Fetching {nameof( BucketBinEntity )} by {nameof( BucketBinNub.Label )} of {bucketBinLabel}";
                if ( !entity.FetchUsingUniqueIndex( connection, bucketBinLabel ) )
                {
                    details = $"Failed {activity}";
                    success = false;
                }
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                details = $"Exception {activity};. {ex}";
                success = false;
            }

            return (success, details, entity);
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entities As IEnumerable(Of BucketBinEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, IEnumerable<BucketBinEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new BucketBinEntity();
                activity = $"fetching all {nameof( BucketBinEntity )}'s";
                int elementCount = entity.FetchAllEntities( connection );
                if ( elementCount != EntityLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( BucketBinEntity.EntityLookupDictionary )} count must equal {nameof( BucketBins )} count " );
                }
                else if ( elementCount != KeyLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( BucketBinEntity.KeyLookupDictionary )} count must equal {nameof( BucketBins )} count " );
                }

                return (true, string.Empty, BucketBins);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<BucketBinEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

        #region " DEFAULT BUCKET BINS "

        /// <summary>   Converts a <see cref="BucketBin"/> to a bucket number. </summary>
        /// <remarks>   David, 2020-07-04. </remarks>
        /// <param name="bucketBin">    The bucket bin. </param>
        /// <returns>   An Integer. </returns>
        public static int ToBucketNumber( BucketBin bucketBin )
        {
            return ( int ) bucketBin + 1;
        }

        /// <summary>   The bucket number bin mapper. </summary>
        private static IDictionary<int, BucketBin> _BucketNumberBinMapper;

        /// <summary>   Gets or sets the bucket mapper. </summary>
        /// <value> The bucket mapper. </value>
        public static IDictionary<int, BucketBin> BucketNumberBinMapper
        {
            get {
                if ( _BucketNumberBinMapper is null )
                {
                    _BucketNumberBinMapper = new Dictionary<int, BucketBin>() { { ToBucketNumber( BucketBin.Good ), BucketBin.Good }, { ToBucketNumber( BucketBin.Bad ), BucketBin.Bad }, { ToBucketNumber( BucketBin.Overflow ), BucketBin.Overflow }, { ToBucketNumber( BucketBin.Unknown ), BucketBin.Unknown } };
                }

                return _BucketNumberBinMapper;
            }

            set => _BucketNumberBinMapper = value;
        }

        /// <summary>   The product sort bin mapper. </summary>
        private static IDictionary<int, BucketBin> _ProductSortBinMapper;

        /// <summary>   Gets or sets the mapper from product sort to Bucket Bin. </summary>
        /// <value> The bucket mapper. </value>
        public static IDictionary<int, BucketBin> ProductSortBinMapper
        {
            get {
                if ( _ProductSortBinMapper is null )
                {
                    _ProductSortBinMapper = new Dictionary<int, BucketBin>() { { (int)ProductSort.CompoundResistorGuardFailed, BucketBin.Bad },
                                                                               { (int)ProductSort.ComputedValueGuardFailed, BucketBin.Bad },
                                                                               { (int)ProductSort.Good, BucketBin.Good },
                                                                               { (int)ProductSort.Nameless, BucketBin.Unknown },
                                                                               { (int)ProductSort.ReadingFailed, BucketBin.Unknown },
                                                                               { (int)ProductSort.ReadingOverflow, BucketBin.Overflow },
                                                                               { (int)ProductSort.ResistorGuardFailed, BucketBin.Bad } };
                }

                return _ProductSortBinMapper;
            }

            set => _ProductSortBinMapper = value;
        }

        /// <summary>   Converts a <see cref="BucketBin"/> to a bucket number. </summary>
        /// <remarks>   David, 2020-07-07. </remarks>
        /// <param name="productSort">  The product sort. </param>
        /// <returns>   An Integer. </returns>
        public static int ToBucketNumber( ProductSort productSort )
        {
            return ToBucketNumber( ProductSortBinMapper[( int ) productSort] );
        }

        /// <summary>   Converts a value to a bucket bin. </summary>
        /// <remarks>   David, 2020-07-07. </remarks>
        /// <param name="value">    The BucketBin interface. </param>
        /// <returns>   Value as a BucketBin. </returns>
        public static BucketBin ToBucketBin( int value )
        {
            return Enum.IsDefined( typeof( BucketBin ), value ) ? ( BucketBin ) value : BucketBin.Unknown;
        }

        /// <summary>   Query if 'bucketBin' is present. </summary>
        /// <remarks>   David, 2020-07-08. </remarks>
        /// <param name="bucketBin">    The bucket bin. </param>
        /// <returns>   True if present, false if not. </returns>
        public static bool IsPresent( BucketBin bucketBin )
        {
            return !(bucketBin == BucketBin.Overflow || bucketBin == BucketBin.Unknown);
        }

        #endregion

        #region " DEFAULT BUCKET BIN COLOR "

        /// <summary>   The back color mapper. </summary>
        private static IDictionary<BucketBin, int> _BackColorMapper;

        /// <summary>   Gets or sets the bucket background color mapper. </summary>
        /// <value> The bucket background color mapper. </value>
        public static IDictionary<BucketBin, int> BackColorMapper
        {
            get {
                if ( _BackColorMapper is null )
                {
                    _BackColorMapper = new Dictionary<BucketBin, int>() { { BucketBin.Good, Color.SeaGreen.ToArgb() }, { BucketBin.Bad, Color.Crimson.ToArgb() }, { BucketBin.Overflow, Color.OrangeRed.ToArgb() }, { BucketBin.Unknown, Color.Silver.ToArgb() } };
                }

                return _BackColorMapper;
            }

            set => _BackColorMapper = value;
        }

        #endregion

        #region " DEFAULT BUCKET BIN DIGITAL OUTPUT "

        /// <summary>   The digital output mapper. </summary>
        private static IDictionary<BucketBin, int> _DigitalOutputMapper;

        /// <summary>   Gets or sets the bucket digital output mapper. </summary>
        /// <value> The bucket digital output mapper. </value>
        public static IDictionary<BucketBin, int> DigitalOutputMapper
        {
            get {
                if ( _DigitalOutputMapper is null )
                {
                    _DigitalOutputMapper = new Dictionary<BucketBin, int>() { { BucketBin.Good, 1 }, { BucketBin.Bad, 2 }, { BucketBin.Overflow, 4 }, { BucketBin.Unknown, 4 } };
                }

                return _DigitalOutputMapper;
            }

            set => _DigitalOutputMapper = value;
        }

        #endregion

        #region " DEFAULT BUCKET CONTINUOUS FAILURE COUNT LIMIT "

        /// <summary>   The continuous failure count limit mapper. </summary>
        private static IDictionary<BucketBin, int> _ContinuousFailureCountLimitMapper;

        /// <summary>   Gets or sets the continuous failure count limit mapper. </summary>
        /// <value> The bucket continuous failure count limit mapper. </value>
        public static IDictionary<BucketBin, int> ContinuousFailureCountLimitMapper
        {
            get {
                if ( _ContinuousFailureCountLimitMapper is null )
                {
                    _ContinuousFailureCountLimitMapper = new Dictionary<BucketBin, int>() { { BucketBin.Good, 0 }, { BucketBin.Bad, 10 }, { BucketBin.Overflow, 10 }, { BucketBin.Unknown, 10 } };
                }

                return _ContinuousFailureCountLimitMapper;
            }

            set => _ContinuousFailureCountLimitMapper = value;
        }

        #endregion

    }

    /// <summary>   Values that represent bucket bins. </summary>
    /// <remarks>   David, 2020-07-04. </remarks>
    public enum BucketBin
    {

        /// <summary>   An enum constant representing the good option. </summary>
        [Description( "Good" )]
        Good = 0,

        /// <summary>   An enum constant representing the bad option. </summary>
        [Description( "Bad" )]
        Bad = 1,

        /// <summary>   An enum constant representing the overflow option. </summary>
        [Description( "Overflow" )]
        Overflow = 2,

        /// <summary>   An enum constant representing the unknown option. </summary>
        [Description( "Unknown" )]
        Unknown = 3
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Meter Model builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class MeterModelBuilder : NominalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( MeterModelNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>
        /// Inserts or ignores the records described by the <see cref="MeterModel"/> enumeration
        /// type.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public override int InsertIgnoreDefaultRecords( System.Data.IDbConnection connection )
        {
            return this.InsertIgnore( connection, typeof( MeterModel ), new int[] { ( int ) MeterModel.None } );
        }

        #region " SINGLETON "

        private static readonly Lazy<MeterModelBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static MeterModelBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Meter Model table based on the <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "MeterModel" )]
    public class MeterModelNub : NominalNub, INominal
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public MeterModelNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new MeterModelNub();
        }
    }

    /// <summary>
    /// The Meter Model Entity implements the <see cref="INominal"/> interface.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class MeterModelEntity : EntityBase<INominal, MeterModelNub>, INominal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public MeterModelEntity() : this( new MeterModelNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public MeterModelEntity( INominal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public MeterModelEntity( INominal cache, INominal store ) : base( new MeterModelNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public MeterModelEntity( MeterModelEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.MeterModelBuilder.TableName, nameof( INominal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new MeterModelNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override INominal CreateCopy()
        {
            var destination = this.CreateNew();
            NominalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( INominal value )
        {
            NominalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Meter Model interface. </param>
        public override void UpdateCache( INominal value )
        {
            // first make the copy to notify of any property change.
            NominalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The Meter Model table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<INominal>( key ) : connection.Get<MeterModelNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Label );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Meter Model label. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string label )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, label ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, INominal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Label ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new MeterModelNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the Meter Model entities. </summary>
        /// <value> The Meter Model entities. </value>
        public static IEnumerable<MeterModelEntity> MeterModels { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<MeterModelEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<INominal>() ) : Populate( connection.GetAll<MeterModelNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            MeterModels = FetchAllEntities( connection, this.UsingNativeTracking );
            return MeterModels?.Any() == true ? MeterModels.Count() : 0;
        }

        /// <summary>   Populates a list of Meter Model entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The Meter Model nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<MeterModelEntity> Populate( IEnumerable<MeterModelNub> nubs )
        {
            var l = new List<MeterModelEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( MeterModelNub nub in nubs )
                    l.Add( new MeterModelEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Meter Model entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The Meter Model interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<MeterModelEntity> Populate( IEnumerable<INominal> interfaces )
        {
            var l = new List<MeterModelEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new MeterModelNub();
                foreach ( INominal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new MeterModelEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, MeterModelEntity> _EntityLookupDictionary;

        /// <summary>   The entity lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of Integer, MeterModelEntity) </returns>
        public static IDictionary<int, MeterModelEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, MeterModelEntity>();
                foreach ( MeterModelEntity entity in MeterModels )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Dictionary of key lookups. </summary>
        private static IDictionary<string, int> _KeyLookupDictionary;

        /// <summary>   The key lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of String, Integer) </returns>
        public static IDictionary<string, int> KeyLookupDictionary()
        {
            if ( !(_KeyLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _KeyLookupDictionary = new Dictionary<string, int>();
                foreach ( MeterModelEntity entity in MeterModels )
                    _KeyLookupDictionary.Add( entity.Label, entity.Id );
            }

            return _KeyLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (MeterModels?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) &&
                   (_KeyLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Meter Model label. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string label )
        {
            // Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{MeterModelNub.TableName}]
            // WHERE {NameOf(MeterModelNub.PlatformName)} = @PlatformName", new {PlatformName = PlatformName})
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.MeterModelBuilder.TableName}] WHERE {nameof( MeterModelNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Meter Model label. </param>
        /// <returns>   An IMeterModel . </returns>
        public static IEnumerable<MeterModelNub> FetchNubs( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.MeterModelBuilder.TableName}] WHERE {nameof( MeterModelNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<MeterModelNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Meter Model exists. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Meter Model label. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string label )
        {
            return 1 == CountEntities( connection, label );
        }

        #endregion

        #region " FIELDS "

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterModelEntity"/> Model.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterModelEntity"/> Model. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( ( object ) this.Id, ( object ) value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( MeterModelEntity.MeterModel ) );
                }
            }
        }

        /// <summary>   Gets or sets the Meter Model. </summary>
        /// <value> the Meter Model. </value>
        public MeterModel MeterModel
        {
            get => ( MeterModel ) this.Id;

            set => this.Id = ( int ) value;
        }

        /// <summary>   Gets or sets the Meter Model label. </summary>
        /// <value> The Meter Model label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the description of the Meter Model. </summary>
        /// <value> The Meter Model description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " DERIVED PROPERTIES "

        private MeterCapabilities _Capabilities;

        /// <summary>   Gets the capabilities. </summary>
        /// <value> The capabilities. </value>
        public MeterCapabilities Capabilities
        {
            get {
                if ( this._Capabilities == MeterCapabilities.None )
                {
                    switch ( this.MeterModel )
                    {
                        case MeterModel.None:
                            break;
                        case MeterModel.K7510:
                            this._Capabilities = MeterCapabilities.Resistance | MeterCapabilities.DcCurrent | MeterCapabilities.DcVoltage;
                            break;
                        case MeterModel.K2002:
                            this._Capabilities = MeterCapabilities.Resistance | MeterCapabilities.DcCurrent | MeterCapabilities.DcVoltage;
                            break;
                        case MeterModel.CLT10:
                            this._Capabilities = MeterCapabilities.ThirdHarmonicsIndex;
                            break;
                        default:
                            break;
                    }
                }

                return this._Capabilities;
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> The entity unique key. </param>
        /// <returns>   A <see cref="MeterModelEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, MeterModelEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int meterModelId )
        {
            string activity = string.Empty;
            var entity = new MeterModelEntity();
            try
            {
                activity = $"Fetching {nameof( MeterModelEntity )} by {nameof( MeterModelNub.Id )} of {meterModelId}";
                return entity.FetchUsingKey( connection, meterModelId ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Try fetch using unique index. </summary>
        /// <remarks>   David, 2020-04-28. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="meterModelLabel">  The Meter Model label. </param>
        /// <returns>   The (Success As Boolean, Details As String, Entity As MeterModelEntity) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, MeterModelEntity Entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, string meterModelLabel )
        {
            string activity = string.Empty;
            var entity = new MeterModelEntity();
            try
            {
                activity = $"Fetching {nameof( MeterModelEntity )} by {nameof( MeterModelNub.Label )} of {meterModelLabel}";
                return entity.FetchUsingUniqueIndex( connection, meterModelLabel ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, details As String, Entities As IEnumerable(Of MeterModelEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string details, IEnumerable<MeterModelEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new MeterModelEntity();
                activity = $"fetching all {nameof( MeterModelEntity )}'s";
                int elementCount = entity.FetchAllEntities( connection );
                if ( elementCount != EntityLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( MeterModelEntity.EntityLookupDictionary )} count must equal {nameof( MeterModels )} count " );
                }
                else if ( elementCount != KeyLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( MeterModelEntity.KeyLookupDictionary )} count must equal {nameof( MeterModels )} count " );
                }

                return (true, string.Empty, MeterModels);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<MeterModelEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }

    /// <summary>   Values that represent Meter Models. </summary>
    /// <remarks>   David, 2020-10-02. </remarks>
    public enum MeterModel
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary> An enum constant representing the 7510 option. </summary>
        [System.ComponentModel.Description( "Keithley 7510" )]
        K7510 = 1,

        /// <summary> An enum constant representing the 2002 option. </summary>
        [System.ComponentModel.Description( "Keithley 2002" )]
        K2002 = 2,

        /// <summary> An enum constant representing the Danbridge CLT10 option. </summary>
        [System.ComponentModel.Description( "Danbridge CLT10" )]
        CLT10 = 3
    }


    /// <summary>   A bit-field of flags for specifying meter capabilities. </summary>
    /// <remarks>   David, 2021-04-22. </remarks>
    [Flags]
    public enum MeterCapabilities
    {

        /// <summary> A binary constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary> A binary constant representing the Resistance option. </summary>
        [System.ComponentModel.Description( "Resistance" )]
        Resistance = 1,

        /// <summary> A binary constant representing the DC Voltage option. </summary>
        [System.ComponentModel.Description( "DC Voltage" )]
        DcVoltage = 2,

        /// <summary> A binary constant representing the DC Current option. </summary>
        [System.ComponentModel.Description( "DC Current" )]
        DcCurrent = 4,

        /// <summary>   A binary constant representing the third harmonics index flag. </summary>
        [System.ComponentModel.Description( "Third Harmonics Index" )]
        ThirdHarmonicsIndex = 8
    }

}

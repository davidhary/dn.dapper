using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   A part entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class PartEntity
    {

        /// <summary>   Gets or sets the <see cref="MeterElementNomTypeNomTraitCollection"/>. </summary>
        /// <value> The <see cref="MeterElementNomTypeNomTraitCollection"/>. </value>
        public MeterElementNomTypeNomTraitCollection NomTraits { get; private set; }

        /// <summary>   Fetches the <see cref="MeterElementNomTypeNomTraitCollection"/>. </summary>
        /// <remarks>   David, 2021-05-20. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeters">   The platform meters. </param>
        /// <returns>   The <see cref="MeterElementNomTypeNomTraitCollection"/>. </returns>
        public MeterElementNomTypeNomTraitCollection FetchNomTraits( System.Data.IDbConnection connection, IEnumerable<PlatformMeter> platformMeters )
        {
            if ( !NomTypeEntity.IsEnumerated() )
                _ = NomTypeEntity.TryFetchAll( connection );
            if ( !MeterEntity.IsEnumerated() )
                _ = MeterEntity.TryFetchAll( connection );
            this.NomTraits = new MeterElementNomTypeNomTraitCollection();
            foreach ( ElementEntity element in this.Elements )
            {
                element.NomTraitsCollection.Clear();
            }
            foreach ( var platformMeter in platformMeters )
            {
                platformMeter.ClearElements();
            }
#if true
            foreach ( var platformMeter in platformMeters )
            {
                foreach ( ElementEntity element in this.Elements )
                {
                    this.FetchNomTraits( connection, element, platformMeter );
                }
            }
#else
            foreach ( ElementEntity element in this.Elements )
            {
                foreach ( var platformMeter in platformMeters )
                {
                    this.FetchNomTraits( connection, element, platformMeter );
                }
            }
#endif
            return this.NomTraits;
        }

        /// <summary>   Fetches the <see cref="MeterElementNomTypeNomTraitCollection"/>. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="element">          The element. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        private void FetchNomTraits( System.Data.IDbConnection connection, ElementEntity element, PlatformMeter platformMeter )
        {
            if ( 0 == element.FetchNoms( connection, this, platformMeter ) )
            {
                if ( platformMeter.ElementLabels.Contains( element.ElementLabel ) )
                {
                    platformMeter.AddElement( element );
                    // nominal traits are added when defining the part traits for each
                    // part specification element.
                }
            }
            else
            {
                // add the element to the platform meter if nominal traits were added
                platformMeter.AddElement( element );
                foreach ( PartNomEntityCollection nominals in element.NomTraitsCollection )
                {
                    if ( nominals.MeterId == platformMeter.MeterId )
                        this.NomTraits.Add( nominals );
                }
            }
        }

    }

    /// <summary>
    /// Keyed collection of <see cref="PartNomEntityCollection"/> keyed by
    /// <see cref="MeterElementNomTypeSelector"/> of <see cref="Dapper.Entities.ElementEntity"/> and
    /// <see cref="Dapper.Entities.MeterEntity"/> and
    /// <see cref="Dapper.Entities.NomTypeEntity"/>.
    /// </summary>
    /// <remarks>   David, 2020-06-16. </remarks>
    public class MeterElementNomTypeNomTraitCollection : KeyedCollection<MeterElementNomTypeSelector, PartNomEntityCollection>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        public MeterElementNomTypeNomTraitCollection() : base()
        {
            this.BindingList = new Std.BindingLists.InvokingBindingList<NomTrait>();
            this.MeterBindingListDictionary = new Dictionary<int, Std.BindingLists.InvokingBindingList<NomTrait>>();
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-06-22. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override MeterElementNomTypeSelector GetKeyForItem( PartNomEntityCollection item )
        {
            return new MeterElementNomTypeSelector( item );
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        [CLSCompliant( false )]
        public void Upsert( TransactedConnection connection )
        {
            foreach ( PartNomEntityCollection nomTraitEntityCollection in this )
                _ = nomTraitEntityCollection.Upsert( connection );
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        public void Upsert( System.Data.IDbConnection connection )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.Upsert( new TransactedConnection( connection, transaction ) );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.Upsert( transactedConnection );
            }
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new void Add( PartNomEntityCollection item )
        {
            base.Add( item );
            this.BindingList.Add( item.NomTrait );
            if ( !this.MeterBindingListDictionary.ContainsKey( item.MeterId ) )
            {
                this.MeterBindingListDictionary.Add( item.MeterId, new Std.BindingLists.InvokingBindingList<NomTrait>() );
            }

            this.MeterBindingListDictionary[item.MeterId].Add( item.NomTrait );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        public new void Clear()
        {
            base.Clear();
            this.BindingList.Clear();
            foreach ( BindingList<NomTrait> bindingList in this.MeterBindingListDictionary.Values )
                bindingList.Clear();
        }

        /// <summary>   Gets or sets a list of Nominal Trait bindings. </summary>
        /// <value> A list of bindings. </value>
        public Std.BindingLists.InvokingBindingList<NomTrait> BindingList { get; private set; }

        /// <summary>   Gets or sets a dictionary of meter binding lists. </summary>
        /// <value> A dictionary of meter binding lists. </value>
        public IDictionary<int, Std.BindingLists.InvokingBindingList<NomTrait>> MeterBindingListDictionary { get; private set; }

        /// <summary>   Toggle binding list change events. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <param name="enabled">  True to enable, false to disable. </param>
        public void ToggleBindingListChangeEvents( bool enabled )
        {
            this.BindingList.RaiseListChangedEvents = enabled;
            foreach ( Std.BindingLists.InvokingBindingList<NomTrait> bl in this.MeterBindingListDictionary.Values )
            {
                bl.RaiseListChangedEvents = enabled;
                if ( enabled )
                    bl.ResetBindings();
            }
        }
    }
}

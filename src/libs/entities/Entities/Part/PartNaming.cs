using System.ComponentModel;
using System.Threading;
using System;

namespace isr.Dapper.Entities
{

    /// <summary>   The Part Naming class holing the Part Naming values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class PartNaming : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-04-26. </remarks>
        public PartNaming() : base()
        {
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter GetterSetter { get; set; }

        /// <summary>   Gets the naming value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Integer. </returns>
        public string Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the naming value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Integer. </returns>
        public string Setter( string value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value, name );
                this.NotifyPropertyChanged( name );
            }

            return value;
        }

        #endregion

        #region " NAMING ITEMS "

        /// <summary>   Gets or sets the part number. </summary>
        /// <value> The part number. </value>
        public string PartNumber
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the family code. </summary>
        /// <value> The family code. </value>
        public string FamilyCode
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the product number. </summary>
        /// <value> The product number. </value>
        public string ProductNumber
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the Product Case Code. </summary>
        /// <value> The Product Case Code. </value>
        public string ProductCaseCode
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the model number. </summary>
        /// <value> The model number. </value>
        public string ModelNumber
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the product number termination. </summary>
        /// <value> The product number termination. </value>
        public string ProductNumberTermination
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the special process specification. </summary>
        /// <value> The special process specification. </value>
        public string SpecialProcessSpecification
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the packaging code. </summary>
        /// <value> The packaging code. </value>
        public string PackagingCode
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the engineering suffix. </summary>
        /// <value> The engineering suffix. </value>
        public string EngineeringSuffix
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the unknown suffix. </summary>
        /// <value> The unknown suffix. </value>
        public string UnknownSuffix
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the tcr code. </summary>
        /// <value> The tcr code. </value>
        public string TcrCode
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the tolerance codes. </summary>
        /// <value> The tolerance codes. </value>
        public string ToleranceCodes
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the tolerance code. </summary>
        /// <value> The tolerance code. </value>
        public string ToleranceCode
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the tracking code. </summary>
        /// <value> The tracking code. </value>
        public string TrackingCode
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the element value 1. </summary>
        /// <value> The element value 1. </value>
        public string ElementValue1
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the element value 2. </summary>
        /// <value> The element value 2. </value>
        public string ElementValue2
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the critical element id (label). </summary>
        /// <value> The critical element label. </value>
        public string CriticalElementId
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the primary element id (label). </summary>
        /// <value> The primary element label. </value>
        public string PrimaryElementId
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        #endregion

    }
}

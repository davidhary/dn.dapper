using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Part Element Equation builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class PartElementEquationBuilder : TwoToManyLabelBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( PartElementEquationNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public override int LabelFieldSize { get; set; } = 255;

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = PartBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( PartNub.AutoId );

        /// <summary>   Gets or sets the name of the Secondary table. </summary>
        /// <value> The name of the Secondary table. </value>
        public override string SecondaryTableName { get; set; } = ElementBuilder.TableName;

        /// <summary>   Gets or sets the name of the Secondary table key. </summary>
        /// <value> The name of the Secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ElementNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( PartElementEquationEntity.PartAutoId );

        /// <summary>   Gets or sets the name of the Secondary identifier field. </summary>
        /// <value> The name of the Secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( PartElementEquationEntity.ElementAutoId );

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public override string LabelFieldName { get; set; } = nameof( PartElementEquationEntity.Equation );

        #region " SINGLETON "

        private static readonly Lazy<PartElementEquationBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static PartElementEquationBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="Dapper.Entities.PartEntity"/> Text table
    /// <see cref="ITwoToManyLabel">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "PartElementEquation" )]
    public class PartElementEquationNub : TwoToManyLabelNub, ITwoToManyLabel
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PartElementEquationNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ITwoToManyLabel CreateNew()
        {
            return new PartElementEquationNub();
        }
    }

    /// <summary>
    /// The <see cref="PartElementEquationEntity"/>. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class PartElementEquationEntity : EntityBase<ITwoToManyLabel, PartElementEquationNub>, ITwoToManyLabel
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PartElementEquationEntity() : this( new PartElementEquationNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PartElementEquationEntity( ITwoToManyLabel value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public PartElementEquationEntity( ITwoToManyLabel cache, ITwoToManyLabel store ) : base( new PartElementEquationNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PartElementEquationEntity( PartElementEquationEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.PartElementEquationBuilder.TableName, nameof( ITwoToManyLabel ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ITwoToManyLabel CreateNew()
        {
            return new PartElementEquationNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ITwoToManyLabel CreateCopy()
        {
            var destination = this.CreateNew();
            TwoToManyLabelNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="PartElementEquationEntity"/> interface value. </param>
        public override void CopyFrom( ITwoToManyLabel value )
        {
            TwoToManyLabelNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached Text, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.PartEntity"/>Text interface. </param>
        public override void UpdateCache( ITwoToManyLabel value )
        {
            // first make the copy to notify of any property change.
            TwoToManyLabelNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int partAutoId, int elementAutoId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PartElementEquationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartElementEquationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartElementEquationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<PartElementEquationNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int partAutoId, int elementAutoId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, partAutoId, elementAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ITwoToManyLabel entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int partAutoId, int elementAutoId )
        {
            return connection.Delete( new PartElementEquationNub() { PrimaryId = partAutoId, SecondaryId = elementAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.PartEntity"/>Text entities. </summary>
        /// <value> the <see cref="Dapper.Entities.PartEntity"/>Text entities. </value>
        public IEnumerable<PartElementEquationEntity> PartElementEquations { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartElementEquationEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ITwoToManyLabel>() ) : Populate( connection.GetAll<PartElementEquationNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.PartElementEquations = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( PartElementEquationEntity.PartElementEquations ) );
            return this.PartElementEquations?.Any() == true ? this.PartElementEquations.Count() : 0;
        }

        /// <summary>   Count Part Texts. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.PartElementEquationBuilder.TableName}] WHERE {nameof( PartElementEquationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartElementEquationEntity> FetchEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartElementEquationBuilder.TableName}] WHERE {nameof( PartElementEquationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<PartElementEquationNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Part Texts by Part Auto Id. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<PartElementEquationNub> FetchNubs( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PartElementEquationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartElementEquationEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PartElementEquationNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of Part Text entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.PartEntity"/>Text nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PartElementEquationEntity> Populate( IEnumerable<PartElementEquationNub> nubs )
        {
            var l = new List<PartElementEquationEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( PartElementEquationNub nub in nubs )
                    l.Add( new PartElementEquationEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Part Text entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.PartEntity"/>Text interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PartElementEquationEntity> Populate( IEnumerable<ITwoToManyLabel> interfaces )
        {
            var l = new List<PartElementEquationEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new PartElementEquationNub();
                foreach ( ITwoToManyLabel iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new PartElementEquationEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Part Texts by unique index; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PartElementEquationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartElementEquationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartElementEquationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches Part Texts by unique index; expected single or none. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<PartElementEquationNub> FetchEntities( System.Data.IDbConnection connection, int partAutoId, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PartElementEquationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartElementEquationNub.PrimaryId )} = @primaryId", new { primaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartElementEquationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PartElementEquationNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.PartEntity"/>Text exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int partAutoId, int elementAutoId )
        {
            return 1 == CountEntities( connection, partAutoId, elementAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count Texts associated with this part. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of Texts. </returns>
        public int CountPartElementEquations( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PartElementEquationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartElementEquationNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Part Text Texts by Part auto id;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-07-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<PartElementEquationNub> FetchPartElementEquations( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PartElementEquationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartElementEquationNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PartElementEquationNub>( selector.RawSql, selector.Parameters );
        }

        #endregion

        #region " RELATIONS: PART "

        /// <summary>   Gets or sets the Part entity. </summary>
        /// <value> The Part entity. </value>
        public PartEntity PartEntity { get; private set; }

        /// <summary>   Fetches a Part Entity. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Part Entity. </returns>
        public PartEntity FetchPartEntity( System.Data.IDbConnection connection )
        {
            var entity = new PartEntity();
            _ = entity.FetchUsingKey( connection, this.PartAutoId );
            this.PartEntity = entity;
            return entity;
        }

        #endregion

        #region " RELATIONS: PRODUCT TEXT TYPE  "

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.PartEntity"/> <see cref="Dapper.Entities.ElementEntity"/>.
        /// </summary>
        /// <value> the <see cref="Dapper.Entities.PartEntity"/> Text type entity. </value>
        public ElementEntity ElementEntity { get; private set; }

        /// <summary>   Fetches a element entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchElementEntity( System.Data.IDbConnection connection )
        {
            this.ElementEntity = new ElementEntity();
            return this.ElementEntity.FetchUsingKey( connection, this.ElementAutoId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartElementEquationEntity.PartAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.PartEntity"/> record.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.PartEntity"/> record. </value>
        public int PartAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartElementEquationEntity.ElementAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the identity (type) of the <see cref="Dapper.Entities.ElementEntity"/>.
        /// </summary>
        /// <value> Identifies the Part Text type. </value>
        public int ElementAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.PartElementEquationEntity"/> label. </summary>
        /// <value> the <see cref="Dapper.Entities.PartElementEquationEntity"/> label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the equation. </summary>
        /// <value> The equation. </value>
        public string Equation
        {
            get => this.Label;

            set => this.Label = value;
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public TwoKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="PartElementEquationEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class PartElementEquationEntityCollection : EntityKeyedCollection<TwoKeySelector, ITwoToManyLabel, PartElementEquationNub, PartElementEquationEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override TwoKeySelector GetKeyForItem( PartElementEquationEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( PartElementEquationEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all parts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<PartElementEquationEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( PartElementEquationEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return PartElementEquationBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of <see cref="Dapper.Entities.PartEntity"/>-Unique
    /// <see cref="PartElementEquationEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class PartUniqueElementEquationEntityCollection : PartElementEquationEntityCollection
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="part"> The part. </param>
        public PartUniqueElementEquationEntityCollection( PartEntity part ) : base()
        {
            this.PartAutoId = part.AutoId;
            this._UniqueIndexDictionary = new Dictionary<TwoKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, TwoKeySelector>();
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<TwoKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, TwoKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( PartElementEquationEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.ElementAutoId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.ElementAutoId );
        }

        /// <summary>
        /// Removes all parts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        #endregion

        #region " SELECTORS "

        /// <summary>   Queries if collection contains 'Element' key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int elementAutoId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( elementAutoId );
        }

        /// <summary>
        /// Returns the equation for the specified identifier of the <see cref="ElementEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-07-16. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   A String. </returns>
        public string Equation( int elementAutoId )
        {
            return this[this._PrimaryKeyDictionary[elementAutoId]].Equation;
        }

        /// <summary>   Returns the equation for the specified <see cref="NutEntity"/>. </summary>
        /// <remarks>   David, 2020-07-16. </remarks>
        /// <param name="nut">  The nut. </param>
        /// <returns>   A String. </returns>
        public string Equation( NutEntity nut )
        {
            return this[this._PrimaryKeyDictionary[nut.ElementAutoId]].Equation;
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.PartEntity"/>. </value>
        public int PartAutoId { get; private set; }

        #endregion

    }
}

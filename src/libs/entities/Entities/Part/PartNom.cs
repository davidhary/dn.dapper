using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Part-Meter-Element-Nominal-Trait builder. </summary>
    /// <remarks>   David, 2020-06-16. </remarks>
    public sealed class PartNomBuilder : ThreeToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( PartNomNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = PartBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( PartNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = MeterBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( MeterNub.Id );

        /// <summary>   Gets or sets the name of the Ternary table. </summary>
        /// <value> The name of the Ternary table. </value>
        public override string TernaryTableName { get; set; } = ElementBuilder.TableName;

        /// <summary>   Gets or sets the name of the Ternary table key. </summary>
        /// <value> The name of the Ternary table key. </value>
        public override string TernaryTableKeyName { get; set; } = nameof( ElementNub.AutoId );

        /// <summary>   Gets or sets the name of the Quaternary table. </summary>
        /// <value> The name of the Quaternary table. </value>
        public override string QuaternaryTableName { get; set; } = NomTraitBuilder.TableName;

        /// <summary>   Gets or sets the name of the Quaternary table key. </summary>
        /// <value> The name of the Quaternary table key. </value>
        public override string QuaternaryTableKeyName { get; set; } = nameof( NomTraitNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( PartNomEntity.PartAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( PartNomEntity.MeterId );

        /// <summary>   Gets or sets the name of the ternary identifier field. </summary>
        /// <value> The name of the ternary identifier field. </value>
        public override string TernaryIdFieldName { get; set; } = nameof( PartNomEntity.ElementAutoId );

        /// <summary>   Gets or sets the name of the quaternary identifier field. </summary>
        /// <value> The name of the quaternary identifier field. </value>
        public override string QuaternaryIdFieldName { get; set; } = nameof( PartNomEntity.NomTraitAutoId );

        #region " SINGLETON "

        private static readonly Lazy<PartNomBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static PartNomBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="PartNomNub"/> based on the
    /// <see cref="IThreeToMany">interface</see>.
    /// </summary>
    /// <remarks>   David, 2020-06-16. </remarks>
    [Table( "PartNom" )]
    public class PartNomNub : ThreeToManyNub, IThreeToMany
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        public PartNomNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IThreeToMany CreateNew()
        {
            return new PartNomNub();
        }
    }

    /// <summary>
    /// The <see cref="PartNomEntity"/> based on the
    /// <see cref="IThreeToMany">interface</see>.
    /// </summary>
    /// <remarks>   David, 2020-06-16. </remarks>
    public class PartNomEntity : EntityBase<IThreeToMany, PartNomNub>, IThreeToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        public PartNomEntity() : this( new PartNomNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PartNomEntity( IThreeToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public PartNomEntity( IThreeToMany cache, IThreeToMany store ) : base( new PartNomNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PartNomEntity( PartNomEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.PartNomBuilder.TableName, nameof( IThreeToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IThreeToMany CreateNew()
        {
            return new PartNomNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IThreeToMany CreateCopy()
        {
            var destination = this.CreateNew();
            ThreeToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IThreeToMany value )
        {
            ThreeToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="value">    The Part-Element interface. </param>
        public override void UpdateCache( IThreeToMany value )
        {
            // first make the copy to notify of any property change.
            ThreeToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches a <see cref="PartNomEntity"/> using the entity key. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTraitAutoId">   Identifies the <see cref="Dapper.Entities.NomTraitEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTraitAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, partAutoId, meterId, elementAutoId, nomTraitAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Fetches a <see cref="PartNomEntity"/> using the entity key. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PartAutoId, this.MeterId, this.ElementAutoId, this.NomTraitAutoId );
        }

        /// <summary>
        /// Fetches an existing <see cref="PartNomEntity"/> using the entity unique index.
        /// </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PartAutoId, this.MeterId, this.ElementAutoId, this.NomTraitAutoId );
        }

        /// <summary>
        /// Fetches an existing <see cref="PartNomEntity"/> using the entity unique index.
        /// </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTraitAutoId">   Identifies the <see cref="Dapper.Entities.NomTraitEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTraitAutoId )
        {
            return this.FetchUsingKey( connection, partAutoId, meterId, elementAutoId, nomTraitAutoId );
        }

        /// <summary>
        /// Tries to fetch an existing <see cref="Dapper.Entities.NomTraitEntity"/> associated with a
        /// <see cref="Dapper.Entities.PartEntity"/> and <see cref="Dapper.Entities.ElementEntity"/>;
        /// otherwise, inserts a new <see cref="Dapper.Entities.NomTraitEntity"/>. Then tries to fetch an
        /// existing or insert a new
        /// <see cref="PartNomEntity"/>.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.PartEntity"/> exists for the specified
        /// <paramref name="partAutoId"/>
        /// Assumes that a <see cref="Dapper.Entities.ElementEntity"/> exists for the specified
        /// <paramref name="elementAutoId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitType"/>. </param>
        /// <param name="amount">           The amount. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainNomTrail( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTypeId, int nomTraitTypeId, double amount )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            this.NomTraitEntity = FetchNomTrait( connection, partAutoId, elementAutoId, nomTypeId, nomTraitTypeId );
            if ( !this.NomTraitEntity.IsClean() )
            {
                this.NomTraitEntity = new NomTraitEntity() { NomTypeId = nomTypeId, NomTraitTypeId = nomTraitTypeId, Amount = amount };
                if ( !this.NomTraitEntity.Insert( connection ) )
                {
                    result = (false, $"Failed inserting {nameof( Dapper.Entities.NomTraitEntity )} with {nameof( Dapper.Entities.NomTraitEntity.NomTypeId )} of {nomTypeId} and {nameof( Dapper.Entities.NomTraitEntity.NomTraitTypeId )} of {nomTraitTypeId}");
                }
            }

            if ( result.Success )
            {
                this.PrimaryId = partAutoId;
                this.SecondaryId = elementAutoId;
                this.TernaryId = this.NomTraitEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( PartNomEntity.NomTraitEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( PartNomEntity )} with {nameof( this.PartAutoId )} of {partAutoId} and {nameof( this.ElementAutoId )} of {elementAutoId} and {nameof( this.NomTraitAutoId )} of {this.NomTraitEntity.AutoId}");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch existing <see cref="Dapper.Entities.PartEntity"/> and
        /// <see cref="Dapper.Entities.ElementEntity"/> and fetches or inserts a new
        /// <see cref="Dapper.Entities.NomTraitEntity"/> and fetches an existing or inserts a new
        /// <see cref="PartNomEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitType"/>. </param>
        /// <param name="amount">           The amount. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTypeId, int nomTraitTypeId, double amount )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.PartEntity is null || this.PartEntity.AutoId != partAutoId )
            {
                // make sure we have a part associated with the part auto ID.
                this.PartEntity = new PartEntity() { AutoId = partAutoId };
                if ( !this.PartEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.PartEntity )} with {nameof( Dapper.Entities.PartEntity.AutoId )} of {partAutoId}");
                }
            }

            if ( this.MeterEntity is null || this.MeterEntity.Id != meterId )
            {
                // make sure we have an meter associated with the meter ID.
                this.MeterEntity = new MeterEntity() { Id = meterId };
                if ( !this.MeterEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.MeterEntity )} with {nameof( Dapper.Entities.MeterEntity.Id )} of {meterId}");
                }
            }

            if ( this.ElementEntity is null || this.ElementEntity.AutoId != elementAutoId )
            {
                // make sure we have an element associated with the element auto ID.
                this.ElementEntity = new ElementEntity() { AutoId = elementAutoId };
                if ( !this.ElementEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.ElementEntity )} with {nameof( Dapper.Entities.ElementEntity.AutoId )} of {elementAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainNomTrail( connection, partAutoId, meterId, elementAutoId, nomTypeId, nomTraitTypeId, amount );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( PartNomEntity.PartEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch existing <see cref="Dapper.Entities.PartEntity"/> and
        /// <see cref="Dapper.Entities.ElementEntity"/> and fetches or inserts a new
        /// <see cref="Dapper.Entities.NomTraitEntity"/> and fetches an existing or inserts a new
        /// <see cref="PartNomEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitType"/>. </param>
        /// <param name="amount">           The amount. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int nomTypeId, int nomTraitTypeId, double amount )
        {
            return this.TryObtain( connection, this.PartAutoId, this.MeterId, this.ElementAutoId, nomTypeId, nomTraitTypeId, amount );
        }

        /// <summary>
        /// Tries to fetch existing <see cref="Dapper.Entities.PartEntity"/> and
        /// <see cref="Dapper.Entities.ElementEntity"/> and fetches or inserts a new
        /// <see cref="Dapper.Entities.NomTraitEntity"/> and fetches an existing or inserts a new
        /// <see cref="PartNomEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitType"/>. </param>
        /// <param name="amount">           The amount. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool Obtain( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTypeId, int nomTraitTypeId, double amount )
        {
            var (Success, Details) = this.TryObtain( connection, partAutoId, meterId, elementAutoId, nomTypeId, nomTraitTypeId, amount );
            return !Success ? throw new InvalidOperationException( Details ) : Success;
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IThreeToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId, entity.TernaryId, entity.QuaternaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId )
        {
            return connection.Delete( new PartNomNub() {
                PrimaryId = partAutoId,
                SecondaryId = meterId,
                TernaryId = elementAutoId
            } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="PartNomEntity"/>'s. </summary>
        /// <value> The Part-Element entities. </value>
        public IEnumerable<PartNomEntity> PartMeterElementNominals { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartNomEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IThreeToMany>() ) : Populate( connection.GetAll<PartNomNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.PartMeterElementNominals = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( PartNomEntity.PartMeterElementNominals ) );
            return this.PartMeterElementNominals?.Any() == true ? this.PartMeterElementNominals.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PartNomEntity> Populate( IEnumerable<PartNomNub> nubs )
        {
            var l = new List<PartNomEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( PartNomNub nub in nubs )
                    l.Add( new PartNomEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PartNomEntity> Populate( IEnumerable<IThreeToMany> interfaces )
        {
            var l = new List<PartNomEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new PartNomNub();
                foreach ( IThreeToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new PartNomEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>
        /// Count <see cref="PartNomEntity"/>'s by <see cref="Dapper.Entities.PartEntity"/>; returns up to Meter
        /// Entities Count * Element entities count * Nominal Types Count * Nominal Traits count.
        /// </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PartNomBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the <see cref="PartNomEntity"/>'s by <see cref="Dapper.Entities.PartEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartNomEntity> FetchEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartNomBuilder.TableName}] WHERE {nameof( PartNomNub.PrimaryId )} = @Id", new { Id = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<PartNomNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>
        /// Count <see cref="PartNomEntity"/>'s by <see cref="Dapper.Entities.ElementEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   The total number of entities by Element. </returns>
        public static int CountEntitiesByElement( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PartNomBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the <see cref="PartNomEntity"/>'s by Elements in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Elements in this
        /// collection.
        /// </returns>
        public static IEnumerable<PartNomEntity> FetchEntitiesByElement( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartNomBuilder.TableName}] WHERE {nameof( PartNomNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<PartNomNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count <see cref="PartNomEntity"/>'s; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTraitAutoId">   Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTraitAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( $"select count(*) from [{PartNomBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.TernaryId )} = @TernaryId", new { TernaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.QuaternaryId )} = @QuaternaryId", new { QuaternaryId = nomTraitAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches <see cref="PartNomNub"/>'s; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTraitAutoId">   Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartNomNub> FetchNubs( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTraitAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( $"select * from [{PartNomBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.TernaryId )} = @TernaryId", new { TernaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartNomNub.QuaternaryId )} = @QuaternaryId", new { QuaternaryId = nomTraitAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }
            // return connection.Query(Of PartNomNub)(selector.RawSql, selector.Parameters, transaction:=TryCast(connection, TransactedConnection)?.Transaction)
            return connection.Query<PartNomNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="PartNomEntity"/> exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTraitAutoId">   Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTraitAutoId )
        {
            return 1 == CountEntities( connection, partAutoId, meterId, elementAutoId, nomTraitAutoId );
        }

        #endregion

        #region " RELATIONS: PART "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <value> The Part entity. </value>
        public PartEntity PartEntity { get; private set; }

        /// <summary>   Fetches  <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Part Entity. </returns>
        public PartEntity FetchPartEntity( System.Data.IDbConnection connection )
        {
            var entity = new PartEntity();
            _ = entity.FetchUsingKey( connection, this.PartAutoId );
            this.PartEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count <see cref="Dapper.Entities.PartEntity"/>'s associated with the specified
        /// <paramref name="elementAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   The total number of Parts. </returns>
        public static int CountParts( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.PartNomBuilder.TableName}] WHERE {nameof( PartNomNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the <see cref="Dapper.Entities.PartEntity"/>'s associated with the specified
        /// <paramref name="elementAutoId"/>; expected a single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Parts in this collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchParts( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartNomBuilder.TableName}] WHERE {nameof( PartNomNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ElementEntity>();
            foreach ( PartNomNub nub in connection.Query<PartNomNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new PartNomEntity( nub );
                l.Add( entity.FetchElementEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Parts associated with the specified <paramref name="elementAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteParts( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.PartNomBuilder.TableName}] WHERE {nameof( PartNomNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        #endregion

        #region " RELATIONS: METER "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public MeterEntity MeterEntity { get; private set; }

        /// <summary>   Fetches a Meter Entity. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The <see cref="Dapper.Entities.MeterEntity"/>. </returns>
        public MeterEntity FetchMeterEntity( System.Data.IDbConnection connection )
        {
            var entity = new MeterEntity();
            _ = entity.FetchUsingKey( connection, this.MeterId );
            this.MeterEntity = entity;
            return entity;
        }

        #endregion

        #region " RELATIONS: ELEMENT "

        /// <summary>   Gets or sets the Element entity. </summary>
        /// <value> The Element entity. </value>
        public ElementEntity ElementEntity { get; private set; }

        /// <summary>   Fetches a Element Entity. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Element Entity. </returns>
        public ElementEntity FetchElementEntity( System.Data.IDbConnection connection )
        {
            var entity = new ElementEntity();
            _ = entity.FetchUsingKey( connection, this.ElementAutoId );
            this.ElementEntity = entity;
            return entity;
        }

        /// <summary>   Count elements. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The total number of elements. </returns>
        public static int CountElements( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.PartNomBuilder.TableName}] WHERE {nameof( PartNomNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Elements in this collection. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Elements in this collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchElements( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartNomBuilder.TableName}] WHERE {nameof( PartNomNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ElementEntity>();
            foreach ( PartNomNub nub in connection.Query<PartNomNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new PartNomEntity( nub );
                l.Add( entity.FetchElementEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Element related to the specified Part. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteElements( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.PartNomBuilder.TableName}] WHERE {nameof( PartNomNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.ElementEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered elements in this
        /// collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchOrderedElements( System.Data.IDbConnection connection, string selectQuery, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = partAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return ElementEntity.Populate( connection.Query<ElementNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.ElementEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered elements in this
        /// collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchOrderedElements( System.Data.IDbConnection connection, int partAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [PartMeterElementNominal] on [PartMeterElementNominal].SecondaryId = [Element].AutoId where [PartMeterElementNominal].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{ElementBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{ElementBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.PartNomBuilder.TableName}].{nameof( PartNomNub.SecondaryId )} = [{isr.Dapper.Entities.ElementBuilder.TableName}].{nameof( ElementNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.PartNomBuilder.TableName}].{nameof( PartNomNub.PrimaryId )} = @Id" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.ElementBuilder.TableName}].{nameof( ElementNub.Label )} ASC; " );
            return FetchOrderedElements( connection, queryBuilder.ToString(), partAutoId );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementLabel"> The <see cref="Dapper.Entities.ElementEntity"/>label. </param>
        /// <returns>   The element. </returns>
        public static ElementEntity FetchElement( System.Data.IDbConnection connection, string selectQuery, int partAutoId, int meterId, string elementLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new {
                PartAutoId = partAutoId,
                MeterId = meterId,
                ElementLabel = elementLabel
            } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<ElementNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new ElementEntity() : new ElementEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <remarks>
        /// David, 2020-05-19.
        /// <code>
        /// SELECT [Element].*
        /// FROM [Element] Inner Join [PartMeterElementNominal]
        /// ON [PartMeterElementNominal].[SecondaryId] = [Element].[AutoId]
        /// WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [Element].[Label] = 'R1')
        /// </code>
        /// </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementLabel"> The <see cref="Dapper.Entities.ElementEntity"/>label. </param>
        /// <returns>   The element. </returns>
        public static ElementEntity FetchElement( System.Data.IDbConnection connection, int partAutoId, int meterId, string elementLabel )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [PartMeterElementNominal] on [PartMeterElementNominal].SecondaryId = [Element].AutoId where [PartMeterElementNominal].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{ElementBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{ElementBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.SecondaryId )}] = [{isr.Dapper.Entities.ElementBuilder.TableName}].[{nameof( ElementNub.AutoId )}]" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.PrimaryId )}] = @PartAutoId " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.SecondaryId )}] = @MeterId " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.ElementBuilder.TableName}].[{nameof( ElementNub.Label )}] = @ElementLabel); " );
            return FetchElement( connection, queryBuilder.ToString(), partAutoId, meterId, elementLabel );
        }

        #endregion

        #region " RELATIONS: NOMINAL TRAITS "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.NomTraitEntity"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.NomTraitEntity"/>. </value>
        public NomTraitEntity NomTraitEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.NomTraitEntity"/>. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The <see cref="Dapper.Entities.NomTraitEntity"/>. </returns>
        public NomTraitEntity FetchNomTraitEntity( System.Data.IDbConnection connection )
        {
            this.NomTraitEntity = FetchNomTrait( connection, this.PartAutoId, this.MeterId, this.ElementAutoId, this.NomTraitAutoId );
            return this.NomTraitEntity;
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.NomTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered
        /// <see cref="Dapper.Entities.NomTraitEntity"/>'s in this collection.
        /// </returns>
        public static IEnumerable<NomTraitEntity> FetchOrderedNomTraits( System.Data.IDbConnection connection, string selectQuery, int partAutoId, int meterId, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new {
                PartAutoId = partAutoId,
                MeterId = meterId,
                ElementAutoId = elementAutoId
            } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return NomTraitEntity.Populate( connection.Query<NomTraitNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.NomTraitEntity"/>'s. </summary>
        /// <remarks>
        /// David, 2020-05-09.
        /// <code>
        /// SELECT [NomTrait].*
        /// FROM [NomTrait] Inner Join [PartMeterElementNominal]
        /// ON [PartMeterElementNominal].[TernaryId] = [NomTrait].[AutoId]
        /// WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [PartMeterElementNominal].[SecondaryId] = 1  AND [PartMeterElementNominal].[TernaryId] = 1)
        /// </code>
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered
        /// <see cref="Dapper.Entities.NomTraitEntity"/>'s in this collection.
        /// </returns>
        public static IEnumerable<NomTraitEntity> FetchOrderedNomTraits( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [PartMeterElementNominal] on [PartMeterElementNominal].SecondaryId = [NomTrait].AutoId where [PartMeterElementNominal].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{NomTraitBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{NomTraitBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.PartNomBuilder.TableName}].{nameof( PartNomNub.QuaternaryId )} = [{isr.Dapper.Entities.NomTraitBuilder.TableName}].{nameof( NomTraitNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.PrimaryId )}] = @PartAutoId " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.SecondaryId )}] = @MeterId " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.TernaryId )}] = @ElementAutoId) " );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.NomTraitBuilder.TableName}].{nameof( NomTraitNub.FirstForeignId )} ASC, [{isr.Dapper.Entities.NomTraitBuilder.TableName}].{nameof( NomTraitNub.SecondForeignId )} ASC; " );
            return FetchOrderedNomTraits( connection, queryBuilder.ToString(), partAutoId, meterId, elementAutoId );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.NomTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered
        /// <see cref="Dapper.Entities.NomTraitEntity"/>'s in this collection.
        /// </returns>
        public static IEnumerable<NomTraitEntity> FetchOrderedNomTraits( System.Data.IDbConnection connection, string selectQuery, int partAutoId, int meterId, int elementAutoId, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new {
                PartAutoId = partAutoId,
                MeterId = meterId,
                ElementAutoId = elementAutoId,
                NomTypeId = nomTypeId
            } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return NomTraitEntity.Populate( connection.Query<NomTraitNub>( template.RawSql, template.Parameters ) );
        }



        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.NomTraitEntity"/>'s. </summary>
        /// <remarks>
        /// David, 2020-05-09.
        /// <code>
        /// SELECT [NomTrait].*
        /// FROM [NomTrait] Inner Join [PartMeterElementNominal]
        /// ON [PartMeterElementNominal].[TernaryId] = [NomTrait].[AutoId]
        /// WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [PartMeterElementNominal].[SecondaryId] = 1 And [NomTrait].[FirstForeignId]=1)
        /// </code>
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered
        /// <see cref="Dapper.Entities.NomTraitEntity"/>'s in this collection.
        /// </returns>
        public static IEnumerable<NomTraitEntity> FetchOrderedNomTraits( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTypeId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"SELECT [{NomTraitBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{NomTraitBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.PartNomBuilder.TableName}].{nameof( PartNomNub.QuaternaryId )} = [{isr.Dapper.Entities.NomTraitBuilder.TableName}].{nameof( NomTraitNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.PrimaryId )}] = @{nameof( partAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.SecondaryId )}] = @{nameof( meterId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.TernaryId )}] = @{nameof( elementAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.NomTraitBuilder.TableName}].[{nameof( NomTraitNub.FirstForeignId )}] = @{nameof( nomTypeId )}) " );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.NomTraitBuilder.TableName}].{nameof( NomTraitNub.SecondForeignId )} ASC; " );
            return FetchOrderedNomTraits( connection, queryBuilder.ToString(), partAutoId, meterId, elementAutoId, nomTypeId );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.NomTraitEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTraitAutoId">   Identifies the <see cref="Dapper.Entities.NomTraitEntity"/>. </param>
        /// <returns>   The Nominal Trait Entity. </returns>
        public static NomTraitEntity FetchNomTrait( System.Data.IDbConnection connection, string selectQuery, int partAutoId, int meterId, int elementAutoId, int nomTraitAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new {
                PartAutoId = partAutoId,
                MeterId = meterId,
                ElementAutoId = elementAutoId,
                NomTraitAutoId = nomTraitAutoId
            } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<NomTraitNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new NomTraitEntity() : new NomTraitEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.NomTraitEntity"/>. </summary>
        /// <remarks>
        /// David, 2020-06-16.
        /// <code>
        /// SELECT [NomTrait].*
        /// FROM [NomTrait] Inner Join [PartMeterElementNominal]
        /// ON [PartMeterElementNominal].[TernaryId] = [NomTrait].[AutoId]
        /// WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [PartMeterElementNominal].[SecondaryId] = 1 AND [PartMeterElementNominal].[TernaryId] = 1 )
        /// </code>
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTraitAutoId">   Identifies the <see cref="Dapper.Entities.NomTraitEntity"/>. </param>
        /// <returns>   The Nom Trait Entity. </returns>
        public static NomTraitEntity FetchNomTrait( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTraitAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"SELECT [{NomTraitBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{NomTraitBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.TernaryId )}] = [{isr.Dapper.Entities.NomTraitBuilder.TableName}].[{nameof( NomTraitNub.AutoId )}]" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.PrimaryId )}] = @{nameof( partAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.SecondaryId )}] = @{nameof( meterId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.TernaryId )}] = @{nameof( elementAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.QuaternaryId )}] = @{nameof( nomTraitAutoId )}); " );
            return FetchNomTrait( connection, queryBuilder.ToString(), partAutoId, meterId, elementAutoId, nomTraitAutoId );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.NomTraitEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">     Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitTypeEntity"/>. </param>
        /// <returns>   The Nom Trait entity. </returns>
        public static NomTraitEntity FetchNomTrait( System.Data.IDbConnection connection, string selectQuery, int partAutoId, int meterId, int elementAutoId, int nomTypeId, int nomTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new {
                PartAutoId = partAutoId,
                MeterId = meterId,
                ElementAutoId = elementAutoId,
                NomTypeId = nomTypeId,
                NomTraitTypeId = nomTraitTypeId
            } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<NomTraitNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new NomTraitEntity() : new NomTraitEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.NomTraitEntity"/>. </summary>
        /// <remarks>
        /// David, 2020-06-16.
        /// <code>
        /// SELECT [NomTrait].*
        /// FROM [NomTrait] Inner Join [PartMeterElementNominal]
        /// ON [PartMeterElementNominal].[TernaryId] = [NomTrait].[AutoId]
        /// WHERE ([PartMeterElementNominal].[PrimaryId] = 8 AND [NomTrait].[FirstForeignId] = 1 AND [NomTrait].[SecondForeignId] = 1 )
        /// </code>
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitTypeEntity"/>. </param>
        /// <returns>   The Nom Trait entity. </returns>
        public static NomTraitEntity FetchNomTrait( System.Data.IDbConnection connection, int partAutoId, int meterId, int elementAutoId, int nomTypeId, int nomTraitTypeId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"SELECT [{NomTraitBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{NomTraitBuilder.TableName}] Inner Join [{PartNomBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.TernaryId )}] = [{isr.Dapper.Entities.NomTraitBuilder.TableName}].[{nameof( NomTraitNub.AutoId )}]" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.PrimaryId )}] = @{nameof( partAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.SecondaryId )}] = @{nameof( meterId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.PartNomBuilder.TableName}].[{nameof( PartNomNub.TernaryId )}] = @{nameof( elementAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.NomTraitBuilder.TableName}].[{nameof( NomTraitNub.FirstForeignId )}] = @{nameof( nomTypeId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.NomTraitBuilder.TableName}].[{nameof( NomTraitNub.SecondForeignId )}] = @{nameof( nomTraitTypeId )}); " );
            return FetchNomTrait( connection, queryBuilder.ToString(), partAutoId, meterId, elementAutoId, nomTypeId, nomTraitTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartNomEntity.PartAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.PartEntity"/> </value>
        public int PartAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartNomEntity.MeterId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>   Gets or sets the id of the Ternary reference. </summary>
        /// <value> The identifier of Ternary reference. </value>
        public int TernaryId
        {
            get => this.ICache.TernaryId;

            set {
                if ( !object.Equals( ( object ) this.TernaryId, ( object ) value ) )
                {
                    this.ICache.TernaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartNomEntity.ElementAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int ElementAutoId
        {
            get => this.TernaryId;

            set => this.TernaryId = value;
        }

        /// <summary>   Gets or sets the id of the Ternary reference. </summary>
        /// <value> The identifier of Ternary reference. </value>
        public int QuaternaryId
        {
            get => this.ICache.QuaternaryId;

            set {
                if ( !object.Equals( ( object ) this.QuaternaryId, ( object ) value ) )
                {
                    this.ICache.QuaternaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartNomEntity.NomTraitAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NomTraitEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTraitEntity"/>. </value>
        public int NomTraitAutoId
        {
            get => this.QuaternaryId;

            set => this.QuaternaryId = value;
        }

        #endregion

    }

    /// <summary>
    /// Collection of unique <see cref="Dapper.Entities.PartEntity"/> +
    /// <see cref="Dapper.Entities.MeterEntity"/> +
    /// <see cref="Dapper.Entities.ElementEntity"/> <see cref="NomTraitEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. <para>
    /// Each product consists of a set of elements such as one or more resistors or compound resistors,
    /// open or short circuits and equations. When associated with specific parts for the product family,
    /// the elements get assigned nominal trait values defining the nominal specification for the part.
    /// Theses specifications are called nominal traits and are specific for each meter.
    /// </para></remarks>
    public class PartNomEntityCollection : NomTraitEntityCollection, Std.Primitives.IGetterSetter<double>
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="part">         The <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="meter">        The <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="element">      The <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        public PartNomEntityCollection( PartEntity part, MeterEntity meter, ElementEntity element, int nomTypeId ) : base()
        {
            this.PartAutoId = part.AutoId;
            this.MeterId = meter.Id;
            this.ElementAutoId = element.AutoId;
            this._UniqueIndexDictionary = new Dictionary<ThreeKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, ThreeKeySelector>();
            this.NomTrait = new NomTrait( this, element, nomTypeId, meter );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        /// <param name="part">             The <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        public PartNomEntityCollection( PartEntity part, PlatformMeter platformMeter ) : this( part, platformMeter.Meter,
                                                                                               platformMeter.PrimaryElement,
                                                                                               ( int ) platformMeter.PrimaryNominalType )
        {

        }
        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<ThreeKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, ThreeKeySelector> _PrimaryKeyDictionary;

        /// <summary>   Gets Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public int NomTypeId => ( int ) this.NomTrait.NomType;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( NomTraitEntity entity )
        {
            if ( entity.NomTypeId == this.NomTypeId )
            {
                base.Add( entity );
                this._PrimaryKeyDictionary.Add( entity.NomTraitTypeId, entity.EntitySelector );
                this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.NomTraitTypeId );
                this.NotifyPropertyChanged( NomTraitTypeEntity.EntityLookupDictionary()[entity.NomTraitTypeId].Label );
            }
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains a key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsTrait( int nomTraitTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( nomTraitTypeId );
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.PartEntity"/>. </value>
        public int PartAutoId { get; private set; }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int ElementAutoId { get; private set; }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId { get; private set; }

        #endregion

        #region " GETTER SETTER "

        /// <summary>
        /// Gets the value of the given <see cref="NomTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        double? IGetterSetter<double>.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Sets the part nom value for the given <see cref="NomTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A double. </returns>
        double IGetterSetter<double>.Setter( double value, string name )
        {
            return this.SetterThis( value, name );
        }


        /// <summary>
        /// Gets the value of the given <see cref="NomTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        protected double? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( name );
        }

        /// <summary>
        /// Sets the part nom value for the given <see cref="NomTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        private double SetterThis( double value, string name )
        {
            int key = this.ToKey( name );
            if ( !int.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>
        /// Sets the part nom value for the given <see cref="NomTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        protected double Setter( double value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets or sets the Nominal Trait. </summary>
        /// <value> The Nominal Trait. </value>
        public NomTrait NomTrait { get; private set; }

        #endregion

        #region " TRAIT SELECTION "

        /// <summary>
        /// Converts a name to a key using the
        /// <see cref="NomTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
        /// extend the element Nominal Traits beyond the values of the enumeration type that is used to
        /// populate this table.
        /// </summary>
        /// <remarks>   David, 2020-05-11. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return NomTraitTypeEntity.KeyLookupDictionary()[name];
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Double. </returns>
        public double? Setter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                this.Setter( this.ToKey( name ), value.Value );
                this.NotifyPropertyChanged( name );
            }

            return value;
        }

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitTypeEntity"/>. </param>
        /// <returns>   An elementReadingRealEntity. </returns>
        public NomTraitEntity Entity( int nomTraitTypeId )
        {
            return this.ContainsTrait( nomTraitTypeId ) ? this[this._PrimaryKeyDictionary[nomTraitTypeId]] : new NomTraitEntity();
        }

        /// <summary>   Gets the value of the given Trait Type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitTypeEntity"/>. </param>
        /// <returns>   A Double? </returns>
        public double? Getter( int nomTraitTypeId )
        {
            return this.ContainsTrait( nomTraitTypeId ) ? this[this._PrimaryKeyDictionary[nomTraitTypeId]].Amount : new double?();
        }

        /// <summary>   Set the specified element value. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="nomTraitTypeId">   Identifies the <see cref="NomTraitTypeEntity"/>. </param>
        /// <param name="value">            The value. </param>
        public void Setter( int nomTraitTypeId, double value )
        {
            if ( this.ContainsTrait( nomTraitTypeId ) )
            {
                this[this._PrimaryKeyDictionary[nomTraitTypeId]].Amount = value;
            }
            else
            {
                this.Add( new NomTraitEntity() { NomTypeId = NomTypeId, NomTraitTypeId = nomTraitTypeId, Amount = value } );
            }
        }

        #endregion

        #region " UPSERT "

        /// <summary>   Inserts or updates all entities using the given connection. </summary>
        /// <remarks>
        /// David, 2020-05-13. Entities that failed to save are enumerated in
        /// <see cref="P:isr.Dapper.Entity.EntityKeyedCollection`4.UnsavedKeys" />
        /// </remarks>
        /// <param name="transcactedConnection">    The <see cref="T:Dapper.TransactedConnection" />. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [CLSCompliant( false )]
        public override bool Upsert( TransactedConnection transcactedConnection )
        {
            this.ClearUnsavedKeys();
            // dictionary is instantiated only after the collection has values.
            if ( !this.Any() )
                return true;
            PartNomEntity partMeterElementNominal;
            foreach ( KeyValuePair<ThreeKeySelector, NomTraitEntity> keyValue in this.Dictionary )
            {
                if ( keyValue.Value.Upsert( transcactedConnection ) )
                {
                    partMeterElementNominal = new PartNomEntity() {
                        PartAutoId = PartAutoId,
                        MeterId = MeterId,
                        ElementAutoId = ElementAutoId,
                        NomTraitAutoId = keyValue.Value.AutoId
                    };
                    try
                    {
                        if ( !partMeterElementNominal.Obtain( transcactedConnection ) )
                        {
                            this.AddUnsavedKey( keyValue.Key );
                        }
                    }
                    catch
                    {
                        throw;
                    }
                }
                else
                {
                    this.AddUnsavedKey( keyValue.Key );
                }
            }

            // success if no unsaved keys
            return !this.UnsavedKeys.Any();
        }

        #endregion

    }
}

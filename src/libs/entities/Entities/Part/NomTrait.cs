using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{
    /// <summary>   The Nominal Trait class holing the Nominal Trait values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class NomTrait : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSestter">    The getter setter. </param>
        /// <param name="element">          The <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        private NomTrait( Std.Primitives.IGetterSetter<double> getterSestter, ElementEntity element, int nomTypeId ) : base()
        {
            this.GetterSetter = getterSestter;
            this.ElementLabel = element.ElementLabel;
            this.ElementType = ( ElementType ) element.ElementTypeId;
            this.ElementOrdinalNumber = element.OrdinalNumber;
            this.NomType = ( NomType ) nomTypeId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSetter"> The getter setter. </param>
        /// <param name="element">      The <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="meter">        The <see cref="Dapper.Entities.MeterEntity"/>. </param>
        public NomTrait( Std.Primitives.IGetterSetter<double> getterSetter, ElementEntity element, int nomTypeId, MeterEntity meter ) : this( getterSetter, element, nomTypeId )
        {
            this.MeterId = meter.Id;
            this.MeterNumber = meter.MeterNumber;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<double> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Double. </returns>
        public double? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Double. </returns>
        public double? Setter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }

            return value;
        }

        #endregion

        #region " ELEMENT PROPERTIES "

        /// <summary>   The element label. </summary>
        private string _ElementLabel;

        /// <summary>   Gets or sets the Label of the Element. </summary>
        /// <value> The Label of the Element. </value>
        public string ElementLabel
        {
            get => this._ElementLabel;

            set {
                if ( !string.Equals( value, this.ElementLabel ) )
                {
                    this._ElementLabel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Type of the element. </summary>
        private ElementType _ElementType;

        /// <summary>   Gets or sets the type of the Element. </summary>
        /// <value> The type of the Element. </value>
        public ElementType ElementType
        {
            get => this._ElementType;

            set {
                if ( value != this.ElementType )
                {
                    this._ElementType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The element ordinal number. </summary>
        private int _ElementOrdinalNumber;

        /// <summary>   Gets or sets the Element Ordinal number. </summary>
        /// <value> The ElementOrdinal number. </value>
        public int ElementOrdinalNumber
        {
            get => this._ElementOrdinalNumber;

            set {
                if ( !Equals( value, this.ElementOrdinalNumber ) )
                {
                    this._ElementOrdinalNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Type of the nom. </summary>
        private NomType _NomType;

        /// <summary>   Gets or sets the type of the nominal. </summary>
        /// <value> The type of the nominal. </value>
        public NomType NomType
        {
            get => this._NomType;

            set {
                if ( value != this.NomType )
                {
                    this._NomType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _MeterId;

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId
        {
            get => this._MeterId;

            set {
                if ( value != this.MeterId )
                {
                    this._MeterId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _MeterNumber;

        /// <summary>   Gets or sets the meter number. </summary>
        /// <value> The meter number. </value>
        public int MeterNumber
        {
            get => this._MeterNumber;

            set {
                if ( value != this.MeterNumber )
                {
                    this._MeterNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " NOMINAL VALUES "

        /// <summary>   Gets or sets the nominal value. </summary>
        /// <value> The nominal value. </value>
        public double? NominalValue
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the tolerance. </summary>
        /// <value> The tolerance. </value>
        public double? Tolerance
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the tracking tolerance. </summary>
        /// <value> The tracking tolerance. </value>
        public double? TrackingTolerance
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the tcr. </summary>
        /// <value> The tcr. </value>
        public double? Tcr
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the Tcr Span. </summary>
        /// <value> The Tcr Span. </value>
        public double? TcrSpan
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the tracking tcr. </summary>
        /// <value> The tracking tcr. </value>
        public double? TrackingTcr
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the tracking Tcr Span. </summary>
        /// <value> The tracking Tcr Span. </value>
        public double? TrackingTcrSpan
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the lower overflow limit. </summary>
        /// <value> The lower overflow limit. </value>
        public double? LowerOverflowLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the upper overflow limit. </summary>
        /// <value> The upper overflow limit. </value>
        public double? UpperOverflowLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the overflow range. </summary>
        /// <value> The overflow range. </value>
        public Std.Primitives.RangeR OverflowRange
        {
            get => this.LowerOverflowLimit.HasValue && this.UpperOverflowLimit.HasValue ? new Std.Primitives.RangeR( this.LowerOverflowLimit.Value, this.UpperOverflowLimit.Value ) : Std.Primitives.RangeR.Empty;

            set {
                this.LowerOverflowLimit = value.Min;
                this.UpperOverflowLimit = value.Max;
            }
        }

        /// <summary>   Gets or sets the nominal power. </summary>
        /// <value> The nominal power. </value>
        public double? NominalPower
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        #endregion

        #region " SPECIFICATION RANGE "

        /// <summary>   Gets or sets the lower specification limit. </summary>
        /// <value> The lower specification limit. </value>
        public double? LowerSpecificationLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the upper specification limit. </summary>
        /// <value> The upper specification limit. </value>
        public double? UpperSpecificationLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the specification range. </summary>
        /// <value> The specification range. </value>
        public Std.Primitives.RangeR SpecificationRange
        {
            get => this.LowerSpecificationLimit.HasValue && this.UpperSpecificationLimit.HasValue ? new Std.Primitives.RangeR( this.LowerSpecificationLimit.Value, this.UpperSpecificationLimit.Value ) : Std.Primitives.RangeR.Empty;

            set {
                this.LowerSpecificationLimit = value.Min;
                this.UpperSpecificationLimit = value.Max;
            }
        }

        #endregion

        #region " GUARD "

        /// <summary>   Gets or sets the guard band factor. </summary>
        /// <value> The guard band factor. </value>
        public double? GuardBandFactor
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the sigma performance level. </summary>
        /// <value> The sigma performance level. </value>
        public double? SigmaPerformanceLevel
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the estimated dispersion. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The estimated dispersion. </value>
        public double? EstimatedDispersion => !this.SigmaPerformanceLevel.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.SigmaPerformanceLevel )} must be set before getting the {nameof( NomTrait.EstimatedDispersion )}" )
                    : !this.Tolerance.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.Tolerance )} must be set before getting the {nameof( NomTrait.EstimatedDispersion )}" )
                    : this.Tolerance / this.SigmaPerformanceLevel;

        /// <summary>   Gets or sets the dispersion. </summary>
        /// <value> The dispersion. </value>
        public double? Dispersion
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the estimated guarded specification limit. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The estimated guarded specification limit. </value>
        public double EstimatedGuardedSpecificationLimit => !this.GuardBandFactor.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.GuardBandFactor )} must be set before getting the {nameof( NomTrait.EstimatedGuardedSpecificationRange )}" )
                    : !this.Tolerance.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.Tolerance )} must be set before getting the {nameof( NomTrait.EstimatedGuardedSpecificationRange )}" )
                    : !this.SigmaPerformanceLevel.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.SigmaPerformanceLevel )} must be set before getting the {nameof( NomTrait.EstimatedGuardedSpecificationRange )}" )
                    : this.Tolerance.Value * (1d - this.GuardBandFactor.Value / this.SigmaPerformanceLevel.Value);

        /// <summary>
        /// Gets or sets the guarded specification limit--this is the tolerance reduced by the guard band
        /// factor.
        /// </summary>
        /// <value> The guarded specification limit. </value>
        public double? GuardedSpecificationLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the lower guarded specification limit. </summary>
        /// <value> The lower guarded specification limit. </value>
        public double? LowerGuardedSpecificationLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the upper guarded specification limit. </summary>
        /// <value> The upper guarded specification limit. </value>
        public double? UpperGuardedSpecificationLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the guarded specification range. </summary>
        /// <value> The guarded specification range. </value>
        public Std.Primitives.RangeR GuardedSpecificationRange
        {
            get => this.LowerGuardedSpecificationLimit.HasValue && this.UpperGuardedSpecificationLimit.HasValue ? new Std.Primitives.RangeR( this.LowerGuardedSpecificationLimit.Value, this.UpperGuardedSpecificationLimit.Value ) : Std.Primitives.RangeR.Empty;

            set {
                this.LowerGuardedSpecificationLimit = value.Min;
                this.UpperGuardedSpecificationLimit = value.Max;
            }
        }

        /// <summary>   Gets or sets the estimated guard band. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The estimated guard band. </value>
        public double? EstimatedGuardBand => !this.GuardBandFactor.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.GuardBandFactor )} must be set before getting the {nameof( NomTrait.EstimatedGuardBand )}" )
                    : !this.Tolerance.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.Tolerance )} must be set before getting the {nameof( NomTrait.EstimatedGuardBand )}" )
                    : !this.SigmaPerformanceLevel.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.SigmaPerformanceLevel )} must be set before getting the {nameof( NomTrait.EstimatedGuardBand )}" )
                    : this.GuardBandFactor * this.Tolerance / this.SigmaPerformanceLevel;

        /// <summary>   Gets or sets the guard band. </summary>
        /// <value> The guard band. </value>
        public double? GuardBand
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the estimated specification range. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The specification range. </value>
        public Std.Primitives.RangeR EstimatedSpecificationRange => !this.NominalValue.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.NominalValue )} must be set before getting the {nameof( NomTrait.SpecificationRange )}" )
                    : !this.Tolerance.HasValue
                    ? throw new System.InvalidOperationException( $"{nameof( NomTrait.Tolerance )} must be set before getting the {nameof( NomTrait.SpecificationRange )}" )
                    : new Std.Primitives.RangeR( this.NominalValue.Value * (1d - this.Tolerance.Value), this.NominalValue.Value * (1d + this.Tolerance.Value) );

        /// <summary>   Gets or sets the estimated guarded specification range. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The estimated guarded specification range. </value>
        public Std.Primitives.RangeR EstimatedGuardedSpecificationRange
        {
            get {
                if ( !this.NominalValue.HasValue )
                    throw new System.InvalidOperationException( $"{nameof( NomTrait.NominalValue )} must be set before getting the {nameof( NomTrait.EstimatedGuardedSpecificationRange )}" );
                if ( !this.GuardBandFactor.HasValue )
                    throw new System.InvalidOperationException( $"{nameof( NomTrait.GuardBandFactor )} must be set before getting the {nameof( NomTrait.EstimatedGuardedSpecificationRange )}" );
                if ( !this.Tolerance.HasValue )
                    throw new System.InvalidOperationException( $"{nameof( NomTrait.Tolerance )} must be set before getting the {nameof( NomTrait.EstimatedGuardedSpecificationRange )}" );
                if ( !this.SigmaPerformanceLevel.HasValue )
                    throw new System.InvalidOperationException( $"{nameof( NomTrait.SigmaPerformanceLevel )} must be set before getting the {nameof( NomTrait.EstimatedGuardedSpecificationRange )}" );
                double guardedTolerance = this.Tolerance.Value * (1d - this.GuardBandFactor.Value / this.SigmaPerformanceLevel.Value);
                return new Std.Primitives.RangeR( this.NominalValue.Value * (1d - guardedTolerance), this.NominalValue.Value * (1d + guardedTolerance) );
            }
        }

        #endregion

        #region " ANNEALING "

        /// <summary>   Gets or sets the lower Tan value. </summary>
        /// <value> The lower Tan value. </value>
        public double? LowerTanValue
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the upper Tan value. </summary>
        /// <value> The upper Tan value. </value>
        public double? UpperTanValue
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the Tan value range. </summary>
        /// <value> The Tan value range. </value>
        public Std.Primitives.RangeR TanValueRange
        {
            get => this.LowerTanValue.HasValue && this.UpperTanValue.HasValue ? new Std.Primitives.RangeR( this.LowerTanValue.Value, this.UpperTanValue.Value ) : Std.Primitives.RangeR.Empty;

            set {
                this.LowerTanValue = value.Min;
                this.UpperTanValue = value.Max;
            }
        }

        /// <summary>   Gets or sets the lower annealing shift. </summary>
        /// <value> The lower annealing shift. </value>
        public double? LowerAnnealingShift
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the upper annealing. </summary>
        /// <value> The upper annealing shift. </value>
        public double? UpperAnnealingShift
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the annealing shift range. </summary>
        /// <value> The annealing shift. </value>
        public Std.Primitives.RangeR AnnealingShiftRange
        {
            get => this.LowerAnnealingShift.HasValue && this.UpperAnnealingShift.HasValue ? new Std.Primitives.RangeR( this.LowerAnnealingShift.Value, this.UpperAnnealingShift.Value ) : Std.Primitives.RangeR.Empty;

            set {
                this.LowerAnnealingShift = value.Min;
                this.UpperAnnealingShift = value.Max;
            }
        }

        #endregion

        #region " CUSTOM PROPERTIES "

        /// <summary>   Gets the specification limit. </summary>
        /// <value> The specification limit. </value>
        public double? SpecificationLimit => this.Tolerance;

        /// <summary>   Gets the decimal precision. </summary>
        /// <value> The decimal precision. </value>
        public int DecimalPrecision
        {
            get {
                double lowestTolerance = 0.00005d;
                double precision = 1d / Math.Max( lowestTolerance, this.SpecificationLimit.GetValueOrDefault( lowestTolerance ) );
                return precision <= 1d ? 0 : ( int ) Math.Floor( Math.Log10( precision ) );
            }
        }

        /// <summary>   Information describing the percent format. </summary>
        private System.Globalization.NumberFormatInfo _PercentFormatInfo;

        /// <summary>
        /// Number format informations for displaying values with the set specification limit.
        /// </summary>
        /// <value> Information describing the percent format. </value>
        public System.Globalization.NumberFormatInfo PercentFormatInfo
        {
            get {
                if ( this._PercentFormatInfo is null )
                {
                    this._PercentFormatInfo = new System.Globalization.NumberFormatInfo() { PercentDecimalDigits = DecimalPrecision };
                }

                return this._PercentFormatInfo;
            }
        }

        /// <summary>   Number of format informations. </summary>
        private System.Globalization.NumberFormatInfo _NumberFormatInfo;

        /// <summary>
        /// Number format informations for displaying values with the set specification limit.
        /// </summary>
        /// <value> The total number of format information. </value>
        public System.Globalization.NumberFormatInfo NumberFormatInfo
        {
            get {
                if ( this._NumberFormatInfo is null )
                {
                    this._NumberFormatInfo = new System.Globalization.NumberFormatInfo() { PercentDecimalDigits = DecimalPrecision };
                    this._NumberFormatInfo.NumberDecimalDigits = this._NumberFormatInfo.PercentDecimalDigits + 2;
                }

                return this._NumberFormatInfo;
            }
        }

        /// <summary>   Information describing the date time format. </summary>
        private System.Globalization.DateTimeFormatInfo _DateTimeFormatInfo;

        /// <summary>
        /// DateTime format informations for displaying values with the set specification limit.
        /// </summary>
        /// <value> Information describing the date time format. </value>
        public System.Globalization.DateTimeFormatInfo DateTimeFormatInfo
        {
            get {
                if ( this._DateTimeFormatInfo is null )
                {
                    this._DateTimeFormatInfo = new System.Globalization.DateTimeFormatInfo() { ShortTimePattern = "HH:MM:ss.fff" };
                }

                return this._DateTimeFormatInfo;
            }
        }

        #endregion

        #region " SHARED "

        /// <summary>   Select default nominal type. </summary>
        /// <remarks>   David, 2020-06-10. </remarks>
        /// <param name="elementType">  Type of the element. </param>
        /// <returns>   A <see cref="NomType"/>. </returns>
        public static NomType SelectDefaultNomType( ElementType elementType )
        {
            switch ( elementType )
            {
                case ElementType.CompoundResistor:
                    {
                        return NomType.Resistance;
                    }

                case ElementType.Equation:
                    {
                        return NomType.Equation;
                    }

                case ElementType.OpenCircuit:
                    {
                        return NomType.Resistance;
                    }

                case ElementType.Resistor:
                    {
                        return NomType.Resistance;
                    }

                case ElementType.ShortCircuit:
                    {
                        return NomType.Resistance;
                    }

                default:
                    {
                        return NomType.Resistance;
                    }
            }
        }

        #endregion

        #region " BIN "

        /// <summary>   Executes the bin operation. </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <param name="amount">   The amount. </param>
        /// <returns>   A ReadingBin. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0046:Convert to conditional expression", Justification = "<Pending>" )]
        public ReadingBin DoBin( double? amount )
        {
#if true
            if ( !amount.HasValue )
            { return ReadingBin.Invalid; }
            else if ( this.GuardedSpecificationRange.Contains( amount.Value ) )
            { return ReadingBin.Good; }
            else if ( !(this.OverflowRange.IsEmpty || this.OverflowRange.Contains( amount.Value )) )
            { return ReadingBin.Overflow; }
            else if ( this.GuardedSpecificationRange.Min > amount.Value )
            { return ReadingBin.Low; }
            else if ( this.GuardedSpecificationRange.Max < amount.Value )
            { return ReadingBin.High; }
            else
            { return ReadingBin.Nameless; }
#else
            return !amount.HasValue
                ? ReadingBin.Invalid
                : this.GuardedSpecificationRange.Contains( amount.Value )
                    ? ReadingBin.Good
                    : !( this.OverflowRange.IsEmpty || this.OverflowRange.Contains( amount.Value ) )
                                    ? ReadingBin.Overflow
                                    : this.GuardedSpecificationRange.Min > amount.Value
                                                    ? ReadingBin.Low
                                                    : this.GuardedSpecificationRange.Max < amount.Value ? ReadingBin.High : ReadingBin.Nameless;
#endif
        }

        #endregion

    }
}

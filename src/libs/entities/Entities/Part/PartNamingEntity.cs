using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Part Naming builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class PartNamingBuilder : OneToManyLabelBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( PartNamingNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = PartBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( PartNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = PartNamingTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( PartNamingTypeNub.Id );

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public override int LabelFieldSize { get; set; } = 63;

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( PartNamingEntity.PartAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( PartNamingEntity.PartNamingTypeId );

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public override string LabelFieldName { get; set; } = nameof( PartNamingEntity.Label );

        #region " SINGLETON "

        private static readonly Lazy<PartNamingBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static PartNamingBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="Dapper.Entities.PartEntity"/> Naming table
    /// <see cref="IOneToManyLabel">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "PartNaming" )]
    public class PartNamingNub : OneToManyLabelNub, IOneToManyLabel
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PartNamingNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyLabel CreateNew()
        {
            return new PartNamingNub();
        }
    }

    /// <summary>
    /// The <see cref="PartNamingEntity"/>. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class PartNamingEntity : EntityBase<IOneToManyLabel, PartNamingNub>, IOneToManyLabel
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PartNamingEntity() : this( new PartNamingNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PartNamingEntity( IOneToManyLabel value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public PartNamingEntity( IOneToManyLabel cache, IOneToManyLabel store ) : base( new PartNamingNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PartNamingEntity( PartNamingEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.PartNamingBuilder.TableName, nameof( IOneToManyLabel ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyLabel CreateNew()
        {
            return new PartNamingNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyLabel CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyLabelNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="PartNamingEntity"/> interface value. </param>
        public override void CopyFrom( IOneToManyLabel value )
        {
            OneToManyLabelNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached Naming, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.PartEntity"/>Naming interface. </param>
        public override void UpdateCache( IOneToManyLabel value )
        {
            // first make the copy to notify of any property change.
            OneToManyLabelNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int partAutoId, int partNamingTypeId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PartNamingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNamingNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartNamingNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partNamingTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<PartNamingNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int partAutoId, int partNamingTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, partAutoId, partNamingTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToManyLabel entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int partAutoId, int partNamingTypeId )
        {
            return connection.Delete( new PartNamingNub() { PrimaryId = partAutoId, SecondaryId = partNamingTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.PartEntity"/>Naming entities. </summary>
        /// <value> the <see cref="Dapper.Entities.PartEntity"/>Naming entities. </value>
        public IEnumerable<PartNamingEntity> PartNamings { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartNamingEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToManyLabel>() ) : Populate( connection.GetAll<PartNamingNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.PartNamings = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( PartNamingEntity.PartNamings ) );
            return this.PartNamings?.Any() == true ? this.PartNamings.Count() : 0;
        }

        /// <summary>   Count Part Namings. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.PartNamingBuilder.TableName}] WHERE {nameof( PartNamingNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartNamingEntity> FetchEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartNamingBuilder.TableName}] WHERE {nameof( PartNamingNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<PartNamingNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Part Namings by Part Auto Id. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<PartNamingNub> FetchNubs( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PartNamingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNamingEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PartNamingNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of Part Naming entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.PartEntity"/>Naming nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PartNamingEntity> Populate( IEnumerable<PartNamingNub> nubs )
        {
            var l = new List<PartNamingEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( PartNamingNub nub in nubs )
                    l.Add( new PartNamingEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Part Naming entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.PartEntity"/>Naming interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PartNamingEntity> Populate( IEnumerable<IOneToManyLabel> interfaces )
        {
            var l = new List<PartNamingEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new PartNamingNub();
                foreach ( IOneToManyLabel iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new PartNamingEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Part Namings by unique index; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId, int partNamingTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PartNamingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNamingNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartNamingNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partNamingTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches Part Namings by unique index; expected single or none. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="partNamingTypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<PartNamingNub> FetchEntities( System.Data.IDbConnection connection, int partAutoId, int partNamingTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PartNamingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNamingNub.PrimaryId )} = @primaryId", new { primaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartNamingNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partNamingTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PartNamingNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.PartEntity"/>Naming exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="partNamingtypeId"> Identifies the <see cref="PartNamingTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int partAutoId, int partNamingtypeId )
        {
            return 1 == CountEntities( connection, partAutoId, partNamingtypeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count Namings associated with this part. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of Namings. </returns>
        public int CountPartNamings( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PartNamingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNamingNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Part Naming Namings by Part auto id;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-07-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<PartNamingNub> FetchPartNamings( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PartNamingBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartNamingNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PartNamingNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.PartEntity"/> Entity. </summary>
        /// <value> the <see cref="Dapper.Entities.PartEntity"/> Entity. </value>
        public PartEntity PartEntity { get; private set; }

        /// <summary>   Fetches a Part Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchPartEntity( System.Data.IDbConnection connection )
        {
            this.PartEntity = new PartEntity();
            return this.PartEntity.FetchUsingKey( connection, this.PrimaryId );
        }

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.PartEntity"/> <see cref="PartNamingTypeEntity"/>.
        /// </summary>
        /// <value> the <see cref="Dapper.Entities.PartEntity"/> Naming type entity. </value>
        public PartNamingTypeEntity PartNamingTypeEntity { get; private set; }

        /// <summary>   Fetches a part naming type Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchPartNamingTypeEntity( System.Data.IDbConnection connection )
        {
            this.PartNamingTypeEntity = new PartNamingTypeEntity();
            return this.PartNamingTypeEntity.FetchUsingKey( connection, this.PrimaryId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartNamingEntity.PartAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.PartEntity"/> record.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.PartEntity"/> record. </value>
        public int PartAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartNamingEntity.PartNamingTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the identity (type) of the <see cref="Dapper.Entities.PartNamingEntity"/>.
        /// </summary>
        /// <value> Identifies the Part Naming type. </value>
        public int PartNamingTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.PartNamingEntity"/> label. </summary>
        /// <value> the <see cref="Dapper.Entities.PartNamingEntity"/> label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="PartNamingEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class PartNamingEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToManyLabel, PartNamingNub, PartNamingEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( PartNamingEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( PartNamingEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<PartNamingEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( PartNamingEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return PartNamingBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of <see cref="Dapper.Entities.PartEntity"/>-Unique <see cref="PartNamingEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class PartUniqueNamingEntityCollection : PartNamingEntityCollection, Std.Primitives.IGetterSetter
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public PartUniqueNamingEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
            this.PartNaming = new PartNaming() { GetterSetter = this };
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        public PartUniqueNamingEntityCollection( int partAutoId ) : this()
        {
            this.PartAutoId = partAutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( PartNamingEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.PartNamingTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.PartNamingTypeId );
            this.NotifyPropertyChanged( PartNamingTypeEntity.EntityLookupDictionary()[entity.PartNamingTypeId].Label );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains 'partNamingType' key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="partNamingTypeId"> Identifies the <see cref="Dapper.Entities.PartNamingTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int partNamingTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( partNamingTypeId );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>
        /// Gets the Text(String)-value for the given <see cref="PartNamingEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   A Nullable String. </returns>
        string IGetterSetter.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>   Set the naming value for the given <see cref="PartNamingEntity.Label"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        string IGetterSetter.Setter( string value, string name )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>
        /// Gets the Text(String)-value for the given <see cref="PartNamingEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable String. </returns>
        protected string Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>   Set the naming value for the given <see cref="PartNamingEntity.Label"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        private string SetterThis( string value, string name )
        {
            int key = this.ToKey( name );
            if ( !string.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>   Set the naming value for the given <see cref="PartNamingEntity.Label"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        protected string Setter( string value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets or sets the Part Naming. </summary>
        /// <value> The Part Naming. </value>
        public PartNaming PartNaming { get; private set; }

        #endregion

        #region " TRAIT SELECTION "

        /// <summary>   Converts a name to a key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return PartNamingTypeEntity.KeyLookupDictionary()[name];
        }

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="partNamingTypeId"> Identifies the <see cref="Dapper.Entities.PartNamingTypeEntity"/>. </param>
        /// <returns>   An PartNamingEntity. </returns>
        public PartNamingEntity Entity( int partNamingTypeId )
        {
            return this.ContainsKey( partNamingTypeId ) ? this[this._PrimaryKeyDictionary[partNamingTypeId]] : new PartNamingEntity();
        }

        /// <summary>   Gets the value of the given Naming type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="partNamingTypeId"> Identifies the <see cref="Dapper.Entities.PartNamingTypeEntity"/>. </param>
        /// <returns>   A String. </returns>
        public string Getter( int partNamingTypeId )
        {
            return this.ContainsKey( partNamingTypeId ) ? this[this._PrimaryKeyDictionary[partNamingTypeId]].Label : string.Empty;
        }

        /// <summary>   Set the specified Part value. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="partNamingTypeId"> Identifies the
        ///                                 <see cref="Dapper.Entities.PartNamingTypeEntity"/>. </param>
        /// <param name="value">            The value. </param>
        public void Setter( int partNamingTypeId, string value )
        {
            if ( this.ContainsKey( partNamingTypeId ) )
            {
                this[this._PrimaryKeyDictionary[partNamingTypeId]].Label = value;
            }
            else
            {
                this.Add( new PartNamingEntity() { PartAutoId = PartAutoId, PartNamingTypeId = partNamingTypeId, Label = value } );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.PartEntity"/>. </value>
        public int PartAutoId { get; private set; }

        #endregion

    }
}

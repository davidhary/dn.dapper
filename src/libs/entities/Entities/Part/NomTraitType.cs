using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Nominal Trait Type builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class NomTraitTypeBuilder : NominalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( NomTraitTypeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>
        /// Inserts or ignore the records as described by the <see cref="NomTraitType"/> enumeration type.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public override int InsertIgnoreDefaultRecords( System.Data.IDbConnection connection )
        {
            return this.InsertIgnore( connection, typeof( NomTraitType ), new int[] { ( int ) NomTraitType.None } );
        }

        #region " SINGLETON "

        private static readonly Lazy<NomTraitTypeBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static NomTraitTypeBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Nominal Trait Type table based on the <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "NomTraitType" )]
    public class NomTraitTypeNub : NominalNub, INominal
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public NomTraitTypeNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new NomTraitTypeNub();
        }
    }

    /// <summary>
    /// The <see cref="NomTraitTypeEntity"/> table based on the <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class NomTraitTypeEntity : EntityBase<INominal, NomTraitTypeNub>, INominal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public NomTraitTypeEntity() : this( new NomTraitTypeNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public NomTraitTypeEntity( INominal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public NomTraitTypeEntity( INominal cache, INominal store ) : base( new NomTraitTypeNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public NomTraitTypeEntity( NomTraitTypeEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.NomTraitTypeBuilder.TableName, nameof( INominal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new NomTraitTypeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override INominal CreateCopy()
        {
            var destination = this.CreateNew();
            NominalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( INominal value )
        {
            NominalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Nominal Trait Type interface. </param>
        public override void UpdateCache( INominal value )
        {
            // first make the copy to notify of any property change.
            NominalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The <see cref="NomTraitTypeEntity"/> primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<INominal>( key ) : connection.Get<NomTraitTypeNub>( key ) );
        }

        /// <summary>   Refetch; Fetches using the primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Label );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Nominal Trait Type label. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string label )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, label ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, INominal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Label ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new NomTraitTypeNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the Nominal Trait Type entities. </summary>
        /// <value> The <see cref="Dapper.Entities.NomTypeEntity"/> entities. </value>
        public static IEnumerable<NomTraitTypeEntity> NomTraitTypes { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<NomTraitTypeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<INominal>() ) : Populate( connection.GetAll<NomTraitTypeNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            NomTraitTypes = FetchAllEntities( connection, this.UsingNativeTracking );
            return NomTraitTypes?.Any() == true ? NomTraitTypes.Count() : 0;
        }

        /// <summary>   Populates a list of <see cref="NomTraitTypeEntity"/> entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The <see cref="NomTraitTypeNub"/>'s. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<NomTraitTypeEntity> Populate( IEnumerable<NomTraitTypeNub> nubs )
        {
            var l = new List<NomTraitTypeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( NomTraitTypeNub nub in nubs )
                    l.Add( new NomTraitTypeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Nominal Trait Type entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The Nominal Trait Type interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<NomTraitTypeEntity> Populate( IEnumerable<INominal> interfaces )
        {
            var l = new List<NomTraitTypeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new NomTraitTypeNub();
                foreach ( INominal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new NomTraitTypeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, NomTraitTypeEntity> _EntityLookupDictionary;

        /// <summary>   The entity lookup dictionary. </summary>
        /// <remarks>   David, 2020-06-12. </remarks>
        /// <returns>   A Dictionary(Of Integer, NomTraitTypeEntity) </returns>
        public static IDictionary<int, NomTraitTypeEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, NomTraitTypeEntity>();
                foreach ( NomTraitTypeEntity entity in NomTraitTypes )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Dictionary of key lookups. </summary>
        private static IDictionary<string, int> _KeyLookupDictionary;

        /// <summary>   The key lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of String, Integer) </returns>
        public static IDictionary<string, int> KeyLookupDictionary()
        {
            if ( !(_KeyLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _KeyLookupDictionary = new Dictionary<string, int>();
                foreach ( NomTraitTypeEntity entity in NomTraitTypes )
                    _KeyLookupDictionary.Add( entity.Label, entity.Id );
            }

            return _KeyLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (NomTraitTypes?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) &&
                   (_KeyLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Nominal Trait Type label. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.NomTraitTypeBuilder.TableName}] WHERE {nameof( NomTraitTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Nominal Trait Type label. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the nubs in this collection.
        /// </returns>
        public static IEnumerable<NomTraitTypeNub> FetchNubs( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.NomTraitTypeBuilder.TableName}] WHERE {nameof( NomTraitTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<NomTraitTypeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="NomTraitTypeEntity"/> exists. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Nominal Trait Type label. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string label )
        {
            return 1 == CountEntities( connection, label );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Nominal Trait Type . </summary>
        /// <value> Identifies the Nominal Trait Type . </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the Nominal Trait Type label. </summary>
        /// <value> The Nominal Trait Type label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the description of the Nominal Trait Type . </summary>
        /// <value> The Nominal Trait Type description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="nomTraitTypeId">   The entity unique key. </param>
        /// <returns>   A <see cref="NomTraitTypeEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, NomTraitTypeEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int nomTraitTypeId )
        {
            string activity = string.Empty;
            var entity = new NomTraitTypeEntity();
            try
            {
                activity = $"Fetching {nameof( NomTraitTypeEntity )} by {nameof( NomTraitTypeNub.Id )} of {nomTraitTypeId}";
                return entity.FetchUsingKey( connection, nomTraitTypeId ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Try fetch using unique index. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="nomTraitTypeLabel">    The Nominal Trait Type label. </param>
        /// <returns>   The (Success As Boolean, Details As String, Entity As NomTraitTypeEntity) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, NomTraitTypeEntity Entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, string nomTraitTypeLabel )
        {
            string activity = string.Empty;
            var entity = new NomTraitTypeEntity();
            try
            {
                activity = $"Fetching {nameof( NomTraitTypeEntity )} by {nameof( NomTraitTypeNub.Label )} of {nomTraitTypeLabel}";
                return entity.FetchUsingUniqueIndex( connection, nomTraitTypeLabel ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entities As IEnumerable(Of NomTraitTypeEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, IEnumerable<NomTraitTypeEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new NomTraitTypeEntity();
                activity = $"fetching all {nameof( NomTraitType )}s";
                int elementCount = entity.FetchAllEntities( connection );
                if ( elementCount != EntityLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( NomTraitTypeEntity.EntityLookupDictionary )} count must equal {nameof( NomTraitTypes )} count " );
                }
                else if ( elementCount != KeyLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( NomTraitTypeEntity.KeyLookupDictionary )} count must equal {nameof( NomTraitTypes )} count " );
                }

                return (true, string.Empty, NomTraitTypes);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<NomTraitTypeEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }

    /// <summary>   Values that represent Nominal Trait Types. </summary>
    /// <remarks>   David, 2020-10-02. </remarks>
    public enum NomTraitType
    {

        /// <summary>   An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary>   An enum constant representing the nominal value option. </summary>
        [System.ComponentModel.Description( "Nominal Value" )]
        NominalValue = 1,

        /// <summary>   An enum constant representing the Tolerance option. </summary>
        [System.ComponentModel.Description( "Tolerance" )]
        Tolerance = 2,

        /// <summary>   An enum constant representing the Tracking Tolerance option. </summary>
        [System.ComponentModel.Description( "Tracking Tolerance" )]
        TrackingTolerance = 3,

        /// <summary>   An enum constant representing the TCR option. </summary>
        [System.ComponentModel.Description( "Tcr" )]
        Tcr = 4,

        /// <summary>   An enum constant representing the Tracking Tcr option. </summary>
        [System.ComponentModel.Description( "Tracking Tcr" )]
        TrackingTcr = 5,

        /// <summary>   An enum constant representing the Guard Band Factor option. </summary>
        [System.ComponentModel.Description( "Guard Band Factor" )]
        GuardBandFactor = 6,

        /// <summary>   An enum constant representing the Sigma Performance Level option. </summary>
        [System.ComponentModel.Description( "Sigma Performance Level" )]
        SigmaPerformanceLevel = 7,

        /// <summary>   An enum constant representing the Lower Specification Limit option. </summary>
        [System.ComponentModel.Description( "Lower Specification Limit" )]
        LowerSpecificationLimit = 8,

        /// <summary>   An enum constant representing the Upper Specification Limit option. </summary>
        [System.ComponentModel.Description( "Upper Specification Limit" )]
        UpperSpecificationLimit = 9,

        /// <summary>   An enum constant representing the Dispersion option. </summary>
        [System.ComponentModel.Description( "Dispersion" )]
        Dispersion = 10,

        /// <summary>   An enum constant representing the Guard Band option. </summary>
        [System.ComponentModel.Description( "Guard Band" )]
        GuardBand = 11,

        /// <summary>   An enum constant representing the Guarded Specification Limit option. </summary>
        [System.ComponentModel.Description( "Guarded Specification Limit" )]
        GuardedSpecificationLimit = 12,

        /// <summary>   An enum constant representing the Lower Guarded Specification Limit option. </summary>
        [System.ComponentModel.Description( "Lower Guarded Specification Limit" )]
        LowerGuardedSpecificationLimit = 13,

        /// <summary>   An enum constant representing the Upper Guarded Specification Limit option. </summary>
        [System.ComponentModel.Description( "Upper Guarded Specification Limit" )]
        UpperGuardedSpecificationLimit = 14,

        /// <summary>   An enum constant representing the Lower TaN Value option. </summary>
        [System.ComponentModel.Description( "Lower TaN Value" )]
        LowerTanValue = 15,

        /// <summary>   An enum constant representing the Upper TaN Value option. </summary>
        [System.ComponentModel.Description( "Upper TaN Value" )]
        UpperTanValue = 16,

        /// <summary>   An enum constant representing the Lower Annealing Shift option. </summary>
        [System.ComponentModel.Description( "Lower Annealing Shift" )]
        LowerAnnealingShift = 17,

        /// <summary>   An enum constant representing the Upper Annealing Shift option. </summary>
        [System.ComponentModel.Description( "Upper Annealing Shift" )]
        UpperAnnealingShift = 18,

        /// <summary>   An enum constant representing the Lower Overflow Limit option. </summary>
        [System.ComponentModel.Description( "Lower Overflow Limit" )]
        LowerOverflowLimit = 19,

        /// <summary>   An enum constant representing the Upper Overflow Limit option. </summary>
        [System.ComponentModel.Description( "Upper Overflow Limit" )]
        UpperOverflowLimit = 20,

        /// <summary> If TCR has dual values, such as TCR Core 00,
        /// the span represents the average around the TCR value. </summary>
        [System.ComponentModel.Description( "Tcr Span" )]
        TcrSpan = 21,

        /// <summary> If Tracking has dual values, such as with TCR Core 00,
        /// the span represents the average around the Tracking TCR value. </summary>
        [System.ComponentModel.Description( "Tracking Tcr Span" )]
        TrackingTcrSpan = 22,

        /// <summary>   An enum constant representing the nominal power option. </summary>
        [System.ComponentModel.Description( "Nominal Power" )]
        NominalPower = 23
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Part-Lot builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class PartLotBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( PartLotNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = PartBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( PartNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = LotBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( LotNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( PartLotEntity.PartAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( PartLotEntity.LotAutoId );

        #region " SINGLETON "

        private static readonly Lazy<PartLotBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static PartLotBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Part Lot Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "PartLot" )]
    public class PartLotNub : OneToManyNub, IOneToMany
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PartLotNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new PartLotNub();
        }
    }

    /// <summary>   The Part-Lot Entity based on the <see cref="IOneToMany">interface</see>. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class PartLotEntity : EntityBase<IOneToMany, PartLotNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PartLotEntity() : this( new PartLotNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PartLotEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public PartLotEntity( IOneToMany cache, IOneToMany store ) : base( new PartLotNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PartLotEntity( PartLotEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.PartLotBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new PartLotNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Part-Lot interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int partAutoId, int lotAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, partAutoId, lotAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PartAutoId, this.LotAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PartAutoId, this.LotAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int partAutoId, int lotAutoId )
        {
            return this.FetchUsingKey( connection, partAutoId, lotAutoId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int partAutoId, int lotAutoId )
        {
            return connection.Delete( new PartLotNub() { PrimaryId = partAutoId, SecondaryId = lotAutoId } );
        }

        #endregion

        #region " OBTAIN "

        /// <summary>
        /// Tries to fetch an existing or insert a new <see cref="Dapper.Entities.LotEntity"/>  and fetch an
        /// existing or insert a new <see cref="PartLotEntity"/>. If the lot number is non-unique, a lot
        /// is inserted if this lot number is not associated with this part.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.PartEntity"/> exists for the specified
        /// <paramref name="partAutoId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainLot( System.Data.IDbConnection connection, int partAutoId, string lotNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( LotBuilder.Instance.UsingUniqueLabel( connection ) )
            {
                // if using a unique lot number, the lot number is unique across all parts
                this.LotEntity = new LotEntity() { LotNumber = lotNumber };
                if ( !this.LotEntity.Obtain( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( Dapper.Entities.LotEntity )} with {nameof( Dapper.Entities.LotEntity.LotNumber )} of {lotNumber}");
                }
            }
            else
            {
                // if not using a unique lot number, the lot number is unique for this part
                this.LotEntity = FetchLot( connection, partAutoId, lotNumber );
                if ( !this.LotEntity.IsClean() )
                {
                    this.LotEntity = new LotEntity() { LotNumber = lotNumber };
                    if ( !this.LotEntity.Insert( connection ) )
                    {
                        result = (false, $"Failed inserting {nameof( Dapper.Entities.LotEntity )} with {nameof( Dapper.Entities.LotEntity.LotNumber )} of {lotNumber}");
                    }
                }
            }

            if ( result.Success )
            {
                this.PrimaryId = partAutoId;
                this.SecondaryId = this.LotEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( PartLotEntity.LotEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( PartLotEntity )} with {nameof( this.PartAutoId )} of {partAutoId} and {nameof( this.LotAutoId )} of {this.LotEntity.AutoId}");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.PartEntity"/> and
        /// <see cref="Dapper.Entities.LotEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="PartLotEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int partAutoId, string lotNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.PartEntity is null || this.PartEntity.AutoId != partAutoId )
            {
                // make sure a part exists for the auto id.
                this.PartEntity = new PartEntity() { AutoId = partAutoId };
                if ( !this.PartEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.PartEntity )} with {nameof( Dapper.Entities.PartEntity.AutoId )} of {partAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainLot( connection, partAutoId, lotNumber );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( PartLotEntity.PartEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.PartEntity"/> and
        /// <see cref="Dapper.Entities.LotEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="PartLotEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, string lotNumber )
        {
            return this.TryObtain( connection, this.PartAutoId, lotNumber );
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.PartEntity"/> and
        /// <see cref="Dapper.Entities.LotEntity"/>
        /// entities and fetches an existing or inserts a new <see cref="PartLotEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-06-20. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partNumber">   The part number. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, string partNumber, string lotNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.PartEntity is null || !string.Equals( this.PartEntity.PartNumber, partNumber ) )
            {
                this.PartEntity = new PartEntity() { PartNumber = partNumber };
                if ( this.PartEntity.Obtain( connection ) )
                {
                    this.PartAutoId = this.PartEntity.AutoId;
                }
                else
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.PartEntity )} with {nameof( Dapper.Entities.PartEntity.AutoId )} of {this.PartAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainLot( connection, this.PartAutoId, lotNumber );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( PartLotEntity.PartEntity ) );
            return result;
        }

        /// <summary>
        /// Fetches an existing or inserts new <see cref="Dapper.Entities.PartEntity"/> and
        /// <see cref="Dapper.Entities.LotEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="PartLotEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool Obtain( System.Data.IDbConnection connection, int partAutoId, string lotNumber )
        {
            var (Success, Details) = this.TryObtain( connection, partAutoId, lotNumber );
            return !Success ? throw new InvalidOperationException( Details ) : Success;
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Part-Lot entities. </summary>
        /// <value> The Part-Lot entities. </value>
        public IEnumerable<PartLotEntity> PartLots { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartLotEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<PartLotNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.PartLots = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( PartLotEntity.PartLots ) );
            return this.PartLots?.Any() == true ? this.PartLots.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PartLotEntity> Populate( IEnumerable<PartLotNub> nubs )
        {
            var l = new List<PartLotEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( PartLotNub nub in nubs )
                    l.Add( new PartLotEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PartLotEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<PartLotEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new PartLotNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new PartLotEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Lot entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PartLotBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartLotNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartLotEntity> FetchEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartLotBuilder.TableName}] WHERE {nameof( PartLotNub.PrimaryId )} = @Id", new { Id = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<PartLotNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Lot. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>   The total number of entities by Lot. </returns>
        public static int CountEntitiesByLot( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PartLotBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartLotNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Lots in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Lots in this
        /// collection.
        /// </returns>
        public static IEnumerable<PartLotEntity> FetchEntitiesByLot( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartLotBuilder.TableName}] WHERE {nameof( PartLotNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<PartLotNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PartLotBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartLotNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartLotNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PartLotNub> FetchNubs( System.Data.IDbConnection connection, int partAutoId, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PartLotBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PartLotNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PartLotNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PartLotNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Part Lot exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int partAutoId, int lotAutoId )
        {
            return 1 == CountEntities( connection, partAutoId, lotAutoId );
        }

        #endregion

        #region " RELATIONS: PART "

        /// <summary>   Gets or sets the Part entity. </summary>
        /// <value> The Part entity. </value>
        public PartEntity PartEntity { get; private set; }

        /// <summary>   Fetches Part Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Part Entity. </returns>
        public PartEntity FetchPartEntity( System.Data.IDbConnection connection )
        {
            var entity = new PartEntity();
            _ = entity.FetchUsingKey( connection, this.PartAutoId );
            this.PartEntity = entity;
            return entity;
        }

        /// <summary>   Fetches Part Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Part Entity. </returns>
        [CLSCompliant( false )]
        public PartEntity FetchPartEntity( TransactedConnection connection )
        {
            var entity = new PartEntity();
            _ = entity.FetchUsingKey( connection, this.PartAutoId );
            this.PartEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Parts associated with the specified <paramref name="lotAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>   The total number of Parts. </returns>
        public static int CountParts( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.PartLotBuilder.TableName}] WHERE {nameof( PartLotNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Parts associated with the specified <paramref name="lotAutoId"/>; expected a
        /// single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Parts in this collection.
        /// </returns>
        public static IEnumerable<PartEntity> FetchParts( System.Data.IDbConnection connection, int lotAutoId )
        {
            IEnumerable<PartEntity> result;
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        result = FetchParts( new TransactedConnection( connection, transaction ), lotAutoId );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                result = FetchParts( transactedConnection, lotAutoId );
            }

            return result;
        }

        /// <summary>
        /// Fetches the Parts associated with the specified <paramref name="lotAutoId"/>; expected a
        /// single entity.
        /// </summary>
        /// <remarks>   David, 2020-07-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Parts in this collection.
        /// </returns>
        [CLSCompliant( false )]
        public static IEnumerable<PartEntity> FetchParts( TransactedConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartLotBuilder.TableName}] WHERE {nameof( PartLotNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<PartEntity>();
            foreach ( PartLotNub nub in connection.Query<PartLotNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new PartLotEntity( nub );
                l.Add( entity.FetchPartEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Parts associated with the specified <paramref name="lotAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteParts( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.PartLotBuilder.TableName}] WHERE {nameof( PartLotNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        #endregion

        #region " RELATIONS: LOTS "

        /// <summary>   Gets or sets the Lot entity. </summary>
        /// <value> The Lot entity. </value>
        public LotEntity LotEntity { get; private set; }

        /// <summary>   Fetches a Lot Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Lot Entity. </returns>
        public LotEntity FetchLotEntity( System.Data.IDbConnection connection )
        {
            var entity = new LotEntity();
            _ = entity.FetchUsingKey( connection, this.LotAutoId );
            this.LotEntity = entity;
            return entity;
        }

        /// <summary>   Gets or sets the <see cref="IEnumerable{LotEntity}"/>. </summary>
        /// <value> The <see cref="IEnumerable{LotEntity}"/>. </value>
        public IEnumerable<LotEntity> Lots { get; private set; }

        /// <summary>   Fetches the <see cref="IEnumerable{LotEntity}"/> in this collection. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Lots in this collection.
        /// </returns>
        public IEnumerable<LotEntity> FetchOrderedLots( System.Data.IDbConnection connection )
        {
            this.Lots = FetchOrderedLots( connection, this.PartAutoId );
            return this.Lots;
        }

        /// <summary>   Count lots. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The total number of lots. </returns>
        public static int CountLots( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.PartLotBuilder.TableName}] WHERE {nameof( PartLotNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Lots in this collection. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Lots in this collection.
        /// </returns>
        public static IEnumerable<LotEntity> FetchLots( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PartLotBuilder.TableName}] WHERE {nameof( PartLotNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<LotEntity>();
            foreach ( PartLotNub nub in connection.Query<PartLotNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new PartLotEntity( nub );
                l.Add( entity.FetchLotEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Lot related to the specified Part. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteLots( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.PartLotBuilder.TableName}] WHERE {nameof( PartLotNub.PrimaryId )} = @PrimaryId", new { PrimaryId = partAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.LotEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotLabel">     The lot label. </param>
        /// <returns>   The lot. </returns>
        public static LotEntity FetchLot( System.Data.IDbConnection connection, string selectQuery, int partAutoId, string lotLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { partAutoId, lotLabel } );
            // Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, new {partAutoId = partAutoId, .lotLabel = lotLabel})
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<LotNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new LotEntity() : new LotEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.LotEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="lotLabel">     The lot label. </param>
        /// <returns>   The lot. </returns>
        public static LotEntity FetchLot( System.Data.IDbConnection connection, int partAutoId, string lotLabel )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [PartLot] on [PartLot].SecondaryId = [Lot].AutoId where [PartLot].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{LotBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{LotBuilder.TableName}] Inner Join [{PartLotBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.PartLotBuilder.TableName}].{nameof( PartLotNub.SecondaryId )} = [{isr.Dapper.Entities.LotBuilder.TableName}].{nameof( LotNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.PartLotBuilder.TableName}].{nameof( PartLotNub.PrimaryId )} = @{nameof( partAutoId )} " );
            _ = queryBuilder.AppendLine( $"  AND [{isr.Dapper.Entities.LotBuilder.TableName}].{nameof( LotNub.Label )} = @{nameof( lotLabel )}); " );
            return FetchLot( connection, queryBuilder.ToString(), partAutoId, lotLabel );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.LotEntity"/>'s. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The lot. </returns>
        public static IEnumerable<LotEntity> FetchOrderedLots( System.Data.IDbConnection connection, string selectQuery, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { partAutoId } );
            // Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, new {partAutoId = partAutoId})
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return LotEntity.Populate( connection.Query<LotNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches <see cref="Dapper.Entities.LotEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The lot. </returns>
        public static IEnumerable<LotEntity> FetchOrderedLots( System.Data.IDbConnection connection, int partAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [PartLot] on [PartLot].SecondaryId = [Lot].AutoId where [PartLot].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{LotBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{LotBuilder.TableName}] Inner Join [{PartLotBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.PartLotBuilder.TableName}].{nameof( PartLotNub.SecondaryId )} = [{isr.Dapper.Entities.LotBuilder.TableName}].{nameof( LotNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.PartLotBuilder.TableName}].{nameof( PartLotNub.PrimaryId )} = @{nameof( partAutoId )}) " );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.LotBuilder.TableName}].{nameof( LotNub.Label )} ASC; " );
            return FetchOrderedLots( connection, queryBuilder.ToString(), partAutoId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartLotEntity.PartAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.PartEntity"/>. </value>
        public int PartAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PartLotEntity.LotAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.LotEntity"/> </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.LotEntity"/> </value>
        public int LotAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

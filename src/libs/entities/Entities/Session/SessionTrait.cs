using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{

    /// <summary>   The Session Attribute class holing the Session trait values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class SessionTrait : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSestter">    The getter setter. </param>
        public SessionTrait( Std.Primitives.IGetterSetter<int> getterSestter ) : base()
        {
            this.GetterSetter = getterSestter;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<int> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) The name. </param>
        /// <returns>   The trait value. </returns>
        protected int? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void Setter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }
        }

        #endregion

        #region " TRAITS "

        /// <summary>   Gets or sets the type of the session. </summary>
        /// <value> The type of the session. </value>
        public int SessionType
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the session number. </summary>
        /// <value> The session number. </value>
        public int SessionNumber
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets a value indicating whether the using guard. </summary>
        /// <value> True if using guard, false if not. </value>
        public bool UsingGuard
        {
            get => 1 == this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value ? 1 : 0 );
        }

        /// <summary>   Gets or sets the first serial number. </summary>
        /// <value> The first serial number. </value>
        public int FirstSerialNumber
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the last serial number. </summary>
        /// <value> The last serial number. </value>
        public int LastSerialNumber
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the board number. </summary>
        /// <value> The board number. </value>
        public int BoardNumber
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the board size. </summary>
        /// <value> The size of the board. </value>
        public int BoardSize
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the binning mode. </summary>
        /// <value> The binning mode. </value>
        public int BinningMode
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the serialization mode. </summary>
        /// <value> The serialization mode. </value>
        public int SerializationMode
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        #endregion

    }
}

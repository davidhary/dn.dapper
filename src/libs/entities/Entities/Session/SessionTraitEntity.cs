using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A session Natural (Integer) value trait builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class SessionTraitBuilder : OneToManyNaturalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( SessionTraitNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = SessionBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( SessionNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = SessionTraitTypeBuilder.TableName;

        /// <summary>    Gets or sets the name of the secondary table key. </summary>
        /// <value>  The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( SessionTraitTypeNub.Id );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( SessionTraitEntity.SessionAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( SessionTraitEntity.SessionTraitTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( SessionTraitEntity.Amount );

        #region " SINGLETON "

        private static readonly Lazy<SessionTraitBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SessionTraitBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="SessionTraitEntity"/> value table
    /// <see cref="IOneToManyNatural">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "SessionTrait" )]
    public class SessionTraitNub : OneToManyNaturalNub, IOneToManyNatural
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SessionTraitNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyNatural CreateNew()
        {
            return new SessionTraitNub();
        }
    }

    /// <summary>
    /// The <see cref="SessionTraitEntity"/> stores <see cref="SessionEntity"/> traits.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class SessionTraitEntity : EntityBase<IOneToManyNatural, SessionTraitNub>, IOneToManyNatural
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SessionTraitEntity() : this( new SessionTraitNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SessionTraitEntity( IOneToManyNatural value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public SessionTraitEntity( IOneToManyNatural cache, IOneToManyNatural store ) : base( new SessionTraitNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SessionTraitEntity( SessionTraitEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.SessionTraitBuilder.TableName, nameof( IOneToManyNatural ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyNatural CreateNew()
        {
            return new SessionTraitNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyNatural CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNaturalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="SessionTraitEntity"/> interface value. </param>
        public override void CopyFrom( IOneToManyNatural value )
        {
            OneToManyNaturalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    the <see cref="SessionEntity"/>Value interface. </param>
        public override void UpdateCache( IOneToManyNatural value )
        {
            // first make the copy to notify of any property change.
            OneToManyNaturalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="sessionTraitTypeId">   Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int sessionAutoId, int sessionTraitTypeId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SessionTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            _ = sqlBuilder.Where( $"{nameof( SessionTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<SessionTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="sessionTraitTypeId">   Identifies the <see cref="SessionEntity"/>
        ///                                     value type. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int sessionAutoId, int sessionTraitTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, sessionAutoId, sessionTraitTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToManyNatural entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="sessionTraitTypeId">   Type of the <see cref="SessionEntity"/> value. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int sessionAutoId, int sessionTraitTypeId )
        {
            return connection.Delete( new SessionTraitNub() { PrimaryId = sessionAutoId, SecondaryId = sessionTraitTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>
        /// Gets or sets the <see cref="SessionEntity"/> Natural (Integer) Value Trait entities.
        /// </summary>
        /// <value> the <see cref="SessionEntity"/> Attribute Natural (Integer) Value trait entities. </value>
        public IEnumerable<SessionTraitEntity> SessionTraits { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionTraitEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToManyNatural>() ) : Populate( connection.GetAll<SessionTraitNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.SessionTraits = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( SessionTraitEntity.SessionTraits ) );
            return this.SessionTraits?.Any() == true ? this.SessionTraits.Count() : 0;
        }

        /// <summary>   Count SessionValues. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.SessionTraitBuilder.TableName}] WHERE {nameof( SessionTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionTraitEntity> FetchEntities( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionTraitBuilder.TableName}] WHERE {nameof( SessionTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<SessionTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Session traits by session auto id. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<SessionTraitNub> FetchNubs( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SessionTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionTraitEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SessionTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of SessionValue entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> the <see cref="SessionEntity"/>Value nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SessionTraitEntity> Populate( IEnumerable<SessionTraitNub> nubs )
        {
            var l = new List<SessionTraitEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( SessionTraitNub nub in nubs )
                    l.Add( new SessionTraitEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of SessionValue entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   the <see cref="SessionEntity"/>Value interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SessionTraitEntity> Populate( IEnumerable<IOneToManyNatural> interfaces )
        {
            var l = new List<SessionTraitEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new SessionTraitNub();
                foreach ( IOneToManyNatural iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new SessionTraitEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Session traits buy unique index; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="sessionTraitTypeId">   Identifies the
        ///                                     <see cref="SessionTraitTypeEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int sessionAutoId, int sessionTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            _ = sqlBuilder.Where( $"{nameof( SessionTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches Session traits by unique index; expected single or none. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="sessionTraitTypeId">   Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<SessionTraitNub> FetchEntities( System.Data.IDbConnection connection, int sessionAutoId, int sessionTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SessionTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionTraitNub.PrimaryId )} = @primaryId", new { primaryId = sessionAutoId } );
            _ = sqlBuilder.Where( $"{nameof( SessionTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SessionTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="SessionEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="SessionEntity"/>. </param>
        /// <param name="sessionTraitTypeId">   Identifies the
        ///                                     <see cref="SessionTraitTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int sessionAutoId, int sessionTraitTypeId )
        {
            return 1 == CountEntities( connection, sessionAutoId, sessionTraitTypeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count traits associated with this <see cref="SessionEntity"/>. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of records. </returns>
        public int CountSessionTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches session Natural (Integer) traits for this <see cref="SessionEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<SessionTraitNub> FetchSessionTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SessionTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SessionTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="SessionEntity"/>. </summary>
        /// <value> the <see cref="SessionEntity"/>. </value>
        public SessionEntity SessionEntity { get; private set; }

        /// <summary>   Fetches a <see cref="SessionEntity"/>. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchSessionEntity( System.Data.IDbConnection connection )
        {
            this.SessionEntity = new SessionEntity();
            return this.SessionEntity.FetchUsingKey( connection, this.PrimaryId );
        }

        /// <summary>   Gets or sets the <see cref="SessionEntity"/>. </summary>
        /// <value> the <see cref="SessionEntity"/>. </value>
        public SessionTraitTypeEntity SessionNaturalTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="SessionEntity"/>. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchSessionNaturalTypeEntity( System.Data.IDbConnection connection )
        {
            this.SessionNaturalTypeEntity = new SessionTraitTypeEntity();
            return this.SessionNaturalTypeEntity.FetchUsingKey( connection, this.PrimaryId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( this.PrimaryId, value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( this.SessionAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="SessionEntity"/> record. </summary>
        /// <value> Identifies the <see cref="SessionEntity"/> record. </value>
        public int SessionAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( this.SecondaryId, value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( this.SessionTraitTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the identity of the <see cref="SessionEntity"/>
        /// <see cref="SessionTraitTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The identity of the <see cref="SessionEntity"/> <see cref="SessionTraitTypeEntity"/>.
        /// </value>
        public int SessionTraitTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }


        /// <summary>
        /// Gets or sets a trait value assigned to the <see cref="SessionEntity"/> for the
        /// specific <see cref="SessionTraitTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The trait value assigned to the <see cref="SessionEntity"/> for the specific
        /// <see cref="SessionTraitTypeEntity"/>.
        /// </value>
        public int Amount
        {
            get => this.ICache.Amount;

            set {
                if ( !object.Equals( this.Amount, value ) )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="SessionTraitEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class SessionTraitEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToManyNatural, SessionTraitNub, SessionTraitEntity>
    {
        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( SessionTraitEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( SessionTraitEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all Sessions from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<SessionTraitEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( SessionTraitEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return SessionTraitBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of <see cref="SessionEntity"/> unique <see cref="SessionTraitEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class SessionUniqueTraitEntityCollection : SessionTraitEntityCollection, Std.Primitives.IGetterSetter<int>
    {

        #region " CONSTRUTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public SessionUniqueTraitEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
            this.SessionTrait = new SessionTrait( this );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        public SessionUniqueTraitEntityCollection( int sessionAutoId ) : this()
        {
            this.SessionAutoId = sessionAutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( SessionTraitEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.SessionTraitTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.SessionTraitTypeId );
            this.NotifyPropertyChanged( SessionTraitTypeEntity.EntityLookupDictionary()[entity.SessionTraitTypeId].Label );
        }

        /// <summary>
        /// Removes all Sessions from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains 'NaturalType' key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="sessionTraitTypeId">   Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int sessionTraitTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( sessionTraitTypeId );
        }

        /// <summary>
        /// Converts a name to a key using the
        /// <see cref="SessionTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
        /// extend the element Nominal Traits beyond the values of the enumeration type that is used to
        /// populate this table.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return SessionTraitTypeEntity.KeyLookupDictionary()[name];
        }

        #endregion

        #region " GETTER/SETTER "

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   The trait value. </returns>
        int? IGetterSetter<int>.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Set the specified element value for the given <see cref="NomTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   An integer. </returns>
        int IGetterSetter<int>.Setter( int value, string name )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   The trait value. </returns>
        protected int? TraitValueGetter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( name );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void TraitValueSetter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue )
            {
                _ = this.Setter( value.Value, name );
            }
        }

        /// <summary>
        /// Gets the Nominal(Integer)-value for the given <see cref="NomTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable integer. </returns>
        protected int? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Set the value for the given <see cref="NomTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   An integer. </returns>
        private int SetterThis( int value, string name )
        {
            int key = this.ToKey( name );
            if ( !int.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>
        /// Set the value for the given <see cref="NomTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   An integer. </returns>
        protected int Setter( int value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets or sets the session attribute. </summary>
        /// <value> The session attribute. </value>
        public SessionTrait SessionTrait { get; private set; }

        #endregion

        #region " TRAIT SELECTION "

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="sessionTraitTypeId">   Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
        /// <returns>   An SessionNaturalEntity. </returns>
        public SessionTraitEntity Entity( int sessionTraitTypeId )
        {
            return this.ContainsKey( sessionTraitTypeId ) ? this[this._PrimaryKeyDictionary[sessionTraitTypeId]] : new SessionTraitEntity();
        }

        /// <summary>
        /// Gets the trait value for the given <see cref="SessionTraitTypeEntity.Id"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="sessionTraitTypeId">   Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
        /// <returns>   An Integer? </returns>
        public int? Getter( int sessionTraitTypeId )
        {
            return this.ContainsKey( sessionTraitTypeId ) ? this[this._PrimaryKeyDictionary[sessionTraitTypeId]].Amount : new int?();
        }

        /// <summary>
        /// Set the specified element value for the given <see cref="SessionTraitTypeEntity.Id"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="sessionTraitTypeId">   Identifies the <see cref="SessionTraitTypeEntity"/>. </param>
        /// <param name="value">                The value. </param>
        public void Setter( int sessionTraitTypeId, int value )
        {
            if ( this.ContainsKey( sessionTraitTypeId ) )
            {
                this[this._PrimaryKeyDictionary[sessionTraitTypeId]].Amount = value;
            }
            else
            {
                this.Add( new SessionTraitEntity() { SessionAutoId = SessionAutoId, SessionTraitTypeId = sessionTraitTypeId, Amount = value } );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="SessionEntity"/>. </summary>
        /// <value> Identifies the <see cref="SessionEntity"/>. </value>
        public int SessionAutoId { get; private set; }

        #endregion

    }
}


using System.Collections.Generic;

namespace isr.Dapper.Entities
{
    /// <summary>   A session entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class SessionEntity
    {

        /// <summary>   Gets or sets the <see cref="IEnumerable{OhmMeterSetupEntity}"/>. </summary>
        /// <value> The <see cref="IEnumerable{OhmMeterSetupEntity}"/>. </value>
        public OhmMeterSetupEntityCollection OhmMeterSetups { get; private set; } = new OhmMeterSetupEntityCollection();

        /// <summary>   Fetches ohm meter setups keyed by meter id. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <param name="connection">   The connection. </param>
        public void FetchOhmMeterSetups( System.Data.IDbConnection connection )
        {
            this.OhmMeterSetups.Clear();
            this.OhmMeterSetups.Populate( SessionOhmMeterSetupEntity.FetchOhmMeterSetups( connection, this.AutoId ) );
        }

        /// <summary>   Gets or sets the <see cref="IEnumerable{OhmMeterSetupEntity}"/> keyed by meter id. </summary>
        /// <value> The <see cref="IEnumerable{OhmMeterSetupEntity}"/> keyed by meter id. </value>
        public MeterIdOhmMeterSetupCollection MeterIdOhmMeterSetups { get; private set; } = new MeterIdOhmMeterSetupCollection();

        /// <summary>   Gets or sets the ohm meter setups keyed by meter number. </summary>
        /// <value> The ohm meter setups keyed by meter number. </value>
        public MeterNumberOhmMeterSetupCollection MeterNumberOhmMeterSetups { get; private set; } = new MeterNumberOhmMeterSetupCollection();

        /// <summary>   Fetches ohm meter setups. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <param name="connection">   The connection. </param>
        public void FetchMeterOhmMeterSetups( System.Data.IDbConnection connection )
        {
            this.MeterIdOhmMeterSetups.Clear();
            this.MeterNumberOhmMeterSetups.Clear();
            if ( !MeterEntity.IsEnumerated() )
                _ = MeterEntity.TryFetchAll( connection );
            this.FetchOhmMeterSetups( connection );
            var sessionOhmMeterSetup = new SessionOhmMeterSetupEntity() { SessionAutoId = AutoId };
            foreach ( SessionOhmMeterSetupNub entity in sessionOhmMeterSetup.FetchSessionOhmMeterSetups( connection ) )
            {
                this.MeterIdOhmMeterSetups.Add( entity.SecondaryId, this.OhmMeterSetups[entity.ForeignId] );
                this.MeterNumberOhmMeterSetups.Add( MeterEntity.EntityLookupDictionary()[entity.SecondaryId].MeterNumber, this.OhmMeterSetups[entity.ForeignId] );
            }
        }

        /// <summary>   Adds ohm meter setups. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="platformMeters">   The platform in meters. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) AddOhmMeterSetups( System.Data.IDbConnection connection, string toleranceCode, IEnumerable<PlatformMeter> platformMeters )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( !MeterEntity.IsEnumerated() )
            {
                var r = MeterEntity.TryFetchAll( connection );
                result = (r.Success, r.details);
            }

            if ( !result.Success )
                return result;
            this.FetchMeterOhmMeterSetups( connection );
            (bool Success, string Details, OhmMeterSetupEntity OhmMeterSetup) r2;
            foreach ( PlatformMeter platformMeter in platformMeters )
            {
                if ( !this.MeterIdOhmMeterSetups.ContainsKey( platformMeter.MeterId ) )
                {
                    double nominalValue = platformMeter.Elements().SelectNomTraitEntity( NomType.Resistance ).Amount;
                    r2 = SessionOhmMeterSetupEntity.AddSessionOhmMeterSetupEntity( connection, this.AutoId, toleranceCode, nominalValue, platformMeter.MeterId );
                    if ( r2.Success )
                    {
                        this.OhmMeterSetups.Add( r2.OhmMeterSetup );
                        this.MeterIdOhmMeterSetups.Add( platformMeter.MeterId, r2.OhmMeterSetup );
                        this.MeterNumberOhmMeterSetups.Add( platformMeter.MeterNumber, r2.OhmMeterSetup );
                    }
                    else
                    {
                        result = (r2.Success, r2.Details);
                    }
                }

                if ( !result.Success )
                {
                    break;
                }
            }

            return result;
        }
    }
}

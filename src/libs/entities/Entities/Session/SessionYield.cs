using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Session-Meter-Yield builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class SessionYieldBuilder : TwoToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( SessionYieldNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = SessionBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( SessionNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = MeterBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( MeterNub.Id );

        /// <summary>   Gets or sets the name of the Ternary table. </summary>
        /// <value> The name of the Ternary table. </value>
        public override string TernaryTableName { get; set; } = YieldTraitBuilder.TableName;

        /// <summary>   Gets or sets the name of the Ternary table key. </summary>
        /// <value> The name of the Ternary table key. </value>
        public override string TernaryTableKeyName { get; set; } = nameof( YieldTraitNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( SessionYieldEntity.SessionAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( SessionYieldEntity.MeterId );

        /// <summary>   Gets or sets the name of the ternary identifier field. </summary>
        /// <value> The name of the ternary identifier field. </value>
        public override string TernaryIdFieldName { get; set; } = nameof( SessionYieldEntity.YieldTraitAutoId );

        #region " SINGLETON "

        private static readonly Lazy<SessionYieldBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SessionYieldBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="SessionYieldNub"/> based on the <see cref="ITwoToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "SessionYield" )]
    public class SessionYieldNub : TwoToManyNub, ITwoToMany
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public SessionYieldNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ITwoToMany CreateNew()
        {
            return new SessionYieldNub();
        }
    }

    /// <summary>
    /// The <see cref="SessionYieldEntity"/> based on the
    /// <see cref="ITwoToMany">interface</see>.
    /// </summary>
    /// <remarks>   David, 2020-06-16. </remarks>
    public class SessionYieldEntity : EntityBase<ITwoToMany, SessionYieldNub>, ITwoToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public SessionYieldEntity() : this( new SessionYieldNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SessionYieldEntity( ITwoToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public SessionYieldEntity( ITwoToMany cache, ITwoToMany store ) : base( new SessionYieldNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SessionYieldEntity( SessionYieldEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.SessionYieldBuilder.TableName, nameof( ITwoToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ITwoToMany CreateNew()
        {
            return new SessionYieldNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ITwoToMany CreateCopy()
        {
            var destination = this.CreateNew();
            TwoToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( ITwoToMany value )
        {
            TwoToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Session-Element interface. </param>
        public override void UpdateCache( ITwoToMany value )
        {
            // first make the copy to notify of any property change.
            TwoToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches a <see cref="SessionYieldEntity"/> using the entity key. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">            Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">              Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int sessionAutoId, int meterId, int yieldAttributeAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, sessionAutoId, meterId, yieldAttributeAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Fetches a <see cref="SessionYieldEntity"/> using the entity key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.SessionAutoId, this.MeterId, this.YieldTraitAutoId );
        }

        /// <summary>
        /// Fetches an existing <see cref="SessionYieldEntity"/> using the entity unique index.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.SessionAutoId, this.MeterId, this.YieldTraitAutoId );
        }

        /// <summary>
        /// Fetches an existing <see cref="SessionYieldEntity"/> using the entity unique index.
        /// </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">            Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">              Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int sessionAutoId, int meterId, int yieldAttributeAutoId )
        {
            return this.FetchUsingKey( connection, sessionAutoId, meterId, yieldAttributeAutoId );
        }

        /// <summary>
        /// Tries to fetch an existing <see cref="YieldTraitEntity"/> associated with a
        /// <see cref="Dapper.Entities.SessionEntity"/> and <see cref="Dapper.Entities.ElementEntity"/>;
        /// otherwise, inserts a new <see cref="YieldTraitEntity"/>. Then tries to fetch an existing or
        /// insert a new
        /// <see cref="SessionYieldEntity"/>.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.SessionEntity"/> exists for the specified
        /// <paramref name="sessionAutoId"/>
        /// Assumes that a <see cref="Dapper.Entities.ElementEntity"/> exists for the specified
        /// <paramref name="meterId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitType"/>. </param>
        /// <param name="amount">           The amount. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainYieldAttribute( System.Data.IDbConnection connection, int sessionAutoId, int meterId, int yieldTraitTypeId, double amount )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            this.YieldTraitEntity = FetchYieldTrait( connection, sessionAutoId, meterId, yieldTraitTypeId );
            if ( !this.YieldTraitEntity.IsClean() )
            {
                this.YieldTraitEntity = new YieldTraitEntity() { YieldTraitTypeId = yieldTraitTypeId, Amount = amount };
                if ( !this.YieldTraitEntity.Insert( connection ) )
                {
                    result = (false, $"Failed inserting {nameof( Dapper.Entities.YieldTraitEntity )} with {nameof( Dapper.Entities.YieldTraitEntity.YieldTraitTypeId )} of {yieldTraitTypeId}");
                }
            }

            if ( result.Success )
            {
                this.PrimaryId = sessionAutoId;
                this.SecondaryId = meterId;
                this.TernaryId = this.YieldTraitEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( SessionYieldEntity.YieldTraitEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( SessionYieldEntity )} with {nameof( this.SessionAutoId )} of {sessionAutoId} and {nameof( this.MeterId )} of {meterId} and {nameof( this.YieldTraitAutoId )} of {this.YieldTraitEntity.AutoId}");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch existing <see cref="Dapper.Entities.SessionEntity"/> and
        /// <see cref="Dapper.Entities.ElementEntity"/> and fetches or inserts a new
        /// <see cref="YieldTraitEntity"/> and fetches an existing or inserts a new
        /// <see cref="SessionYieldEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitType"/>. </param>
        /// <param name="amount">           The amount. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int sessionAutoId, int meterId, int yieldTraitTypeId, double amount )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.SessionEntity is null || this.SessionEntity.AutoId != sessionAutoId )
            {
                // make sure we have a session associated with the session auto ID.
                this.SessionEntity = new SessionEntity() { AutoId = sessionAutoId };
                if ( !this.SessionEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.SessionEntity )} with {nameof( Dapper.Entities.SessionEntity.AutoId )} of {sessionAutoId}");
                }
            }

            if ( this.ElementEntity is null || this.ElementEntity.AutoId != meterId )
            {
                // make sure we have an element associated with the element auto ID.
                this.ElementEntity = new ElementEntity() { AutoId = meterId };
                if ( !this.ElementEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.ElementEntity )} with {nameof( Dapper.Entities.ElementEntity.AutoId )} of {meterId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainYieldAttribute( connection, sessionAutoId, meterId, yieldTraitTypeId, amount );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( SessionYieldEntity.SessionEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch existing <see cref="Dapper.Entities.SessionEntity"/> and
        /// <see cref="Dapper.Entities.ElementEntity"/> and fetches or inserts a new
        /// <see cref="YieldTraitEntity"/> and fetches an existing or inserts a new
        /// <see cref="SessionYieldEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitType"/>. </param>
        /// <param name="amount">           The amount. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int yieldTraitTypeId, double amount )
        {
            return this.TryObtain( connection, this.SessionAutoId, this.MeterId, yieldTraitTypeId, amount );
        }

        /// <summary>
        /// Tries to fetch existing <see cref="Dapper.Entities.SessionEntity"/> and
        /// <see cref="Dapper.Entities.ElementEntity"/> and fetches or inserts a new
        /// <see cref="YieldTraitEntity"/> and fetches an existing or inserts a new
        /// <see cref="SessionYieldEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitType"/>. </param>
        /// <param name="amount">           The amount. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool Obtain( System.Data.IDbConnection connection, int sessionAutoId, int meterId, int yieldTraitTypeId, double amount )
        {
            (bool Success, string Details) r;
            r = this.TryObtain( connection, sessionAutoId, meterId, yieldTraitTypeId, amount );
            return !r.Success ? throw new InvalidOperationException( r.Details ) : r.Success;
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ITwoToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId, entity.TernaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int sessionAutoId, int meterId )
        {
            return connection.Delete( new SessionYieldNub() { PrimaryId = sessionAutoId, SecondaryId = meterId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="SessionYieldEntity"/>'s. </summary>
        /// <value> The Session-Element entities. </value>
        public IEnumerable<SessionYieldEntity> SessionMeterYields { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionYieldEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ITwoToMany>() ) : Populate( connection.GetAll<SessionYieldNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.SessionMeterYields = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( SessionYieldEntity.SessionMeterYields ) );
            return this.SessionMeterYields?.Any() == true ? this.SessionMeterYields.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SessionYieldEntity> Populate( IEnumerable<SessionYieldNub> nubs )
        {
            var l = new List<SessionYieldEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( SessionYieldNub nub in nubs )
                    l.Add( new SessionYieldEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SessionYieldEntity> Populate( IEnumerable<ITwoToMany> interfaces )
        {
            var l = new List<SessionYieldEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new SessionYieldNub();
                foreach ( ITwoToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new SessionYieldEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>
        /// Count <see cref="SessionYieldEntity"/>'s by <see cref="Dapper.Entities.SessionEntity"/>; returns up to
        /// Element entities count.
        /// </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionYieldBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionYieldNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the <see cref="SessionYieldEntity"/>'s by <see cref="Dapper.Entities.SessionEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionYieldEntity> FetchEntities( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.PrimaryId )} = @sessionAutoId", new { sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<SessionYieldNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>
        /// Count <see cref="SessionYieldEntity"/>'s by <see cref="Dapper.Entities.ElementEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   The total number of entities by Element. </returns>
        public static int CountEntitiesByElement( System.Data.IDbConnection connection, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionYieldBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionYieldNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the <see cref="SessionYieldEntity"/>'s by Elements in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Elements in this
        /// collection.
        /// </returns>
        public static IEnumerable<SessionYieldEntity> FetchEntitiesByElement( System.Data.IDbConnection connection, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<SessionYieldNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count <see cref="SessionYieldEntity"/>'s; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">            Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">              Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int sessionAutoId, int meterId, int yieldAttributeAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionYieldBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionYieldNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            _ = sqlBuilder.Where( $"{nameof( SessionYieldNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            _ = sqlBuilder.Where( $"{nameof( SessionYieldNub.TernaryId )} = @TernaryId", new { TernaryId = yieldAttributeAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches <see cref="SessionYieldNub"/>'s; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">            Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">              Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionYieldNub> FetchNubs( System.Data.IDbConnection connection, int sessionAutoId, int meterId, int yieldAttributeAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SessionYieldBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionYieldNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            _ = sqlBuilder.Where( $"{nameof( SessionYieldNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            _ = sqlBuilder.Where( $"{nameof( SessionYieldNub.TernaryId )} = @TernaryId", new { TernaryId = yieldAttributeAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SessionYieldNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="SessionYieldEntity"/> exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="sessionAutoId">            Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">              Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldAttributeAutoId"> Identifies the <see cref="YieldTraitEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int sessionAutoId, int meterId, int yieldAttributeAutoId )
        {
            return 1 == CountEntities( connection, sessionAutoId, meterId, yieldAttributeAutoId );
        }

        #endregion

        #region " RELATIONS: LOT "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.SessionEntity"/>. </summary>
        /// <value> The Session entity. </value>
        public SessionEntity SessionEntity { get; private set; }

        /// <summary>   Fetches  <see cref="Dapper.Entities.SessionEntity"/>. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Session Entity. </returns>
        public SessionEntity FetchSessionEntity( System.Data.IDbConnection connection )
        {
            var entity = new SessionEntity();
            _ = entity.FetchUsingKey( connection, this.SessionAutoId );
            this.SessionEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count <see cref="Dapper.Entities.SessionEntity"/>'s associated with the specified
        /// <paramref name="meterId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   The total number of Sessions. </returns>
        public static int CountSessions( System.Data.IDbConnection connection, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the <see cref="Dapper.Entities.SessionEntity"/>'s associated with the specified
        /// <paramref name="meterId"/>; expected a single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Sessions in this collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchSessions( System.Data.IDbConnection connection, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ElementEntity>();
            foreach ( SessionYieldNub nub in connection.Query<SessionYieldNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new SessionYieldEntity( nub );
                l.Add( entity.FetchElementEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Sessions associated with the specified <paramref name="meterId"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteSessions( System.Data.IDbConnection connection, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        #endregion

        #region " RELATIONS: METER "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public MeterEntity MeterEntity { get; private set; }

        /// <summary>   Fetches a Meter Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The <see cref="Dapper.Entities.MeterEntity"/>. </returns>
        public MeterEntity FetchMeterEntity( System.Data.IDbConnection connection )
        {
            var entity = new MeterEntity();
            _ = entity.FetchUsingKey( connection, this.MeterId );
            this.MeterEntity = entity;
            return entity;
        }

        /// <summary>   Count Meters. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   The total number of Meters. </returns>
        public static int CountMeters( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Meters in this collection. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Meters in this collection.
        /// </returns>
        public static IEnumerable<MeterEntity> FetchMeters( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                 $"SELECT * FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<MeterEntity>();
            foreach ( SessionYieldNub nub in connection.Query<SessionYieldNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new SessionYieldEntity( nub );
                l.Add( entity.FetchMeterEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Meter related to the specified Session. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteMeters( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.MeterEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered Meters in this
        /// collection.
        /// </returns>
        public static IEnumerable<MeterEntity> FetchOrderedMeters( System.Data.IDbConnection connection, string selectQuery, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { SessionAutoId = sessionAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return MeterEntity.Populate( connection.Query<MeterNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.MeterEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered Meters in this
        /// collection.
        /// </returns>
        public static IEnumerable<MeterEntity> FetchOrderedMeters( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [SessionMeterYield] on [SessionMeterYield].SecondaryId = [Meter].AutoId where [SessionMeterYield].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{MeterBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{MeterBuilder.TableName}] Inner Join [{SessionYieldBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].{nameof( SessionYieldNub.SecondaryId )} = [{isr.Dapper.Entities.MeterBuilder.TableName}].{nameof( MeterNub.Id )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].{nameof( SessionYieldNub.PrimaryId )} = @{nameof( sessionAutoId )}" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.MeterBuilder.TableName}].{nameof( MeterNub.Id )} ASC; " );
            return FetchOrderedMeters( connection, queryBuilder.ToString(), sessionAutoId );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterNumber">  The <see cref="Dapper.Entities.MeterEntity"/> number. </param>
        /// <returns>   The Meter. </returns>
        public static MeterEntity FetchMeter( System.Data.IDbConnection connection, string selectQuery, int sessionAutoId, int meterNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { SessionAutoId = sessionAutoId, MeterNumber = meterNumber } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<MeterNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new MeterEntity() : new MeterEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <remarks>
        /// David, 2020-05-19.
        /// <code>
        /// SELECT [Meter].*
        /// FROM [Meter] Inner Join [SessionMeterYield]
        /// ON [SessionMeterYield].[SecondaryId] = [Meter].[Id]
        /// WHERE ([SessionMeterYield].[PrimaryId] = 8 AND [Meter].[Number] = 1)
        /// </code>
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterNumber"> The <see cref="Dapper.Entities.MeterEntity"/>number. </param>
        /// <returns>   The Meter. </returns>
        public static MeterEntity FetchMeter( System.Data.IDbConnection connection, int sessionAutoId, int meterNumber )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [SessionMeterYield] on [SessionMeterYield].SecondaryId = [Meter].AutoId where [SessionMeterYield].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{MeterBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{MeterBuilder.TableName}] Inner Join [{SessionYieldBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].[{nameof( SessionYieldNub.SecondaryId )}] = [{isr.Dapper.Entities.MeterBuilder.TableName}].[{nameof( MeterNub.Id )}]" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.SessionYieldBuilder.TableName}].[{nameof( SessionYieldNub.PrimaryId )}] = @{nameof( sessionAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.MeterBuilder.TableName}].[{nameof( MeterNub.Amount )}] = @{nameof( meterNumber )}); " );
            return FetchMeter( connection, queryBuilder.ToString(), sessionAutoId, meterNumber );
        }

        #endregion

        #region " RELATIONS: ELEMENT "

        /// <summary>   Gets or sets the Element entity. </summary>
        /// <value> The Element entity. </value>
        public ElementEntity ElementEntity { get; private set; }

        /// <summary>   Fetches a Element Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Element Entity. </returns>
        public ElementEntity FetchElementEntity( System.Data.IDbConnection connection )
        {
            var entity = new ElementEntity();
            _ = entity.FetchUsingKey( connection, this.MeterId );
            this.ElementEntity = entity;
            return entity;
        }

        /// <summary>   Count elements. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   The total number of elements. </returns>
        public static int CountElements( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Elements in this collection. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Elements in this collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchElements( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ElementEntity>();
            foreach ( SessionYieldNub nub in connection.Query<SessionYieldNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new SessionYieldEntity( nub );
                l.Add( entity.FetchElementEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Element related to the specified Session. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteElements( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.SessionYieldBuilder.TableName}] WHERE {nameof( SessionYieldNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.ElementEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered elements in this
        /// collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchOrderedElements( System.Data.IDbConnection connection, string selectQuery, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { SessionAutoId = sessionAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return ElementEntity.Populate( connection.Query<ElementNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.ElementEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered elements in this
        /// collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchOrderedElements( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [SessionMeterYield] on [SessionMeterYield].SecondaryId = [Element].AutoId where [SessionMeterYield].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{ElementBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{ElementBuilder.TableName}] Inner Join [{SessionYieldBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].{nameof( SessionYieldNub.SecondaryId )} = [{isr.Dapper.Entities.ElementBuilder.TableName}].{nameof( ElementNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].{nameof( SessionYieldNub.PrimaryId )} = @{nameof( sessionAutoId )}" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.ElementBuilder.TableName}].{nameof( ElementNub.Label )} ASC; " );
            return FetchOrderedElements( connection, queryBuilder.ToString(), sessionAutoId );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="elementLabel"> The <see cref="Dapper.Entities.ElementEntity"/>label. </param>
        /// <returns>   The element. </returns>
        public static ElementEntity FetchElement( System.Data.IDbConnection connection, string selectQuery, int sessionAutoId, string elementLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { SessionAutoId = sessionAutoId, ElementLabel = elementLabel } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<ElementNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new ElementEntity() : new ElementEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <remarks>
        /// David, 2020-05-19.
        /// <code>
        /// SELECT [Element].*
        /// FROM [Element] Inner Join [SessionMeterYield]
        /// ON [SessionMeterYield].[SecondaryId] = [Element].[AutoId]
        /// WHERE ([SessionMeterYield].[PrimaryId] = 8 AND [Element].[Label] = 'R1')
        /// </code>
        /// </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="elementLabel"> The <see cref="Dapper.Entities.ElementEntity"/>label. </param>
        /// <returns>   The element. </returns>
        public static ElementEntity FetchElement( System.Data.IDbConnection connection, int sessionAutoId, string elementLabel )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [SessionMeterYield] on [SessionMeterYield].SecondaryId = [Element].AutoId where [SessionMeterYield].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{ElementBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{ElementBuilder.TableName}] Inner Join [{SessionYieldBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].[{nameof( SessionYieldNub.SecondaryId )}] = [{isr.Dapper.Entities.ElementBuilder.TableName}].[{nameof( ElementNub.AutoId )}]" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.SessionYieldBuilder.TableName}].[{nameof( SessionYieldNub.PrimaryId )}] = @{nameof( sessionAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.ElementBuilder.TableName}].[{nameof( ElementNub.Label )}] = @{nameof( elementLabel )}); " );
            return FetchElement( connection, queryBuilder.ToString(), sessionAutoId, elementLabel );
        }

        #endregion

        #region " RELATIONS: YIELD TRAITS "

        /// <summary>   Gets or sets the <see cref="YieldTraitEntity"/>. </summary>
        /// <value> The <see cref="YieldTraitEntity"/>. </value>
        public YieldTraitEntity YieldTraitEntity { get; private set; }

        /// <summary>   Fetches a <see cref="YieldTraitEntity"/>. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The <see cref="YieldTraitEntity"/>. </returns>
        public YieldTraitEntity FetchYieldAttributeEntity( System.Data.IDbConnection connection )
        {
            this.YieldTraitEntity = FetchYieldTrait( connection, this.SessionAutoId, this.MeterId, this.YieldTraitAutoId );
            return this.YieldTraitEntity;
        }

        /// <summary>   Fetches the ordered <see cref="YieldTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered
        /// <see cref="YieldTraitEntity"/>'s in this collection.
        /// </returns>
        public static IEnumerable<YieldTraitEntity> FetchOrderedYieldTraits( System.Data.IDbConnection connection, string selectQuery, int sessionAutoId, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { sessionAutoId, meterId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return YieldTraitEntity.Populate( connection.Query<YieldTraitNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered <see cref="YieldTraitEntity"/>'s. </summary>
        /// <remarks>
        /// David, 2020-05-09.
        /// <code>
        /// SELECT [YieldTrait].*
        /// FROM [YieldTrait] Inner Join [SessionMeterYield]
        /// ON [SessionMeterYield].[TernaryId] = [YieldTrait].[AutoId]
        /// WHERE ([SessionMeterYield].[PrimaryId] = 8 AND [SessionMeterYield].[SecondaryId] = 1)
        /// </code>
        /// </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered
        /// <see cref="YieldTraitEntity"/>'s in this collection.
        /// </returns>
        public static IEnumerable<YieldTraitEntity> FetchOrderedYieldTraits( System.Data.IDbConnection connection, int sessionAutoId, int meterId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [SessionMeterYield] on [SessionMeterYield].SecondaryId = [YieldAttribute].AutoId where [SessionMeterYield].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{YieldTraitBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{YieldTraitBuilder.TableName}] Inner Join [{SessionYieldBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].{nameof( SessionYieldNub.TernaryId )} = [{isr.Dapper.Entities.YieldTraitBuilder.TableName}].{nameof( YieldTraitNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.SessionYieldBuilder.TableName}].[{nameof( SessionYieldNub.PrimaryId )}] = @{nameof( sessionAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].[{nameof( SessionYieldNub.SecondaryId )}] = @{nameof( meterId )}) " );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.YieldTraitBuilder.TableName}].{nameof( YieldTraitNub.ForeignId )} ASC; " );
            return FetchOrderedYieldTraits( connection, queryBuilder.ToString(), sessionAutoId, meterId );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.YieldTraitEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldTraitTypeId"> Identifies the
        ///                                 <see cref="YieldTraitTypeEntity"/>. </param>
        /// <returns>   The YieldAttribute. </returns>
        public static YieldTraitEntity FetchYieldTrait( System.Data.IDbConnection connection, string selectQuery, int sessionAutoId, int meterId, int yieldTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new {
                SessionAutoId = sessionAutoId,
                MeterId = meterId,
                YieldTraitTypeId = yieldTraitTypeId
            } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<YieldTraitNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new YieldTraitEntity() : new YieldTraitEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.YieldTraitEntity"/>. </summary>
        /// <remarks>
        /// David, 2020-06-16.
        /// <code>
        /// SELECT [YieldTrait].*
        /// FROM [YieldTrait] Inner Join [SessionMeterYield]
        /// ON [SessionMeterYield].[TernaryId] = [YieldTrait].[AutoId]
        /// WHERE ([SessionMeterYield].[PrimaryId] = 8 AND [YieldTrait].[FirstForeignId] = 1 AND [YieldTrait].[SecondForeignId] = 1 )
        /// </code>
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">        Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
        /// <returns>   The YieldAttribute. </returns>
        public static YieldTraitEntity FetchYieldTrait( System.Data.IDbConnection connection, int sessionAutoId, int meterId, int yieldTraitTypeId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"SELECT [{YieldTraitBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{YieldTraitBuilder.TableName}] Inner Join [{SessionYieldBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].[{nameof( SessionYieldNub.TernaryId )}] = [{isr.Dapper.Entities.YieldTraitBuilder.TableName}].[{nameof( YieldTraitNub.AutoId )}]" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.SessionYieldBuilder.TableName}].[{nameof( SessionYieldNub.PrimaryId )}] = @{nameof( sessionAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.SessionYieldBuilder.TableName}].[{nameof( SessionYieldNub.SecondaryId )}] = @{nameof( meterId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.YieldTraitBuilder.TableName}].[{nameof( YieldTraitNub.ForeignId )}] = @{nameof( yieldTraitTypeId )}); " );
            return FetchYieldTrait( connection, queryBuilder.ToString(), sessionAutoId, meterId, yieldTraitTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SessionYieldEntity.SessionAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.SessionEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </value>
        public int SessionAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SessionYieldEntity.MeterId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>   Gets or sets the id of the Ternary reference. </summary>
        /// <value> The identifier of Ternary reference. </value>
        public int TernaryId
        {
            get => this.ICache.TernaryId;

            set {
                if ( !object.Equals( ( object ) this.TernaryId, ( object ) value ) )
                {
                    this.ICache.TernaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SessionYieldEntity.YieldTraitAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="YieldTraitEntity"/>. </summary>
        /// <value> Identifies the <see cref="YieldTraitEntity"/>. </value>
        public int YieldTraitAutoId
        {
            get => this.TernaryId;

            set => this.TernaryId = value;
        }

        #endregion

    }

    /// <summary>
    /// Collection of unique <see cref="Dapper.Entities.ElementEntity"/> +
    /// <see cref="Dapper.Entities.MeterEntity"/> +
    /// <see cref="Dapper.Entities.NomTypeEntity"/> <see cref="YieldTraitEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class SessionYieldTraitEntityCollection : YieldTraitEntityCollection, Std.Primitives.IGetterSetter<double>
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="session">                  The <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meter">                The <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="certifiedQuantity">    (Optional) The certified quantity. </param>
        public SessionYieldTraitEntityCollection( SessionEntity session, MeterEntity meter, int certifiedQuantity = 0 ) : base()
        {
            this.SessionAutoId = session.AutoId;
            this.MeterId = meter.Id;
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
            this.YieldTrait = new YieldTrait( this, meter, certifiedQuantity );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-05-14. </remarks>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        public SessionYieldTraitEntityCollection( int sessionAutoId, int meterId ) : base()
        {
            this.SessionAutoId = sessionAutoId;
            this.MeterId = meterId;
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
            this.YieldTrait = new YieldTrait( this, meterId );
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( YieldTraitEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.YieldTraitTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.YieldTraitTypeId );
            this.NotifyPropertyChanged( YieldTraitTypeEntity.EntityLookupDictionary()[entity.YieldTraitTypeId].Label );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains a key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsAttribute( int yieldTraitTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( yieldTraitTypeId );
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.SessionEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </value>
        public int SessionAutoId { get; private set; }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId { get; private set; }


        #endregion

        #region " GETTER SETTER "

        /// <summary>
        /// Gets the value of the given <see cref="YieldTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        double? IGetterSetter<double>.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Sets value for the given <see cref="YieldTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        double IGetterSetter<double>.Setter( double value, string name )
        {
            return this.SetterThis( value, name );
        }


        /// <summary>
        /// Gets the value of the given <see cref="YieldTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        protected double? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Sets the value for the given <see cref="YieldTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        private double SetterThis( double value, string name )
        {
            int key = this.ToKey( name );
            if ( !int.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>
        /// Sets the value for the given <see cref="YieldTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        protected double Setter( double value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets or sets the Yield Trait. </summary>
        /// <value> The Yield Trait. </value>
        public YieldTrait YieldTrait { get; private set; }

        #endregion

        #region " TRAIT SELECTION "

        /// <summary>
        /// Converts a name to a key using the
        /// <see cref="YieldTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
        /// extend the element Yield Traits beyond the values of the enumeration type that is used to
        /// populate this table.
        /// </summary>
        /// <remarks>   David, 2020-05-11. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return YieldTraitTypeEntity.KeyLookupDictionary()[name];
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Double. </returns>
        public double? Setter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }

            return value;
        }

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
        /// <returns>   An elementReadingRealEntity. </returns>
        public YieldTraitEntity Entity( int yieldTraitTypeId )
        {
            return this.ContainsAttribute( yieldTraitTypeId ) ? this[this._PrimaryKeyDictionary[yieldTraitTypeId]] : new YieldTraitEntity();
        }

        /// <summary>   Gets the value of the given Trait Type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
        /// <returns>   A Double? </returns>
        public double? Getter( int yieldTraitTypeId )
        {
            return this.ContainsAttribute( yieldTraitTypeId ) ? this[this._PrimaryKeyDictionary[yieldTraitTypeId]].Amount : new double?();
        }

        /// <summary>   Set the specified element value. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="yieldTraitTypeId"> Identifies the <see cref="YieldTraitTypeEntity"/>. </param>
        /// <param name="value">            The value. </param>
        public void Setter( int yieldTraitTypeId, double value )
        {
            if ( this.ContainsAttribute( yieldTraitTypeId ) )
            {
                this[this._PrimaryKeyDictionary[yieldTraitTypeId]].Amount = value;
            }
            else
            {
                this.Add( new YieldTraitEntity() { YieldTraitTypeId = yieldTraitTypeId, Amount = value } );
            }
        }

        #endregion

        #region " UPSERT "

        /// <summary>   Inserts or updates all entities using the given connection. </summary>
        /// <remarks>
        /// David, 2020-05-13. Entities that failed to save are enumerated in
        /// <see cref="P:isr.Dapper.Entity.EntityKeyedCollection`4.UnsavedKeys" />
        /// </remarks>
        /// <param name="transcactedConnection">    The <see cref="T:Dapper.TransactedConnection" />. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [CLSCompliant( false )]
        public override bool Upsert( TransactedConnection transcactedConnection )
        {
            this.ClearUnsavedKeys();
            // dictionary is instantiated only after the collection has values
            if ( !this.Any() )
                return true;
            SessionYieldEntity sessionMeterYield;
            foreach ( KeyValuePair<DualKeySelector, YieldTraitEntity> keyValue in this.Dictionary )
            {
                if ( keyValue.Value.Upsert( transcactedConnection ) )
                {
                    sessionMeterYield = new SessionYieldEntity() {
                        SessionAutoId = SessionAutoId,
                        MeterId = MeterId,
                        YieldTraitAutoId = keyValue.Value.AutoId
                    };
                    if ( !sessionMeterYield.Obtain( transcactedConnection ) )
                    {
                        this.AddUnsavedKey( keyValue.Key );
                    }
                }
                else
                {
                    this.AddUnsavedKey( keyValue.Key );
                }
            }
            // success if no unsaved keys
            return !this.UnsavedKeys.Any();
        }

        #endregion

    }
}

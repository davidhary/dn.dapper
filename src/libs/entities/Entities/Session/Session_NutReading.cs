using System;
using System.Collections.Generic;

using isr.Std.Cartesian;
using isr.Dapper.Entities.RandomExtensions;

namespace isr.Dapper.Entities
{
    /// <summary>   A session entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class SessionEntity
    {

        /// <summary>
        /// Gets or sets the Gets or sets the collection of <see cref="NutReadingEntityCollection"/>
        /// initializing the histogram over 6 sigma with 0.2 sigma bin width.
        /// </summary>
        /// <value> The collection of nut reading entity collections. </value>
        public SessionNutReadingEntitiesCollection NutReadingEntitiesCollection { get; private set; }

        /// <summary>   Fetches nut readings. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The nut readings. </returns>
        public int FetchNutReadings( System.Data.IDbConnection connection )
        {
            return this.Uuts is object ? this.Uuts.FetchNutReadings( connection ) : 0;
        }

        /// <summary>   Initializes the nut reading entities collection. </summary>
        /// <remarks>   David, 2021-05-18. </remarks>
        public void InitializeNutReadingEntitiesCollection()
        {
            this.NutReadingEntitiesCollection = new SessionNutReadingEntitiesCollection();
        }

        /// <summary>   Initializes the collection of nut reading entity collection. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="platformMeters">   The platform in meters. </param>
        public void InitializeNutReadingEntitiesCollection( IEnumerable<PlatformMeter> platformMeters )
        {
            this.NutReadingEntitiesCollection = new SessionNutReadingEntitiesCollection( platformMeters );
            this.Uuts.InitializeNutReadingEntitiesCollection( platformMeters );
        }

        /// <summary>
        /// Populates the <see cref="Dapper.Entities.UutEntity"/> <see cref="UutEntity.NutReadingEntitiesCollection"/> for all
        /// <see cref="Dapper.Entities.NutEntity"/>'s.
        /// </summary>
        /// <remarks>   David, 2020-06-11. </remarks>
        /// <returns>   The number of <see cref="NutReadingEntity"/>'s . </returns>
        public int PopulateNutReadingEntitiesCollection()
        {
            this.NutReadingEntitiesCollection.Clear();
            foreach ( UutEntity uut in this.Uuts )
            {
                _ = uut.PopulateNutReadingEntitiesCollection();
                this.NutReadingEntitiesCollection.AddRange( uut.NutReadingEntitiesCollection );
            }

            return this.NutReadingEntitiesCollection.Count;
        }

    }

    /// <summary>   Collection of nut reading entities collections keyed by meters. </summary>
    /// <remarks>   David, 2020-06-27. </remarks>
    public class SessionNutReadingEntitiesCollection : NutReadingEntitiesCollection
    {

        #region " CONSTRUCTION "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-05-18. </remarks>
        public SessionNutReadingEntitiesCollection() : base()
        {
            this.YieldDictionary = new Dictionary<int, YieldTrait>();
            this.SampleDictionary = new Dictionary<MeterElementNomTypeSelector, SampleTrait>();
            this.HistogramDictionary = new Dictionary<MeterElementNomTypeSelector, HistogramBindingList>();
            this.UsingDeltaDictionary = new Dictionary<MeterElementNomTypeSelector, bool>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="platformMeters">   The platform in meters. </param>
        public SessionNutReadingEntitiesCollection( IEnumerable<PlatformMeter> platformMeters ) : base( platformMeters )
        {
            this.YieldDictionary = new Dictionary<int, YieldTrait>();
            this.SampleDictionary = new Dictionary<MeterElementNomTypeSelector, SampleTrait>();
            this.HistogramDictionary = new Dictionary<MeterElementNomTypeSelector, HistogramBindingList>();
            this.UsingDeltaDictionary = new Dictionary<MeterElementNomTypeSelector, bool>();
        }

        /// <summary>   Gets or sets the item count limit. </summary>
        /// <value> The item count limit. </value>
        public int ItemCountLimit { get; set; }

        /// <summary>   Adds a nut readings. </summary>
        /// <remarks>   David, 2020-07-16. </remarks>
        /// <param name="uut">  The <see cref="UutEntity"/>. </param>
        public void AddNutReadings( UutEntity uut )
        {
            foreach ( NutEntity nut in uut.Nuts )
            {
                this.Add( nut.NutReadings );
                this.AddSampleReading( uut, nut );
            }
            this.AddYieldReading( uut );
        }

        /// <summary>   Populates a yield sample readings. </summary>
        /// <remarks>   David, 2021-05-24. </remarks>
        /// <param name="uut">  The <see cref="UutEntity"/>. </param>
        public void PopulateYieldSampleReadings( UutEntity uut )
        {
            foreach ( NutEntity nut in uut.Nuts )
            {
                this.AddSampleReading( uut, nut );
            }
            this.AddYieldReading( uut );
        }

        /// <summary>
        /// Adds an item to the end of the collection and update the histogram with a reading.
        /// </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public override void Add( NutUniqueNutReadingEntityCollection item )
        {
            base.Add( item );
            _ = this.UpdateHistogram( item );
            if ( this.ItemCountLimit > 0 )
            {
                while ( this.Count > this.ItemCountLimit )
                    this.RemoveFirst();
            }
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-30. </remarks>
        public override void Clear()
        {
            base.Clear();
            foreach ( HistogramBindingList histogram in this.HistogramDictionary.Values )
                histogram.Initialize();

            if ( this.SampleDictionary is object )
            {
                foreach ( SampleTrait sampleTrait in this.SampleDictionary.Values )
                    sampleTrait.Clear();
            }

            if ( this.YieldDictionary is object )
            {
                foreach ( YieldTrait yieldTrait in this.YieldDictionary.Values )
                    yieldTrait.Clear();
            }
        }


        #endregion

        #region " SAMPLE "

        /// <summary>
        /// Adds a sample trait to the sample trait dictionary for specified session sample trait entity collection.
        /// </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public void AddSampleTrait( SessionSampleTraitEntityCollection item )
        {
            var selector = new MeterElementNomTypeSelector( item.SampleTrait );
            // It is assumed that this will be the one and only place where the traits will be added.
            this.SampleDictionary.Add( selector, item.SampleTrait );
            foreach ( var sampleTraitEntity in item )
            {
                sampleTraitEntity.NomTypeId = ( int ) item.SampleTrait.NomType;
            }
        }

        /// <summary>
        /// Adds a reading to the sample trait
        /// </summary>
        /// <remarks>   David, 2020-07-07. </remarks>
        /// <param name="uut">  The <see cref="UutEntity"/>. </param>
        /// <param name="nut">  The nut. </param>
        public void AddSampleReading( UutEntity uut, NutEntity nut )
        {
            var selector = new MeterElementNomTypeSelector( uut.Traits.UutTrait.MeterId, nut.ElementAutoId, nut.NomTypeId );
            if ( this.SampleDictionary.ContainsKey( selector ) )
            {
                var sampleTrait = this.SampleDictionary[selector];
                sampleTrait.AddItem( nut.NutReadings.MetaReading.Amount );
            }
        }

        /// <summary>   Evaluates this sample statistics. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="outliersCalculatorMethod"> The outliers calculator method. </param>
        public void Evaluate( OutliersCalculatorMethod outliersCalculatorMethod )
        {
            foreach ( SampleTrait sampleTrait in this.SampleDictionary.Values )
                sampleTrait.Evaluate( outliersCalculatorMethod );
        }

        /// <summary>   Gets or sets a dictionary of samples. </summary>
        /// <value> A dictionary of samples. </value>
        public IDictionary<MeterElementNomTypeSelector, SampleTrait> SampleDictionary { get; private set; }

        /// <summary>   Select <see cref="SampleTrait"/>. </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   A <see cref="SampleTrait"/>. </returns>
        public SampleTrait SelectSampleTrait( int meterId, int elementAutoId, int nomTypeId )
        {
            return this.SampleDictionary[new MeterElementNomTypeSelector( meterId, elementAutoId, nomTypeId )];
        }

        /// <summary>   Select <see cref="SampleTrait"/>. </summary>
        /// <remarks>   David, 2021-05-13. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   A <see cref="SampleTrait"/>. </returns>
        public SampleTrait SelectSampleTrait( PlatformMeter platformMeter )
        {
            return this.SampleDictionary[new MeterElementNomTypeSelector( platformMeter )];
        }

        #endregion

        #region " YIELD "

        /// <summary>
        /// Adds a yield trait to the <see cref="YieldDictionary"/>.
        /// </summary>
        /// <remarks>   David, 2020-06-30. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public void AddYieldTrait( SessionYieldTraitEntityCollection item )
        {
            // It is assumed that this will be the one and only place where the traits will be added.
            this.YieldDictionary.Add( item.YieldTrait.MeterId, item.YieldTrait );
        }

        /// <summary>
        /// Adds a yield reading to the yield trait for the specified UUT.
        /// </summary>
        /// <remarks>   David, 2020-07-07. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public void AddYieldReading( UutEntity item )
        {
            if ( ((this.YieldDictionary?.ContainsKey( item.Traits.UutTrait.MeterId )).GetValueOrDefault( false ) &&
                  (item.ProductSortEntity?.IsClean()).GetValueOrDefault( false )) == true )
            {
                var yieldTrait = this.YieldDictionary[item.Traits.UutTrait.MeterId];
                yieldTrait.AddItem( item );
            }
        }

        /// <summary>   Gets or sets a dictionary of Yields. </summary>
        /// <value> A dictionary of Yields. </value>
        public IDictionary<int, YieldTrait> YieldDictionary { get; private set; }

        /// <summary>   Select Yield Trait. </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="meterId">  Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   A <see cref="YieldTrait"/>. </returns>
        public YieldTrait SelectYieldTrait( int meterId )
        {
            return this.YieldDictionary[meterId];
        }

        /// <summary>   Select Yield Trait. </summary>
        /// <remarks>   David, 2021-05-13. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   A <see cref="YieldTrait"/>. </returns>
        public YieldTrait SelectYieldTrait( PlatformMeter platformMeter )
        {
            return this.YieldDictionary[platformMeter.MeterId];
        }

        #endregion

        #region " HISTOGRAM "

        /// <summary>
        /// Dictionary of <see cref="HistogramBindingList"/>.
        /// </summary>
        /// <value> A dictionary of histograms. </value>
        private IDictionary<MeterElementNomTypeSelector, HistogramBindingList> HistogramDictionary { get; set; }

        /// <summary>   Gets or sets a dictionary of using deltas. </summary>
        /// <value> A dictionary of using deltas. </value>
        private IDictionary<MeterElementNomTypeSelector, bool> UsingDeltaDictionary { get; set; }

        /// <summary>   Select <see cref="HistogramBindingList"/>. </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   An <see cref="HistogramBindingList"/> </returns>
        public HistogramBindingList SelectHistogram( int meterId, int elementAutoId, int nomTypeId )
        {
            return this.HistogramDictionary[new MeterElementNomTypeSelector( elementAutoId, meterId, nomTypeId )];
        }

        /// <summary>   Select <see cref="HistogramBindingList"/>. </summary>
        /// <remarks>   David, 2021-05-13. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   An <see cref="HistogramBindingList"/> </returns>
        public HistogramBindingList SelectHistogram( PlatformMeter platformMeter )
        {
            return this.HistogramDictionary[new MeterElementNomTypeSelector( platformMeter )];
        }

        /// <summary>   Uut count. </summary>
        /// <remarks>   David, 2021-04-26. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   An int. </returns>
        public int UutCount( PlatformMeter platformMeter )
        {
            return this.SelectHistogram( platformMeter ).TotalCount;
        }

        /// <summary>   Adds a histogram to the <see cref="HistogramDictionary"/>. </summary>
        /// <remarks>   David, 2021-05-13. <para>
        /// The histogram dictionary can be updated only if the histogram is empty. </para></remarks>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementId">        Identifies the element. </param>
        /// <param name="nominalType">      Type of the nominal. </param>
        /// <param name="lowerBound">       The lower bound. </param>
        /// <param name="upperBound">       The upper bound. </param>
        /// <param name="binCount">         The number of bins. </param>
        /// <param name="usingDeltaRange">  True to using delta range; otherwise, using nominal value
        ///                                 range. </param>
        public void AddHistogram( int meterId, int elementId, NomType nominalType, double lowerBound, double upperBound, int binCount, bool usingDeltaRange )
        {
            MeterElementNomTypeSelector selector = new( meterId, elementId, ( int ) nominalType );
            if ( this.HistogramDictionary.ContainsKey( selector ) && this.HistogramDictionary[selector].TotalCount > 0 )
            {
                // if we have an existing filled histogram, neither the histogram not the using delta dictionaries can be changed
            }
            else
            {
                HistogramBindingList histogram = new( lowerBound, upperBound, binCount );
                histogram.Initialize();
                if ( this.HistogramDictionary.ContainsKey( selector ) )
                    _ = this.HistogramDictionary.Remove( selector );
                if ( this.UsingDeltaDictionary.ContainsKey( selector ) )
                    _ = this.UsingDeltaDictionary.Remove( selector );
                this.HistogramDictionary.Add( selector, histogram );
                this.UsingDeltaDictionary.Add( selector, usingDeltaRange );
            }
        }

        /// <summary>   Adds a histogram to the <see cref="HistogramDictionary"/>. </summary>
        /// <remarks>   David, 2021-05-13. <para>
        /// The histogram dictionary can be updated only if the histogram is empty. </para></remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <param name="lowerBound">       The lower bound. </param>
        /// <param name="upperBound">       The upper bound. </param>
        /// <param name="binCount">         The number of bins. </param>
        /// <param name="usingDeltaRange">  True to using delta range; otherwise, using nominal value
        ///                                 range. </param>
        public void AddHistogram( PlatformMeter platformMeter, double lowerBound, double upperBound, int binCount, bool usingDeltaRange )
        {
            this.AddHistogram( platformMeter.MeterId, platformMeter.PrimaryElementId,
                                         platformMeter.PrimaryNominalType, lowerBound, upperBound, binCount, usingDeltaRange );
        }

        /// <summary>   Adds a histogram to the <see cref="HistogramDictionary"/>. </summary>
        /// <remarks>   David, 2021-05-13. <para>
        /// The histogram dictionary can be updated only if the histogram is empty. </para></remarks>
        /// <param name="part">             The <see cref="PartEntity"/>. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <param name="binCount">         The number of bins. </param>
        /// <param name="usingDeltaRange">  True to using delta range; otherwise, using nominal value range. </param>
        public void AddHistogram( PartEntity part, PlatformMeter platformMeter, int binCount, bool usingDeltaRange )
        {
            MeterElementNomTypeSelector selector = new( platformMeter );
            double lowerBound;
            double upperBound;
            if ( usingDeltaRange )
            {
                // double nominalValue = part.NomTraits[selector].NomTrait.NominalValue.Value;
                double tolerance = part.NomTraits[selector].NomTrait.Tolerance.Value;
                lowerBound = -tolerance;
                upperBound = tolerance;
            }
            else
            {
                lowerBound = part.NomTraits[selector].NomTrait.LowerSpecificationLimit.Value;
                upperBound = part.NomTraits[selector].NomTrait.UpperSpecificationLimit.Value;
            }
            this.AddHistogram( platformMeter.MeterId, platformMeter.PrimaryElementId,
                               platformMeter.PrimaryNominalType, lowerBound, upperBound, binCount, usingDeltaRange );
        }

        /// <summary>   Updates the histogram described by item. </summary>
        /// <remarks>   David, 2021-05-13. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        /// <returns>   An int. </returns>
        public int UpdateHistogram( NutUniqueNutReadingEntityCollection item )
        {
            var selector = new MeterElementNomTypeSelector( item.MetaReading );
            return this.UsingDeltaDictionary.ContainsKey( selector ) && this.HistogramDictionary.ContainsKey( selector )
                ? this.HistogramDictionary[selector].Update( this.UsingDeltaDictionary[selector] ? item.MetaReading.Delta : item.MetaReading.Amount )
                : 0;
        }

        /// <summary>   Simulates this the data. </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="part">         The <see cref="PartEntity"/>. </param>
        /// <param name="element">      The <see cref="ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="uut">          The <see cref="UutEntity"/>. </param>
        /// <param name="usingNominal"> True to using nominal. </param>
        public void Simulate( PartEntity part, ElementEntity element, int nomTypeId, UutEntity uut, bool usingNominal )
        {
            this.Clear();
            var rnd = new Random();
            double nominalValue = part.NomTraits[new MeterElementNomTypeSelector( uut.Traits.UutTrait.MeterId, element.AutoId, nomTypeId )].NomTrait.NominalValue.Value;
            double tolerance = part.NomTraits[new MeterElementNomTypeSelector( uut.Traits.UutTrait.MeterId, element.AutoId, nomTypeId )].NomTrait.Tolerance.Value;
            double lsl = part.NomTraits[new MeterElementNomTypeSelector( uut.Traits.UutTrait.MeterId, element.AutoId, nomTypeId )].NomTrait.LowerSpecificationLimit.Value;
            double usl = part.NomTraits[new MeterElementNomTypeSelector( uut.Traits.UutTrait.MeterId, element.AutoId, nomTypeId )].NomTrait.UpperSpecificationLimit.Value;
            double mean;
            double sigma;
            if ( usingNominal )
            {
                mean = nominalValue;
                sigma = nominalValue * tolerance / 3;
            }
            else
            {
                mean = 0.5 * (lsl + usl);
                sigma = (usl - lsl) / 6;
            }
            NutUniqueNutReadingEntityCollection item;
            for ( int i = 1; i <= 20000; i++ )
            {
                item = new NutUniqueNutReadingEntityCollection( 0, element, nomTypeId, uut );
                item.MetaReading.Amount = mean + sigma * rnd.NextNormal();
                this.Add( item );
            }
        }

        #endregion

    }
}

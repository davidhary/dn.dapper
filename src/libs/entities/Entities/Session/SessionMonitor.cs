using System.ComponentModel;
using System.Threading;
using System;

namespace isr.Dapper.Entities
{

    /// <summary>   A product test monitor. </summary>
    /// <remarks>
    /// David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class SessionMonitor : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        public SessionMonitor() : base()
        {
        }

        #endregion

        #region " SINGLETON "

        private static readonly Lazy<SessionMonitor> LazySessionMonitor = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SessionMonitor Instance => LazySessionMonitor.Value;

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        /// <summary>   Gets or sets the part. </summary>
        /// <value> The part. </value>
        public PartEntity Part { get; set; }

        /// <summary>   Gets or sets the lot. </summary>
        /// <value> The lot. </value>
        public LotEntity Lot { get; set; }

        /// <summary>   Gets or sets the session. </summary>
        /// <value> The session. </value>
        public SessionEntity Session { get; set; }

        /// <summary>   Gets or sets the product. </summary>
        /// <value> The product. </value>
        public ProductEntity Product { get; set; }

        /// <summary>   Gets or sets the Uut monitor. </summary>
        /// <value> The Uut monitor. </value>
        public UutMonitor UutMonitor { get; set; }

        /// <summary>   Gets or sets the elements. </summary>
        /// <value> The elements. </value>
        public ProductUniqueElementEntityCollection Elements { get; set; }

        /// <summary>   Gets or sets the uuts. </summary>
        /// <value> The uuts. </value>
        public SessionUniqueUutEntityCollection Uuts { get; set; }

        /// <summary>   Gets or sets the sorts. </summary>
        /// <value> The sorts. </value>
        public ProductUniqueProductSortEntityCollection Sorts { get; set; }

        /// <summary>   Gets or sets the Binning Entities. </summary>
        /// <value> The Binning Entities. </value>
        public ProductUniqueBinningEntityCollection Binnings { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   A Session entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class SessionEntity
    {

        /// <summary>   Gets or sets the <see cref="SessionYieldTraitEntityCollection"/>. </summary>
        /// <value> The <see cref="SessionYieldTraitEntityCollection"/>. </value>
        public SessionYieldTraitEntitiesCollection YieldTraits { get; private set; } = new SessionYieldTraitEntitiesCollection();

        /// <summary>   Gets or sets the <see cref="SessionSampleTraitEntityCollection"/>. </summary>
        /// <value> The <see cref="SessionSampleTraitEntityCollection"/>. </value>
        public SessionSampleTraitEntitiesCollection SampleTraits { get; private set; } = new();

#if false
        /// <summary>   Initializes the sample yield traits. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="meters">    The meters. </param>
        /// <param name="elements">  The elements. </param>
        public void InitializeSampleYieldTraits( IEnumerable<MeterEntity> meters, IEnumerable<ElementEntity> elements )
        {
            this.ClearSampleYieldTraits();
            SessionYieldTraitEntityCollection SessionYieldTraits;
            SessionSampleTraitEntityCollection SessionSampleTraits;
            foreach ( MeterEntity meter in meters )
            {
                SessionYieldTraits = new SessionYieldTraitEntityCollection( this, meter );
                this.YieldTraits.Add( SessionYieldTraits );
                this.NutReadingEntitiesCollection.AddYieldTrait( SessionYieldTraits );
                foreach ( ElementEntity element in elements )
                {
                    foreach ( NomTypeEntity nomType in element.NomTypes )
                    {
                        SessionSampleTraits = new SessionSampleTraitEntityCollection( this, element, nomType.Id, meter );
                        this.SampleTraits.Add( SessionSampleTraits );
                        this.NutReadingEntitiesCollection.AddSampleTrait( SessionSampleTraits );
                    }
                }
            }
        }

        /// <summary>   Initializes the sample yield traits. </summary>
        /// <remarks>   David, 2021-05-14. </remarks>
        /// <param name="element">          The element. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        public void InitializeSampleYieldTraits( ElementEntity element, PlatformMeter platformMeter )
        {
            SessionYieldTraitEntityCollection SessionYieldTraits;
            SessionSampleTraitEntityCollection SessionSampleTraits;
            SessionYieldTraits = new SessionYieldTraitEntityCollection( this.AutoId, platformMeter.MeterId );
            this.YieldTraits.Add( SessionYieldTraits );
            this.NutReadingEntitiesCollection.AddYieldTrait( SessionYieldTraits );
            SessionSampleTraits = new SessionSampleTraitEntityCollection( this, element, ( int ) platformMeter.DefaultNominalType, platformMeter.MeterId );
            this.SampleTraits.Add( SessionSampleTraits );
            this.NutReadingEntitiesCollection.AddSampleTrait( SessionSampleTraits );
        }

#endif

#if false
        public void FetchSampleTraits( System.Data.IDbConnection connection, IEnumerable<PlatformMeter> platformMeters, IEnumerable<ElementEntity> elements )
        {
            this.SampleTraits.Clear();
            foreach ( PlatformMeter platformMeter in platformMeters )
            {
                this.FetchSampleTraits( connection, platformMeter, elements );
            }
        }

        public void FetchSampleTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter , IEnumerable<ElementEntity> elements )
        {
            // TO_DO: Populate by Platform Meter using the platform meter elements and nominal types for that platform meter.
            if ( !SampleTraitTypeEntity.IsEnumerated() )
                _ = SampleTraitTypeEntity.TryFetchAll( connection );
            foreach ( ElementEntity element in elements )
            {
                foreach ( NomTypeEntity nomType in element.NomTypes )
                {
                    MeterElementNomTypeSelector selector = new ( platformMeter.MeterId, element.AutoId, nomType.Id );
                    if (!this.SampleTraits.Contains( selector ))
                    {
                        SessionSampleTraitEntityCollection SessionSampleTraits;
                        SessionSampleTraits = new SessionSampleTraitEntityCollection( this, element, nomType.Id, platformMeter.MeterId );
                        this.SampleTraits.Add( SessionSampleTraits );
                    }
                    this.FetchSampleTraits( connection, platformMeter, element.AutoId, nomType.Id );
                }
            }
        }
#endif

        /// <summary>   Fetches sample traits for all meters. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeters">   The platform in meters. </param>
        public void FetchSampleTraits( System.Data.IDbConnection connection, IEnumerable<PlatformMeter> platformMeters )
        {
            this.SampleTraits.Clear();
            foreach ( PlatformMeter platformMeter in platformMeters )
            {
                this.FetchSampleTraits( connection, platformMeter );
            }
        }

        /// <summary>   Fetches sample traits for the specified meter. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        private void FetchSampleTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter )
        {
            // TO_DO: Populate by Platform Meter using the platform meter elements and nominal types for that platform meter.
            if ( !SampleTraitTypeEntity.IsEnumerated() )
                _ = SampleTraitTypeEntity.TryFetchAll( connection );
            foreach ( ElementEntity element in platformMeter.Elements() )
            {
                foreach ( NomTypeEntity nomType in element.NomTypes )
                {
                    MeterElementNomTypeSelector selector = new( platformMeter.MeterId, element.AutoId, nomType.Id );
                    if ( !this.SampleTraits.Contains( selector ) )
                    {
                        SessionSampleTraitEntityCollection SessionSampleTraits;
                        SessionSampleTraits = new SessionSampleTraitEntityCollection( this, element, nomType.Id, platformMeter.MeterId );
                        this.SampleTraits.Add( SessionSampleTraits );
                    }
                    this.FetchSampleTraits( connection, platformMeter, element.AutoId, nomType.Id );
                }
            }
        }

        /// <summary>   Fetches sample traits for the specified meter, element, and nominal type. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <param name="elementAutoId">    Identifies the element automatic. </param>
        /// <param name="nomTypeId">        Identifies the nom type. </param>
        private void FetchSampleTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter, int elementAutoId, int nomTypeId )
        {
            if ( !SampleTraitTypeEntity.IsEnumerated() )
                _ = SampleTraitTypeEntity.TryFetchAll( connection );
            SessionSampleTraitEntityCollection SessionSampleTraits;
            SessionSampleTraits = this.SampleTraits[new MeterElementNomTypeSelector( platformMeter.MeterId, elementAutoId, nomTypeId )];
            SessionSampleTraits.Populate( SessionSampleEntity.FetchOrderedSampleTraits( connection, this.AutoId, platformMeter.MeterId, elementAutoId ) );
            // It is assumed that this will be the one and only place where the traits will be added.
            this.NutReadingEntitiesCollection.AddSampleTrait( SessionSampleTraits );
        }

        /// <summary>   Fetches sample traits for the specified meter. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        public void FetchPrimaryElementSampleTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter )
        {
            SessionSampleTraitEntityCollection sessionSampleTraits = this.FetchPlatformMeterSampleTraits( connection, platformMeter );
            // It is assumed that this will be the one and only place where the traits will be added.
            this.NutReadingEntitiesCollection.AddSampleTrait( sessionSampleTraits );
        }

        /// <summary>   Fetches platform meter sample traits. </summary>
        /// <remarks>
        /// David, 2021-06-05. <para>
        /// This function was added to allow fetching the sample trait without having to fetch the
        /// UUTs.</para>
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   The platform meter sample traits. </returns>
        public SessionSampleTraitEntityCollection FetchPlatformMeterSampleTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter )
        {
            if ( !SampleTraitTypeEntity.IsEnumerated() )
                _ = SampleTraitTypeEntity.TryFetchAll( connection );
            MeterElementNomTypeSelector selector = new( platformMeter );
            SessionSampleTraitEntityCollection sessionSampleTraits;
            if ( !this.SampleTraits.Contains( selector ) )
            {
                sessionSampleTraits = new SessionSampleTraitEntityCollection( this, platformMeter.PrimaryElement, ( int ) platformMeter.PrimaryNominalType, platformMeter.MeterId );
                this.SampleTraits.Add( sessionSampleTraits );
            }
            sessionSampleTraits = this.SampleTraits[new MeterElementNomTypeSelector( platformMeter )];
            sessionSampleTraits.Populate( SessionSampleEntity.FetchOrderedSampleTraits( connection, this.AutoId, platformMeter.MeterId, platformMeter.PrimaryElementId ) );
            return sessionSampleTraits;
        }

        /// <summary>   Fetches yield traits. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeters">   The platform in meters. </param>
        public void FetchYieldTraits( System.Data.IDbConnection connection, IEnumerable<PlatformMeter> platformMeters )
        {
            this.YieldTraits.Clear();
            foreach ( PlatformMeter platformMeter in platformMeters )
            {
                this.FetchYieldTraits( connection, platformMeter );
            }
        }

        /// <summary>   Fetches yield traits. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        private void FetchYieldTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter )
        {
            if ( !YieldTraitTypeEntity.IsEnumerated() )
                _ = YieldTraitTypeEntity.TryFetchAll( connection );
            SessionYieldTraitEntityCollection SessionYieldTraits = new( this.AutoId, platformMeter.MeterId );
            SessionYieldTraits.Populate( SessionYieldEntity.FetchOrderedYieldTraits( connection, this.AutoId, platformMeter.MeterId ) );
            this.YieldTraits.Add( SessionYieldTraits );
            // Note: It is assumed that this will be the one and only place where the traits will be added.
            this.NutReadingEntitiesCollection.AddYieldTrait( SessionYieldTraits );
        }


    }

    /// <summary>
    /// A collection of <see cref="SessionYieldTraitEntityCollection"/> keyed by meter id.
    /// </summary>
    /// <remarks>   David, 2020-07-03. </remarks>
    public class SessionYieldTraitEntitiesCollection : KeyedCollection<int, SessionYieldTraitEntityCollection>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override int GetKeyForItem( SessionYieldTraitEntityCollection item )
        {
            return item.MeterId;
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        [CLSCompliant( false )]
        public void Upsert( TransactedConnection connection )
        {
            foreach ( SessionYieldTraitEntityCollection item in this )
                _ = item.Upsert( connection );
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        public void Upsert( System.Data.IDbConnection connection )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.Upsert( new TransactedConnection( connection, transaction ) );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.Upsert( transactedConnection );
            }
        }
    }

    /// <summary>   A collection of <see cref="SessionSampleTraitEntityCollection"/> keyed by meter element and nominal type. </summary>
    /// <remarks>   David, 2020-07-03. </remarks>
    public class SessionSampleTraitEntitiesCollection : KeyedCollection<MeterElementNomTypeSelector, SessionSampleTraitEntityCollection>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override MeterElementNomTypeSelector GetKeyForItem( SessionSampleTraitEntityCollection item )
        {
            return item.SampleTrait.Selector;
        }

        /// <summary>   Select item. </summary>
        /// <remarks>   David, 2021-06-05. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   A SampleTraitEntityCollection. </returns>
        public SampleTraitEntityCollection SelectItem( PlatformMeter platformMeter )
        {
            return this[new MeterElementNomTypeSelector( platformMeter )];
        }

        /// <summary>   Select sample trait. </summary>
        /// <remarks>   David, 2021-06-05. </remarks>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   A SampleTrait. </returns>
        public SampleTrait SelectSampleTrait( PlatformMeter platformMeter )
        {
            return this[new MeterElementNomTypeSelector( platformMeter )].SampleTrait;
        }

        /// <summary>   Evaluates all elements. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="outliersCalculatorMethod"> The outliers calculator method. </param>
        public void Evaluate( OutliersCalculatorMethod outliersCalculatorMethod )
        {
            foreach ( SessionSampleTraitEntityCollection item in this )
            {
                item.SampleTrait.Evaluate( outliersCalculatorMethod );
                foreach ( var sampleTraitEntity in item )
                {
                    sampleTraitEntity.NomTypeId = ( int ) item.SampleTrait.NomType;
                }
            }
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        [CLSCompliant( false )]
        public void Upsert( TransactedConnection connection )
        {
            foreach ( SessionSampleTraitEntityCollection item in this )
                _ = item.Upsert( connection );
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        public void Upsert( System.Data.IDbConnection connection )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.Upsert( new TransactedConnection( connection, transaction ) );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.Upsert( transactedConnection );
            }
        }
    }
}

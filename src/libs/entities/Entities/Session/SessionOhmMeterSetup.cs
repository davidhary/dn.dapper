using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Session OhmMeterSetup builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class SessionOhmMeterSetupBuilder : OneToManyIdBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( SessionOhmMeterSetupNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = SessionBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( SessionNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = MeterBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( MeterNub.Id );

        /// <summary>   Gets or sets the name of the Foreign ID table. </summary>
        /// <value> The name of the Foreign ID table. </value>
        public override string ForeignIdTableName { get; set; } = OhmMeterSetupBuilder.TableName;

        /// <summary>   Gets or sets the name of the Foreign ID table key. </summary>
        /// <value> The name of the Foreign ID table key. </value>
        public override string ForeignIdTableKeyName { get; set; } = nameof( OhmMeterSetupNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( SessionOhmMeterSetupEntity.SessionAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( SessionOhmMeterSetupEntity.MeterId );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( SessionOhmMeterSetupEntity.OhmMeterSetupAutoId );

        #region " SINGLETON "

        private static readonly Lazy<SessionOhmMeterSetupBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SessionOhmMeterSetupBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="Dapper.Entities.SessionEntity"/> OhmMeterSetup table
    /// <see cref="IOneToManyId">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "SessionOhmMeterSetup" )]
    public class SessionOhmMeterSetupNub : OneToManyIdNub, IOneToManyId
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public SessionOhmMeterSetupNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyId CreateNew()
        {
            return new SessionOhmMeterSetupNub();
        }
    }

    /// <summary>
    /// The <see cref="Dapper.Entities.SessionEntity"/>OhmMeterSetup Entity. Implements access to the database
    /// using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class SessionOhmMeterSetupEntity : EntityBase<IOneToManyId, SessionOhmMeterSetupNub>, IOneToManyId
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public SessionOhmMeterSetupEntity() : this( new SessionOhmMeterSetupNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SessionOhmMeterSetupEntity( IOneToManyId value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public SessionOhmMeterSetupEntity( IOneToManyId cache, IOneToManyId store ) : base( new SessionOhmMeterSetupNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SessionOhmMeterSetupEntity( SessionOhmMeterSetupEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.SessionOhmMeterSetupBuilder.TableName, nameof( IOneToManyId ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyId CreateNew()
        {
            return new SessionOhmMeterSetupNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyId CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyIdNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="SessionOhmMeterSetupEntity"/> interface value. </param>
        public override void CopyFrom( IOneToManyId value )
        {
            OneToManyIdNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached OhmMeterSetup, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.SessionEntity"/>OhmMeterSetup interface. </param>
        public override void UpdateCache( IOneToManyId value )
        {
            // first make the copy to notify of any property change.
            OneToManyIdNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int sessionAutoId, int meterId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, sessionAutoId, meterId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int sessionAutoId, int meterId )
        {
            this.ClearStore();
            return this.FetchUsingKey( connection, sessionAutoId, meterId );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToManyId entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int sessionAutoId, int meterId )
        {
            return connection.Delete( new SessionOhmMeterSetupNub() { PrimaryId = sessionAutoId, SecondaryId = meterId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.SessionEntity"/>OhmMeterSetup entities. </summary>
        /// <value> The session ohm meter setups. </value>
        public IEnumerable<SessionOhmMeterSetupEntity> SessionOhmMeterSetups { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionOhmMeterSetupEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToManyId>() ) : Populate( connection.GetAll<SessionOhmMeterSetupNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.SessionOhmMeterSetups = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( SessionOhmMeterSetupEntity.SessionOhmMeterSetups ) );
            return this.SessionOhmMeterSetups?.Any() == true ? this.SessionOhmMeterSetups.Count() : 0;
        }

        /// <summary>   Count SessionOhmMeterSetups. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.SessionOhmMeterSetupBuilder.TableName}] WHERE {nameof( SessionOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SessionOhmMeterSetupEntity> FetchEntities( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SessionOhmMeterSetupBuilder.TableName}] WHERE {nameof( SessionOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<SessionOhmMeterSetupNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Session ohm meter setup by Session auto id; expected single or none. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<SessionOhmMeterSetupNub> FetchNubs( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionOhmMeterSetupEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SessionOhmMeterSetupNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of SessionOhmMeterSetup entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.SessionEntity"/>OhmMeterSetup nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SessionOhmMeterSetupEntity> Populate( IEnumerable<SessionOhmMeterSetupNub> nubs )
        {
            var l = new List<SessionOhmMeterSetupEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( SessionOhmMeterSetupNub nub in nubs )
                    l.Add( new SessionOhmMeterSetupEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of SessionOhmMeterSetup entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.SessionEntity"/>OhmMeterSetup interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SessionOhmMeterSetupEntity> Populate( IEnumerable<IOneToManyId> interfaces )
        {
            var l = new List<SessionOhmMeterSetupEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new SessionOhmMeterSetupNub();
                foreach ( IOneToManyId iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new SessionOhmMeterSetupEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Session Traits; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int sessionAutoId, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { PrimaryId = sessionAutoId } );
            _ = sqlBuilder.Where( $"{nameof( SessionOhmMeterSetupNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Session Ohm Meter Setup by Session Auto ID and Meter Id;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<SessionOhmMeterSetupNub> FetchNubs( System.Data.IDbConnection connection, int sessionAutoId, int meterId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionOhmMeterSetupNub.PrimaryId )} = @primaryId", new { primaryId = sessionAutoId } );
            _ = sqlBuilder.Where( $"{nameof( SessionOhmMeterSetupNub.SecondaryId )} = @SecondaryId", new { SecondaryId = meterId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SessionOhmMeterSetupNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.SessionEntity"/>OhmMeterSetup exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <param name="meterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int sessionAutoId, int meterId )
        {
            return 1 == CountEntities( connection, sessionAutoId, meterId );
        }

        #endregion

        #region " RELATIONS: LOT "

        /// <summary>   Count OhmMeterSetups associated with this Session. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of OhmMeterSetup OhmMeterSetups. </returns>
        public int CountSessionOhmMeterSetups( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Session OhmMeterSetup OhmMeterSetups by <see cref="SessionOhmMeterSetupEntity.SessionAutoId"/>;
        /// expected as manger as the number of meters.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<SessionOhmMeterSetupNub> FetchSessionOhmMeterSetups( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SessionOhmMeterSetupBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SessionOhmMeterSetupNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SessionOhmMeterSetupNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.SessionEntity"/> Entity. </summary>
        /// <value> The session entity. </value>
        public SessionEntity SessionEntity { get; private set; }

        /// <summary>   Fetches a Session Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchSessionEntity( System.Data.IDbConnection connection )
        {
            this.SessionEntity = new SessionEntity();
            return this.SessionEntity.FetchUsingKey( connection, this.SessionAutoId );
        }

        #endregion

        #region " RELATIONS: METER "

        /// <summary>   The meter entity. </summary>
        private MeterEntity _MeterEntity;

        /// <summary>   Gets the meter entity. </summary>
        /// <value> The meter entity. </value>
        public MeterEntity MeterEntity
        {
            get {
                if ( this._MeterEntity is null )
                {
                    this._MeterEntity = MeterEntity.EntityLookupDictionary()[this.MeterId];
                }

                return this._MeterEntity;
            }
        }

        /// <summary>   Fetches the meter. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchMeterEntity( System.Data.IDbConnection connection )
        {
            if ( !MeterEntity.IsEnumerated() )
                _ = MeterEntity.TryFetchAll( connection );
            this._MeterEntity = MeterEntity.EntityLookupDictionary()[this.MeterId];
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.MeterEntity ) );
            return this._MeterEntity is object;
        }

        #endregion

        #region " RELATIONS: METER MODEL "

        /// <summary>   The Meter Model entity. </summary>
        private MeterModelEntity _MeterModelEntity;

        /// <summary>   Gets the Meter Model entity. </summary>
        /// <value> The meter model entity. </value>
        public MeterModelEntity MeterModelEntity
        {
            get {
                if ( this._MeterModelEntity is null )
                {
                    this._MeterModelEntity = this.MeterEntity.MeterModelEntity;
                }

                return this._MeterModelEntity;
            }
        }

        /// <summary>   Fetches the Meter Model entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchMeterModel( System.Data.IDbConnection connection )
        {
            _ = this.FetchMeterEntity( connection );
            _ = this.MeterEntity.FetchMeterModel( connection );
            this._MeterModelEntity = this.MeterEntity.MeterModelEntity;
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.MeterModelEntity ) );
            return this.MeterModelEntity is object;
        }

        #endregion

        #region " RELATIONS: OHM METER SETUP "

        /// <summary>   Gets or sets the ohm meter setup entity. </summary>
        /// <value> The ohm meter setup entity. </value>
        public OhmMeterSetupEntity OhmMeterSetupEntity { get; private set; }

        /// <summary>   Fetches ohm meter setup entity. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool FetchOhmMeterSetupEntity( System.Data.IDbConnection connection )
        {
            this.OhmMeterSetupEntity = new OhmMeterSetupEntity();
            return this.OhmMeterSetupEntity.FetchUsingKey( connection, this.OhmMeterSetupAutoId );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.OhmMeterSetupEntity"/>'s. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   The OhmMeterSetup. </returns>
        public static IEnumerable<OhmMeterSetupEntity> FetchOhmMeterSetups( System.Data.IDbConnection connection, string selectQuery, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { sessionAutoId } );
            // Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, new {sessionAutoId = sessionAutoId})
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return OhmMeterSetupEntity.Populate( connection.Query<OhmMeterSetupNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches <see cref="Dapper.Entities.OhmMeterSetupEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        /// <returns>   The <see cref="IEnumerable{OhmMeterSetupEntity}"/>. </returns>
        public static IEnumerable<OhmMeterSetupEntity> FetchOhmMeterSetups( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [SessionOhmMeterSetup] on [SessionOhmMeterSetup].SecondaryId = [OhmMeterSetup].AutoId where [SessionOhmMeterSetup].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{OhmMeterSetupBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{OhmMeterSetupBuilder.TableName}] Inner Join [{SessionOhmMeterSetupBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.SessionOhmMeterSetupBuilder.TableName}].{nameof( SessionOhmMeterSetupNub.ForeignId )} = [{isr.Dapper.Entities.OhmMeterSetupBuilder.TableName}].{nameof( OhmMeterSetupNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.SessionOhmMeterSetupBuilder.TableName}].{nameof( SessionOhmMeterSetupNub.PrimaryId )} = @{nameof( sessionAutoId )}; " );
            return FetchOhmMeterSetups( connection, queryBuilder.ToString(), sessionAutoId );
        }

        /// <summary>   Adds a Session ohm meter setup entity. </summary>
        /// <remarks>   David, 2020-06-14. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">        Identifies the Session identity. </param>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="nominalValue">     The nominal value. </param>
        /// <param name="meterId">          The identifier of the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   An OhmMeterSetupEntity. </returns>
        public static (bool Success, string Details, OhmMeterSetupEntity OhmMeterSetup) AddSessionOhmMeterSetupEntity( System.Data.IDbConnection connection,
            int sessionAutoId, string toleranceCode, double nominalValue, int meterId )
        {
            var ohmMeterSettingLookup = new OhmMeterSettingLookupEntity();
            var ohmMeterSettingEntity = new OhmMeterSettingEntity();
            var ohmMeterSetupEntity = new OhmMeterSetupEntity();
            var sessionOhmMeterSetupEntity = new SessionOhmMeterSetupEntity();
            (bool Success, string Details, OhmMeterSetupEntity OhmMeterSetup) result = (true, string.Empty, ohmMeterSetupEntity);
            ohmMeterSettingLookup = OhmMeterSettingLookupEntity.SelectOhmMeterSettingLookupEntity( toleranceCode, meterId, nominalValue );
            (bool Success, string Details) r;
            r = ohmMeterSettingLookup.ValidateStoredEntity( $"Selected {nameof( OhmMeterSettingLookupEntity )} for tolerance '{toleranceCode}' and nominal value of {nominalValue}" );
            if ( !r.Success )
                result = (r.Success, r.Details, ohmMeterSetupEntity);
            if ( result.Success )
            {
                ohmMeterSettingEntity = new OhmMeterSettingEntity() { Id = ohmMeterSettingLookup.OhmMeterSettingId };
                _ = ohmMeterSettingEntity.FetchUsingUniqueIndex( connection );
                r = ohmMeterSettingEntity.ValidateStoredEntity( $"Fetched {nameof( OhmMeterSettingEntity )} with {nameof( OhmMeterSettingLookupEntity.OhmMeterSettingId )} of {ohmMeterSettingLookup.OhmMeterSettingId}" );
                if ( !r.Success )
                    result = (r.Success, r.Details, ohmMeterSetupEntity);
            }

            if ( result.Success )
            {
                ohmMeterSetupEntity.Copy( ohmMeterSettingEntity );
                _ = ohmMeterSetupEntity.Insert( connection );
                r = ohmMeterSetupEntity.ValidateStoredEntity( $"Inserted {nameof( Dapper.Entities.OhmMeterSetupEntity )}" );
                if ( !r.Success )
                    result = (r.Success, r.Details, ohmMeterSetupEntity);
            }

            if ( result.Success )
            {
                sessionOhmMeterSetupEntity = new SessionOhmMeterSetupEntity() {
                    SessionAutoId = sessionAutoId,
                    OhmMeterSetupAutoId = ohmMeterSetupEntity.AutoId,
                    MeterId = meterId
                };
                _ = sessionOhmMeterSetupEntity.Upsert( connection );
                r = sessionOhmMeterSetupEntity.ValidateStoredEntity( $"Upserted {nameof( SessionOhmMeterSetupEntity )} " );
                if ( !r.Success )
                    result = (r.Success, r.Details, ohmMeterSetupEntity);
            }

            return result;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SessionOhmMeterSetupEntity.SessionAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.SessionEntity"/> record.
        /// </summary>
        /// <value> The identifier of the session automatic. </value>
        public int SessionAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SessionOhmMeterSetupEntity.MeterId ) );
                }
            }
        }

        /// <summary>   Gets or sets the identity of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> The identifier of the meter. </value>
        public int MeterId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>   Gets or sets the Three-to-Many referenced table identifier. </summary>
        /// <value> The identifier of Three-to-Many referenced table. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SessionOhmMeterSetupEntity.OhmMeterSetupAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the identity of the <see cref="OhmMeterSetupEntity"/>. </summary>
        /// <value> The identifier of the ohm meter setup automatic. </value>
        public int OhmMeterSetupAutoId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of Session Trait Natural (Integer-value) entities. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class SessionOhmMeterSetupEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToManyId, SessionOhmMeterSetupNub, SessionOhmMeterSetupEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( SessionOhmMeterSetupEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( SessionOhmMeterSetupEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all Sessions from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<SessionOhmMeterSetupEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( SessionOhmMeterSetupEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return SessionOhmMeterSetupBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of Session-Unique Session Trait Natural (Integer-value) Entities unique to the Session.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class SessionUniqueOhmMeterSetupEntityCollection : SessionOhmMeterSetupEntityCollection
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public SessionUniqueOhmMeterSetupEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="sessionAutoId">    Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </param>
        public SessionUniqueOhmMeterSetupEntityCollection( int sessionAutoId ) : this()
        {
            this.SessionAutoId = sessionAutoId;
        }


        /// <summary>
        /// Dictionary of unique indexes for selecting the ohm meter setup by meter.
        /// </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>
        /// Dictionary of primary keys for selecting the ohm meter setup by meter id.
        /// </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( SessionOhmMeterSetupEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.MeterId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.MeterId );
        }

        /// <summary>
        /// Removes all Sessions from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains 'ohmMeterSetupAutoId' key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="meterId">  Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int meterId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( meterId );
        }

        /// <summary>
        /// Gets the setup for the specified <see cref="Dapper.Entities.MeterEntity"/> Id.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="meterId">  Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>   A SessionOhmMeterSetupEntity. </returns>
        public SessionOhmMeterSetupEntity OhmMeterSetup( int meterId )
        {
            return this.ContainsKey( meterId ) ? this[this._PrimaryKeyDictionary[meterId]] : new SessionOhmMeterSetupEntity();
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.SessionEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.SessionEntity"/>. </value>
        public int SessionAutoId { get; private set; }
    }
}

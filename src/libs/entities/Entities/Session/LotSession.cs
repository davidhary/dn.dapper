using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Lot-Session builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class LotSessionBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( LotSessionNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = LotBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( LotNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = SessionBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( SessionNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( LotSessionEntity.LotAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( LotSessionEntity.SessionAutoId );

        #region " SINGLETON "

        private static readonly Lazy<LotSessionBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static LotSessionBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Lot Session Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "LotSession" )]
    public class LotSessionNub : OneToManyNub, IOneToMany
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public LotSessionNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new LotSessionNub();
        }
    }

    /// <summary>   The Lot-Session Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class LotSessionEntity : EntityBase<IOneToMany, LotSessionNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public LotSessionEntity() : this( new LotSessionNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotSessionEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public LotSessionEntity( IOneToMany cache, IOneToMany store ) : base( new LotSessionNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotSessionEntity( LotSessionEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.LotSessionBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new LotSessionNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Lot-Session interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int lotAutoId, int sessionAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, lotAutoId, sessionAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.LotAutoId, this.SessionAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.LotAutoId, this.SessionAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int lotAutoId, int sessionAutoId )
        {
            return this.FetchUsingKey( connection, lotAutoId, sessionAutoId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int lotAutoId, int sessionAutoId )
        {
            return connection.Delete( new LotSessionNub() { PrimaryId = lotAutoId, SecondaryId = sessionAutoId } );
        }

        #endregion

        #region " OBTAIN "

        /// <summary>
        /// Tries to fetch an existing or insert a new <see cref="SessionEntity"/>  and fetch an existing
        /// or insert a new <see cref="LotSessionEntity"/>. If the session label is non-unique, a session
        /// is inserted if this session label is not associated with this lot.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.LotEntity"/> exists for the specified
        /// <paramref name="lotAutoId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionLabel"> The session label. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainSession( System.Data.IDbConnection connection, int lotAutoId, string sessionLabel )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( SessionBuilder.Instance.UsingUniqueLabel( connection ) )
            {
                // if using a unique session label, the session label is unique across all lots
                this.SessionEntity = new SessionEntity() { SessionLabel = sessionLabel };
                if ( !this.SessionEntity.Obtain( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( Dapper.Entities.SessionEntity )} with {nameof( Dapper.Entities.SessionEntity.SessionLabel )} of {sessionLabel}");
                }
            }
            else
            {
                // if not using a unique session label, the session label is unique for this lot
                this.SessionEntity = FetchSession( connection, lotAutoId, sessionLabel );
                if ( !this.SessionEntity.IsClean() )
                {
                    this.SessionEntity = new SessionEntity() { SessionLabel = sessionLabel };
                    if ( !this.SessionEntity.Insert( connection ) )
                    {
                        result = (false, $"Failed inserting {nameof( Dapper.Entities.SessionEntity )} with {nameof( Dapper.Entities.SessionEntity.SessionLabel )} of {sessionLabel}");
                    }
                }
            }

            if ( result.Success )
            {
                _ = this.SessionEntity.FetchTraits( connection );
                this.LotAutoId = lotAutoId;
                this.SessionAutoId = this.SessionEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( LotSessionEntity.SessionEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( LotSessionEntity )} with {nameof( this.LotAutoId )} of {lotAutoId} and {nameof( this.SessionAutoId )} of {this.SessionEntity.AutoId}");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.LotEntity"/> and
        /// <see cref="SessionEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="LotSessionEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionLabel"> The session label. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int lotAutoId, string sessionLabel )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.LotEntity is null || this.LotEntity.AutoId != lotAutoId )
            {
                // make sure a lot exists for the auto id.
                this.LotEntity = new LotEntity() { AutoId = lotAutoId };
                if ( !this.LotEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.LotEntity )} with {nameof( Dapper.Entities.LotEntity.AutoId )} of {lotAutoId}");
                }
            }

            if ( result.Success )
            {
                _ = this.LotEntity.FetchTraits( connection );
                result = this.TryObtainSession( connection, lotAutoId, sessionLabel );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( LotSessionEntity.LotEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.LotEntity"/> and
        /// <see cref="SessionEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="LotSessionEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionLabel"> The session label. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, string sessionLabel )
        {
            return this.TryObtain( connection, this.LotAutoId, sessionLabel );
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.LotEntity"/> and
        /// <see cref="SessionEntity"/>
        /// entities and fetches an existing or inserts a new <see cref="LotSessionEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-06-20. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <param name="sessionLabel"> The session label. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, string lotNumber, string sessionLabel )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.LotEntity is null || !string.Equals( this.LotEntity.LotNumber, lotNumber ) )
            {
                this.LotEntity = new LotEntity() { LotNumber = lotNumber };
                if ( this.LotEntity.Obtain( connection ) )
                {
                    this.LotAutoId = this.LotEntity.AutoId;
                }
                else
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.LotEntity )} with {nameof( Dapper.Entities.LotEntity.AutoId )} of {this.LotAutoId}");
                }
            }

            if ( result.Success )
            {
                _ = this.LotEntity.FetchTraits( connection );
                result = this.TryObtainSession( connection, this.LotAutoId, sessionLabel );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( LotSessionEntity.LotEntity ) );
            return result;
        }

        /// <summary>
        /// Fetches an existing or inserts new <see cref="Dapper.Entities.LotEntity"/> and
        /// <see cref="SessionEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="LotSessionEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionLabel"> The session label. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool Obtain( System.Data.IDbConnection connection, int lotAutoId, string sessionLabel )
        {
            var (Success, Details) = this.TryObtain( connection, lotAutoId, sessionLabel );
            return !Success ? throw new InvalidOperationException( Details ) : Success;
        }

        /// <summary>   Try obtain new session. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainNewSession( System.Data.IDbConnection connection, string lotNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.LotEntity is null || !string.Equals( this.LotEntity.LotNumber, lotNumber ) )
            {
                this.LotEntity = new LotEntity() { LotNumber = lotNumber };
                if ( this.LotEntity.Obtain( connection ) )
                {
                    this.LotAutoId = this.LotEntity.AutoId;
                }
                else
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.LotEntity )} with {nameof( Dapper.Entities.LotEntity.AutoId )} of {this.LotAutoId}");
                }
            }

            if ( result.Success )
            {
                var lastSession = FetchOrderedSessions( connection, this.LotAutoId ).LastOrDefault();
                int lastSessionNumber = (lastSession?.IsClean()).GetValueOrDefault( false ) ? int.Parse( lastSession.SessionNumber ) : 0;
                string lastSessionLabel = SessionEntity.BuildSessionIdentity( this.LotEntity.LotNumber, lastSessionNumber );
                result = this.TryObtainSession( connection, this.LotAutoId, SessionEntity.BuildSessionIdentity( this.LotEntity.LotNumber, lastSessionLabel ) );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( LotSessionEntity.LotEntity ) );
            return result;
        }

        /// <summary>   Obtain new session. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotNumber">    The lot number. </param>
        public void ObtainNewSession( System.Data.IDbConnection connection, string lotNumber )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        var (Success, Details) = this.TryObtainNewSession( new TransactedConnection( connection, transaction ), lotNumber );
                        if ( !Success )
                            throw new InvalidOperationException( Details );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                var (Success, Details) = this.TryObtainNewSession( transactedConnection, lotNumber );
                if ( !Success )
                    throw new InvalidOperationException( Details );
            }
        }

        /// <summary>   Try obtain the last or a new session. </summary>
        /// <remarks>   David, 2020-06-25. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainLastSession( System.Data.IDbConnection connection, string lotNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.LotEntity is null || !string.Equals( this.LotEntity.LotNumber, lotNumber ) )
            {
                this.LotEntity = new LotEntity() { LotNumber = lotNumber };
                if ( !this.LotEntity.Obtain( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( Dapper.Entities.LotEntity )} with {nameof( Dapper.Entities.LotEntity.AutoId )} of {this.LotAutoId}");
                }
            }

            var lastSession = new SessionEntity();
            if ( result.Success )
            {
                this.LotAutoId = this.LotEntity.AutoId;
                lastSession = FetchOrderedSessions( connection, this.LotAutoId ).LastOrDefault();
                if ( (lastSession?.IsClean()).GetValueOrDefault( false ) )
                {
                    this.SessionEntity = lastSession;
                }
                else
                {
                    string lastSessionLabel = SessionEntity.BuildSessionIdentity( this.LotEntity.LotNumber, 0 );
                    result = this.TryObtainSession( connection, this.LotAutoId, SessionEntity.BuildSessionIdentity( this.LotEntity.LotNumber, lastSessionLabel ) );
                }
            }

            if ( result.Success )
            {
                this.LotAutoId = this.LotEntity.AutoId;
                this.SessionAutoId = this.SessionEntity.AutoId;
                if ( !this.Obtain( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( LotSessionEntity )} with {nameof( this.LotAutoId )} of {this.LotAutoId} and {nameof( this.SessionAutoId )} of {this.SessionAutoId}");
                }
            }

            if ( result.Success )
            {
                this.NotifyPropertyChanged( nameof( LotSessionEntity.SessionEntity ) );
                this.NotifyPropertyChanged( nameof( LotSessionEntity.LotEntity ) );
            }

            return result;
        }

        /// <summary>   Obtain the last or a new session. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotNumber">    The lot number. </param>
        public void ObtainLastSession( System.Data.IDbConnection connection, string lotNumber )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        var (Success, Details) = this.TryObtainLastSession( new TransactedConnection( connection, transaction ), lotNumber );
                        if ( !Success )
                            throw new InvalidOperationException( Details );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                var (Success, Details) = this.TryObtainLastSession( transactedConnection, lotNumber );
                if ( !Success )
                    throw new InvalidOperationException( Details );
            }
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Lot-Session entities. </summary>
        /// <value> The Lot-Session entities. </value>
        public IEnumerable<LotSessionEntity> LotSessions { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotSessionEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<LotSessionNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.LotSessions = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( LotSessionEntity.LotSessions ) );
            return this.LotSessions?.Any() == true ? this.LotSessions.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotSessionEntity> Populate( IEnumerable<LotSessionNub> nubs )
        {
            var l = new List<LotSessionEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( LotSessionNub nub in nubs )
                    l.Add( new LotSessionEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotSessionEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<LotSessionEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new LotSessionNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new LotSessionEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to session entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotSessionBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotSessionNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotSessionEntity> FetchEntities( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotSessionBuilder.TableName}] WHERE {nameof( LotSessionNub.PrimaryId )} = @Id", new { Id = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<LotSessionNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by session. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   The total number of entities by session. </returns>
        public static int CountEntitiesBySession( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotSessionBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotSessionNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by sessions in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by sessions in this
        /// collection.
        /// </returns>
        public static IEnumerable<LotSessionEntity> FetchEntitiesBySession( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotSessionBuilder.TableName}] WHERE {nameof( LotSessionNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<LotSessionNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int lotAutoId, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotSessionBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotSessionNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotSessionNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotSessionNub> FetchNubs( System.Data.IDbConnection connection, int lotAutoId, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotSessionBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotSessionNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotSessionNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LotSessionNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Lot Session exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int lotAutoId, int sessionAutoId )
        {
            return 1 == CountEntities( connection, lotAutoId, sessionAutoId );
        }

        #endregion

        #region " RELATIONS: LOT "

        /// <summary>   Gets or sets the Lot entity. </summary>
        /// <value> The Lot entity. </value>
        public LotEntity LotEntity { get; private set; }

        /// <summary>   Fetches Lot Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Lot Entity. </returns>
        public LotEntity FetchLotEntity( System.Data.IDbConnection connection )
        {
            var entity = new LotEntity();
            _ = entity.FetchUsingKey( connection, this.LotAutoId );
            this.LotEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count lots associated with the specified <paramref name="sessionAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   The total number of lots. </returns>
        public static int CountLots( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.LotSessionBuilder.TableName}] WHERE {nameof( LotSessionNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Lots associated with the specified <paramref name="sessionAutoId"/>; expected a
        /// single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Lots in this collection.
        /// </returns>
        public static IEnumerable<LotEntity> FetchLots( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotSessionBuilder.TableName}] WHERE {nameof( LotSessionNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<LotEntity>();
            foreach ( LotSessionNub nub in connection.Query<LotSessionNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new LotSessionEntity( nub );
                l.Add( entity.FetchLotEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all lots associated with the specified <paramref name="sessionAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="sessionAutoId">    Identifies the <see cref="SessionEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteLots( System.Data.IDbConnection connection, int sessionAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.LotSessionBuilder.TableName}] WHERE {nameof( LotSessionNub.SecondaryId )} = @SecondaryId", new { SecondaryId = sessionAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        #endregion

        #region " RELATIONS: SESSION "

        /// <summary>   Gets or sets the session entity. </summary>
        /// <value> The session entity. </value>
        public SessionEntity SessionEntity { get; private set; }

        /// <summary>   Fetches a session Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The session Entity. </returns>
        public SessionEntity FetchSessionEntity( System.Data.IDbConnection connection )
        {
            var entity = new SessionEntity();
            _ = entity.FetchUsingKey( connection, this.SessionAutoId );
            this.SessionEntity = entity;
            return entity;
        }

        /// <summary>   Gets or sets the <see cref="IEnumerable{SessionEntity}"/>. </summary>
        /// <value> The <see cref="IEnumerable{SessionEntity}"/>. </value>
        public IEnumerable<SessionEntity> Sessions { get; private set; }

        /// <summary>   Fetches the <see cref="IEnumerable{SessionEntity}"/> in this collection. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Sessions in this collection.
        /// </returns>
        public IEnumerable<SessionEntity> FetchOrderedSessions( System.Data.IDbConnection connection )
        {
            this.Sessions = FetchOrderedSessions( connection, this.LotAutoId );
            return this.Sessions;
        }

        /// <summary>   Count sessions. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The total number of sessions. </returns>
        public static int CountSessions( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.LotSessionBuilder.TableName}] WHERE {nameof( LotSessionNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the sessions in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the sessions in this collection.
        /// </returns>
        public static IEnumerable<SessionEntity> FetchSessions( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotSessionBuilder.TableName}] WHERE {nameof( LotSessionNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<SessionEntity>();
            foreach ( LotSessionNub nub in connection.Query<LotSessionNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new LotSessionEntity( nub );
                l.Add( entity.FetchSessionEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all session related to the specified Lot. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteSessions( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.LotSessionBuilder.TableName}] WHERE {nameof( LotSessionNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.SessionEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionLabel"> The session label. </param>
        /// <returns>   The session. </returns>
        public static SessionEntity FetchSession( System.Data.IDbConnection connection, string selectQuery, int lotAutoId, string sessionLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { lotAutoId, sessionLabel } );
            // Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, new {lotAutoId = lotAutoId, .sessionLabel = sessionLabel})
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<SessionNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new SessionEntity() : new SessionEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.SessionEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="sessionLabel"> The session label. </param>
        /// <returns>   The session. </returns>
        public static SessionEntity FetchSession( System.Data.IDbConnection connection, int lotAutoId, string sessionLabel )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [LotSession] on [LotSession].SecondaryId = [Session].AutoId where [LotSession].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{SessionBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{SessionBuilder.TableName}] Inner Join [{LotSessionBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.LotSessionBuilder.TableName}].{nameof( LotSessionNub.SecondaryId )} = [{isr.Dapper.Entities.SessionBuilder.TableName}].{nameof( SessionNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.LotSessionBuilder.TableName}].{nameof( LotSessionNub.PrimaryId )} = @{nameof( lotAutoId )} " );
            _ = queryBuilder.AppendLine( $"  AND [{isr.Dapper.Entities.SessionBuilder.TableName}].{nameof( SessionNub.Label )} = @{nameof( sessionLabel )}); " );
            return FetchSession( connection, queryBuilder.ToString(), lotAutoId, sessionLabel );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.SessionEntity"/>'s. </summary>
        /// <remarks>   David, 2020-06-23. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The session. </returns>
        public static IEnumerable<SessionEntity> FetchOrderedSessions( System.Data.IDbConnection connection, string selectQuery, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { lotAutoId } );
            // Dim template As SqlBuilder.Template = sqlBuilder.AddTemplate(selectQuery.ToString, new {lotAutoId = lotAutoId})
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return SessionEntity.Populate( connection.Query<SessionNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches <see cref="Dapper.Entities.SessionEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The <see cref="IEnumerable{SessionEntity}"/>. </returns>
        public static IEnumerable<SessionEntity> FetchOrderedSessions( System.Data.IDbConnection connection, int lotAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [LotSession] on [LotSession].SecondaryId = [Session].AutoId where [LotSession].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{SessionBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{SessionBuilder.TableName}] Inner Join [{LotSessionBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.LotSessionBuilder.TableName}].{nameof( LotSessionNub.SecondaryId )} = [{isr.Dapper.Entities.SessionBuilder.TableName}].{nameof( SessionNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.LotSessionBuilder.TableName}].{nameof( LotSessionNub.PrimaryId )} = @{nameof( lotAutoId )}) " );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.SessionBuilder.TableName}].{nameof( SessionNub.Timestamp )} ASC; " );
            return FetchOrderedSessions( connection, queryBuilder.ToString(), lotAutoId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( LotSessionEntity.LotAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.LotEntity"/> </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.LotEntity"/> </value>
        public int LotAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( LotSessionEntity.SessionAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the session. </summary>
        /// <value> Identifies the session. </value>
        public int SessionAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }

    public partial class LotEntity
    {

        /// <summary>   Select last session. </summary>
        /// <remarks>   David, 2021-06-05. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="sessionType">  Type of the session. </param>
        /// <returns>   A Dapper.Entities.SessionEntity. </returns>
        public Dapper.Entities.SessionEntity SelectLastSession( System.Data.IDbConnection connection, SessionType sessionType )
        {
            SessionEntity lastSessionEntity = new();
            if ( this.IsClean() || this.FetchUsingUniqueIndex( connection ) )
            {
                LotSessionEntity lotSession = new() { LotAutoId = this.AutoId };
                _ = lotSession.FetchOrderedSessions( connection );
                if ( lotSession.Sessions.Any() )
                {
                    foreach ( SessionEntity session in lotSession.Sessions.Reverse() )
                    {
                        _ = session.FetchTraits( connection );
                        if ( session.Traits.SessionTrait.SessionType == ( int ) sessionType )
                        {
                            lastSessionEntity = session;
                            break;
                        }
                    }
                }
            }
            else
            {
                throw new InvalidOperationException( $"Failed fetching lot #{this.LotNumber}" );
            }
            return lastSessionEntity;
        }
    }


}

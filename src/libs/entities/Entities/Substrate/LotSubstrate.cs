using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Lot-Substrate builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class LotSubstrateBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( LotSubstrateNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = LotBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( LotNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = SubstrateBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( SubstrateNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( LotSubstrateEntity.LotAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( LotSubstrateEntity.SubstrateAutoId );

        #region " SINGLETON "

        private static readonly Lazy<LotSubstrateBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static LotSubstrateBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Lot Substrate Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "LotSubstrate" )]
    public class LotSubstrateNub : OneToManyNub, IOneToMany
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public LotSubstrateNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new LotSubstrateNub();
        }
    }

    /// <summary>   The Lot-Substrate Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class LotSubstrateEntity : EntityBase<IOneToMany, LotSubstrateNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public LotSubstrateEntity() : this( new LotSubstrateNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotSubstrateEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public LotSubstrateEntity( IOneToMany cache, IOneToMany store ) : base( new LotSubstrateNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotSubstrateEntity( LotSubstrateEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.LotSubstrateBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new LotSubstrateNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Lot-Substrate interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int lotAutoId, int substrateAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, lotAutoId, substrateAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.LotAutoId, this.SubstrateAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.LotAutoId, this.SubstrateAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int lotAutoId, int substrateAutoId )
        {
            return this.FetchUsingKey( connection, lotAutoId, substrateAutoId );
        }

        /// <summary>
        /// Attempts to insert or retrieve a <see cref="LotSubstrateEntity"/> from the given data.
        /// Specifying new <paramref name="lotNumber"/> add this lot; Specifying a new
        /// <paramref name="substrateNumber"/> adds a new substrate with the next substrate number for
        /// this lot. Updates the <see cref="LotSubstrateEntity.SubstrateEntity"/> and
        /// <see cref="LotSubstrateEntity.LotEntity"/>
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotNumber">        The lot number. </param>
        /// <param name="substrateNumber">  The substrate number. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, string lotNumber, int substrateNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            this.LotEntity = new LotEntity() { LotNumber = lotNumber };
            this.SubstrateEntity = new SubstrateEntity() { SubstrateNumber = substrateNumber };
            if ( this.LotEntity.Obtain( connection ) )
            {
                if ( 0 < substrateNumber )
                {
                    if ( !this.SubstrateEntity.FetchUsingUniqueIndex( connection ) )
                    {
                        result = (false, $"Failed fetching existing {nameof( Dapper.Entities.SubstrateEntity )} with {nameof( Dapper.Entities.SubstrateEntity.SubstrateNumber )} of {substrateNumber}");
                    }
                }
                else
                {
                    this.SubstrateEntity = FetchLastSubstrate( connection, this.LotEntity.AutoId );
                    substrateNumber = this.SubstrateEntity.IsClean() ? this.SubstrateEntity.SubstrateNumber + 1 : 1;
                    this.SubstrateEntity = new SubstrateEntity() { SubstrateNumber = substrateNumber };
                    if ( !this.SubstrateEntity.Obtain( connection ) )
                    {
                        result = (false, $"Failed obtaining {nameof( Dapper.Entities.SubstrateEntity )} with {nameof( Dapper.Entities.SubstrateEntity.SubstrateNumber )} of {substrateNumber}");
                    }
                }
            }
            else
            {
                result = (false, $"Failed obtaining {nameof( Dapper.Entities.LotEntity )} with {nameof( Dapper.Entities.LotEntity.LotNumber )} of {lotNumber}");
            }

            if ( result.Success )
            {
                this.LotAutoId = this.LotEntity.AutoId;
                this.SubstrateAutoId = this.SubstrateEntity.AutoId;
                if ( !this.Obtain( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( LotSubstrateEntity )} for [{nameof( Dapper.Entities.LotEntity.LotNumber )}, {nameof( Dapper.Entities.SubstrateEntity.SubstrateNumber )}] of [{lotNumber},{substrateNumber}]");
                }
            };
            this.NotifyPropertyChanged( nameof( LotSubstrateEntity.SubstrateEntity ) );
            this.NotifyPropertyChanged( nameof( LotSubstrateEntity.LotEntity ) );
            return result;
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int lotAutoId, int substrateAutoId )
        {
            return connection.Delete( new LotSubstrateNub() { PrimaryId = lotAutoId, SecondaryId = substrateAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Lot-Substrate entities. </summary>
        /// <value> The Lot-Substrate entities. </value>
        public IEnumerable<LotSubstrateEntity> LotSubstrates { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotSubstrateEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<LotSubstrateNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.LotSubstrates = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( LotSubstrateEntity.LotSubstrates ) );
            return this.LotSubstrates?.Any() == true ? this.LotSubstrates.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotSubstrateEntity> Populate( IEnumerable<LotSubstrateNub> nubs )
        {
            var l = new List<LotSubstrateEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( LotSubstrateNub nub in nubs )
                    l.Add( new LotSubstrateEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotSubstrateEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<LotSubstrateEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new LotSubstrateNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new LotSubstrateEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Substrate entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotSubstrateBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotSubstrateNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotSubstrateEntity> FetchEntities( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}] WHERE {nameof( LotSubstrateNub.PrimaryId )} = @Id", new { Id = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<LotSubstrateNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Substrate. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>   The total number of entities by Substrate. </returns>
        public static int CountEntitiesBySubstrate( System.Data.IDbConnection connection, int substrateAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotSubstrateBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotSubstrateNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substrateAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Substrates in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Substrates in this
        /// collection.
        /// </returns>
        public static IEnumerable<LotSubstrateEntity> FetchEntitiesBySubstrate( System.Data.IDbConnection connection, int substrateAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}] WHERE {nameof( LotSubstrateNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substrateAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<LotSubstrateNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int lotAutoId, int substrateAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotSubstrateBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotSubstrateNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotSubstrateNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substrateAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotSubstrateNub> FetchNubs( System.Data.IDbConnection connection, int lotAutoId, int substrateAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotSubstrateBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotSubstrateNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotSubstrateNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substrateAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LotSubstrateNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Lot Substrate exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int lotAutoId, int substrateAutoId )
        {
            return 1 == CountEntities( connection, lotAutoId, substrateAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Lot entity. </summary>
        /// <value> The Lot entity. </value>
        public LotEntity LotEntity { get; private set; }

        /// <summary>   Fetches Lot Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Lot Entity. </returns>
        public LotEntity FetchLotEntity( System.Data.IDbConnection connection )
        {
            var entity = new LotEntity();
            _ = entity.FetchUsingKey( connection, this.LotAutoId );
            this.LotEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count lots associated with the specified <paramref name="substrateAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>   The total number of lots. </returns>
        public static int CountLots( System.Data.IDbConnection connection, int substrateAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}] WHERE {nameof( LotSubstrateNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substrateAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Lots associated with the specified <paramref name="substrateAutoId"/>; expected a
        /// single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Lots in this collection.
        /// </returns>
        public static IEnumerable<SubstrateEntity> FetchLots( System.Data.IDbConnection connection, int substrateAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}] WHERE {nameof( LotSubstrateNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substrateAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<SubstrateEntity>();
            foreach ( LotSubstrateNub nub in connection.Query<LotSubstrateNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new LotSubstrateEntity( nub );
                l.Add( entity.FetchSubstrateEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all lots associated with the specified <paramref name="substrateAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="substrateAutoId">  Identifies the <see cref="SubstrateEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteLots( System.Data.IDbConnection connection, int substrateAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}] WHERE {nameof( LotSubstrateNub.SecondaryId )} = @SecondaryId", new { SecondaryId = substrateAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the Substrate entity. </summary>
        /// <value> The Substrate entity. </value>
        public SubstrateEntity SubstrateEntity { get; private set; }

        /// <summary>   Fetches a Substrate Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Substrate Entity. </returns>
        public SubstrateEntity FetchSubstrateEntity( System.Data.IDbConnection connection )
        {
            var entity = new SubstrateEntity();
            _ = entity.FetchUsingKey( connection, this.SubstrateAutoId );
            this.SubstrateEntity = entity;
            return entity;
        }

        /// <summary>   Count substrates. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The total number of substrates. </returns>
        public static int CountSubstrates( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}] WHERE {nameof( LotSubstrateNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Substrates in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Substrates in this collection.
        /// </returns>
        public static IEnumerable<SubstrateEntity> FetchSubstrates( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}] WHERE {nameof( LotSubstrateNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<SubstrateEntity>();
            foreach ( LotSubstrateNub nub in connection.Query<LotSubstrateNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new LotSubstrateEntity( nub );
                l.Add( entity.FetchSubstrateEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Substrate related to the specified Lot. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteSubstrates( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}] WHERE {nameof( LotSubstrateNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the ordered substrates in this collection. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered substrates in this
        /// collection.
        /// </returns>
        public static IEnumerable<SubstrateEntity> FetchOrderedSubstrates( System.Data.IDbConnection connection, string selectQuery, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = lotAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return SubstrateEntity.Populate( connection.Query<SubstrateNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered substrates in this collection. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered substrates in this
        /// collection.
        /// </returns>
        public static IEnumerable<SubstrateEntity> FetchOrderedSubstrates( System.Data.IDbConnection connection, int lotAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [LotSubstrate] on [LotSubstrate].SecondaryId = [Substrate].AutoId where [LotSubstrate].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{SubstrateBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{SubstrateBuilder.TableName}] Inner Join [{LotSubstrateBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}].{nameof( LotSubstrateNub.SecondaryId )} = [{isr.Dapper.Entities.SubstrateBuilder.TableName}].{nameof( SubstrateNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}].{nameof( LotSubstrateNub.PrimaryId )} = @Id" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.SubstrateBuilder.TableName}].{nameof( SubstrateNub.Amount )} ASC; " );
            return FetchOrderedSubstrates( connection, queryBuilder.ToString(), lotAutoId );
        }

        /// <summary>   Fetches the last Substrate in this collection. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the last Substrates in this
        /// collection.
        /// </returns>
        public static SubstrateEntity FetchFirstSubstrate( System.Data.IDbConnection connection, string selectQuery, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery, new { Id = lotAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<SubstrateNub>( template.RawSql, template.Parameters ).FirstOrDefault();
            return nub is null ? new SubstrateEntity() : new SubstrateEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches the last Substrate in this collection. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the last Substrates in this
        /// collection.
        /// </returns>
        public static SubstrateEntity FetchLastSubstrate( System.Data.IDbConnection connection, int lotAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [LotSubstrate] on [LotSubstrate].SecondaryId = [Substrate].AutoId where [LotSubstrate].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{SubstrateBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{SubstrateBuilder.TableName}] Inner Join [{LotSubstrateBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}].{nameof( LotSubstrateNub.SecondaryId )} = [{isr.Dapper.Entities.SubstrateBuilder.TableName}].{nameof( SubstrateNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.LotSubstrateBuilder.TableName}].{nameof( LotSubstrateNub.PrimaryId )} = @Id" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.SubstrateBuilder.TableName}].{nameof( SubstrateNub.Amount )} DESC; " );
            return FetchFirstSubstrate( connection, queryBuilder.ToString(), lotAutoId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( LotSubstrateEntity.LotAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.LotEntity"/> </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.LotEntity"/> </value>
        public int LotAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( LotSubstrateEntity.SubstrateAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="SubstrateEntity"/>. </summary>
        /// <value> Identifies the <see cref="SubstrateEntity"/>. </value>
        public int SubstrateAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the ImportFile nub and entity. Includes the fields as kept in the data table.
    /// Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IImportFile
    {

        /// <summary>   Gets or sets the id of the import file. </summary>
        /// <value> Identifies the import file. </value>
        [ExplicitKey]
        int Id { get; set; }

        /// <summary>   Gets or sets the filename of the file. </summary>
        /// <value> The name of the file. </value>
        string FileName { get; set; }

        /// <summary>   Gets or sets the File UTC timestamp. </summary>
        /// <remarks>   Stored in universal time. </remarks>
        /// <value> The File UTC timestamp. </value>
        DateTime FileTimestamp { get; set; }

        /// <summary>   Gets or sets the id of the file import state. </summary>
        /// <value> Identifies the file import state. </value>
        int FileImportStateId { get; set; }

        /// <summary>   Gets or sets the details. </summary>
        /// <value> The details. </value>
        string Details { get; set; }

        /// <summary>   Gets or sets the UTC timestamp of the last import. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        DateTime ImportTimestamp { get; set; }
    }

    /// <summary>   An import file builder. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public sealed class ImportFileBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ImportFileNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets the name of the FileName index. </summary>
        /// <value> The name of the FileName index. </value>
        private static string FileNameIndexName => $"UQ_{isr.Dapper.Entities.ImportFileBuilder.TableName}_{nameof( ImportFileNub.FileName )}";

        /// <summary>   Filename of the using unique file. </summary>
        private static bool? _UsingUniqueFileName;

        /// <summary>   Indicates if the entity uses a unique FileName. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool UsingUniqueFileName( System.Data.IDbConnection connection )
        {
            if ( !_UsingUniqueFileName.HasValue )
            {
                _UsingUniqueFileName = connection.IndexExists( FileNameIndexName );
            }

            return _UsingUniqueFileName.Value;
        }

        /// <summary>   Inserts values from file. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="fileName">     Filename of the file. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertFileValues( System.Data.IDbConnection connection, string fileName )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : fileName is null
                ? throw new ArgumentNullException( nameof( fileName ) )
                : connection.Execute( System.IO.File.ReadAllText( fileName ) );
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="uniqueFileName">   True to unique file name. </param>
        /// <returns>   The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection, bool uniqueFileName )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql, uniqueFileName )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite, uniqueFileName ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="uniqueFileName">   True to unique file name. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection, bool uniqueFileName )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Entities.ImportFileBuilder.TableName}] (
                [{nameof( ImportFileNub.Id )}] integer NOT NULL PRIMARY KEY, 
                [{nameof( ImportFileNub.FileName )}] nvarchar(512) NOT NULL, 
                [{nameof( ImportFileNub.FileTimestamp )}] datetime NOT NULL, 
                [{nameof( ImportFileNub.FileImportStateId )}] integer NOT NULL, 
                [{nameof( ImportFileNub.Details )}] nvarchar(2048) NOT NULL, 
                [{nameof( ImportFileNub.ImportTimestamp )}] datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
            FOREIGN KEY ([{nameof( ImportFileNub.FileImportStateId )}]) REFERENCES [{isr.Dapper.Entities.FileImportStateBuilder.TableName}] ([{nameof( FileImportStateNub.Id )}])
                    ON UPDATE CASCADE ON DELETE CASCADE); " );
            if ( uniqueFileName )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{isr.Dapper.Entities.ImportFileBuilder.FileNameIndexName}] ON [{isr.Dapper.Entities.ImportFileBuilder.TableName}] ([{nameof( ImportFileNub.FileName )}]); ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="uniqueFileName">   True to unique file name. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection, bool uniqueFileName )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.ImportFileBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Entities.ImportFileBuilder.TableName}](
                [{nameof( ImportFileNub.Id )}] [int] NOT NULL,
                [{nameof( ImportFileNub.FileName )}] [nvarchar](512) NOT NULL,
                [{nameof( ImportFileNub.FileTimestamp )}] [datetime] NOT NULL,
                [{nameof( ImportFileNub.FileImportStateId )}] [int] NOT NULL,
                [{nameof( ImportFileNub.Details )}] [nvarchar](2048) NOT NULL,
                [{nameof( ImportFileNub.ImportTimestamp )}] [datetime] NOT NULL DEFAULT (sysutcdatetime()),
             CONSTRAINT [PK_{isr.Dapper.Entities.ImportFileBuilder.TableName}] PRIMARY KEY CLUSTERED 
            ([{nameof( ImportFileNub.Id )}] ASC) 
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
             ON [PRIMARY]
            END; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.ImportFileBuilder.TableName}_{isr.Dapper.Entities.FileImportStateBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.ImportFileBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.ImportFileBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.ImportFileBuilder.TableName}_{isr.Dapper.Entities.FileImportStateBuilder.TableName}] FOREIGN KEY([{nameof( ImportFileNub.FileImportStateId )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.FileImportStateBuilder.TableName}] ([{nameof( FileImportStateNub.Id )}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.ImportFileBuilder.TableName}_{isr.Dapper.Entities.FileImportStateBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.ImportFileBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.ImportFileBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.ImportFileBuilder.TableName}_{isr.Dapper.Entities.FileImportStateBuilder.TableName}]; " );
            if ( uniqueFileName )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.ImportFileBuilder.TableName}]') AND name = N'{isr.Dapper.Entities.ImportFileBuilder.FileNameIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{isr.Dapper.Entities.ImportFileBuilder.FileNameIndexName}] ON [dbo].[{isr.Dapper.Entities.ImportFileBuilder.TableName}] ([{nameof( ImportFileNub.FileName )}] ASC) 
                WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
                ON [PRIMARY]; ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        #endregion

        #region " VIEWS "

        /// <summary>   Creates imported files view sq lite. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void CreateImportedFilesViewSQLite( System.Data.IDbConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @"CREATE VIEW IF NOT EXISTS [ImportedFiles] AS
SELECT ImportFile.FileName, ImportFile.FileTimestamp, ImportFile.Timestamp, FileImportState.Name AS ImportState, ImportFile.Details
FROM   ImportFile INNER JOIN FileImportState ON ImportFile.FileImportStateId = FileImportState.FileImportStateId; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
        }

        /// <summary>   Creates imported files view SQL server. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void CreateImportedFilesViewSqlServer( System.Data.IDbConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( $@"IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ImportedFiles]'))
EXEC   dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[ImportedFiles] AS
SELECT dbo.ImportFile.FileName, dbo.ImportFile.FileTimestamp, dbo.ImportFile.Timestamp, dbo.FileImportState.Name AS ImportState, dbo.ImportFile.Details
FROM   dbo.ImportFile INNER JOIN dbo.FileImportState ON dbo.ImportFile.FileImportStateId = dbo.FileImportState.FileImportStateId'; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
        }

        #endregion

    }


    /// <summary>   Implements the ImportFile table <see cref="IImportFile">interface</see>. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-10-10 </para>
    /// </remarks>
    [Table( "ImportFile" )]
    public class ImportFileNub : EntityNubBase<IImportFile>, IImportFile
    {

        #region " CONSTRUCTION "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ImportFileNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IImportFile CreateNew()
        {
            return new ImportFileNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IImportFile CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IImportFile value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IImportFile source, IImportFile destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Details = source.Details;
            destination.FileImportStateId = source.FileImportStateId;
            destination.FileName = source.FileName;
            destination.FileTimestamp = source.FileTimestamp;
            destination.Id = source.Id;
            destination.ImportTimestamp = source.ImportTimestamp;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IImportFile );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IImportFile other ) // Implements IEquatable(Of IImportFile).Equals
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IImportFile left, IImportFile right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.FileImportStateId, right.FileImportStateId );
                result = result && string.Equals( left.Details, right.Details );
                result = result && string.Equals( left.FileName, right.FileName );
                result = result && DateTime.Equals( left.FileTimestamp, right.FileTimestamp );
                result = result && Equals( left.Id, right.Id );
                result = result && DateTime.Equals( left.ImportTimestamp, right.ImportTimestamp );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.FileImportStateId, this.Details, this.FileName, this.FileTimestamp, this.Id, this.ImportTimestamp ).GetHashCode();
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the import file. </summary>
        /// <value> Identifies the import file. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary>   Gets or sets the filename of the file. </summary>
        /// <value> The name of the file. </value>
        public string FileName { get; set; }

        /// <summary>   Gets or sets the File UTC timestamp. </summary>
        /// <remarks>   Stored in universal time. </remarks>
        /// <value> The File UTC timestamp. </value>
        public DateTime FileTimestamp { get; set; }

        /// <summary>   Gets or sets the id of the file import state. </summary>
        /// <value> Identifies the file import state. </value>
        public int FileImportStateId { get; set; }

        /// <summary>   Gets or sets the details. </summary>
        /// <value> The details. </value>
        public string Details { get; set; }

        /// <summary>   Gets or sets the UTC timestamp of the last import. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime ImportTimestamp { get; set; }

        #endregion

    }

    /// <summary>   The ImportFile Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-10-10 </para>
    /// </remarks>
    public class ImportFileEntity : EntityBase<IImportFile, ImportFileNub>, IImportFile
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ImportFileEntity() : this( new ImportFileNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ImportFileEntity( IImportFile value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ImportFileEntity( IImportFile cache, IImportFile store ) : base( new ImportFileNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ImportFileEntity( ImportFileEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ImportFileBuilder.TableName, nameof( IImportFile ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IImportFile CreateNew()
        {
            return new ImportFileNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IImportFile CreateCopy()
        {
            var destination = this.CreateNew();
            ImportFileNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IImportFile value )
        {
            ImportFileNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    the Meter Model interface. </param>
        public override void UpdateCache( IImportFile value )
        {
            // first make the copy to notify of any property change.
            ImportFileNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The ImportFile table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IImportFile>( key ) : connection.Get<ImportFileNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }


        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.FileName );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="fileName">     The <see cref="ImportFileEntity"/> file name. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string fileName )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, fileName ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IImportFile entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.FileName ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Stores an entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool StoreEntity( System.Data.IDbConnection connection, IImportFile entity )
        {
            return new ImportFileEntity().Upsert( connection, entity );
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete<IImportFile>( new ImportFileNub() { Id = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the import files. </summary>
        /// <value> The import files. </value>
        public IEnumerable<ImportFileEntity> ImportFiles { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ImportFileEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IImportFile>() ) : Populate( connection.GetAll<ImportFileNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ImportFiles = FetchAllEntities( connection, true );
            this.NotifyPropertyChanged( nameof( ImportFileEntity.ImportFiles ) );
            return this.ImportFiles?.Any() == true ? this.ImportFiles.Count() : 0;
        }

        /// <summary>   Populates a list of entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The entity nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ImportFileEntity> Populate( IEnumerable<ImportFileNub> nubs )
        {
            var l = new List<ImportFileEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ImportFileNub nub in nubs )
                    l.Add( new ImportFileEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The entity interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ImportFileEntity> Populate( IEnumerable<IImportFile> interfaces )
        {
            var l = new List<ImportFileEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ImportFileNub();
                foreach ( IImportFile iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ImportFileEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="fileName">     The <see cref="ImportFileEntity"/> file name. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string fileName )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.ImportFileBuilder.TableName}] WHERE {nameof( ImportFileNub.FileName )} = @{nameof( fileName )}", new { fileName } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="fileName">     The <see cref="ImportFileEntity"/> file name. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ImportFileNub> FetchNubs( System.Data.IDbConnection connection, string fileName )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ImportFileBuilder.TableName}] WHERE {nameof( ImportFileNub.FileName )} = @FileName", new { fileName } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ImportFileNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the ImportFile exists. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="fileName">     The <see cref="ImportFileEntity"/> file name. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string fileName )
        {
            return 1 == CountEntities( connection, fileName );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the import file. </summary>
        /// <value> Identifies the import file. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the filename of the file. </summary>
        /// <value> The name of the file. </value>
        public string FileName
        {
            get => this.ICache.FileName;

            set {
                if ( !string.Equals( this.FileName, value ) )
                {
                    this.ICache.FileName = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the File UTC timestamp. </summary>
        /// <remarks>   Stored in universal time. </remarks>
        /// <value> The File UTC timestamp. </value>
        public DateTime FileTimestamp
        {
            get => this.ICache.FileTimestamp;

            set {
                if ( !DateTime.Equals( this.FileTimestamp, value ) )
                {
                    this.ICache.FileTimestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the file import state. </summary>
        /// <value> Identifies the file import state. </value>
        public int FileImportStateId
        {
            get => this.ICache.FileImportStateId;

            set {
                if ( !object.Equals( ( object ) this.FileImportStateId, ( object ) value ) )
                {
                    this.ICache.FileImportStateId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ImportFileEntity.FileImportState ) );
                }
            }
        }

        /// <summary>   Gets or sets the File Import State. </summary>
        /// <value> The File Import State. </value>
        public FileImportState FileImportState
        {
            get => ( FileImportState ) this.FileImportStateId;

            set => this.FileImportStateId = ( int ) value;
        }

        /// <summary>   Gets or sets the details. </summary>
        /// <value> The details. </value>
        public string Details
        {
            get => this.ICache.Details;

            set {
                if ( !string.Equals( this.Details, value ) )
                {
                    this.ICache.Details = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the UTC timestamp of the last import. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime ImportTimestamp
        {
            get => this.ICache.ImportTimestamp;

            set {
                if ( !DateTime.Equals( this.ImportTimestamp, value ) )
                {
                    this.ICache.ImportTimestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " TIMESTAMP "

        /// <summary>   Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        /// <summary>   The timestamp caption format. </summary>
        private string _TimestampCaptionFormat;

        /// <summary>   Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._TimestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._TimestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.ImportTimestamp.ToString( this.TimestampCaptionFormat );

        #endregion

        #region " ACTIVE IMPORT FILE "

        /// <summary>   Gets or sets the active import file identifier. </summary>
        /// <value> Identifies the active import file. </value>
        public static int ActiveImportFileId { get; set; }

        /// <summary>   Gets or sets the active lot import file entity. </summary>
        /// <value> The active lot import file entity. </value>
        public static ImportFileEntity ActiveImportFileEntity { get; private set; }

        /// <summary>   Fetches active import file entity. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool FetchActiveImportFileEntity( System.Data.IDbConnection connection )
        {
            if ( ActiveImportFileId <= 0 )
            {
                throw new InvalidOperationException( $"{nameof( ActiveImportFileId )} is {ActiveImportFileId} must be set before fetching the active file import record." );
            }

            ActiveImportFileEntity = new ImportFileEntity() { Id = ActiveImportFileId };
            return ActiveImportFileEntity.FetchUsingKey( connection );
        }

        /// <summary>   Import file information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <returns>   A System.IO.FileInfo. </returns>
        public static System.IO.FileInfo ImportFileInfo()
        {
            string filename = ActiveImportFileEntity.FileName;
            return string.IsNullOrWhiteSpace( filename )
                ? throw new InvalidOperationException( $"{nameof( ActiveImportFileEntity )} {nameof( ActiveImportFileEntity.FileName )} is empty" )
                : new System.IO.FileInfo( filename );
        }

        /// <summary>   Query if an import required. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   <c>true</c> if import required; otherwise <c>false</c> </returns>
        public static bool IsImportRequired()
        {
            return ActiveImportFileEntity.ImportTimestamp < ImportFileInfo().LastWriteTimeUtc;
        }

        /// <summary>   Queries if a given if import file folder exists. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool IsImportFileFolderExists()
        {
            return ImportFileInfo().Directory.Exists;
        }

        /// <summary>   Query if this the import file exists. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <returns>   True if import file exists, false if not. </returns>
        public static bool IsImportFileExists()
        {
            return ImportFileInfo().Exists;
        }

        /// <summary>   Stores last import date time. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="lastImportUniversalTime">  The last import universal time. </param>
        public static void StoreLastImportDateTime( System.Data.IDbConnection connection, DateTime lastImportUniversalTime )
        {
            ActiveImportFileEntity.FileTimestamp = ImportFileInfo().LastWriteTimeUtc;
            ActiveImportFileEntity.ImportTimestamp = lastImportUniversalTime;
            _ = ActiveImportFileEntity.Update( connection );
        }


        #endregion

    }
}

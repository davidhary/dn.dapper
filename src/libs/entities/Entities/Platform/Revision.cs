using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// A builder for a nominal Revision table identified by a revision number based on the
    /// <see cref="IKeyLabelTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class RevisionBuilder : KeyLabelTimeBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( RevisionNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Inserts or update a revision. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="version">      The version. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool UpsertRevision( System.Data.IDbConnection connection, string version )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var releaseHistory = new RevisionNub() { Label = version };
            var entity = new RevisionEntity();
            return entity.Upsert( connection, releaseHistory );
        }

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public override int LabelFieldSize { get; set; } = 31;

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( RevisionEntity.AutoId );

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public override string LabelFieldName { get; set; } = nameof( RevisionEntity.Revision );


        /// <summary>   Gets or sets the name of the timestamp field. </summary>
        /// <value> The name of the timestamp field. </value>
        public override string TimestampFieldName { get; set; } = nameof( RevisionEntity.Timestamp );

        #region " SINGLETON "

        private static readonly Lazy<RevisionBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static RevisionBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Revision table based on the <see cref="IKeyLabelTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "Revision" )]
    public class RevisionNub : KeyLabelTimeNub, IKeyLabelTime
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public RevisionNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyLabelTime CreateNew()
        {
            return new RevisionNub();
        }
    }


    /// <summary>
    /// The Revision Entity  based on the <see cref="IKeyLabelTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class RevisionEntity : EntityBase<IKeyLabelTime, RevisionNub>, IKeyLabelTime
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public RevisionEntity() : this( new RevisionNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public RevisionEntity( IKeyLabelTime value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public RevisionEntity( IKeyLabelTime cache, IKeyLabelTime store ) : base( new RevisionNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public RevisionEntity( RevisionEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.RevisionBuilder.TableName, nameof( IKeyLabelTime ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "


        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyLabelTime CreateNew()
        {
            return new RevisionNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyLabelTime CreateCopy()
        {
            var destination = this.CreateNew();
            KeyLabelTimeNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyLabelTime value )
        {
            KeyLabelTimeNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Revision interface. </param>
        public override void UpdateCache( IKeyLabelTime value )
        {
            // first make the copy to notify of any property change.
            KeyLabelTimeNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The Revision table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyLabelTime>( key ) : connection.Get<RevisionNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Revision );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="revision">     The revision. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string revision )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, revision ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyLabelTime entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            bool fetched = RevisionBuilder.Instance.UsingUniqueLabel( connection ) ? this.FetchUsingUniqueIndex( connection, entity.Label ) : this.FetchUsingKey( connection, entity.AutoId );
            if ( fetched )
            {
                // update the existing record from the specified entity.
                entity.AutoId = this.AutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new RevisionNub() { AutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Revision entities. </summary>
        /// <value> The Revision entities. </value>
        public IEnumerable<RevisionEntity> Revisions { get; private set; }

        /// <summary>   Fetches entities ordered by the specified field. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="orderBy">      Specifies the field name for ordering the entities. </param>
        /// <returns>   The entities. </returns>
        public int FetchEntities( System.Data.IDbConnection connection, string orderBy )
        {
            this.Revisions = FetchAllEntities( connection, orderBy );
            return this.Revisions?.Any() == true ? this.Revisions.Count() : 0;
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="orderBy">      Specifies the field name for ordering the entities. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<RevisionEntity> FetchAllEntities( System.Data.IDbConnection connection, string orderBy )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( @$"select * from [{RevisionBuilder.TableName}] ORDER BY {orderBy} ASC; " );
            var gridReader = connection.QueryMultiple( queryBuilder.ToString() );
            return Populate( gridReader.Read<RevisionNub>() );
        }


        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<RevisionEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyLabelTime>() ) : Populate( connection.GetAll<RevisionNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Revisions = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( RevisionEntity.Revisions ) );
            return this.Revisions?.Any() == true ? this.Revisions.Count() : 0;
        }

        /// <summary>   Populates a list of Revision entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The Revision nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<RevisionEntity> Populate( IEnumerable<RevisionNub> nubs )
        {
            var l = new List<RevisionEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( RevisionNub nub in nubs )
                    l.Add( new RevisionEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Revision entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The Revision interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<RevisionEntity> Populate( IEnumerable<IKeyLabelTime> interfaces )
        {
            var l = new List<RevisionEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new RevisionNub();
                foreach ( IKeyLabelTime iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new RevisionEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="revisionName"> The name of the revision. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string revisionName )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{RevisionBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( RevisionNub.Label )} = @RevisionName", new { revisionName } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="revisionName"> The name of the revision. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntitiesAlternative( System.Data.IDbConnection connection, string revisionName )
        {
            var selectSql = new System.Text.StringBuilder( $"SELECT * FROM [{RevisionBuilder.TableName}]" );
            _ = selectSql.Append( $"WHERE (@{nameof( RevisionNub.Label )} IS NULL OR {nameof( RevisionNub.Label )} = @revisionName)" );
            _ = selectSql.Append( "; " );
            return connection.ExecuteScalar<int>( selectSql.ToString(), new { revisionName } );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="revisionName"> The name of the revision. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<RevisionNub> FetchNubs( System.Data.IDbConnection connection, string revisionName )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{RevisionBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( RevisionNub.Label )} = @RevisionName", new { revisionName } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<RevisionNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the revision exists. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="revisionName"> The name of the revision. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string revisionName )
        {
            return 1 == CountEntities( connection, revisionName );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Revision. </summary>
        /// <value> Identifies the Revision. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the Revision number. </summary>
        /// <value> The Revision number. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( RevisionEntity.Revision ) );
                }
            }
        }

        /// <summary>   Gets or sets the revision. </summary>
        /// <value> The revision. </value>
        public string Revision
        {
            get => this.Label;

            set => this.Label = value;
        }

        /// <summary>   Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTime.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " FIELDS: TIMESTAMP "

        /// <summary>   Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        /// <summary>   The timestamp caption format. </summary>
        private string _TimestampCaptionFormat;

        /// <summary>   Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._TimestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._TimestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.Timestamp.ToString( this.TimestampCaptionFormat );

        #endregion

        #region " ACTIVE RELEASE "

        /// <summary>   Fetches the active entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchActive( System.Data.IDbConnection connection )
        {
            if ( 0 < this.FetchEntities( connection, nameof( RevisionNub.Timestamp ) ) )
            {
                this.UpdateCache( this.Revisions.LastOrDefault() );
                return this.FetchUsingKey( connection );
            }
            else { return false; }
        }

        /// <summary>   Query if 'expectedVersion' is expected current release. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="expectedVersion">  The expected version. </param>
        /// <returns>   <c>true</c> if expected current release; otherwise <c>false</c> </returns>
        public bool IsExpectedCurrentRelease( string expectedVersion )
        {
            return string.Equals( this.Revision, expectedVersion, StringComparison.OrdinalIgnoreCase );
        }

        /// <summary>   Returns True if the database version verifies. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="expectedVersion">  The expected version. </param>
        /// <returns>   <c>True</c> if current release; <c>False</c> otherwise. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details) TryVerifyDatabaseVersion( System.Data.IDbConnection connection, string expectedVersion )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) outcome = (true, string.Empty);
            try
            {
                var entity = new RevisionEntity();
                if ( entity.FetchActive( connection ) )
                {
                    if ( !entity.IsExpectedCurrentRelease( expectedVersion ) )
                    {
                        outcome = (false, $"Active database version {entity.Revision} does not match the program data version {expectedVersion}");
                    }
                }
                else
                {
                    outcome = (false, "Release history has no active records");
                }
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                outcome = (false, $"Exception fetching active release;. {ex}");
            }

            return outcome;
        }

        /// <summary>   Fetches active release history version. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The active release history version. </returns>
        public static Version FetchActiveReleaseHistoryVersion( System.Data.IDbConnection connection )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : new Version( FetchAllEntities( connection, nameof( RevisionNub.Timestamp ) ).Last().Revision );
        }

        #endregion


    }
}

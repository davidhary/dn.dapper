using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Computer builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ComputerBuilder : KeyLabelTimezoneBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ComputerNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public override int LabelFieldSize { get; set; } = 50;

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( ComputerEntity.AutoId );

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public override string LabelFieldName { get; set; } = nameof( ComputerEntity.ComputerName );

        /// <summary>   Gets or sets the name of the timestamp field. </summary>
        /// <value> The name of the timestamp field. </value>
        public override string TimestampFieldName { get; set; } = nameof( ComputerEntity.Timestamp );

        /// <summary>   Gets or sets the name of the timezone identifier field. </summary>
        /// <value> The name of the timezone identifier field. </value>
        public override string TimezoneIdFieldName { get; set; } = nameof( ComputerEntity.TimezoneId );

        #region " SINGLETON "

        private static readonly Lazy<ComputerBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ComputerBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Computer table based on the <see cref="IKeyLabelTimezone">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "Computer" )]
    public class ComputerNub : KeyLabelTimezoneNub, IKeyLabelTimezone
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ComputerNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyLabelTimezone CreateNew()
        {
            return new ComputerNub();
        }
    }


    /// <summary>   The Computer Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class ComputerEntity : EntityBase<IKeyLabelTimezone, ComputerNub>, IKeyLabelTimezone
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ComputerEntity() : this( new ComputerNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ComputerEntity( IKeyLabelTimezone value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ComputerEntity( IKeyLabelTimezone cache, IKeyLabelTimezone store ) : base( new ComputerNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ComputerEntity( ComputerEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ComputerBuilder.TableName, nameof( IKeyLabelTimezone ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyLabelTimezone CreateNew()
        {
            return new ComputerNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyLabelTimezone CreateCopy()
        {
            var destination = this.CreateNew();
            KeyLabelTimezoneNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyLabelTimezone value )
        {
            KeyLabelTimezoneNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Computer interface. </param>
        public override void UpdateCache( IKeyLabelTimezone value )
        {
            // first make the copy to notify of any property change.
            KeyLabelTimezoneNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The Computer table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyLabelTimezone>( key ) : connection.Get<ComputerNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.ComputerName );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="computerName"> The name of the computer. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string computerName )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, computerName ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Obtains a default entity using the given connection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool ObtainDefault( System.Data.IDbConnection connection )
        {
            if ( string.IsNullOrEmpty( this.ComputerName ) )
                this.ComputerName = Environment.MachineName;
            if ( string.IsNullOrWhiteSpace( this.TimezoneId ) )
                this.TimezoneId = System.TimeZoneInfo.Local.StandardName;
            return this.Obtain( connection );
        }

        /// <summary>   Attempts to obtain from the given data. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="computerName">         The name of the computer. </param>
        /// <param name="addRecordPredicate">   The add record predicate. Returns true to add the record.
        ///                                     Set as affirmative or open a message box requesting
        ///                                     operator confirmation. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryObtain( System.Data.IDbConnection connection, string computerName, Func<bool> addRecordPredicate )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            if ( string.IsNullOrWhiteSpace( computerName ) )
                throw new ArgumentNullException( nameof( computerName ) );
            this.ComputerName = computerName;
            return this.FetchUsingUniqueIndex( connection )
                ? this.IsClean() : addRecordPredicate.Invoke() && this.ObtainDefault( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyLabelTimezone entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            bool fetched = ComputerBuilder.Instance.UsingUniqueLabel( connection ) ? this.FetchUsingUniqueIndex( connection, entity.Label ) : this.FetchUsingKey( connection, entity.AutoId );
            if ( fetched )
            {
                // update the existing record from the specified entity.
                entity.AutoId = this.AutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new ComputerNub() { AutoId = key } );
        }

        /// <summary>   Updates the time stamp. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="timestamp">    The UTC timestamp. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool UpdateTimeStamp( System.Data.IDbConnection connection, DateTime timestamp )
        {
            ComputerBuilder.Instance.UpdateTimestamp( connection, this.ComputerName, timestamp );
            return this.FetchUsingKey( connection );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Computer entities. </summary>
        /// <value> The Computer entities. </value>
        public IEnumerable<ComputerEntity> Computers { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ComputerEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyLabelTimezone>() ) : Populate( connection.GetAll<ComputerNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Computers = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ComputerEntity.Computers ) );
            return this.Computers?.Any() == true ? this.Computers.Count() : 0;
        }

        /// <summary>   Populates a list of Computer entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The Computer nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ComputerEntity> Populate( IEnumerable<ComputerNub> nubs )
        {
            var l = new List<ComputerEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ComputerNub nub in nubs )
                    l.Add( new ComputerEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Computer entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The Computer interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ComputerEntity> Populate( IEnumerable<IKeyLabelTimezone> interfaces )
        {
            var l = new List<ComputerEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ComputerNub();
                foreach ( IKeyLabelTimezone iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ComputerEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="computerName"> The name of the computer. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string computerName )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ComputerBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ComputerNub.Label )} = @ComputerName", new { computerName } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="computerName"> The name of the computer. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntitiesAlternative( System.Data.IDbConnection connection, string computerName )
        {
            var selectSql = new System.Text.StringBuilder( $"SELECT * FROM [{ComputerBuilder.TableName}]" );
            _ = selectSql.Append( $"WHERE (@{nameof( ComputerNub.Label )} IS NULL OR {nameof( ComputerNub.Label )} = @computerName)" );
            _ = selectSql.Append( "; " );
            return connection.ExecuteScalar<int>( selectSql.ToString(), new { computerName } );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="computerName"> The name of the computer. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ComputerNub> FetchNubs( System.Data.IDbConnection connection, string computerName )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ComputerBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ComputerNub.Label )} = @ComputerName", new { computerName } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ComputerNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the computer exists. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="computerName"> The name of the computer. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string computerName )
        {
            return 1 == CountEntities( connection, computerName );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Computer. </summary>
        /// <value> Identifies the Computer. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the Computer number. </summary>
        /// <value> The Computer number. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ComputerEntity.ComputerName ) );
                }
            }
        }

        /// <summary>   Gets or sets the name of the computer. </summary>
        /// <value> The name of the computer. </value>
        public string ComputerName
        {
            get => this.Label;

            set => this.Label = value;
        }

        /// <summary>   Gets or sets the timezone identity. </summary>
        /// <remarks>
        /// The entity <see cref="KeyLabelTimezoneNub.TimezoneId"/> is derived from the
        /// <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name, which, in-fact, is the timezone
        /// identity. The Standard Name is used by the
        /// <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
        /// <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
        /// Name).
        /// </remarks>
        /// <value> The timezone identity. </value>
        public string TimezoneId
        {
            get => this.ICache.TimezoneId;

            set {
                if ( !string.Equals( this.TimezoneId, value ) )
                {
                    this.ICache.TimezoneId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ComputerEntity.ComputerName ) );
                }
            }
        }

        /// <summary>   Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTime.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " TIMESTAMP CAPTION "

        /// <summary>   Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        /// <summary>   The timestamp caption format. </summary>
        private string _TimestampCaptionFormat;

        /// <summary>   Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._TimestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._TimestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.Timestamp.ToString( this.TimestampCaptionFormat );

        #endregion

        #region " ENTITY LOCAL TIMES "

        /// <summary>   Information describing the locale time zone. </summary>
        private TimeZoneInfo _LocaleTimeZoneInfo;

        /// <summary>   Returns the information describing the time zone in the entity locale. </summary>
        /// <remarks>
        /// The entity <see cref="KeyLabelTimezoneNub.TimezoneId"/> is derived from the
        /// <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name The standard name is used by the
        /// <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
        /// <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
        /// Name).
        /// </remarks>
        /// <returns>   A TimeZoneInfo. </returns>
        public TimeZoneInfo LocaleTimeZoneInfo()
        {
            if ( this._LocaleTimeZoneInfo is null )
            {
                if ( string.IsNullOrWhiteSpace( this.TimezoneId ) )
                    this.TimezoneId = TimeZoneInfo.Local.Id;
                this._LocaleTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById( this.TimezoneId );
            }

            return this._LocaleTimeZoneInfo;
        }

        /// <summary>
        /// Returns the time now in the entity locale based on the Locale
        /// <see cref="System.TimeZoneInfo"/>.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The time now in the entity locale. </returns>
        public DateTimeOffset LocaleTimeNow()
        {
            return TimeZoneInfo.ConvertTime( DateTimeOffset.Now, this.LocaleTimeZoneInfo() );
        }

        /// <summary>   Returns the <see cref="Timestamp"/> in the entity local. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The <see cref="Timestamp"/> entity locale. </returns>
        public DateTimeOffset LocaleTimestamp()
        {
            return TimeZoneInfo.ConvertTimeFromUtc( this.Timestamp, this.LocaleTimeZoneInfo() );
        }

        /// <summary>   Converts an universalTime to a locale timestamp. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="universalTime">    The universal time. </param>
        /// <returns>   UniversalTime as a DateTimeOffset. </returns>
        public DateTimeOffset ToLocaleTimestamp( DateTime universalTime )
        {
            return TimeZoneInfo.ConvertTimeFromUtc( universalTime, this.LocaleTimeZoneInfo() );
        }

        #endregion

    }
}

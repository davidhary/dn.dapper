using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Platform-Station builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class PlatformStationBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( PlatformStationNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = PlatformBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( PlatformNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = StationBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( StationNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( PlatformStationEntity.PlatformAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( PlatformStationEntity.StationAutoId );

        #region " SINGLETON "

        private static readonly Lazy<PlatformStationBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static PlatformStationBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Platform Station Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "PlatformStation" )]
    public class PlatformStationNub : OneToManyNub, IOneToMany
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public PlatformStationNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new PlatformStationNub();
        }
    }

    /// <summary>
    /// The Platform-Station Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class PlatformStationEntity : EntityBase<IOneToMany, PlatformStationNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public PlatformStationEntity() : this( new PlatformStationNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PlatformStationEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public PlatformStationEntity( IOneToMany cache, IOneToMany store ) : base( new PlatformStationNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PlatformStationEntity( PlatformStationEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.PlatformStationBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new PlatformStationNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Platform-Station interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int platformAutoId, int stationAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, platformAutoId, stationAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PlatformAutoId, this.StationAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PlatformAutoId, this.StationAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int platformAutoId, int stationAutoId )
        {
            return this.FetchUsingKey( connection, platformAutoId, stationAutoId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int platformAutoId, int stationAutoId )
        {
            return connection.Delete( new PlatformStationNub() { PrimaryId = platformAutoId, SecondaryId = stationAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Platform-Station entities. </summary>
        /// <value> The Platform-Station entities. </value>
        public IEnumerable<PlatformStationEntity> PlatformStations { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PlatformStationEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<PlatformStationNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.PlatformStations = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( PlatformStationEntity.PlatformStations ) );
            return this.PlatformStations?.Any() == true ? this.PlatformStations.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PlatformStationEntity> Populate( IEnumerable<PlatformStationNub> nubs )
        {
            var l = new List<PlatformStationEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( PlatformStationNub nub in nubs )
                    l.Add( new PlatformStationEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PlatformStationEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<PlatformStationEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new PlatformStationNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new PlatformStationEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Station entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int platformAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PlatformStationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PlatformStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = platformAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PlatformStationEntity> FetchEntities( System.Data.IDbConnection connection, int platformAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PlatformStationBuilder.TableName}] WHERE {nameof( PlatformStationNub.PrimaryId )} = @Id", new { Id = platformAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<PlatformStationNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Station. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   The total number of entities by Station. </returns>
        public static int CountEntitiesByStation( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PlatformStationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PlatformStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Stations in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Stations in this
        /// collection.
        /// </returns>
        public static IEnumerable<PlatformStationEntity> FetchEntitiesByStation( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PlatformStationBuilder.TableName}] WHERE {nameof( PlatformStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<PlatformStationNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int platformAutoId, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PlatformStationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PlatformStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = platformAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PlatformStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PlatformStationNub> FetchNubs( System.Data.IDbConnection connection, int platformAutoId, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PlatformStationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PlatformStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = platformAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PlatformStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PlatformStationNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Platform Station exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int platformAutoId, int stationAutoId )
        {
            return 1 == CountEntities( connection, platformAutoId, stationAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Platform entity. </summary>
        /// <value> The Platform entity. </value>
        public PlatformEntity PlatformEntity { get; private set; }

        /// <summary>   Fetches Platform Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Platform Entity. </returns>
        public PlatformEntity FetchPlatformEntity( System.Data.IDbConnection connection )
        {
            var entity = new PlatformEntity();
            _ = entity.FetchUsingKey( connection, this.PlatformAutoId );
            this.PlatformEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Platforms associated with the specified <paramref name="stationAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   The total number of Platforms. </returns>
        public static int CountPlatforms( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.PlatformStationBuilder.TableName}] WHERE {nameof( PlatformStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Platforms associated with the specified <paramref name="stationAutoId"/>;
        /// expected a single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Platforms in this collection.
        /// </returns>
        public static IEnumerable<StationEntity> FetchPlatforms( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PlatformStationBuilder.TableName}] WHERE {nameof( PlatformStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<StationEntity>();
            foreach ( PlatformStationNub nub in connection.Query<PlatformStationNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new PlatformStationEntity( nub );
                l.Add( entity.FetchStationEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Platforms associated with the specified <paramref name="stationAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeletePlatforms( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.PlatformStationBuilder.TableName}] WHERE {nameof( PlatformStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the Station entity. </summary>
        /// <value> The Station entity. </value>
        public StationEntity StationEntity { get; private set; }

        /// <summary>   Fetches a Station Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Station Entity. </returns>
        public StationEntity FetchStationEntity( System.Data.IDbConnection connection )
        {
            var entity = new StationEntity();
            _ = entity.FetchUsingKey( connection, this.StationAutoId );
            this.StationEntity = entity;
            return entity;
        }

        /// <summary>   Count stations. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <returns>   The total number of stations. </returns>
        public static int CountStations( System.Data.IDbConnection connection, int platformAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.PlatformStationBuilder.TableName}] WHERE {nameof( PlatformStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = platformAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Stations in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Stations in this collection.
        /// </returns>
        public static IEnumerable<StationEntity> FetchStations( System.Data.IDbConnection connection, int platformAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PlatformStationBuilder.TableName}] WHERE {nameof( PlatformStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = platformAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<StationEntity>();
            foreach ( PlatformStationNub nub in connection.Query<PlatformStationNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new PlatformStationEntity( nub );
                l.Add( entity.FetchStationEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Station related to the specified Platform. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteStations( System.Data.IDbConnection connection, int platformAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.PlatformStationBuilder.TableName}] WHERE {nameof( PlatformStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = platformAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.StationEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <param name="stationLabel">     The Station label. </param>
        /// <returns>   The Station. </returns>
        public static StationEntity FetchStation( System.Data.IDbConnection connection, string selectQuery, int platformAutoId, string stationLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = platformAutoId, Label = stationLabel } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<StationNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new StationEntity() : new StationEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.StationEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformAutoId">   Identifies the <see cref="PlatformEntity"/>. </param>
        /// <param name="stationLabel">     The Station label. </param>
        /// <returns>   The Station. </returns>
        public static StationEntity FetchStation( System.Data.IDbConnection connection, int platformAutoId, string stationLabel )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [PlatformStation] on [PlatformStation].SecondaryId = [Station].AutoId where [PlatformStation].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{StationBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{StationBuilder.TableName}] Inner Join [{PlatformStationBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.PlatformStationBuilder.TableName}].{nameof( PlatformStationNub.SecondaryId )} = [{isr.Dapper.Entities.StationBuilder.TableName}].{nameof( StationNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.PlatformStationBuilder.TableName}].{nameof( PlatformStationNub.PrimaryId )} = @Id AND [{isr.Dapper.Entities.StationBuilder.TableName}].{nameof( StationNub.Label )} = @Label); " );
            return FetchStation( connection, queryBuilder.ToString(), platformAutoId, stationLabel );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PlatformStationEntity.PlatformAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Platform. </summary>
        /// <value> Identifies the Platform. </value>
        public int PlatformAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PlatformStationEntity.StationAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Station. </summary>
        /// <value> Identifies the Station. </value>
        public int StationAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

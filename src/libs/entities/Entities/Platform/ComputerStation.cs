using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using isr.Dapper.Entity;
using isr.Dapper.Entities.TrimExtensions;

namespace isr.Dapper.Entities
{

    /// <summary>   A Computer-Station builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ComputerStationBuilder : OneToOneBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ComputerStationNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = ComputerBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( ComputerNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = StationBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( StationNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( ComputerStationEntity.ComputerAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( ComputerStationEntity.StationAutoId );

        #region " SINGLETON "

        private static readonly Lazy<ComputerStationBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ComputerStationBuilder Instance => LazyBuilder.Value;

        #endregion

        #region " FULL VIEW "

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected override string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( OneToOneNub.PrimaryId )}] AS [{this.PrimaryIdFieldName}], 
                [{isr.Dapper.Entities.ComputerBuilder.TableName}].[{nameof( ComputerNub.Label )}] AS [{nameof( ComputerEntity.ComputerName )}], 
                [{nameof( OneToOneNub.SecondaryId )}] AS [{this.SecondaryIdFieldName}],
                [{isr.Dapper.Entities.StationBuilder.TableName}].[{nameof( StationNub.Label )}] AS [{nameof( StationEntity.StationName )}],
                [{isr.Dapper.Entities.StationBuilder.TableName}].[{nameof( StationNub.TimezoneId )}] AS [{nameof( StationEntity.TimezoneId )}],
                [{isr.Dapper.Entities.StationBuilder.TableName}].[{nameof( StationNub.Timestamp )}] AS [{nameof( StationEntity.Timestamp )}]
            FROM [{this.TableNameThis}]
                 INNER JOIN [{isr.Dapper.Entities.ComputerBuilder.TableName}] ON [{isr.Dapper.Entities.ComputerStationBuilder.TableName}].[{nameof( ComputerStationNub.PrimaryId )}] = [{isr.Dapper.Entities.ComputerBuilder.TableName}].[{nameof( ComputerNub.AutoId )}]
                 INNER JOIN [{isr.Dapper.Entities.StationBuilder.TableName}] ON [{isr.Dapper.Entities.ComputerStationBuilder.TableName}].[{nameof( ComputerStationNub.SecondaryId )}] = [{isr.Dapper.Entities.StationBuilder.TableName}].[{nameof( StationNub.AutoId )}]
            ".Clean();
        }

        #endregion

    }

    /// <summary>
    /// Implements the Computer Station Nub based on the <see cref="IOneToOne">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "ComputerStation" )]
    public class ComputerStationNub : OneToOneNub, IOneToOne
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ComputerStationNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToOne CreateNew()
        {
            return new ComputerStationNub();
        }
    }

    /// <summary>
    /// The Computer-Station Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class ComputerStationEntity : EntityBase<IOneToOne, ComputerStationNub>, IOneToOne
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ComputerStationEntity() : this( new ComputerStationNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ComputerStationEntity( IOneToOne value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ComputerStationEntity( IOneToOne cache, IOneToOne store ) : base( new ComputerStationNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ComputerStationEntity( ComputerStationEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ComputerStationBuilder.TableName, nameof( IOneToOne ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToOne CreateNew()
        {
            return new ComputerStationNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToOne CreateCopy()
        {
            var destination = this.CreateNew();
            OneToOneNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToOne value )
        {
            OneToOneNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Computer-Station interface. </param>
        public override void UpdateCache( IOneToOne value )
        {
            // first make the copy to notify of any property change.
            OneToOneNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int computerAutoId, int stationAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, computerAutoId, stationAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.ComputerAutoId, this.StationAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.ComputerAutoId, this.StationAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int computerAutoId, int stationAutoId )
        {
            return this.FetchUsingKey( connection, computerAutoId, stationAutoId );
        }

        /// <summary>   Obtains a default entity using the given connection. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   As. </returns>
        public (bool Success, string Details) ObtainDefault( System.Data.IDbConnection connection )
        {
            return this.TryObtain( connection, Environment.MachineName, Environment.MachineName, () => { return true; } );
        }

        /// <summary>
        /// Attempts to insert or retrieve a <see cref="ComputerStationEntity"/> from the given data.
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="computerName">         The name of the computer. </param>
        /// <param name="stationName">          Name of the station. </param>
        /// <param name="addRecordPredicate">   The add record predicate. Returns true to add the record.
        ///                                     Set as affirmative or open a message box requesting
        ///                                     operator confirmation. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, string computerName, string stationName, Func<bool> addRecordPredicate )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            if ( string.IsNullOrWhiteSpace( computerName ) )
                throw new ArgumentNullException( nameof( computerName ) );
            (bool Success, string Details) result = (true, string.Empty);
            var computerEntity = new ComputerEntity() { ComputerName = computerName, Timestamp = DateTime.UtcNow, TimezoneId = System.TimeZoneInfo.Local.StandardName };
            if ( computerEntity.TryObtain( connection, computerName, addRecordPredicate ) )
            {
                var stationEntity = new StationEntity() { StationName = stationName, Timestamp = DateTime.UtcNow, TimezoneId = System.TimeZoneInfo.Local.StandardName };
                if ( stationEntity.TryObtain( connection, stationName, addRecordPredicate ) )
                {
                    this.ComputerAutoId = computerEntity.AutoId;
                    this.StationAutoId = stationEntity.AutoId;
                    if ( this.Obtain( connection ) )
                    {
                        _ = this.FetchComputerEntity( connection );
                        _ = this.FetchStationEntity( connection );
                    }
                    else
                    {
                        result = (false, $"Failed obtaining {nameof( ComputerStationEntity )} for [{nameof( Dapper.Entities.ComputerEntity.AutoId )}, {nameof( Dapper.Entities.StationEntity.AutoId )}] of [{computerEntity.AutoId},{stationEntity.AutoId}]");
                    }
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( Dapper.Entities.StationEntity )} with {nameof( Dapper.Entities.StationEntity.StationName )} of {stationName}");
                }
            }
            else
            {
                result = (false, $"Failed obtaining {nameof( Dapper.Entities.ComputerEntity )} with {nameof( Dapper.Entities.ComputerEntity.ComputerName )} of {computerName}");
            }

            return result;
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToOne entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int computerAutoId, int stationAutoId )
        {
            return connection.Delete( new ComputerStationNub() { PrimaryId = computerAutoId, SecondaryId = stationAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Computer-Station entities. </summary>
        /// <value> The Computer-Station entities. </value>
        public IEnumerable<ComputerStationEntity> ComputerStations { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ComputerStationEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToOne>() ) : Populate( connection.GetAll<ComputerStationNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ComputerStations = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ComputerStationEntity.ComputerStations ) );
            return this.ComputerStations?.Any() == true ? this.ComputerStations.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ComputerStationEntity> Populate( IEnumerable<ComputerStationNub> nubs )
        {
            var l = new List<ComputerStationEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ComputerStationNub nub in nubs )
                    l.Add( new ComputerStationEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ComputerStationEntity> Populate( IEnumerable<IOneToOne> interfaces )
        {
            var l = new List<ComputerStationEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ComputerStationNub();
                foreach ( IOneToOne iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ComputerStationEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Station entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int computerAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ComputerStationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ComputerStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = computerAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ComputerStationEntity> FetchEntities( System.Data.IDbConnection connection, int computerAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ComputerStationBuilder.TableName}] WHERE {nameof( ComputerStationNub.PrimaryId )} = @Id", new { Id = computerAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ComputerStationNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Station. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   The total number of entities by Station. </returns>
        public static int CountEntitiesByStation( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ComputerStationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ComputerStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Stations in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Stations in this
        /// collection.
        /// </returns>
        public static IEnumerable<ComputerStationEntity> FetchEntitiesByStation( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ComputerStationBuilder.TableName}] WHERE {nameof( ComputerStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ComputerStationNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int computerAutoId, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ComputerStationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ComputerStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = computerAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ComputerStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ComputerStationNub> FetchNubs( System.Data.IDbConnection connection, int computerAutoId, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ComputerStationBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ComputerStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = computerAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ComputerStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ComputerStationNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Computer Station exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int computerAutoId, int stationAutoId )
        {
            return 1 == CountEntities( connection, computerAutoId, stationAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Computer entity. </summary>
        /// <value> The Computer entity. </value>
        public ComputerEntity ComputerEntity { get; private set; }

        /// <summary>   Fetches Computer Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Computer Entity. </returns>
        public ComputerEntity FetchComputerEntity( System.Data.IDbConnection connection )
        {
            var entity = new ComputerEntity();
            _ = entity.FetchUsingKey( connection, this.ComputerAutoId );
            this.ComputerEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Computers associated with the specified <paramref name="stationAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   The total number of Computers. </returns>
        public static int CountComputers( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.ComputerStationBuilder.TableName}] WHERE {nameof( ComputerStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Computers associated with the specified <paramref name="stationAutoId"/>;
        /// expected a single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Computers in this collection.
        /// </returns>
        public static IEnumerable<StationEntity> FetchComputers( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ComputerStationBuilder.TableName}] WHERE {nameof( ComputerStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<StationEntity>();
            foreach ( ComputerStationNub nub in connection.Query<ComputerStationNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new ComputerStationEntity( nub );
                l.Add( entity.FetchStationEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Computers associated with the specified <paramref name="stationAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="stationAutoId">    Identifies the <see cref="StationEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteComputers( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.ComputerStationBuilder.TableName}] WHERE {nameof( ComputerStationNub.SecondaryId )} = @SecondaryId", new { SecondaryId = stationAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the Station entity. </summary>
        /// <value> The Station entity. </value>
        public StationEntity StationEntity { get; private set; }

        /// <summary>   Fetches a Station Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Station Entity. </returns>
        public StationEntity FetchStationEntity( System.Data.IDbConnection connection )
        {
            var entity = new StationEntity();
            _ = entity.FetchUsingKey( connection, this.StationAutoId );
            this.StationEntity = entity;
            return entity;
        }

        /// <summary>   Count stations. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <returns>   The total number of stations. </returns>
        public static int CountStations( System.Data.IDbConnection connection, int computerAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.ComputerStationBuilder.TableName}] WHERE {nameof( ComputerStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = computerAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Stations in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Stations in this collection.
        /// </returns>
        public static IEnumerable<StationEntity> FetchStations( System.Data.IDbConnection connection, int computerAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ComputerStationBuilder.TableName}] WHERE {nameof( ComputerStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = computerAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<StationEntity>();
            foreach ( ComputerStationNub nub in connection.Query<ComputerStationNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new ComputerStationEntity( nub );
                l.Add( entity.FetchStationEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Station related to the specified Computer. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="computerAutoId">   Identifies the <see cref="ComputerEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteStations( System.Data.IDbConnection connection, int computerAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.ComputerStationBuilder.TableName}] WHERE {nameof( ComputerStationNub.PrimaryId )} = @PrimaryId", new { PrimaryId = computerAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ComputerStationEntity.ComputerAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Computer. </summary>
        /// <value> Identifies the Computer. </value>
        public int ComputerAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ComputerStationEntity.StationAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Station. </summary>
        /// <value> Identifies the Station. </value>
        public int StationAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

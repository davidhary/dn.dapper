using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Sample Trait Type builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class SampleTraitTypeBuilder : NominalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( SampleTraitTypeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>
        /// Inserts or ignore the records as described by the <see cref="Dapper.Entities.SampleTraitType"/>
        /// enumeration type.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public override int InsertIgnoreDefaultRecords( System.Data.IDbConnection connection )
        {
            return this.InsertIgnore( connection, typeof( SampleTraitType ), new int[] { ( int ) SampleTraitType.None } );
        }

        #region " SINGLETON "

        private static readonly Lazy<SampleTraitTypeBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SampleTraitTypeBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="SampleTraitTypeNub"/> table based on the
    /// <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "SampleTraitType" )]
    public class SampleTraitTypeNub : NominalNub, INominal
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SampleTraitTypeNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new SampleTraitTypeNub();
        }
    }


    /// <summary>
    /// The <see cref="SampleTraitTypeNub"/> Entity table based on the
    /// <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class SampleTraitTypeEntity : EntityBase<INominal, SampleTraitTypeNub>, INominal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SampleTraitTypeEntity() : this( new SampleTraitTypeNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SampleTraitTypeEntity( INominal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public SampleTraitTypeEntity( INominal cache, INominal store ) : base( new SampleTraitTypeNub(), cache, store )
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.SampleTraitTypeBuilder.TableName, nameof( INominal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new SampleTraitTypeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override INominal CreateCopy()
        {
            var destination = this.CreateNew();
            NominalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( INominal value )
        {
            NominalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The SampleTraitType interface. </param>
        public override void UpdateCache( INominal value )
        {
            // first make the copy to notify of any property change.
            NominalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The SampleTraitType table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<INominal>( key ) : connection.Get<SampleTraitTypeNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Label );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Sample Trait Type label. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string label )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, label ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, INominal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Label ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new SampleTraitTypeNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the SampleTraitType entities. </summary>
        /// <value> The SampleTraitType entities. </value>
        public static IEnumerable<SampleTraitTypeEntity> SampleTraitTypes { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SampleTraitTypeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<INominal>() ) : Populate( connection.GetAll<SampleTraitTypeNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            SampleTraitTypes = FetchAllEntities( connection, this.UsingNativeTracking );
            return SampleTraitTypes?.Any() == true ? SampleTraitTypes.Count() : 0;
        }

        /// <summary>   Populates a list of SampleTraitType entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The SampleTraitType nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SampleTraitTypeEntity> Populate( IEnumerable<SampleTraitTypeNub> nubs )
        {
            var l = new List<SampleTraitTypeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( SampleTraitTypeNub nub in nubs )
                    l.Add( new SampleTraitTypeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of SampleTraitType entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The SampleTraitType interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SampleTraitTypeEntity> Populate( IEnumerable<INominal> interfaces )
        {
            var l = new List<SampleTraitTypeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new SampleTraitTypeNub();
                foreach ( INominal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new SampleTraitTypeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, SampleTraitTypeEntity> _EntityLookupDictionary;

        /// <summary>   The entity lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A Dictionary(Of Integer, SampleTraitTypeEntity) </returns>
        public static IDictionary<int, SampleTraitTypeEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, SampleTraitTypeEntity>();
                foreach ( SampleTraitTypeEntity entity in SampleTraitTypes )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Dictionary of key lookups. </summary>
        private static IDictionary<string, int> _KeyLookupDictionary;

        /// <summary>   The key lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A Dictionary(Of String, Integer) </returns>
        public static IDictionary<string, int> KeyLookupDictionary()
        {
            if ( !(_KeyLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _KeyLookupDictionary = new Dictionary<string, int>();
                foreach ( SampleTraitTypeEntity entity in SampleTraitTypes )
                    _KeyLookupDictionary.Add( entity.Label, entity.Id );
            }

            return _KeyLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (SampleTraitTypes?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) &&
                   (_KeyLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Sample Trait Type label. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.SampleTraitTypeBuilder.TableName}] WHERE {nameof( SampleTraitTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Sample Trait Type label. </param>
        /// <returns>   An ISampleTraitType . </returns>
        public static IEnumerable<SampleTraitTypeNub> FetchNubs( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.SampleTraitTypeBuilder.TableName}] WHERE {nameof( SampleTraitTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SampleTraitTypeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the SampleTraitType exists. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The Sample Trait Type label. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string label )
        {
            return 1 == CountEntities( connection, label );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Sample Trait Type. </summary>
        /// <value> Identifies the Sample Trait Type. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the Sample Trait Type label. </summary>
        /// <value> The Sample Trait Type label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the description of the Sample Trait Type. </summary>
        /// <value> The Sample Trait Type description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="sampleTraitTypeId">    The entity unique key. </param>
        /// <returns>   A <see cref="SampleTraitTypeEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, SampleTraitTypeEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int sampleTraitTypeId )
        {
            string activity = string.Empty;
            var result = new SampleTraitTypeEntity();
            try
            {
                activity = $"Fetching {nameof( SampleTraitTypeEntity )} by {nameof( SampleTraitTypeNub.Id )} of {sampleTraitTypeId}";
                return result.FetchUsingKey( connection, sampleTraitTypeId )
                    ? (true, string.Empty, result)
                    : (false, $"Failed {activity}", result);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", result);
            }
        }

        /// <summary>   Try fetch using unique index. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="sampleTraitTypeLabel"> The Sample Trait Type label. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entity As SampleTraitTypeEntity)
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, SampleTraitTypeEntity Entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, string sampleTraitTypeLabel )
        {
            string activity = string.Empty;
            var entity = new SampleTraitTypeEntity();
            try
            {
                activity = $"Fetching {nameof( SampleTraitTypeEntity )} by {nameof( SampleTraitTypeNub.Label )} of {sampleTraitTypeLabel}";
                return entity.FetchUsingUniqueIndex( connection, sampleTraitTypeLabel ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entities As IEnumerable(Of SampleTraitTypeEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, IEnumerable<SampleTraitTypeEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new SampleTraitTypeEntity();
                activity = $"fetching all {nameof( SampleTraitType )}s";
                int SampleCount = entity.FetchAllEntities( connection );
                if ( SampleCount != EntityLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( SampleTraitTypeEntity.EntityLookupDictionary )} count must equal {nameof( SampleTraitTypes )} count " );
                }
                else if ( SampleCount != KeyLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( SampleTraitTypeEntity.KeyLookupDictionary )} count must equal {nameof( SampleTraitTypes )} count " );
                }

                return (true, string.Empty, SampleTraitTypes);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<SampleTraitTypeEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }

    /// <summary>   Values that represent Sample Trait Types. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public enum SampleTraitType
    {

        /// <summary>   An enum constant representing the none option </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary>   An enum constant representing the Nominal Value option </summary>
        [System.ComponentModel.Description( "Nominal Value" )]
        NominalValue = 1,

        /// <summary>   An enum constant representing the Target Value option </summary>
        [System.ComponentModel.Description( "Target Value" )]
        TargetValue = 2,

        /// <summary>   An enum constant representing the Lower Specification Limit (LSL) option </summary>
        [System.ComponentModel.Description( "Lower Specification Limit (LSL)" )]
        LowerSpecificationLimit = 3,

        /// <summary>   An enum constant representing the Upper Specification Limit (USL) option </summary>
        [System.ComponentModel.Description( "Upper Specification Limit (USL)" )]
        UpperSpecificationLimit = 4,

        /// <summary>   An enum constant representing the Mean option </summary>
        [System.ComponentModel.Description( "Mean" )]
        Mean = 5,

        /// <summary>   An enum constant representing the Sigma option </summary>
        [System.ComponentModel.Description( "Sigma" )]
        Sigma = 6,

        /// <summary>   An enum constant representing the Sample Count option </summary>
        [System.ComponentModel.Description( "Sample Count" )]
        SampleCount = 7,

        /// <summary>   An enum constant representing the First Quartile option </summary>
        [System.ComponentModel.Description( "First Quartile" )]
        FirstQuartile = 8,

        /// <summary>   An enum constant representing the Median option </summary>
        [System.ComponentModel.Description( "Median" )]
        Median = 9,

        /// <summary>   An enum constant representing the Third Quartile option </summary>
        [System.ComponentModel.Description( "Third Quartile" )]
        ThirdQuartile = 10,

        /// <summary>   An enum constant representing the Minimum option </summary>
        [System.ComponentModel.Description( "Minimum" )]
        Minimum = 11,

        /// <summary>   An enum constant representing the Maximum option </summary>
        [System.ComponentModel.Description( "Maximum" )]
        Maximum = 12,

        /// <summary>   An enum constant representing the Fence Factor option </summary>
        [System.ComponentModel.Description( "Fence Factor" )]
        FenceFactor = 13,

        /// <summary>   An enum constant representing the Lower Outlier Limit option </summary>
        [System.ComponentModel.Description( "Lower Outlier Limit" )]
        LowerOutlierLimit = 14,

        /// <summary>   An enum constant representing the Upper Outlier Limit option </summary>
        [System.ComponentModel.Description( "Upper Outlier Limit" )]
        UpperOutlierLimit = 15,

        /// <summary>   An enum constant representing the Outlier Count option </summary>
        [System.ComponentModel.Description( "Outlier Count" )]
        OutlierCount = 16,

        /// <summary>   An enum constant representing the Minimum Cp option </summary>
        [System.ComponentModel.Description( "Minimum Cp" )]
        MinimumCp = 17,

        /// <summary>   An enum constant representing the Minimum Cpk option </summary>
        [System.ComponentModel.Description( "Minimum Cpk" )]
        MinimumCpk = 18,

        /// <summary>   An enum constant representing the Minimum Cpm option </summary>
        [System.ComponentModel.Description( "Minimum Cpm" )]
        MinimumCpm = 19,

        /// <summary>   An enum constant representing the Cp option </summary>
        [System.ComponentModel.Description( "Cp" )]
        Cp = 20,

        /// <summary>   An enum constant representing the Cpk option </summary>
        [System.ComponentModel.Description( "Cpk" )]
        Cpk = 21,

        /// <summary>   An enum constant representing the Cpm option </summary>
        [System.ComponentModel.Description( "Cpm" )]
        Cpm = 22,

        /// <summary>   An enum constant representing the Process Target option </summary>
        [System.ComponentModel.Description( "Process Target" )]
        ProcessTarget = 23,

        /// <summary>   An enum constant representing the Process Mean option </summary>
        [System.ComponentModel.Description( "Process Mean" )]
        ProcessMean = 24,

        /// <summary>   An enum constant representing the Initial Process Nominal option </summary>
        [System.ComponentModel.Description( "Initial Process Nominal" )]
        InitialProcessNominal = 25,

        /// <summary>   An enum constant representing the Process Nominal option </summary>
        [System.ComponentModel.Description( "Process Nominal" )]
        ProcessNominal = 26,

        /// <summary>   An enum constant representing the Process Error option </summary>
        [System.ComponentModel.Description( "Process Error" )]
        ProcessError = 27,

        /// <summary>   An enum constant representing the Adjusted Process Nominal option </summary>
        [System.ComponentModel.Description( "Adjusted Process Nominal" )]
        AdjustedProcessNominal = 28,

        /// <summary>   An enum constant representing the Process Bias option </summary>
        [System.ComponentModel.Description( "Process Bias" )]
        ProcessBias = 29,

        /// <summary>   An enum constant representing the Process Bias Mean option </summary>
        [System.ComponentModel.Description( "Process Bias Mean" )]
        ProcessBiasMean = 30,

        /// <summary>   An enum constant representing the Process Bias Error option </summary>
        [System.ComponentModel.Description( "Process Bias Error" )]
        ProcessBiasError = 31,

        /// <summary>   An enum constant representing the total count option. </summary>
        [System.ComponentModel.Description( "Total Count" )]
        TotalCount = 32,

        /// <summary>   An enum constant representing the invalid count option. </summary>
        [System.ComponentModel.Description( "Invalid Count" )]
        InvalidCount = 33,

        /// <summary>   An enum constant representing the good count option. </summary>
        [System.ComponentModel.Description( "Good Count" )]
        GoodCount = 34,

        /// <summary>   An enum constant representing the failed count option. </summary>
        [System.ComponentModel.Description( "Failed Count" )]
        FailedCount = 35,

        /// <summary>   An enum constant representing the yield option. </summary>
        [System.ComponentModel.Description( "Yield" )]
        Yield = 36,

        /// <summary>   An enum constant representing the valid yield option. </summary>
        [System.ComponentModel.Description( "Valid Yield" )]
        ValidYield = 37,

        /// <summary>   An enum constant representing the yield decimal places option. </summary>
        [System.ComponentModel.Description( "Yield Decimal Places " )]
        YieldDecimalPlaces = 38
    }
}

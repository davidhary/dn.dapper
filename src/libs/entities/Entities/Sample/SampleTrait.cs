using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// The Sample Trait class holing the values for the sample of units under test.
    /// </summary>
    /// <remarks>
    /// David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class SampleTrait : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSetter">         The getter setter. </param>
        /// <param name="element">              The <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">            Identifier for the
        ///                                     <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="meterId">              Identifier for the
        ///                                     <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="certifiedQuantity">    (Optional) The certified quantity. </param>
        public SampleTrait( Std.Primitives.IGetterSetter<double> getterSetter, ElementEntity element, int nomTypeId,
                            int meterId, int certifiedQuantity = 0 ) : base()
        {
            this.GetterSetter = getterSetter;
            this.ElementAutoId = element.AutoId;
            this.ElementLabel = element.ElementLabel;
            this.ElementType = ( ElementType ) element.ElementTypeId;
            this.ElementOrdinalNumber = element.OrdinalNumber;
            this.NomType = ( NomType ) nomTypeId;
            this.MeterId = meterId;
            this.CertifiedQuantity = certifiedQuantity;
            this.NominalValue = element.NomTraitsCollection[new MeterNomTypeSelector( meterId, nomTypeId )].NomTrait.NominalValue;
            this.TargetValue = this.NominalValue;
            this.LowerSpecificationLimit = element.NomTraitsCollection[new MeterNomTypeSelector( meterId, nomTypeId )].NomTrait.LowerSpecificationLimit;
            this.UpperSpecificationLimit = element.NomTraitsCollection[new MeterNomTypeSelector( meterId, nomTypeId )].NomTrait.UpperSpecificationLimit;
            this.Selector = new MeterElementNomTypeSelector( this );
            this.Sample = new Std.Statistics.SampleStatistics();
        }

        /// <summary>   Gets or sets the selector. </summary>
        /// <value> The selector. </value>
        public MeterElementNomTypeSelector Selector { get; private set; }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<double> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) The name. </param>
        /// <returns>   The trait value. </returns>
        protected double? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void Setter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }
        }

        #endregion

        #region " IDENTIFIERS "

        /// <summary>   Type of the nom. </summary>
        private NomType _NomType;

        /// <summary>   Gets or sets the type of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> The type of the nominal. </value>
        public NomType NomType
        {
            get => this._NomType;

            set {
                if ( value != this.NomType )
                {
                    this._NomType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _MeterId;

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId
        {
            get => this._MeterId;

            set {
                if ( value != this.MeterId )
                {
                    this._MeterId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Identifies the element automatic. </summary>
        private int _ElementAutoId;

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int ElementAutoId
        {
            get => this._ElementAutoId;

            set {
                if ( value != this.ElementAutoId )
                {
                    this._ElementAutoId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " STATISTICS "

        /// <summary>   Gets or sets the sample. </summary>
        /// <value> The sample. </value>
        public Std.Statistics.SampleStatistics Sample { get; private set; }

#if false
        /// <summary>   Gets or sets the sample quartiles. </summary>
        /// <value> The sample quartiles. </value>
        public Std.Statistics.SampleQuartiles SampleQuartiles { get; private set; }

        /// <summary>   Gets or sets the sample skewness. </summary>
        /// <value> The sample skewness. </value>
        public Std.Statistics.SampleSkewness SampleSkewness { get; private set; }

        /// <summary>   Gets or sets the outliers calculator method. </summary>
        /// <value> The outliers calculator method. </value>
        public OutliersCalculatorMethod OutliersCalculatorMethod { get; set; }

#endif

        /// <summary>   Clears this object to its blank/initial state. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public void Clear()
        {
            this.SampleCount = new int?();
            this.OutlierCount = new int?();
            this.Mean = new double?();
            this.Sigma = new double?();
            this.FirstQuartile = new double?();
            this.Median = new double?();
            this.ThirdQuartile = new double?();
            this.Minimum = new double?();
            this.Maximum = new double?();
            this.FenceFactor = new double?();
            this.LowerOutlierLimit = new double?();
            this.UpperOutlierLimit = new double?();
            this.Cp = new double?();
            this.Cpk = new double?();
            this.Cpm = new double?();
        }


        /// <summary>   Evaluates the sample quartiles and apply. </summary>
        /// <remarks>   David, 2020-05-29. </remarks>
        public void Evaluate( OutliersCalculatorMethod outliersCalculatorMethod )
        {
            this.Clear();
            // evaluate the sample statistics.
            this.SampleCount = this.Sample.Count;
            if ( this.Sample.Count >= 2 )
            {
                this.Sample.Evaluate();
                this.Mean = this.Sample.Mean;
                this.Sigma = this.Sample.Sigma;
                this.Minimum = this.Sample.Minimum;
                this.Maximum = this.Sample.Maximum;
            }
            if ( outliersCalculatorMethod == OutliersCalculatorMethod.UsingSkewness )
            {
                if ( this.Sample.Count >= isr.Std.Statistics.SampleSkewness.MinimumSampleSize )
                {
                    double criticalValue = isr.Std.Statistics.SampleSkewness.CriticalValue( this.Sample.Count );
                    Std.Statistics.SampleSkewness sampleSkewness = new( this.Sample );
                    sampleSkewness.LowerFence = -criticalValue;
                    sampleSkewness.UpperFence = criticalValue;
                    sampleSkewness.FilterSample();
                    this.OutlierCount = sampleSkewness.OutlierCount;

                    Std.Statistics.SampleQuartiles filteredSampleQuarties = new( sampleSkewness.FilteredSample );
                    filteredSampleQuarties.Sample.Evaluate();
                    filteredSampleQuarties.Evaluate();

                    this.SampleCount = filteredSampleQuarties.Sample.Count;
                    this.Mean = filteredSampleQuarties.Sample.Mean;
                    this.Sigma = filteredSampleQuarties.Sample.Sigma;
                    this.Minimum = filteredSampleQuarties.Sample.Minimum;
                    this.Maximum = filteredSampleQuarties.Sample.Maximum;
                    this.FirstQuartile = filteredSampleQuarties.Quartiles.First;
                    this.Median = filteredSampleQuarties.Quartiles.Median;
                    this.ThirdQuartile = filteredSampleQuarties.Quartiles.Third;
                }
            }
            else if ( outliersCalculatorMethod == OutliersCalculatorMethod.UsingTukeyFences )
            {
                Std.Statistics.SampleQuartiles sampleQuartiles = new( this.Sample );
                if ( this.Sample.Count >= sampleQuartiles.Quartiles.MinimumLength )
                {
                    sampleQuartiles.Evaluate();
                    sampleQuartiles.FilterSample();
                    this.OutlierCount = sampleQuartiles.OutlierCount;
                    this.FenceFactor = sampleQuartiles.FenceFactor;
                    this.LowerOutlierLimit = sampleQuartiles.LowerFence;
                    this.UpperOutlierLimit = sampleQuartiles.UpperFence;

                    Std.Statistics.SampleQuartiles filteredSampleQuarties = new( sampleQuartiles.FilteredSample );
                    filteredSampleQuarties.Sample.Evaluate();
                    filteredSampleQuarties.Evaluate();

                    this.SampleCount = filteredSampleQuarties.Sample.Count;
                    this.Mean = filteredSampleQuarties.Sample.Mean;
                    this.Sigma = filteredSampleQuarties.Sample.Sigma;
                    this.Minimum = filteredSampleQuarties.Sample.Minimum;
                    this.Maximum = filteredSampleQuarties.Sample.Maximum;
                    this.FirstQuartile = filteredSampleQuarties.Quartiles.First;
                    this.Median = filteredSampleQuarties.Quartiles.Median;
                    this.ThirdQuartile = filteredSampleQuarties.Quartiles.Third;
                }
            }
            this.Cp = this.EstimateCp();
            this.Cpk = this.EstimateCpk();
            this.Cpm = this.EstimateCpm();
        }

        /// <summary>   Adds an item. </summary>
        /// <remarks>   David, 2020-07-07. </remarks>
        /// <param name="value">    The value. </param>
        public void AddItem( double? value )
        {
            if ( value.HasValue )
                this.Sample.AddValue( value.Value );
        }

        #endregion

        #region " ADDITIONAL PROPERTIES "

        /// <summary>   The certified quantity. </summary>
        private int _CertifiedQuantity;

        /// <summary>   Gets or sets the certified quantity. </summary>
        /// <value> The certified quantity. </value>
        public int CertifiedQuantity
        {
            get => this._CertifiedQuantity;

            set {
                if ( value != this.CertifiedQuantity )
                {
                    this._CertifiedQuantity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " ELEMENT PROPERTIES "

        /// <summary>   The element label. </summary>
        private string _ElementLabel;

        /// <summary>   Gets or sets the Label of the Element. </summary>
        /// <value> The Label of the Element. </value>
        public string ElementLabel
        {
            get => this._ElementLabel;

            set {
                if ( !string.Equals( value, this.ElementLabel ) )
                {
                    this._ElementLabel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Type of the element. </summary>
        private ElementType _ElementType;

        /// <summary>   Gets or sets the type of the Element. </summary>
        /// <value> The type of the Element. </value>
        public ElementType ElementType
        {
            get => this._ElementType;

            set {
                if ( value != this.ElementType )
                {
                    this._ElementType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The element ordinal number. </summary>
        private int _ElementOrdinalNumber;

        /// <summary>   Gets or sets the Element Ordinal number. </summary>
        /// <value> The ElementOrdinal number. </value>
        public int ElementOrdinalNumber
        {
            get => this._ElementOrdinalNumber;

            set {
                if ( !Equals( value, this.ElementOrdinalNumber ) )
                {
                    this._ElementOrdinalNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        #endregion

        #region " DERIVED TRAITS "

        private MeterEntity _Meter;

        /// <summary>   Gets the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public MeterEntity Meter
        {
            get {
                if ( !(this._Meter?.IsClean()).GetValueOrDefault( false ) )
                {
                    this._Meter = MeterEntity.EntityLookupDictionary()[this.MeterId];
                }

                return this._Meter;
            }
        }

        private MeterModelEntity _MeterModel;

        /// <summary>   Gets the Meter Model entity. </summary>
        /// <value> The Meter Model entity. </value>
        public MeterModelEntity MeterModel
        {
            get {
                if ( !(this._MeterModel?.IsClean()).GetValueOrDefault( false ) )
                {
                    this._MeterModel = MeterModelEntity.EntityLookupDictionary()[this.Meter.MeterModelId];
                }

                return this._MeterModel;
            }
        }

        #endregion

        #region " PRODUCT SPECIFICATION  "

        /// <summary>   Gets or sets the nominal value. </summary>
        /// <value> The nominal value. </value>
        public double? NominalValue
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets target value. </summary>
        /// <value> The target value. </value>
        public double? TargetValue
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the lower specification limit. </summary>
        /// <value> The lower specification limit. </value>
        public double? LowerSpecificationLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the upper specification limit. </summary>
        /// <value> The upper specification limit. </value>
        public double? UpperSpecificationLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        #endregion

        #region " SAMPLE QUARTILE COUNTS "

        /// <summary>   Gets or sets the number of samples. </summary>
        /// <value> The number of samples. </value>
        public int? SampleCount
        {
            get => ( int? ) this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the number of outliers. </summary>
        /// <value> The number of outliers. </value>
        public int? OutlierCount
        {
            get => ( int? ) this.Getter();

            set => this.Setter( value );
        }

        #endregion

        #region " SAMPLE STATISTICS "

        /// <summary>   Gets or sets the mean. </summary>
        /// <value> The mean value. </value>
        public double? Mean
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Mean percent caption. </summary>
        /// <remarks>   David, 2020-06-11. </remarks>
        /// <param name="numberFormatInfo"> Number of format information
        ///                                 <see cref="NomTrait.PercentFormatInfo"/>. </param>
        /// <returns>   A String. </returns>
        public string MeanPercentCaption( IFormatProvider numberFormatInfo )
        {
            return (this.SampleCount > 0 && this.Mean.HasValue) == true ? this.Mean.Value.ToString( "P", numberFormatInfo ) : string.Empty;
        }

        /// <summary>   Mean percent caption. </summary>
        /// <remarks>   David, 2020-06-11. </remarks>
        /// <returns>   A String. </returns>
        public string MeanPercentCaption()
        {
            return this.MeanPercentCaption( this.PercentFormatInfo );
        }

        /// <summary>   Gets or sets the sigma. </summary>
        /// <value> The sigma. </value>
        public double? Sigma
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Standard deviation percent caption. </summary>
        /// <remarks>   David, 2020-06-11. </remarks>
        /// <param name="numberFormatInfo"> Number of format information
        ///                                 <see cref="NomTrait.PercentFormatInfo"/>. </param>
        /// <returns>   A String. </returns>
        public string SigmaPercentCaption( IFormatProvider numberFormatInfo )
        {
            return (this.SampleCount >= 3 && this.Sigma.HasValue) == true ? this.Sigma.Value.ToString( "P", numberFormatInfo ) : string.Empty;
        }

        /// <summary>   Standard deviation percent caption. </summary>
        /// <remarks>   David, 2020-06-11. </remarks>
        /// <returns>   A String. </returns>
        public string SigmaPercentCaption()
        {
            return this.SigmaPercentCaption( this.PercentFormatInfo );
        }


        /// <summary>   Gets or sets the first quartile. </summary>
        /// <value> The first quartile. </value>
        public double? FirstQuartile
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the median. </summary>
        /// <value> The median value. </value>
        public double? Median
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the third quartile. </summary>
        /// <value> The third quartile. </value>
        public double? ThirdQuartile
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the minimum. </summary>
        /// <value> The minimum value. </value>
        public double? Minimum
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the maximum. </summary>
        /// <value> The maximum value. </value>
        public double? Maximum
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the fence factor. </summary>
        /// <value> The fence factor. </value>
        public double? FenceFactor
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the lower outlier limit. </summary>
        /// <value> The lower outlier limit. </value>
        public double? LowerOutlierLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the upper outlier limit. </summary>
        /// <value> The upper outlier limit. </value>
        public double? UpperOutlierLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        #endregion

        #region " PROCESS CAPABILITY "

        /// <summary>   Gets or sets the minimum Cp (process capability for a centered process). </summary>
        /// <value> The minimum Cp. </value>
        public double? MinimumCp
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>
        /// Gets or sets the minimum Cpk (process capability for a process that is not centered).
        /// </summary>
        /// <value> The minimum Cpk. </value>
        public double? MinimumCpk
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the minimum Cpm (process capability with bias consideration). </summary>
        /// <value> The minimum Cpm. </value>
        public double? MinimumCpm
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   The sigma level. </summary>
        private double _SigmaLevel = 6d;

        /// <summary>   Gets or sets the sigma level. </summary>
        /// <value> The sigma level. </value>
        public double SigmaLevel
        {
            get => this._SigmaLevel;

            set {
                if ( !(value == this.SigmaLevel) )
                {
                    this._SigmaLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets the Cp. </summary>
        /// <value> The Cp. </value>
        public double? Cp
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Estimate cp. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateCp()
        {
            return this.Sigma.GetValueOrDefault( 0d ) > 0d
                                ? (this.UpperSpecificationLimit - this.LowerSpecificationLimit) / (this.SigmaLevel * this.Sigma)
                                : new double?();
        }

        /// <summary>   Gets or sets the Cpk. </summary>
        /// <value> The cpk. </value>
        public double? Cpk
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Estimate Cpk. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateCpk()
        {
            return this.Sigma.GetValueOrDefault( 0d ) > 0d
                                ? (this.UpperSpecificationLimit - this.Mean < this.Mean - this.LowerSpecificationLimit == true
                                    ? this.UpperSpecificationLimit - this.Mean
                                    : this.Mean - this.LowerSpecificationLimit) / (0.5d * this.SigmaLevel * this.Sigma)
                                : new double?();
        }

        /// <summary>   Gets or sets the Cpm. </summary>
        /// <value> The Cpm. </value>
        public double? Cpm
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Estimate Cpm. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateCpm()
        {
            return this.Sigma.GetValueOrDefault( 0d ) > 0d && this.Mean.HasValue && this.TargetValue.HasValue
                            ? this.Cp / Math.Sqrt( 1d + Math.Pow( (this.Mean.Value - this.TargetValue.Value) / this.Sigma.Value, 2d ) )
                            : new double?();
        }

        #endregion

        #region " PROCESS TUNE "

        /// <summary>   Gets or sets the initial process nominal. </summary>
        /// <value> The initial process nominal. </value>
        public double? InitialProcessNominal
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the process nominal. </summary>
        /// <value> The process nominal. </value>
        public double? ProcessNominal
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the process target. </summary>
        /// <value> The process target. </value>
        public double? ProcessTarget
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the process mean. </summary>
        /// <value> The process mean. </value>
        public double? ProcessMean
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the process error. </summary>
        /// <value> The process error. </value>
        public double? ProcessError
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Estimate process error. </summary>
        /// <remarks>   David, 2020-05-29. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateProcessError()
        {
            return this.Mean - this.ProcessTarget;
        }

        /// <summary>   Gets or sets the adjusted process nominal. </summary>
        /// <value> The adjusted process nominal. </value>
        public double? AdjustedProcessNominal
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Estimate adjusted process nominal. </summary>
        /// <remarks>   David, 2020-05-29. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateAdjustedProcessNominal()
        {
            return this.ProcessNominal - this.ProcessError;
        }

        /// <summary>   Gets or sets the Process Bias (deltum standard). </summary>
        /// <value> The Process Bias (deltum standard). </value>
        public double? ProcessBias
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Estimate process bias. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateProcessBias()
        {
            return this.NominalValue - this.ProcessTarget;
        }

        /// <summary>   Gets or sets the average (actual) process bias. </summary>
        /// <value> The average (actual) process bias. </value>
        public double? ProcessBiasMean
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Estimate process bias mean. </summary>
        /// <remarks>   David, 2020-05-29. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateProcessBiasMean()
        {
            return this.Mean - this.ProcessMean;
        }

        /// <summary>   Gets or sets the Process Bias (deltum) error. </summary>
        /// <value> The deltum error. </value>
        public double? ProcessBiasError
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Estimate process bias error. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateProcessBiasError()
        {
            return this.ProcessBiasMean - this.ProcessBias;
        }


        #endregion

        #region " CUSTOM PROPERTIES "

        /// <summary>   The specification limit. </summary>
        private double? _SpecificationLimit;

        /// <summary>   Gets or sets the specification limit. </summary>
        /// <value> The specification limit. </value>
        public double? SpecificationLimit
        {
            get => this._SpecificationLimit;

            set {
                if ( !Equals( value, this.SpecificationLimit ) )
                {
                    this._SpecificationLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets the decimal precision. </summary>
        /// <value> The decimal precision. </value>
        public int DecimalPrecision
        {
            get {
                double lowestTolerance = 0.00005d;
                double precision = 1d / Math.Max( lowestTolerance, this.SpecificationLimit.GetValueOrDefault( lowestTolerance ) );
                return precision <= 1d ? 0 : ( int ) Math.Floor( Math.Log10( precision ) );
            }
        }

        /// <summary>   Information describing the percent format. </summary>
        private System.Globalization.NumberFormatInfo _PercentFormatInfo;

        /// <summary>
        /// Number format informations for displaying values with the set specification limit.
        /// </summary>
        /// <value> Information describing the percent format. </value>
        public System.Globalization.NumberFormatInfo PercentFormatInfo
        {
            get {
                if ( this._PercentFormatInfo is null )
                {
                    this._PercentFormatInfo = new System.Globalization.NumberFormatInfo() { PercentDecimalDigits = DecimalPrecision };
                }

                return this._PercentFormatInfo;
            }
        }

        /// <summary>   Number of format informations. </summary>
        private System.Globalization.NumberFormatInfo _NumberFormatInfo;

        /// <summary>
        /// Number format informations for displaying values with the set specification limit.
        /// </summary>
        /// <value> The total number of format information. </value>
        public System.Globalization.NumberFormatInfo NumberFormatInfo
        {
            get {
                if ( this._NumberFormatInfo is null )
                {
                    this._NumberFormatInfo = new System.Globalization.NumberFormatInfo() { PercentDecimalDigits = DecimalPrecision };
                    this._NumberFormatInfo.NumberDecimalDigits = this._NumberFormatInfo.PercentDecimalDigits + 2;
                }

                return this._NumberFormatInfo;
            }
        }

        /// <summary>   Information describing the date time format. </summary>
        private System.Globalization.DateTimeFormatInfo _DateTimeFormatInfo;

        /// <summary>
        /// DateTime format informations for displaying values with the set specification limit.
        /// </summary>
        /// <value> Information describing the date time format. </value>
        public System.Globalization.DateTimeFormatInfo DateTimeFormatInfo
        {
            get {
                if ( this._DateTimeFormatInfo is null )
                {
                    this._DateTimeFormatInfo = new System.Globalization.DateTimeFormatInfo() { ShortTimePattern = "HH:MM:ss.fff" };
                }

                return this._DateTimeFormatInfo;
            }
        }

        #endregion

    }

    /// <summary>   A sample trait selector by meter, element and nominal type. </summary>
    /// <remarks>   David, 2020-06-29. </remarks>
    public struct SampleTraitSelector : IEquatable<SampleTraitSelector>
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="attribute">    The attribute. </param>
        public SampleTraitSelector( SampleTrait attribute ) : this( attribute.MeterId, attribute.ElementAutoId, ( int ) attribute.NomType )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="NomTypeEntity"/>. </param>
        public SampleTraitSelector( int meterId, int elementAutoId, int nomTypeId )
        {
            this.MeterId = meterId;
            this.ElementAutoId = elementAutoId;
            this.NomTypeId = nomTypeId;
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> The identifier of the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="ElementEntity"/>. </summary>
        /// <value> The identifier of the <see cref="ElementEntity"/>. </value>
        public int ElementAutoId { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="NomTypeEntity"/>. </summary>
        /// <value> The identifier of the <see cref="NomTypeEntity"/>. </value>
        public int NomTypeId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( SampleTraitSelector ) obj );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( SampleTraitSelector other )
        {
            return this.MeterId == other.MeterId && this.NomTypeId == other.NomTypeId && this.ElementAutoId == other.ElementAutoId;
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.MeterId, this.NomTypeId, this.ElementAutoId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( SampleTraitSelector left, SampleTraitSelector right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( SampleTraitSelector left, SampleTraitSelector right )
        {
            return !(left == right);
        }
    }

    /// <summary>   Values that represent outliers calculator methods. </summary>
    /// <remarks>   David, 2021-05-13. </remarks>
    public enum OutliersCalculatorMethod
    {
        /// <summary>   An enum constant representing the none option. </summary>
        None,
        /// <summary>   An enum constant representing the using skewness option. </summary>
        UsingSkewness,
        /// <summary>   An enum constant representing the using Tukey fences option. </summary>
        UsingTukeyFences
    }
}

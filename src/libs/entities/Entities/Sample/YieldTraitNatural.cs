using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{
    /// <summary>   Yield Natural (Integer) Value summary. </summary>
    /// <remarks>
    /// David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class YieldTraitNatural : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSestter">    The getter setter. </param>
        public YieldTraitNatural( Std.Primitives.IGetterSetter<int> getterSestter ) : base()
        {
            this.GetterSetter = getterSestter;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<int> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) The name. </param>
        /// <returns>   The trait value. </returns>
        protected int? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void Setter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }
        }

        #endregion

        #region " ADD ITEM(S) "

        /// <summary>   Adds an item. </summary>
        /// <remarks>   David, 2020-05-29. </remarks>
        /// <param name="isGood">           True if is good, false if not. </param>
        /// <param name="isValidReading">   True if is valid reading, false if not. </param>
        public void AddItem( bool isGood, bool isValidReading )
        {
            if ( !this.YieldDecimalPlaces.HasValue )
                this.YieldDecimalPlaces = 1;
            this.TotalCount += 1;
            if ( !isValidReading )
            {
                this.InvalidCount += 1;
            }
            else if ( isGood )
            {
                this.GoodCount += 1;
            }
            else
            {
                this.FailureCount += 1;
            }

            this.Yield = this.EstimateYield();
            this.ValidYield = this.EstimateValidYield();
        }

        #endregion

        #region " YIELD COUNTS "

        /// <summary>   Gets or sets the number of totals. </summary>
        /// <value> The total number of count. </value>
        public int? TotalCount
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the number of invalids. </summary>
        /// <value> The number of invalids. </value>
        public int? InvalidCount
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the number of goods. </summary>
        /// <value> The number of goods. </value>
        public int? GoodCount
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the number of failures. </summary>
        /// <value> The number of failures. </value>
        public int? FailureCount
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets the number of valid readings. </summary>
        /// <value> The number of valid readings. </value>
        public int? ValidCount => this.TotalCount - this.InvalidCount;

        /// <summary>   Estimate yield. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateYield()
        {
            return this.TotalCount.GetValueOrDefault( 0 ) > 0 ? this.GoodCount / this.TotalCount : new double?();
        }

        /// <summary>   Estimate valid yield. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateValidYield()
        {
            return this.ValidCount.GetValueOrDefault( 0 ) > 0 ? this.GoodCount / this.ValidCount : new double?();
        }

        #endregion

        #region " YIELD "

        /// <summary>   Gets or sets the yield decimal places. </summary>
        /// <value> The yield decimal places. </value>
        public int? YieldDecimalPlaces
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the yield. </summary>
        /// <value> The yield. </value>
        public double? Yield
        {
            get => this.Getter() / Math.Pow( 10d, this.YieldDecimalPlaces.GetValueOrDefault( 3 ) );

            set => this.Setter( ( int ) (value * Math.Pow( 10d, this.YieldDecimalPlaces.GetValueOrDefault( 3 ) )) );
        }

        /// <summary>   Gets or sets the valid yield. </summary>
        /// <value> The valid yield. </value>
        public double? ValidYield
        {
            get => this.Getter() / Math.Pow( 10d, this.YieldDecimalPlaces.GetValueOrDefault( 3 ) );

            set => this.Setter( ( int ) (value * Math.Pow( 10d, this.YieldDecimalPlaces.GetValueOrDefault( 3 ) )) );
        }

        #endregion

    }
}

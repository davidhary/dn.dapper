using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Sample Trait builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class SampleTraitBuilder : KeyTwoForeignRealBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( SampleTraitNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the first foreign reference table. </summary>
        /// <value> The name of the first foreign reference table. </value>
        public override string FirstForeignTableName { get; set; } = NomTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the first foreign reference table key. </summary>
        /// <value> The name of the first foreign reference table key. </value>
        public override string FirstForeignTableKeyName { get; set; } = nameof( NomTypeNub.Id );

        /// <summary>   Gets or sets the name of the second foreign reference table. </summary>
        /// <value> The name of the first foreign reference table. </value>
        public override string SecondForeignTableName { get; set; } = SampleTraitTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the second foreign reference table key. </summary>
        /// <value> The name of the first foreign reference table key. </value>
        public override string SecondForeignTableKeyName { get; set; } = nameof( SampleTraitTypeNub.Id );


        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The new table. </returns>
        public string CreateTable( System.Data.IDbConnection connection )
        {
            // the same Element ID and Sample Type would repeat for the NUT associated with the next UUT.
            return this.CreateTable( connection, UniqueIndexOptions.None );
        }

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( SampleTraitEntity.AutoId );

        /// <summary>   Gets or sets the name of the First foreign identifier field. </summary>
        /// <value> The name of the First foreign identifier field. </value>
        public override string FirstForeignIdFieldName { get; set; } = nameof( SampleTraitEntity.NomTypeId );

        /// <summary>   Gets or sets the name of the second foreign identifier field. </summary>
        /// <value> The name of the second foreign identifier field. </value>
        public override string SecondForeignIdFieldName { get; set; } = nameof( SampleTraitEntity.SampleTraitTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( SampleTraitEntity.Amount );

        #region " SINGLETON "

        private static readonly Lazy<SampleTraitBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SampleTraitBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="Dapper.Entities.SampleTraitEntity"/>
    /// <see cref="IKeyTwoForeignReal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "SampleTrait" )]
    public class SampleTraitNub : KeyTwoForeignRealNub, IKeyTwoForeignReal
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SampleTraitNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyTwoForeignReal CreateNew()
        {
            return new SampleTraitNub();
        }
    }

    /// <summary>
    /// The <see cref="Dapper.Entities.SampleTraitEntity"/> storing Sample Traits for a specific
    /// element reading.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-22 </para>
    /// </remarks>
    public class SampleTraitEntity : EntityBase<IKeyTwoForeignReal, SampleTraitNub>, IKeyTwoForeignReal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public SampleTraitEntity() : this( new SampleTraitNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SampleTraitEntity( IKeyTwoForeignReal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public SampleTraitEntity( IKeyTwoForeignReal cache, IKeyTwoForeignReal store ) : base( new SampleTraitNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public SampleTraitEntity( SampleTraitEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.SampleTraitBuilder.TableName, nameof( IKeyTwoForeignReal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyTwoForeignReal CreateNew()
        {
            return new SampleTraitNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyTwoForeignReal CreateCopy()
        {
            var destination = this.CreateNew();
            KeyTwoForeignRealNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="Dapper.Entities.SampleTraitEntity"/> interface value. </param>
        public override void CopyFrom( IKeyTwoForeignReal value )
        {
            KeyTwoForeignRealNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.ElementEntity"/>Value interface. </param>
        public override void UpdateCache( IKeyTwoForeignReal value )
        {
            // first make the copy to notify of any property change.
            KeyTwoForeignRealNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The <see cref="Dapper.Entities.SampleTraitEntity"/> primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyTwoForeignReal>( key ) : connection.Get<SampleTraitNub>( key ) );
        }

        /// <summary>   Refetch; Fetches using the primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-15. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="Dapper.Entities.SampleTraitEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int autoId )
        {
            return this.FetchUsingKey( connection, autoId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyTwoForeignReal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.AutoId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="Dapper.Entities.SampleTraitEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int autoId )
        {
            return connection.Delete( new SampleTraitNub() { AutoId = autoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets or <see cref="Dapper.Entities.SampleTraitEntity"/>'s. </summary>
        /// <value> The <see cref="Dapper.Entities.SampleTraitEntity"/>'s. </value>
        public IEnumerable<SampleTraitEntity> SampleTraits { get; private set; }

        /// <summary>   Fetches all records into <see cref="Dapper.Entities.SampleTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<SampleTraitEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyTwoForeignReal>() ) : Populate( connection.GetAll<SampleTraitNub>() );
        }

        /// <summary>   Fetches all records into <see cref="Dapper.Entities.SampleTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.SampleTraits = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( SampleTraitEntity.SampleTraits ) );
            return this.SampleTraits?.Any() == true ? this.SampleTraits.Count() : 0;
        }

        /// <summary>   Populates a list of <see cref="Dapper.Entities.SampleTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.ElementEntity"/>Value nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SampleTraitEntity> Populate( IEnumerable<SampleTraitNub> nubs )
        {
            var l = new List<SampleTraitEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( SampleTraitNub nub in nubs )
                    l.Add( new SampleTraitEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of <see cref="Dapper.Entities.SampleTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.ElementEntity"/>Value interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<SampleTraitEntity> Populate( IEnumerable<IKeyTwoForeignReal> interfaces )
        {
            var l = new List<SampleTraitEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new SampleTraitNub();
                foreach ( IKeyTwoForeignReal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new SampleTraitEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count element Sample Traits; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="Dapper.Entities.SampleTraitEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int autoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{SampleTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SampleTraitNub.AutoId )} = @AutoId", new { AutoId = autoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches element Sample Traits by Element number and element Sample Trait element Sample Trait
        /// Real Value type;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="Dapper.Entities.SampleTraitEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<SampleTraitNub> FetchEntities( System.Data.IDbConnection connection, int autoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SampleTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SampleTraitNub.AutoId )} = @AutoId", new { AutoId = autoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SampleTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches element Sample Traits by <see cref="Dapper.Entities.SampleTraitEntity"/> id; expected single
        /// or none.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="Dapper.Entities.SampleTraitEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<SampleTraitNub> FetchNubs( System.Data.IDbConnection connection, int autoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SampleTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SampleTraitEntity.AutoId )} = @AutoId", new { AutoId = autoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SampleTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.ElementEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the <see cref="Dapper.Entities.SampleTraitEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int autoId )
        {
            return 1 == CountEntities( connection, autoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>
        /// Fetches Sample trait values associated with the primary reference identified by the
        /// <see cref="AutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<SampleTraitNub> FetchSampleTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{SampleTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( SampleTraitNub.AutoId )} = @AutoId", new { this.AutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<SampleTraitNub>( selector.RawSql, selector.Parameters );
        }

        #endregion

        #region " RELATIONS: NOMINAL TYPE "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public NomTypeEntity NomTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.NomTypeEntity"/> . </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchNomTypeEntity( System.Data.IDbConnection connection )
        {
            this.NomTypeEntity = new NomTypeEntity();
            return this.NomTypeEntity.FetchUsingKey( connection, this.NomTypeId );
        }

        #endregion

        #region " RELATIONS: SAMPLE TRAIT TYPE "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.SampleTraitTypeEntity"/> . </summary>
        /// <value> the <see cref="Dapper.Entities.SampleTraitTypeEntity"/> . </value>
        public SampleTraitTypeEntity SampleTraitTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.SampleTraitTypeEntity"/>. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchSampleTraitTypeEntity( System.Data.IDbConnection connection )
        {
            this.SampleTraitTypeEntity = new SampleTraitTypeEntity();
            return this.SampleTraitTypeEntity.FetchUsingKey( connection, this.SampleTraitTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the first foreign key. </summary>
        /// <value> Identifies the first foreign key. </value>
        public int FirstForeignId
        {
            get => this.ICache.FirstForeignId;

            set {
                if ( !object.Equals( ( object ) this.FirstForeignId, ( object ) value ) )
                {
                    this.ICache.FirstForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SampleTraitEntity.NomTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> The identifier of the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public int NomTypeId
        {
            get => this.FirstForeignId;

            set => this.FirstForeignId = value;
        }

        /// <summary>   Gets or sets the id of the Second Foreign key. </summary>
        /// <value> Identifies the Second Foreign key. </value>
        public int SecondForeignId
        {
            get => this.ICache.SecondForeignId;

            set {
                if ( !object.Equals( ( object ) this.SecondForeignId, ( object ) value ) )
                {
                    this.ICache.SecondForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( SampleTraitEntity.SampleTraitTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the <see cref="Dapper.Entities.SampleTraitEntity"/>.
        /// </summary>
        /// <value> The type of the <see cref="Dapper.Entities.SampleTraitEntity"/>. </value>
        public int SampleTraitTypeId
        {
            get => this.SecondForeignId;

            set => this.SecondForeignId = value;
        }

        /// <summary>
        /// Gets or sets a sample value assigned to the <see cref="Dapper.Entities.SampleTraitEntity"/>
        /// for the specific <see cref="Dapper.Entities.SampleTraitTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The sample value assigned to the <see cref="Dapper.Entities.SampleTraitEntity"/> for the
        /// specific <see cref="Dapper.Entities.SampleTraitTypeEntity"/>.
        /// </value>
        public double Amount
        {
            get => this.ICache.Amount;

            set {
                if ( !object.Equals( this.Amount, value ) )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The entity selector. </value>
        public ThreeKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="Dapper.Entities.SampleTraitEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class SampleTraitEntityCollection : EntityKeyedCollection<ThreeKeySelector, IKeyTwoForeignReal, SampleTraitNub, SampleTraitEntity>
    {
        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override ThreeKeySelector GetKeyForItem( SampleTraitEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        public SampleTraitEntityCollection() : base()
        {
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( SampleTraitEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<SampleTraitEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                this.Clear();
                foreach ( SampleTraitEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return SampleTraitBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>   A sample trait entity selector. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public struct SampleTraitEntitySelector : IEquatable<SampleTraitEntitySelector>
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="sampleTraitEntity">    The sample trait entity. </param>
        public SampleTraitEntitySelector( IKeyTwoForeignReal sampleTraitEntity ) : this( sampleTraitEntity.AutoId, sampleTraitEntity.FirstForeignId, sampleTraitEntity.SecondForeignId )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="sampleTraitEntity">    The sample trait entity. </param>
        public SampleTraitEntitySelector( SampleTraitEntity sampleTraitEntity ) : this( sampleTraitEntity.AutoId, sampleTraitEntity.NomTypeId, sampleTraitEntity.SampleTraitTypeId )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="sampleTraitEntityAutoId">  The identifier of the sample trait entity automatic. </param>
        /// <param name="nomTypeId">                The identifier of the nom type. </param>
        /// <param name="sampleTrait">              The sample trait. </param>
        public SampleTraitEntitySelector( int sampleTraitEntityAutoId, int nomTypeId, int sampleTrait )
        {
            this.SampleTraitEntityAutoId = sampleTraitEntityAutoId;
            this.NomTypeId = nomTypeId;
            this.SampleTraitTypeId = sampleTrait;
        }

        /// <summary>   Gets or sets the id of the sample trait entity automatic. </summary>
        /// <value> The identifier of the sample trait entity automatic. </value>
        public int SampleTraitEntityAutoId { get; set; }
        /// <summary>   Gets or sets the id of the nom type. </summary>
        /// <value> The identifier of the nom type. </value>
        public int NomTypeId { get; set; }
        /// <summary>   Gets or sets the id of the sample trait type. </summary>
        /// <value> The identifier of the sample trait type. </value>
        public int SampleTraitTypeId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( SampleTraitEntitySelector ) obj );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( SampleTraitEntitySelector other )
        {
            return this.SampleTraitEntityAutoId == other.SampleTraitEntityAutoId && this.SampleTraitTypeId == other.SampleTraitTypeId && this.NomTypeId == other.NomTypeId;
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.SampleTraitEntityAutoId, this.SampleTraitTypeId, this.NomTypeId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( SampleTraitEntitySelector left, SampleTraitEntitySelector right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( SampleTraitEntitySelector left, SampleTraitEntitySelector right )
        {
            return !(left == right);
        }
    }
}

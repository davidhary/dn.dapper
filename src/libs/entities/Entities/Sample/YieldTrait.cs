using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Yield <see cref="isr.Std.Primitives.IGetterSetter{TValue}"/> values.
    /// </summary>
    /// <remarks>
    /// David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class YieldTrait : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSestter">    The getter setter. </param>
        public YieldTrait( Std.Primitives.IGetterSetter<double> getterSestter ) : base()
        {
            this.GetterSetter = getterSestter;
            this.BucketBinCounters = new Dictionary<int, int>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-29. </remarks>
        /// <param name="getterSestter">        The getter setter. </param>
        /// <param name="meter">                The <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="certifiedQuantity">    (Optional) The certified quantity. </param>
        public YieldTrait( Std.Primitives.IGetterSetter<double> getterSestter, MeterEntity meter, int certifiedQuantity = 0 ) : this( getterSestter )
        {
            this.MeterId = meter.Id;
            this.CertifiedQuantity = certifiedQuantity;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-05-14. </remarks>
        /// <param name="getterSestter">        The getter setter. </param>
        /// <param name="meterId">              Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="certifiedQuantity">    (Optional) The certified quantity. </param>
        public YieldTrait( Std.Primitives.IGetterSetter<double> getterSestter, int meterId, int certifiedQuantity = 0 ) : this( getterSestter )
        {
            this.MeterId = meterId;
            this.CertifiedQuantity = certifiedQuantity;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<double> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) The name. </param>
        /// <returns>   The trait value. </returns>
        protected double? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void Setter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }
        }

        #endregion

        #region " ADD ITEM(S) "

        /// <summary>   Adds an item. </summary>
        /// <remarks>   David, 2020-05-29. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="uut">  The uut. </param>
        public void AddItem( UutEntity uut )
        {
            if ( !uut.ProductSortEntity.ProductSortTraits.ProductSortTrait.BucketBin.HasValue )
            {
                ;
                throw new System.InvalidOperationException( $"{nameof( ProductSortEntity )} {nameof( ProductSortTrait.BucketBin )} was not set" );
            }

            var bucketBin = BucketBinEntity.ToBucketBin( uut.ProductSortEntity.ProductSortTraits.ProductSortTrait.BucketBin.Value );
            if ( !this.YieldDecimalPlaces.HasValue )
                this.YieldDecimalPlaces = 1;
            this.TotalCount += 1;
            if ( bucketBin == BucketBin.Good )
            {
                this.GoodCount += 1;
            }
            else if ( bucketBin == BucketBin.Bad )
            {
                this.BadCount += 1;
            }
            else
            {
                this.InvalidCount += 1;
            }

            if ( this.BucketBinCounters.ContainsKey( ( int ) bucketBin ) )
            {
                this.BucketBinCounters[( int ) bucketBin] += 1;
            }
            else
            {
                this.BucketBinCounters.Add( ( int ) bucketBin, 1 );
            }

            this.Yield = this.EstimateYield();
            this.ValidYield = this.EstimateValidYield();
        }

        #endregion

        #region " BUCKET BIN COUNTS "

        /// <summary>   Gets or sets the bucket bin counters. </summary>
        /// <value> The bucket bin counters. </value>
        public IDictionary<int, int> BucketBinCounters { get; private set; }

        /// <summary>   Hourly rate. </summary>
        /// <remarks>   David, 2020-07-07. </remarks>
        /// <param name="elapsed">  The elapsed. </param>
        /// <returns>   A Double. </returns>
        public double HourlyRate( TimeSpan elapsed )
        {
            return elapsed.TotalSeconds > 0d ? 3600 * this.GoodCount.GetValueOrDefault( 0 ) / elapsed.TotalSeconds : 0d;
        }

        #endregion

        #region " ADDITIONAL PROPERTIES "

        /// <summary>   The certified quantity. </summary>
        private int _CertifiedQuantity;

        /// <summary>   Gets or sets the certified quantity. </summary>
        /// <value> The certified quantity. </value>
        public int CertifiedQuantity
        {
            get => this._CertifiedQuantity;

            set {
                if ( value != this.CertifiedQuantity )
                {
                    this._CertifiedQuantity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " IDENTIFIERS "

        private int _MeterId;

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId
        {
            get => this._MeterId;

            set {
                if ( value != this.MeterId )
                {
                    this._MeterId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " DERIVED TRAITS "

        private MeterEntity _Meter;

        /// <summary>   Gets the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public MeterEntity Meter
        {
            get {
                if ( !(this._Meter?.IsClean()).GetValueOrDefault( false ) )
                {
                    this._Meter = MeterEntity.EntityLookupDictionary()[this.MeterId];
                }

                return this._Meter;
            }
        }

        private MeterModelEntity _MeterModel;

        /// <summary>   Gets the Meter Model entity. </summary>
        /// <value> The Meter Model entity. </value>
        public MeterModelEntity MeterModel
        {
            get {
                if ( !(this._MeterModel?.IsClean()).GetValueOrDefault( false ) )
                {
                    this._MeterModel = MeterModelEntity.EntityLookupDictionary()[this.Meter.MeterModelId];
                }

                return this._MeterModel;
            }
        }

        #endregion

        #region " YIELD COUNTS "

        /// <summary>   Clears this object to its blank/initial state. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public void Clear()
        {
            this.TotalCount = new int?();
            this.InvalidCount = new int?();
            this.GoodCount = new int?();
            this.BadCount = new int?();
            this.Yield = new int?();
            this.ValidYield = new int?();
        }

        /// <summary>   Gets or sets the number of totals. </summary>
        /// <value> The total number of count. </value>
        public int? TotalCount
        {
            get => ( int? ) this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the number of invalids. </summary>
        /// <value> The number of invalids. </value>
        public int? InvalidCount
        {
            get => ( int? ) this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the number of goods. </summary>
        /// <value> The number of goods. </value>
        public int? GoodCount
        {
            get => ( int? ) this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the number of failures. </summary>
        /// <value> The number of failures. </value>
        public int? BadCount
        {
            get => ( int? ) this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets the number of valid readings. </summary>
        /// <value> The number of valid readings. </value>
        public int? ValidCount => this.TotalCount - this.InvalidCount;

        /// <summary>   Estimate yield. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateYield()
        {
            return this.TotalCount.GetValueOrDefault( 0 ) > 0 ? this.GoodCount / this.TotalCount : new double?();
        }

        /// <summary>   Estimate valid yield. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <returns>   A Double? </returns>
        public double? EstimateValidYield()
        {
            return this.ValidCount.GetValueOrDefault( 0 ) > 0 ? this.GoodCount / this.ValidCount : new double?();
        }

        #endregion

        #region " YIELD "

        /// <summary>   Evaluates this object. </summary>
        /// <remarks>   David, 2020-07-04. </remarks>
        public void Evaluate()
        {
            this.Yield = this.EstimateYield();
            this.ValidYield = this.EstimateValidYield();
        }

        /// <summary>   Gets or sets the yield decimal places. </summary>
        /// <value> The yield decimal places. </value>
        public int? YieldDecimalPlaces
        {
            get => ( int? ) this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the yield. </summary>
        /// <value> The yield. </value>
        public double? Yield
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the valid yield. </summary>
        /// <value> The valid yield. </value>
        public double? ValidYield
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        #endregion

    }
}

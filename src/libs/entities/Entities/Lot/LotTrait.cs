using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{

    /// <summary>   The Lot Trait class holing the Lot Trait values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class LotTrait : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSestter">    The getter setter. </param>
        public LotTrait( Std.Primitives.IGetterSetter<int> getterSestter ) : base()
        {
            this.GetterSetter = getterSestter;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<int> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Integer. </returns>
        public int? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Integer. </returns>
        public int? Setter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }

            return value;
        }

        #endregion

        #region " TRAITS "

        /// <summary>   Gets or sets the certified quantity. </summary>
        /// <value> The certified quantity. </value>
        public int CertifiedQuantity
        {
            get => this.Getter().GetValueOrDefault( 0 );

            set => this.Setter( value );
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Lot trait builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class LotTraitBuilder : OneToManyNaturalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( LotTraitNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = LotBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( LotNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = LotTraitTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( LotTraitTypeNub.Id );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( LotTraitEntity.LotAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( LotTraitEntity.LotTraitTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( LotTraitEntity.Amount );

        #region " SINGLETON "

        private static readonly Lazy<LotTraitBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static LotTraitBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="LotTraitEntity"/> value table
    /// <see cref="IOneToManyNatural">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "LotTrait" )]
    public class LotTraitNub : OneToManyNaturalNub, IOneToManyNatural
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public LotTraitNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyNatural CreateNew()
        {
            return new LotTraitNub();
        }
    }

    /// <summary>
    /// The <see cref="LotTraitEntity"/> stores <see cref="Dapper.Entities.LotEntity"/> traits .
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class LotTraitEntity : EntityBase<IOneToManyNatural, LotTraitNub>, IOneToManyNatural
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public LotTraitEntity() : this( new LotTraitNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotTraitEntity( IOneToManyNatural value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public LotTraitEntity( IOneToManyNatural cache, IOneToManyNatural store ) : base( new LotTraitNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotTraitEntity( LotTraitEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.LotTraitBuilder.TableName, nameof( IOneToManyNatural ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyNatural CreateNew()
        {
            return new LotTraitNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyNatural CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNaturalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="LotTraitEntity"/> interface value. </param>
        public override void CopyFrom( IOneToManyNatural value )
        {
            OneToManyNaturalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.LotEntity"/>Value interface. </param>
        public override void UpdateCache( IOneToManyNatural value )
        {
            // first make the copy to notify of any property change.
            OneToManyNaturalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </param>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int lotAutoId, int lotTraitTypeId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<LotTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </param>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int lotAutoId, int lotTraitTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, lotAutoId, lotTraitTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToManyNatural entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </param>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int lotAutoId, int lotTraitTypeId )
        {
            return connection.Delete( new LotTraitNub() { PrimaryId = lotAutoId, SecondaryId = lotTraitTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.LotTraitEntity"/>'s.
        /// </summary>
        /// <value>
        /// the <see cref="Dapper.Entities.LotEntity"/> trait entities.
        /// </value>
        public IEnumerable<LotTraitEntity> LotTraits { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotTraitEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToManyNatural>() ) : Populate( connection.GetAll<LotTraitNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.LotTraits = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( LotTraitEntity.LotTraits ) );
            return this.LotTraits?.Any() == true ? this.LotTraits.Count() : 0;
        }

        /// <summary>   Count <see cref="LotTraitEntity"/>'s associated with the <see cref="LotEntity"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountLotTraits( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.LotTraitBuilder.TableName}] WHERE {nameof( LotTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   All <see cref="LotTraitEntity"/>'s associated witht eh <see cref="LotEntity"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotTraitEntity> FetchLotTraits( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotTraitBuilder.TableName}] WHERE {nameof( LotTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<LotTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Lot Traits by Lot auto id. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<LotTraitNub> FetchNubs( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotTraitEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LotTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of LotValue entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.LotEntity"/>Value nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotTraitEntity> Populate( IEnumerable<LotTraitNub> nubs )
        {
            var l = new List<LotTraitEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( LotTraitNub nub in nubs )
                    l.Add( new LotTraitEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of LotValue entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.LotEntity"/>Value interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotTraitEntity> Populate( IEnumerable<IOneToManyNatural> interfaces )
        {
            var l = new List<LotTraitEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new LotTraitNub();
                foreach ( IOneToManyNatural iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new LotTraitEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Lot Traits buy unique index; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </param>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int lotAutoId, int lotTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches Lot Traits by unique index; expected single or none. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </param>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<LotTraitNub> FetchEntities( System.Data.IDbConnection connection, int lotAutoId, int lotTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotTraitNub.PrimaryId )} = @primaryId", new { primaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = lotTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LotTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.LotEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="lotAutoId">        Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </param>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int lotAutoId, int lotTraitTypeId )
        {
            return 1 == CountEntities( connection, lotAutoId, lotTraitTypeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count values associated with this <see cref="LotEntity"/>. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of records. </returns>
        public int CountLotTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Lot Trait Values for this <see cref="LotEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<LotTraitNub> FetchLotTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LotTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.LotEntity"/>. </summary>
        /// <value> the <see cref="Dapper.Entities.LotEntity"/>. </value>
        public LotEntity LotEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.LotEntity"/>. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchLotEntity( System.Data.IDbConnection connection )
        {
            this.LotEntity = new LotEntity();
            return this.LotEntity.FetchUsingKey( connection, this.PrimaryId );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.LotEntity"/>. </summary>
        /// <value> the <see cref="Dapper.Entities.LotEntity"/>. </value>
        public LotTraitTypeEntity LotNaturalTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.LotEntity"/>. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchLotNaturalTypeEntity( System.Data.IDbConnection connection )
        {
            this.LotNaturalTypeEntity = new LotTraitTypeEntity();
            return this.LotNaturalTypeEntity.FetchUsingKey( connection, this.PrimaryId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( this.PrimaryId, value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( this.LotAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.LotEntity"/> record.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.LotEntity"/> record. </value>
        public int LotAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( this.SecondaryId, value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( this.LotTraitTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the identity of the <see cref="Dapper.Entities.LotEntity"/>
        /// <see cref="LotTraitTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The identity of the <see cref="Dapper.Entities.LotEntity"/> <see cref="LotTraitTypeEntity"/>.
        /// </value>
        public int LotTraitTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>
        /// Gets or sets a trait value assigned to the <see cref="Dapper.Entities.LotEntity"/> for
        /// the specific <see cref="LotTraitTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The trait value assigned to the <see cref="Dapper.Entities.LotEntity"/> for the specific
        /// <see cref="LotTraitTypeEntity"/>.
        /// </value>
        public int Amount
        {
            get => this.ICache.Amount;

            set {
                if ( !object.Equals( this.Amount, value ) )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="LotTraitEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class LotTraitEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToManyNatural, LotTraitNub, LotTraitEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( LotTraitEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( LotTraitEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all Lots from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<LotTraitEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( LotTraitEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return LotTraitBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of <see cref="Dapper.Entities.LotEntity"/> unique <see cref="LotTraitEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class LotUniqueTraitEntityCollection : LotTraitEntityCollection, Std.Primitives.IGetterSetter<int>
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public LotUniqueTraitEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
            this.LotTrait = new LotTrait( this );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        public LotUniqueTraitEntityCollection( int lotAutoId ) : this()
        {
            this.LotAutoId = lotAutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( LotTraitEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.LotTraitTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.LotTraitTypeId );
            this.NotifyPropertyChanged( LotTraitTypeEntity.EntityLookupDictionary()[entity.LotTraitTypeId].Label );
        }

        /// <summary>
        /// Removes all Lots from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains 'NaturalType' key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int lotTraitTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( lotTraitTypeId );
        }

        #endregion

        #region " GETTER/SETTER "

        /// <summary>   Gets the Integer-value for the given <see cref="LotTraitType"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        int? IGetterSetter<int>.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A nullable Integer. </returns>
        int IGetterSetter<int>.Setter( int value, string name )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>
        /// Converts a name to a key using the
        /// <see cref="LotTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
        /// extend the element Nominal Traits beyond the values of the enumeration type that is used to
        /// populate this table.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return LotTraitTypeEntity.KeyLookupDictionary()[name];
        }

        /// <summary>   Sets the trait value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Integer. </returns>
        public int? Setter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue )
            {
                _ = this.SetterThis( value.Value, name );
            }

            return value;
        }

        /// <summary>   Gets the Integer-value for the given <see cref="LotTraitType"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        protected int? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>   Set the specified value for the given <see cref="LotTraitType"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        private int SetterThis( int value, string name )
        {
            int key = this.ToKey( name );
            if ( !int.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>   Set the specified value for the given <see cref="LotTraitType"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        protected int Setter( int value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets or sets the Lot Trait. </summary>
        /// <value> The Lot Trait. </value>
        public LotTrait LotTrait { get; private set; }

        #endregion

        #region " ATTRIBUTE  SELECTION "

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <returns>   An LotNaturalEntity. </returns>
        public LotTraitEntity Entity( int lotTraitTypeId )
        {
            return this.ContainsKey( lotTraitTypeId ) ? this[this._PrimaryKeyDictionary[lotTraitTypeId]] : new LotTraitEntity();
        }

        /// <summary>
        /// Gets the trait value of for the given <see cref="LotTraitTypeEntity.Id"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <returns>   An Integer? </returns>
        public int? Getter( int lotTraitTypeId )
        {
            return this.ContainsKey( lotTraitTypeId ) ? this[this._PrimaryKeyDictionary[lotTraitTypeId]].Amount : new int?();
        }

        /// <summary>
        /// Set the specified element value for the given <see cref="LotTraitTypeEntity.Id"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="lotTraitTypeId">   Identifies the <see cref="LotTraitTypeEntity"/>. </param>
        /// <param name="value">            The value. </param>
        public void Setter( int lotTraitTypeId, int value )
        {
            if ( this.ContainsKey( lotTraitTypeId ) )
            {
                this[this._PrimaryKeyDictionary[lotTraitTypeId]].Amount = value;
            }
            else
            {
                this.Add( new LotTraitEntity() { LotAutoId = LotAutoId, LotTraitTypeId = lotTraitTypeId, Amount = value } );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.LotEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.LotEntity"/>. </value>
        public int LotAutoId { get; private set; }

        #endregion

    }
}

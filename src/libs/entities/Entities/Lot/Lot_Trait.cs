

namespace isr.Dapper.Entities
{
    /// <summary>   A lot entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class LotEntity
    {

        /// <summary>   Gets or sets the <see cref="LotTraitEntity"/> collection. </summary>
        /// <value> The traits. </value>
        public LotUniqueTraitEntityCollection Traits { get; private set; }

        /// <summary>
        /// Fetches the <see cref="LotUniqueTraitEntityCollection"/>.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of trait. </returns>
        public int FetchTraits( System.Data.IDbConnection connection )
        {
            if ( !LotTraitTypeEntity.IsEnumerated() )
                _ = LotTraitTypeEntity.TryFetchAll( connection );
            this.Traits = new LotUniqueTraitEntityCollection( this.AutoId );
            this.Traits.Populate( LotTraitEntity.FetchLotTraits( connection, this.AutoId ) );
            return this.Traits.Count;
        }
    }
}

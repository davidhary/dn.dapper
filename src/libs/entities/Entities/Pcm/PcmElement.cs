using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;
using isr.Dapper.Entity.EntityExtensions;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// A builder for a PC Element based on the <see cref="IKeyForeignLabel">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class PcmElementBuilder : KeyForeignLabelBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( PcmElementNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the foreign table . </summary>
        /// <value> The name of the foreign table. </value>
        public override string ForeignTableName { get; set; } = PcmElementTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <value> The name of the foreign table key. </value>
        public override string ForeignTableKeyName { get; set; } = nameof( PcmElementTypeNub.Id );

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public override int LabelFieldSize { get; set; } = 50;

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( PcmElementEntity.AutoId );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( PcmElementEntity.PcmElementTypeId );

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public override string LabelFieldName { get; set; } = nameof( PcmElementEntity.PcmElementLabel );

        /// <summary>
        /// Inserts or ignores the records described by the <see cref="PcmElementType"/> enumeration type.
        /// </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of inserted or total existing records. </returns>
        public static int InsertIgnorePcmElementTypeValues( System.Data.IDbConnection connection )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : InsertIgnoreTypeValues( connection, typeof( PcmElementType ), new int[] { ( int ) PcmElementType.None } );
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="type">         The enumeration type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of inserted or total existing records. </returns>
        public static int InsertIgnoreTypeValues( System.Data.IDbConnection connection, Type type, IEnumerable<int> excluded )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.InsertIgnoreNameDescriptionRecords( PcmElementTypeBuilder.TableName, typeof( IKeyForeignLabel ).EnumerateEntityFieldNames(), type, excluded );
        }

        #region " SINGLETON "

        private static readonly Lazy<PcmElementBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static PcmElementBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Natural PcmElement table based on the
    /// <see cref="IKeyForeignLabel">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "PcmElement" )]
    public class PcmElementNub : KeyForeignLabelNub, IKeyForeignLabel
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PcmElementNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignLabel CreateNew()
        {
            return new PcmElementNub();
        }
    }

    /// <summary>
    /// The Nominal PcmElement Entity based on the <see cref="IKeyForeignLabel">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class PcmElementEntity : EntityBase<IKeyForeignLabel, PcmElementNub>, IKeyForeignLabel
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PcmElementEntity() : this( new PcmElementNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PcmElementEntity( IKeyForeignLabel value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public PcmElementEntity( IKeyForeignLabel cache, IKeyForeignLabel store ) : base( new PcmElementNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PcmElementEntity( PcmElementEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.PcmElementBuilder.TableName, nameof( IKeyForeignLabel ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignLabel CreateNew()
        {
            return new PcmElementNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyForeignLabel CreateCopy()
        {
            var destination = this.CreateNew();
            KeyForeignLabelNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyForeignLabel value )
        {
            KeyForeignLabelNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The PcmElement interface. </param>
        public override void UpdateCache( IKeyForeignLabel value )
        {
            // first make the copy to notify of any property change.
            KeyForeignLabelNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The PcmElement table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyForeignLabel>( key ) : connection.Get<PcmElementNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Label );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="pcmElementLabel">  The PcmElement number. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string pcmElementLabel )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, pcmElementLabel ).SingleOrDefault();
            return nub is object && this.FetchUsingKey( connection, nub.AutoId );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyForeignLabel entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            bool fetched = UniqueIndexOptions.Label == (PcmElementBuilder.Instance.QueryUniqueIndexOptions( connection ) & UniqueIndexOptions.Amount) ? this.FetchUsingUniqueIndex( connection, entity.Label ) : this.FetchUsingKey( connection, entity.AutoId );
            if ( fetched )
            {
                // update the existing record from the specified entity.
                entity.AutoId = this.AutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new PcmElementNub() { AutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the PcmElement entities. </summary>
        /// <value> The PcmElement entities. </value>
        public IEnumerable<PcmElementEntity> PcmElements { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PcmElementEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyForeignLabel>() ) : Populate( connection.GetAll<PcmElementNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.PcmElements = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( PcmElementEntity.PcmElements ) );
            return this.PcmElements?.Any() == true ? this.PcmElements.Count() : 0;
        }

        /// <summary>   Populates a list of PcmElement entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The PcmElement nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PcmElementEntity> Populate( IEnumerable<PcmElementNub> nubs )
        {
            var l = new List<PcmElementEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( PcmElementNub nub in nubs )
                    l.Add( new PcmElementEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of PcmElement entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The PcmElement interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PcmElementEntity> Populate( IEnumerable<IKeyForeignLabel> interfaces )
        {
            var l = new List<PcmElementEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new PcmElementNub();
                foreach ( IKeyForeignLabel iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new PcmElementEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="pcmElementLabel">  The PcmElement number. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string pcmElementLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PcmElementBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNub.Label )} = @PcmElementLabel", new { pcmElementLabel } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="pcmElementLabel">  The PcmElement number. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntitiesAlternative( System.Data.IDbConnection connection, string pcmElementLabel )
        {
            var selectSql = new System.Text.StringBuilder( $"SELECT * FROM [{PcmElementBuilder.TableName}]" );
            _ = selectSql.Append( $"WHERE (@{nameof( PcmElementNub.Label )} IS NULL OR {nameof( PcmElementNub.Label )} = @PcmElementLabel)" );
            _ = selectSql.Append( "; " );
            return connection.ExecuteScalar<int>( selectSql.ToString(), new { pcmElementLabel } );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="pcmElementLabel">  The PcmElement number. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PcmElementNub> FetchNubs( System.Data.IDbConnection connection, string pcmElementLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PcmElementBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNub.Label )} = @PcmElementLabel", new { pcmElementLabel } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PcmElementNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the PcmElement exists. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="pcmElementLabel">  The PcmElement number. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string pcmElementLabel )
        {
            return 1 == CountEntities( connection, pcmElementLabel );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="PcmElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="PcmElementEntity"/>. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="PcmElementEntity"/> type. </summary>
        /// <value> Identifies the <see cref="PcmElementEntity"/> type. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PcmElementEntity.PcmElementTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the PCM element type. </summary>
        /// <value> Identifies the PCM element type. </value>
        public int PcmElementTypeId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }

        /// <summary>   Gets or sets the element Label. </summary>
        /// <value> The Label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PcmElementEntity.PcmElementLabel ) );
                }
            }
        }

        /// <summary>   Gets or sets the PcmElement label. </summary>
        /// <value> The PcmElement label. </value>
        public string PcmElementLabel
        {
            get => this.Label;

            set => this.Label = value;
        }

        #endregion

    }

    /// <summary>   Collection of pcmElement entities. </summary>
    /// <remarks>   David, 2020-05-18. </remarks>
    public class PcmElementEntityCollection : EntityKeyedCollection<int, IKeyForeignLabel, PcmElementNub, PcmElementEntity>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public PcmElementEntityCollection() : base()
        {
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified pcmElement.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The pcmElement from which to extract the key. </param>
        /// <returns>   The key for the specified pcmElement. </returns>
        protected override int GetKeyForItem( PcmElementEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.AutoId;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( PcmElementEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all pcmElements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<PcmElementEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( PcmElementEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>   Collection of pcmElement entities uniquely associated with a part. </summary>
    /// <remarks>   David, 2020-05-18. </remarks>
    public class PartUniquePcmElementEntityCollection : PcmElementEntityCollection
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public PartUniquePcmElementEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<int, string>();
            this._PrimaryKeyDictionary = new Dictionary<string, int>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <param name="partAutoId">   Identifies the <see cref="PcmElementEntity"/>. </param>
        public PartUniquePcmElementEntityCollection( int partAutoId ) : this()
        {
            this.PartAutoId = partAutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<int, string> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<string, int> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( PcmElementEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.Label, entity.AutoId );
            this._UniqueIndexDictionary.Add( entity.AutoId, entity.Label );
        }

        /// <summary>
        /// Removes all pcmElements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Selects an pcmElement from the collection. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="pcmElementLabel">  The pcmElement label. </param>
        /// <returns>   An PcmElementEntity. </returns>
        public PcmElementEntity PcmElement( string pcmElementLabel )
        {
            int id = this._PrimaryKeyDictionary[pcmElementLabel];
            return this.Contains( id ) ? this[id] : new PcmElementEntity();
        }

        /// <summary>   PcmElement label. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="pcmElementAutoId"> Identifies the pcmElement. </param>
        /// <returns>   A String. </returns>
        public string PcmElementLabel( int pcmElementAutoId )
        {
            return this._UniqueIndexDictionary.ContainsKey( pcmElementAutoId ) ? this._UniqueIndexDictionary[pcmElementAutoId] : string.Empty;
        }

        /// <summary>   Gets or sets the id of the <see cref="PcmElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="PcmElementEntity"/>. </value>
        public int PartAutoId { get; private set; }
    }
}

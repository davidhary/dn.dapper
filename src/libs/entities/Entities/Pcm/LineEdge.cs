using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the LineEdge nub and entity. Includes the fields as kept in the data table.
    /// Allows tracking of property changes.
    /// </summary>
    /// <remarks>   David, 2020-10-02. </remarks>
    public interface ILineEdge
    {

        /// <summary>   Gets or sets the id of the line edge. </summary>
        /// <value> Identifies the line edge. </value>
        [ExplicitKey]
        int Id { get; set; }

        /// <summary>   Gets or sets the title. </summary>
        /// <value> The title. </value>
        string Title { get; set; }

        /// <summary>   Gets or sets a list of source channels. </summary>
        /// <value> A List of source channels. </value>
        string SourceChannelList { get; set; }

        /// <summary>   Gets or sets a list of measure channels. </summary>
        /// <value> A List of measure channels. </value>
        string MeasureChannelList { get; set; }
    }

    /// <summary>   A line edge builder. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public sealed class LineEdgeBuilder
    {

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( LineEdgeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets the name of the unique label index. </summary>
        /// <value> The name of the unique label index. </value>
        private static string TitleIndexName => $"UQ_{isr.Dapper.Entities.LineEdgeBuilder.TableName}_{nameof( LineEdgeNub.Title )}";

        /// <summary>   The using unique title. </summary>
        private static bool? _UsingUniqueTitle;

        /// <summary>   Indicates if the entity uses a unique Title. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool UsingUniqueTitle( System.Data.IDbConnection connection )
        {
            if ( !_UsingUniqueTitle.HasValue )
            {
                _UsingUniqueTitle = connection.IndexExists( TitleIndexName );
            }

            return _UsingUniqueTitle.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Entities.LineEdgeBuilder.TableName}] (
                [{nameof( LineEdgeNub.Id )}] integer NOT NULL PRIMARY KEY, 
                [{nameof( LineEdgeNub.Title )}] nvarchar(50) NOT NULL, 
                [{nameof( LineEdgeNub.SourceChannelList )}] nvarchar(50) NOT NULL,
                [{nameof( LineEdgeNub.MeasureChannelList )}] nvarchar(50) NOT NULL); 

            CREATE UNIQUE INDEX IF NOT EXISTS [{isr.Dapper.Entities.LineEdgeBuilder.TitleIndexName}] ON [{isr.Dapper.Entities.LineEdgeBuilder.TableName}] ([{nameof( LineEdgeNub.Title )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.LineEdgeBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Entities.LineEdgeBuilder.TableName}](
                [{nameof( LineEdgeNub.Id )}] [int] NOT NULL,
                [{nameof( LineEdgeNub.Title )}] [nvarchar](50) NOT NULL,
                [{nameof( LineEdgeNub.SourceChannelList )}] [nvarchar](50) NOT NULL,
                [{nameof( LineEdgeNub.MeasureChannelList )}] [nvarchar](50) NOT NULL,
             CONSTRAINT [PK_{isr.Dapper.Entities.LineEdgeBuilder.TableName}] PRIMARY KEY CLUSTERED 
            ([{nameof( LineEdgeNub.Id )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 
            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.LineEdgeBuilder.TableName}]') AND name = N'{isr.Dapper.Entities.LineEdgeBuilder.TitleIndexName}')
            CREATE UNIQUE NONCLUSTERED INDEX [{isr.Dapper.Entities.LineEdgeBuilder.TitleIndexName}] ON [dbo].[{isr.Dapper.Entities.LineEdgeBuilder.TableName}] ([{nameof( LineEdgeNub.Title )}] ASC)
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>   Implements the LineEdge table <see cref="ILineEdge">interface</see>. </summary>
    /// <remarks>   David, 2020-10-02. </remarks>
    [Table( "LineEdge" )]
    public class LineEdgeNub : EntityNubBase<ILineEdge>, ILineEdge
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public LineEdgeNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ILineEdge CreateNew()
        {
            return new LineEdgeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ILineEdge CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( ILineEdge value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( ILineEdge source, ILineEdge destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Id = source.Id;
            destination.MeasureChannelList = source.MeasureChannelList;
            destination.SourceChannelList = source.SourceChannelList;
            destination.Title = source.Title;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as ILineEdge );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( ILineEdge other ) // Implements IEquatable(Of ILineEdge).Equals
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( ILineEdge left, ILineEdge right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.Id, right.Id );
                result = result && string.Equals( left.MeasureChannelList, right.MeasureChannelList );
                result = result && string.Equals( left.SourceChannelList, right.SourceChannelList );
                result = result && string.Equals( left.Title, right.Title );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.Id, this.MeasureChannelList, this.SourceChannelList, this.Title ).GetHashCode();
        }


        #endregion

        #region " FIELDS  "

        /// <summary>   Gets or sets the id of the line edge. </summary>
        /// <value> Identifies the line edge. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary>   Gets or sets the title. </summary>
        /// <value> The title. </value>
        public string Title { get; set; }

        /// <summary>   Gets or sets a list of source channels. </summary>
        /// <value> A List of source channels. </value>
        public string SourceChannelList { get; set; }

        /// <summary>   Gets or sets a list of measure channels. </summary>
        /// <value> A List of measure channels. </value>
        public string MeasureChannelList { get; set; }

        #endregion

    }

    /// <summary>   The LineEdge Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>   David, 2020-10-02. </remarks>
    public class LineEdgeEntity : EntityBase<ILineEdge, LineEdgeNub>, ILineEdge
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public LineEdgeEntity() : this( new LineEdgeNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LineEdgeEntity( ILineEdge value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public LineEdgeEntity( ILineEdge cache, ILineEdge store ) : base( new LineEdgeNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LineEdgeEntity( LineEdgeEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.LineEdgeBuilder.TableName, nameof( ILineEdge ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ILineEdge CreateNew()
        {
            return new LineEdgeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ILineEdge CreateCopy()
        {
            var destination = this.CreateNew();
            LineEdgeNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( ILineEdge value )
        {
            LineEdgeNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the Meter Model interface. </param>
        public override void UpdateCache( ILineEdge value )
        {
            // first make the copy to notify of any property change.
            LineEdgeNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The LineEdge table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<ILineEdge>( key ) : connection.Get<LineEdgeNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Title );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="title">        The LineEdge title. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string title )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, title ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-05-16. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ILineEdge entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Title ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool StoreEntity( System.Data.IDbConnection connection, ILineEdge entity )
        {
            return new LineEdgeEntity().Upsert( connection, entity );
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete<ILineEdge>( new LineEdgeNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the LineEdge entities. </summary>
        /// <value> The LineEdge entities. </value>
        public static IEnumerable<LineEdgeEntity> LineEdges { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LineEdgeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ILineEdge>() ) : Populate( connection.GetAll<LineEdgeNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            LineEdges = FetchAllEntities( connection, true );
            return LineEdges?.Any() == true ? LineEdges.Count() : 0;
        }

        /// <summary>   Dictionary of line edge titles. </summary>
        private static IDictionary<string, LineEdgeEntity> _LineEdgeTitleDictionary;

        /// <summary>   Line edge title dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of String, LineEdgeEntity) </returns>
        public static IDictionary<string, LineEdgeEntity> LineEdgeTitleDictionary()
        {
            if ( !(_LineEdgeTitleDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _LineEdgeTitleDictionary = new Dictionary<string, LineEdgeEntity>();
                foreach ( LineEdgeEntity entity in LineEdges )
                    _LineEdgeTitleDictionary.Add( entity.Title, entity );
            }

            return _LineEdgeTitleDictionary;
        }

        /// <summary>   Populates a list of entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The entity nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LineEdgeEntity> Populate( IEnumerable<LineEdgeNub> nubs )
        {
            var l = new List<LineEdgeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( LineEdgeNub nub in nubs )
                    l.Add( new LineEdgeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The entity interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LineEdgeEntity> Populate( IEnumerable<ILineEdge> interfaces )
        {
            var l = new List<LineEdgeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new LineEdgeNub();
                foreach ( ILineEdge iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new LineEdgeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="title">        The LineEdge title. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string title )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.LineEdgeBuilder.TableName}] WHERE {nameof( LineEdgeNub.Title )} = @title", new { title } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="title">        The LineEdge title. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LineEdgeNub> FetchNubs( System.Data.IDbConnection connection, string title )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LineEdgeBuilder.TableName}] WHERE {nameof( LineEdgeNub.Title )} = @title", new { title } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LineEdgeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the LineEdge exists. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="title">        The LineEdge title. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string title )
        {
            return 1 == CountEntities( connection, title );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the line edge. </summary>
        /// <value> Identifies the line edge. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the title. </summary>
        /// <value> The title. </value>
        public string Title
        {
            get => this.ICache.Title;

            set {
                if ( !string.Equals( this.Title, value ) )
                {
                    this.ICache.Title = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets a list of source channels. </summary>
        /// <value> A List of source channels. </value>
        public string SourceChannelList
        {
            get => this.ICache.SourceChannelList;

            set {
                if ( !string.Equals( this.SourceChannelList, value ) )
                {
                    this.ICache.SourceChannelList = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets a list of measure channels. </summary>
        /// <value> A List of measure channels. </value>
        public string MeasureChannelList
        {
            get => this.ICache.MeasureChannelList;

            set {
                if ( !string.Equals( this.MeasureChannelList, value ) )
                {
                    this.ICache.MeasureChannelList = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

    }
}

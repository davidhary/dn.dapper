using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A PCM element nominal trait builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class PcmElementNomTraitBuilder : OneToManyRealBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( PcmElementNomTraitNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = PcmElementBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( PcmElementNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = PcmElementNomTraitTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( PcmNomTraitTypeNub.Id );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( PcmElementNomTraitEntity.PcmElementAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( PcmElementNomTraitEntity.PcmNomTraitTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( PcmElementNomTraitEntity.Amount );

        #region " SINGLETON "

        private static readonly Lazy<PcmElementNomTraitBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static PcmElementNomTraitBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="PcmElementNomTraitEntity"/>
    /// <see cref="IOneToManyReal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "PcmElementNomTrait" )]
    public class PcmElementNomTraitNub : OneToManyRealNub, IOneToManyReal
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PcmElementNomTraitNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyReal CreateNew()
        {
            return new PcmElementNomTraitNub();
        }
    }

    /// <summary>
    /// The <see cref="PcmElementNomTraitEntity"/> holding PC element Nominal Traits.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class PcmElementNomTraitEntity : EntityBase<IOneToManyReal, PcmElementNomTraitNub>, IOneToManyReal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public PcmElementNomTraitEntity() : this( new PcmElementNomTraitNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PcmElementNomTraitEntity( IOneToManyReal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public PcmElementNomTraitEntity( IOneToManyReal cache, IOneToManyReal store ) : base( new PcmElementNomTraitNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PcmElementNomTraitEntity( PcmElementNomTraitEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.PcmElementNomTraitBuilder.TableName, nameof( IOneToManyReal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyReal CreateNew()
        {
            return new PcmElementNomTraitNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyReal CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyRealNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="PcmElementNomTraitEntity"/> interface value. </param>
        public override void CopyFrom( IOneToManyReal value )
        {
            OneToManyRealNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the <see cref="PcmElementEntity"/>Value interface. </param>
        public override void UpdateCache( IOneToManyReal value )
        {
            // first make the copy to notify of any property change.
            OneToManyRealNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="elementAutoId">        Identifies the
        ///                                     <see cref="PcmElementEntity"/>. </param>
        /// <param name="pcmElementNomTraitId"> Identifies the
        ///                                     <see cref="PcmElementEntity"/> value type. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int elementAutoId, int pcmElementNomTraitId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PcmElementNomTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNomTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNomTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = pcmElementNomTraitId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<PcmElementNomTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-15. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="elementAutoId">            Identifies the
        ///                                         <see cref="PcmElementEntity"/>. </param>
        /// <param name="pcmElementTraitTypeId">    Identifies the
        ///                                         <see cref="PcmElementEntity"/> value type. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int elementAutoId, int pcmElementTraitTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, elementAutoId, pcmElementTraitTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToManyReal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="elementAutoId">            Identifies the
        ///                                         <see cref="PcmElementEntity"/>. </param>
        /// <param name="pcmElementTraitTypeId">    Type of the <see cref="PcmElementEntity"/> value. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int elementAutoId, int pcmElementTraitTypeId )
        {
            return connection.Delete( new PcmElementNomTraitNub() { PrimaryId = elementAutoId, SecondaryId = pcmElementTraitTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>
        /// Gets or sets the <see cref="PcmElementNomTraitEntity"/>'s.
        /// </summary>
        /// <value> the <see cref="PcmElementNomTraitEntity"/>'s. </value>
        public IEnumerable<PcmElementNomTraitEntity> PcmElementNomTraits { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PcmElementNomTraitEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToManyReal>() ) : Populate( connection.GetAll<PcmElementNomTraitNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.PcmElementNomTraits = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( PcmElementNomTraitEntity.PcmElementNomTraits ) );
            return this.PcmElementNomTraits?.Any() == true ? this.PcmElementNomTraits.Count() : 0;
        }

        /// <summary>   Count the <see cref="PcmElementNomTraitEntity"/>'s associated with the <see cref="PcmElementEntity"/>. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="PcmElementEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.PcmElementNomTraitBuilder.TableName}] WHERE {nameof( PcmElementNomTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="PcmElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PcmElementNomTraitEntity> FetchEntities( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PcmElementNomTraitBuilder.TableName}] WHERE {nameof( PcmElementNomTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<PcmElementNomTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>
        /// Fetches PCM Element Nominal Traits by PcmElement number and PCM element Nominal Trait PCM
        /// element nominal Trait Type;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="PcmElementEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<PcmElementNomTraitNub> FetchNubs( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PcmElementNomTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNomTraitEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PcmElementNomTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of <see cref="PcmElementNomTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the <see cref="PcmElementNomTraitNub"/>'s. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PcmElementNomTraitEntity> Populate( IEnumerable<PcmElementNomTraitNub> nubs )
        {
            var l = new List<PcmElementNomTraitEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( PcmElementNomTraitNub nub in nubs )
                    l.Add( new PcmElementNomTraitEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of <see cref="PcmElementNomTraitEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the <see cref="PcmElementEntity"/>Value interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PcmElementNomTraitEntity> Populate( IEnumerable<IOneToManyReal> interfaces )
        {
            var l = new List<PcmElementNomTraitEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new PcmElementNomTraitNub();
                foreach ( IOneToManyReal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new PcmElementNomTraitEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count PCM Element Nominal Traits; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">               The connection. </param>
        /// <param name="elementAutoId">            Identifies the <see cref="PcmElementEntity"/>. </param>
        /// <param name="pcmElementTraitTypeId">    Type of the <see cref="PcmElementEntity"/> value. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int elementAutoId, int pcmElementTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PcmElementNomTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNomTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNomTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = pcmElementTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches PCM Element Nominal Traits by PcmElement number and PCM element Nominal Trait PCM
        /// Element Trait Type;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">               The connection. </param>
        /// <param name="elementAutoId">            Identifies the <see cref="PcmElementEntity"/>. </param>
        /// <param name="pcmElementTraitTypeId">    Identifies the <see cref="PcmElementEntity"/> value
        ///                                         type. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<PcmElementNomTraitNub> FetchEntities( System.Data.IDbConnection connection, int elementAutoId, int pcmElementTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PcmElementNomTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNomTraitNub.PrimaryId )} = @primaryId", new { primaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNomTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = pcmElementTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PcmElementNomTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="PcmElementEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="elementAutoId">            Identifies the <see cref="PcmElementEntity"/>. </param>
        /// <param name="pcmElementTraitTypeId">    Identifies the PCM element nominal Trait Type. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int elementAutoId, int pcmElementTraitTypeId )
        {
            return 1 == CountEntities( connection, elementAutoId, pcmElementTraitTypeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count values associated with this Pcm element Nominal Trait. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of records. </returns>
        public int CountPcmElementNomTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{PcmElementNomTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNomTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches PCM element nominal trait values by PcmElement auto id;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<PcmElementNomTraitNub> FetchPcmElementNomTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{PcmElementNomTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( PcmElementNomTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PcmElementNomTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="PcmElementEntity"/>. </summary>
        /// <value> the <see cref="PcmElementEntity"/>. </value>
        public PcmElementEntity PcmElementEntity { get; private set; }

        /// <summary>   Fetches a <see cref="PcmElementEntity"/> . </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchPcmElementEntity( System.Data.IDbConnection connection )
        {
            this.PcmElementEntity = new PcmElementEntity();
            return this.PcmElementEntity.FetchUsingKey( connection, this.PcmElementAutoId );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.PcmNomTraitTypeEntity"/> . </summary>
        /// <value> the <see cref="Dapper.Entities.PcmNomTraitTypeEntity"/> . </value>
        public PcmNomTraitTypeEntity PcmNomTraitTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.PcmNomTraitTypeEntity"/>. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchPcmNomTraitTypeEntity( System.Data.IDbConnection connection )
        {
            this.PcmNomTraitTypeEntity = new PcmNomTraitTypeEntity();
            return this.PcmNomTraitTypeEntity.FetchUsingKey( connection, this.PcmNomTraitTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PcmElementNomTraitEntity.PcmElementAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="PcmElementEntity"/> record. </summary>
        /// <value> Identifies the <see cref="PcmElementEntity"/> record. </value>
        public int PcmElementAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PcmElementNomTraitEntity.PcmNomTraitTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.PcmNomTraitTypeEntity"/>.
        /// </summary>
        /// <value> Identifies the PCM Nominal Trait type. </value>
        public int PcmNomTraitTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>
        /// Gets or sets the value assigned to the <see cref="PcmElementEntity"/> for the
        /// specific <see cref="Dapper.Entities.PcmNomTraitTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The value assigned to the <see cref="PcmElementEntity"/> for the specific
        /// <see cref="Dapper.Entities.PcmNomTraitTypeEntity"/>.
        /// </value>
        public double Amount
        {
            get => this.ICache.Amount;

            set {
                if ( !object.Equals( this.Amount, value ) )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>
    /// Collection of <see cref="PcmElementNomTraitEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class PcmElementNomTraitEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToManyReal, PcmElementNomTraitNub, PcmElementNomTraitEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( PcmElementNomTraitEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( PcmElementNomTraitEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all pcmPcmElements from the
        /// <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<PcmElementNomTraitEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( PcmElementNomTraitEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return PcmElementNomTraitBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of PcmElement-Unique Pcm element Nominal Trait Real Entities unique to the
    /// pcmPcmElement.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class PcmElementUniqueTraitEntityCollection : PcmElementNomTraitEntityCollection
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public PcmElementUniqueTraitEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="pcmPcmElementAutoId">  Identifies the PCM element automatic. </param>
        public PcmElementUniqueTraitEntityCollection( int pcmPcmElementAutoId ) : this()
        {
            this.PcmElementAutoId = pcmPcmElementAutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( PcmElementNomTraitEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.PcmNomTraitTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.PcmNomTraitTypeId );
            this.NotifyPropertyChanged( PcmNomTraitTypeEntity.EntityLookupDictionary()[entity.PcmNomTraitTypeId].Label );
        }

        /// <summary>
        /// Removes all pcmPcmElements from the
        /// <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains akey. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="pcmNomTraitTypeId">    Identifies the <see cref="PcmNomTraitType"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int pcmNomTraitTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( pcmNomTraitTypeId );
        }

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="pcmNomTraitTypeId">    Identifies the <see cref="PcmNomTraitType"/>. </param>
        /// <returns>   A PcmElementNomTraitEntity. </returns>
        public PcmElementNomTraitEntity Entity( int pcmNomTraitTypeId )
        {
            return this.ContainsKey( pcmNomTraitTypeId ) ? this[this._PrimaryKeyDictionary[pcmNomTraitTypeId]] : new PcmElementNomTraitEntity();
        }

        /// <summary>   Gets the value for the specified Trait Type identifier. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="pcmNomTraitTypeId">    Identifies the <see cref="PcmNomTraitType"/>. </param>
        /// <returns>   A Double? </returns>
        public double? Getter( int pcmNomTraitTypeId )
        {
            return this.ContainsKey( pcmNomTraitTypeId ) ? this[this._PrimaryKeyDictionary[pcmNomTraitTypeId]].Amount : new double?();
        }

        /// <summary>   Set the specified PcmElement value. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="pcmNomTraitTypeId">    Identifies the <see cref="PcmNomTraitType"/>. </param>
        /// <param name="value">                The value. </param>
        public void Setter( int pcmNomTraitTypeId, double value )
        {
            if ( this.ContainsKey( pcmNomTraitTypeId ) )
            {
                this[this._PrimaryKeyDictionary[pcmNomTraitTypeId]].Amount = value;
            }
            else
            {
                this.Add( new PcmElementNomTraitEntity() { PcmElementAutoId = PcmElementAutoId, PcmNomTraitTypeId = pcmNomTraitTypeId, Amount = value } );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="PcmElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="PcmElementEntity"/>. </value>
        public int PcmElementAutoId { get; private set; }
    }
}

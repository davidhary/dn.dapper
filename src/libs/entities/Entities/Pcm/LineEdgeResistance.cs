using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the LineEdgeResistance nub and entity. Includes the fields as kept in the data
    /// table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ILineEdgeResistance
    {

        /// <summary>   Gets or sets the id of the Line edge resistance. </summary>
        /// <value> Identifies the Line edge resistance. </value>
        [Key]
        int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="StructureEntity"/>. </summary>
        /// <value> Identifies the <see cref="StructureEntity"/>. </value>
        int StructureAutoId { get; set; }

        /// <summary>   Gets or sets the id of the Line edge resistance. </summary>
        /// <value> Identifies the Line edge resistance. </value>
        int LineEdgeId { get; set; }

        /// <summary>   Gets or sets the voltage. </summary>
        /// <value> The voltage. </value>
        double? Voltage { get; set; }

        /// <summary>   Gets or sets the current. </summary>
        /// <value> The current. </value>
        double? Current { get; set; }
    }

    /// <summary>   The Line edge resistance builder. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public sealed class LineEdgeResistanceBuilder
    {

        private static string _tableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _tableName ) )
                {
                    _tableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( LineEdgeResistanceNub ), typeof( TableAttribute ) )).Name;
                }

                return _tableName;
            }
        }

        /// <summary>   Gets the name of the unique index. </summary>
        /// <value> The name of the unique index. </value>
        private static string UniqueIndexName => $"UQ_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}_{nameof( LineEdgeResistanceNub.StructureAutoId )}_{nameof( LineEdgeResistanceNub.LineEdgeId )}";

        private static bool? _usingUniqueIndex;

        /// <summary>   Indicates if the entity uses a unique Title. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool UsingUniqueTitle( System.Data.IDbConnection connection )
        {
            if ( !_usingUniqueIndex.HasValue )
            {
                _usingUniqueIndex = connection.IndexExists( UniqueIndexName );
            }

            return _usingUniqueIndex.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}] (
            [{nameof( LineEdgeResistanceNub.AutoId )}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
            [{nameof( LineEdgeResistanceNub.StructureAutoId )}] integer NOT NULL, 
            [{nameof( LineEdgeResistanceNub.LineEdgeId )}] integer NOT NULL, 
            [{nameof( LineEdgeResistanceNub.Voltage )}] float, 
            [{nameof( LineEdgeResistanceNub.Current )}] float, 
            FOREIGN KEY ([{nameof( LineEdgeResistanceNub.StructureAutoId )}]) REFERENCES [{isr.Dapper.Entities.StructureBuilder.TableName}] ([{nameof( StructureNub.AutoId )}])
                    ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( LineEdgeResistanceNub.LineEdgeId )}]) REFERENCES [{isr.Dapper.Entities.LineEdgeBuilder.TableName}] ([{nameof( LineEdgeNub.Id )}])
                    ON UPDATE CASCADE ON DELETE CASCADE);
            CREATE UNIQUE INDEX [{isr.Dapper.Entities.LineEdgeResistanceBuilder.UniqueIndexName}] ON [{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}] ([{nameof( LineEdgeResistanceNub.StructureAutoId )}], [{nameof( LineEdgeResistanceNub.LineEdgeId )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}](
                [{nameof( LineEdgeResistanceNub.AutoId )}] [int] IDENTITY(1,1) NOT NULL,
                [{nameof( LineEdgeResistanceNub.StructureAutoId )}] [int] NOT NULL,
                [{nameof( LineEdgeResistanceNub.LineEdgeId )}] [int] NOT NULL,
                [{nameof( LineEdgeResistanceNub.Voltage )}] [float] NULL,
                [{nameof( LineEdgeResistanceNub.Current )}] [float] NULL,
             CONSTRAINT [PK_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}] PRIMARY KEY CLUSTERED ([{nameof( LineEdgeResistanceNub.AutoId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END;

            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}]') AND name = N'{isr.Dapper.Entities.LineEdgeResistanceBuilder.UniqueIndexName}')
            CREATE UNIQUE NONCLUSTERED INDEX [{isr.Dapper.Entities.LineEdgeResistanceBuilder.UniqueIndexName}] ON [dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}] ([{nameof( LineEdgeResistanceNub.StructureAutoId )}] ASC, [{nameof( LineEdgeResistanceNub.LineEdgeId )}] ASC) 
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}_{isr.Dapper.Entities.LineEdgeBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}]'))
                ALTER TABLE [dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}] WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}_{isr.Dapper.Entities.LineEdgeBuilder.TableName}] FOREIGN KEY([{nameof( LineEdgeResistanceNub.LineEdgeId )}])
                REFERENCES [dbo].[{isr.Dapper.Entities.LineEdgeBuilder.TableName}] ([{nameof( LineEdgeNub.Id )}])
                ON UPDATE CASCADE ON DELETE CASCADE; 

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}_{isr.Dapper.Entities.LineEdgeBuilder.TableName}') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}]'))
                ALTER TABLE [dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}_{isr.Dapper.Entities.LineEdgeBuilder.TableName}]; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}_{isr.Dapper.Entities.StructureBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}_{isr.Dapper.Entities.StructureBuilder.TableName}] FOREIGN KEY([{nameof( LineEdgeResistanceNub.StructureAutoId )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.StructureBuilder.TableName}] ([{nameof( StructureNub.AutoId )}])
            ON UPDATE CASCADE ON DELETE CASCADE;

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}_{isr.Dapper.Entities.StructureBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}_{isr.Dapper.Entities.StructureBuilder.TableName}]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// Implements the LineEdgeResistance table <see cref="ILineEdgeResistance">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "LineEdgeResistance" )]
    public class LineEdgeResistanceNub : EntityNubBase<ILineEdgeResistance>, ILineEdgeResistance
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public LineEdgeResistanceNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ILineEdgeResistance CreateNew()
        {
            return new LineEdgeResistanceNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ILineEdgeResistance CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( ILineEdgeResistance value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( ILineEdgeResistance source, ILineEdgeResistance destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.LineEdgeId = source.LineEdgeId;
            destination.AutoId = source.AutoId;
            destination.Current = source.Current;
            destination.StructureAutoId = source.StructureAutoId;
            destination.Voltage = source.Voltage;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as ILineEdgeResistance );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( ILineEdgeResistance other ) // Implements IEquatable(Of ILineEdgeResistance).Equals
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( ILineEdgeResistance left, ILineEdgeResistance right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.LineEdgeId, right.LineEdgeId );
                result = result && Equals( left.AutoId, right.AutoId );
                result = result && Equals( left.Current, right.Current );
                result = result && Equals( left.StructureAutoId, right.StructureAutoId );
                result = result && Equals( left.Voltage, right.Voltage );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.LineEdgeId, this.AutoId, this.Current, this.StructureAutoId, this.Voltage ).GetHashCode();
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Line edge resistance. </summary>
        /// <value> Identifies the Line edge resistance. </value>
        [Key]
        public int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="StructureEntity"/>. </summary>
        /// <value> Identifies the <see cref="StructureEntity"/>. </value>
        public int StructureAutoId { get; set; }

        /// <summary>   Gets or sets the id of the Line edge resistance. </summary>
        /// <value> Identifies the Line edge resistance. </value>
        public int LineEdgeId { get; set; }

        /// <summary>   Gets or sets the voltage. </summary>
        /// <value> The voltage. </value>
        public double? Voltage { get; set; }

        /// <summary>   Gets or sets the current. </summary>
        /// <value> The current. </value>
        public double? Current { get; set; }

        #endregion

    }

    /// <summary>
    /// The LineEdgeResistance Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class LineEdgeResistanceEntity : EntityBase<ILineEdgeResistance, LineEdgeResistanceNub>, ILineEdgeResistance
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public LineEdgeResistanceEntity() : this( new LineEdgeResistanceNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LineEdgeResistanceEntity( ILineEdgeResistance value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public LineEdgeResistanceEntity( ILineEdgeResistance cache, ILineEdgeResistance store ) : base( new LineEdgeResistanceNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LineEdgeResistanceEntity( LineEdgeResistanceEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName, nameof( ILineEdgeResistance ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ILineEdgeResistance CreateNew()
        {
            return new LineEdgeResistanceNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ILineEdgeResistance CreateCopy()
        {
            var destination = this.CreateNew();
            LineEdgeResistanceNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( ILineEdgeResistance value )
        {
            LineEdgeResistanceNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the Meter Model interface. </param>
        public override void UpdateCache( ILineEdgeResistance value )
        {
            // first make the copy to notify of any property change.
            LineEdgeResistanceNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The LineEdgeResistance table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<ILineEdgeResistance>( key ) : connection.Get<LineEdgeResistanceNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.StructureAutoId, this.LineEdgeId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="lineEdgeId">       Identifies the Line edge resistance. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int structureAutoId, int lineEdgeId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, structureAutoId, lineEdgeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ILineEdgeResistance entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.StructureAutoId, entity.LineEdgeId ) )
            {
                // update the existing record from the specified entity.
                entity.AutoId = this.AutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool StoreEntity( System.Data.IDbConnection connection, ILineEdgeResistance entity )
        {
            return new LineEdgeResistanceEntity().Upsert( connection, entity );
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete<ILineEdgeResistance>( new LineEdgeResistanceNub() { AutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the LineEdgeResistance entities. </summary>
        /// <value> The LineEdgeResistance entities. </value>
        public IEnumerable<LineEdgeResistanceEntity> LineEdgeResistances { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LineEdgeResistanceEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ILineEdgeResistance>() ) : Populate( connection.GetAll<LineEdgeResistanceNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.LineEdgeResistances = FetchAllEntities( connection, true );
            this.NotifyPropertyChanged( nameof( LineEdgeResistanceEntity.LineEdgeResistances ) );
            return this.LineEdgeResistances?.Any() == true ? this.LineEdgeResistances.Count() : 0;
        }

        /// <summary>   Fetches entities in this collection. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LineEdgeResistanceEntity> FetchEntities( System.Data.IDbConnection connection, int structureAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate( @$"SELECT * FROM [{isr.Dapper.Entities.LineEdgeResistanceBuilder.TableName}]
            WHERE {nameof( LineEdgeResistanceNub.StructureAutoId )} = @Id", new { Id = structureAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<LineEdgeResistanceNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Populates a list of entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The entity nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LineEdgeResistanceEntity> Populate( IEnumerable<LineEdgeResistanceNub> nubs )
        {
            var l = new List<LineEdgeResistanceEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( LineEdgeResistanceNub nub in nubs )
                    l.Add( new LineEdgeResistanceEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The entity interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LineEdgeResistanceEntity> Populate( IEnumerable<ILineEdgeResistance> interfaces )
        {
            var l = new List<LineEdgeResistanceEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new LineEdgeResistanceNub();
                foreach ( ILineEdgeResistance iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new LineEdgeResistanceEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="lineEdgeId">       Identifies the Line edge resistance. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int structureAutoId, int lineEdgeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LineEdgeResistanceBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( "StructureAutoId = @StructureAutoId", new { structureAutoId } );
            _ = sqlBuilder.Where( "LineEdgeId = @LineEdgeId", new { lineEdgeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="lineEdgeId">       Identifies the Line edge resistance. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LineEdgeResistanceNub> FetchNubs( System.Data.IDbConnection connection, int structureAutoId, int lineEdgeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LineEdgeResistanceBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( "StructureAutoId = @StructureAutoId", new { structureAutoId } );
            _ = sqlBuilder.Where( "LineEdgeId = @LineEdgeId", new { lineEdgeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LineEdgeResistanceNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Line Edge Resistance record exists. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="structureAutoId">  Identifies the <see cref="StructureEntity"/>. </param>
        /// <param name="lineEdgeId">       Identifies the Line edge resistance. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int structureAutoId, int lineEdgeId )
        {
            return 1 == CountEntities( connection, structureAutoId, lineEdgeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Line edge entity. </summary>
        /// <value> The Line edge entity. </value>
        public LineEdgeEntity LineEdgeEntity { get; private set; }

        /// <summary>   Fetches Line edge Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Line edge. </returns>
        public LineEdgeEntity FetchLineEdgeEntity( System.Data.IDbConnection connection )
        {
            this.LineEdgeEntity = new LineEdgeEntity();
            _ = this.LineEdgeEntity.FetchUsingKey( connection, this.LineEdgeId );
            return this.LineEdgeEntity;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Line edge resistance. </summary>
        /// <value> Identifies the Line edge resistance. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="StructureEntity"/>. </summary>
        /// <value> Identifies the <see cref="StructureEntity"/>. </value>
        public int StructureAutoId
        {
            get => this.ICache.StructureAutoId;

            set {
                if ( !object.Equals( this.StructureAutoId, value ) )
                {
                    this.ICache.StructureAutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the Line edge resistance. </summary>
        /// <value> Identifies the Line edge resistance. </value>
        public int LineEdgeId
        {
            get => this.ICache.LineEdgeId;

            set {
                if ( !object.Equals( this.LineEdgeId, value ) )
                {
                    this.ICache.LineEdgeId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the voltage. </summary>
        /// <value> The voltage. </value>
        public double? Voltage
        {
            get => this.ICache.Voltage;

            set {
                if ( !object.Equals( this.Voltage, value ) )
                {
                    this.ICache.Voltage = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the current. </summary>
        /// <value> The current. </value>
        public double? Current
        {
            get => this.ICache.Current;

            set {
                if ( !object.Equals( this.Current, value ) )
                {
                    this.ICache.Current = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A PCM reading Type builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class PcmReadingTypeBuilder : NominalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( PcmReadingTypeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>
        /// Inserts or ignores the records described by the <see cref="PcmReadingType"/> enumeration type.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public override int InsertIgnoreDefaultRecords( System.Data.IDbConnection connection )
        {
            return this.InsertIgnore( connection, typeof( PcmReadingType ), new int[] { ( int ) PcmReadingType.None } );
        }

        #region " SINGLETON "

        private static readonly Lazy<PcmReadingTypeBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static PcmReadingTypeBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the PcmReadingType table based on the <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "PcmReadingType" )]
    public class PcmReadingTypeNub : NominalNub, INominal
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public PcmReadingTypeNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new PcmReadingTypeNub();
        }
    }


    /// <summary>   The PcmReadingType Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class PcmReadingTypeEntity : EntityBase<INominal, PcmReadingTypeNub>, INominal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public PcmReadingTypeEntity() : this( new PcmReadingTypeNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PcmReadingTypeEntity( INominal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public PcmReadingTypeEntity( INominal cache, INominal store ) : base( new PcmReadingTypeNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PcmReadingTypeEntity( PcmReadingTypeEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.PcmReadingTypeBuilder.TableName, nameof( INominal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new PcmReadingTypeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override INominal CreateCopy()
        {
            var destination = this.CreateNew();
            NominalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( INominal value )
        {
            NominalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The PcmReadingType interface. </param>
        public override void UpdateCache( INominal value )
        {
            // first make the copy to notify of any property change.
            NominalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The PcmReadingType table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<INominal>( key ) : connection.Get<PcmReadingTypeNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Label );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The PCM Reading Type Label. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string label )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, label ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, INominal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Label ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new PcmReadingTypeNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the PcmReadingType entities. </summary>
        /// <value> The PcmReadingType entities. </value>
        public static IEnumerable<PcmReadingTypeEntity> PcmReadingTypes { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PcmReadingTypeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<INominal>() ) : Populate( connection.GetAll<PcmReadingTypeNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            PcmReadingTypes = FetchAllEntities( connection, this.UsingNativeTracking );
            return PcmReadingTypes?.Any() == true ? PcmReadingTypes.Count() : 0;
        }

        /// <summary>   Populates a list of PcmReadingType entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The PcmReadingType nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PcmReadingTypeEntity> Populate( IEnumerable<PcmReadingTypeNub> nubs )
        {
            var l = new List<PcmReadingTypeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( PcmReadingTypeNub nub in nubs )
                    l.Add( new PcmReadingTypeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of PcmReadingType entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The PcmReadingType interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PcmReadingTypeEntity> Populate( IEnumerable<INominal> interfaces )
        {
            var l = new List<PcmReadingTypeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new PcmReadingTypeNub();
                foreach ( INominal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new PcmReadingTypeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, PcmReadingTypeEntity> _EntityLookupDictionary;

        /// <summary>   The PCM reading Type lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A Dictionary(Of Integer, PcmReadingTypeEntity) </returns>
        public static IDictionary<int, PcmReadingTypeEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, PcmReadingTypeEntity>();
                foreach ( PcmReadingTypeEntity entity in PcmReadingTypes )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Dictionary of key lookups. </summary>
        private static IDictionary<string, int> _KeyLookupDictionary;

        /// <summary>   The key lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A Dictionary(Of String, Integer) </returns>
        public static IDictionary<string, int> KeyLookupDictionary()
        {
            if ( !(_KeyLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _KeyLookupDictionary = new Dictionary<string, int>();
                foreach ( PcmReadingTypeEntity entity in PcmReadingTypes )
                    _KeyLookupDictionary.Add( entity.Label, entity.Id );
            }

            return _KeyLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (PcmReadingTypes?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) &&
                   (_KeyLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The PCM reading Type label. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.PcmReadingTypeBuilder.TableName}] WHERE {nameof( PcmReadingTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The PCM reading Type label. </param>
        /// <returns>   An IPcmReadingType . </returns>
        public static IEnumerable<PcmReadingTypeNub> FetchNubs( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PcmReadingTypeBuilder.TableName}] WHERE {nameof( PcmReadingTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PcmReadingTypeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the PcmReadingType exists. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The PCM Reading Type Label. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string label )
        {
            return 1 == CountEntities( connection, label );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="PcmReadingTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="PcmReadingTypeEntity"/>. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( ( object ) this.Id, ( object ) value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( PcmReadingTypeEntity.PcmReadingType ) );
                }
            }
        }

        /// <summary>   Gets or sets the PcmReadingType. </summary>
        /// <value> The PcmReadingType. </value>
        public PcmReadingType PcmReadingType
        {
            get => ( PcmReadingType ) this.Id;

            set => this.Id = ( int ) value;
        }

        /// <summary>   Gets or sets the PCM reading Type label. </summary>
        /// <value> The PCM reading Type label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the description of the reading type. </summary>
        /// <value> The PCM reading Type description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="pcmReadingTypeId"> The entity unique key. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entity As PcmReadingTypeEntity)
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, PcmReadingTypeEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int pcmReadingTypeId )
        {
            string activity = string.Empty;
            var entity = new PcmReadingTypeEntity();
            bool success = true;
            string details = string.Empty;
            try
            {
                activity = $"Fetching {nameof( PcmReadingTypeEntity )} by {nameof( PcmReadingTypeNub.Id )} of {pcmReadingTypeId}";
                if ( !entity.FetchUsingKey( connection, pcmReadingTypeId ) )
                {
                    details = $"Failed {activity}";
                    success = false;
                }
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                details = $"Exception {activity};. {ex}";

                success = false;
            }

            return (success, details, entity);
        }

        /// <summary>   Try fetch using unique index. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="pcmReadingTypeLabel">  The PCM reading Type label. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entity As PcmReadingTypeEntity)
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, PcmReadingTypeEntity Entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, string pcmReadingTypeLabel )
        {
            string activity = string.Empty;
            var entity = new PcmReadingTypeEntity();
            bool success = true;
            string details = string.Empty;
            try
            {
                activity = $"Fetching {nameof( PcmReadingTypeEntity )} by {nameof( PcmReadingTypeNub.Label )} of {pcmReadingTypeLabel}";
                if ( !entity.FetchUsingUniqueIndex( connection, pcmReadingTypeLabel ) )
                {
                    details = $"Failed {activity}";
                    success = false;
                }
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                details = $"Exception {activity};. {ex}";

                success = false;
            }

            return (success, details, entity);
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entities As IEnumerable(Of PcmReadingTypeEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, IEnumerable<PcmReadingTypeEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new PcmReadingTypeEntity();
                activity = $"fetching all {nameof( PcmReadingTypeEntity )}'s";
                int elementCount = entity.FetchAllEntities( connection );
                if ( elementCount != EntityLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( PcmReadingTypeEntity.EntityLookupDictionary )} count must equal {nameof( PcmReadingTypes )} count " );
                }
                else if ( elementCount != KeyLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( PcmReadingTypeEntity.KeyLookupDictionary )} count must equal {nameof( PcmReadingTypes )} count " );
                }

                return (true, string.Empty, PcmReadingTypes);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<PcmReadingTypeEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }

    /// <summary>   Values that represent reading types. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public enum PcmReadingType
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "PCM Reading Type" )]
        None,

        /// <summary>   An enum constant representing the sheet resistance option. </summary>
        [System.ComponentModel.Description( "Sheet Resistance" )]
        SheetResistance,

        /// <summary>   An enum constant representing the line width option. </summary>
        [System.ComponentModel.Description( "Line Width" )]
        LineWidth,

        /// <summary>   An enum constant representing the line width deviation option. </summary>
        [System.ComponentModel.Description( "Line Width Deviation" )]
        LineWidthDeviation,

        /// <summary>   An enum constant representing the split line width option. </summary>
        [System.ComponentModel.Description( "Split Line Width" )]
        SplitLineWidth,

        /// <summary>   An enum constant representing the split line width deviation option. </summary>
        [System.ComponentModel.Description( "Split Line Width Deviation" )]
        SplitLineWidthDeviation,

        /// <summary>   An enum constant representing the Line Pitch option. </summary>
        [System.ComponentModel.Description( "Line Pitch" )]
        LinePitch,

        /// <summary>   An enum constant representing the Test Current option. </summary>
        [System.ComponentModel.Description( "Test Current" )]
        TestCurrent
    }
}

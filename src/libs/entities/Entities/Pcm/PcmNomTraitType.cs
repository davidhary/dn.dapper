using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A PCM Nominal Trait type builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class PcmElementNomTraitTypeBuilder : NominalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( PcmNomTraitTypeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>
        /// Inserts or ignores the records described by the <see cref="PcmNomTraitType"/> enumeration
        /// type.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public override int InsertIgnoreDefaultRecords( System.Data.IDbConnection connection )
        {
            return this.InsertIgnore( connection, typeof( PcmNomTraitType ), new int[] { ( int ) PcmNomTraitType.None } );
        }

        #region " SINGLETON "

        private static readonly Lazy<PcmElementNomTraitTypeBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static PcmElementNomTraitTypeBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="PcmNomTraitTypeEntity"/> table based on the
    /// <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "PcmNomTraitType" )]
    public class PcmNomTraitTypeNub : NominalNub, INominal
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public PcmNomTraitTypeNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new PcmNomTraitTypeNub();
        }
    }


    /// <summary>
    /// Implements the <see cref="PcmNomTraitTypeEntity"/> table based on the
    /// <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class PcmNomTraitTypeEntity : EntityBase<INominal, PcmNomTraitTypeNub>, INominal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public PcmNomTraitTypeEntity() : this( new PcmNomTraitTypeNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PcmNomTraitTypeEntity( INominal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public PcmNomTraitTypeEntity( INominal cache, INominal store ) : base( new PcmNomTraitTypeNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public PcmNomTraitTypeEntity( PcmNomTraitTypeEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.PcmElementNomTraitTypeBuilder.TableName, nameof( INominal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override INominal CreateNew()
        {
            return new PcmNomTraitTypeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override INominal CreateCopy()
        {
            var destination = this.CreateNew();
            NominalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( INominal value )
        {
            NominalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Pcm Nominal Trait Type interface. </param>
        public override void UpdateCache( INominal value )
        {
            // first make the copy to notify of any property change.
            NominalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The Pcm Nominal Trait Type table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<INominal>( key ) : connection.Get<PcmNomTraitTypeNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Label );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The PCM Nominal Trait Type label. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string label )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, label ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, INominal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Label ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new PcmNomTraitTypeNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the Pcm  Trait Type entities. </summary>
        /// <value> A list of types of the PCM Nominal Traits. </value>
        public static IEnumerable<PcmNomTraitTypeEntity> PcmNomTraitTypes { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<PcmNomTraitTypeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<INominal>() ) : Populate( connection.GetAll<PcmNomTraitTypeNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            PcmNomTraitTypes = FetchAllEntities( connection, this.UsingNativeTracking );
            return PcmNomTraitTypes?.Any() == true ? PcmNomTraitTypes.Count() : 0;
        }

        /// <summary>   Populates a list of <see cref="PcmNomTraitTypeEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The <see cref="PcmNomTraitTypeNub"/>'s. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PcmNomTraitTypeEntity> Populate( IEnumerable<PcmNomTraitTypeNub> nubs )
        {
            var l = new List<PcmNomTraitTypeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( PcmNomTraitTypeNub nub in nubs )
                    l.Add( new PcmNomTraitTypeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of <see cref="PcmNomTraitTypeEntity"/>'s. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The Pcm Nominal Trait Type interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<PcmNomTraitTypeEntity> Populate( IEnumerable<INominal> interfaces )
        {
            var l = new List<PcmNomTraitTypeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new PcmNomTraitTypeNub();
                foreach ( INominal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new PcmNomTraitTypeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, PcmNomTraitTypeEntity> _EntityLookupDictionary;

        /// <summary>   The PCM Nominal Trait Type lookup dictionary. </summary>
        /// <remarks>   David, 2020-06-12. </remarks>
        /// <returns>   A Dictionary(Of Integer, PcmNomTraitTypeEntity) </returns>
        public static IDictionary<int, PcmNomTraitTypeEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, PcmNomTraitTypeEntity>();
                foreach ( PcmNomTraitTypeEntity entity in PcmNomTraitTypes )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Dictionary of key lookups. </summary>
        private static IDictionary<string, int> _KeyLookupDictionary;

        /// <summary>   The key lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A Dictionary(Of String, Integer) </returns>
        public static IDictionary<string, int> KeyLookupDictionary()
        {
            if ( !(_KeyLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _KeyLookupDictionary = new Dictionary<string, int>();
                foreach ( PcmNomTraitTypeEntity entity in PcmNomTraitTypes )
                    _KeyLookupDictionary.Add( entity.Label, entity.Id );
            }

            return _KeyLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (PcmNomTraitTypes?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) &&
                   (_KeyLookupDictionary?.Any()).GetValueOrDefault( false );
        }


        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The PCM Nominal Trait Type label. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.PcmElementNomTraitTypeBuilder.TableName}] WHERE {nameof( PcmNomTraitTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-01. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The PCM Nominal Trait Type label. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the nubs in this collection.
        /// </returns>
        public static IEnumerable<PcmNomTraitTypeNub> FetchNubs( System.Data.IDbConnection connection, string label )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.PcmElementNomTraitTypeBuilder.TableName}] WHERE {nameof( PcmNomTraitTypeNub.Label )} = @label", new { label } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<PcmNomTraitTypeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="PcmNomTraitTypeEntity"/> exists. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="label">        The PCM Nominal Trait Type label. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string label )
        {
            return 1 == CountEntities( connection, label );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the PCM Nominal Trait Type . </summary>
        /// <value> Identifies the PCM Nominal Trait Type . </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the PCM Nominal Trait Type label. </summary>
        /// <value> The PCM Nominal Trait Type label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the description of the PCM Nominal Trait Type . </summary>
        /// <value> The PCM Nominal Trait Type description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="pcmNomTraitTypeId">    The entity unique key. </param>
        /// <returns>   A <see cref="PcmNomTraitTypeEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, PcmNomTraitTypeEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int pcmNomTraitTypeId )
        {
            string activity = string.Empty;
            var entity = new PcmNomTraitTypeEntity();
            try
            {
                activity = $"Fetching {nameof( PcmNomTraitTypeEntity )} by {nameof( PcmNomTraitTypeNub.Id )} of {pcmNomTraitTypeId}";
                return entity.FetchUsingKey( connection, pcmNomTraitTypeId ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Try fetch using unique index. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="pcmNomTraitTypeLabel"> The PCM Nominal Trait Type label. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entity As PcmNomTraitTypeEntity)
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, PcmNomTraitTypeEntity Entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, string pcmNomTraitTypeLabel )
        {
            string activity = string.Empty;
            var entity = new PcmNomTraitTypeEntity();
            try
            {
                activity = $"Fetching {nameof( PcmNomTraitTypeEntity )} by {nameof( PcmNomTraitTypeNub.Label )} of {pcmNomTraitTypeLabel}";
                return entity.FetchUsingUniqueIndex( connection, pcmNomTraitTypeLabel ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entities As IEnumerable(Of PcmNomTraitTypeEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, IEnumerable<PcmNomTraitTypeEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new PcmNomTraitTypeEntity();
                activity = $"fetching all {nameof( PcmNomTraitTypeEntity )}'s";
                int elementCount = entity.FetchAllEntities( connection );
                if ( elementCount != EntityLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( PcmNomTraitTypeEntity.EntityLookupDictionary )} count must equal {nameof( PcmNomTraitTypes )} count " );
                }
                else if ( elementCount != KeyLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( PcmNomTraitTypeEntity.KeyLookupDictionary )} count must equal {nameof( PcmNomTraitTypes )} count " );
                }

                return (true, string.Empty, PcmNomTraitTypes);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<PcmNomTraitTypeEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }

    /// <summary>   Values that represent PCM Nominal Trait Types. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public enum PcmNomTraitType
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary>   An enum constant representing the line length option. </summary>
        [System.ComponentModel.Description( "Line Length" )]
        LineLength,

        /// <summary>   An enum constant representing the line width option. </summary>
        [System.ComponentModel.Description( "Line Width" )]
        LineWidth,

        /// <summary>   An enum constant representing the Split Line Length option. </summary>
        [System.ComponentModel.Description( "Split Line Length" )]
        SplitLineLength,

        /// <summary>   An enum constant representing the Split Line Width option. </summary>
        [System.ComponentModel.Description( "Split Line Width" )]
        SplitLineWidth
    }
}

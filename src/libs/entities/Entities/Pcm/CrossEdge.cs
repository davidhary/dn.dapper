using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the CrossEdge nub and entity. Includes the fields as kept in the data table.
    /// Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ICrossEdge
    {

        /// <summary>   Gets or sets the id of the cross edge. </summary>
        /// <value> Identifies the cross edge. </value>
        [ExplicitKey]
        int Id { get; set; }

        /// <summary>   Gets or sets the title. </summary>
        /// <value> The title. </value>
        string Title { get; set; }

        /// <summary>   Gets or sets a list of source channels. </summary>
        /// <value> A List of source channels. </value>
        string SourceChannelList { get; set; }

        /// <summary>   Gets or sets a list of sense channels. </summary>
        /// <value> A List of sense channels. </value>
        string SenseChannelList { get; set; }

        /// <summary>   Gets or sets the id of the source <see cref="PolarityEntity"/>. </summary>
        /// <value> The source polarity identifier. </value>
        int SourcePolarityId { get; set; }

        /// <summary>
        /// Gets or sets the id of the source leads <see cref="PolarityEntity"/>.
        /// </summary>
        /// <value> The source leads polarity identifier. </value>
        int SourceLeadsPolarityId { get; set; }

        /// <summary>
        /// Gets or sets the id of the sense leads <see cref="PolarityEntity"/>.
        /// </summary>
        /// <value> The sense leads polarity identifier. </value>
        int SenseLeadsPolarityId { get; set; }
    }

    /// <summary>   The cross edge builder. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public sealed class CrossEdgeBuilder
    {

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( CrossEdgeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets the name of the unique label index. </summary>
        /// <value> The name of the unique label index. </value>
        private static string TitleIndexName => $"UQ_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}_{nameof( CrossEdgeNub.Title )}";

        /// <summary>   The using unique title. </summary>
        private static bool? _UsingUniqueTitle;

        /// <summary>   Indicates if the entity uses a unique Title. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool UsingUniqueTitle( System.Data.IDbConnection connection )
        {
            if ( !_UsingUniqueTitle.HasValue )
            {
                _UsingUniqueTitle = connection.IndexExists( TitleIndexName );
            }

            return _UsingUniqueTitle.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] (
            [{nameof( CrossEdgeNub.Id )}] integer NOT NULL PRIMARY KEY, 
            [{nameof( CrossEdgeNub.Title )}] nvarchar(50) NOT NULL, 
            [{nameof( CrossEdgeNub.SourceChannelList )}] nvarchar(50) NOT NULL, 
            [{nameof( CrossEdgeNub.SenseChannelList )}] nvarchar(50) NOT NULL, 
            [{nameof( CrossEdgeNub.SourcePolarityId )}] integer NOT NULL, 
            [{nameof( CrossEdgeNub.SourceLeadsPolarityId )}] integer NOT NULL, 
            [{nameof( CrossEdgeNub.SenseLeadsPolarityId )}] integer NOT NULL,
            FOREIGN KEY ([{nameof( CrossEdgeNub.SourceLeadsPolarityId )}]) REFERENCES [{isr.Dapper.Entities.PolarityBuilder.TableName}] ([{nameof( PolarityNub.Id )}])
                    ON UPDATE NO ACTION ON DELETE NO ACTION,
            FOREIGN KEY ([{nameof( CrossEdgeNub.SourcePolarityId )}]) REFERENCES [{isr.Dapper.Entities.PolarityBuilder.TableName}] ([{nameof( PolarityNub.Id )}])
                    ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( CrossEdgeNub.SenseLeadsPolarityId )}]) REFERENCES [{isr.Dapper.Entities.PolarityBuilder.TableName}] ([{nameof( PolarityNub.Id )}])
                    ON UPDATE NO ACTION ON DELETE NO ACTION); 

            CREATE UNIQUE INDEX IF NOT EXISTS [{isr.Dapper.Entities.CrossEdgeBuilder.TitleIndexName}] ON [{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] ([{nameof( CrossEdgeNub.Title )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Entities.CrossEdgeBuilder.TableName}') AND type in (N'U'))
            BEGIN
                CREATE TABLE [dbo].[{isr.Dapper.Entities.CrossEdgeBuilder.TableName}](
                    [{nameof( CrossEdgeNub.Id )}] [int] NOT NULL,
                    [{nameof( CrossEdgeNub.Title )}] [nvarchar](50) NOT NULL,
                    [{nameof( CrossEdgeNub.SourceChannelList )}] [nvarchar](50) NOT NULL,
                    [{nameof( CrossEdgeNub.SenseChannelList )}] [nvarchar](50) NOT NULL,
                    [{nameof( CrossEdgeNub.SourcePolarityId )}] [int] NOT NULL,
                    [{nameof( CrossEdgeNub.SourceLeadsPolarityId )}] [int] NOT NULL,
                    [{nameof( CrossEdgeNub.SenseLeadsPolarityId )}] [int] NOT NULL,
                CONSTRAINT [PK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] PRIMARY KEY CLUSTERED 
                    ([{nameof( CrossEdgeNub.Id )}] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
                    ON [PRIMARY]
            END; 

            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.CrossEdgeBuilder.TableName}]') AND name = N'{isr.Dapper.Entities.CrossEdgeBuilder.TitleIndexName}')
            CREATE UNIQUE NONCLUSTERED INDEX [{isr.Dapper.Entities.CrossEdgeBuilder.TitleIndexName}] ON [dbo].[{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] ([{nameof( CrossEdgeNub.Title )}] ASC) 
            WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}_{nameof( CrossEdgeNub.SenseLeadsPolarityId )}]') AND parent_object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Entities.CrossEdgeBuilder.TableName}'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}_{nameof( CrossEdgeNub.SenseLeadsPolarityId )}] FOREIGN KEY([{nameof( CrossEdgeNub.SenseLeadsPolarityId )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.PolarityBuilder.TableName}] ([{nameof( PolarityNub.Id )}]); 

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}_{nameof( CrossEdgeNub.SenseLeadsPolarityId )}]') AND parent_object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Entities.CrossEdgeBuilder.TableName}'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}_{nameof( CrossEdgeNub.SenseLeadsPolarityId )}]; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}_{nameof( CrossEdgeNub.SourceLeadsPolarityId )}]') AND parent_object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Entities.CrossEdgeBuilder.TableName}'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.CrossEdgeBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}_{nameof( CrossEdgeNub.SourceLeadsPolarityId )}] FOREIGN KEY([{nameof( CrossEdgeNub.SourceLeadsPolarityId )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.PolarityBuilder.TableName}] ([{nameof( PolarityNub.Id )}]); 

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}_{nameof( CrossEdgeNub.SourceLeadsPolarityId )}]') AND parent_object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Entities.CrossEdgeBuilder.TableName}'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}_{nameof( CrossEdgeNub.SourceLeadsPolarityId )}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}{nameof( CrossEdgeNub.SourcePolarityId )}]') AND parent_object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Entities.CrossEdgeBuilder.TableName}'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.CrossEdgeBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}{nameof( CrossEdgeNub.SourcePolarityId )}] FOREIGN KEY([{nameof( CrossEdgeNub.SourcePolarityId )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.PolarityBuilder.TableName}] ([{nameof( PolarityNub.Id )}])
            ON UPDATE CASCADE ON DELETE CASCADE; 

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}{nameof( CrossEdgeNub.SourcePolarityId )}]') AND parent_object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Entities.CrossEdgeBuilder.TableName}'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.CrossEdgeBuilder.TableName}{nameof( CrossEdgeNub.SourcePolarityId )}]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>   Implements the CrossEdge table <see cref="ICrossEdge">interface</see>. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "CrossEdge" )]
    public class CrossEdgeNub : EntityNubBase<ICrossEdge>, ICrossEdge
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public CrossEdgeNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ICrossEdge CreateNew()
        {
            return new CrossEdgeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ICrossEdge CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( ICrossEdge value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( ICrossEdge source, ICrossEdge destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Id = source.Id;
            destination.SenseChannelList = source.SenseChannelList;
            destination.SenseLeadsPolarityId = source.SenseLeadsPolarityId;
            destination.SourceChannelList = source.SourceChannelList;
            destination.SourceLeadsPolarityId = source.SourceLeadsPolarityId;
            destination.SourcePolarityId = source.SourcePolarityId;
            destination.Title = source.Title;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as ICrossEdge );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( ICrossEdge other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( ICrossEdge left, ICrossEdge right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.Id, right.Id );
                result = result && string.Equals( left.SenseChannelList, right.SenseChannelList );
                result = result && Equals( left.SenseLeadsPolarityId, right.SenseLeadsPolarityId );
                result = result && string.Equals( left.SourceChannelList, right.SourceChannelList );
                result = result && Equals( left.SourceLeadsPolarityId, right.SourceLeadsPolarityId );
                result = result && Equals( left.SourcePolarityId, right.SourcePolarityId );
                result = result && string.Equals( left.Title, right.Title );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.Id, this.SenseChannelList, this.SenseLeadsPolarityId, this.SourceChannelList, this.SourceLeadsPolarityId, this,SourcePolarityId, this.Title ).GetHashCode();
        }


        #endregion

        /// <summary>   Gets or sets the id of the cross edge. </summary>
        /// <value> Identifies the cross edge. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary>   Gets or sets the title. </summary>
        /// <value> The title. </value>
        public string Title { get; set; }

        /// <summary>   Gets or sets a list of source channels. </summary>
        /// <value> A List of source channels. </value>
        public string SourceChannelList { get; set; }

        /// <summary>   Gets or sets a list of sense channels. </summary>
        /// <value> A List of sense channels. </value>
        public string SenseChannelList { get; set; }

        /// <summary>   Gets or sets the id of the source <see cref="PolarityEntity"/>. </summary>
        /// <value> The source polarity identifier. </value>
        public int SourcePolarityId { get; set; }

        /// <summary>
        /// Gets or sets the id of the source leads <see cref="PolarityEntity"/>.
        /// </summary>
        /// <value> The source leads polarity identifier. </value>
        public int SourceLeadsPolarityId { get; set; }

        /// <summary>
        /// Gets or sets the id of the sense leads <see cref="PolarityEntity"/>.
        /// </summary>
        /// <value> The sense leads polarity identifier. </value>
        public int SenseLeadsPolarityId { get; set; }
    }

    /// <summary>   The CrossEdge Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class CrossEdgeEntity : EntityBase<ICrossEdge, CrossEdgeNub>, ICrossEdge
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public CrossEdgeEntity() : this( new CrossEdgeNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CrossEdgeEntity( ICrossEdge value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public CrossEdgeEntity( ICrossEdge cache, ICrossEdge store ) : base( new CrossEdgeNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CrossEdgeEntity( CrossEdgeEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.CrossEdgeBuilder.TableName, nameof( ICrossEdge ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ICrossEdge CreateNew()
        {
            return new CrossEdgeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ICrossEdge CreateCopy()
        {
            var destination = this.CreateNew();
            CrossEdgeNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( ICrossEdge value )
        {
            CrossEdgeNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the Meter Model interface. </param>
        public override void UpdateCache( ICrossEdge value )
        {
            // first make the copy to notify of any property change.
            CrossEdgeNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The CrossEdge table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<ICrossEdge>( key ) : connection.Get<CrossEdgeNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Title );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="title">        The CrossEdge title. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string title )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, title ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ICrossEdge entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.Id ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool StoreEntity( System.Data.IDbConnection connection, ICrossEdge entity )
        {
            return new CrossEdgeEntity().Upsert( connection, entity );
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete<ICrossEdge>( new CrossEdgeNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the CrossEdge entities. </summary>
        /// <value> The CrossEdge entities. </value>
        public static IEnumerable<CrossEdgeEntity> CrossEdges { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CrossEdgeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ICrossEdge>() ) : Populate( connection.GetAll<CrossEdgeNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            CrossEdges = FetchAllEntities( connection, true );
            this.NotifyPropertyChanged( nameof( CrossEdges ) );
            return CrossEdges?.Any() == true ? CrossEdges.Count() : 0;
        }

        /// <summary>   Dictionary of cross edge titles. </summary>
        private static IDictionary<string, CrossEdgeEntity> _CrossEdgeTitleDictionary;

        /// <summary>   Cross edge title dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of String, CrossEdgeEntity) </returns>
        public static IDictionary<string, CrossEdgeEntity> CrossEdgeTitleDictionary()
        {
            if ( !(_CrossEdgeTitleDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _CrossEdgeTitleDictionary = new Dictionary<string, CrossEdgeEntity>();
                foreach ( CrossEdgeEntity entity in CrossEdges )
                    _CrossEdgeTitleDictionary.Add( entity.Title, entity );
            }

            return _CrossEdgeTitleDictionary;
        }

        /// <summary>   Populates a list of entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the Meter Model nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CrossEdgeEntity> Populate( IEnumerable<CrossEdgeNub> nubs )
        {
            var l = new List<CrossEdgeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( CrossEdgeNub nub in nubs )
                    l.Add( new CrossEdgeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the Meter Model interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CrossEdgeEntity> Populate( IEnumerable<ICrossEdge> interfaces )
        {
            var l = new List<CrossEdgeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new CrossEdgeNub();
                foreach ( ICrossEdge iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new CrossEdgeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="title">        The CrossEdge title. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string title )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] WHERE {nameof( CrossEdgeNub.Title )} = @title", new { title } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single nub or none. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="title">        The CrossEdge title. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CrossEdgeNub> FetchNubs( System.Data.IDbConnection connection, string title )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.CrossEdgeBuilder.TableName}] WHERE {nameof( CrossEdgeNub.Title )} = @title", new { title } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<CrossEdgeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the CrossEdge exists. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="title">        The CrossEdge title. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string title )
        {
            return 1 == CountEntities( connection, title );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the cross edge. </summary>
        /// <value> Identifies the cross edge. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the title. </summary>
        /// <value> The title. </value>
        public string Title
        {
            get => this.ICache.Title;

            set {
                if ( !string.Equals( this.Title, value ) )
                {
                    this.ICache.Title = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets a list of source channels. </summary>
        /// <value> A List of source channels. </value>
        public string SourceChannelList
        {
            get => this.ICache.SourceChannelList;

            set {
                if ( !string.Equals( this.SourceChannelList, value ) )
                {
                    this.ICache.SourceChannelList = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets a list of sense channels. </summary>
        /// <value> A List of sense channels. </value>
        public string SenseChannelList
        {
            get => this.ICache.SenseChannelList;

            set {
                if ( !string.Equals( this.SenseChannelList, value ) )
                {
                    this.ICache.SenseChannelList = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the source <see cref="PolarityEntity"/>. </summary>
        /// <value> The source polarity identifier. </value>
        public int SourcePolarityId
        {
            get => this.ICache.SourcePolarityId;

            set {
                if ( !object.Equals( ( object ) this.SourcePolarityId, ( object ) value ) )
                {
                    this.ICache.SourcePolarityId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( CrossEdgeEntity.SourcePolarity ) );
                }
            }
        }

        /// <summary>   Gets or sets the Source Polarity. </summary>
        /// <value> The Source Polarity. </value>
        public Polarity SourcePolarity
        {
            get => ( Polarity ) this.SourcePolarityId;

            set => this.SourcePolarityId = ( int ) value;
        }

        /// <summary>
        /// Gets or sets the id of the source leads <see cref="PolarityEntity"/>.
        /// </summary>
        /// <value> The source leads polarity identifier. </value>
        public int SourceLeadsPolarityId
        {
            get => this.ICache.SourceLeadsPolarityId;

            set {
                if ( !object.Equals( ( object ) this.SourceLeadsPolarityId, ( object ) value ) )
                {
                    this.ICache.SourceLeadsPolarityId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( CrossEdgeEntity.SourceLeadsPolarity ) );
                }
            }
        }

        /// <summary>   Gets or sets the Source Leads Polarity. </summary>
        /// <value> The Source Leads Polarity. </value>
        public Polarity SourceLeadsPolarity
        {
            get => ( Polarity ) this.SourceLeadsPolarityId;

            set => this.SourceLeadsPolarityId = ( int ) value;
        }

        /// <summary>
        /// Gets or sets the id of the sense leads <see cref="PolarityEntity"/>.
        /// </summary>
        /// <value> The sense leads polarity identifier. </value>
        public int SenseLeadsPolarityId
        {
            get => this.ICache.SenseLeadsPolarityId;

            set {
                if ( !object.Equals( ( object ) this.SenseLeadsPolarityId, ( object ) value ) )
                {
                    this.ICache.SenseLeadsPolarityId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( CrossEdgeEntity.SenseLeadsPolarity ) );
                }
            }
        }

        /// <summary>   Gets or sets the Sense Leads Polarity. </summary>
        /// <value> The Sense Leads Polarity. </value>
        public Polarity SenseLeadsPolarity
        {
            get => ( Polarity ) this.SenseLeadsPolarityId;

            set => this.SenseLeadsPolarityId = ( int ) value;
        }

        /// <summary>   Converts a value to a polarity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The CrossEdge interface. </param>
        /// <returns>   Value as a Polarity. </returns>
        public static Polarity ToPolarity( int value )
        {
            return ( Polarity ) value;
        }

        #endregion

    }
}

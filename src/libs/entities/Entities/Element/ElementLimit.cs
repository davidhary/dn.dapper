using System;
using System.Collections.Generic;

namespace isr.Dapper.Entities
{

    /// <summary>   An element limit. </summary>
    /// <remarks>   David, 2021-04-27. </remarks>
    public class ElementLimit
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="limitSource">  The limit source. </param>
        /// <param name="lower">        The lower. </param>
        /// <param name="upper">        The upper. </param>
        public ElementLimit( LimitSource limitSource, double lower, double upper )
        {
            this.Source = limitSource;
            this.Lower = lower;
            this.Upper = upper;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="limit">    The limit. </param>
        public ElementLimit( ElementLimit limit ) : this( limit.Source, limit.Lower, limit.Upper )
        {
        }

        /// <summary>   Query if 'source' is non editable. </summary>
        /// <remarks>   David, 2021-04-29. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <returns>   True if non editable, false if not. </returns>
        public static bool IsNonEditable( LimitSource source )
        {
            return source == LimitSource.Lot || source == LimitSource.Historical;
        }

        /// <summary>   Query if 'source' is non editable. </summary>
        /// <remarks>   David, 2021-04-29. </remarks>
        /// <returns>   True if non editable, false if not. </returns>
        public bool IsNonEditable()
        {
            return ElementLimit.IsNonEditable( this.Source );
        }

        /// <summary>   Gets the empty. </summary>
        /// <value> The empty. </value>
        public static ElementLimit Empty => new( LimitSource.None, 0, 0 );

        /// <summary>   Gets the limit source. </summary>
        /// <value> The limit source. </value>
        public LimitSource Source { get; }

        /// <summary>   Gets the lower. </summary>
        /// <value> The lower. </value>
        public double Lower { get; set; }

        /// <summary>   Gets the upper. </summary>
        /// <value> The upper. </value>
        public double Upper { get; set; }

        /// <summary>   Gets a value indicating whether this object has value. </summary>
        /// <value> True if this object has value, false if not. </value>
        public bool HasValue => this.Lower != this.Upper;

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="left">     Element limit to be compared. </param>
        /// <param name="right">    Element limit to be compared. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="left" /> and <paramref name="right" />  are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public static bool Equals( ElementLimit left, ElementLimit right )
        {
            return (left.Source == right.Source)
                    && (left.Lower == right.Lower)
                    && (left.Upper == right.Upper);
        }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return (obj is ElementLimit limit) && ElementLimit.Equals( this, limit );

        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.Source, this.Lower, this.Upper ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( ElementLimit left, ElementLimit right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( ElementLimit left, ElementLimit right )
        {
            return !(left == right);
        }
    }

    /// <summary>   List of element limit bindings. </summary>
    /// <remarks>   David, 2021-04-27. </remarks>
    public class ElementLimitBindingList : isr.Std.BindingLists.InvokingBindingList<ElementLimit>
    {

        /// <summary>   Simulates this object. </summary>
        /// <remarks>   David, 2021-05-04. </remarks>
        public void Simulate()
        {
            bool raiseListChangedEnabled = this.RaiseListChangedEvents;
            try
            {
                this.RaiseListChangedEvents = false;
                this.SimulateInternal();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raiseListChangedEnabled;
                if ( this.RaiseListChangedEvents ) this.ResetBindings();
            }
        }

        /// <summary>   Simulate internal. </summary>
        /// <remarks>   David, 2021-05-17. </remarks>
        private void SimulateInternal()
        {
            Random sampleRng = new( DateTime.Now.Second );
            double mean = 125 + 10 * sampleRng.NextDouble();
            double sigma = 3 + 3 * (sampleRng.NextDouble() - 0.5);
            this.AddLimit( LimitSource.Lot, mean - 3 * sigma, mean + 3 * sigma );

            double sum = 0;
            double sumSquares = 0;
            int ensableCount = sampleRng.Next( 10, 20 );
            this.EnsembleCount = ensableCount;
            for ( int i = 0; i < ensableCount; i++ )
            {
                mean = 125 + 10 * sampleRng.NextDouble();
                sum += mean;
                sigma = 3 + 3 * (sampleRng.NextDouble() - 0.5);
                sumSquares += sigma * sigma;
            }
            mean = sum / ensableCount;
            sigma = Math.Sqrt( sumSquares / ensableCount );
            this.AddLimit( LimitSource.Historical, mean - 3 * sigma, mean + 3 * sigma );
            this.AddEngineeringLimit( 115, 150 );
        }

        /// <summary>   Adds the limits internal. </summary>
        /// <remarks>   David, 2021-05-17. </remarks>
        /// <param name="statistics">           The statistics. </param>
        /// <param name="engineeringLowLimit">  The engineering low limit. </param>
        /// <param name="engineeringHighLimit"> The engineering high limit. </param>
        private void AddLimitsInternal( IEnumerable<PerformanceStatistic> statistics, double engineeringLowLimit, double engineeringHighLimit )
        {
            foreach ( PerformanceStatistic nrs in statistics )
            {
                if ( nrs.SampleSource == SampleSource.Historical )
                {
                    this.EnsembleCount = nrs.EnsembleCount;
                    this.AddLimit( LimitSource.Historical, nrs.Mean - 3 * nrs.Sigma, nrs.Mean + 3 * nrs.Sigma );
                }
                else if ( nrs.SampleSource == SampleSource.Session )
                {
                    this.AddLimit( LimitSource.Lot, nrs.Mean - 3 * nrs.Sigma, nrs.Mean + 3 * nrs.Sigma );
                }
            }
            this.AddEngineeringLimit( engineeringLowLimit, engineeringHighLimit );
        }

        /// <summary>   Adds the limits. </summary>
        /// <remarks>   David, 2021-05-04. </remarks>
        /// <param name="statistics">           The statistics. </param>
        /// <param name="engineeringLowLimit">  The engineering low limit. </param>
        /// <param name="engineeringHighLimit"> The engineering high limit. </param>
        public void AddLimits( IEnumerable<PerformanceStatistic> statistics, double engineeringLowLimit, double engineeringHighLimit )
        {
            bool raiseListChangedEnabled = this.RaiseListChangedEvents;
            try
            {

                this.RaiseListChangedEvents = false;
                this.AddLimitsInternal( statistics, engineeringLowLimit, engineeringHighLimit );
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raiseListChangedEnabled;
                if ( this.RaiseListChangedEvents ) this.ResetBindings();
            }
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="minimumEnsembleSize">  The minimum size of the ensemble. </param>
        public ElementLimitBindingList( int minimumEnsembleSize )
        {
            this.MinimumEnsembleSize = minimumEnsembleSize;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-04-29. </remarks>
        /// <param name="values">   The values. </param>
        public ElementLimitBindingList( ElementLimitBindingList values ) : this( values.MinimumEnsembleSize )
        {
            bool raisingListChangedEvents = this.RaiseListChangedEvents;
            try
            {
                this.RaiseListChangedEvents = false;
                foreach ( var item in values )
                {
                    this.Add( item );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raisingListChangedEvents;
                if ( this.RaiseListChangedEvents )
                    this.ResetBindings();
            }
        }

        private ElementLimitBindingList _Cache;
        /// <summary>   Stores the current value into its cache. </summary>
        /// <remarks>   David, 2021-04-29. </remarks>
        public void Store()
        {
            this._Cache = new( this );
        }

        /// <summary>   Restore non editable values. </summary>
        /// <remarks>   David, 2021-05-17. </remarks>
        public void RestoreNonEditableValues()
        {
            bool raiseListChangedEnabled = this.RaiseListChangedEvents;
            try
            {
                this.RaiseListChangedEvents = false;
                // replace the existing elements with non-editable elements from the cache
                foreach ( var item in this._Cache )
                {
                    if ( item.IsNonEditable() )
                    {
                        this.AddLimit( item );
                    }
                }

            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raiseListChangedEnabled;
                if ( this.RaiseListChangedEvents ) this.ResetBindings();
            }
        }

        /// <summary>   Gets or sets the minimum size of the ensemble. </summary>
        /// <value> The minimum size of the ensemble. </value>
        public int MinimumEnsembleSize { get; set; }

        /// <summary>   Gets or sets the number of ensembles. </summary>
        /// <value> The number of ensembles. </value>
        public int EnsembleCount { get; set; }

        /// <summary>   Adds a limit. </summary>
        /// <remarks>   David, 2021-05-04. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <param name="lower">    The lower. </param>
        /// <param name="upper">    The upper. </param>
        public void AddLimit( LimitSource source, double lower, double upper )
        {
            this.AddLimit( new ElementLimit( source, lower, upper ) );
        }

        /// <summary>   Adds a limit. </summary>
        /// <remarks>   David, 2021-05-18. </remarks>
        /// <param name="elementLimit"> The element limit. </param>
        public void AddLimit( ElementLimit elementLimit )
        {
            bool raisingListChangedEvents = this.RaiseListChangedEvents;
            try
            {
                this.RaiseListChangedEvents = false;
                this.Remove( elementLimit.Source );
                this.Add( elementLimit );
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raisingListChangedEvents;
                if ( this.RaiseListChangedEvents )
                    this.ResetBindings();
            }
        }

        /// <summary>   Removes the given limitSource. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="limitSource">  The limit source to remove. </param>
        public void Remove( LimitSource limitSource )
        {
            foreach ( ElementLimit l in this )
            {
                if ( l.Source == limitSource )
                {
                    _ = this.Remove( l );
                    break;
                }
            }
        }


        /// <summary>   Adds an limit based on statistical perfromance data. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="stats">    The statistics. </param>
        public void AddPerformanceLimit( PerformanceStatistic stats )
        {
            LimitSource source = (stats.EnsembleCount > 1) ? LimitSource.Historical : LimitSource.Lot;
            ElementLimit limit = new( source, stats.Mean - 3 * stats.Sigma, stats.Mean + 3 * stats.Sigma );
            this.EnsembleCount = stats.EnsembleCount > 1 ? stats.EnsembleCount : this.EnsembleCount;
            this.AddLimit( limit );
        }

        /// <summary>   Adds an limits based on statistical performance data. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="stats">    The statistics. </param>
        public void AddPerformanceLimits( IEnumerable<PerformanceStatistic> stats )
        {
            bool raisingListChangedEvents = this.RaiseListChangedEvents;
            try
            {
                this.RaiseListChangedEvents = false;
                foreach ( PerformanceStatistic s in stats )
                {
                    this.AddPerformanceLimit( s );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raisingListChangedEvents;
                if ( this.RaiseListChangedEvents )
                    this.ResetBindings();
            }
        }

        /// <summary>   Adds an applied limit. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="limit">    The limit. </param>
        public void AddAppliedLimit( ElementLimit limit )
        {
            this.AddLimit( new ElementLimit( LimitSource.Applied, limit.Lower, limit.Upper ) );
        }

        /// <summary>   Gets a limit. </summary>
        /// <remarks>   David, 2021-05-10. </remarks>
        /// <param name="limitSource">  The limit source to remove. </param>
        /// <returns>   The limit. </returns>
        public ElementLimit GetLimit( LimitSource limitSource )
        {
            ElementLimit result = ElementLimit.Empty;
            foreach ( ElementLimit l in this )
            {
                if ( l.Source == limitSource )
                {
                    result = l;
                    break;
                }
            }
            return result;
        }

        /// <summary>   Adds an engineering limit to 'upper'. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="lower">    The lower. </param>
        /// <param name="upper">    The upper. </param>
        public void AddEngineeringLimit( double lower, double upper )
        {
            bool raisingListChangedEvents = this.RaiseListChangedEvents;
            try
            {
                this.RaiseListChangedEvents = false;
                this.AddLimit( new ElementLimit( LimitSource.Engineering, lower, upper ) );

                // update the default value in case this is a change that is applied by engineering.
                _ = this.AddDefaultElementLimit();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raisingListChangedEvents;
                if ( this.RaiseListChangedEvents )
                    this.ResetBindings();
            }
        }

        /// <summary>   Adds default element limit. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        public ElementLimit AddDefaultElementLimit()
        {
            double lowerLimit = double.MinValue;
            double upperLimit = double.MaxValue;

            foreach ( ElementLimit l in this )
            {
                if ( l.HasValue )
                {
                    if ( l.Source == LimitSource.Engineering )
                    {
                        lowerLimit = Math.Max( lowerLimit, l.Lower );
                        upperLimit = Math.Min( upperLimit, l.Upper );
                    }
                    else if ( l.Source == LimitSource.Lot && this.EnsembleCount > 0 )
                    {
                        lowerLimit = Math.Max( lowerLimit, l.Lower );
                        upperLimit = Math.Min( upperLimit, l.Upper );
                    }
                    else if ( l.Source == LimitSource.Historical && this.EnsembleCount >= this.MinimumEnsembleSize )
                    {
                        lowerLimit = Math.Max( lowerLimit, l.Lower );
                        upperLimit = Math.Min( upperLimit, l.Upper );
                    }
                }
            }
            ElementLimit defaultLimit = new( LimitSource.Default, lowerLimit, upperLimit );
            this.AddLimit( defaultLimit );
            return defaultLimit;
        }

        /// <summary>   Updates the applied limit. </summary>
        /// <remarks>   David, 2021-05-18. </remarks>
        public void UpdateAppliedLimit()
        {
            bool raisingListChangedEvents = this.RaiseListChangedEvents;
            try
            {
                this.RaiseListChangedEvents = false;

                // restore the non-editable values from the cache
                this.RestoreNonEditableValues();

                // recalculate the default value.
                ElementLimit defaultLimit = this.AddDefaultElementLimit();

                // make the default value the applied value
                this.AddAppliedLimit( defaultLimit );

            }
            catch
            {
                throw;
            }
            finally
            {
                this.RaiseListChangedEvents = raisingListChangedEvents;
                if ( this.RaiseListChangedEvents )
                    this.ResetBindings();
            }
        }


    }



    /// <summary>   Values that represent element limit types. </summary>
    /// <remarks>   David, 2021-04-27. </remarks>
    public enum LimitSource
    {

        /// <summary>   An enum constant representing the none option. </summary>
        None,

        /// <summary>   An enum constant representing the lot (last sample session) option. </summary>
        Lot,

        /// <summary>   An enum constant representing the historical option. </summary>
        Historical,

        /// <summary>   An enum constant representing the engineering option. </summary>
        Engineering,

        /// <summary>   An enum constant representing the default option. </summary>
        Default,

        /// <summary>   An enum constant representing the applied option. </summary>
        Applied
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   An element entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class ElementEntity
    {

        /// <summary>
        /// Gets or sets the <see cref=" ElementUniqueNomTextEntityCollection">Element Nom Text
        /// values</see>.
        /// </summary>
        /// <value> The Element Text trait values. </value>
        public ElementTextCollection NomTexts { get; private set; }

        /// <summary>   Initializes the nominal texts. </summary>
        /// <remarks>   David, 2020-07-06. </remarks>
        public void InitializeNomTexts()
        {
            this.NomTexts = new ElementTextCollection();
            foreach ( NomTypeEntity nomTypeEntity in this.NomTypes )
                this.NomTexts.Add( new ElementUniqueNomTextEntityCollection( this, nomTypeEntity.Id ) );
        }

        /// <summary>   Fetches <see cref=" ElementTextCollection">Element Text values</see>. </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of specifications. </returns>
        public int FetchNomTexts( System.Data.IDbConnection connection )
        {
            if ( !(this.NomTexts?.Any()).GetValueOrDefault( false ) )
                this.InitializeNomTexts();
            this.NomTexts.Add( ElementNomTextEntity.FetchEntities( connection, this.AutoId ) );
            return this.NomTexts?.Any() == true ? this.NomTexts.Count : 0;
        }
    }

    /// <summary>   Collection of sample traits. </summary>
    /// <remarks>   David, 2020-07-03. </remarks>
    public class ElementTextCollection : KeyedCollection<int, ElementUniqueNomTextEntityCollection>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override int GetKeyForItem( ElementUniqueNomTextEntityCollection item )
        {
            return item.ElementText.Selector;
        }

        /// <summary>   Adds texts. </summary>
        /// <remarks>   David, 2020-07-06. </remarks>
        /// <param name="texts">    The texts to add. </param>
        public void Add( IEnumerable<ElementNomTextEntity> texts )
        {
            foreach ( ElementNomTextEntity text in texts )
                this[text.NomTypeId].Add( text );
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        [CLSCompliant( false )]
        public void Upsert( TransactedConnection connection )
        {
            foreach ( ElementUniqueNomTextEntityCollection item in this )
                _ = item.Upsert( connection );
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        public void Upsert( System.Data.IDbConnection connection )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.Upsert( new TransactedConnection( connection, transaction ) );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.Upsert( transactedConnection );
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Product-Element builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ProductElementBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ProductElementNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = ProductBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( ProductNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = ElementBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ElementNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( ProductElementEntity.ProductAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( ProductElementEntity.ElementAutoId );

        #region " SINGLETON "

        private static readonly Lazy<ProductElementBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ProductElementBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Product Element Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "ProductElement" )]
    public class ProductElementNub : OneToManyNub, IOneToMany
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public ProductElementNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new ProductElementNub();
        }
    }

    /// <summary>
    /// The Product-Element Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class ProductElementEntity : EntityBase<IOneToMany, ProductElementNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public ProductElementEntity() : this( new ProductElementNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductElementEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ProductElementEntity( IOneToMany cache, IOneToMany store ) : base( new ProductElementNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductElementEntity( ProductElementEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ProductElementBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new ProductElementNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Product-Element interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int productAutoId, int elementAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, productAutoId, elementAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.ProductAutoId, this.ElementAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.ProductAutoId, this.ElementAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the Product. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int productAutoId, int elementAutoId )
        {
            return this.FetchUsingKey( connection, productAutoId, elementAutoId );
        }

        /// <summary>
        /// Tries to fetch an existing <see cref="Dapper.Entities.ElementEntity"/> associated with this product;
        /// otherwise, inserts a new entity. Then tries to fetch an existing or insert a new
        /// <see cref="ProductElementEntity"/>.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.ProductEntity"/> exists for the specified
        /// <paramref name="productAutoId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementLabel">         The element label. </param>
        /// <param name="elementTypeId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementOrdinalNumber"> The element ordinal number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainElement( System.Data.IDbConnection connection, int productAutoId, string elementLabel, int elementTypeId, int elementOrdinalNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            this.ElementEntity = FetchElement( connection, productAutoId, elementLabel );
            if ( this.ElementEntity.IsClean() )
            {
                if ( this.ElementEntity.ElementTypeId != elementTypeId )
                {
                    result = (false, $"Attempt to change {nameof( Dapper.Entities.ElementEntity )} {nameof( this.ElementEntity.ElementLabel )} of type {nameof( this.ElementEntity.ElementTypeId )} to {elementTypeId} was aborted");
                }
            }
            else
            {
                this.ElementEntity = new ElementEntity() { ElementLabel = elementLabel, ElementTypeId = elementTypeId, OrdinalNumber = elementOrdinalNumber };
                if ( !this.ElementEntity.Insert( connection ) )
                {
                    result = (false, $"Failed inserting {nameof( Dapper.Entities.ElementEntity )} with {nameof( Dapper.Entities.ElementEntity.ElementLabel )} of {elementLabel} and {nameof( Dapper.Entities.ElementEntity.ElementTypeId )} of {elementTypeId}");
                }
            }

            if ( result.Success )
            {
                this.PrimaryId = productAutoId;
                this.SecondaryId = this.ElementEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( ProductElementEntity.ElementEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( ProductElementEntity )} with {nameof( this.ProductAutoId )} of {productAutoId} and {nameof( this.ElementAutoId )} of {this.ElementEntity.AutoId}");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch an existing <see cref="Dapper.Entities.ProductEntity"/>, fetch or insert a new
        /// <see cref="Dapper.Entities.ElementEntity"/> and fetches an existing or inserts a new
        /// <see cref="ProductElementEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementLabel">         The element label. </param>
        /// <param name="elementTypeId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementOrdinalNumber"> The element ordinal number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int productAutoId, string elementLabel, int elementTypeId, int elementOrdinalNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.ProductEntity is null || this.ProductEntity.AutoId != productAutoId )
            {
                // make sure we have a product associated with the product auto ID.
                this.ProductEntity = new ProductEntity() { AutoId = productAutoId };
                if ( !this.ProductEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.ProductEntity )} with {nameof( Dapper.Entities.ProductEntity.AutoId )} of {productAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainElement( connection, productAutoId, elementLabel, elementTypeId, elementOrdinalNumber );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( ProductElementEntity.ProductEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch an existing <see cref="Dapper.Entities.ProductEntity"/>, fetch or insert a new
        /// <see cref="Dapper.Entities.ElementEntity"/> and fetches an existing or inserts a new
        /// <see cref="ProductElementEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="elementLabel">         The element label. </param>
        /// <param name="elementTypeId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementOrdinalNumber"> The element ordinal number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, string elementLabel, int elementTypeId, int elementOrdinalNumber )
        {
            return this.TryObtain( connection, this.ProductAutoId, elementLabel, elementTypeId, elementOrdinalNumber );
        }

        /// <summary>
        /// Fetches an existing <see cref="Dapper.Entities.ProductEntity"/>, fetch or insert a new
        /// <see cref="Dapper.Entities.ElementEntity"/> and fetches an existing or inserts a new
        /// <see cref="ProductElementEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementLabel">         The element label. </param>
        /// <param name="elementTypeId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementOrdinalNumber"> The element ordinal number. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool Obtain( System.Data.IDbConnection connection, int productAutoId, string elementLabel, int elementTypeId, int elementOrdinalNumber )
        {
            var (Success, Details) = this.TryObtain( connection, productAutoId, elementLabel, elementTypeId, elementOrdinalNumber );
            return !Success ? throw new InvalidOperationException( Details ) : Success;
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int productAutoId, int elementAutoId )
        {
            return connection.Delete( new ProductElementNub() { PrimaryId = productAutoId, SecondaryId = elementAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Product-Element entities. </summary>
        /// <value> The Product-Element entities. </value>
        public IEnumerable<ProductElementEntity> ProductElements { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductElementEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<ProductElementNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ProductElements = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ProductElementEntity.ProductElements ) );
            return this.ProductElements?.Any() == true ? this.ProductElements.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductElementEntity> Populate( IEnumerable<ProductElementNub> nubs )
        {
            var l = new List<ProductElementEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ProductElementNub nub in nubs )
                    l.Add( new ProductElementEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductElementEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<ProductElementEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ProductElementNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ProductElementEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Element entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductElementBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductElementNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductElementEntity> FetchEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductElementBuilder.TableName}] WHERE {nameof( ProductElementNub.PrimaryId )} = @Id", new { Id = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ProductElementNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Element. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   The total number of entities by Element. </returns>
        public static int CountEntitiesByElement( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductElementBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductElementNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Elements in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Elements in this
        /// collection.
        /// </returns>
        public static IEnumerable<ProductElementEntity> FetchEntitiesByElement( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductElementBuilder.TableName}] WHERE {nameof( ProductElementNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ProductElementNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productAutoId, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductElementBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductElementNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductElementNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductElementNub> FetchNubs( System.Data.IDbConnection connection, int productAutoId, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductElementBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductElementNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductElementNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductElementNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Product Element exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int productAutoId, int elementAutoId )
        {
            return 1 == CountEntities( connection, productAutoId, elementAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Product entity. </summary>
        /// <value> The Product entity. </value>
        public ProductEntity ProductEntity { get; private set; }

        /// <summary>   Fetches Product Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Product Entity. </returns>
        public ProductEntity FetchProductEntity( System.Data.IDbConnection connection )
        {
            var entity = new ProductEntity();
            _ = entity.FetchUsingKey( connection, this.ProductAutoId );
            this.ProductEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Products associated with the specified <paramref name="elementAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   The total number of Products. </returns>
        public static int CountProducts( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.ProductElementBuilder.TableName}] WHERE {nameof( ProductElementNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Products associated with the specified <paramref name="elementAutoId"/>; expected
        /// a single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Products in this collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchProducts( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductElementBuilder.TableName}] WHERE {nameof( ProductElementNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ElementEntity>();
            foreach ( ProductElementNub nub in connection.Query<ProductElementNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new ProductElementEntity( nub );
                l.Add( entity.FetchElementEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Products associated with the specified <paramref name="elementAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteProducts( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.ProductElementBuilder.TableName}] WHERE {nameof( ProductElementNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the Element entity. </summary>
        /// <value> The Element entity. </value>
        public ElementEntity ElementEntity { get; private set; }

        /// <summary>   Fetches a Element Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Element Entity. </returns>
        public ElementEntity FetchElementEntity( System.Data.IDbConnection connection )
        {
            var entity = new ElementEntity();
            _ = entity.FetchUsingKey( connection, this.ElementAutoId );
            this.ElementEntity = entity;
            return entity;
        }

        /// <summary>   Count elements. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The total number of elements. </returns>
        public static int CountElements( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.ProductElementBuilder.TableName}] WHERE {nameof( ProductElementNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Elements in this collection. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Elements in this collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchElements( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductElementBuilder.TableName}] WHERE {nameof( ProductElementNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ElementEntity>();
            foreach ( ProductElementNub nub in connection.Query<ProductElementNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new ProductElementEntity( nub );
                l.Add( entity.FetchElementEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Element related to the specified Product. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteElements( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.ProductElementBuilder.TableName}] WHERE {nameof( ProductElementNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the ordered elements in this collection. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered elements in this
        /// collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchOrderedElements( System.Data.IDbConnection connection, string selectQuery, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = productAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return ElementEntity.Populate( connection.Query<ElementNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered elements in this collection. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered elements in this
        /// collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchOrderedElements( System.Data.IDbConnection connection, int productAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [ProductElement] on [ProductElement].SecondaryId = [Element].AutoId where [ProductElement].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{ElementBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{ElementBuilder.TableName}] Inner Join [{ProductElementBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.ProductElementBuilder.TableName}].{nameof( ProductElementNub.SecondaryId )} = [{isr.Dapper.Entities.ElementBuilder.TableName}].{nameof( ElementNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.ProductElementBuilder.TableName}].{nameof( ProductElementNub.PrimaryId )} = @Id" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.ElementBuilder.TableName}].{nameof( ElementNub.Amount )} ASC; " );
            return FetchOrderedElements( connection, queryBuilder.ToString(), productAutoId );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementLabel">     The element label. </param>
        /// <returns>   The element. </returns>
        public static ElementEntity FetchElement( System.Data.IDbConnection connection, string selectQuery, int productAutoId, string elementLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = productAutoId, Label = elementLabel } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<ElementNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new ElementEntity() : new ElementEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <remarks>
        /// David, 2020-05-19.
        /// <code>
        /// SELECT [Element].*
        /// FROM [Element] Inner Join [ProductElement]
        /// ON [ProductElement].[SecondaryId] = [Element].[AutoId]
        /// WHERE ([ProductElement].[PrimaryId] = 8 AND [Element].[Label] = 'R1')
        /// </code>
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementLabel">     The element label. </param>
        /// <returns>   The element. </returns>
        public static ElementEntity FetchElement( System.Data.IDbConnection connection, int productAutoId, string elementLabel )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [ProductElement] on [ProductElement].SecondaryId = [Element].AutoId where [ProductElement].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{ElementBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{ElementBuilder.TableName}] Inner Join [{ProductElementBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.ProductElementBuilder.TableName}].[{nameof( ProductElementNub.SecondaryId )}] = [{isr.Dapper.Entities.ElementBuilder.TableName}].[{nameof( ElementNub.AutoId )}]" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.ProductElementBuilder.TableName}].[{nameof( ProductElementNub.PrimaryId )}] = @Id AND [{isr.Dapper.Entities.ElementBuilder.TableName}].[{nameof( ElementNub.Label )}] = @Label); " );
            return FetchElement( connection, queryBuilder.ToString(), productAutoId, elementLabel );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductElementEntity.ProductAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Product. </summary>
        /// <value> Identifies the Product. </value>
        public int ProductAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductElementEntity.ElementAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int ElementAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

        #region " SHARED "

        /// <summary>   Enumerates select element information in this collection. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        /// <param name="productNumber">    Name of the product. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process select element information in this
        /// collection.
        /// </returns>
        public static IEnumerable<ElementInfo> SelectElementInfo( string productNumber )
        {
            switch ( productNumber ?? "" )
            {
                case "D1206LF":
                    {
                        return new List<ElementInfo>() { new ElementInfo( "R1", 1, ( int ) ElementType.Resistor, ( int ) NomType.Resistance,true, true ),
                                                         new ElementInfo( "R2", 2, ( int ) ElementType.Resistor, ( int ) NomType.Resistance, false, false ),
                                                         new ElementInfo( "D1", 3, ( int ) ElementType.CompoundResistor, ( int ) NomType.Resistance, false, false ),
                                                         new ElementInfo( "M1", 4, ( int ) ElementType.Equation, ( int ) NomType.Equation, false, false ) };
                    }

                default:
                    {
                        return new List<ElementInfo>() { new ElementInfo( "R1", 1, ( int ) ElementType.Resistor, ( int ) NomType.Resistance, true, true ) };
                    }
            }
        }

        #endregion

        #region " ELEMENTS "

        /// <summary>
        /// Tries to fetch an existing <see cref="Dapper.Entities.ElementEntity"/> associated with this product;
        /// otherwise, inserts a new entity. Then tries to fetch an existing or insert a new
        /// <see cref="ProductElementEntity"/>.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.ProductEntity"/> exists for the specified.
        /// </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="elementInfo">  Information describing the element. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainElement( System.Data.IDbConnection connection, ElementInfo elementInfo )
        {
            var result = this.TryObtain( connection, this.ProductAutoId, elementInfo.Label, elementInfo.ElementTypeId, elementInfo.OrdinalNumber );
            if ( !result.Success )
                return result;
            _ = this.ElementEntity.FetchElementNomTypeEntities( connection );
            this.ElementEntity.NomTypeEntities.Add( connection, this.ElementEntity.AutoId, elementInfo.NomTypeId );
            int expectedCount = 1;
            if ( this.ElementEntity.NomTypeEntities.Any() )
            {
                if ( this.ElementEntity.NomTypeEntities.Count == expectedCount )
                {
                    if ( !this.ElementEntity.NomTypeEntities.ContainsNomType( elementInfo.NomTypeId ) )
                    {
                        result = (false, $"{nameof( this.ElementEntity.NomTypeEntities )} should contain key {elementInfo.NomTypeId}");
                    }
                }
                else
                {
                    result = (false, $"{nameof( this.ElementEntity.NomTypeEntities )} count {this.ElementEntity.NomTypeEntities.Count} should be {expectedCount}");
                }
            }
            else
            {
                result = (false, $"{nameof( this.ElementEntity.NomTypeEntities )} is empty where it should have items");
            }

            if ( !result.Success )
                return result;
            _ = this.ElementEntity.FetchNomTypes( connection );
            expectedCount = 1;
            if ( this.ElementEntity.NomTypes.Any() )
            {
                if ( this.ElementEntity.NomTypes.Count == expectedCount )
                {
                    if ( !this.ElementEntity.NomTypes.Contains( elementInfo.NomTypeId ) )
                    {
                        result = (false, $"{nameof( this.ElementEntity.NomTypes )} should contain key {elementInfo.NomTypeId}");
                    }
                }
                else
                {
                    result = (false, $"{nameof( this.ElementEntity.NomTypes )} count {this.ElementEntity.NomTypeEntities.Count} should be {expectedCount}");
                }
            }
            else
            {
                result = (false, $"{nameof( this.ElementEntity.NomTypes )} is empty where it should have items");
            }

            return result;
        }

        /// <summary>   Adds the elements to 'productName'. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productNumber">    The product number. </param>
        public void AddElements( System.Data.IDbConnection connection, string productNumber )
        {
            (bool Success, string Details) result;
            foreach ( ElementInfo elementinfo in SelectElementInfo( productNumber ) )
            {
                result = this.TryObtainElement( connection, elementinfo );
                if ( !result.Success )
                    throw new InvalidOperationException( result.Details );
                if ( elementinfo.IsCritical )
                    this.CriticalElementLabel = elementinfo.Label;
                if ( elementinfo.IsPrimary )
                    this.PrimaryElementLabel = elementinfo.Label;
            }
        }

        /// <summary>   The critical element label. </summary>
        private string _CriticalElementLabel;

        /// <summary>   Gets or sets the critical element label. </summary>
        /// <value> The critical element label. </value>
        public string CriticalElementLabel
        {
            get => this._CriticalElementLabel;

            set {
                if ( !string.Equals( value, this.CriticalElementLabel ) )
                {
                    this._CriticalElementLabel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The primary element label. </summary>
        private string _PrimaryElementLabel;

        /// <summary>   Gets or sets the Primary element label. </summary>
        /// <value> The Primary element label. </value>
        public string PrimaryElementLabel
        {
            get => this._PrimaryElementLabel;

            set {
                if ( !string.Equals( value, this.PrimaryElementLabel ) )
                {
                    this._PrimaryElementLabel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}

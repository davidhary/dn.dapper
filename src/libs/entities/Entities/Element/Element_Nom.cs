
using System.Collections.ObjectModel;

namespace isr.Dapper.Entities
{
    /// <summary>   An element entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class ElementEntity
    {

        /// <summary>
        /// Gets or sets the Keyed collection of
        /// <see cref="PartNomEntityCollection"/> index by
        /// <see cref="Dapper.Entities.MeterEntity"/> and <see cref="Dapper.Entities.NomTypeEntity"/> identities.
        /// </summary>
        /// <value> The nominals. </value>
        public PartNomEntitiesCollection NomTraitsCollection { get; private set; }

        /// <summary>   Fetches the collection of nominal trait entities for the specified platform meter. </summary>
        /// <remarks>   David, 2021-05-20. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="part">             The <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   The number of nominal traits that were added for this platform meter. </returns>
        public int FetchNoms( System.Data.IDbConnection connection, PartEntity part, PlatformMeter platformMeter )
        {
            if ( !NomTypeEntity.IsEnumerated() )
                _ = NomTypeEntity.TryFetchAll( connection );
            int count = 0;
            PartNomEntityCollection partNomTraits;
            foreach ( NomType nomType in platformMeter.NominalTypes )
            {
                partNomTraits = new PartNomEntityCollection( part, MeterEntity.EntityLookupDictionary()[platformMeter.MeterId], this, ( int ) nomType );
                var entities = PartNomEntity.FetchOrderedNomTraits( connection, part.AutoId, platformMeter.MeterId, this.AutoId );
                partNomTraits.Populate( entities );
                count += partNomTraits.Count;
                this.NomTraitsCollection.Add( partNomTraits );
            }
            return count;
        }
    }

    /// <summary>
    /// Keyed collection of <see cref="PartNomEntityCollection"/> indexed by
    /// <see cref="Dapper.Entities.MeterEntity"/> and <see cref="Dapper.Entities.NomTypeEntity"/> identities.
    /// </summary>
    /// <remarks>   David, 2020-06-16. </remarks>
    public class PartNomEntitiesCollection : KeyedCollection<MeterNomTypeSelector, PartNomEntityCollection>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        public PartNomEntitiesCollection() : base()
        {
            this.NomTraitBindingList = new Std.BindingLists.InvokingBindingList<NomTrait>();
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-06-22. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override MeterNomTypeSelector GetKeyForItem( PartNomEntityCollection item )
        {
            return new MeterNomTypeSelector( item.MeterId, item.NomTypeId );
        }

        /// <summary>   Gets or sets a list of meter Nominal Trait bindings. </summary>
        /// <value> A list of meter Nominal Trait bindings. </value>
        public Std.BindingLists.InvokingBindingList<NomTrait> NomTraitBindingList { get; private set; }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new void Add( PartNomEntityCollection item )
        {
            base.Add( item );
            this.NomTraitBindingList.Add( item.NomTrait );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        public new void Clear()
        {
            base.Clear();
            this.NomTraitBindingList.Clear();
        }
    }
}

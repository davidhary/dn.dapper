using System.ComponentModel;
using System.Threading;
using System;

namespace isr.Dapper.Entities
{

    /// <summary>   The Element Text class holing the Element Text values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class ElementText : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Initializes a new instance of the <see cref="T:ElementText" /> class. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSetter"> The getter setter. </param>
        /// <param name="element">      The <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        public ElementText( Std.Primitives.IGetterSetter getterSetter, ElementEntity element, int nomTypeId ) : base()
        {
            this.GetterSetter = getterSetter;
            this.ElementAutoId = element.AutoId;
            this.ElementLabel = element.ElementLabel;
            this.ElementType = ( ElementType ) element.ElementTypeId;
            this.NomType = ( NomType ) nomTypeId;
            this.Selector = nomTypeId;
        }

        /// <summary>   Gets or sets the selector. </summary>
        /// <value> The selector. </value>
        public int Selector { get; private set; }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter GetterSetter { get; set; }

        /// <summary>   Gets the text value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Integer. </returns>
        public string Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Sets the text value. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A nullable Integer. </returns>
        public string Setter( string value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value, name );
                this.NotifyPropertyChanged( name );
            }

            return value;
        }

        #endregion

        #region " IDENTIFIERS "

        /// <summary>   Type of the nom. </summary>
        private NomType _NomType;

        /// <summary>   Gets or sets the type of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> The type of the nominal. </value>
        public NomType NomType
        {
            get => this._NomType;

            set {
                if ( value != this.NomType )
                {
                    this._NomType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Identifies the element automatic. </summary>
        private int _ElementAutoId;

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int ElementAutoId
        {
            get => this._ElementAutoId;

            set {
                if ( value != this.ElementAutoId )
                {
                    this._ElementAutoId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " ELEMENT PROPERTIES "

        /// <summary>   The element label. </summary>
        private string _ElementLabel;

        /// <summary>   Gets or sets the Label of the Element. </summary>
        /// <value> The Label of the Element. </value>
        public string ElementLabel
        {
            get => this._ElementLabel;

            set {
                if ( !string.Equals( value, this.ElementLabel ) )
                {
                    this._ElementLabel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Type of the element. </summary>
        private ElementType _ElementType;

        /// <summary>   Gets or sets the type of the Element. </summary>
        /// <value> The type of the Element. </value>
        public ElementType ElementType
        {
            get => this._ElementType;

            set {
                if ( value != this.ElementType )
                {
                    this._ElementType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " SCANNNING "

        /// <summary>   Gets or sets a list of scans. </summary>
        /// <value> A list of scans. </value>
        public string ScanList
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        #endregion

    }
}

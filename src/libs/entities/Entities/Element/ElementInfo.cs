using System;
using System.Collections.Generic;

namespace isr.Dapper.Entities
{

    /// <summary>   Information about the element. </summary>
    /// <remarks>   David, 2020-06-15. </remarks>
    public struct ElementInfo : IEquatable<ElementInfo>
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="label">            The label. </param>
        /// <param name="ordinalNumber">    The ordinal number. </param>
        /// <param name="elementTypeId">    Identifies the <see cref="Dapper.Entities.ElementTypeEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="isCritical">       The critical identifier. </param>
        /// <param name="isPrimary">        The primary identifier. </param>
        public ElementInfo( string label, int ordinalNumber, int elementTypeId, int nomTypeId, bool isCritical, bool isPrimary )
        {
            this.Label = label;
            this.OrdinalNumber = ordinalNumber;
            this.ElementTypeId = elementTypeId;
            this.NomTypeId = nomTypeId;
            this.IsCritical = isCritical;
            this.IsPrimary = isPrimary;
        }

        /// <summary>   Gets or sets the ordinal number. </summary>
        /// <value> The ordinal number. </value>
        public int OrdinalNumber { get; set; }

        /// <summary>   Gets or sets the label. </summary>
        /// <value> The label. </value>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the Identifies the <see cref="Dapper.Entities.ElementTypeEntity"/>.
        /// </summary>
        /// <value> The identifier of the <see cref="Dapper.Entities.ElementTypeEntity"/>. </value>
        public int ElementTypeId { get; set; }

        /// <summary>   Gets or sets the Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> The Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public int NomTypeId { get; set; }

        /// <summary>   Identifies the critical element. </summary>
        /// <value> The critical identifier. </value>
        public bool IsCritical { get; set; }

        /// <summary>   Identifies the primary element. </summary>
        /// <value> The primary identifier. </value>
        public bool IsPrimary { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( ElementInfo ) obj );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( ElementInfo other )
        {
            return (this.Label ?? "") == (other.Label ?? "") && this.ElementTypeId == other.ElementTypeId && this.OrdinalNumber == other.OrdinalNumber;
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.Label, this.ElementTypeId, this.OrdinalNumber ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( ElementInfo left, ElementInfo right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( ElementInfo left, ElementInfo right )
        {
            return !(left == right);
        }
    }

    /// <summary>   Collection of part specification elements. </summary>
    /// <remarks>   David, 2020-04-18. </remarks>
    public class ElementInfoCollection : System.Collections.ObjectModel.KeyedCollection<string, ElementInfo>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-07-12. </remarks>
        public ElementInfoCollection() : base()
        {
            this.OrdinalNumberKeyDictionary = new Dictionary<int, ElementInfo>();
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-04-18. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override string GetKeyForItem( ElementInfo item )
        {
            return item.Label;
        }

        /// <summary>   Gets or sets a dictionary of ordinal number keys. </summary>
        /// <value> A dictionary of ordinal number keys. </value>
        public IDictionary<int, ElementInfo> OrdinalNumberKeyDictionary { get; private set; }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-07-12. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new void Add( ElementInfo item )
        {
            base.Add( item );
            this.OrdinalNumberKeyDictionary.Add( item.OrdinalNumber, item );
        }
    }
}

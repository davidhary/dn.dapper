using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// A builder for a Element table based on the
    /// <see cref="IKeyForeignLabelNatural">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ElementBuilder : KeyForeignLabelNaturalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ElementNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the foreign table . </summary>
        /// <value> The name of the foreign table. </value>
        public override string ForeignTableName { get; set; } = ElementTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <value> The name of the foreign table key. </value>
        public override string ForeignTableKeyName { get; set; } = nameof( ElementTypeNub.Id );

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public override int LabelFieldSize { get; set; } = 50;

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( ElementEntity.AutoId );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( ElementEntity.ElementTypeId );

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public override string LabelFieldName { get; set; } = nameof( ElementEntity.ElementLabel );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( ElementEntity.OrdinalNumber );

        #region " SINGLETON "

        private static readonly Lazy<ElementBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ElementBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Natural Element table based on the
    /// <see cref="IKeyForeignLabelNatural">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "Element" )]
    public class ElementNub : KeyForeignLabelNaturalNub, IKeyForeignLabelNatural
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public ElementNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignLabelNatural CreateNew()
        {
            return new ElementNub();
        }
    }

    /// <summary>
    /// The Element Entity based on the <see cref="IKeyForeignLabelNatural">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public partial class ElementEntity : EntityBase<IKeyForeignLabelNatural, ElementNub>, IKeyForeignLabelNatural
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public ElementEntity() : this( new ElementNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ElementEntity( IKeyForeignLabelNatural value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ElementEntity( IKeyForeignLabelNatural cache, IKeyForeignLabelNatural store ) : base( new ElementNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ElementEntity( ElementEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ElementBuilder.TableName, nameof( IKeyForeignLabelNatural ).TrimStart( 'I' ) );
            this.NomTraitsCollection = new PartNomEntitiesCollection();
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignLabelNatural CreateNew()
        {
            return new ElementNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyForeignLabelNatural CreateCopy()
        {
            var destination = this.CreateNew();
            KeyForeignLabelNaturalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyForeignLabelNatural value )
        {
            KeyForeignLabelNaturalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Element interface. </param>
        public override void UpdateCache( IKeyForeignLabelNatural value )
        {
            // first make the copy to notify of any property change.
            KeyForeignLabelNaturalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The Element table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyForeignLabelNatural>( key ) : connection.Get<ElementNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Label );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="elementLabel"> The Element number. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string elementLabel )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, elementLabel ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyForeignLabelNatural entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            bool fetched = UniqueIndexOptions.Label == (ElementBuilder.Instance.QueryUniqueIndexOptions( connection ) & UniqueIndexOptions.Label) ? this.FetchUsingUniqueIndex( connection, entity.Label ) : this.FetchUsingKey( connection, entity.AutoId );
            if ( fetched )
            {
                // update the existing record from the specified entity.
                entity.AutoId = this.AutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new ElementNub() { AutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Element entities. </summary>
        /// <value> The Element entities. </value>
        public IEnumerable<ElementEntity> Elements { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ElementEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyForeignLabelNatural>() ) : Populate( connection.GetAll<ElementNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Elements = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ElementEntity.Elements ) );
            return this.Elements?.Any() == true ? this.Elements.Count() : 0;
        }

        /// <summary>   Populates a list of Element entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The Element nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ElementEntity> Populate( IEnumerable<ElementNub> nubs )
        {
            var l = new List<ElementEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ElementNub nub in nubs )
                    l.Add( new ElementEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Element entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The Element interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ElementEntity> Populate( IEnumerable<IKeyForeignLabelNatural> interfaces )
        {
            var l = new List<ElementEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ElementNub();
                foreach ( IKeyForeignLabelNatural iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ElementEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="elementLabel"> The Element number. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string elementLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ElementBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNub.Label )} = @elementLabel", new { elementLabel } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="elementLabel"> The Element number. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntitiesAlternative( System.Data.IDbConnection connection, string elementLabel )
        {
            var selectSql = new System.Text.StringBuilder( $"SELECT * FROM [{ElementBuilder.TableName}]" );
            _ = selectSql.Append( $"WHERE (@{nameof( ElementNub.Label )} IS NULL OR {nameof( ElementNub.Label )} = @ElementLabel)" );
            _ = selectSql.Append( "; " );
            return connection.ExecuteScalar<int>( selectSql.ToString(), new { elementLabel } );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="elementLabel"> The Element number. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ElementNub> FetchNubs( System.Data.IDbConnection connection, string elementLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ElementBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNub.Label )} = @elementLabel", new { elementLabel } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ElementNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Element exists. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="elementLabel"> The Element number. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string elementLabel )
        {
            return 1 == CountEntities( connection, elementLabel );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the Element type. </summary>
        /// <value> Identifies the Element type. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ElementEntity.ElementTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the identity of the Element Type. </summary>
        /// <value> The type of the identity of the Element Type. </value>
        public int ElementTypeId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }

        /// <summary>   Gets or sets the element Label. </summary>
        /// <value> The Label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ElementEntity.ElementLabel ) );
                }
            }
        }

        /// <summary>   Gets or sets the element label. </summary>
        /// <value> The element label. </value>
        public string ElementLabel
        {
            get => this.Label;

            set => this.Label = value;
        }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        public int Amount
        {
            get => this.ICache.Amount;

            set {
                if ( this.Amount != value )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( MeterEntity.MeterNumber ) );
                }
            }
        }

        /// <summary>   Gets or sets the ordinal number. </summary>
        /// <value> The ordinal number. </value>
        public int OrdinalNumber
        {
            get => this.Amount;

            set => this.Amount = value;
        }

        #endregion

    }

    /// <summary>   Collection of element entities. </summary>
    /// <remarks>   David, 2020-05-18. </remarks>
    public class ElementEntityCollection : EntityKeyedCollection<int, IKeyForeignLabelNatural, ElementNub, ElementEntity>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public ElementEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<int, string>();
            this._PrimaryKeyDictionary = new Dictionary<string, int>();
            this._ElementOrdinalNumberDictionary = new Dictionary<int, int>();
            this._OrdinalNumberDictionary = new Dictionary<int, int>();
            this._ElementTypeCountDictionary = new Dictionary<int, int>();
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override int GetKeyForItem( ElementEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.AutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<int, string> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<string, int> _PrimaryKeyDictionary;

        /// <summary>   Dictionary of Element Identifiers by Element ordinal numbers. </summary>
        private readonly IDictionary<int, int> _ElementOrdinalNumberDictionary;

        /// <summary>   Dictionary of Element Identifiers by collection ordinal numbers. </summary>
        private readonly IDictionary<int, int> _OrdinalNumberDictionary;

        /// <summary>   Dictionary of element type counts. </summary>
        private readonly IDictionary<int, int> _ElementTypeCountDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( ElementEntity item )
        {
            base.Add( item );
            this._PrimaryKeyDictionary.Add( item.Label, item.AutoId );
            this._UniqueIndexDictionary.Add( item.AutoId, item.Label );
            this._OrdinalNumberDictionary.Add( this.Count, item.AutoId );
            this._ElementOrdinalNumberDictionary.Add( item.OrdinalNumber, item.AutoId );
            if ( this._ElementTypeCountDictionary.ContainsKey( item.ElementTypeId ) )
            {
                this._ElementTypeCountDictionary[item.ElementTypeId] += 1;
            }
            else
            {
                this._ElementTypeCountDictionary.Add( item.ElementTypeId, 1 );
            }
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
            this._ElementTypeCountDictionary.Clear();
            this._ElementOrdinalNumberDictionary.Clear();
            this._OrdinalNumberDictionary.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public virtual void Populate( IEnumerable<ElementEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ElementEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Query if this  contains the given elementLabel. </summary>
        /// <remarks>   David, 2020-06-10. </remarks>
        /// <param name="elementLabel"> The element label. </param>
        /// <returns>   True if the object is in this collection, false if not. </returns>
        public bool Contains( string elementLabel )
        {
            return this._PrimaryKeyDictionary.ContainsKey( elementLabel );
        }

        /// <summary>   Selects an element from the collection. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="elementLabel"> The element label. </param>
        /// <returns>   An ElementEntity. </returns>
        public ElementEntity SelectElementByElementLabel( string elementLabel )
        {
            int id = this._PrimaryKeyDictionary[elementLabel];
            return this.Contains( id ) ? this[id] : new ElementEntity();
        }

        /// <summary>   Selects an element from the collection by the element ordinal number. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="elementOrdinalNumber"> The element ordinal number. </param>
        /// <returns>   An ElementEntity. </returns>
        public ElementEntity SelectElementByElementOrdinalNumber( int elementOrdinalNumber )
        {
            int id = this._ElementOrdinalNumberDictionary[elementOrdinalNumber];
            return this.Contains( id ) ? this[id] : new ElementEntity();
        }

        /// <summary>   Select element by the order it was entered into the collection. </summary>
        /// <remarks>   David, 2021-05-20. </remarks>
        /// <param name="ordinalNumber">    The ordinal number. </param>
        /// <returns>   An ElementEntity. </returns>
        public ElementEntity SelectElementByOrdinalNumber( int ordinalNumber )
        {
            int id = this._OrdinalNumberDictionary[ordinalNumber];
            return this.Contains( id ) ? this[id] : new ElementEntity();
        }

        /// <summary>   Element label. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   A String. </returns>
        public string ElementLabel( int elementAutoId )
        {
            return this._UniqueIndexDictionary.ContainsKey( elementAutoId ) ? this._UniqueIndexDictionary[elementAutoId] : string.Empty;
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return connection.Upsert( ElementBuilder.TableName, this.Items );
        }
    }

    /// <summary>   Collection of element entities uniquely associated with a product. </summary>
    /// <remarks>   David, 2020-05-18. </remarks>
    public class ProductUniqueElementEntityCollection : ElementEntityCollection
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public ProductUniqueElementEntityCollection() : base()
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        public ProductUniqueElementEntityCollection( int productAutoId ) : this()
        {
            this.ProductAutoId = productAutoId;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( ElementEntity item )
        {
            base.Add( item );
            if ( this.PrimaryElement is null || string.Equals( item.ElementLabel, "R1" ) )
            {
                this.PrimaryElement = item;
            }
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public override void Populate( IEnumerable<ElementEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ElementEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ProductEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </value>
        public int ProductAutoId { get; private set; }

        /// <summary>   Fetches nominal types. </summary>
        /// <remarks>   David, 2020-06-25. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of elements in this collection. </returns>
        public int FetchNomTypes( System.Data.IDbConnection connection )
        {
            foreach ( ElementEntity element in this )
                _ = element.FetchNomTypes( connection );
            return this.Count;
        }

        /// <summary>   Gets or sets the primary element. </summary>
        /// <value> The primary element. </value>
        public ElementEntity PrimaryElement { get; set; }

    }

    /// <summary>   Collection of element entities uniquely associated with a Meter. </summary>
    /// <remarks>   David, 2020-05-18. </remarks>
    public class MeterUniqueElementEntityCollection : ElementEntityCollection
    {

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        private MeterUniqueElementEntityCollection() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <param name="meterId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        public MeterUniqueElementEntityCollection( int meterId ) : this()
        {
            this.MeterId = meterId;
        }

        /// <summary>   Gets or sets the number of expected meter readings. </summary>
        /// <value> The number of expected meter readings. </value>
        public int ExpectedMeterReadingsCount { get; private set; }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( ElementEntity entity )
        {
            base.Add( entity );
            if ( this.PrimaryElement is null || string.Equals( entity.ElementLabel, "R1" ) || string.Equals( entity.ElementLabel, "THI" ) )
            {
                this.PrimaryElement = entity;
            }

            if ( ( int ) ElementType.Resistor == entity.ElementTypeId || ( int ) ElementType.CompoundResistor == entity.ElementTypeId
              || ( int ) ElementType.OpenCircuit == entity.ElementTypeId || ( int ) ElementType.ShortCircuit == entity.ElementTypeId )
            {
                // TO_DO: This number should be derived from the 
                // number of meter-aware nominal types. 
                this.ExpectedMeterReadingsCount += 1;
            }
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this.ExpectedMeterReadingsCount = 0;
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public override void Populate( IEnumerable<ElementEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ElementEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId { get; private set; }

        /// <summary>   Fetches nominal types. </summary>
        /// <remarks>   David, 2020-06-25. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of elements in this collection. </returns>
        public int FetchNomTypes( System.Data.IDbConnection connection )
        {
            foreach ( ElementEntity element in this )
                _ = element.FetchNomTypes( connection );
            return this.Count;
        }

        /// <summary>   Gets or sets the primary element. </summary>
        /// <value> The primary element. </value>
        public ElementEntity PrimaryElement { get; set; }

        /// <summary>
        /// Select a nominal trait entity; This is used for selecting the <see cref="NomTraitEntity"/> 
        /// for selecting the most stringent meter settings.
        /// </summary>
        /// <remarks>   David, 2020-06-18. </remarks>
        /// <param name="nomType">  Type of the nom. </param>
        /// <returns>   A <see cref="NomTraitEntity"/>. </returns>
        public NomTraitEntity SelectNomTraitEntity( NomType nomType )
        {
            var result = new NomTraitEntity();
            foreach ( ElementEntity element in this )
            {
                foreach ( NomTypeEntity nomTypeEntity in element.NomTypes )
                {
                    if ( nomTypeEntity.Id == ( int ) nomType )
                    {
                        result = element.NomTraitsCollection[nomTypeEntity.Id].Entity( nomTypeEntity.Id );
                        break;
                    }
                }

                if ( result.IsClean() )
                    break;
            }

            return result;
        }

    }

}

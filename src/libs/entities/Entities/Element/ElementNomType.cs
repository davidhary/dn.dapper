using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Element-Nominal-Type builder. </summary>
    /// <remarks>   David, 2020-06-24. </remarks>
    public sealed class ElementNomTypeBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ElementNomTypeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = ElementBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( ElementNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = NomTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( NomTypeNub.Id );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( ElementNomTypeEntity.ElementAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( ElementNomTypeEntity.NomTypeId );

        #region " SINGLETON "

        private static readonly Lazy<ElementNomTypeBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ElementNomTypeBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="ElementNomTypeNub"/> based on the
    /// <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>   David, 2020-06-24. </remarks>
    [Table( "ElementNomType" )]
    public class ElementNomTypeNub : OneToManyNub, IOneToMany
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        public ElementNomTypeNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new ElementNomTypeNub();
        }
    }

    /// <summary>
    /// The Element-Nominal-Type Entity based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>   David, 2020-06-24. </remarks>
    public class ElementNomTypeEntity : EntityBase<IOneToMany, ElementNomTypeNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        public ElementNomTypeEntity() : this( new ElementNomTypeNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ElementNomTypeEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ElementNomTypeEntity( IOneToMany cache, IOneToMany store ) : base( new ElementNomTypeNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ElementNomTypeEntity( ElementNomTypeEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ElementNomTypeBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
            this.NomTypes = new NomTypeEntityCollection();
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new ElementNomTypeNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="value">    The Element-Nominal-Type interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, elementAutoId, nomTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.ElementAutoId, this.NomTypeId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.ElementAutoId, this.NomTypeId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            return this.FetchUsingKey( connection, elementAutoId, nomTypeId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            return connection.Delete( new ElementNomTypeNub() { PrimaryId = elementAutoId, SecondaryId = nomTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Element-Nominal-Type entities. </summary>
        /// <value> The Element-Nominal-Type entities. </value>
        public IEnumerable<ElementNomTypeEntity> ElementNomTypes { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ElementNomTypeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<ElementNomTypeNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ElementNomTypes = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ElementNomTypeEntity.ElementNomTypes ) );
            return this.ElementNomTypes?.Any() == true ? this.ElementNomTypes.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ElementNomTypeEntity> Populate( IEnumerable<ElementNomTypeNub> nubs )
        {
            var l = new List<ElementNomTypeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ElementNomTypeNub nub in nubs )
                    l.Add( new ElementNomTypeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ElementNomTypeEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<ElementNomTypeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ElementNomTypeNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ElementNomTypeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>
        /// Count entities; returns up to <see cref="Dapper.Entities.NomTypeEntity"/>'s count.
        /// </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ElementNomTypeBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTypeNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ElementNomTypeEntity> FetchEntities( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}] WHERE {nameof( ElementNomTypeNub.PrimaryId )} = @Id", new { Id = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ElementNomTypeNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Nominal Type. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   The total number of entities by Nominal Type. </returns>
        public static int CountEntitiesByNomType( System.Data.IDbConnection connection, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ElementNomTypeBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTypeNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nomTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Nominal Type in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Nominal Types in this
        /// collection.
        /// </returns>
        public static IEnumerable<ElementNomTypeEntity> FetchEntitiesByNomType( System.Data.IDbConnection connection, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}] WHERE {nameof( ElementNomTypeNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nomTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ElementNomTypeNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ElementNomTypeBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTypeNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTypeNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nomTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ElementNomTypeNub> FetchNubs( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ElementNomTypeBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTypeNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTypeNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nomTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ElementNomTypeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Element Nominal Type exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            return 1 == CountEntities( connection, elementAutoId, nomTypeId );
        }

        #endregion

        #region " RELATIONS: ELEMENT "

        /// <summary>   Gets or sets the Element entity. </summary>
        /// <value> The Element entity. </value>
        public ElementEntity ElementEntity { get; private set; }

        /// <summary>   Fetches Element Entity. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Element Entity. </returns>
        public ElementEntity FetchElementEntity( System.Data.IDbConnection connection )
        {
            var entity = new ElementEntity();
            _ = entity.FetchUsingKey( connection, this.ElementAutoId );
            this.ElementEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count elements associated with the specified <paramref name="nomTypeId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   The total number of elements. </returns>
        public static int CountElements( System.Data.IDbConnection connection, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}] WHERE {nameof( ElementNomTypeNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nomTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Elements associated with the specified <paramref name="nomTypeId"/>; expected a
        /// single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Elements in this collection.
        /// </returns>
        public static IEnumerable<NomTypeEntity> FetchElements( System.Data.IDbConnection connection, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}] WHERE {nameof( ElementNomTypeNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nomTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<NomTypeEntity>();
            foreach ( ElementNomTypeNub nub in connection.Query<ElementNomTypeNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new ElementNomTypeEntity( nub );
                l.Add( entity.FetchNomTypeEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all elements associated with the specified <paramref name="nomTypeId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteElements( System.Data.IDbConnection connection, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}] WHERE {nameof( ElementNomTypeNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nomTypeId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        #endregion

        #region " RELATIONS: NOMINAL TYPE "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public NomTypeEntity NomTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The <see cref="Dapper.Entities.NomTypeEntity"/>. </returns>
        public NomTypeEntity FetchNomTypeEntity( System.Data.IDbConnection connection )
        {
            var entity = new NomTypeEntity();
            _ = entity.FetchUsingKey( connection, this.NomTypeId );
            this.NomTypeEntity = entity;
            return entity;
        }

        /// <summary>   Gets or sets a list of types of the nominals. </summary>
        /// <value> A list of types of the nominals. </value>
        public NomTypeEntityCollection NomTypes { get; private set; }

        /// <summary>   Fetches the <see cref="NomTypeEntityCollection"/>. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Nominal Types in this collection.
        /// </returns>
        public NomTypeEntityCollection FetchNomTypes( System.Data.IDbConnection connection )
        {
            this.NomTypes.Clear();
            this.NomTypes.Populate( FetchOrderedNomTypes( connection, this.ElementAutoId ) );
            return this.NomTypes;
        }

        /// <summary>   Count nominal types. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   The total number of nominal types. </returns>
        public static int CountNomTypes( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}] WHERE {nameof( ElementNomTypeNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the <see cref="Dapper.Entities.NomTypeEntity"/>'s. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Nominal Types in this collection.
        /// </returns>
        public static IEnumerable<NomTypeEntity> FetchNomTypes( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}] WHERE {nameof( ElementNomTypeNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<NomTypeEntity>();
            foreach ( ElementNomTypeNub nub in connection.Query<ElementNomTypeNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new ElementNomTypeEntity( nub );
                l.Add( entity.FetchNomTypeEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all <see cref="Dapper.Entities.NomTypeEntity"/> related to the specified Element.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteNomTypes( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}] WHERE {nameof( ElementNomTypeNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.NomTypeEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered nominal Types in this
        /// collection.
        /// </returns>
        public static IEnumerable<NomTypeEntity> FetchOrderedNomTypes( System.Data.IDbConnection connection, string selectQuery, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { elementAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return NomTypeEntity.Populate( connection.Query<NomTypeNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered <see cref="Dapper.Entities.NomTypeEntity"/>'s. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered nominal Types in this
        /// collection.
        /// </returns>
        public static IEnumerable<NomTypeEntity> FetchOrderedNomTypes( System.Data.IDbConnection connection, int elementAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [ElementNomType] on [ElementNomType].SecondaryId = [NomType].AutoId where [ElementNomType].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{NomTypeBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{NomTypeBuilder.TableName}] Inner Join [{ElementNomTypeBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}].{nameof( ElementNomTypeNub.SecondaryId )} = [{isr.Dapper.Entities.NomTypeBuilder.TableName}].{nameof( NomTypeNub.Id )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}].{nameof( ElementNomTypeNub.PrimaryId )} = @{nameof( elementAutoId )}" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.NomTypeBuilder.TableName}].{nameof( NomTypeNub.Id )} ASC; " );
            return FetchOrderedNomTypes( connection, queryBuilder.ToString(), elementAutoId );
        }

        /// <summary>   Fetches the first <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the last Nominal Types in this
        /// collection.
        /// </returns>
        public static NomTypeEntity FetchFirstNomType( System.Data.IDbConnection connection, string selectQuery, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery, new { elementAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<NomTypeNub>( template.RawSql, template.Parameters ).FirstOrDefault();
            return nub is null ? new NomTypeEntity() : new NomTypeEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches the last <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the last Nominal Type in this
        /// collection.
        /// </returns>
        public static NomTypeEntity FetchLastNomType( System.Data.IDbConnection connection, int elementAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [ElementNomType] on [ElementNomType].SecondaryId = [NomType].AutoId where [ElementNomType].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{NomTypeBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{NomTypeBuilder.TableName}] Inner Join [{ElementNomTypeBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}].{nameof( ElementNomTypeNub.SecondaryId )} = [{isr.Dapper.Entities.NomTypeBuilder.TableName}].{nameof( NomTypeNub.Id )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.ElementNomTypeBuilder.TableName}].{nameof( ElementNomTypeNub.PrimaryId )} = @{nameof( elementAutoId )}" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.NomTypeBuilder.TableName}].{nameof( NomTypeNub.Id )} DESC; " );
            return FetchFirstNomType( connection, queryBuilder.ToString(), elementAutoId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ElementNomTypeEntity.ElementAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/> </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/> </value>
        public int ElementAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ElementNomTypeEntity.NomTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public int NomTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="ElementNomTypeEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class ElementNomTypeEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToMany, ElementNomTypeNub, ElementNomTypeEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( ElementNomTypeEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( ElementNomTypeEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all Elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<ElementNomTypeEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ElementNomTypeEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return ElementNomTypeBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of unique <see cref="Dapper.Entities.ElementEntity"/> +
    /// <see cref="Dapper.Entities.NomTypeEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class ElementUniqueNomTypeEntityCollection : ElementNomTypeEntityCollection
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        public ElementUniqueNomTypeEntityCollection( int elementAutoId ) : base()
        {
            this.ElementAutoId = elementAutoId;
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( ElementNomTypeEntity entity )
        {
            if ( entity.ElementAutoId == this.ElementAutoId )
            {
                base.Add( entity );
                this._PrimaryKeyDictionary.Add( entity.NomTypeId, entity.EntitySelector );
                this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.NomTypeId );
            }
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        public void Add( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            var elementNomType = new ElementNomTypeEntity() { ElementAutoId = elementAutoId, NomTypeId = nomTypeId };
            if ( elementNomType.Obtain( connection ) )
            {
                this.Add( elementNomType );
            }
            else
            {
                throw new InvalidOperationException( $"Failed obtaining {nameof( ElementNomTypeEntity )} with [{nameof( ElementNomTypeEntity.ElementAutoId )},{nameof( ElementNomTypeEntity.NomTypeId )} ] of [{elementAutoId},{nomTypeId}] " );
            }
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Query if collection contains nominal type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsNomType( int nomTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( nomTypeId );
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int ElementAutoId { get; private set; }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Element Nominal Text builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ElementNomTextBuilder : ThreeToManyLabelBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ElementNomTextNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = ElementBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( ElementNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = NomTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the Secondary table key. </summary>
        /// <value> The name of the Secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( NomTypeNub.Id );

        /// <summary>   Gets or sets the name of the Ternary table. </summary>
        /// <value> The name of the Ternary table. </value>
        public override string TernaryTableName { get; set; } = ElementTextTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the Ternary table key. </summary>
        /// <value> The name of the Ternary table key. </value>
        public override string TernaryTableKeyName { get; set; } = nameof( ElementTextTypeNub.Id );

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public override int LabelFieldSize { get; set; } = 255;

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( ElementNomTextEntity.ElementAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( ElementNomTextEntity.NomTypeId );

        /// <summary>   Gets or sets the name of the ternary identifier field. </summary>
        /// <value> The name of the ternary identifier field. </value>
        public override string TernaryIdFieldName { get; set; } = nameof( ElementNomTextEntity.ElementTextTypeId );

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public override string LabelFieldName { get; set; } = nameof( ElementNomTextEntity.Label );

        #region " SINGLETON "

        private static readonly Lazy<ElementNomTextBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ElementNomTextBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="Dapper.Entities.ElementEntity"/> Text table
    /// <see cref="IThreeToManyLabel">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "ElementNomText" )]
    public class ElementNomTextNub : ThreeToManyLabelNub, IThreeToManyLabel
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public ElementNomTextNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IThreeToManyLabel CreateNew()
        {
            return new ElementNomTextNub();
        }
    }

    /// <summary>
    /// The <see cref="ElementNomTextEntity"/>. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class ElementNomTextEntity : EntityBase<IThreeToManyLabel, ElementNomTextNub>, IThreeToManyLabel
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public ElementNomTextEntity() : this( new ElementNomTextNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ElementNomTextEntity( IThreeToManyLabel value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ElementNomTextEntity( IThreeToManyLabel cache, IThreeToManyLabel store ) : base( new ElementNomTextNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ElementNomTextEntity( ElementNomTextEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ElementNomTextBuilder.TableName, nameof( IThreeToManyLabel ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IThreeToManyLabel CreateNew()
        {
            return new ElementNomTextNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IThreeToManyLabel CreateCopy()
        {
            var destination = this.CreateNew();
            ThreeToManyLabelNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="ElementNomTextEntity"/> interface value. </param>
        public override void CopyFrom( IThreeToManyLabel value )
        {
            ThreeToManyLabelNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached Text, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.ElementEntity"/>Text interface. </param>
        public override void UpdateCache( IThreeToManyLabel value )
        {
            // first make the copy to notify of any property change.
            ThreeToManyLabelNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="elementAutoId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementTextTypeId">    Identifies the <see cref="ElementTextTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int elementAutoId, int elementTextTypeId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ElementNomTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTextNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTextNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementTextTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<ElementNomTextNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="elementAutoId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementTextTypeId">    Identifies the <see cref="ElementTextTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int elementAutoId, int elementTextTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, elementAutoId, elementTextTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IThreeToManyLabel entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="elementAutoId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementTextTypeId">    Identifies the <see cref="ElementTextTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int elementAutoId, int elementTextTypeId )
        {
            return connection.Delete( new ElementNomTextNub() { PrimaryId = elementAutoId, SecondaryId = elementTextTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.ElementEntity"/>Text entities. </summary>
        /// <value> the <see cref="Dapper.Entities.ElementEntity"/>Text entities. </value>
        public IEnumerable<ElementNomTextEntity> ElementTexts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ElementNomTextEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IThreeToManyLabel>() ) : Populate( connection.GetAll<ElementNomTextNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ElementTexts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ElementNomTextEntity.ElementTexts ) );
            return this.ElementTexts?.Any() == true ? this.ElementTexts.Count() : 0;
        }

        /// <summary>   Count Element Texts. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.ElementNomTextBuilder.TableName}] WHERE {nameof( ElementNomTextNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ElementNomTextEntity> FetchEntities( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ElementNomTextBuilder.TableName}] WHERE {nameof( ElementNomTextNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ElementNomTextNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Element Texts by Element Auto Id. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ElementNomTextNub> FetchNubs( System.Data.IDbConnection connection, int elementAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ElementNomTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTextEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ElementNomTextNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of Element Text entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.ElementEntity"/>Text nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ElementNomTextEntity> Populate( IEnumerable<ElementNomTextNub> nubs )
        {
            var l = new List<ElementNomTextEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ElementNomTextNub nub in nubs )
                    l.Add( new ElementNomTextEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Element Text entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.ElementEntity"/>Text interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ElementNomTextEntity> Populate( IEnumerable<IThreeToManyLabel> interfaces )
        {
            var l = new List<ElementNomTextEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ElementNomTextNub();
                foreach ( IThreeToManyLabel iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ElementNomTextEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Element Texts by unique index; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="elementAutoId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementTextTypeId">    Identifies the <see cref="ElementTextTypeEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int elementAutoId, int elementTextTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ElementNomTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTextNub.PrimaryId )} = @PrimaryId", new { PrimaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTextNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementTextTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches Element Texts by unique index; expected single or none. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="elementAutoId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementTextTypeId">    Identifies the <see cref="ElementTextTypeEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ElementNomTextNub> FetchEntities( System.Data.IDbConnection connection, int elementAutoId, int elementTextTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ElementNomTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTextNub.PrimaryId )} = @primaryId", new { primaryId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTextNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementTextTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ElementNomTextNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.ElementEntity"/>Text exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="elementAutoId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="elementTexttypeId">    Identifies the <see cref="ElementTextTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int elementAutoId, int elementTexttypeId )
        {
            return 1 == CountEntities( connection, elementAutoId, elementTexttypeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count Texts associated with this element. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of Texts. </returns>
        public int CountElementTexts( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ElementNomTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTextNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Element Text Texts by Element auto id;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-07-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<ElementNomTextNub> FetchElementTexts( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ElementNomTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ElementNomTextNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ElementNomTextNub>( selector.RawSql, selector.Parameters );
        }

        #endregion

        #region " RELATIONS: ELEMENT "

        /// <summary>   Gets or sets the Element entity. </summary>
        /// <value> The Element entity. </value>
        public ElementEntity ElementEntity { get; private set; }

        /// <summary>   Fetches a Element Entity. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Element Entity. </returns>
        public ElementEntity FetchElementEntity( System.Data.IDbConnection connection )
        {
            var entity = new ElementEntity();
            _ = entity.FetchUsingKey( connection, this.ElementAutoId );
            this.ElementEntity = entity;
            return entity;
        }

        #endregion

        #region " RELATIONS: NOM TYPE  "

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.ElementEntity"/> <see cref="NomTypeEntity"/>.
        /// </summary>
        /// <value> the <see cref="Dapper.Entities.ElementEntity"/> Text type entity. </value>
        public NomTypeEntity NomTypeEntity { get; private set; }

        /// <summary>   Fetches a element text type Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchNomTypeEntity( System.Data.IDbConnection connection )
        {
            this.NomTypeEntity = new NomTypeEntity();
            return this.NomTypeEntity.FetchUsingKey( connection, this.NomTypeId );
        }

        #endregion

        #region " RELATIONS: ELEMENT TEXT TYPE  "

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.ElementEntity"/> <see cref="ElementTextTypeEntity"/>.
        /// </summary>
        /// <value> the <see cref="Dapper.Entities.ElementEntity"/> Text type entity. </value>
        public ElementTextTypeEntity ElementTextTypeEntity { get; private set; }

        /// <summary>   Fetches a element text type Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchElementTextTypeEntity( System.Data.IDbConnection connection )
        {
            this.ElementTextTypeEntity = new ElementTextTypeEntity();
            return this.ElementTextTypeEntity.FetchUsingKey( connection, this.ElementTextTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ElementNomTextEntity.ElementAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/> record.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/> record. </value>
        public int ElementAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ElementNomTextEntity.NomTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the identity of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> Identifies the Element Text type. </value>
        public int NomTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>   Gets or sets the id of the Ternary reference. </summary>
        /// <value> The identifier of Ternary reference. </value>
        public int TernaryId
        {
            get => this.ICache.TernaryId;

            set {
                if ( !object.Equals( ( object ) this.TernaryId, ( object ) value ) )
                {
                    this.ICache.TernaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ElementNomTextEntity.ElementTextTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the identity (type) of the <see cref="Dapper.Entities.ElementNomTextEntity"/>.
        /// </summary>
        /// <value> Identifies the Element Text type. </value>
        public int ElementTextTypeId
        {
            get => this.TernaryId;

            set => this.TernaryId = value;
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.ElementNomTextEntity"/> label. </summary>
        /// <value> the <see cref="Dapper.Entities.ElementNomTextEntity"/> label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public ThreeKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="ElementNomTextEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class ElementNomTextEntityCollection : EntityKeyedCollection<ThreeKeySelector, IThreeToManyLabel, ElementNomTextNub, ElementNomTextEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override ThreeKeySelector GetKeyForItem( ElementNomTextEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( ElementNomTextEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<ElementNomTextEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ElementNomTextEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return ElementNomTextBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of <see cref="Dapper.Entities.ElementEntity"/>-Unique
    /// <see cref="ElementNomTextEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class ElementUniqueNomTextEntityCollection : ElementNomTextEntityCollection, isr.Std.Primitives.IGetterSetter
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="element">      The element. </param>
        /// <param name="nomTypeId">    Identifies the nom type. </param>
        public ElementUniqueNomTextEntityCollection( ElementEntity element, int nomTypeId ) : base()
        {
            this._UniqueIndexDictionary = new Dictionary<ThreeKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, ThreeKeySelector>();
            this.ElementText = new ElementText( this, element, nomTypeId );
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<ThreeKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, ThreeKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( ElementNomTextEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.ElementTextTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.ElementTextTypeId );
            this.NotifyPropertyChanged( ElementTextTypeEntity.EntityLookupDictionary()[entity.ElementTextTypeId].Label );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains 'elementTextType' key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="elementTextTypeId">    Identifies the
        ///                                     <see cref="Dapper.Entities.ElementTextTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int elementTextTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( elementTextTypeId );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Getters the given &lt; runtime. compiler services. caller member name. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        string IGetterSetter.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Gets the Real(String)-value for the given <see cref="ElementNomTextEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable String. </returns>
        protected string Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Set the specified element text value for the given <see cref="ElementNomTextEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A string. </returns>
        private string SetterThis( string value, string name )
        {
            int key = this.ToKey( name );
            if ( !string.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>
        /// Set the specified element text value for the given <see cref="ElementNomTextEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        string IGetterSetter.Setter( string value, string name )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>
        /// Set the specified element text value for the given <see cref="ElementNomTextEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        protected string Setter( string value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets or sets the Element Text. </summary>
        /// <value> The Element Text. </value>
        public ElementText ElementText { get; private set; }

        #endregion

        #region " TRAIT SELECTION "

        /// <summary>   Converts a name to a key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return ElementTextTypeEntity.KeyLookupDictionary()[name];
        }

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="elementTextTypeId">    Identifies the
        ///                                     <see cref="Dapper.Entities.ElementTextTypeEntity"/>. </param>
        /// <returns>   An ElementTextEntity. </returns>
        public ElementNomTextEntity Entity( int elementTextTypeId )
        {
            return this.ContainsKey( elementTextTypeId ) ? this[this._PrimaryKeyDictionary[elementTextTypeId]] : new ElementNomTextEntity();
        }

        /// <summary>   Gets the value for the given Text type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="elementTextTypeId">    Identifies the
        ///                                     <see cref="Dapper.Entities.ElementTextTypeEntity"/>. </param>
        /// <returns>   A String. </returns>
        public string Getter( int elementTextTypeId )
        {
            return this.ContainsKey( elementTextTypeId ) ? this[this._PrimaryKeyDictionary[elementTextTypeId]].Label : string.Empty;
        }

        /// <summary>   Set the specified Element value. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="elementTextTypeId">    Identifies the
        ///                                     <see cref="Dapper.Entities.ElementTextTypeEntity"/>. </param>
        /// <param name="value">                The value. </param>
        public void Setter( int elementTextTypeId, string value )
        {
            if ( this.ContainsKey( elementTextTypeId ) )
            {
                this[this._PrimaryKeyDictionary[elementTextTypeId]].Label = value;
            }
            else
            {
                this.Add( new ElementNomTextEntity() { ElementAutoId = ElementAutoId, ElementTextTypeId = elementTextTypeId, Label = value } );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int ElementAutoId { get; private set; }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;

using Dapper;

namespace isr.Dapper.Entities
{

    /// <summary>   Monitors the active UUT. At this time, each active UUT is associated with a single meter. </summary>
    /// <remarks>
    /// David, 2020-05-28. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class LotUutMonitor : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-04. </remarks>
        /// <param name="meter">   The <see cref="MeterEntity"/>. </param>
        public LotUutMonitor( PlatformMeter meter ) : base()
        {
            this.PlatformMeter = meter;
            this.ActiveUUTEntity = new UutEntity();
            this.MetaReadingsBindingList = new Std.BindingLists.InvokingBindingList<MetaReading>();
            // AddHandler Me.ActiveUUTEntity.PropertyChanged, AddressOf Me.ActiveUUTEntityPropertyChanged
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " METER IDENTITY "

        private PlatformMeter _PlatformMeter;

        /// <summary>   Gets or sets the platform meter. </summary>
        /// <value> The platform meter. </value>
        public PlatformMeter PlatformMeter
        {
            get => this._PlatformMeter;

            set {
                this._PlatformMeter = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>   Gets the meter number. </summary>
        /// <value> The meter number. </value>
        public int MeterNumber => this.PlatformMeter.MeterNumber;

        /// <summary>   Gets the identifier of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId => this.PlatformMeter.MeterId;

        #endregion

        #region " NUT's "

        /// <summary>   Adds a nut entity. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the uut automatic. </param>
        /// <param name="elementId">    Identifies the element. </param>
        /// <param name="nomTypeId">    Identifies the nom type. </param>
        /// <returns>   A Dapper.Entities.UutNutEntity. </returns>
        public static UutNutEntity AddNutEntity( System.Data.IDbConnection connection, int uutAutoId, int elementId, int nomTypeId )
        {
            UutNutEntity uutNut;
            uutNut = new UutNutEntity();
            var (Success, Details) = uutNut.TryObtainNut( connection, uutAutoId, elementId, nomTypeId );
            return !Success
                ? throw new InvalidOperationException( $"{nameof( UutNutEntity )} with '{nameof( UutNutEntity.UutAutoId )}' of {uutAutoId} should have been inserted; {Details}" )
                : uutNut;
        }

#if false
        /// <summary>   Adds a nut entities. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="elements">     The elements. </param>
        /// <param name="uut">          The uut. </param>
        /// <returns>   A Dapper.Entities.UutEntity. </returns>
        public static UutEntity AddNutEntities( System.Data.IDbConnection connection, ProductUniqueElementEntityCollection elements, UutEntity uut )
        {
            NutEntity nut;
            int expectedCount = 0;
            foreach ( ElementEntity element in elements )
            {
                foreach ( NomTypeEntity nomTypeEntity in element.NomTypes )
                {
                    nut = AddNutEntity( connection, uut.AutoId, element.AutoId, nomTypeEntity.Id ).NutEntity;
                    nut.InitializeNutReadings( uut, element );
                    _ = nut.FetchNutReadings( connection );
                    expectedCount += 1;
                }
            }

            if ( expectedCount != uut.FetchNuts( connection ) )
                throw new InvalidOperationException( $"{nameof( UutEntity.Nuts )} count {uut.Nuts.Count} should be {expectedCount}" );
            uut.InitializeNutReadingEntitiesCollection( elements );
            _ = uut.FetchNutReadings( connection );
            uut.UutState = (uut.Nuts?.Any()).GetValueOrDefault( false ) ? UutState.Ready : UutState.Undefined;
            foreach ( var currentNut in uut.Nuts )
            {
                nut = currentNut;
                if ( nut.NutReadings.Any() )
                {
                    uut.UutState = UutState.Measured;
                    break;
                }
            }

            return uut;
        }
#endif

        /// <summary>   Adds a nut entities. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <param name="uut">              The uut. </param>
        /// <returns>   A Dapper.Entities.UutEntity. </returns>
        public static UutEntity AddNutEntities( System.Data.IDbConnection connection, PlatformMeter platformMeter, UutEntity uut )
        {
            NutEntity nut;
            int expectedCount = 0;
            foreach ( ElementEntity element in platformMeter.Elements() )
            {
                foreach ( NomTypeEntity nomTypeEntity in element.NomTypes )
                {
                    nut = AddNutEntity( connection, uut.AutoId, element.AutoId, nomTypeEntity.Id ).NutEntity;
                    nut.InitializeNutReadings( uut, element );
                    _ = nut.FetchNutReadings( connection );
                    expectedCount += 1;
                }
            }

            if ( expectedCount != uut.FetchNuts( connection ) )
                throw new InvalidOperationException( $"{nameof( UutEntity.Nuts )} count {uut.Nuts.Count} should be {expectedCount}" );
            uut.InitializeNutReadingEntitiesCollection( platformMeter );
            _ = uut.FetchNutReadings( connection );
            uut.UutState = (uut.Nuts?.Any()).GetValueOrDefault( false ) ? UutState.Ready : UutState.Undefined;
            foreach ( var currentNut in uut.Nuts )
            {
                nut = currentNut;
                if ( nut.NutReadings.Any() )
                {
                    uut.UutState = UutState.Measured;
                    break;
                }
            }

            return uut;
        }

        #endregion

        #region " ACTIVE UUT "

        /// <summary>   Releases the active uut. </summary>
        /// <remarks>   David, 2020-07-08. </remarks>
        public void ReleaseActiveUut()
        {
            if ( this.ActiveUUTEntity is object )
            {
                // RemoveHandler Me.ActiveUUTEntity.PropertyChanged, AddressOf Me.ActiveUUTEntityPropertyChanged
                this.ActiveUUTEntity = null;
            }
        }

        /// <summary>   Prepare next uut. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="LotEntity"/>. </param>
        /// <param name="uutType">      Type of the uut. </param>
        public void PrepareNextUut( System.Data.IDbConnection connection, int lotAutoId, UutType uutType )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.PrepareNextUut( new TransactedConnection( connection, transaction ), lotAutoId, uutType );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.PrepareNextUut( transactedConnection, lotAutoId, uutType );
            }
        }

        /// <summary>   Prepare next uut. </summary>
        /// <remarks>
        /// David, 2020-07-11. <para>
        /// SQLite elapsed time: less than 20 ms </para><para>
        /// SQL Server elapsed time: Less than 10 ms.</para><para>
        /// First elapsed time: ~70 ms, which occurs before test starts.
        /// </para>
        /// </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="LotEntity"/>. </param>
        /// <param name="uutType">      Type of the uut. </param>
        [CLSCompliant( false )]
        public void PrepareNextUut( TransactedConnection connection, int lotAutoId, UutType uutType )
        {
            var lotUut = new LotUutEntity() { LotAutoId = lotAutoId };
            this.ActiveUUTEntity.UutState = UutState.Undefined;
            var (Success, Details) = lotUut.TryObtainUut( connection, lotAutoId, -1, ( int ) uutType );
            if ( Success )
            {
                this.PrepareUut( connection, lotUut.UutAutoId );
            }
            else
            {
                throw new InvalidOperationException( Details );
            }
        }

        /// <summary>
        /// Fetches the <see cref="LotUutMonitor.ActiveUUTEntity"/>, its <see cref="NutEntity"/>'s and
        /// <see cref="ProductSortEntity"/> with readings and traits.
        /// </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="UutEntity"/>. </param>
        protected void PrepareUut( System.Data.IDbConnection connection, int uutAutoId )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.PrepareUut( new TransactedConnection( connection, transaction ), uutAutoId );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.PrepareUut( transactedConnection, uutAutoId );
            }
        }

        /// <summary>
        /// Fetches the <see cref="LotUutMonitor.ActiveUUTEntity"/>, its <see cref="NutEntity"/>'s and
        /// <see cref="ProductSortEntity"/> with readings and traits.
        /// </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="UutEntity"/>. </param>
        private void PrepareUut( TransactedConnection connection, int uutAutoId )
        {
            this.ActiveUUTEntity.Clear();
            this.ActiveUUTEntity.ProductSortEntity = null;
            this.ActiveUUTEntity.AutoId = uutAutoId;
            if ( this.ActiveUUTEntity.FetchUsingKey( connection ) )
            {
                _ = this.ActiveUUTEntity.FetchTraits( connection );
                this.ActiveUUTEntity.Traits.UutTrait.MeterId = this.MeterId;
                _ = this.ActiveUUTEntity.Traits.Upsert( connection );
                _ = AddNutEntities( connection, this.PlatformMeter, this.ActiveUUTEntity );
                this.ActiveUUTEntity.ProductSortEntity = UutProductSortEntity.FetchSortEntity( connection, uutAutoId );
                this.ActiveUUTEntity.UutState = this.ActiveUUTEntity.ProductSortEntity?.IsClean() == true ? UutState.Complete : this.ActiveUUTEntity.UutState;
            }
            else
            {
                throw new InvalidOperationException( $"Failed fetching {nameof( UutEntity )} with {nameof( UutEntity.AutoId )} of {uutAutoId}" );
            }
        }

        /// <summary>   Gets or sets the active uut entity. </summary>
        /// <value> The active uut entity. </value>
        public UutEntity ActiveUUTEntity { get; private set; }

        /// <summary>   Gets or sets the exception. </summary>
        /// <value> The exception. </value>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Sorts the Active UUT and sets the <see cref="Dapper.Entities.UutEntity.UutState"/>.
        /// </summary>
        /// <remarks>   David, 2020-07-16. </remarks>
        /// <param name="binnings"> The <see cref="ProductUniqueBinningEntityCollection"/>. </param>
        /// <param name="sorts">    The <see cref="ProductUniqueProductSortEntityCollection"/>. </param>
        /// <returns>   An <see cref="Dapper.Entities.UutProductSortEntity"/>. </returns>
        public ProductSortEntity SortUut( ProductUniqueBinningEntityCollection binnings, ProductUniqueProductSortEntityCollection sorts )
        {
            var result = this.ActiveUUTEntity.UutState == UutState.Measured ? this.ActiveUUTEntity.DoSort( binnings, sorts ) : new ProductSortEntity();
            this.UpdateContinuousFailures( result );
            return result;
        }

        /// <summary>   Deletes the incomplete uut described by connection. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        [CLSCompliant( false )]
        public void DeleteIncompleteUut( TransactedConnection connection )
        {
            // remove any incomplete DUTs.
            if ( this.ActiveUUTEntity.UutNumber > 0 && this.ActiveUUTEntity.UutState != UutState.Complete )
            {
                _ = this.ActiveUUTEntity.Delete( connection );
            }
        }

        #endregion

        #region " CONTINUOUS FAILURE MANAGEMENT "

        /// <summary>   Updates the continuous failures described by productSortEntity. </summary>
        /// <remarks>   David, 2020-05-27. </remarks>
        /// <param name="productSortEntity">    The product sort entity. </param>
        public void UpdateContinuousFailures( ProductSortEntity productSortEntity )
        {
            if ( productSortEntity is null )
            {
            }
            // if sort is nothing, ignore as this represents an intermediate state of getting a new UUT
            else if ( Entity.EntityBase<IKeyForeignLabelNatural, ProductSortNub>.Equals( this.LastSortEntity, productSortEntity ) )
            {
                if ( this.ActiveUUTEntity.BucketBin != BucketBin.Good )
                {
                    this.ContinuousFailureCount += 1;
                    // assumes that the product sort traits were fetches when setting the product sort.
                    this.ContinuousFailureLimit = productSortEntity.ProductSortTraits.ProductSortTrait?.ContinuousFailureCountLimit;
                    this.ContinuousFailureAlert = this.ContinuousFailureLimit.HasValue && this.ContinuousFailureLimit.Value > 0 && this.ContinuousFailureLimit.Value <= this.ContinuousFailureCount;
                    if ( this._ContinuousFailureAlert )
                    {
                        var kvp = new KeyValuePair<string, int>( productSortEntity.Label, this.ContinuousFailureCount );
                        if ( this.ContinuousFailuresStack.Any() && string.Equals( this.ContinuousFailuresStack.Peek().Key, productSortEntity.Label ) )
                        {
                            // if this is the same sort as the previous continuous failure sort, pop the last entry to be replaced with a new one
                            _ = this.ContinuousFailuresStack.Pop();
                        }
                        else
                        {
                        }

                        this.ContinuousFailuresStack.Push( kvp );
                    }
                    this.NotifyPropertyChanged( nameof( LotUutMonitor.ContinuousFailureCount ) );
                }
                else
                {
                    this.ContinuousFailureAlert = false;
                }
            }
            else
            {
                // if a new sort, initialize the Continuous Failure Count and clear the alert
                this.ContinuousFailureAlert = false;
                this.ContinuousFailureCount = this.ActiveUUTEntity.BucketBin != BucketBin.Good ? 1 : 0;
            }

            if ( productSortEntity is object )
                this.LastSortEntity = productSortEntity;
        }

        /// <summary>   True to continuous failure alert. </summary>
        private bool _ContinuousFailureAlert;

        /// <summary>   Gets or sets the continuous failure alert. </summary>
        /// <value> The continuous failure alert. </value>
        public bool ContinuousFailureAlert
        {
            get => this._ContinuousFailureAlert;

            set {
                if ( value != this.ContinuousFailureAlert )
                {
                    this._ContinuousFailureAlert = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The last sort entity. </summary>
        private ProductSortEntity _LastSortEntity;

        /// <summary>   Gets or sets the last sort entity. </summary>
        /// <value> The last sort entity. </value>
        public ProductSortEntity LastSortEntity
        {
            get => this._LastSortEntity;

            set {
                if ( !Entity.EntityBase<IKeyForeignLabelNatural, ProductSortNub>.Equals( value, this.LastSortEntity ) )
                {
                    this._LastSortEntity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Number of continuous failures. </summary>
        private int _ContinuousFailureCount;

        /// <summary>   Gets or sets the number of continuous failures. </summary>
        /// <value> The number of continuous failures. </value>
        public int ContinuousFailureCount
        {
            get => this._ContinuousFailureCount;

            set {
                if ( value != this.ContinuousFailureCount )
                {
                    this._ContinuousFailureCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The continuous failure limit. </summary>
        private int? _ContinuousFailureLimit;

        /// <summary>   Gets or sets the continuous failure limit. </summary>
        /// <value> The continuous failure limit. </value>
        public int? ContinuousFailureLimit
        {
            get => this._ContinuousFailureLimit;

            set {
                if ( !Nullable.Equals( value, this.ContinuousFailureLimit ) )
                {
                    this._ContinuousFailureLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        /// <summary>   Clears the continuous failures. </summary>
        /// <remarks>   David, 2020-05-27. </remarks>
        public void ClearContinuousFailures()
        {
            this.ContinuousFailuresStack.Clear();
        }

        /// <summary>   Gets or sets a stack of continuous failures. </summary>
        /// <value> A stack of continuous failures. </value>
        public Stack<KeyValuePair<string, int>> ContinuousFailuresStack { get; private set; } = new Stack<KeyValuePair<string, int>>();

        #endregion

        #region " COMPLETED UUT INFO "

        /// <summary>   The bucket color. </summary>
        private int _BucketColor;

        /// <summary>   Gets or sets the bucket Color. </summary>
        /// <value> The bucket Color. </value>
        public int BucketColor
        {
            get => this._BucketColor;

            set {
                if ( value != this.BucketColor )
                {
                    this._BucketColor = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The completed uut number. </summary>
        private int _CompletedUutNumber;

        /// <summary>   Gets or sets the completed uut number. </summary>
        /// <value> The ordinal number. </value>
        public int CompletedUutNumber
        {
            get => this._CompletedUutNumber;

            set {
                if ( value != this.CompletedUutNumber )
                {
                    this._CompletedUutNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The sort label. </summary>
        private string _SortLabel;

        /// <summary>   Gets or sets the sort label. </summary>
        /// <value> The sort label. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string SortLabel
        {
            get => this._SortLabel;

            set {
                if ( !string.Equals( value, this.SortLabel ) )
                {
                    this._SortLabel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// A <see cref="isr.Std.BindingLists.InvokingBindingList{T}"/> of <see cref="MetaReading"/>.
        /// </summary>
        /// <value> A list of meta readings bindings. </value>
        public Std.BindingLists.InvokingBindingList<MetaReading> MetaReadingsBindingList { get; private set; }

        /// <summary>   Updates the readings described by the meta Readings. </summary>
        /// <remarks>   David, 2020-08-03. </remarks>
        /// <param name="metaReadings">  The meta readings. </param>
        public void UpdateReadings( IEnumerable<MetaReading> metaReadings )
        {
            this.MetaReadingsBindingList.RaiseListChangedEvents = false;
            this.MetaReadingsBindingList.Clear();
            foreach ( MetaReading metaReading in metaReadings )
                this.MetaReadingsBindingList.Add( metaReading );
            this.MetaReadingsBindingList.RaiseListChangedEvents = true;
            this.MetaReadingsBindingList.ResetBindings();
        }

        #endregion

        #region " MESSAGES "

        /// <summary>   Message describing the progress. </summary>
        private string _ProgressMessage;

        /// <summary>   Gets or sets a message describing the progress. </summary>
        /// <value> A message describing the progress. </value>
        public string ProgressMessage
        {
            get => this._ProgressMessage;

            set {
                if ( (value ?? "") != (this.ProgressMessage ?? "") )
                {
                    this._ProgressMessage = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }

    /// <summary>   Collection of <see cref="LotUutMonitor"/>'s keyed by the meter number. </summary>
    /// <remarks>   David, 2020-06-12. </remarks>
    public class LotUutMonitorCollection : System.Collections.ObjectModel.KeyedCollection<int, LotUutMonitor>
    {
        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override int GetKeyForItem( LotUutMonitor item )
        {
            return item.MeterNumber;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Lot-Uut builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class LotUutBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( LotUutNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = LotBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( LotNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = UutBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( UutNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( LotUutEntity.LotAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( LotUutEntity.UutAutoId );

        #region " SINGLETON "

        private static readonly Lazy<LotUutBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static LotUutBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Lot Uut Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "LotUut" )]
    public class LotUutNub : OneToManyNub, IOneToMany
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public LotUutNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new LotUutNub();
        }
    }

    /// <summary>   The Lot-Uut Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class LotUutEntity : EntityBase<IOneToMany, LotUutNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public LotUutEntity() : this( new LotUutNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotUutEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public LotUutEntity( IOneToMany cache, IOneToMany store ) : base( new LotUutNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public LotUutEntity( LotUutEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.LotUutBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new LotUutNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Lot-Uut interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int lotAutoId, int uutAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, lotAutoId, uutAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.LotAutoId, this.UutAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.LotAutoId, this.UutAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int lotAutoId, int uutAutoId )
        {
            return this.FetchUsingKey( connection, lotAutoId, uutAutoId );
        }

        /// <summary>
        /// Gets or sets the slim locker for managing thread save addition of DUT tests.
        /// </summary>
        /// <value> The locker. </value>
        protected System.Threading.ReaderWriterLockSlim SlimLocker { get; private set; } = new System.Threading.ReaderWriterLockSlim();

        /// <summary>   Try obtain uut. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutNumber">    . </param>
        /// <param name="uutTypeId">    Identifies the <see cref="Dapper.Entities.UutEntity"/> type. </param>
        /// <returns>   A Tuple. </returns>
        public (bool Success, string Details) TryObtainUut( System.Data.IDbConnection connection, int lotAutoId, int uutNumber, int uutTypeId )
        {
            this.SlimLocker.EnterReadLock();
            try
            {
                return this.TryObtainUutThis( connection, lotAutoId, uutNumber, uutTypeId );
            }
            catch
            {
                throw;
            }
            finally
            {
                this.SlimLocker.ExitReadLock();
            }
        }

        /// <summary>
        /// Attempts to retrieve an existing or insert a new <see cref="Dapper.Entities.UutEntity"/> associated
        /// with an existing <see cref="Dapper.Entities.LotEntity"/> with <paramref name="lotAutoId"/>.
        /// Specifying a negative
        /// <paramref name="uutNumber"/> adds a new Uut with the next Uut number for this Lot. Thereafter
        /// a <see cref="LotUutEntity"/> is updated or inserted.
        /// </summary>
        /// <remarks>
        /// Using this method assumes the UUT has a non-unique <see cref="UutEntity.UutNumber"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutNumber">    The UUT Number. </param>
        /// <param name="uutTypeId">    Identifies the <see cref="Dapper.Entities.UutEntity"/> type. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        private (bool Success, string Details) TryObtainUutThis( System.Data.IDbConnection connection, int lotAutoId, int uutNumber, int uutTypeId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( 0 < uutNumber )
            {
                this.UutEntity = FetchUut( connection, lotAutoId, uutNumber );
                if ( !this.UutEntity.IsClean() )
                {
                    result = (false, $"Failed fetching existing {nameof( Dapper.Entities.UutEntity )} with {nameof( Dapper.Entities.UutEntity.UutNumber )} of {uutNumber}");
                }
            }
            else
            {
                this.UutEntity = FetchLastUut( connection, lotAutoId );
                uutNumber = this.UutEntity.IsClean() ? this.UutEntity.UutNumber + 1 : 1;
                this.UutEntity = new UutEntity() { UutNumber = uutNumber, UutTypeId = uutTypeId };
                if ( !this.UutEntity.Insert( connection ) )
                {
                    result = (false, $"Failed inserting {nameof( Dapper.Entities.UutEntity )} with {nameof( Dapper.Entities.UutEntity.UutNumber )} of {uutNumber}");
                }
                else if ( this.UutEntity.AutoId == 0 )
                {
                    result = (false, $"Failed inserting {nameof( Dapper.Entities.UutEntity )} with {nameof( Dapper.Entities.UutEntity.UutNumber )} of {uutNumber}; Id set to {this.UutEntity.AutoId}");
                }
            }

            if ( result.Success )
            {
                this.LotAutoId = lotAutoId;
                this.UutAutoId = this.UutEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( LotUutEntity.UutEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( LotUutEntity )} for [{nameof( Dapper.Entities.LotEntity.AutoId )}, {nameof( Dapper.Entities.UutEntity.UutNumber )}] of [{lotAutoId},{uutNumber}]");
                }
            }

            return result;
        }

        /// <summary>
        /// Attempts to retrieve an existing or insert a new <see cref="LotUutEntity"/> from the given
        /// data. Specifying a negative <paramref name="uutNumber"/> adds a new Uut with the next Uut
        /// number for this Lot. Updates the <see cref="LotUutEntity.LotEntity"/> and
        /// <see cref="LotUutEntity.UutEntity"/>
        /// </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutNumber">    UUT Number. </param>
        /// <param name="uutTypeId">    Identifies the <see cref="Dapper.Entities.UutEntity"/> type. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int lotAutoId, int uutNumber, int uutTypeId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.LotEntity is null || this.LotEntity.AutoId != lotAutoId )
            {
                this.LotEntity = new LotEntity() { AutoId = lotAutoId };
                if ( !this.LotEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( Dapper.Entities.LotEntity )} with {nameof( Dapper.Entities.LotEntity.AutoId )} of {lotAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainUut( connection, lotAutoId, uutNumber, uutTypeId );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( LotUutEntity.LotEntity ) );
            return result;
        }

        /// <summary>
        /// Attempts to retrieve an existing or insert a new <see cref="LotUutEntity"/> from the given
        /// data. Specifying new <paramref name="lotLabel"/> adds this Lot;
        /// Specifying a negative
        /// <paramref name="uutNumber"/> adds a new Uut with the next Uut number for
        /// this Lot. Updates the <see cref="LotUutEntity.LotEntity"/> and
        /// <see cref="LotUutEntity.UutEntity"/>
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotLabel">     The lot label. </param>
        /// <param name="uutNumber">    The Uut number. </param>
        /// <param name="uutTypeId">    Identifies the <see cref="Dapper.Entities.UutEntity"/> type. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, string lotLabel, int uutNumber, int uutTypeId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.LotEntity is null || (this.LotEntity.LotNumber ?? "") != (lotLabel ?? "") )
            {
                this.LotEntity = new LotEntity() { Label = lotLabel };
                if ( !this.LotEntity.Obtain( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( Dapper.Entities.LotEntity )} with {nameof( Dapper.Entities.LotEntity.Label )} of {lotLabel}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtain( connection, this.LotEntity.AutoId, uutNumber, uutTypeId );
            }

            return result;
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int lotAutoId, int uutAutoId )
        {
            return connection.Delete( new LotUutNub() { PrimaryId = lotAutoId, SecondaryId = uutAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Lot-Uut entities. </summary>
        /// <value> The Lot-Uut entities. </value>
        public IEnumerable<LotUutEntity> LotUuts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotUutEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<LotUutNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.LotUuts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( LotUutEntity.LotUuts ) );
            return this.LotUuts?.Any() == true ? this.LotUuts.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotUutEntity> Populate( IEnumerable<LotUutNub> nubs )
        {
            var l = new List<LotUutEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( LotUutNub nub in nubs )
                    l.Add( new LotUutEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<LotUutEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<LotUutEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new LotUutNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new LotUutEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Uut entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotUutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotUutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotUutEntity> FetchEntities( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotUutBuilder.TableName}] WHERE {nameof( LotUutNub.PrimaryId )} = @Id", new { Id = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<LotUutNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Uut. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The total number of entities by Uut. </returns>
        public static int CountEntitiesByUut( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotUutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotUutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Uuts in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Uuts in this
        /// collection.
        /// </returns>
        public static IEnumerable<LotUutEntity> FetchEntitiesByUut( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotUutBuilder.TableName}] WHERE {nameof( LotUutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<LotUutNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int lotAutoId, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{LotUutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotUutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotUutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<LotUutNub> FetchNubs( System.Data.IDbConnection connection, int lotAutoId, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{LotUutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( LotUutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            _ = sqlBuilder.Where( $"{nameof( LotUutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<LotUutNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Lot Uut exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int lotAutoId, int uutAutoId )
        {
            return 1 == CountEntities( connection, lotAutoId, uutAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Lot entity. </summary>
        /// <value> The Lot entity. </value>
        public LotEntity LotEntity { get; private set; }

        /// <summary>   Fetches Lot Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Lot Entity. </returns>
        public LotEntity FetchLotEntity( System.Data.IDbConnection connection )
        {
            var entity = new LotEntity();
            _ = entity.FetchUsingKey( connection, this.LotAutoId );
            this.LotEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Lots associated with the specified <paramref name="uutAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The total number of Lots. </returns>
        public static int CountLots( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.LotUutBuilder.TableName}] WHERE {nameof( LotUutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Lots associated with the specified <paramref name="uutAutoId"/>; expected a
        /// single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Lots in this collection.
        /// </returns>
        public static IEnumerable<UutEntity> FetchLots( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotUutBuilder.TableName}] WHERE {nameof( LotUutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<UutEntity>();
            foreach ( LotUutNub nub in connection.Query<LotUutNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new LotUutEntity( nub );
                l.Add( entity.FetchUutEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Lots associated with the specified <paramref name="uutAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteLots( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.LotUutBuilder.TableName}] WHERE {nameof( LotUutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = uutAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the Uut entity. </summary>
        /// <value> The Uut entity. </value>
        public UutEntity UutEntity { get; private set; }

        /// <summary>   Fetches a Uut Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Uut Entity. </returns>
        public UutEntity FetchUutEntity( System.Data.IDbConnection connection )
        {
            var entity = new UutEntity();
            _ = entity.FetchUsingKey( connection, this.UutAutoId );
            this.UutEntity = entity;
            return entity;
        }

        /// <summary>   Count uuts. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   The total number of uuts. </returns>
        public static int CountUuts( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.LotUutBuilder.TableName}] WHERE {nameof( LotUutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Uuts in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Uuts in this collection.
        /// </returns>
        public static IEnumerable<UutEntity> FetchUuts( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.LotUutBuilder.TableName}] WHERE {nameof( LotUutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<UutEntity>();
            foreach ( LotUutNub nub in connection.Query<LotUutNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new LotUutEntity( nub );
                l.Add( entity.FetchUutEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Uut related to the specified Lot. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteUuts( System.Data.IDbConnection connection, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.LotUutBuilder.TableName}] WHERE {nameof( LotUutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = lotAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the ordered uuts in this collection. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered uuts in this collection.
        /// </returns>
        public static IEnumerable<UutEntity> FetchOrderedUuts( System.Data.IDbConnection connection, string selectQuery, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = lotAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return UutEntity.Populate( connection.Query<UutNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered uuts in this collection. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered uuts in this collection.
        /// </returns>
        public static IEnumerable<UutEntity> FetchOrderedUuts( System.Data.IDbConnection connection, int lotAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [LotUut] on [LotUut].SecondaryId = [Uut].AutoId where [LotUut].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{UutBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{UutBuilder.TableName}] Inner Join [{LotUutBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.LotUutBuilder.TableName}].{nameof( LotUutNub.SecondaryId )} = [{isr.Dapper.Entities.UutBuilder.TableName}].{nameof( UutNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.LotUutBuilder.TableName}].{nameof( LotUutNub.PrimaryId )} = @Id" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.UutBuilder.TableName}].{nameof( UutNub.Amount )} ASC; " );
            return FetchOrderedUuts( connection, queryBuilder.ToString(), lotAutoId );
        }

        /// <summary>
        /// Fetches the first Uut from the ordered list; this would be the last ordinal number entity if
        /// using descending order.
        /// </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the last Uuts in this collection.
        /// </returns>
        public static UutEntity FetchFirstUut( System.Data.IDbConnection connection, string selectQuery, int lotAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery, new { Id = lotAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<UutNub>( template.RawSql, template.Parameters ).FirstOrDefault();
            return nub is null ? new UutEntity() : new UutEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches the last Uut in this collection. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the last Uuts in this collection.
        /// </returns>
        public static UutEntity FetchLastUut( System.Data.IDbConnection connection, int lotAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [LotUut] on [LotUut].SecondaryId = [Uut].AutoId where [LotUut].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{UutBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{UutBuilder.TableName}] Inner Join [{LotUutBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.LotUutBuilder.TableName}].{nameof( LotUutNub.SecondaryId )} = [{isr.Dapper.Entities.UutBuilder.TableName}].{nameof( UutNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.LotUutBuilder.TableName}].{nameof( LotUutNub.PrimaryId )} = @Id" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.UutBuilder.TableName}].{nameof( UutNub.Amount )} DESC; " );
            return FetchFirstUut( connection, queryBuilder.ToString(), lotAutoId );
        }

        /// <summary>   Fetches an Uut. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutNumber">    The UUT Number. </param>
        /// <returns>   The Uut. </returns>
        public static UutEntity FetchUut( System.Data.IDbConnection connection, string selectQuery, int lotAutoId, int uutNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = lotAutoId, UutNumber = uutNumber } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<UutNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new UutEntity() : new UutEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches an Uut. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="lotAutoId">    Identifies the <see cref="Dapper.Entities.LotEntity"/>. </param>
        /// <param name="uutNumber">    The UUT Number. </param>
        /// <returns>   The Uut. </returns>
        public static UutEntity FetchUut( System.Data.IDbConnection connection, int lotAutoId, int uutNumber )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [LotUut] on [LotUut].SecondaryId = [Uut].AutoId where [LotUut].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{UutBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{UutBuilder.TableName}] Inner Join [{LotUutBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.LotUutBuilder.TableName}].{nameof( LotUutNub.SecondaryId )} = [{isr.Dapper.Entities.UutBuilder.TableName}].{nameof( UutNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.LotUutBuilder.TableName}].{nameof( LotUutNub.PrimaryId )} = @Id AND [{isr.Dapper.Entities.UutBuilder.TableName}].{nameof( UutNub.Amount )} = @UutNumber); " );
            return FetchUut( connection, queryBuilder.ToString(), lotAutoId, uutNumber );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( LotUutEntity.LotAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.LotEntity"/> </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.LotEntity"/> </value>
        public int LotAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( LotUutEntity.UutAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Uut. </summary>
        /// <value> Identifies the Uut. </value>
        public int UutAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

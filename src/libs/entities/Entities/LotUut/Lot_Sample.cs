using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   A lot entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class LotEntity
    {

        /// <summary>   Gets or sets the <see cref="LotYieldTraitEntityCollection"/>. </summary>
        /// <value> The <see cref="LotYieldTraitEntityCollection"/>. </value>
        public LotYieldTraitEntitiesCollection YieldTraits { get; private set; } = new LotYieldTraitEntitiesCollection();

        /// <summary>   Gets or sets the <see cref="LotSampleTraitEntityCollection"/>. </summary>
        /// <value> The <see cref="LotSampleTraitEntityCollection"/>. </value>
        public LotSampleTraitEntitiesCollection SampleTraits { get; private set; } = new();

#if false
        /// <summary>   Initializes the sample yield traits. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="meters">    The meters. </param>
        /// <param name="elements">  The elements. </param>
        public void InitializeSampleYieldTraits( IEnumerable<MeterEntity> meters, IEnumerable<ElementEntity> elements )
        {
            this.ClearSampleYieldTraits();
            LotYieldTraitEntityCollection lotYieldTraits;
            LotSampleTraitEntityCollection lotSampleTraits;
            foreach ( MeterEntity meter in meters )
            {
                lotYieldTraits = new LotYieldTraitEntityCollection( this, meter );
                this.YieldTraits.Add( lotYieldTraits );
                this.NutReadingEntitiesCollection.AddYieldTrait( lotYieldTraits );
                foreach ( ElementEntity element in elements )
                {
                    foreach ( NomTypeEntity nomType in element.NomTypes )
                    {
                        lotSampleTraits = new LotSampleTraitEntityCollection( this, element, nomType.Id, meter );
                        this.SampleTraits.Add( lotSampleTraits );
                        this.NutReadingEntitiesCollection.AddSampleTrait( lotSampleTraits );
                    }
                }
            }
        }

        /// <summary>   Initializes the sample yield traits. </summary>
        /// <remarks>   David, 2021-05-14. </remarks>
        /// <param name="element">          The element. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        public void InitializeSampleYieldTraits( ElementEntity element, PlatformMeter platformMeter )
        {
            LotYieldTraitEntityCollection lotYieldTraits;
            LotSampleTraitEntityCollection lotSampleTraits;
            lotYieldTraits = new LotYieldTraitEntityCollection( this.AutoId, platformMeter.MeterId );
            this.YieldTraits.Add( lotYieldTraits );
            this.NutReadingEntitiesCollection.AddYieldTrait( lotYieldTraits );
            lotSampleTraits = new LotSampleTraitEntityCollection( this, element, ( int ) platformMeter.DefaultNominalType, platformMeter.MeterId );
            this.SampleTraits.Add( lotSampleTraits );
            this.NutReadingEntitiesCollection.AddSampleTrait( lotSampleTraits );
        }

#endif

#if false
        public void FetchSampleTraits( System.Data.IDbConnection connection, IEnumerable<PlatformMeter> platformMeters, IEnumerable<ElementEntity> elements )
        {
            this.SampleTraits.Clear();
            foreach ( PlatformMeter platformMeter in platformMeters )
            {
                this.FetchSampleTraits( connection, platformMeter, elements );
            }
        }

        public void FetchSampleTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter , IEnumerable<ElementEntity> elements )
        {
            // TO_DO: Populate by Platform Meter using the platform meter elements and nominal types for that platform meter.
            if ( !SampleTraitTypeEntity.IsEnumerated() )
                _ = SampleTraitTypeEntity.TryFetchAll( connection );
            foreach ( ElementEntity element in elements )
            {
                foreach ( NomTypeEntity nomType in element.NomTypes )
                {
                    MeterElementNomTypeSelector selector = new ( platformMeter.MeterId, element.AutoId, nomType.Id );
                    if (!this.SampleTraits.Contains( selector ))
                    {
                        LotSampleTraitEntityCollection lotSampleTraits;
                        lotSampleTraits = new LotSampleTraitEntityCollection( this, element, nomType.Id, platformMeter.MeterId );
                        this.SampleTraits.Add( lotSampleTraits );
                    }
                    this.FetchSampleTraits( connection, platformMeter, element.AutoId, nomType.Id );
                }
            }
        }
#endif

        /// <summary>   Fetches sample traits for all meters. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeters">   The platform in meters. </param>
        public void FetchSampleTraits( System.Data.IDbConnection connection, IEnumerable<PlatformMeter> platformMeters )
        {
            this.SampleTraits.Clear();
            foreach ( PlatformMeter platformMeter in platformMeters )
            {
                this.FetchSampleTraits( connection, platformMeter );
            }
        }

        /// <summary>   Fetches sample traits for the specified meter. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        private void FetchSampleTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter )
        {
            // TO_DO: Populate by Platform Meter using the platform meter elements and nominal types for that platform meter.
            if ( !SampleTraitTypeEntity.IsEnumerated() )
                _ = SampleTraitTypeEntity.TryFetchAll( connection );
            foreach ( ElementEntity element in platformMeter.Elements() )
            {
                foreach ( NomTypeEntity nomType in element.NomTypes )
                {
                    MeterElementNomTypeSelector selector = new( platformMeter.MeterId, element.AutoId, nomType.Id );
                    if ( !this.SampleTraits.Contains( selector ) )
                    {
                        LotSampleTraitEntityCollection lotSampleTraits;
                        lotSampleTraits = new LotSampleTraitEntityCollection( this, element, nomType.Id, platformMeter.MeterId );
                        this.SampleTraits.Add( lotSampleTraits );
                    }
                    this.FetchSampleTraits( connection, platformMeter, element.AutoId, nomType.Id );
                }
            }
        }

        /// <summary>   Fetches sample traits for the specified meter, element, and nominal type. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <param name="elementAutoId">    Identifies the element automatic. </param>
        /// <param name="nomTypeId">        Identifies the nom type. </param>
        private void FetchSampleTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter, int elementAutoId, int nomTypeId )
        {
            if ( !SampleTraitTypeEntity.IsEnumerated() )
                _ = SampleTraitTypeEntity.TryFetchAll( connection );
            LotSampleTraitEntityCollection lotSampleTraits;
            lotSampleTraits = this.SampleTraits[new MeterElementNomTypeSelector( platformMeter.MeterId, elementAutoId, nomTypeId )];
            lotSampleTraits.Populate( LotSampleEntity.FetchOrderedSampleTraits( connection, this.AutoId, platformMeter.MeterId, elementAutoId ) );
            // It is assumed that this will be the one and only place where the traits will be added.
            this.NutReadingEntitiesCollection.AddSampleTrait( lotSampleTraits );
        }

        /// <summary>   Fetches sample traits for the specified meter. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        public void FetchPrimaryElementSampleTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter )
        {
            if ( !SampleTraitTypeEntity.IsEnumerated() )
                _ = SampleTraitTypeEntity.TryFetchAll( connection );
            MeterElementNomTypeSelector selector = new( platformMeter );
            LotSampleTraitEntityCollection lotSampleTraits;
            if ( !this.SampleTraits.Contains( selector ) )
            {
                lotSampleTraits = new LotSampleTraitEntityCollection( this, platformMeter.PrimaryElement, ( int ) platformMeter.PrimaryNominalType, platformMeter.MeterId );
                this.SampleTraits.Add( lotSampleTraits );
            }
            lotSampleTraits = this.SampleTraits[new MeterElementNomTypeSelector( platformMeter )];
            lotSampleTraits.Populate( LotSampleEntity.FetchOrderedSampleTraits( connection, this.AutoId, platformMeter.MeterId, platformMeter.PrimaryElementId ) );
            // It is assumed that this will be the one and only place where the traits will be added.
            this.NutReadingEntitiesCollection.AddSampleTrait( lotSampleTraits );
        }

        /// <summary>   Fetches yield traits. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeters">   The platform in meters. </param>
        public void FetchYieldTraits( System.Data.IDbConnection connection, IEnumerable<PlatformMeter> platformMeters )
        {
            this.YieldTraits.Clear();
            foreach ( PlatformMeter platformMeter in platformMeters )
            {
                this.FetchYieldTraits( connection, platformMeter );
            }
        }

        /// <summary>   Fetches yield traits. </summary>
        /// <remarks>   David, 2021-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        private void FetchYieldTraits( System.Data.IDbConnection connection, PlatformMeter platformMeter )
        {
            if ( !YieldTraitTypeEntity.IsEnumerated() )
                _ = YieldTraitTypeEntity.TryFetchAll( connection );
            LotYieldTraitEntityCollection lotYieldTraits = new( this.AutoId, platformMeter.MeterId );
            lotYieldTraits.Populate( LotYieldEntity.FetchOrderedYieldTraits( connection, this.AutoId, platformMeter.MeterId ) );
            this.YieldTraits.Add( lotYieldTraits );
            // Note: It is assumed that this will be the one and only place where the traits will be added.
            this.NutReadingEntitiesCollection.AddYieldTrait( lotYieldTraits );
        }


    }

    /// <summary>
    /// A collection of <see cref="LotYieldTraitEntityCollection"/> keyed by meter id.
    /// </summary>
    /// <remarks>   David, 2020-07-03. </remarks>
    public class LotYieldTraitEntitiesCollection : KeyedCollection<int, LotYieldTraitEntityCollection>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override int GetKeyForItem( LotYieldTraitEntityCollection item )
        {
            return item.MeterId;
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        [CLSCompliant( false )]
        public void Upsert( TransactedConnection connection )
        {
            foreach ( LotYieldTraitEntityCollection item in this )
                _ = item.Upsert( connection );
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        public void Upsert( System.Data.IDbConnection connection )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.Upsert( new TransactedConnection( connection, transaction ) );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.Upsert( transactedConnection );
            }
        }
    }

    /// <summary>   A collection of <see cref="LotSampleTraitEntityCollection"/> keyed by meter element and nominal type. </summary>
    /// <remarks>   David, 2020-07-03. </remarks>
    public class LotSampleTraitEntitiesCollection : KeyedCollection<MeterElementNomTypeSelector, LotSampleTraitEntityCollection>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override MeterElementNomTypeSelector GetKeyForItem( LotSampleTraitEntityCollection item )
        {
            return item.SampleTrait.Selector;
        }

        /// <summary>   Evaluates all elements. </summary>
        /// <remarks>   David, 2020-07-03. </remarks>
        /// <param name="outliersCalculatorMethod"> The outliers calculator method. </param>
        public void Evaluate( OutliersCalculatorMethod outliersCalculatorMethod )
        {
            foreach ( LotSampleTraitEntityCollection item in this )
            {
                item.SampleTrait.Evaluate( outliersCalculatorMethod );
                foreach ( var sampleTraitEntity in item )
                {
                    sampleTraitEntity.NomTypeId = ( int ) item.SampleTrait.NomType;
                }
            }
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        [CLSCompliant( false )]
        public void Upsert( TransactedConnection connection )
        {
            foreach ( LotSampleTraitEntityCollection item in this )
                _ = item.Upsert( connection );
        }

        /// <summary>   Updates or inserts entities using the given connection. </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="connection">   The connection. </param>
        public void Upsert( System.Data.IDbConnection connection )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                        connection.Open();
                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        this.Upsert( new TransactedConnection( connection, transaction ) );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                        connection.Close();
                }
            }
            else
            {
                this.Upsert( transactedConnection );
            }
        }
    }
}

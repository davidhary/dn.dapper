using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using FastEnums;

using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Product Sort builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ProductSortBuilder : KeyForeignLabelNaturalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ProductSortNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the foreign table . </summary>
        /// <value> The name of the foreign table. </value>
        public override string ForeignTableName { get; set; } = ProductBuilder.TableName;

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <value> The name of the foreign table key. </value>
        public override string ForeignTableKeyName { get; set; } = nameof( ProductNub.AutoId );

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public override int LabelFieldSize { get; set; } = 50;

        /// <summary>   Inserts a product sorts. The first sort label must be 'Good'. </summary>
        /// <remarks>   David, 2020-05-22. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="labels">           The labels. </param>
        /// <returns>   An Integer. </returns>
        public int InsertSortLables( System.Data.IDbConnection connection, int productAutoId, IEnumerable<string> labels )
        {
            var entities = new List<ProductSortNub>();
            int sortOrder = 0;
            foreach ( string label in labels )
            {
                sortOrder += 1;
                entities.Add( new ProductSortNub() { ForeignId = productAutoId, Label = label, Amount = sortOrder } );
            }

            return connection.InsertIgnore( this.TableNameThis, entities );
        }

        /// <summary>   Inserts a sort labels. </summary>
        /// <remarks>   David, 2020-05-22. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="type">             The type. </param>
        /// <returns>   An Integer. </returns>
        public int InsertSortLabels( System.Data.IDbConnection connection, int productAutoId, Type type )
        {
            return this.InsertSortLables( connection, productAutoId, type.Descriptions() );
        }



        /// <summary>   Inserts a the sorts from the <see cref="ProductSort">Enum type</see>/&gt;. </summary>
        /// <remarks>   David, 2020-05-22. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public int InsertDefaultProductSortRecords( System.Data.IDbConnection connection, int productAutoId )
        {
            return this.InsertSortLabels( connection, productAutoId, typeof( ProductSort ) );
        }

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( ProductSortEntity.AutoId );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( ProductSortEntity.ProductAutoId );

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public override string LabelFieldName { get; set; } = nameof( ProductSortEntity.SortLabel );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( ProductSortEntity.Amount );

        #region " SINGLETON "

        private static readonly Lazy<ProductSortBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ProductSortBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the ProductSort table based on the
    /// <see cref="IKeyForeignLabelNatural">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "ProductSort" )]
    public class ProductSortNub : KeyForeignLabelNaturalNub, IKeyForeignLabelNatural
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductSortNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignLabelNatural CreateNew()
        {
            return new ProductSortNub();
        }
    }

    /// <summary>   The ProductSort Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public partial class ProductSortEntity : EntityBase<IKeyForeignLabelNatural, ProductSortNub>, IKeyForeignLabelNatural
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductSortEntity() : this( new ProductSortNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductSortEntity( IKeyForeignLabelNatural value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ProductSortEntity( IKeyForeignLabelNatural cache, IKeyForeignLabelNatural store ) : base( new ProductSortNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductSortEntity( ProductSortEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ProductSortBuilder.TableName, nameof( IKeyForeignLabelNatural ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyForeignLabelNatural CreateNew()
        {
            return new ProductSortNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyForeignLabelNatural CreateCopy()
        {
            var destination = this.CreateNew();
            KeyForeignLabelNaturalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyForeignLabelNatural value )
        {
            KeyForeignLabelNaturalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The ProductSort interface. </param>
        public override void UpdateCache( IKeyForeignLabelNatural value )
        {
            // first make the copy to notify of any property change.
            KeyForeignLabelNaturalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The ProductSort table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyForeignLabelNatural>( key ) : connection.Get<ProductSortNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="autoId">       Identifies the ProductSort. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int autoId )
        {
            return this.FetchUsingKey( connection, autoId );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyForeignLabelNatural entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.AutoId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new ProductSortNub() { AutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the ProductSort entities. </summary>
        /// <value> The ProductSort entities. </value>
        public IEnumerable<ProductSortEntity> ProductSorts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductSortEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyForeignLabelNatural>() ) : Populate( connection.GetAll<ProductSortNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ProductSorts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ProductSortEntity.ProductSorts ) );
            return this.ProductSorts?.Any() == true ? this.ProductSorts.Count() : 0;
        }

        /// <summary>   Count ProductAttributeValues. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.ProductSortBuilder.TableName}] WHERE {nameof( ProductSortNub.ForeignId )} = @ForeignId", new { ForeignId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        private static IEnumerable<ProductSortNub> FetchNubsInternal( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            // SELECT * FROM [ProductSort] WHERE ForeignId = 1
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductSortBuilder.TableName}] WHERE {nameof( ProductSortNub.ForeignId )} = @ForeignId", new { ForeignId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductSortNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductSortEntity> FetchEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            IEnumerable<ProductSortNub> nubs = ProductSortEntity.FetchNubsInternal( connection, productAutoId );
            return Populate( nubs );
        }

        /// <summary>
        /// Fetches Product Traits by Product number and Product Trait Product Trait Range Value type;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductSortNub> FetchNubs( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductSortBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductSortNub.ForeignId )} = @ForeignId", new { ForeignId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductSortNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of ProductSort entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> The ProductSort nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductSortEntity> Populate( IEnumerable<ProductSortNub> nubs )
        {
            var l = new List<ProductSortEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ProductSortNub nub in nubs )
                    l.Add( new ProductSortEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of ProductSort entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   The ProductSort interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductSortEntity> Populate( IEnumerable<IKeyForeignLabelNatural> interfaces )
        {
            var l = new List<ProductSortEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ProductSortNub();
                foreach ( IKeyForeignLabelNatural iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ProductSortEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the ProductSort. </summary>
        /// <value> Identifies the ProductSort. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the foreign entity. </summary>
        /// <value> Identifies the foreign entity. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductSortEntity.ProductAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="ProductEntity"/>. </summary>
        /// <value> Identifies the <see cref="ProductEntity"/>. </value>
        public int ProductAutoId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }

        /// <summary>   Gets or sets the element Label. </summary>
        /// <value> The Label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ElementEntity.ElementLabel ) );
                }
            }
        }

        /// <summary>   Gets or sets the sort label. </summary>
        /// <value> The sort label. </value>
        public string SortLabel
        {
            get => this.Label;

            set => this.Label = value;
        }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        public int Amount
        {
            get => this.ICache.Amount;

            set {
                if ( !object.Equals( ( object ) this.Amount, ( object ) value ) )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductSortEntity.SortOrder ) );
                }
            }
        }

        /// <summary>   Gets or sets the sort order. </summary>
        /// <value> The sort order. </value>
        public int SortOrder
        {
            get => this.Amount;

            set => this.Amount = value;
        }

        #endregion

    }

    /// <summary>   Collection of ProductSort entities. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class ProductSortEntityCollection : EntityKeyedCollection<int, IKeyForeignLabelNatural, ProductSortNub, ProductSortEntity>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public ProductSortEntityCollection() : base()
        {
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified product.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The product from which to extract the key. </param>
        /// <returns>   The key for the specified product. </returns>
        protected override int GetKeyForItem( ProductSortEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.AutoId;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( ProductSortEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        /// <returns>   An int. </returns>
        public int Populate( IEnumerable<ProductSortEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ProductSortEntity entity in entities )
                    this.Add( entity );
            }

            return this.Any() ? this.Count : 0;
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inserts or updates all entities using the given connection including the traits.
        /// </summary>
        /// <remarks>
        /// David, 2020-05-13. Entities that failed to save are enumerated in
        /// <see cref="P:isr.Dapper.Entity.EntityKeyedCollection`4.UnsavedKeys" />
        /// </remarks>
        /// <param name="transcactedConnection">    The <see cref="T:Dapper.TransactedConnection" />. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [CLSCompliant( false )]
        public override bool Upsert( TransactedConnection transcactedConnection )
        {
            this.ClearUnsavedKeys();
            // Dictionary is instantiated only after the collection has values.
            if ( !this.Any() )
                return true;
            foreach ( ProductSortEntity productSort in this )
            {
                if ( productSort.Upsert( transcactedConnection ) )
                {
                    _ = productSort.ProductSortTraits.Upsert( transcactedConnection );
                }
                else
                {
                    this.AddUnsavedKey( productSort.AutoId );
                }
            }
            // success if no unsaved keys
            return !this.UnsavedKeys.Any();
        }
    }

    /// <summary>
    /// Collection of productSort entities uniquely associated with a product; this is also a UUT set
    /// of sorts.
    /// </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class ProductUniqueProductSortEntityCollection : ProductSortEntityCollection
    {

        #region " CONSTUCTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public ProductUniqueProductSortEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<int, string>();
            this._PrimaryKeyDictionary = new Dictionary<string, int>();
            this._SortedSortLabelDix = new SortedDictionary<int, string>();
            this._SortedSortEntityDix = new SortedDictionary<int, ProductSortEntity>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        public ProductUniqueProductSortEntityCollection( int productAutoId ) : this()
        {
            this.ProductAutoId = productAutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<int, string> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<string, int> _PrimaryKeyDictionary;

        /// <summary>   The dictionary of sort labels ordered by the sort order. </summary>
        private readonly SortedDictionary<int, string> _SortedSortLabelDix;

        /// <summary>   The dictionary of sort entities ordered by the sort order. </summary>
        private readonly SortedDictionary<int, ProductSortEntity> _SortedSortEntityDix;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( ProductSortEntity entity )
        {
            base.Add( entity );
            this._SortedSortLabelDix.Add( entity.SortOrder, entity.SortLabel );
            this._SortedSortEntityDix.Add( entity.SortOrder, entity );
            this._PrimaryKeyDictionary.Add( entity.Label, entity.AutoId );
            this._UniqueIndexDictionary.Add( entity.AutoId, entity.Label );
        }

        /// <summary>
        /// Removes all productSorts from the
        /// <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Populates entities fetched for the <see cref="ProductAutoId"/>. </summary>
        /// <remarks>   David, 2020-05-22. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public int Populate( System.Data.IDbConnection connection )
        {
            return this.Populate( ProductSortEntity.FetchEntities( connection, this.ProductAutoId ) );
        }

        #endregion

        #region " PRODUCT SORT "

        /// <summary>   Selects an productSort from the collection. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="sortLabel">    The sort label. </param>
        /// <returns>   An ProductSortEntity. </returns>
        public ProductSortEntity ProductSort( string sortLabel )
        {
            int id = this._PrimaryKeyDictionary[sortLabel];
            return this.Contains( id ) ? this[id] : new ProductSortEntity();
        }

        /// <summary>   Product sort label. </summary>
        /// <remarks>   David, 2020-05-22. </remarks>
        /// <param name="productSortAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/> sort. </param>
        /// <returns>   A String. </returns>
        public string ProductSortLabel( int productSortAutoId )
        {
            return this._UniqueIndexDictionary.ContainsKey( productSortAutoId ) ? this._UniqueIndexDictionary[productSortAutoId] : string.Empty;
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ProductEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </value>
        public int ProductAutoId { get; private set; }

        /// <summary>   Gets the order sorts. </summary>
        /// <value> The order sorts. </value>
        public IEnumerable<ProductSortEntity> OrderedSorts
        {
            get {
                var result = new List<ProductSortEntity>();
                foreach ( string sortLabel in this._SortedSortLabelDix.Values )
                    result.Add( this.ProductSort( sortLabel ) );
                return result;
            }
        }

        #endregion

        #region " BUCKETS "

        /// <summary>   Parse sort description bucket bin. </summary>
        /// <remarks>   David, 2020-07-04. </remarks>
        /// <param name="sortLabel">    The sort label. </param>
        /// <returns>   A BucketBin. </returns>
        public BucketBin SelectBucketBin1( string sortLabel )
        {
            return ( BucketBin ) this.ProductSort( sortLabel ).ProductSortTraits.ProductSortTrait.BucketBin.GetValueOrDefault( 0 );
        }

        #endregion

    }

    /// <summary>   Values that represent a product Sorts. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public enum ProductSort
    {
        /// <summary>   An enum constant representing the good option. </summary>
        [Description( "Good" )] Good = 0,

        /// <summary>   An enum constant representing the resistor guard failed option. </summary>
        [Description( "R-Guard" )]
        ResistorGuardFailed = 1,

        /// <summary>   An enum constant representing the compound resistor guard failed option. </summary>
        [Description( "D-Guard" )]
        CompoundResistorGuardFailed = 2,

        /// <summary>   An enum constant representing the computed value guard failed option. </summary>
        [Description( "M-Guard" )]
        ComputedValueGuardFailed = 3,

        /// <summary>   An enum constant representing the reading overflow option. </summary>
        [Description( "Overflow" )]
        ReadingOverflow = 4,

        /// <summary>   An enum constant representing the reading failed option. </summary>
        [Description( "Failed" )]
        ReadingFailed = 5,

        /// <summary>   An enum constant representing the nameless option. </summary>
        [Description( "Not Sorted" )]
        Nameless = 30
    }
}

﻿
using System.Linq;

namespace isr.Dapper.Entities
{
    /// <summary>   A product entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class ProductEntity
    {

        /// <summary>
        /// Gets or sets the <see cref=" ProductUniqueTextEntityCollection">Product Nom Text values</see>.
        /// </summary>
        /// <value> The Product Text trait values. </value>
        public ProductUniqueTextEntityCollection Texts { get; private set; }

        /// <summary>   Initializes the nominal texts. </summary>
        /// <remarks>   David, 2020-07-06. </remarks>
        public void InitializeTexts()
        {
            this.Texts = new ProductUniqueTextEntityCollection( this );
        }

        /// <summary>
        /// Fetches <see cref="ProductEntity.Texts"/> <see cref="ProductUniqueTextEntityCollection"/>.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of specifications. </returns>
        public int FetchTexts( System.Data.IDbConnection connection )
        {
            if ( !ProductTextTypeEntity.IsEnumerated() )
                _ = ProductTextTypeEntity.TryFetchAll( connection );
            if ( !(this.Texts?.Any()).GetValueOrDefault( false ) )
                this.InitializeTexts();
            this.Texts.Populate( ProductTextEntity.FetchEntities( connection, this.AutoId ) );
            return this.Texts?.Any() == true ? this.Texts.Count : 0;
        }

        /// <summary>   Select scan list. </summary>
        /// <remarks>   David, 2020-07-06. </remarks>
        /// <param name="productNumber">    Name of the product. </param>
        /// <returns>   A String. </returns>
        public static string SelectScanList( string productNumber )
        {
            switch ( productNumber ?? "" )
            {
                case "D1206LF":
                    {
                        return "(@1:3)";
                    }

                default:
                    {
                        return string.Empty;
                    }
            }
        }
    }
}
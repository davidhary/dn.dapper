using System.ComponentModel;
using System.Threading;
using System;

namespace isr.Dapper.Entities
{

    /// <summary>   The Product Text class holing the Product Text values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class ProductText : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Initializes a new instance of the <see cref="T:ProductText" /> class. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSetter"> The getter setter. </param>
        /// <param name="product">      The <see cref="Dapper.Entities.ProductEntity"/>. </param>
        public ProductText( Std.Primitives.IGetterSetter getterSetter, ProductEntity product ) : base()
        {
            this.GetterSetter = getterSetter;
            this.ProductAutoId = product.AutoId;
            this.ProductNumber = product.ProductNumber;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter GetterSetter { get; set; }

        /// <summary>   Gets or sets the text value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) The name. </param>
        /// <returns>   The text value. </returns>
        protected string Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Setters. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void Setter( string value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value, name );
                this.NotifyPropertyChanged( name );
            }
        }

        #endregion

        #region " IDENTIFIERS "

        /// <summary>   Identifies the product automatic. </summary>
        private int _ProductAutoId;

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ProductEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int ProductAutoId
        {
            get => this._ProductAutoId;

            set {
                if ( value != this.ProductAutoId )
                {
                    this._ProductAutoId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " Product PROPERTIES "

        /// <summary>   The product number. </summary>
        private string _ProductNumber;

        /// <summary>   Gets or sets the product number. </summary>
        /// <value> The product number. </value>
        public string ProductNumber
        {
            get => this._ProductNumber;

            set {
                if ( !string.Equals( value, this.ProductNumber ) )
                {
                    this._ProductNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " SCANNNING "

        /// <summary>   Gets or sets a list of scans. </summary>
        /// <value> A list of scans. </value>
        public string ScanList
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        #endregion

    }
}

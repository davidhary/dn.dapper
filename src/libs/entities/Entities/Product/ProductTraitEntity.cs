using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Product trait builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ProductTraitBuilder : OneToManyRealBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ProductTraitNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = ProductBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( ProductNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = ProductTraitTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ProductTraitTypeNub.Id );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( ProductTraitEntity.ProductAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( ProductTraitEntity.ProductTraitTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( ProductTraitEntity.Amount );

        #region " SINGLETON "

        private static readonly Lazy<ProductTraitBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ProductTraitBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="ProductTraitEntity"/> <see cref="IOneToManyReal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "ProductTrait" )]
    public class ProductTraitNub : OneToManyRealNub, IOneToManyReal
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductTraitNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyReal CreateNew()
        {
            return new ProductTraitNub();
        }
    }

    /// <summary>
    /// The <see cref="ProductTraitEntity"/> stores Product Traits.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class ProductTraitEntity : EntityBase<IOneToManyReal, ProductTraitNub>, IOneToManyReal
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductTraitEntity() : this( new ProductTraitNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductTraitEntity( IOneToManyReal value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ProductTraitEntity( IOneToManyReal cache, IOneToManyReal store ) : base( new ProductTraitNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductTraitEntity( ProductTraitEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ProductTraitBuilder.TableName, nameof( IOneToManyReal ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyReal CreateNew()
        {
            return new ProductTraitNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyReal CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyRealNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="ProductTraitEntity"/> interface value. </param>
        public override void CopyFrom( IOneToManyReal value )
        {
            OneToManyRealNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.ProductEntity"/>Value interface. </param>
        public override void UpdateCache( IOneToManyReal value )
        {
            // first make the copy to notify of any property change.
            OneToManyRealNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTraitTypeId">   Identifies the <see cref="ProductTraitTypeEntity"/>
        ///                                     value type. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int productAutoId, int productTraitTypeId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<ProductTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-15. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTraitTypeId">   Identifies the <see cref="ProductTraitTypeEntity"/>
        ///                                     value type. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int productAutoId, int productTraitTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, productAutoId, productTraitTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToManyReal entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTraitTypeId">   Type of the <see cref="ProductTraitTypeEntity"/> value. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int productAutoId, int productTraitTypeId )
        {
            return connection.Delete( new ProductTraitNub() { PrimaryId = productAutoId, SecondaryId = productTraitTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.ProductTraitEntity"/>'s.
        /// </summary>
        /// <value> the <see cref="Dapper.Entities.ProductTraitEntity"/>'s. </value>
        public IEnumerable<ProductTraitEntity> ProductTraits { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductTraitEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToManyReal>() ) : Populate( connection.GetAll<ProductTraitNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ProductTraits = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ProductTraitEntity.ProductTraits ) );
            return this.ProductTraits?.Any() == true ? this.ProductTraits.Count() : 0;
        }

        /// <summary>   Count ProductAttributeValues. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.ProductTraitBuilder.TableName}] WHERE {nameof( ProductTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductTraitEntity> FetchEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductTraitBuilder.TableName}] WHERE {nameof( ProductTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ProductTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>
        /// Fetches Product Traits by Product Id.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductTraitNub> FetchNubs( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTraitEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of ProductAttributeValue entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.ProductEntity"/>Value nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductTraitEntity> Populate( IEnumerable<ProductTraitNub> nubs )
        {
            var l = new List<ProductTraitEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ProductTraitNub nub in nubs )
                    l.Add( new ProductTraitEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of ProductAttributeValue entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.ProductEntity"/>Value interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductTraitEntity> Populate( IEnumerable<IOneToManyReal> interfaces )
        {
            var l = new List<ProductTraitEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ProductTraitNub();
                foreach ( IOneToManyReal iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ProductTraitEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Product Traits; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTraitTypeId">   Type of the <see cref="ProductTraitTypeEntity"/> value. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productAutoId, int productTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Product Traits by Product number and Product Trait Product Trait Real Value type;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTraitTypeId">   Identifies the <see cref="ProductTraitTypeEntity"/>
        ///                                     value type. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductTraitNub> FetchEntities( System.Data.IDbConnection connection, int productAutoId, int productTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTraitNub.PrimaryId )} = @primaryId", new { primaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.ProductEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTraitTypeId">   Identifies the <see cref="ProductTraitTypeEntity"/>
        ///                                     value type. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int productAutoId, int productTraitTypeId )
        {
            return 1 == CountEntities( connection, productAutoId, productTraitTypeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count traits associated with this Product. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of records. </returns>
        public int CountProductTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Product traits by Product id.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<ProductTraitNub> FetchProductTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.ProductEntity"/>. </summary>
        /// <value> the <see cref="Dapper.Entities.ProductEntity"/>. </value>
        public ProductEntity ProductEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.ProductEntity"/> . </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchProductEntity( System.Data.IDbConnection connection )
        {
            this.ProductEntity = new ProductEntity();
            return this.ProductEntity.FetchUsingKey( connection, this.ProductAutoId );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.ProductTraitTypeEntity"/> . </summary>
        /// <value> the <see cref="Dapper.Entities.ProductTraitTypeEntity"/> . </value>
        public ProductTraitTypeEntity ProductTraitTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.ProductTraitTypeEntity"/>. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchProductTraitTypeEntity( System.Data.IDbConnection connection )
        {
            this.ProductTraitTypeEntity = new ProductTraitTypeEntity();
            return this.ProductTraitTypeEntity.FetchUsingKey( connection, this.ProductTraitTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductTraitEntity.ProductAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.ProductEntity"/> record.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductEntity"/> record. </value>
        public int ProductAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductTraitEntity.ProductTraitTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.ProductTraitTypeEntity"/>.
        /// </summary>
        /// <value> The type of Identifies the <see cref="Dapper.Entities.ProductTraitTypeEntity"/>. </value>
        public int ProductTraitTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>
        /// Gets or sets a value assigned to the <see cref="Dapper.Entities.ProductEntity"/> for
        /// the specific <see cref="Dapper.Entities.ProductTraitTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The value assigned to the <see cref="Dapper.Entities.ProductEntity"/> for the specific
        /// <see cref="Dapper.Entities.ProductTraitTypeEntity"/>.
        /// </value>
        public double Amount
        {
            get => this.ICache.Amount;

            set {
                if ( !object.Equals( this.Amount, value ) )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="ProductTraitEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class ProductTraitEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToManyReal, ProductTraitNub, ProductTraitEntity>
    {
        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( ProductTraitEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( ProductTraitEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all Products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<ProductTraitEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ProductTraitEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return ProductTraitBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of unique <see cref="Dapper.Entities.ProductEntity"/> <see cref="ProductTraitEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class ProductUniqueTraitEntityCollection : ProductTraitEntityCollection, Std.Primitives.IGetterSetter<double>
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public ProductUniqueTraitEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
            this.ProductTrait = new ProductTrait( this );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        public ProductUniqueTraitEntityCollection( int productAutoId ) : this()
        {
            this.ProductAutoId = productAutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( ProductTraitEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.ProductTraitTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.ProductTraitTypeId );
            this.NotifyPropertyChanged( ProductTraitTypeEntity.EntityLookupDictionary()[entity.ProductTraitTypeId].Label );
        }

        /// <summary>
        /// Removes all Products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productTraitTypeId">   Identifies the
        ///                                     <see cref="Dapper.Entities.ProductTraitTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int productTraitTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( productTraitTypeId );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>
        /// Gets the value of the given <see cref="ProductTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        double? IGetterSetter<double>.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Sets the value for the given <see cref="ProductTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        double IGetterSetter<double>.Setter( double value, string name )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>
        /// Gets the value of the given <see cref="ProductTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable Double. </returns>
        protected double? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Sets the value for the given <see cref="ProductTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        private double SetterThis( double value, string name )
        {
            int key = this.ToKey( name );
            if ( !int.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>
        /// Sets the value for the given <see cref="ProductTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Double. </returns>
        protected double Setter( double value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   The trait value. </returns>
        protected double? TraitValueGetter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>   Trait value setter. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        protected void TraitValueSetter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.TraitValueGetter( name ) ) )
            {
                this.Setter( this.ToKey( name ), value.Value );
                this.NotifyPropertyChanged( name );
            }
        }


        /// <summary>   Gets or sets the product trait. </summary>
        /// <value> The product trait. </value>
        public ProductTrait ProductTrait { get; private set; }


        #endregion

        #region " TRAIT SELECTION "

        /// <summary>
        /// Converts a name to a key using the
        /// <see cref="ProductTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
        /// extend the Product Traits beyond the values of the enumeration type that is used to populate
        /// this table.
        /// </summary>
        /// <remarks>   David, 2020-05-11. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return ProductTraitTypeEntity.KeyLookupDictionary()[name];
        }

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productTraitTypeId">   Identifies the
        ///                                     <see cref="Dapper.Entities.ProductTraitTypeEntity"/>. </param>
        /// <returns>   An ProductAttributeRealEntity. </returns>
        public ProductTraitEntity Entity( int productTraitTypeId )
        {
            return this.ContainsKey( productTraitTypeId ) ? this[this._PrimaryKeyDictionary[productTraitTypeId]] : new ProductTraitEntity();
        }

        /// <summary>   Gets the value of the given reading type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productTraitTypeId">   Identifies the
        ///                                     <see cref="Dapper.Entities.ProductTraitTypeEntity"/>. </param>
        /// <returns>   A Nullable Double. </returns>
        public double? Getter( int productTraitTypeId )
        {
            return this.ContainsKey( productTraitTypeId ) ? this[this._PrimaryKeyDictionary[productTraitTypeId]].Amount : new double?();
        }

        /// <summary>   Set the specified Product value. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productTraitTypeId">   Identifies the
        ///                                     <see cref="Dapper.Entities.ProductTraitTypeEntity"/>. </param>
        /// <param name="value">                The value. </param>
        public void Setter( int productTraitTypeId, double value )
        {
            if ( this.ContainsKey( productTraitTypeId ) )
            {
                this[this._PrimaryKeyDictionary[productTraitTypeId]].Amount = value;
            }
            else
            {
                this.Add( new ProductTraitEntity() { ProductAutoId = ProductAutoId, ProductTraitTypeId = productTraitTypeId, Amount = value } );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ProductEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </value>
        public int ProductAutoId { get; private set; }

        #endregion

    }
}

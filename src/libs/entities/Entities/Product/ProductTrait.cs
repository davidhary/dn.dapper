using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{

    /// <summary>   The Product Trait class holing the Product trait values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class ProductTrait : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSestter">    The getter setter. </param>
        public ProductTrait( Std.Primitives.IGetterSetter<double> getterSestter ) : base()
        {
            this.GetterSetter = getterSestter;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<double> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) The name. </param>
        /// <returns>   The trait value. </returns>
        protected double? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Setters. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void Setter( double? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }
        }

        #endregion

        #region " PCM "

        /// <summary>   Assign default PCM traits. </summary>
        /// <remarks>   David, 2020-07-07. </remarks>
        /// <returns>   An Integer. </returns>
        public int AssignDefaultPcmTraits()
        {
            int addedCount = 0;
            if ( !this.MinimumCp.HasValue )
                addedCount += 1;
            this.MinimumCp = this.DefaultCp;
            if ( !this.MinimumCpk.HasValue )
                addedCount += 1;
            this.MinimumCpk = this.DefaultCpk;
            if ( !this.MinimumCpm.HasValue )
                addedCount += 1;
            this.MinimumCpm = this.DefaultCpm;
            if ( !this.SigmaLevel.HasValue )
                addedCount += 1;
            this.SigmaLevel = this.DefaultSigmaLevel;
            return addedCount;
        }

        /// <summary>   The default cp. </summary>
        private double _DefaultCp = 1.33d;

        /// <summary>   Gets or sets the default cp. </summary>
        /// <value> The default cp. </value>
        public double DefaultCp
        {
            get => this._DefaultCp;

            set {
                if ( value != this.DefaultCp )
                {
                    this._DefaultCp = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets the minimum Cp (process capability for a centered process). </summary>
        /// <value> The minimum Cp. </value>
        public double? MinimumCp
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   The default cpk. </summary>
        private double _DefaultCpk = 1.33d;

        /// <summary>   Gets or sets the default cpk. </summary>
        /// <value> The default cpk. </value>
        public double DefaultCpk
        {
            get => this._DefaultCpk;

            set {
                if ( value != this.DefaultCpk )
                {
                    this._DefaultCpk = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the minimum Cpk (process capability for a process that is not centered).
        /// </summary>
        /// <value> The minimum Cpk. </value>
        public double? MinimumCpk
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   The default cpm. </summary>
        private double _DefaultCpm = 1.33d;

        /// <summary>   Gets or sets the default cpm. </summary>
        /// <value> The default cpm. </value>
        public double DefaultCpm
        {
            get => this._DefaultCpm;

            set {
                if ( value != this.DefaultCpm )
                {
                    this._DefaultCpm = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets the minimum Cpm (process capability with bias consideration). </summary>
        /// <value> The minimum Cpm. </value>
        public double? MinimumCpm
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   The default sigma level. </summary>
        private double _DefaultSigmaLevel = 6d;

        /// <summary>   Gets or sets the default sigma level. </summary>
        /// <value> The default sigma level. </value>
        public double DefaultSigmaLevel
        {
            get => this._DefaultSigmaLevel;

            set {
                if ( value != this.DefaultSigmaLevel )
                {
                    this._DefaultSigmaLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets the sigma level. </summary>
        /// <value> The sigma level. </value>
        public double? SigmaLevel
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        #endregion

        #region " REFERENCE DATE "

        /// <summary>   Gets or sets the reference date. </summary>
        /// <value> The reference date. </value>
        public DateTime? ReferenceDate
        {
            get {

                double? days = this.Getter();
                return days.HasValue ? DateTime.MinValue.AddDays( days.Value ) : new DateTime?();
            }
            set {
                if ( value.HasValue )
                {
                    double days = value.Value.Subtract( DateTime.MinValue ).TotalDays;
                    this.Setter( days );
                }
                else
                {
                    this.Setter( new double?() );
                }
            }
        }

        #endregion

    }
}

using System.Collections.Generic;
using System.Linq;

using Dapper;

namespace isr.Dapper.Entities
{
    /// <summary>   A product entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class ProductEntity
    {

        /// <summary>
        /// Gets or sets the <see cref="ProductTraitEntity">Product Trait entities</see>.
        /// </summary>
        /// <value> The product Trait entities. </value>
        public IEnumerable<ProductTraitEntity> TraitEntities { get; private set; }

        /// <summary>
        /// Fetches the <see cref="ProductTraitEntity">Product Trait entities</see>.
        /// </summary>
        /// <remarks>   David, 2020-05-11. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The product Trait entities. </returns>
        public int FetchTraitEntities( System.Data.IDbConnection connection )
        {
            if ( !ProductTraitTypeEntity.IsEnumerated() )
                _ = ProductTraitTypeEntity.TryFetchAll( connection );
            return this.Populate( ProductTraitEntity.FetchEntities( connection, this.AutoId ) );
        }

        /// <summary>
        /// Fetches the <see cref="ProductTraitEntity">Product Trait entities</see>.
        /// </summary>
        /// <remarks>
        /// https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
        /// https://dapper-tutorial.net/querymultiple.
        /// </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the Product. </param>
        /// <returns>   The number of Product Trait entities. </returns>
        public int FetchTraitEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( @$"select * from [{ProductBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( @$" where {nameof( ProductNub.AutoId )} = @Id; " );
            _ = queryBuilder.AppendLine( @$"select * from [{ProductTraitBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( @$" where {nameof( ProductTraitNub.PrimaryId )} = @Id; " );
            var gridReader = connection.QueryMultiple( queryBuilder.ToString(), new { Id = productAutoId } );
            var entity = gridReader.ReadSingle<ProductNub>();
            if ( entity is null )
            {
                return this.Populate( new List<ProductTraitEntity>() );
            }
            else
            {
                if ( !KeyLabelTimeNub.AreEqual( entity, this.ICache ) )
                {
                    this.UpdateCache( entity );
                    _ = this.UpdateStore();
                }

                if ( !ProductTraitTypeEntity.IsEnumerated() )
                    _ = ProductTraitTypeEntity.TryFetchAll( connection );
                return this.Populate( ProductTraitEntity.Populate( gridReader.Read<ProductTraitNub>() ) );
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ProductUniqueTraitEntityCollection"> Product Trait Real (Double)
        /// values</see>.
        /// </summary>
        /// <value> The traits. </value>
        public ProductUniqueTraitEntityCollection Traits { get; private set; }

        /// <summary>
        /// Fetches the <see cref="ProductUniqueTraitEntityCollection">Product Trait Real(Double)-
        /// values</see>.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of <see cref="Dapper.Entities.ProductUniqueTraitEntityCollection"/>'s. </returns>
        public int FetchTraits( System.Data.IDbConnection connection )
        {
            if ( !ProductTraitTypeEntity.IsEnumerated() )
                _ = ProductTraitTypeEntity.TryFetchAll( connection );
            return this.Populate( ProductTraitEntity.FetchEntities( connection, this.AutoId ) );
        }

        /// <summary>
        /// Fetches the <see cref="ProductUniqueTraitEntityCollection">Product Trait Real(Double)-
        /// values</see>.
        /// </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The Product Traits. </returns>
        public static ProductUniqueTraitEntityCollection FetchTraits( System.Data.IDbConnection connection, int productAutoId )
        {
            if ( !ProductTraitTypeEntity.IsEnumerated() )
                _ = ProductTraitTypeEntity.TryFetchAll( connection );
            var Values = new ProductUniqueTraitEntityCollection( productAutoId );
            Values.Populate( ProductTraitEntity.FetchEntities( connection, productAutoId ) );
            return Values;
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="entities"> The entities. </param>
        /// <returns>   The number of entities. </returns>
        private int Populate( IEnumerable<ProductTraitEntity> entities )
        {
            this.TraitEntities = entities;
            if ( this.Traits is null )
            {
                this.Traits = new ProductUniqueTraitEntityCollection( this.AutoId );
            }
            else
            {
                this.Traits.Clear();
            }

            this.Traits.Populate( entities );
            this.NotifyPropertyChanged( nameof( ProductEntity.TraitEntities ) );
            this.NotifyPropertyChanged( nameof( ProductEntity.Traits ) );
            return entities?.Any() == true ? entities.Count() : 0;
        }
    }
}

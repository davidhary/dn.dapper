using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using FastEnums;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{
    /// <summary>   A Product Binning builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ProductBinningBuilder : FourToManyIdBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ProductBinningNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = ProductBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( ProductNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = ElementBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ElementNub.AutoId );

        /// <summary>   Gets or sets the name of the Ternary table. </summary>
        /// <value> The name of the Ternary table. </value>
        public override string TernaryTableName { get; set; } = NomTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the Ternary table key. </summary>
        /// <value> The name of the Ternary table key. </value>
        public override string TernaryTableKeyName { get; set; } = nameof( NomTypeNub.Id );

        /// <summary>   Gets or sets the name of the Quaternary table. </summary>
        /// <value> The name of the Quaternary table. </value>
        public override string QuaternaryTableName { get; set; } = ReadingBinBuilder.TableName;

        /// <summary>   Gets or sets the name of the Quaternary table key. </summary>
        /// <value> The name of the Quaternary table key. </value>
        public override string QuaternaryTableKeyName { get; set; } = nameof( ReadingBinNub.Id );

        /// <summary>   Gets or sets the name of the Id table. </summary>
        /// <value> The name of the Id table. </value>
        public override string ForeignIdTableName { get; set; } = ProductSortBuilder.TableName;

        /// <summary>   Gets or sets the name of the Id table key. </summary>
        /// <value> The name of the Id table key. </value>
        public override string ForeignIdTableKeyName { get; set; } = nameof( ProductSortNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( ProductBinningEntity.ProductAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( ProductBinningEntity.ElementAutoId );

        /// <summary>   Gets or sets the name of the ternary identifier field. </summary>
        /// <value> The name of the ternary identifier field. </value>
        public override string TernaryIdFieldName { get; set; } = nameof( ProductBinningEntity.NomTypeId );

        /// <summary>   Gets or sets the name of the quaternary identifier field. </summary>
        /// <value> The name of the quaternary identifier field. </value>
        public override string QuaternaryIdFieldName { get; set; } = nameof( ProductBinningEntity.ReadingBinId );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( ProductBinningEntity.ProductSortAutoId );


        #region " SINGLETON "

        private static readonly Lazy<ProductBinningBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ProductBinningBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="ProductBinningEntity"/> <see cref="IFourToManyId">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "ProductBinning" )]
    public class ProductBinningNub : FourToManyIdNub, IFourToManyId
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductBinningNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IFourToManyId CreateNew()
        {
            return new ProductBinningNub();
        }
    }

    /// <summary>
    /// The <see cref="ProductBinningEntity"/> storing product binning information.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class ProductBinningEntity : EntityBase<IFourToManyId, ProductBinningNub>, IFourToManyId
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductBinningEntity() : this( new ProductBinningNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductBinningEntity( IFourToManyId value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ProductBinningEntity( IFourToManyId cache, IFourToManyId store ) : base( new ProductBinningNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductBinningEntity( ProductBinningEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ProductBinningBuilder.TableName, nameof( IFourToManyId ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IFourToManyId CreateNew()
        {
            return new ProductBinningNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IFourToManyId CreateCopy()
        {
            var destination = this.CreateNew();
            FourToManyIdNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="ProductBinningEntity"/> interface value. </param>
        public override void CopyFrom( IFourToManyId value )
        {
            FourToManyIdNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.ProductEntity"/>Value interface. </param>
        public override void UpdateCache( IFourToManyId value )
        {
            // first make the copy to notify of any property change.
            FourToManyIdNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="productId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int productId, int elementId, int nomTypeId, int readingBinId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductBinningBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productId } );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementId } );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.TernaryId )} = @TernaryId", new { TernaryId = nomTypeId } );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.QuaternaryId )} = @QuaternaryId", new { QuaternaryId = readingBinId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<ProductBinningNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId, this.TernaryId, this.QuaternaryId );
        }


        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId, this.TernaryId, this.QuaternaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-15. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId">     Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int productAutoId, int elementId, int nomTypeId, int readingBinId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, productAutoId, elementId, nomTypeId, readingBinId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IFourToManyId entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId, entity.TernaryId, entity.QuaternaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementId">        Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId">     Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <param name="id">               The identifier of Quaternary reference. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int productAutoId, int elementId, int nomTypeId, int readingBinId, int id )
        {
            return connection.Delete( new ProductBinningNub() {
                PrimaryId = productAutoId,
                SecondaryId = elementId,
                TernaryId = nomTypeId,
                QuaternaryId = readingBinId,
                ForeignId = id
            } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.ProductEntity"/> Binning entities. </summary>
        /// <value> the <see cref="Dapper.Entities.ProductEntity"/> Binning entities. </value>
        public IEnumerable<ProductBinningEntity> ProductBinnings { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductBinningEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IFourToManyId>() ) : Populate( connection.GetAll<ProductBinningNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ProductBinnings = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ProductBinningEntity.ProductBinnings ) );
            return this.ProductBinnings?.Any() == true ? this.ProductBinnings.Count() : 0;
        }

        /// <summary>   Count ProductAttributeValues. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="productId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.ProductBinningBuilder.TableName}] WHERE {nameof( ProductBinningNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="productId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductBinningEntity> FetchEntities( System.Data.IDbConnection connection, int productId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductBinningBuilder.TableName}] WHERE {nameof( ProductBinningNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ProductBinningNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>
        /// Fetches Product Traits by Product number and Product Trait Product Trait Real Value type;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="productId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductBinningNub> FetchNubs( System.Data.IDbConnection connection, int productId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductBinningBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = productId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductBinningNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="productId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductBinningEntity> FetchEntities( System.Data.IDbConnection connection, int productId, int elementId, int nomTypeId, int readingBinId )
        {
            return Populate( FetchNubs( connection, productId, elementId, nomTypeId, readingBinId ) );
        }

        /// <summary>
        /// Fetches Product Traits by Product number and Product Trait Product Trait Real Value type;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="productId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductBinningNub> FetchNubs( System.Data.IDbConnection connection, int productId, int elementId, int nomTypeId, int readingBinId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate( @$"SELECT * FROM [{isr.Dapper.Entities.ProductBinningBuilder.TableName}]
            WHERE (
                    {nameof( ProductBinningNub.PrimaryId )} = @PrimaryId
                AND {nameof( ProductBinningNub.SecondaryId )} = @SecondaryId
                AND {nameof( ProductBinningNub.TernaryId )} = @TernaryId
                AND {nameof( ProductBinningNub.QuaternaryId )} = @QuaternaryId
                  ) ".Clean(), new { PrimaryId = productId, SecondaryId = elementId, TernaryId = nomTypeId, QuaternaryId = readingBinId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductBinningNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of ProductAttributeValue entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.ProductEntity"/>Value nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductBinningEntity> Populate( IEnumerable<ProductBinningNub> nubs )
        {
            var l = new List<ProductBinningEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ProductBinningNub nub in nubs )
                    l.Add( new ProductBinningEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of ProductAttributeValue entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.ProductEntity"/>Value interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductBinningEntity> Populate( IEnumerable<IFourToManyId> interfaces )
        {
            var l = new List<ProductBinningEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ProductBinningNub();
                foreach ( IFourToManyId iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ProductBinningEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Product Traits; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="productId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productId, int elementId, int nomTypeId, int readingBinId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductBinningBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productId } );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementId } );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.TernaryId )} = @TernaryId", new { TernaryId = nomTypeId } );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.QuaternaryId )} = @QuaternaryId", new { QuaternaryId = readingBinId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Product Traits by Product number and Product Trait Product Trait Real Value type;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="productId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductBinningNub> FetchEntitiesUsingBuilder( System.Data.IDbConnection connection, int productId, int elementId, int nomTypeId, int readingBinId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductBinningBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productId } );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.SecondaryId )} = @SecondaryId", new { SecondaryId = elementId } );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.TernaryId )} = @TernaryId", new { TernaryId = nomTypeId } );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.QuaternaryId )} = @QuaternaryId", new { QuaternaryId = readingBinId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductBinningNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.ProductEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="productId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="elementId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int productId, int elementId, int nomTypeId, int readingBinId )
        {
            return 1 == CountEntities( connection, productId, elementId, nomTypeId, readingBinId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count values associated with this Product Trait. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of records. </returns>
        public int CountProductAttributeValues( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductBinningBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Product BinningValues by Product auto id;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<ProductBinningNub> FetchProductAttributeValues( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductBinningBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductBinningNub.PrimaryId )} = @PrimaryId", new { PrimaryId = this.ProductAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductBinningNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.ProductEntity"/>. </summary>
        /// <value> the <see cref="Dapper.Entities.ProductEntity"/>. </value>
        public ProductEntity ProductEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.ProductEntity"/> . </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchProductEntity( System.Data.IDbConnection connection )
        {
            this.ProductEntity = new ProductEntity();
            return this.ProductEntity.FetchUsingKey( connection, this.ProductAutoId );
        }

        /// <summary>   Gets or sets the <see cref="ProductSortEntity"/> . </summary>
        /// <value> the <see cref="ProductSortEntity"/> . </value>
        public ProductSortEntity ProductSortEntity { get; private set; }

        /// <summary>   Fetches a <see cref="ProductSortEntity"/>. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchProductSortEntity( System.Data.IDbConnection connection )
        {
            this.ProductSortEntity = new ProductSortEntity();
            return this.ProductSortEntity.FetchUsingKey( connection, this.ProductSortAutoId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductBinningEntity.ProductAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.ProductEntity"/> record.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductEntity"/> record. </value>
        public int ProductAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductBinningEntity.ElementAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int ElementAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>   Gets or sets the id of the Ternary reference. </summary>
        /// <value> The identifier of Ternary reference. </value>
        public int TernaryId
        {
            get => this.ICache.TernaryId;

            set {
                if ( !object.Equals( ( object ) this.TernaryId, ( object ) value ) )
                {
                    this.ICache.TernaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductBinningEntity.NomTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public int NomTypeId
        {
            get => this.TernaryId;

            set => this.TernaryId = value;
        }

        /// <summary>   Gets or sets the id of the Quaternary reference. </summary>
        /// <value> The identifier of Quaternary reference. </value>
        public int QuaternaryId
        {
            get => this.ICache.QuaternaryId;

            set {
                if ( !object.Equals( ( object ) this.QuaternaryId, ( object ) value ) )
                {
                    this.ICache.QuaternaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductBinningEntity.ReadingBinId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the reading bin. </summary>
        /// <value> Identifies the reading bin. </value>
        public int ReadingBinId
        {
            get => this.QuaternaryId;

            set => this.QuaternaryId = value;
        }

        /// <summary>   Gets or sets the Three-to-Many referenced table identifier. </summary>
        /// <value> The identifier of Three-to-Many referenced table. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductBinningEntity.ProductSortAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="ProductSortEntity"/>. </summary>
        /// <value> Identifies the <see cref="ProductSortEntity"/>. </value>
        public int ProductSortAutoId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The entity selector. </value>
        public FourKeySelector EntitySelector => new( this );

        /// <summary>   Gets the unique selector. </summary>
        /// <value> The unique selector. </value>
        public ProductBinningUniqueKey UniqueSelector => new( this );

        #endregion

    }

    /// <summary>
    /// Collection of Product Binning Entities uniquely identified by the
    /// <see cref="ProductBinningEntity.EntitySelector"/> <see cref="FourKeySelector"/>.
    /// </summary>
    /// <remarks>   David, 2020-05-23. </remarks>
    public class ProductBinningEntityCollection : EntityKeyedCollection<FourKeySelector, IFourToManyId, ProductBinningNub, ProductBinningEntity>
    {
        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override FourKeySelector GetKeyForItem( ProductBinningEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        public ProductBinningEntityCollection() : base()
        {
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( ProductBinningEntity item )
        {
            base.Add( item );
        }

        /// <summary>   Updates or adds the given entity. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        /// <param name="entity">   The entity. </param>
        public virtual void Upadd( ProductBinningEntity entity )
        {
            if ( this.Contains( entity.EntitySelector ) )
            {
                this[entity.EntitySelector].ProductSortAutoId = entity.ProductSortAutoId;
            }
            else
            {
                this.Add( entity );
            }
        }

        /// <summary>
        /// Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="entities"> The entities. </param>
        /// <returns>   An int. </returns>
        public int Populate( IEnumerable<ProductBinningEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ProductBinningEntity entity in entities )
                    this.Add( entity );
            }

            return this.Any() ? this.Count : 0;
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return ProductBinningBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of Product-Unique binning entities.
    /// </summary>
    /// <remarks>   David, 2020-05-23. </remarks>
    public class ProductUniqueBinningEntityCollection : ProductBinningEntityCollection
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public ProductUniqueBinningEntityCollection() : base()
        {
            this._ElementReadingBinDictionary = new Dictionary<ProductBinningUniqueKey, int>();
            this._UniqueIndexDictionary = new Dictionary<FourKeySelector, ProductBinningUniqueKey>();
            this._PrimaryKeyDictionary = new Dictionary<ProductBinningUniqueKey, FourKeySelector>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        public ProductUniqueBinningEntityCollection( int productAutoId ) : this()
        {
            this.ProductAutoId = productAutoId;
        }

        /// <summary>   Dictionary of element reading bins. </summary>
        private readonly IDictionary<ProductBinningUniqueKey, int> _ElementReadingBinDictionary;
        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<FourKeySelector, ProductBinningUniqueKey> _UniqueIndexDictionary;
        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<ProductBinningUniqueKey, FourKeySelector> _PrimaryKeyDictionary;

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ProductEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </value>
        public int ProductAutoId { get; private set; }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( ProductBinningEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.UniqueSelector, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.UniqueSelector );
            this._ElementReadingBinDictionary.Add( entity.UniqueSelector, entity.ProductSortAutoId );
        }

        /// <summary>
        /// Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._ElementReadingBinDictionary.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Fetch and populates entities for the <see cref="ProductAutoId"/>. </summary>
        /// <remarks>   David, 2020-05-22. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public int Populate( System.Data.IDbConnection connection )
        {
            return this.Populate( ProductBinningEntity.FetchEntities( connection, this.ProductAutoId ) );
        }

        /// <summary>   Query if the collection contains 'productSortId' key. </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId">     Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int elementAutoId, int nomTypeId, int readingBinId )
        {
            return this.ContainsKey( new ProductBinningUniqueKey( elementAutoId, nomTypeId, readingBinId ) );
        }

        /// <summary>   Query if the collection contains 'productSortId' key. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( ProductBinningEntity entity )
        {
            return this._PrimaryKeyDictionary.ContainsKey( entity.UniqueSelector );
        }

        /// <summary>   Query if the collection contains 'productSortId' key. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        /// <param name="uniqueKey">    The unique key. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( ProductBinningUniqueKey uniqueKey )
        {
            return this._PrimaryKeyDictionary.ContainsKey( uniqueKey );
        }

        /// <summary>   Selects the entity associated with the given element and bin identities. </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId">     Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   A ProductBinningEntity. </returns>
        public ProductBinningEntity Entity( int elementAutoId, int nomTypeId, int readingBinId )
        {
            return this.Entity( new ProductBinningUniqueKey( elementAutoId, nomTypeId, readingBinId ) );
        }

        /// <summary>   Selects the entity associated with the given element and bin identities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="selector"> The selector. </param>
        /// <returns>   A ProductBinningEntity. </returns>
        public ProductBinningEntity Entity( ProductBinningUniqueKey selector )
        {
            return this._ElementReadingBinDictionary.ContainsKey( selector ) ? this[this._ElementReadingBinDictionary[selector]] : new ProductBinningEntity();
        }

        /// <summary>   Gets the identity of the sort for the given element and bin identities. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId">     Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   An Integer? </returns>
        public int? Getter( int elementAutoId, int nomTypeId, int readingBinId )
        {
            return this.Getter( new ProductBinningUniqueKey( elementAutoId, nomTypeId, readingBinId ) );
        }

        /// <summary>   Gets the identity of the sort for the given element and bin identities. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        /// <param name="selector"> The selector. </param>
        /// <returns>   An Integer? </returns>
        public int? Getter( ProductBinningUniqueKey selector )
        {
            return this._ElementReadingBinDictionary.ContainsKey( selector ) ? this._ElementReadingBinDictionary[selector] : new int?();
        }

        /// <summary>   Set the specified product sort value. </summary>
        /// <remarks>   David, 2020-06-14. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId">     Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <param name="productSortId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/> sort. </param>
        public void Setter( int elementAutoId, int nomTypeId, int readingBinId, int productSortId )
        {
            var selector = new ProductBinningUniqueKey( elementAutoId, nomTypeId, readingBinId );
            if ( this._ElementReadingBinDictionary.ContainsKey( selector ) )
            {
                this._ElementReadingBinDictionary[selector] = productSortId;
            }
            else
            {
                this.Add( new isr.Dapper.Entities.ProductBinningEntity {
                    ProductAutoId = this.ProductAutoId,
                    ElementAutoId = elementAutoId,
                    NomTypeId = nomTypeId,
                    ReadingBinId = readingBinId,
                    ForeignId = productSortId
                } );
            }
        }

        #endregion

        #region " BUILD "

        /// <summary>   Select guarded product sort. </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
        ///                                         illegal values. </exception>
        /// <param name="elementType">  The <see cref="Dapper.Entities.ElementTypeNub"/>. </param>
        /// <param name="nomType">      The <see cref="NomTypeEntity"/>. </param>
        /// <returns>   A ProductSort. </returns>
        public virtual ProductSort SelectGuardedProductSort( ElementType elementType, NomType nomType )
        {
            var productSort = default( ProductSort );
            switch ( nomType )
            {
                case NomType.Resistance:
                    {
                        switch ( elementType )
                        {
                            case ElementType.Resistor:
                                {
                                    productSort = ProductSort.ResistorGuardFailed;
                                    break;
                                }

                            case ElementType.CompoundResistor:
                                {
                                    productSort = ProductSort.CompoundResistorGuardFailed;
                                    break;
                                }

                            default:
                                {
                                    throw new ArgumentException( $"unhandled {nameof( elementType )} = {elementType} for {nameof( nomType )}={nomType}" );
                                }
                        }

                        break;
                    }

                case NomType.ThirdHarmonicsIndex:
                    {
                        switch ( elementType )
                        {
                            case ElementType.Resistor:
                                {
                                    productSort = ProductSort.ResistorGuardFailed;
                                    break;
                                }

                            default:
                                {
                                    throw new ArgumentException( $"unhandled {nameof( elementType )} = {elementType} for {nameof( nomType )}={nomType}" );
                                }
                        }

                        break;
                    }

                case NomType.Equation:
                    {
                        switch ( elementType )
                        {
                            case ElementType.Equation:
                                {
                                    productSort = ProductSort.ComputedValueGuardFailed;
                                    break;
                                }

                            default:
                                {
                                    throw new ArgumentException( $"unhandled {nameof( elementType )} = {elementType} for {nameof( nomType )}={nomType}" );
                                }
                        }

                        break;
                    }
            }

            return productSort;
        }

        /// <summary>   Select product sort. </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <param name="elementType">  The <see cref="Dapper.Entities.ElementTypeNub"/>. </param>
        /// <param name="nomType">      The <see cref="NomTypeEntity"/>. </param>
        /// <param name="readingBin">   The <see cref="Dapper.Entities.ReadingBin"/>. </param>
        /// <returns>   A ProductSort. </returns>
        public virtual ProductSort SelectProductSort( ElementType elementType, NomType nomType, ReadingBin readingBin )
        {
            var productSort = ProductSort.Good;
            switch ( readingBin )
            {
                case ReadingBin.Good:
                    {
                        productSort = ProductSort.Good;
                        break;
                    }

                case ReadingBin.High:
                    {
                        productSort = this.SelectGuardedProductSort( elementType, nomType );
                        break;
                    }

                case ReadingBin.Invalid:
                    {
                        productSort = ProductSort.ReadingFailed;
                        break;
                    }

                case ReadingBin.Low:
                    {
                        productSort = this.SelectGuardedProductSort( elementType, nomType );
                        break;
                    }

                case ReadingBin.Nameless:
                    {
                        // not sorted
                        productSort = ProductSort.Nameless;
                        break;
                    }

                case ReadingBin.Overflow:
                    {
                        productSort = ProductSort.ReadingOverflow;
                        break;
                    }
            }

            return productSort;
        }

        /// <summary>   Fetch and populates entities for the <see cref="ProductAutoId"/>. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="elements">     The elements. </param>
        /// <param name="sorts">        The sorts. </param>
        public void Populate( System.Data.IDbConnection connection, ProductUniqueElementEntityCollection elements, ProductUniqueProductSortEntityCollection sorts )
        {
            ProductSort productSort;
            int productSortId;
            foreach ( var element in elements )
            {
                foreach ( NomTypeEntity nomTypeEntity in element.NomTypes )
                {
                    foreach ( ReadingBin readingBin in Enum.GetValues( typeof( ReadingBin ) ) )
                    {
                        productSort = this.SelectProductSort( ( ElementType ) element.ElementTypeId, ( NomType ) nomTypeEntity.Id, readingBin );
                        productSortId = sorts.ProductSort( productSort.Description() ).AutoId;
                        // add mapping from bin to sort for all elements and all nominal types
                        this.Upadd( new ProductBinningEntity() {
                            ProductAutoId = ProductAutoId,
                            ElementAutoId = element.AutoId,
                            NomTypeId = nomTypeEntity.Id,
                            ReadingBinId = ( int ) readingBin,
                            ProductSortAutoId = productSortId
                        } );
                    }
                }
            }
            // save the collection
            _ = this.Upsert( connection );
        }

        #endregion

    }

    /// <summary>   Product binning unique key selector. </summary>
    /// <remarks>   David, 2020-06-14. </remarks>
    public struct ProductBinningUniqueKey : IEquatable<ProductBinningUniqueKey>
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="binning">  The binning. </param>
        public ProductBinningUniqueKey( ProductBinningEntity binning ) : this()
        {
            this.ElementId = binning.ElementAutoId;
            this.NomTypeId = binning.NomTypeId;
            this.ReadingBinId = binning.ReadingBinId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="readingBinId">     Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        public ProductBinningUniqueKey( int elementAutoId, int nomTypeId, int readingBinId ) : this()
        {
            this.ElementId = elementAutoId;
            this.NomTypeId = nomTypeId;
            this.ReadingBinId = readingBinId;
        }

        /// <summary>   Gets or sets the id of the element. </summary>
        /// <value> The identifier of the element. </value>
        public int ElementId { get; set; }
        /// <summary>   Gets or sets the id of the nom type. </summary>
        /// <value> The identifier of the nom type. </value>
        public int NomTypeId { get; set; }
        /// <summary>   Gets or sets the id of the reading bin. </summary>
        /// <value> The identifier of the reading bin. </value>
        public int ReadingBinId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( ProductBinningUniqueKey ) obj );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( ProductBinningUniqueKey other )
        {
            return this.ElementId == other.ElementId && this.NomTypeId == other.NomTypeId && this.ReadingBinId == other.ReadingBinId;
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.ElementId, this.NomTypeId, this.ReadingBinId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( ProductBinningUniqueKey left, ProductBinningUniqueKey right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( ProductBinningUniqueKey left, ProductBinningUniqueKey right )
        {
            return !(left == right);
        }
    }
}

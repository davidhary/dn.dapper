﻿
using System.Linq;

namespace isr.Dapper.Entities
{
    /// <summary>   A product entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class ProductEntity
    {

        /// <summary>
        /// Gets or sets the <see cref=" ProductUniqueBinningEntityCollection">Product
        /// <see cref="ProductBinningEntity"/>'s</see>.
        /// </summary>
        /// <value> The Product ProductSort trait values. </value>
        public ProductUniqueBinningEntityCollection ProductBinnings { get; private set; }

        /// <summary>
        /// Fetches the <see cref=" ProductUniqueBinningEntityCollection">Product
        /// <see cref="ProductBinningEntity"/>'s</see>.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of ProductBinning. </returns>
        public int FetchProductBinning( System.Data.IDbConnection connection )
        {
            this.ProductBinnings = FetchProductBinning( connection, this.AutoId );
            return this.ProductBinnings?.Any() == true ? this.ProductBinnings.Count : 0;
        }

        /// <summary>
        /// Fetches the <see cref=" ProductUniqueBinningEntityCollection">Product
        /// <see cref="ProductBinningEntity"/>'s</see>.
        /// </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The ProductBinning. </returns>
        public static ProductUniqueBinningEntityCollection FetchProductBinning( System.Data.IDbConnection connection, int productAutoId )
        {
            var productBinning = new ProductUniqueBinningEntityCollection( productAutoId );
            _ = productBinning.Populate( ProductBinningEntity.FetchEntities( connection, productAutoId ) );
            return productBinning;
        }
    }
}
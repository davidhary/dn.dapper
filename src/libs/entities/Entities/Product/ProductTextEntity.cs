using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Product Text builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ProductTextBuilder : TwoToManyLabelBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ProductTextNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public override int LabelFieldSize { get; set; } = 255;

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = ProductBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( ProductNub.AutoId );

        /// <summary>   Gets or sets the name of the Secondary table. </summary>
        /// <value> The name of the Secondary table. </value>
        public override string SecondaryTableName { get; set; } = ProductTextTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the Secondary table key. </summary>
        /// <value> The name of the Secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ProductTextTypeNub.Id );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( ProductTextEntity.ProductAutoId );

        /// <summary>   Gets or sets the name of the Secondary identifier field. </summary>
        /// <value> The name of the Secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( ProductTextEntity.ProductTextTypeId );

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public override string LabelFieldName { get; set; } = nameof( ProductTextEntity.Label );

        #region " SINGLETON "

        private static readonly Lazy<ProductTextBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ProductTextBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="Dapper.Entities.ProductEntity"/> Text table
    /// <see cref="ITwoToManyLabel">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "ProductText" )]
    public class ProductTextNub : TwoToManyLabelNub, ITwoToManyLabel
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductTextNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ITwoToManyLabel CreateNew()
        {
            return new ProductTextNub();
        }
    }

    /// <summary>
    /// The <see cref="ProductTextEntity"/>. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class ProductTextEntity : EntityBase<ITwoToManyLabel, ProductTextNub>, ITwoToManyLabel
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductTextEntity() : this( new ProductTextNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductTextEntity( ITwoToManyLabel value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ProductTextEntity( ITwoToManyLabel cache, ITwoToManyLabel store ) : base( new ProductTextNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductTextEntity( ProductTextEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ProductTextBuilder.TableName, nameof( ITwoToManyLabel ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override ITwoToManyLabel CreateNew()
        {
            return new ProductTextNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override ITwoToManyLabel CreateCopy()
        {
            var destination = this.CreateNew();
            TwoToManyLabelNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="ProductTextEntity"/> interface value. </param>
        public override void CopyFrom( ITwoToManyLabel value )
        {
            TwoToManyLabelNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached Text, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    the <see cref="Dapper.Entities.ProductEntity"/>Text interface. </param>
        public override void UpdateCache( ITwoToManyLabel value )
        {
            // first make the copy to notify of any property change.
            TwoToManyLabelNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTextTypeId">    Identifies the <see cref="ProductTextTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int productAutoId, int productTextTypeId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTextNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductTextNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productTextTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<ProductTextNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTextTypeId">    Identifies the <see cref="ProductTextTypeEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int productAutoId, int productTextTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, productAutoId, productTextTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ITwoToManyLabel entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTextTypeId">    Identifies the <see cref="ProductTextTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int productAutoId, int productTextTypeId )
        {
            return connection.Delete( new ProductTextNub() { PrimaryId = productAutoId, SecondaryId = productTextTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.ProductEntity"/>Text entities. </summary>
        /// <value> the <see cref="Dapper.Entities.ProductEntity"/>Text entities. </value>
        public IEnumerable<ProductTextEntity> ProductTexts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductTextEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ITwoToManyLabel>() ) : Populate( connection.GetAll<ProductTextNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ProductTexts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ProductTextEntity.ProductTexts ) );
            return this.ProductTexts?.Any() == true ? this.ProductTexts.Count() : 0;
        }

        /// <summary>   Count Product Texts. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.ProductTextBuilder.TableName}] WHERE {nameof( ProductTextNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductTextEntity> FetchEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductTextBuilder.TableName}] WHERE {nameof( ProductTextNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ProductTextNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Product Texts by Product Auto Id. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductTextNub> FetchNubs( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTextEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductTextNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of Product Text entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> the <see cref="Dapper.Entities.ProductEntity"/>Text nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductTextEntity> Populate( IEnumerable<ProductTextNub> nubs )
        {
            var l = new List<ProductTextEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ProductTextNub nub in nubs )
                    l.Add( new ProductTextEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Product Text entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   the <see cref="Dapper.Entities.ProductEntity"/>Text interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductTextEntity> Populate( IEnumerable<ITwoToManyLabel> interfaces )
        {
            var l = new List<ProductTextEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ProductTextNub();
                foreach ( ITwoToManyLabel iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ProductTextEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Product Texts by unique index; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTextTypeId">    Identifies the <see cref="ProductTextTypeEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productAutoId, int productTextTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTextNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductTextNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productTextTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches Product Texts by unique index; expected single or none. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTextTypeId">    Identifies the <see cref="ProductTextTypeEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductTextNub> FetchEntities( System.Data.IDbConnection connection, int productAutoId, int productTextTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTextNub.PrimaryId )} = @primaryId", new { primaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductTextNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productTextTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductTextNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.ProductEntity"/>Text exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="productAutoId">        Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="productTexttypeId">    Identifies the <see cref="ProductTextTypeEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int productAutoId, int productTexttypeId )
        {
            return 1 == CountEntities( connection, productAutoId, productTexttypeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count Texts associated with this product. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of Texts. </returns>
        public int CountProductTexts( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTextNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Product Text Texts by Product auto id;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-07-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<ProductTextNub> FetchProductTexts( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductTextBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductTextNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductTextNub>( selector.RawSql, selector.Parameters );
        }

        #endregion

        #region " RELATIONS: PRODUCT "

        /// <summary>   Gets or sets the Product entity. </summary>
        /// <value> The Product entity. </value>
        public ProductEntity ProductEntity { get; private set; }

        /// <summary>   Fetches a Product Entity. </summary>
        /// <remarks>   David, 2020-06-16. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Product Entity. </returns>
        public ProductEntity FetchProductEntity( System.Data.IDbConnection connection )
        {
            var entity = new ProductEntity();
            _ = entity.FetchUsingKey( connection, this.ProductAutoId );
            this.ProductEntity = entity;
            return entity;
        }

        #endregion

        #region " RELATIONS: PRODUCT TEXT TYPE  "

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.ProductEntity"/> <see cref="ProductTextTypeEntity"/>.
        /// </summary>
        /// <value> the <see cref="Dapper.Entities.ProductEntity"/> Text type entity. </value>
        public ProductTextTypeEntity ProductTextTypeEntity { get; private set; }

        /// <summary>   Fetches a product text type Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchProductTextTypeEntity( System.Data.IDbConnection connection )
        {
            this.ProductTextTypeEntity = new ProductTextTypeEntity();
            return this.ProductTextTypeEntity.FetchUsingKey( connection, this.ProductTextTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductTextEntity.ProductAutoId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.ProductEntity"/> record.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductEntity"/> record. </value>
        public int ProductAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductTextEntity.ProductTextTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the identity (type) of the <see cref="Dapper.Entities.ProductTextEntity"/>.
        /// </summary>
        /// <value> Identifies the Product Text type. </value>
        public int ProductTextTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }


        /// <summary>   Gets or sets the <see cref="Dapper.Entities.ProductTextEntity"/> label. </summary>
        /// <value> the <see cref="Dapper.Entities.ProductTextEntity"/> label. </value>
        public string Label
        {
            get => this.ICache.Label;

            set {
                if ( !string.Equals( this.Label, value ) )
                {
                    this.ICache.Label = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public TwoKeySelector EntitySelector => new( this );

        #endregion

    }

    /// <summary>   Collection of <see cref="ProductTextEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class ProductTextEntityCollection : EntityKeyedCollection<TwoKeySelector, ITwoToManyLabel, ProductTextNub, ProductTextEntity>
    {
        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override TwoKeySelector GetKeyForItem( ProductTextEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( ProductTextEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<ProductTextEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ProductTextEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return ProductTextBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of <see cref="Dapper.Entities.ProductEntity"/>-Unique <see cref="ProductTextEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class ProductUniqueTextEntityCollection : ProductTextEntityCollection, Std.Primitives.IGetterSetter
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="product">  The product. </param>
        public ProductUniqueTextEntityCollection( ProductEntity product ) : base()
        {
            this.ProductAutoId = product.AutoId;
            this._UniqueIndexDictionary = new Dictionary<TwoKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, TwoKeySelector>();
            this.ProductText = new ProductText( this, product );
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<TwoKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, TwoKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( ProductTextEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.ProductTextTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.ProductTextTypeId );
            this.NotifyPropertyChanged( ProductTextTypeEntity.EntityLookupDictionary()[entity.ProductTextTypeId].Label );
        }

        /// <summary>
        /// Removes all products from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Queries if collection contains 'productTextType' key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productTextTypeId">    Identifies the
        ///                                     <see cref="Dapper.Entities.ProductTextTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int productTextTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( productTextTypeId );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>
        /// Gets the Test(String)-value for the given <see cref="ProductTextEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        string IGetterSetter.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Set the specified product value for the given <see cref="ProductTextEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        string IGetterSetter.Setter( string value, string name )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>
        /// Gets the Test(String)-value for the given <see cref="ProductTextEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable String. </returns>
        protected string Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Set the specified product value for the given <see cref="ProductTextEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        private string SetterThis( string value, string name )
        {
            int key = this.ToKey( name );
            if ( !string.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>
        /// Set the specified product value for the given <see cref="ProductTextEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A String. </returns>
        protected string Setter( string value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets or sets the Product Text. </summary>
        /// <value> The Product Text. </value>
        public ProductText ProductText { get; private set; }

        #endregion

        #region " TRAIT SELECTION "

        /// <summary>   Converts a name to a key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return ProductTextTypeEntity.KeyLookupDictionary()[name];
        }

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productTextTypeId">    Identifies the
        ///                                     <see cref="Dapper.Entities.ProductTextTypeEntity"/>. </param>
        /// <returns>   An ProductTextEntity. </returns>
        public ProductTextEntity Entity( int productTextTypeId )
        {
            return this.ContainsKey( productTextTypeId ) ? this[this._PrimaryKeyDictionary[productTextTypeId]] : new ProductTextEntity();
        }

        /// <summary>   Gets the value of the given Text type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productTextTypeId">    Identifies the
        ///                                     <see cref="Dapper.Entities.ProductTextTypeEntity"/>. </param>
        /// <returns>   A String. </returns>
        public string Getter( int productTextTypeId )
        {
            return this.ContainsKey( productTextTypeId ) ? this[this._PrimaryKeyDictionary[productTextTypeId]].Label : string.Empty;
        }

        /// <summary>   Set the specified Product value. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productTextTypeId">    Identifies the
        ///                                     <see cref="Dapper.Entities.ProductTextTypeEntity"/>. </param>
        /// <param name="value">                The value. </param>
        public void Setter( int productTextTypeId, string value )
        {
            if ( this.ContainsKey( productTextTypeId ) )
            {
                this[this._PrimaryKeyDictionary[productTextTypeId]].Label = value;
            }
            else
            {
                this.Add( new ProductTextEntity() { ProductAutoId = ProductAutoId, ProductTextTypeId = productTextTypeId, Label = value } );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ProductEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </value>
        public int ProductAutoId { get; private set; }

        #endregion

    }
}

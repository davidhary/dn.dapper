using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Dapper.Entities
{

    /// <summary>   The Product Sort Trait class holing the Product Sort trait values. </summary>
    /// <remarks>   David, 2020-05-29. </remarks>
    public class ProductSortTrait : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-28. </remarks>
        /// <param name="getterSestter">    The getter setter. </param>
        public ProductSortTrait( Std.Primitives.IGetterSetter<int> getterSestter ) : base()
        {
            this.GetterSetter = getterSestter;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>   Gets or sets the getter setter. </summary>
        /// <value> The getter setter. </value>
        public Std.Primitives.IGetterSetter<int> GetterSetter { get; set; }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) The name. </param>
        /// <returns>   The trait value. </returns>
        protected int? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.GetterSetter.Getter( name );
        }

        /// <summary>   Trait value setter. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void Setter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue && !Nullable.Equals( value, this.Getter( name ) ) )
            {
                _ = this.GetterSetter.Setter( value.Value, name );
                this.NotifyPropertyChanged( name );
            }
        }

        #endregion

        #region " TRAITS "

        /// <summary>   Gets or sets the continuous failure count limit. </summary>
        /// <value> The continuous failure count limit. </value>
        public int? ContinuousFailureCountLimit
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the digital output. </summary>
        /// <value> The digital output. </value>
        public int? DigitalOutput
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the bucket number. </summary>
        /// <value> The bucket number. </value>
        public int? BucketNumber
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the bucket Bin. </summary>
        /// <value> The bucket Bin. </value>
        public int? BucketBin
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        /// <summary>   Gets or sets the background color of the bucket. </summary>
        /// <value> The color of the bucket. </value>
        public int? BucketBackColor
        {
            get => this.Getter();

            set => this.Setter( value );
        }

        #endregion

    }
}

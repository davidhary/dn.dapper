using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Product Sort Trait builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ProductSortTraitBuilder : OneToManyNaturalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ProductSortTraitNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = ProductSortBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( ProductNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = ProductSortTraitTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ProductSortTraitTypeNub.Id );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( ProductSortTraitEntity.ProductSortAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( ProductSortTraitEntity.ProductSortTraitTypeId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( ProductSortTraitEntity.Amount );

        #region " SINGLETON "

        private static readonly Lazy<ProductSortTraitBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ProductSortTraitBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="ProductSortTraitEntity"/>
    /// <see cref="IOneToManyNatural">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "ProductSortTrait" )]
    public class ProductSortTraitNub : OneToManyNaturalNub, IOneToManyNatural
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductSortTraitNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyNatural CreateNew()
        {
            return new ProductSortTraitNub();
        }
    }

    /// <summary>
    /// The <see cref="ProductSortTraitEntity"/> stores product Sort Traits.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class ProductSortTraitEntity : EntityBase<IOneToManyNatural, ProductSortTraitNub>, IOneToManyNatural
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductSortTraitEntity() : this( new ProductSortTraitNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductSortTraitEntity( IOneToManyNatural value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ProductSortTraitEntity( IOneToManyNatural cache, IOneToManyNatural store ) : base( new ProductSortTraitNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductSortTraitEntity( ProductSortTraitEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ProductSortTraitBuilder.TableName, nameof( IOneToManyNatural ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToManyNatural CreateNew()
        {
            return new ProductSortTraitNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyNatural CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNaturalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies from given entity. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The <see cref="ProductSortTraitEntity"/> interface value. </param>
        public override void CopyFrom( IOneToManyNatural value )
        {
            OneToManyNaturalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    the <see cref="ProductSortEntity"/>Value interface. </param>
        public override void UpdateCache( IOneToManyNatural value )
        {
            // first make the copy to notify of any property change.
            OneToManyNaturalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">               The connection. </param>
        /// <param name="productSortAutoId">        Identifies the <see cref="ProductSortEntity"/>
        ///                                         record. </param>
        /// <param name="productSortTraitTypeId">   Identifies the
        ///                                         <see cref="Dapper.Entities.ProductSortTraitTypeEntity"/>
        ///                                         value type. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int productSortAutoId, int productSortTraitTypeId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductSortTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductSortTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productSortAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductSortTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<ProductSortTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-15. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="productSortAutoId">        Identifies the <see cref="ProductSortEntity"/>
        ///                                         record. </param>
        /// <param name="productSortTraitTypeId">   Identifies the
        ///                                         <see cref="Dapper.Entities.ProductSortTraitTypeEntity"/>
        ///                                         value type. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int productSortAutoId, int productSortTraitTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, productSortAutoId, productSortTraitTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PrimaryId, this.SecondaryId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToManyNatural entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="productSortAutoId">        Identifies the <see cref="ProductSortEntity"/>
        ///                                         record. </param>
        /// <param name="productSortTraitTypeId">   Type of the
        ///                                         <see cref="Dapper.Entities.ProductSortTraitTypeEntity"/>
        ///                                         value. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int productSortAutoId, int productSortTraitTypeId )
        {
            return connection.Delete( new ProductSortTraitNub() { PrimaryId = productSortAutoId, SecondaryId = productSortTraitTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>
        /// Gets or sets the <see cref="ProductSortTraitEntity"/>'s.
        /// </summary>
        /// <value> the <see cref="ProductSortTraitEntity"/>'s. </value>
        public IEnumerable<ProductSortTraitEntity> ProductSortTraits { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductSortTraitEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToManyNatural>() ) : Populate( connection.GetAll<ProductSortTraitNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ProductSortTraits = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ProductSortTraitEntity.ProductSortTraits ) );
            return this.ProductSortTraits?.Any() == true ? this.ProductSortTraits.Count() : 0;
        }

        /// <summary>   Count entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Entities.ProductSortTraitBuilder.TableName}] WHERE {nameof( ProductSortTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productSortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductSortTraitEntity> FetchEntities( System.Data.IDbConnection connection, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductSortTraitBuilder.TableName}] WHERE {nameof( ProductSortTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productSortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ProductSortTraitNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches Product Sort Traits by product sort auto id. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="productSortAutoId">    Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductSortTraitNub> FetchNubs( System.Data.IDbConnection connection, int productSortAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductSortTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductSortTraitEntity.PrimaryId )} = @PrimaryId", new { PrimaryId = productSortAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductSortTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Populates a list of ProductSortValue entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="nubs"> the <see cref="ProductSortEntity"/>Value nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductSortTraitEntity> Populate( IEnumerable<ProductSortTraitNub> nubs )
        {
            var l = new List<ProductSortTraitEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ProductSortTraitNub nub in nubs )
                    l.Add( new ProductSortTraitEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of ProductSortValue entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="interfaces">   the <see cref="ProductSortEntity"/>Value interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductSortTraitEntity> Populate( IEnumerable<IOneToManyNatural> interfaces )
        {
            var l = new List<ProductSortTraitEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ProductSortTraitNub();
                foreach ( IOneToManyNatural iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ProductSortTraitEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Product Sort Traits by unique index; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">               The connection. </param>
        /// <param name="productSortAutoId">        Identifies the <see cref="ProductSortEntity"/>
        ///                                         record. </param>
        /// <param name="productSortTraitTypeId">   Type of the <see cref="ProductSortEntity"/> value. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productSortAutoId, int productSortTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductSortTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductSortTraitNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productSortAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductSortTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches Product Sort Traits by unique index; expected single or none. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">               The connection. </param>
        /// <param name="productSortAutoId">        Identifies the <see cref="ProductSortEntity"/>
        ///                                         record. </param>
        /// <param name="productSortTraitTypeId">   Identifies the <see cref="ProductSortEntity"/>
        ///                                         value type. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<ProductSortTraitNub> FetchEntities( System.Data.IDbConnection connection, int productSortAutoId, int productSortTraitTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductSortTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductSortTraitNub.PrimaryId )} = @primaryId", new { primaryId = productSortAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductSortTraitNub.SecondaryId )} = @SecondaryId", new { SecondaryId = productSortTraitTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductSortTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="ProductSortEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="productSortAutoId">        Identifies the <see cref="ProductSortEntity"/>. </param>
        /// <param name="productSortTraitTypeId">   Identifies the
        ///                                         <see cref="Dapper.Entities.ProductSortTraitTypeEntity"/>
        ///                                         value type. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int productSortAutoId, int productSortTraitTypeId )
        {
            return 1 == CountEntities( connection, productSortAutoId, productSortTraitTypeId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Count values associated with this session suite. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of records. </returns>
        public int CountProductSortTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductSortTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductSortTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Product Sort Traits;
        /// expected single or none.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public virtual IEnumerable<ProductSortTraitNub> FetchProductSortTraits( System.Data.IDbConnection connection )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductSortTraitBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductSortTraitNub.PrimaryId )} = @PrimaryId", new { this.PrimaryId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductSortTraitNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Gets or sets the <see cref="ProductSortEntity"/> Entity. </summary>
        /// <value> the <see cref="ProductSortEntity"/> Entity. </value>
        public ProductSortEntity ProductSortEntity { get; private set; }

        /// <summary>   Fetches a ProductSort Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchProductSortEntity( System.Data.IDbConnection connection )
        {
            this.ProductSortEntity = new ProductSortEntity();
            return this.ProductSortEntity.FetchUsingKey( connection, this.PrimaryId );
        }

        /// <summary>
        /// Gets or sets the <see cref="ProductSortEntity"/>
        /// <see cref="Dapper.Entities.ProductSortTraitTypeEntity"/>.
        /// </summary>
        /// <value> the <see cref="ProductSortEntity"/> value type entity. </value>
        public ProductSortTraitEntity ProductSortTraitTypeEntity { get; private set; }

        /// <summary>   Fetches a <see cref="Dapper.Entities.ProductSortTraitTypeEntity"/>. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="productSortAttributeId">   Identifies the product sort attribute. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchProductSortTraitTypeEntity( System.Data.IDbConnection connection, int productSortAttributeId )
        {
            this.ProductSortTraitTypeEntity = new ProductSortTraitEntity();
            return this.ProductSortTraitTypeEntity.FetchUsingKey( connection, this.ProductSortAutoId, productSortAttributeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductSortTraitEntity.ProductSortAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="ProductSortEntity"/> record. </summary>
        /// <value> Identifies the <see cref="ProductSortEntity"/> record. </value>
        public int ProductSortAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }


        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductSortTraitEntity.ProductSortTraitTypeId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.ProductSortTraitTypeEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ProductSortTraitTypeEntity"/>. </value>
        public int ProductSortTraitTypeId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        /// <summary>
        /// Gets or sets a Natural (whole number) value assigned to the <see cref="ProductSortEntity"/>
        /// for the specific <see cref="ProductSortTraitTypeEntity"/>.
        /// </summary>
        /// <value>
        /// The value assigned to the <see cref="ProductSortEntity"/> for the specific
        /// <see cref="ProductSortTraitTypeEntity"/>.
        /// </value>
        public int Amount
        {
            get => this.ICache.Amount;

            set {
                if ( !object.Equals( this.Amount, value ) )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the entity unique key selector. </summary>
        /// <value> The selector. </value>
        public DualKeySelector EntitySelector => new( this );

        #endregion

    }


    /// <summary>   Collection of <see cref="ProductSortTraitEntity"/>'s. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class ProductSortTraitEntityCollection : EntityKeyedCollection<DualKeySelector, IOneToManyNatural, ProductSortTraitNub, ProductSortTraitEntity>
    {
        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override DualKeySelector GetKeyForItem( ProductSortTraitEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.EntitySelector;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( ProductSortTraitEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all productSorts from the
        /// <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<ProductSortTraitEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( ProductSortTraitEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return ProductSortTraitBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>
    /// Collection of <see cref="ProductSortEntity"/> unique <see cref="ProductSortTraitEntity"/>'s.
    /// </summary>
    /// <remarks>   David, 2020-05-05. </remarks>
    public class ProductUniqueSortTraitEntityCollection : ProductSortTraitEntityCollection, Std.Primitives.IGetterSetter<int>
    {

        #region " CONSTRUCTION "


        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public ProductUniqueSortTraitEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<DualKeySelector, int>();
            this._PrimaryKeyDictionary = new Dictionary<int, DualKeySelector>();
            this.ProductSortTrait = new ProductSortTrait( this );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="productSortAutoId">    Identifies the <see cref="ProductSortEntity"/>. </param>
        public ProductUniqueSortTraitEntityCollection( int productSortAutoId ) : this()
        {
            this.ProductSortAutoId = productSortAutoId;
        }

        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<DualKeySelector, int> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<int, DualKeySelector> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( ProductSortTraitEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( entity.ProductSortTraitTypeId, entity.EntitySelector );
            this._UniqueIndexDictionary.Add( entity.EntitySelector, entity.ProductSortTraitTypeId );
            this.NotifyPropertyChanged( ProductSortTraitTypeEntity.EntityLookupDictionary()[entity.ProductSortTraitTypeId].Label );
        }

        /// <summary>
        /// Removes all productSorts from the
        /// <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        #endregion

        #region " GETTER SETTER "

        /// <summary>
        /// Gets the Real(Integer)-value for the given <see cref="ProductSortTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> Name of the runtime caller member. </param>
        /// <returns>   A Nullable Integer. </returns>
        int? IGetterSetter<int>.Getter( string name )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Set the specified element value for the given <see cref="ProductSortTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     Name of the runtime caller member. </param>
        /// <returns>   A Integer. </returns>
        int IGetterSetter<int>.Setter( int value, string name )
        {
            return this.SetterThis( value, name );
        }


        /// <summary>
        /// Gets the Real(Integer)-value for the given <see cref="ProductSortTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Nullable Integer. </returns>
        protected int? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>
        /// Set the specified value for the given <see cref="ProductSortTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Integer. </returns>
        private int SetterThis( int value, string name )
        {
            int key = this.ToKey( name );
            if ( !int.Equals( value, this.Getter( key ) ) )
            {
                this.Setter( key, value );
                this.NotifyPropertyChanged( name );
            }
            return value;
        }

        /// <summary>
        /// Set the specified value for the given <see cref="ProductSortTraitTypeEntity.Label"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="value">    value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        /// <returns>   A Integer. </returns>
        protected int Setter( int value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.SetterThis( value, name );
        }

        /// <summary>   Gets the trait value. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller member. </param>
        /// <returns>   The Trait value. Returns <see cref="Double.NaN"/> if value does not exist. </returns>
        protected int? TraitValueGetter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            return this.Getter( this.ToKey( name ) );
        }

        /// <summary>   Trait value setter. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) Name of the runtime caller member. </param>
        protected void TraitValueSetter( int? value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( value.HasValue )
            {
                _ = this.SetterThis( value.Value, name );
            }
        }

        /// <summary>   Gets or sets the product Sort Trait. </summary>
        /// <value> The product Sort Trait. </value>
        public ProductSortTrait ProductSortTrait { get; private set; }

        #endregion

        #region " TRAIT SELECTION "

        /// <summary>   Queries if collection contains a key. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productSortTraitTypeId">   Identifies the
        ///                                         <see cref="ProductSortTraitTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( int productSortTraitTypeId )
        {
            return this._PrimaryKeyDictionary.ContainsKey( productSortTraitTypeId );
        }

        /// <summary>
        /// Converts a name to a key using the
        /// <see cref="ProductSortTraitTypeEntity.KeyLookupDictionary()"/> lookup. This design allows to
        /// extend the element Nominal Traits beyond the values of the enumeration type that is used to
        /// populate this table.
        /// </summary>
        /// <remarks>   David, 2020-05-24. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns>   Name as an Integer. </returns>
        protected virtual int ToKey( string name )
        {
            return ProductSortTraitTypeEntity.KeyLookupDictionary()[name];
        }

        /// <summary>   gets the entity associated with the specified type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productSortTraitTypeId">   Identified for the
        ///                                         <see cref="ProductSortTraitTypeEntity"/>. </param>
        /// <returns>   An ProductSortAttributeNaturalEntity. </returns>
        public ProductSortTraitEntity Entity( int productSortTraitTypeId )
        {
            return this.ContainsKey( productSortTraitTypeId ) ? this[this._PrimaryKeyDictionary[productSortTraitTypeId]] : new ProductSortTraitEntity();
        }

        /// <summary>   Gets the value of the given reading type. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productSortTraitTypeId">   Identified for the
        ///                                         <see cref="ProductSortTraitTypeEntity"/>. </param>
        /// <returns>   An Integer? </returns>
        public int? Getter( int productSortTraitTypeId )
        {
            return this.ContainsKey( productSortTraitTypeId ) ? this[this._PrimaryKeyDictionary[productSortTraitTypeId]].Amount : new int?();
        }

        /// <summary>   Set the specified ProductSort value. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="productSortTraitTypeId">   Identified for the
        ///                                         <see cref="ProductSortTraitTypeEntity"/>. </param>
        /// <param name="value">                    The value. </param>
        public void Setter( int productSortTraitTypeId, int value )
        {
            if ( this.ContainsKey( productSortTraitTypeId ) )
            {
                this[this._PrimaryKeyDictionary[productSortTraitTypeId]].Amount = value;
            }
            else
            {
                this.Add( new ProductSortTraitEntity() { ProductSortAutoId = ProductSortAutoId, ProductSortTraitTypeId = productSortTraitTypeId, Amount = value } );
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="ProductSortEntity"/>. </summary>
        /// <value> Identifies the <see cref="ProductSortEntity"/>. </value>
        public int ProductSortAutoId { get; private set; }

        #endregion

    }
}

﻿
namespace isr.Dapper.Entities
{
    /// <summary>   A product entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class ProductEntity
    {

        /// <summary>
        /// Gets or sets the <see cref="ProductUniqueElementEntityCollection">product elements</see>.
        /// </summary>
        /// <value> The Product elements entities. </value>
        public ProductUniqueElementEntityCollection Elements { get; private set; }

        /// <summary>
        /// Fetches the <see cref="ProductUniqueElementEntityCollection">product elements</see>.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of Elements. </returns>
        public int FetchElements( System.Data.IDbConnection connection )
        {
            this.Elements = FetchElements( connection, this.AutoId );
            return (this.Elements?.Count).GetValueOrDefault( 0 );
        }

        /// <summary>
        /// Fetches the <see cref="ProductUniqueElementEntityCollection">product elements</see>.
        /// </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The Elements. </returns>
        public static ProductUniqueElementEntityCollection FetchElements( System.Data.IDbConnection connection, int productAutoId )
        {
            if ( !ElementTypeEntity.IsEnumerated() )
                _ = ElementTypeEntity.TryFetchAll( connection );
            var elements = new ProductUniqueElementEntityCollection( productAutoId );
            elements.Populate( ProductElementEntity.FetchOrderedElements( connection, productAutoId ) );
            _ = elements.FetchNomTypes( connection );
            return elements;
        }
    }
}
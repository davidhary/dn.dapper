using System;
using System.Linq;

using FastEnums;

namespace isr.Dapper.Entities
{
    /// <summary>   A product entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class ProductEntity
    {

        /// <summary>
        /// Gets or sets the <see cref=" ProductUniqueProductSortEntityCollection">Product ProductSort
        /// values</see>.
        /// </summary>
        /// <value> The Product ProductSort trait values. </value>
        public ProductUniqueProductSortEntityCollection ProductSorts { get; private set; }

        /// <summary>
        /// Fetches <see cref=" ProductUniqueProductSortEntityCollection">Product ProductSort
        /// values</see>.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of ProductSorts. </returns>
        public int FetchProductSorts( System.Data.IDbConnection connection )
        {
            this.ProductSorts = FetchProductSorts( connection, this.AutoId );
            return this.ProductSorts?.Any() == true ? this.ProductSorts.Count : 0;
        }

        /// <summary>
        /// Fetches <see cref="ProductUniqueProductSortEntityCollection">Product ProductSort values</see>.
        /// </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The ProductSorts. </returns>
        public static ProductUniqueProductSortEntityCollection FetchProductSorts( System.Data.IDbConnection connection, int productAutoId )
        {
            if ( !ProductSortTraitTypeEntity.IsEnumerated() )
                _ = ProductSortTraitTypeEntity.TryFetchAll( connection );
            if ( !BucketBinEntity.IsEnumerated() )
                _ = BucketBinEntity.TryFetchAll( connection );
            var productSorts = new ProductUniqueProductSortEntityCollection( productAutoId );
            _ = productSorts.Populate( ProductSortEntity.FetchEntities( connection, productAutoId ) );
            foreach ( ProductSortEntity productSort in productSorts )
                _ = productSort.FetchTraitEntities( connection );
            return productSorts;
        }

        /// <summary>   Assigns product sort buckets and bucket bins. </summary>
        /// <remarks>   David, 2020-07-04. </remarks>
        /// <returns>   An Integer. </returns>
        public virtual int AssignProductSortBuckets()
        {
            ProductSortEntity productSortEntity;
            int count = 0;
            foreach ( ProductSort productSort in Enum.GetValues( typeof( ProductSort ) ) )
            {
                productSortEntity = this.ProductSorts.ProductSort( productSort.Description() );
                var sort = productSortEntity.ProductSortTraits.ProductSortTrait;
                sort.BucketNumber = BucketBinEntity.ToBucketNumber( productSort );
                count += 1;
                sort.BucketBin = ( int? ) BucketBinEntity.ProductSortBinMapper[( int ) productSort];
                count += 1;
                sort.BucketBackColor = BucketBinEntity.BackColorMapper[BucketBinEntity.ProductSortBinMapper[( int ) productSort]];
                count += 1;
                sort.DigitalOutput = BucketBinEntity.DigitalOutputMapper[BucketBinEntity.ProductSortBinMapper[( int ) productSort]];
                count += 1;
                sort.ContinuousFailureCountLimit = BucketBinEntity.ContinuousFailureCountLimitMapper[BucketBinEntity.ProductSortBinMapper[( int ) productSort]];
                count += 1;
            }

            return count;
        }

        /// <summary>   Assign product sort traits. </summary>
        /// <remarks>   David, 2020-07-07. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="productSort">                  The product sort. </param>
        /// <param name="bucketNumber">                 The bucket number. </param>
        /// <param name="bucketBin">                    The bucket bin. </param>
        /// <param name="bucketBackColor">              The bucket back color. </param>
        /// <param name="digitalOutput">                The digital output. </param>
        /// <param name="continuousFailureCountLimit">  The continuous failure count limit. </param>
        /// <returns>   A ProductSortEntity. </returns>
        public ProductSortEntity AssignProductSortTraits( ProductSort productSort, int bucketNumber, int bucketBin, int bucketBackColor, int digitalOutput, int continuousFailureCountLimit )
        {
            string sortLabel = productSort.Description();
            var productSortEntity = this.ProductSorts.ProductSort( sortLabel );
            (bool Success, string Details) r;
            r = productSortEntity.ValidateStoredEntity( $"{nameof( ProductSortEntity )} with {nameof( ProductSortEntity.Label )} of {sortLabel}" );
            if ( !r.Success )
                throw new InvalidOperationException( r.Details );
            var sort = productSortEntity.ProductSortTraits.ProductSortTrait;
            sort.BucketNumber = bucketNumber;
            sort.BucketBin = bucketBin;
            sort.BucketBackColor = bucketBackColor;
            sort.DigitalOutput = digitalOutput;
            sort.ContinuousFailureCountLimit = continuousFailureCountLimit;
            return productSortEntity;
        }
    }
}

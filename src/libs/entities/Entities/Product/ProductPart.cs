using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Product-Part builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class ProductPartBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( ProductPartNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = ProductBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( ProductNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = PartBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( PartNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( ProductPartEntity.ProductAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( ProductPartEntity.PartAutoId );

        #region " SINGLETON "

        private static readonly Lazy<ProductPartBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static ProductPartBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Product Part Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "ProductPart" )]
    public class ProductPartNub : OneToManyNub, IOneToMany
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductPartNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new ProductPartNub();
        }
    }

    /// <summary>
    /// The Product-Part Entity based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-22 </para>
    /// </remarks>
    public class ProductPartEntity : EntityBase<IOneToMany, ProductPartNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        public ProductPartEntity() : this( new ProductPartNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductPartEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public ProductPartEntity( IOneToMany cache, IOneToMany store ) : base( new ProductPartNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public ProductPartEntity( ProductPartEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.ProductPartBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new ProductPartNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="value">    The Product-Part interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int productAutoId, int partAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, productAutoId, partAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.ProductAutoId, this.PartAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.ProductAutoId, this.PartAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the Product. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int productAutoId, int partAutoId )
        {
            return this.FetchUsingKey( connection, productAutoId, partAutoId );
        }

        /// <summary>
        /// Tries to fetch an existing or insert a new <see cref="Dapper.Entities.PartEntity"/>  and fetch an
        /// existing or insert a new <see cref="ProductPartEntity"/>. If the part number is non-unique, a
        /// part is inserted if this part number is not associated with this product.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.ProductEntity"/> exists for the specified
        /// <paramref name="productAutoId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="partNumber">       The part number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainPart( System.Data.IDbConnection connection, int productAutoId, string partNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( PartBuilder.Instance.UsingUniqueLabel( connection ) )
            {
                // if using a unique part number, the part number is unique across all products
                this.PartEntity = new PartEntity() { PartNumber = partNumber };
                if ( !this.PartEntity.Obtain( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( Dapper.Entities.PartEntity )} with {nameof( Dapper.Entities.PartEntity.PartNumber )} of {partNumber}");
                }
            }
            else
            {
                // if not using a unique part number, the part number is unique for this product
                this.PartEntity = FetchPart( connection, productAutoId, partNumber );
                if ( !this.PartEntity.IsClean() )
                {
                    this.PartEntity = new PartEntity() { PartNumber = partNumber };
                    if ( !this.PartEntity.Insert( connection ) )
                    {
                        result = (false, $"Failed inserting {nameof( Dapper.Entities.PartEntity )} with {nameof( Dapper.Entities.PartEntity.PartNumber )} of {partNumber}");
                    }
                }
            }

            if ( result.Success )
            {
                this.PrimaryId = productAutoId;
                this.SecondaryId = this.PartEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( ProductPartEntity.PartEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( ProductPartEntity )} with {nameof( this.ProductAutoId )} of {productAutoId} and {nameof( this.PartAutoId )} of {this.PartEntity.AutoId}");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.ProductEntity"/> and
        /// <see cref="Dapper.Entities.PartEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="ProductPartEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="partNumber">       The part number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int productAutoId, string partNumber )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.ProductEntity is null || this.ProductEntity.AutoId != productAutoId )
            {
                // make sure a product exists for the auto id.
                this.ProductEntity = new ProductEntity() { AutoId = productAutoId };
                if ( !this.ProductEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.ProductEntity )} with {nameof( Dapper.Entities.ProductEntity.AutoId )} of {productAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainPart( connection, productAutoId, partNumber );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( ProductPartEntity.ProductEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.ProductEntity"/> and
        /// <see cref="Dapper.Entities.PartEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="ProductPartEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="partNumber">   The part number. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, string partNumber )
        {
            return this.TryObtain( connection, this.ProductAutoId, partNumber );
        }

        /// <summary>
        /// Fetches an existing or inserts new <see cref="Dapper.Entities.ProductEntity"/> and
        /// <see cref="Dapper.Entities.PartEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="ProductPartEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the Product. </param>
        /// <param name="partNumber">       The part number. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool Obtain( System.Data.IDbConnection connection, int productAutoId, string partNumber )
        {
            var (Success, Details) = this.TryObtain( connection, productAutoId, partNumber );
            return !Success ? throw new InvalidOperationException( Details ) : Success;
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int productAutoId, int partAutoId )
        {
            return connection.Delete( new ProductPartNub() { PrimaryId = productAutoId, SecondaryId = partAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Product-Part entities. </summary>
        /// <value> The Product-Part entities. </value>
        public IEnumerable<ProductPartEntity> ProductParts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductPartEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<ProductPartNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ProductParts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( ProductPartEntity.ProductParts ) );
            return this.ProductParts?.Any() == true ? this.ProductParts.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductPartEntity> Populate( IEnumerable<ProductPartNub> nubs )
        {
            var l = new List<ProductPartEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( ProductPartNub nub in nubs )
                    l.Add( new ProductPartEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<ProductPartEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<ProductPartEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new ProductPartNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new ProductPartEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Part entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductPartBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductPartNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductPartEntity> FetchEntities( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductPartBuilder.TableName}] WHERE {nameof( ProductPartNub.PrimaryId )} = @Id", new { Id = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ProductPartNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Part. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The total number of entities by Part. </returns>
        public static int CountEntitiesByPart( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductPartBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductPartNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Parts in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Parts in this
        /// collection.
        /// </returns>
        public static IEnumerable<ProductPartEntity> FetchEntitiesByPart( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductPartBuilder.TableName}] WHERE {nameof( ProductPartNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<ProductPartNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int productAutoId, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{ProductPartBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductPartNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductPartNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<ProductPartNub> FetchNubs( System.Data.IDbConnection connection, int productAutoId, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{ProductPartBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( ProductPartNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            _ = sqlBuilder.Where( $"{nameof( ProductPartNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<ProductPartNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Product Part exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="partAutoId">       Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int productAutoId, int partAutoId )
        {
            return 1 == CountEntities( connection, productAutoId, partAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Product entity. </summary>
        /// <value> The Product entity. </value>
        public ProductEntity ProductEntity { get; private set; }

        /// <summary>   Fetches Product Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Product Entity. </returns>
        public ProductEntity FetchProductEntity( System.Data.IDbConnection connection )
        {
            var entity = new ProductEntity();
            _ = entity.FetchUsingKey( connection, this.ProductAutoId );
            this.ProductEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Products associated with the specified <paramref name="partAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   The total number of Products. </returns>
        public static int CountProducts( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.ProductPartBuilder.TableName}] WHERE {nameof( ProductPartNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Products associated with the specified <paramref name="partAutoId"/>; expected a
        /// single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Products in this collection.
        /// </returns>
        public static IEnumerable<PartEntity> FetchProducts( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductPartBuilder.TableName}] WHERE {nameof( ProductPartNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<PartEntity>();
            foreach ( ProductPartNub nub in connection.Query<ProductPartNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new ProductPartEntity( nub );
                l.Add( entity.FetchPartEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Products associated with the specified <paramref name="partAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="partAutoId">   Identifies the <see cref="Dapper.Entities.PartEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteProducts( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.ProductPartBuilder.TableName}] WHERE {nameof( ProductPartNub.SecondaryId )} = @SecondaryId", new { SecondaryId = partAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the Part entity. </summary>
        /// <value> The Part entity. </value>
        public PartEntity PartEntity { get; private set; }

        /// <summary>   Fetches a Part Entity. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Part Entity. </returns>
        public PartEntity FetchPartEntity( System.Data.IDbConnection connection )
        {
            var entity = new PartEntity();
            _ = entity.FetchUsingKey( connection, this.PartAutoId );
            this.PartEntity = entity;
            return entity;
        }

        /// <summary>   Count parts. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   The total number of parts. </returns>
        public static int CountParts( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.ProductPartBuilder.TableName}] WHERE {nameof( ProductPartNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Parts in this collection. </summary>
        /// <remarks>   David, 2020-10-06. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Parts in this collection.
        /// </returns>
        public static IEnumerable<PartEntity> FetchParts( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.ProductPartBuilder.TableName}] WHERE {nameof( ProductPartNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<PartEntity>();
            foreach ( ProductPartNub nub in connection.Query<ProductPartNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new ProductPartEntity( nub );
                l.Add( entity.FetchPartEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all Part related to the specified Product. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteParts( System.Data.IDbConnection connection, int productAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.ProductPartBuilder.TableName}] WHERE {nameof( ProductPartNub.PrimaryId )} = @PrimaryId", new { PrimaryId = productAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="selectQuery">      The select query. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="partLabel">        The part label. </param>
        /// <returns>   The part. </returns>
        public static PartEntity FetchPart( System.Data.IDbConnection connection, string selectQuery, int productAutoId, string partLabel )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = productAutoId, Label = partLabel } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<PartNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new PartEntity() : new PartEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="productAutoId">    Identifies the <see cref="Dapper.Entities.ProductEntity"/>. </param>
        /// <param name="partLabel">        The part label. </param>
        /// <returns>   The part. </returns>
        public static PartEntity FetchPart( System.Data.IDbConnection connection, int productAutoId, string partLabel )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [ProductPart] on [ProductPart].SecondaryId = [Part].AutoId where [ProductPart].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{PartBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{PartBuilder.TableName}] Inner Join [{ProductPartBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.ProductPartBuilder.TableName}].{nameof( ProductPartNub.SecondaryId )} = [{isr.Dapper.Entities.PartBuilder.TableName}].{nameof( PartNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.ProductPartBuilder.TableName}].{nameof( ProductPartNub.PrimaryId )} = @Id AND [{isr.Dapper.Entities.PartBuilder.TableName}].{nameof( PartNub.Label )} = @Label); " );
            return FetchPart( connection, queryBuilder.ToString(), productAutoId, partLabel );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductPartEntity.ProductAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Product. </summary>
        /// <value> Identifies the Product. </value>
        public int ProductAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( ProductPartEntity.PartAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.PartEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.PartEntity"/>. </value>
        public int PartAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

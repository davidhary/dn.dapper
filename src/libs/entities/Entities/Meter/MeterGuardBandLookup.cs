using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the Meter Guard Band Lookup nub and entity. Includes the fields as kept in the
    /// data table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value. <para>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IMeterGuardBandLookup
    {

        /// <summary>   Gets or sets the id of the GuardBandLookup. </summary>
        /// <value> Identifies the GuardBandLookup. </value>
        [ExplicitKey]
        int Id { get; set; }

        /// <summary>   Gets or sets the tolerance code. </summary>
        /// <value> The tolerance code. </value>
        string ToleranceCode { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        int MeterId { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        int NomTypeId { get; set; }

        /// <summary>   Gets or sets the minimum inclusive value. </summary>
        /// <value> The minimum inclusive value. </value>
        double? MinimumInclusiveValue { get; set; }

        /// <summary>   Gets or sets the maximum exclusive value. </summary>
        /// <value> The maximum exclusive value. </value>
        double? MaximumExclusiveValue { get; set; }

        /// <summary>   Gets or sets the guard band factor. </summary>
        /// <value> The guard band factor. </value>
        double GuardBandFactor { get; set; }
    }

    /// <summary>   A meter guard band lookup builder. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public sealed class MeterGuardBandLookupBuilder
    {

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( MeterGuardBandLookupNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the Meter table. </summary>
        /// <value> The name of the Meter table. </value>
        public static string MeterTableName { get; set; } = MeterBuilder.TableName;

        /// <summary>   Gets or sets the name of the Meter table key. </summary>
        /// <value> The name of the Meter table key. </value>
        public static string MeterTableKeyName { get; set; } = nameof( MeterNub.Id );

        /// <summary>   Gets or sets the name of the Nominal (measurement) type table. </summary>
        /// <value> The name of the Nominal type table. </value>
        public static string NomTypeTableName { get; set; } = NomTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the Nominal (measurement) type table key. </summary>
        /// <value> The name of the Nominal type table key. </value>
        public static string NomTypeTableKeyName { get; set; } = nameof( NomTypeNub.Id );

        /// <summary>   Inserts values from file. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="fileName">     Filename of the file. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertFileValues( System.Data.IDbConnection connection, string fileName )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : fileName is null
                ? throw new ArgumentNullException( nameof( fileName ) )
                : connection.Execute( System.IO.File.ReadAllText( fileName ) );
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}] (
            [{nameof( MeterGuardBandLookupNub.Id )}] integer NOT NULL PRIMARY KEY, 
            [{nameof( MeterGuardBandLookupNub.ToleranceCode )}] [nchar](1) NOT NULL,
            [{nameof( MeterGuardBandLookupNub.MeterId )}] integer NOT NULL DEFAULT (1),
            [{nameof( MeterGuardBandLookupNub.NomTypeId )}] integer NOT NULL DEFAULT (1), 
            [{nameof( MeterGuardBandLookupNub.MinimumInclusiveValue )}] [float] NULL,
            [{nameof( MeterGuardBandLookupNub.MaximumExclusiveValue )}] [float] NULL,
            [{nameof( MeterGuardBandLookupNub.GuardBandFactor )}] [float] NOT NULL,
            FOREIGN KEY ([{nameof( MeterGuardBandLookupNub.NomTypeId )}]) REFERENCES [{isr.Dapper.Entities.MeterGuardBandLookupBuilder.NomTypeTableName}] ([{isr.Dapper.Entities.MeterGuardBandLookupBuilder.NomTypeTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE, 
            FOREIGN KEY ([{nameof( MeterGuardBandLookupNub.ToleranceCode )}]) REFERENCES [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([{nameof( ToleranceNub.ToleranceCode )}]) ON UPDATE CASCADE ON DELETE CASCADE, 
            FOREIGN KEY ([{nameof( MeterGuardBandLookupNub.MeterId )}]) REFERENCES [{isr.Dapper.Entities.MeterGuardBandLookupBuilder.MeterTableName}] ([{isr.Dapper.Entities.MeterGuardBandLookupBuilder.MeterTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}](
                [{nameof( MeterGuardBandLookupNub.Id )}] [int] NOT NULL,
                [{nameof( MeterGuardBandLookupNub.ToleranceCode )}] [nchar](1) NOT NULL,
                [{nameof( MeterGuardBandLookupNub.MeterId )}] [int] NOT NULL,
                [{nameof( MeterGuardBandLookupNub.NomTypeId )}] [int] NOT NULL,
                [{nameof( MeterGuardBandLookupNub.MinimumInclusiveValue )}] [float] NULL,
                [{nameof( MeterGuardBandLookupNub.MaximumExclusiveValue )}] [float] NULL,
                [{nameof( MeterGuardBandLookupNub.GuardBandFactor )}] [float] NOT NULL,
             CONSTRAINT [PK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}] PRIMARY KEY CLUSTERED 
            (
                [{nameof( MeterGuardBandLookupNub.Id )}] ASC
            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            ) ON [PRIMARY]
            END;

            IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{nameof( MeterGuardBandLookupNub.MeterId )}]') AND type = 'D')
            BEGIN
            ALTER TABLE [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}] ADD  CONSTRAINT [DF_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{nameof( MeterGuardBandLookupNub.MeterId )}]  DEFAULT ((1)) FOR [{nameof( MeterGuardBandLookupNub.MeterId )}]
            END;

            IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{nameof( MeterGuardBandLookupNub.NomTypeId )}]') AND type = 'D')
            BEGIN
            ALTER TABLE [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}] ADD  CONSTRAINT [DF_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{nameof( MeterGuardBandLookupNub.NomTypeId )}]  DEFAULT ((1)) FOR [{nameof( MeterGuardBandLookupNub.NomTypeId )}]
            END;

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.NomTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.NomTypeTableName}] FOREIGN KEY([{nameof( MeterGuardBandLookupNub.NomTypeId )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.NomTypeTableName}] ([{isr.Dapper.Entities.MeterGuardBandLookupBuilder.NomTypeTableKeyName}])
            ON UPDATE CASCADE
            ON DELETE CASCADE;

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.NomTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.NomTypeTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.MeterTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.MeterTableName}] FOREIGN KEY([{nameof( MeterGuardBandLookupNub.MeterId )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.MeterTableName}] ([{isr.Dapper.Entities.MeterGuardBandLookupBuilder.MeterTableKeyName}]);

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.MeterTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.MeterTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.ToleranceBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.ToleranceBuilder.TableName}] FOREIGN KEY([{nameof( MeterGuardBandLookupNub.ToleranceCode )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([{nameof( MeterGuardBandLookupNub.ToleranceCode )}])
            ON UPDATE CASCADE
            ON DELETE CASCADE;

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.ToleranceBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}_{isr.Dapper.Entities.ToleranceBuilder.TableName}]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// Implements the Meter Guard Band Lookup table
    /// <see cref="IMeterGuardBandLookup">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-19 </para>
    /// </remarks>
    [Table( "MeterGuardBandLookup" )]
    public class MeterGuardBandLookupNub : EntityNubBase<IMeterGuardBandLookup>, IMeterGuardBandLookup
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public MeterGuardBandLookupNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IMeterGuardBandLookup CreateNew()
        {
            return new MeterGuardBandLookupNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IMeterGuardBandLookup CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IMeterGuardBandLookup value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IMeterGuardBandLookup source, IMeterGuardBandLookup destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.GuardBandFactor = source.GuardBandFactor;
            destination.Id = source.Id;
            destination.MaximumExclusiveValue = source.MaximumExclusiveValue;
            destination.NomTypeId = source.NomTypeId;
            destination.MeterId = source.MeterId;
            destination.MinimumInclusiveValue = source.MinimumInclusiveValue;
            destination.ToleranceCode = source.ToleranceCode;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IMeterGuardBandLookup );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IMeterGuardBandLookup other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IMeterGuardBandLookup left, IMeterGuardBandLookup right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.GuardBandFactor, right.GuardBandFactor );
                result = result && Equals( left.Id, right.Id );
                result = result && string.Equals( left.ToleranceCode, right.ToleranceCode );
                result = result && Equals( left.NomTypeId, right.NomTypeId );
                result = result && Equals( left.MinimumInclusiveValue, right.MinimumInclusiveValue );
                result = result && Equals( left.MaximumExclusiveValue, right.MaximumExclusiveValue );
                result = result && Equals( left.MeterId, right.MeterId );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.GuardBandFactor, this.Id, this.ToleranceCode, this.NomTypeId, this.MinimumInclusiveValue, this.MaximumExclusiveValue, this.MeterId ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the GuardBandLookup. </summary>
        /// <value> Identifies the GuardBandLookup. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary>   Gets or sets the tolerance code. </summary>
        /// <value> The tolerance code. </value>
        public string ToleranceCode { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public int NomTypeId { get; set; }

        /// <summary>   Gets or sets the minimum inclusive value. </summary>
        /// <value> The minimum inclusive value. </value>
        public double? MinimumInclusiveValue { get; set; }

        /// <summary>   Gets or sets the maximum exclusive value. </summary>
        /// <value> The maximum exclusive value. </value>
        public double? MaximumExclusiveValue { get; set; }

        /// <summary>   Gets or sets the guard band factor. </summary>
        /// <value> The guard band factor. </value>
        public double GuardBandFactor { get; set; }

        #endregion

    }

    /// <summary>
    /// The Meter Guard Band Lookup Entity implementing the <see cref="IMeterGuardBandLookup"/>
    /// interface.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-19 </para>
    /// </remarks>
    public class MeterGuardBandLookupEntity : EntityBase<IMeterGuardBandLookup, MeterGuardBandLookupNub>, IMeterGuardBandLookup
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public MeterGuardBandLookupEntity() : this( new MeterGuardBandLookupNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public MeterGuardBandLookupEntity( IMeterGuardBandLookup value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public MeterGuardBandLookupEntity( IMeterGuardBandLookup cache, IMeterGuardBandLookup store ) : base( new MeterGuardBandLookupNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public MeterGuardBandLookupEntity( MeterGuardBandLookupEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName, nameof( IMeterGuardBandLookup ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IMeterGuardBandLookup CreateNew()
        {
            return new MeterGuardBandLookupNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IMeterGuardBandLookup CreateCopy()
        {
            var destination = this.CreateNew();
            MeterGuardBandLookupNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IMeterGuardBandLookup value )
        {
            MeterGuardBandLookupNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The GuardBandLookup interface. </param>
        public override void UpdateCache( IMeterGuardBandLookup value )
        {
            // first make the copy to notify of any property change.
            MeterGuardBandLookupNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The GuardBandLookup table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IMeterGuardBandLookup>( key ) : connection.Get<MeterGuardBandLookupNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="guardBandLookupId">    Identifies the guard band lookup. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int guardBandLookupId )
        {
            return this.FetchUsingKey( connection, guardBandLookupId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IMeterGuardBandLookup entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.Id ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new MeterGuardBandLookupNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the Meter Guard Band Lookup entities. </summary>
        /// <value> the Meter Guard Band Lookup entities. </value>
        public static IEnumerable<MeterGuardBandLookupEntity> MeterGuardBandLookups { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<MeterGuardBandLookupEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IMeterGuardBandLookup>() ) : Populate( connection.GetAll<MeterGuardBandLookupNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            MeterGuardBandLookups = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookups ) );
            return MeterGuardBandLookups?.Any() == true ? MeterGuardBandLookups.Count() : 0;
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<MeterGuardBandLookupEntity> FetchEntities( System.Data.IDbConnection connection, int meterModelId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.MeterGuardBandLookupBuilder.TableName}] WHERE {nameof( MeterGuardBandLookupNub.MeterId )} = @Id", new { Id = meterModelId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return Populate( connection.Query<MeterGuardBandLookupNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Populates a list of GuardBandLookup entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The GuardBandLookup nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<MeterGuardBandLookupEntity> Populate( IEnumerable<MeterGuardBandLookupNub> nubs )
        {
            var l = new List<MeterGuardBandLookupEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( MeterGuardBandLookupNub nub in nubs )
                    l.Add( new MeterGuardBandLookupEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of GuardBandLookup entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The GuardBandLookup interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<MeterGuardBandLookupEntity> Populate( IEnumerable<IMeterGuardBandLookup> interfaces )
        {
            var l = new List<MeterGuardBandLookupEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new MeterGuardBandLookupNub();
                foreach ( IMeterGuardBandLookup iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new MeterGuardBandLookupEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, MeterGuardBandLookupEntity> _EntityLookupDictionary;

        /// <summary>   The entity lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of Integer, MeterGuardBandLookupEntity) </returns>
        public static IDictionary<int, MeterGuardBandLookupEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, MeterGuardBandLookupEntity>();
                foreach ( MeterGuardBandLookupEntity entity in MeterGuardBandLookups )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (MeterGuardBandLookups?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " FIND "

        /// <summary>   Select meter guard band. </summary>
        /// <remarks>   David, 2020-05-31. </remarks>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="multmeterId">      Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="nominalValue">     The nominal value. </param>
        /// <returns>   A MeterGuardBandLookupEntity matching the provided arguments. </returns>
        public static MeterGuardBandLookupEntity SelectMeterGuardBandEntity( string toleranceCode, int multmeterId, int nomTypeId, double nominalValue )
        {
            var entity = new MeterGuardBandLookupEntity();
            foreach ( var currentEntity in MeterGuardBandLookups )
            {
                entity = currentEntity;
                if ( string.Equals( toleranceCode, entity.ToleranceCode ) && multmeterId == entity.MeterId && nomTypeId == entity.NomTypeId )
                {
                    if ( entity.MinimumInclusiveValue.HasValue && entity.MaximumExclusiveValue.HasValue && entity.MaximumExclusiveValue.Value < nominalValue && entity.MinimumInclusiveValue.Value >= nominalValue )
                    {
                        break;
                    }
                    else if ( entity.MinimumInclusiveValue.HasValue && entity.MinimumInclusiveValue.Value <= nominalValue )
                    {
                        break;
                    }
                    else if ( entity.MaximumExclusiveValue.HasValue && entity.MaximumExclusiveValue.Value >= nominalValue )
                    {
                        break;
                    }
                }
            }

            return entity;
        }



        /// <summary>   Builds where clause. </summary>
        /// <remarks>   David, 2020-06-17. </remarks>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="value">            the Meter Guard Band Lookup interface. </param>
        /// <returns>   A String. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        private static string BuildWhereClause( string toleranceCode, int meterId, int nomTypeId, double value )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"WHERE ({nameof( MeterGuardBandLookupEntity.ToleranceCode )} = @{nameof( toleranceCode )} AND " );
            _ = queryBuilder.AppendLine( $"{nameof( MeterGuardBandLookupEntity.MeterId )} = @{nameof( meterId )} AND " );
            _ = queryBuilder.AppendLine( $"{nameof( MeterGuardBandLookupEntity.NomTypeId )} = @{nameof( nomTypeId )} AND " );
            _ = queryBuilder.AppendLine( $"(({nameof( MeterGuardBandLookupEntity.MinimumInclusiveValue )} IS NULL AND {nameof( MeterGuardBandLookupEntity.MaximumExclusiveValue )} > @{nameof( value )}) OR " );
            _ = queryBuilder.AppendLine( $"({nameof( MeterGuardBandLookupEntity.MinimumInclusiveValue )} <= @{nameof( value )} AND {nameof( MeterGuardBandLookupEntity.MaximumExclusiveValue )} > @{nameof( value )}) OR " );
            _ = queryBuilder.AppendLine( $"({nameof( MeterGuardBandLookupEntity.MinimumInclusiveValue )} <= @{nameof( value )} AND {nameof( MeterGuardBandLookupEntity.MaximumExclusiveValue )} IS NULL) ) ); " );
            return queryBuilder.ToString();
        }

        /// <summary>   Count entities; returns number of records with the specified values. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="value">            the Meter Guard Band Lookup interface. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string toleranceCode, int meterId, int nomTypeId, double value )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( @$"select count(*) from [{MeterGuardBandLookupBuilder.TableName}] " );
            _ = queryBuilder.Append( BuildWhereClause( toleranceCode, meterId, nomTypeId, value ) );
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( queryBuilder.ToString(), new { toleranceCode, meterId, nomTypeId, value } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="value">            the Meter Guard Band Lookup interface. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<MeterGuardBandLookupNub> FetchNubs( System.Data.IDbConnection connection, string toleranceCode, int meterId, int nomTypeId, double value )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( @$"select * from [{MeterGuardBandLookupBuilder.TableName}] " );
            _ = queryBuilder.Append( BuildWhereClause( toleranceCode, meterId, nomTypeId, value ) );
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( queryBuilder.ToString(), new { toleranceCode, meterId, nomTypeId, value } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<MeterGuardBandLookupNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Meter exists. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="value">            the Meter Guard Band Lookup interface. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string toleranceCode, int meterId, int nomTypeId, double value )
        {
            return 1 == CountEntities( connection, toleranceCode, meterId, nomTypeId, value );
        }

        #endregion

        #region " RELATIONS "

        #region " TOLERANCE "

        /// <summary>   The tolerance entity. </summary>
        private ToleranceEntity _ToleranceEntity;

        /// <summary>   Gets the meter model entity. </summary>
        /// <value> The meter model entity. </value>
        public ToleranceEntity ToleranceEntity
        {
            get {
                if ( this._ToleranceEntity is null )
                {
                    this._ToleranceEntity = ToleranceEntity.EntityLookupDictionary()[this.ToleranceCode];
                }

                return this._ToleranceEntity;
            }
        }

        /// <summary>   Fetches the Tolerance entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchToleranceEntity( System.Data.IDbConnection connection )
        {
            if ( !ToleranceEntity.IsEnumerated() )
                _ = ToleranceEntity.TryFetchAll( connection );
            this._ToleranceEntity = ToleranceEntity.EntityLookupDictionary()[this.ToleranceCode];
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.ToleranceEntity ) );
            return this.ToleranceEntity is object;
        }

        #endregion

        #region " METER "

        /// <summary>   The meter entity. </summary>
        private MeterEntity _MeterEntity;

        /// <summary>   Gets the meter entity. </summary>
        /// <value> The meter entity. </value>
        public MeterEntity MeterEntity
        {
            get {
                if ( this._MeterEntity is null )
                {
                    this._MeterEntity = MeterEntity.EntityLookupDictionary()[this.MeterId];
                }

                return this._MeterEntity;
            }
        }

        /// <summary>   Fetches the meter. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchMeterEntity( System.Data.IDbConnection connection )
        {
            if ( !MeterEntity.IsEnumerated() )
                _ = MeterEntity.TryFetchAll( connection );
            this._MeterEntity = MeterEntity.EntityLookupDictionary()[this.MeterId];
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.MeterEntity ) );
            return this._MeterEntity is object;
        }

        #endregion

        #region " METER MODEL "

        /// <summary>   The meter model entity. </summary>
        private MeterModelEntity _MeterModelEntity;

        /// <summary>   Gets the meter model entity. </summary>
        /// <value> The meter model entity. </value>
        public MeterModelEntity MeterModelEntity
        {
            get {
                if ( this._MeterModelEntity is null )
                {
                    this._MeterModelEntity = this.MeterEntity.MeterModelEntity;
                }

                return this._MeterModelEntity;
            }
        }

        /// <summary>   Fetches the meter model entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchMeterModel( System.Data.IDbConnection connection )
        {
            _ = this.FetchMeterEntity( connection );
            _ = this.MeterEntity.FetchMeterModel( connection );
            this._MeterModelEntity = this.MeterEntity.MeterModelEntity;
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.MeterModelEntity ) );
            return this.MeterModelEntity is object;
        }

        #endregion

        #region " NOMINAL TYPE "

        /// <summary>   The nom type entity. </summary>
        private NomTypeEntity _NomTypeEntity;

        /// <summary>   Gets the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> The <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public NomTypeEntity NomTypeEntity
        {
            get {
                if ( this._NomTypeEntity is null )
                {
                    this._NomTypeEntity = NomTypeEntity.EntityLookupDictionary()[this.NomTypeId];
                }

                return this._NomTypeEntity;
            }
        }

        /// <summary>   Fetches the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchNomTypeEntity( System.Data.IDbConnection connection )
        {
            if ( !NomTypeEntity.IsEnumerated() )
                _ = NomTypeEntity.TryFetchAll( connection );
            this._NomTypeEntity = NomTypeEntity.EntityLookupDictionary()[this.NomTypeId];
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.NomTypeEntity ) );
            return this._NomTypeEntity is object;
        }

        #endregion

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the GuardBandLookup. </summary>
        /// <value> Identifies the GuardBandLookup. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the tolerance code. </summary>
        /// <value> The tolerance code. </value>
        public string ToleranceCode
        {
            get => this.ICache.ToleranceCode;

            set {
                if ( !string.Equals( this.ToleranceCode, value ) )
                {
                    this.ICache.ToleranceCode = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId
        {
            get => this.ICache.MeterId;

            set {
                if ( !object.Equals( this.MeterId, value ) )
                {
                    this.ICache.MeterId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NomTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public int NomTypeId
        {
            get => this.ICache.NomTypeId;

            set {
                if ( !object.Equals( this.NomTypeId, value ) )
                {
                    this.ICache.NomTypeId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the minimum inclusive value. </summary>
        /// <value> The minimum inclusive value. </value>
        public double? MinimumInclusiveValue
        {
            get => this.ICache.MinimumInclusiveValue;

            set {
                if ( !object.Equals( this.MinimumInclusiveValue, value ) )
                {
                    this.ICache.MinimumInclusiveValue = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the maximum exclusive value. </summary>
        /// <value> The maximum exclusive value. </value>
        public double? MaximumExclusiveValue
        {
            get => this.ICache.MaximumExclusiveValue;

            set {
                if ( !object.Equals( this.MaximumExclusiveValue, value ) )
                {
                    this.ICache.MaximumExclusiveValue = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the timestamp. </summary>
        /// <value> The timestamp. </value>
        public double GuardBandFactor
        {
            get => this.ICache.GuardBandFactor;

            set {
                if ( !object.Equals( this.GuardBandFactor, value ) )
                {
                    this.ICache.GuardBandFactor = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="meterGuardBandLookupId">   The entity unique key. </param>
        /// <returns>   A <see cref="MeterGuardBandLookupEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, MeterGuardBandLookupEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int meterGuardBandLookupId )
        {
            string activity = string.Empty;
            var entity = new MeterGuardBandLookupEntity();
            try
            {
                activity = $"Fetching {nameof( MeterGuardBandLookupEntity )} by {nameof( MeterGuardBandLookupNub.Id )} of {meterGuardBandLookupId}";
                return entity.FetchUsingKey( connection, meterGuardBandLookupId ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, details As String, Entities As IEnumerable(Of
        /// MeterGuardBandLookupEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string details, IEnumerable<MeterGuardBandLookupEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new MeterGuardBandLookupEntity();
                activity = $"fetching all {nameof( MeterGuardBandLookupEntity )}'s";
                int elementCount = entity.FetchAllEntities( connection );
                return elementCount != EntityLookupDictionary().Count
                    ? throw new InvalidOperationException( $"{nameof( MeterGuardBandLookupEntity.EntityLookupDictionary )} count must equal {nameof( MeterGuardBandLookups )} count " )
                    : (true, string.Empty, MeterGuardBandLookups);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<MeterGuardBandLookupEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }
}

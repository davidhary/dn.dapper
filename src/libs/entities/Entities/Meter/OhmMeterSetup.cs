using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the Ohm Meter Setup nub and entity. Includes the fields as kept in the data
    /// table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Test settings are stored in this entity for each test session. The test settings come from
    /// <see cref="OhmMeterSettingEntity"/> records. These records are selected from
    /// <see cref="OhmMeterSettingLookupEntity"/> records based on the part resistance and tolerance
    /// and meter model and number (meter identity.)  <para>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IOhmMeterSetup
    {

        /// <summary>   Gets or sets the id of the Ohm Meter Setup. </summary>
        /// <value> Identifies the Ohm Meter Setup. </value>
        [Key]
        int AutoId { get; set; }

        /// <summary>   Gets or sets the Nplc. </summary>
        /// <value> The Nplc. </value>
        double Nplc { get; set; }

        /// <summary>   Gets or sets the number of averaging filters. </summary>
        /// <value> The number of averaging filters. </value>
        int AveragingFilterCount { get; set; }

        /// <summary>   Gets or sets the averaging filter window. </summary>
        /// <value> The averaging filter window. </value>
        double AveragingFilterWindow { get; set; }

        /// <summary>   Gets or sets the start delay. </summary>
        /// <value> The start delay. </value>
        double StartDelay { get; set; }

        /// <summary>   Gets or sets the end delay. </summary>
        /// <value> The end delay. </value>
        double EndDelay { get; set; }
    }

    /// <summary>   An ohm meter setup builder. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public sealed class OhmMeterSetupBuilder
    {

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( OhmMeterSetupNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Entities.OhmMeterSetupBuilder.TableName}] (
            [{nameof( OhmMeterSetupNub.AutoId )}]  integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
            [{nameof( OhmMeterSetupNub.Nplc )}] float NOT NULL, 
            [{nameof( OhmMeterSetupNub.AveragingFilterCount )}] integer NOT NULL, 
            [{nameof( OhmMeterSetupNub.AveragingFilterWindow )}] float NOT NULL, 
            [{nameof( OhmMeterSetupNub.StartDelay )}] float NOT NULL, 
            [{nameof( OhmMeterSetupNub.EndDelay )}] float NOT NULL); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Entities.OhmMeterSetupBuilder.TableName}') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Entities.OhmMeterSetupBuilder.TableName}](
                [{nameof( OhmMeterSetupNub.AutoId )}]  [int] IDENTITY(1,1) NOT NULL,
                [{nameof( OhmMeterSetupNub.Nplc )}] [float] NOT NULL,
                [{nameof( OhmMeterSetupNub.AveragingFilterCount )}] [int] NOT NULL,
                [{nameof( OhmMeterSetupNub.AveragingFilterWindow )}] [float] NOT NULL,
                [{nameof( OhmMeterSetupNub.StartDelay )}] [float] NOT NULL,
                [{nameof( OhmMeterSetupNub.EndDelay )}] [float] NOT NULL,
             CONSTRAINT [PK_{isr.Dapper.Entities.OhmMeterSetupBuilder.TableName}] PRIMARY KEY CLUSTERED 
            ([{nameof( OhmMeterSetupNub.AutoId )}] ASC
            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            ) ON [PRIMARY]
            END; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// Implements the Ohm Meter Setup table <see cref="IOhmMeterSetup">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-22 </para>
    /// </remarks>
    [Table( "OhmMeterSetup" )]
    public class OhmMeterSetupNub : EntityNubBase<IOhmMeterSetup>, IOhmMeterSetup
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public OhmMeterSetupNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOhmMeterSetup CreateNew()
        {
            return new OhmMeterSetupNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOhmMeterSetup CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOhmMeterSetup value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IOhmMeterSetup source, IOhmMeterSetup destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Nplc = source.Nplc;
            destination.AveragingFilterCount = source.AveragingFilterCount;
            destination.AveragingFilterWindow = source.AveragingFilterWindow;
            destination.EndDelay = source.EndDelay;
            destination.AutoId = source.AutoId;
            destination.StartDelay = source.StartDelay;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IOhmMeterSetup );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IOhmMeterSetup other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IOhmMeterSetup left, IOhmMeterSetup right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.Nplc, right.Nplc );
                result = result && Equals( left.AveragingFilterCount, right.AveragingFilterCount );
                result = result && Equals( left.AveragingFilterWindow, right.AveragingFilterWindow );
                result = result && Equals( left.EndDelay, right.EndDelay );
                result = result && Equals( left.AutoId, right.AutoId );
                result = result && Equals( left.StartDelay, right.StartDelay );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.Nplc, this.AveragingFilterCount, this.AveragingFilterWindow, this.AutoId, this.StartDelay ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Ohm Meter Setup. </summary>
        /// <value> Identifies the Ohm Meter Setup. </value>
        [Key]
        public int AutoId { get; set; }

        /// <summary>   Gets or sets the Nplc. </summary>
        /// <value> The Nplc. </value>
        public double Nplc { get; set; }

        /// <summary>   Gets or sets the number of averaging filters. </summary>
        /// <value> The number of averaging filters. </value>
        public int AveragingFilterCount { get; set; }

        /// <summary>   Gets or sets the averaging filter window. </summary>
        /// <value> The averaging filter window. </value>
        public double AveragingFilterWindow { get; set; }

        /// <summary>   Gets or sets the start delay. </summary>
        /// <value> The start delay. </value>
        public double StartDelay { get; set; }

        /// <summary>   Gets or sets the end delay. </summary>
        /// <value> The end delay. </value>
        public double EndDelay { get; set; }

        #endregion

    }

    /// <summary>
    /// the Ohm Meter Setup Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-22 </para>
    /// </remarks>
    public class OhmMeterSetupEntity : EntityBase<IOhmMeterSetup, OhmMeterSetupNub>, IOhmMeterSetup
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public OhmMeterSetupEntity() : this( new OhmMeterSetupNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmMeterSetupEntity( IOhmMeterSetup value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public OhmMeterSetupEntity( IOhmMeterSetup cache, IOhmMeterSetup store ) : base( new OhmMeterSetupNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmMeterSetupEntity( OhmMeterSetupEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.OhmMeterSetupBuilder.TableName, nameof( IOhmMeterSetup ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOhmMeterSetup CreateNew()
        {
            return new OhmMeterSetupNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOhmMeterSetup CreateCopy()
        {
            var destination = this.CreateNew();
            OhmMeterSetupNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOhmMeterSetup value )
        {
            OhmMeterSetupNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The OhmMeterSetup interface. </param>
        public override void UpdateCache( IOhmMeterSetup value )
        {
            // first make the copy to notify of any property change.
            OhmMeterSetupNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The OhmMeterSetup table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IOhmMeterSetup>( key ) : connection.Get<OhmMeterSetupNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="ohmMeterSetupId">  Identifies the ohm meter setup. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int ohmMeterSetupId )
        {
            return this.FetchUsingKey( connection, ohmMeterSetupId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOhmMeterSetup entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.AutoId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new OhmMeterSetupNub() { AutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Guard Band Lookup entities. </summary>
        /// <value> the Guard Band Lookup entities. </value>
        public IEnumerable<OhmMeterSetupEntity> OhmMeterSetups { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSetupEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOhmMeterSetup>() ) : Populate( connection.GetAll<OhmMeterSetupNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.OhmMeterSetups = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( OhmMeterSetupEntity.OhmMeterSetups ) );
            return this.OhmMeterSetups?.Any() == true ? this.OhmMeterSetups.Count() : 0;
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="ohmMeterSetupAutoId">  Identifies the <see cref="Dapper.Entities.MeterEntity"/>
        ///                                     model. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSetupEntity> FetchEntities( System.Data.IDbConnection connection, int ohmMeterSetupAutoId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.OhmMeterSetupBuilder.TableName}] WHERE {nameof( OhmMeterSetupNub.AutoId )} = @Id", new { Id = ohmMeterSetupAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return Populate( connection.Query<OhmMeterSetupNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Populates a list of OhmMeterSetup entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The OhmMeterSetup nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSetupEntity> Populate( IEnumerable<OhmMeterSetupNub> nubs )
        {
            var l = new List<OhmMeterSetupEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( OhmMeterSetupNub nub in nubs )
                    l.Add( new OhmMeterSetupEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of OhmMeterSetup entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The OhmMeterSetup interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSetupEntity> Populate( IEnumerable<IOhmMeterSetup> interfaces )
        {
            var l = new List<OhmMeterSetupEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new OhmMeterSetupNub();
                foreach ( IOhmMeterSetup iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new OhmMeterSetupEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Count entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.CountEntities( OhmMeterSetupBuilder.TableName );
        }

        #endregion

        #region " SETTINGS "

        /// <summary>   Copies from the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the Ohm Meter settings interface. </param>
        public void Copy( IOhmMeterSetting value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the meter setup from the selected meter setting. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       the Ohm Meter settings interface. </param>
        /// <param name="destination">  The ohm meter setup. </param>
        public static void Copy( IOhmMeterSetting source, IOhmMeterSetup destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Nplc = source.Nplc;
            destination.AveragingFilterCount = source.AveragingFilterCount;
            destination.AveragingFilterWindow = source.AveragingFilterWindow;
            destination.EndDelay = source.EndDelay;
            destination.StartDelay = source.StartDelay;
        }

        /// <summary>   Determine if the meter setup is equal between these entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool IsEqualSetup( IOhmMeterSetup left, IOhmMeterSetting right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && object.Equals( left.Nplc, right.Nplc );
                result = result && object.Equals( left.AveragingFilterCount, right.AveragingFilterCount );
                result = result && object.Equals( left.AveragingFilterWindow, right.AveragingFilterWindow );
                result = result && object.Equals( left.EndDelay, right.EndDelay );
                result = result && object.Equals( left.StartDelay, right.StartDelay );
                return result;
            }
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Ohm Meter Setup. </summary>
        /// <value> Identifies the Ohm Meter Setup. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the Nplc. </summary>
        /// <value> The Nplc. </value>
        public double Nplc
        {
            get => this.ICache.Nplc;

            set {
                if ( !object.Equals( this.Nplc, value ) )
                {
                    this.ICache.Nplc = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the number of averaging filters. </summary>
        /// <value> The number of averaging filters. </value>
        public int AveragingFilterCount
        {
            get => this.ICache.AveragingFilterCount;

            set {
                if ( !object.Equals( this.AveragingFilterCount, value ) )
                {
                    this.ICache.AveragingFilterCount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the averaging filter window. </summary>
        /// <value> The averaging filter window. </value>
        public double AveragingFilterWindow
        {
            get => this.ICache.AveragingFilterWindow;

            set {
                if ( !object.Equals( this.AveragingFilterWindow, value ) )
                {
                    this.ICache.AveragingFilterWindow = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the start delay. </summary>
        /// <value> The start delay. </value>
        public double StartDelay
        {
            get => this.ICache.StartDelay;

            set {
                if ( !object.Equals( this.StartDelay, value ) )
                {
                    this.ICache.StartDelay = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the end delay. </summary>
        /// <value> The end delay. </value>
        public double EndDelay
        {
            get => this.ICache.EndDelay;

            set {
                if ( !object.Equals( this.EndDelay, value ) )
                {
                    this.ICache.EndDelay = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

    }

    /// <summary>   Collection of ohm meter setup entities. </summary>
    /// <remarks>   David, 2020-06-02. </remarks>
    public class OhmMeterSetupEntityCollection : EntityKeyedCollection<int, IOhmMeterSetup, OhmMeterSetupNub, OhmMeterSetupEntity>
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public OhmMeterSetupEntityCollection() : base()
        {
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified OhmMeterSetup.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The OhmMeterSetup from which to extract the key. </param>
        /// <returns>   The key for the specified OhmMeterSetup. </returns>
        protected override int GetKeyForItem( OhmMeterSetupEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.AutoId;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( OhmMeterSetupEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all OhmMeterSetups from the
        /// <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<OhmMeterSetupEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( OhmMeterSetupEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            throw new NotImplementedException();
        }

        #endregion

        #region " CUSTOM ADDS "

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-02. </remarks>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="nominalValue">     The nominal value. </param>
        public void Add( string toleranceCode, int meterId, double nominalValue )
        {
            // assume the setting lookup entities were fetched
            int ohmMeterSettingId = OhmMeterSettingLookupEntity.SelectOhmMeterSettingLookupEntity( toleranceCode, meterId, nominalValue ).OhmMeterSettingId;
            var entity = new OhmMeterSetupEntity();
            entity.Copy( OhmMeterSettingEntity.EntityLookupDictionary()[ohmMeterSettingId] );
            this.Add( entity );
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-02. </remarks>
        /// <param name="ohmMeterSettingEntity">    The ohm meter setting entity to add. </param>
        public void Add( OhmMeterSettingEntity ohmMeterSettingEntity )
        {
            var entity = new OhmMeterSetupEntity();
            entity.Copy( ohmMeterSettingEntity );
            this.Add( entity );
        }


        #endregion

    }

    /// <summary>
    /// Collection of <see cref="OhmMeterSetupEntity"/>'s keyed by the meter id.
    /// </summary>
    /// <remarks>   David, 2020-06-12. </remarks>
    public class MeterIdOhmMeterSetupCollection : Dictionary<int, OhmMeterSetupEntity>
    {
    }

    /// <summary>
    /// Collection of <see cref="OhmMeterSetupEntity"/>'s keyed by the meter number.
    /// </summary>
    /// <remarks>   David, 2020-06-12. </remarks>
    public class MeterNumberOhmMeterSetupCollection : Dictionary<int, OhmMeterSetupEntity>
    {
    }
}

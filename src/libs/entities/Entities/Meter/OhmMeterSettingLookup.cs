using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the Ohm Meter Setting Lookup nub and entity. Includes the fields as kept in the
    /// data table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value. <para>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IOhmMeterSettingLookup
    {

        /// <summary>   Gets or sets the id of the Ohm Meter Setting Lookup. </summary>
        /// <value> Identifies the Ohm Meter Setting Lookup. </value>
        [ExplicitKey]
        int Id { get; set; }

        /// <summary>   Gets or sets the tolerance code. </summary>
        /// <value> The tolerance code. </value>
        string ToleranceCode { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        int MeterId { get; set; }

        /// <summary>   Gets or sets the minimum inclusive value. </summary>
        /// <value> The minimum inclusive value. </value>
        double? MinimumInclusiveValue { get; set; }

        /// <summary>   Gets or sets the maximum exclusive value. </summary>
        /// <value> The maximum exclusive value. </value>
        double? MaximumExclusiveValue { get; set; }

        /// <summary>   Gets or sets the id of the selected Ohm Meter Setting. </summary>
        /// <value> Identifies the selected Ohm Meter Setting. </value>
        int OhmMeterSettingId { get; set; }
    }

    /// <summary>   The ohm meter setting lookup builder. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public sealed class OhmMeterSettingLookupBuilder
    {

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( OhmMeterSettingLookupNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the meter table. </summary>
        /// <value> The name of the meter table. </value>
        public static string MeterTableName { get; set; } = MeterModelBuilder.TableName;

        /// <summary>   Gets or sets the name of the meter table foreign key. </summary>
        /// <value> The name of the meter table foreign key. </value>
        public static string MeterTableKeyName { get; set; } = nameof( MeterModelNub.Id );

        /// <summary>   Inserts values from file. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="fileName">     Filename of the file. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertFileValues( System.Data.IDbConnection connection, string fileName )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : fileName is null
                ? throw new ArgumentNullException( nameof( fileName ) )
                : connection.Execute( System.IO.File.ReadAllText( fileName ) );
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}] (
            [{nameof( OhmMeterSettingLookupNub.Id )}] integer NOT NULL PRIMARY KEY, 
            [{nameof( OhmMeterSettingLookupNub.ToleranceCode )}] [nchar](1) NULL, 
            [{nameof( OhmMeterSettingLookupNub.MeterId )}] integer NULL,
            [{nameof( OhmMeterSettingLookupNub.MinimumInclusiveValue )}] [float] NULL,
            [{nameof( OhmMeterSettingLookupNub.MaximumExclusiveValue )}] [float] NULL,
            [{nameof( OhmMeterSettingLookupNub.OhmMeterSettingId )}] [integer] NOT NULL,
            FOREIGN KEY ([{nameof( OhmMeterSettingLookupNub.ToleranceCode )}]) REFERENCES [{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([{nameof( ToleranceNub.ToleranceCode )}]) ON UPDATE CASCADE ON DELETE CASCADE, 
            FOREIGN KEY ([{nameof( OhmMeterSettingLookupNub.OhmMeterSettingId )}]) REFERENCES [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] ([{nameof( OhmMeterSettingNub.Id )}]) ON UPDATE CASCADE ON DELETE CASCADE, 
            FOREIGN KEY ([{nameof( OhmMeterSettingLookupNub.MeterId )}]) REFERENCES [{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.MeterTableName}] ([{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.MeterTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}](
                [{nameof( OhmMeterSettingLookupNub.Id )}] [int] NOT NULL,
                [{nameof( OhmMeterSettingLookupNub.ToleranceCode )}] [nchar](1) NULL,
                [{nameof( OhmMeterSettingLookupNub.MeterId )}] [int] NULL,
                [{nameof( OhmMeterSettingLookupNub.MinimumInclusiveValue )}] [float] NULL,
                [{nameof( OhmMeterSettingLookupNub.MaximumExclusiveValue )}] [float] NULL,
                [{nameof( OhmMeterSettingLookupNub.OhmMeterSettingId )}] [int] NOT NULL,
             CONSTRAINT [PK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}] PRIMARY KEY CLUSTERED 
            (
                [{nameof( OhmMeterSettingLookupNub.Id )}] ASC
            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            ) ON [PRIMARY]
            END;

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.MeterTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]  WITH CHECK ADD CONSTRAINT [FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.MeterTableName}] FOREIGN KEY([{nameof( OhmMeterSettingLookupNub.MeterId )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.MeterTableName}] ([{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.MeterTableKeyName}])
            ON UPDATE CASCADE
            ON DELETE CASCADE;

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.MeterTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.MeterTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] FOREIGN KEY([{nameof( OhmMeterSettingLookupNub.OhmMeterSettingId )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] ([{nameof( OhmMeterSettingNub.Id )}])
            ON UPDATE CASCADE
            ON DELETE CASCADE;

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.ToleranceBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.ToleranceBuilder.TableName}] FOREIGN KEY([{nameof( OhmMeterSettingLookupNub.ToleranceCode )}])
            REFERENCES [dbo].[{isr.Dapper.Entities.ToleranceBuilder.TableName}] ([{nameof( ToleranceNub.ToleranceCode )}])
            ON UPDATE CASCADE
            ON DELETE CASCADE;

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.ToleranceBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}_{isr.Dapper.Entities.ToleranceBuilder.TableName}]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// Implements the Ohm Meter Setting Lookup table
    /// <see cref="IOhmMeterSettingLookup">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-19 </para>
    /// </remarks>
    [Table( "OhmMeterSettingLookup" )]
    public class OhmMeterSettingLookupNub : EntityNubBase<IOhmMeterSettingLookup>, IOhmMeterSettingLookup
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public OhmMeterSettingLookupNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOhmMeterSettingLookup CreateNew()
        {
            return new OhmMeterSettingLookupNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOhmMeterSettingLookup CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOhmMeterSettingLookup value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IOhmMeterSettingLookup source, IOhmMeterSettingLookup destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.MaximumExclusiveValue = source.MaximumExclusiveValue;
            destination.MeterId = source.MeterId;
            destination.MinimumInclusiveValue = source.MinimumInclusiveValue;
            destination.OhmMeterSettingId = source.OhmMeterSettingId;
            destination.Id = source.Id;
            destination.ToleranceCode = source.ToleranceCode;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IOhmMeterSettingLookup );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IOhmMeterSettingLookup other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IOhmMeterSettingLookup left, IOhmMeterSettingLookup right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.MaximumExclusiveValue, right.MaximumExclusiveValue );
                result = result && Equals( left.MeterId, right.MeterId );
                result = result && Equals( left.MinimumInclusiveValue, right.MinimumInclusiveValue );
                result = result && Equals( left.OhmMeterSettingId, right.OhmMeterSettingId );
                result = result && Equals( left.Id, right.Id );
                result = result && string.Equals( left.ToleranceCode, right.ToleranceCode );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.MaximumExclusiveValue, this.MeterId, this.MinimumInclusiveValue, this.OhmMeterSettingId, this.Id, this.ToleranceCode ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Ohm Meter Setting Lookup. </summary>
        /// <value> Identifies the Ohm Meter Setting Lookup. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary>   Gets or sets the tolerance code. </summary>
        /// <value> The tolerance code. </value>
        public string ToleranceCode { get; set; }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId { get; set; }

        /// <summary>   Gets or sets the minimum inclusive value. </summary>
        /// <value> The minimum inclusive value. </value>
        public double? MinimumInclusiveValue { get; set; }

        /// <summary>   Gets or sets the maximum exclusive value. </summary>
        /// <value> The maximum exclusive value. </value>
        public double? MaximumExclusiveValue { get; set; }

        /// <summary>   Gets or sets the id of the selected Ohm Meter Setting. </summary>
        /// <value> Identifies the selected Ohm Meter Setting. </value>
        public int OhmMeterSettingId { get; set; }

        #endregion

    }

    /// <summary>
    /// The Ohm Meter Setting Lookup Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-19 </para>
    /// </remarks>
    public class OhmMeterSettingLookupEntity : EntityBase<IOhmMeterSettingLookup, OhmMeterSettingLookupNub>, IOhmMeterSettingLookup
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public OhmMeterSettingLookupEntity() : this( new OhmMeterSettingLookupNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmMeterSettingLookupEntity( IOhmMeterSettingLookup value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public OhmMeterSettingLookupEntity( IOhmMeterSettingLookup cache, IOhmMeterSettingLookup store ) : base( new OhmMeterSettingLookupNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmMeterSettingLookupEntity( OhmMeterSettingLookupEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName, nameof( IOhmMeterSettingLookup ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOhmMeterSettingLookup CreateNew()
        {
            return new OhmMeterSettingLookupNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOhmMeterSettingLookup CreateCopy()
        {
            var destination = this.CreateNew();
            OhmMeterSettingLookupNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOhmMeterSettingLookup value )
        {
            OhmMeterSettingLookupNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    the Meter Model interface. </param>
        public override void UpdateCache( IOhmMeterSettingLookup value )
        {
            // first make the copy to notify of any property change.
            OhmMeterSettingLookupNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          the Meter Model table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IOhmMeterSettingLookup>( key ) : connection.Get<OhmMeterSettingLookupNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="ohmMeterSettingLookupId">  Identifies the
        ///                                         <see cref="OhmMeterSettingLookupEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int ohmMeterSettingLookupId )
        {
            return this.FetchUsingKey( connection, ohmMeterSettingLookupId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-05-16. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOhmMeterSettingLookup entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.Id ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new OhmMeterSettingLookupNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the Trait Type entities. </summary>
        /// <value> The Trait Type entities. </value>
        public static IEnumerable<OhmMeterSettingLookupEntity> OhmMeterSettingLookups { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSettingLookupEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOhmMeterSettingLookup>() ) : Populate( connection.GetAll<OhmMeterSettingLookupNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            OhmMeterSettingLookups = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( OhmMeterSettingLookups ) );
            return OhmMeterSettingLookups?.Any() == true ? OhmMeterSettingLookups.Count() : 0;
        }

        /// <summary>   Populates a list of MeterModel entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> the Meter Model nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSettingLookupEntity> Populate( IEnumerable<OhmMeterSettingLookupNub> nubs )
        {
            var l = new List<OhmMeterSettingLookupEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( OhmMeterSettingLookupNub nub in nubs )
                    l.Add( new OhmMeterSettingLookupEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of MeterModel entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   the Meter Model interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSettingLookupEntity> Populate( IEnumerable<IOhmMeterSettingLookup> interfaces )
        {
            var l = new List<OhmMeterSettingLookupEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new OhmMeterSettingLookupNub();
                foreach ( IOhmMeterSettingLookup iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new OhmMeterSettingLookupEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSettingLookupEntity> FetchEntities( System.Data.IDbConnection connection, int meterModelId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.OhmMeterSettingLookupBuilder.TableName}] WHERE {nameof( OhmMeterSettingLookupNub.MeterId )} = @Id", new { Id = meterModelId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return Populate( connection.Query<OhmMeterSettingLookupNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, OhmMeterSettingLookupEntity> _EntityLookupDictionary;

        /// <summary>   The entity lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of Integer, OhmMeterSettingLookupEntity) </returns>
        public static IDictionary<int, OhmMeterSettingLookupEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, OhmMeterSettingLookupEntity>();
                foreach ( OhmMeterSettingLookupEntity entity in OhmMeterSettingLookups )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (OhmMeterSettingLookups?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false );
        }


        #endregion

        #region " FIND "

        /// <summary>   Select ohm meter setting lookup entity. </summary>
        /// <remarks>   David, 2020-05-31. </remarks>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="nominalValue">     The nominal value. </param>
        /// <returns>   An OhmMeterSettingLookupEntity. </returns>
        public static OhmMeterSettingLookupEntity SelectOhmMeterSettingLookupEntity( string toleranceCode, int meterId, double nominalValue )
        {
            var entity = new OhmMeterSettingLookupEntity();
            foreach ( var currentEntity in OhmMeterSettingLookups )
            {
                entity = currentEntity;
                if ( string.Equals( toleranceCode, entity.ToleranceCode ) && meterId == entity.MeterId )
                {
                    if ( entity.MinimumInclusiveValue.HasValue && entity.MaximumExclusiveValue.HasValue && entity.MaximumExclusiveValue.Value > nominalValue && entity.MinimumInclusiveValue.Value <= nominalValue )
                    {
                        break;
                    }
                    else if ( !entity.MinimumInclusiveValue.HasValue && entity.MaximumExclusiveValue.Value > nominalValue )
                    {
                        break;
                    }
                    else if ( !entity.MaximumExclusiveValue.HasValue && entity.MinimumInclusiveValue.Value <= nominalValue )
                    {
                        break;
                    }
                }
            }

            return entity;
        }

        /// <summary>   Builds where clause. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="value">            the nominal value. </param>
        /// <returns>   A String. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        private static string BuildWhereClause( string toleranceCode, int meterId, double value )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"WHERE ({nameof( OhmMeterSettingLookupEntity.ToleranceCode )} = @{nameof( toleranceCode )} AND " );
            _ = queryBuilder.AppendLine( $"{nameof( OhmMeterSettingLookupEntity.MeterId )} = @{nameof( meterId )} AND " );
            _ = queryBuilder.AppendLine( $"(({nameof( OhmMeterSettingLookupEntity.MinimumInclusiveValue )} IS NULL AND {nameof( OhmMeterSettingLookupEntity.MaximumExclusiveValue )} > @{nameof( value )}) OR " );
            _ = queryBuilder.AppendLine( $"({nameof( OhmMeterSettingLookupEntity.MinimumInclusiveValue )} <= @{nameof( value )} AND {nameof( OhmMeterSettingLookupEntity.MaximumExclusiveValue )} > @{nameof( value )}) OR " );
            _ = queryBuilder.AppendLine( $"({nameof( OhmMeterSettingLookupEntity.MinimumInclusiveValue )} <= @{nameof( value )} AND {nameof( OhmMeterSettingLookupEntity.MaximumExclusiveValue )} IS NULL) ) ); " );
            return queryBuilder.ToString();
        }

        /// <summary>   Count entities; returns number of records with the specified values. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="value">            the nominal value. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string toleranceCode, int meterId, double value )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"select count(*) from [{OhmMeterSettingLookupBuilder.TableName}] " );
            _ = queryBuilder.Append( BuildWhereClause( toleranceCode, meterId, value ) );
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( queryBuilder.ToString(), new { toleranceCode, meterId, value } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="value">            the nominal value. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSettingLookupNub> FetchNubs( System.Data.IDbConnection connection, string toleranceCode, int meterId, double value )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( @$"select * from [{OhmMeterSettingLookupBuilder.TableName}] " );
            _ = queryBuilder.Append( BuildWhereClause( toleranceCode, meterId, value ) );
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( queryBuilder.ToString(), new { toleranceCode, meterId, value } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<OhmMeterSettingLookupNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Meter exists. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="value">            the nominal value. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string toleranceCode, int meterId, double value )
        {
            return 1 == CountEntities( connection, toleranceCode, meterId, value );
        }

        #endregion

        #region " RELATIONS "

        #region " TOLERANCE "

        /// <summary>   The tolerance entity. </summary>
        private ToleranceEntity _ToleranceEntity;

        /// <summary>   Gets the meter model entity. </summary>
        /// <value> The meter model entity. </value>
        public ToleranceEntity ToleranceEntity
        {
            get {
                if ( this._ToleranceEntity is null )
                {
                    this._ToleranceEntity = ToleranceEntity.EntityLookupDictionary()[this.ToleranceCode];
                }

                return this._ToleranceEntity;
            }
        }

        /// <summary>   Fetches the Tolerance entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchToleranceEntity( System.Data.IDbConnection connection )
        {
            if ( !ToleranceEntity.IsEnumerated() )
                _ = ToleranceEntity.TryFetchAll( connection );
            this._ToleranceEntity = ToleranceEntity.EntityLookupDictionary()[this.ToleranceCode];
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.ToleranceEntity ) );
            return this.ToleranceEntity is object;
        }

        #endregion

        #region " OHM METER SETTING "

        /// <summary>   Gets or sets the selected Ohm Meter Setting entity. </summary>
        /// <value> The selected Ohm Meter Settings entity. </value>
        public OhmMeterSettingEntity OhmMeterSetting { get; private set; }

        /// <summary>   Fetches the Ohm Meter Setting. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchOhmMeterSetting( System.Data.IDbConnection connection )
        {
            var entity = new OhmMeterSettingEntity();
            bool result = entity.FetchUsingKey( connection, this.OhmMeterSettingId );
            this.OhmMeterSetting = entity;
            this.NotifyPropertyChanged( nameof( OhmMeterSettingLookupEntity.OhmMeterSetting ) );
            return result;
        }

        #endregion

        #region " METER "

        private MeterEntity _MeterEntity;

        /// <summary>   Gets the meter entity. </summary>
        /// <value> The meter entity. </value>
        public MeterEntity MeterEntity
        {
            get {
                if ( this._MeterEntity is null )
                {
                    this._MeterEntity = MeterEntity.EntityLookupDictionary()[this.MeterId];
                }

                return this._MeterEntity;
            }
        }

        /// <summary>   Fetches the meter. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchMeterEntity( System.Data.IDbConnection connection )
        {
            if ( !MeterEntity.IsEnumerated() )
                _ = MeterEntity.TryFetchAll( connection );
            this._MeterEntity = MeterEntity.EntityLookupDictionary()[this.MeterId];
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.MeterEntity ) );
            return this._MeterEntity is object;
        }

        #endregion

        #region " METER MODEL "

        private MeterModelEntity _MeterModelEntity;

        /// <summary>   Gets the Meter Model entity. </summary>
        /// <value> The meter model entity. </value>
        public MeterModelEntity MeterModelEntity
        {
            get {
                if ( this._MeterModelEntity is null )
                {
                    this._MeterModelEntity = this.MeterEntity.MeterModelEntity;
                }

                return this._MeterModelEntity;
            }
        }

        /// <summary>   Fetches the Meter Model entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchMeterModel( System.Data.IDbConnection connection )
        {
            _ = this.FetchMeterEntity( connection );
            _ = this.MeterEntity.FetchMeterModel( connection );
            this._MeterModelEntity = this.MeterEntity.MeterModelEntity;
            this.NotifyPropertyChanged( nameof( MeterGuardBandLookupEntity.MeterModelEntity ) );
            return this.MeterModelEntity is object;
        }

        #endregion

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Ohm Meter Setting Lookup. </summary>
        /// <value> Identifies the Ohm Meter Setting Lookup. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the tolerance code. </summary>
        /// <value> The tolerance code. </value>
        public string ToleranceCode
        {
            get => this.ICache.ToleranceCode;

            set {
                if ( !string.Equals( this.ToleranceCode, value ) )
                {
                    this.ICache.ToleranceCode = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int MeterId
        {
            get => this.ICache.MeterId;

            set {
                if ( !object.Equals( this.MeterId, value ) )
                {
                    this.ICache.MeterId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the minimum inclusive value. </summary>
        /// <value> The minimum inclusive value. </value>
        public double? MinimumInclusiveValue
        {
            get => this.ICache.MinimumInclusiveValue;

            set {
                if ( !object.Equals( this.MinimumInclusiveValue, value ) )
                {
                    this.ICache.MinimumInclusiveValue = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the maximum exclusive value. </summary>
        /// <value> The maximum exclusive value. </value>
        public double? MaximumExclusiveValue
        {
            get => this.ICache.MaximumExclusiveValue;

            set {
                if ( !object.Equals( this.MaximumExclusiveValue, value ) )
                {
                    this.ICache.MaximumExclusiveValue = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the selected Ohm Meter Setting. </summary>
        /// <value> Identifies the selected Ohm Meter Setting. </value>
        public int OhmMeterSettingId
        {
            get => this.ICache.OhmMeterSettingId;

            set {
                if ( !object.Equals( this.OhmMeterSettingId, value ) )
                {
                    this.ICache.OhmMeterSettingId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">               The connection. </param>
        /// <param name="ohmMeterSettingLookupId">  The entity unique key. </param>
        /// <returns>   A <see cref="OhmMeterSettingLookupEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, OhmMeterSettingLookupEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int ohmMeterSettingLookupId )
        {
            string activity = string.Empty;
            var entity = new OhmMeterSettingLookupEntity();
            try
            {
                activity = $"Fetching {nameof( OhmMeterSettingLookupEntity )} by {nameof( OhmMeterSettingLookupNub.Id )} of {ohmMeterSettingLookupId}";
                return entity.FetchUsingKey( connection, ohmMeterSettingLookupId ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, details As String, Entities As IEnumerable(Of
        /// OhmMeterSettingLookupEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string details, IEnumerable<OhmMeterSettingLookupEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new OhmMeterSettingLookupEntity();
                activity = $"fetching all {nameof( OhmMeterSettingLookupEntity )}'s";
                int elementCount = entity.FetchAllEntities( connection );
                return elementCount != EntityLookupDictionary().Count
                    ? throw new InvalidOperationException( $"{nameof( OhmMeterSettingLookupEntity.EntityLookupDictionary )} count must equal {nameof( OhmMeterSettingLookups )} count " )
                    : (true, string.Empty, OhmMeterSettingLookups);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<OhmMeterSettingLookupEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }
}

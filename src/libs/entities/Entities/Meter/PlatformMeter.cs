using System;
using System.Collections.Generic;

namespace isr.Dapper.Entities
{

    /// <summary>   A platform meter. </summary>
    /// <remarks>
    /// David, 2020-02-25 <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class PlatformMeter
    {

        /// <summary>   Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks>
        /// David, 2020-11-11. <para>
        /// TO_DO: Add elements and nom types to the Platform Meter and use for adding sample and yield
        /// traits and displaying element nominal information. It maybe sufficient add elements and then
        /// a dictionary of element types relevant for this meter for each element id. </para>
        /// </remarks>
        public PlatformMeter() : base()
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-11. </remarks>
        /// <param name="meter">   The meter entity. </param>
        public PlatformMeter( Dapper.Entities.MeterEntity meter ) : this()
        {
            this.Meter = meter;
            this.MeterId = meter.Id;
            this.MeterNumber = meter.MeterNumber;
            this.MeterModel = ( Dapper.Entities.MeterModel ) meter.MeterModelId;
            this.Caption = $"Meter #{this.MeterNumber}: {this.MeterModel}";
            this._Elements = new MeterUniqueElementEntityCollection( meter.Id );
            this.SetNominalTypes();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-06-11. </remarks>
        /// <param name="meter">    The meter entity. </param>
        /// <param name="enabled">  True to enable, false to disable. </param>
        /// <param name="emulated"> True if emulated. </param>
        public PlatformMeter( Dapper.Entities.MeterEntity meter, bool enabled, bool emulated ) : this( meter )
        {
            this.Enabled = enabled;
            this.Emulated = emulated;
        }

        /// <summary>   Gets or sets the meter. </summary>
        /// <value> The meter. </value>
        public MeterEntity Meter { get; private set; }

        /// <summary>   Gets or sets the meter number. </summary>
        /// <value> The meter number. </value>
        public int MeterNumber { get; set; }

        /// <summary>   Gets or sets the meter model. </summary>
        /// <value> The meter model. </value>
        public Dapper.Entities.MeterModel MeterModel { get; set; }

        /// <summary>   Gets or sets the id of the meter. </summary>
        /// <value> The identifier of the meter. </value>
        public int MeterId { get; set; }

        /// <summary>   Gets or sets the enabled. </summary>
        /// <value> The enabled. </value>
        public bool Enabled { get; set; }

        /// <summary>   Gets or sets the emulated. </summary>
        /// <value> The emulated. </value>
        public bool Emulated { get; set; }

        /// <summary>   Gets or sets the caption. </summary>
        /// <value> The caption. </value>
        public string Caption { get; private set; }

        /// <summary>   Gets or sets the primary element label. </summary>
        /// <value> The primary element label. </value>
        public String PrimaryElementLabel { get; private set; }

        /// <summary>   Gets or sets the element labels. </summary>
        /// <value> The element labels. </value>
        public IList<String> ElementLabels { get; private set; }

        /// <summary>   Sets element labels. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        /// <param name="elementLabels">    The element labels. </param>
        public void SetElementLabels( String[] elementLabels )
        {
            this.ElementLabels = elementLabels;
        }

        /// <summary>
        /// Gets or sets the primary nominal type. This nominal type is used for binning. It is the
        /// primary measure the meter is measuring.
        /// </summary>
        /// <value> The default nominal type. </value>
        public NomType PrimaryNominalType { get; private set; }

        /// <summary>   Gets a list of the nominal types. </summary>
        /// <value> A list of the nominal types. </value>
        public IList<NomType> NominalTypes { get; private set; }

        /// <summary>   Sets nominal types. </summary>
        /// <remarks>   David, 2021-05-19. </remarks>
        /// <param name="nominalTypes"> List of types of the nominals. </param>
        public void SetNominalTypes( NomType[] nominalTypes )
        {
            this.NominalTypes = nominalTypes;
        }

        /// <summary>   Sets nominal types. </summary>
        /// <remarks>   David, 2021-05-19. </remarks>
        private void SetNominalTypes()
        {
            switch ( this.MeterModel )
            {
                case MeterModel.None:
                    this.ElementLabels = Array.Empty<String>();
                    this.PrimaryElementLabel = string.Empty;
                    this.PrimaryNominalType = Dapper.Entities.NomType.None;
                    this.NominalTypes = Array.Empty<NomType>();
                    break;
                case MeterModel.K7510:
                    this.PrimaryElementLabel = "R1";
                    this.ElementLabels = new String[] { "R1" };
                    this.PrimaryNominalType = Dapper.Entities.NomType.Resistance;
                    this.NominalTypes = new NomType[] { NomType.Resistance };
                    break;
                case MeterModel.K2002:
                    this.PrimaryElementLabel = "R1";
                    this.ElementLabels = new String[] { "R1", "R2", "D1", "M1" };
                    this.PrimaryNominalType = Dapper.Entities.NomType.Resistance;
                    this.NominalTypes = new NomType[] { NomType.Resistance, NomType.Equation };
                    break;
                case MeterModel.CLT10:
                    this.PrimaryElementLabel = "THI";
                    this.ElementLabels = new String[] { "THI" };
                    this.PrimaryNominalType = Dapper.Entities.NomType.ThirdHarmonicsIndex;
                    this.NominalTypes = new NomType[] { NomType.ThirdHarmonicsIndex };
                    break;
                default:
                    break;
            }
        }

        /// <summary>   Gets or sets the primary element identifier. The element is used for binning.
        /// It is the primary element the meter is measuring. </summary>
        /// <value> The default element identifier. </value>
        public int PrimaryElementId { get; private set; }

        private readonly MeterUniqueElementEntityCollection _Elements;
        /// <summary>   Gets or sets the elements. </summary>
        /// <value> The elements. </value>
        public MeterUniqueElementEntityCollection Elements()
        {
            return this._Elements;
        }

        /// <summary>   Clears the elements. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        public void ClearElements()
        {
            this.PrimaryElementId = 0;
            this._Elements.Clear();
            this.PrimaryElement = new ElementEntity();
        }

        /// <summary>   Adds an element to collection of meter elements. </summary>
        /// <remarks>   David, 2021-05-21. </remarks>
        /// <param name="element">      The element. </param>
        public void AddElement( ElementEntity element )
        {
            this._Elements.Add( element );
            if ( string.Equals( this.PrimaryElementLabel, element.ElementLabel, StringComparison.OrdinalIgnoreCase ) )
            {
                this.PrimaryElement = element;
                this.PrimaryElementId = element.AutoId;
            }
        }

        /// <summary>   Primary element. </summary>
        /// <remarks>   David, 2021-05-20. </remarks>
        /// <returns>   An ElementEntity. </returns>
        public ElementEntity PrimaryElement { get; private set; }

        private NomTrait _PrimaryNominalTrait;
        /// <summary>   Primary nominal trait. </summary>
        /// <remarks>   David, 2021-05-20. </remarks>
        /// <param name="clear">    (Optional) True to clear the existing value. </param>
        /// <returns>   A NomTrait. </returns>
        public NomTrait PrimaryNominalTrait( bool clear = false )
        {
            if ( clear ) { this._PrimaryNominalTrait = null; }
            if ( this._PrimaryNominalTrait is not object )
            {
                this._PrimaryNominalTrait = this.PrimaryElement.NomTraitsCollection[new MeterNomTypeSelector( this )].NomTrait;
            }
            return this._PrimaryNominalTrait;
        }

        /// <summary>   Query if this object is enabled or emulated. </summary>
        /// <remarks>   David, 2020-11-11. </remarks>
        /// <returns>   <c>true</c> if enabled or emulated; otherwise <c>false</c> </returns>
        public bool IsEnabledOrEmulated()
        {
            return this.Enabled || this.Emulated;
        }
    }

}


using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// A Meter Table builder based on the <see cref="IExplicitKeyForeignNatural">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class MeterBuilder : ExplicitKeyForeignNaturalBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( MeterNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the foreign table . </summary>
        /// <value> The name of the foreign table. </value>
        public override string ForeignTableName { get; set; } = MeterModelBuilder.TableName;

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <value> The name of the foreign table key. </value>
        public override string ForeignTableKeyName { get; set; } = nameof( MeterModelNub.Id );

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValues( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? InsertValues( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? InsertValues( sqlite ) : 0;
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValues( System.Data.SQLite.SQLiteConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (1,1,1); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (2,1,2); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (3,2,1); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (4,2,2); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (5,3,1); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (6,3,2); " );
            return connection.Execute( builder.ToString() );
        }

        /// <summary>   Inserts or ignores the records using an SQL client connection. </summary>
        /// <remarks>   David, 2020-03-14. Supports SQL Client only library. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValuesSqlClient( System.Data.IDbConnection connection )
        {
            return InsertValues( connection as System.Data.SqlClient.SqlConnection );
        }

        /// <summary>   Inserts the values SQL lite described by connection. </summary>
        /// <remarks>   David, 2020-06-18. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValuesSQLite( System.Data.IDbConnection connection )
        {
            return InsertValues( connection as System.Data.SQLite.SQLiteConnection );
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValues( System.Data.SqlClient.SqlConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.MeterBuilder.TableName}] WHERE [{nameof( MeterNub.Id )}] = 1) INSERT INTO [{isr.Dapper.Entities.MeterBuilder.TableName}] VALUES (1,1,1); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.MeterBuilder.TableName}] WHERE [{nameof( MeterNub.Id )}] = 2) INSERT INTO [{isr.Dapper.Entities.MeterBuilder.TableName}] VALUES (2,1,2); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.MeterBuilder.TableName}] WHERE [{nameof( MeterNub.Id )}] = 3) INSERT INTO [{isr.Dapper.Entities.MeterBuilder.TableName}] VALUES (3,2,1); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.MeterBuilder.TableName}] WHERE [{nameof( MeterNub.Id )}] = 4) INSERT INTO [{isr.Dapper.Entities.MeterBuilder.TableName}] VALUES (4,2,2); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.MeterBuilder.TableName}] WHERE [{nameof( MeterNub.Id )}] = 5) INSERT INTO [{isr.Dapper.Entities.MeterBuilder.TableName}] VALUES (5,3,1); " );
            _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{isr.Dapper.Entities.MeterBuilder.TableName}] WHERE [{nameof( MeterNub.Id )}] = 6) INSERT INTO [{isr.Dapper.Entities.MeterBuilder.TableName}] VALUES (6,3,2); " );
            return connection.Execute( builder.ToString() );
        }

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string IdFieldName { get; set; } = nameof( MeterEntity.Id );

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public override string ForeignIdFieldName { get; set; } = nameof( MeterEntity.MeterModelId );

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public override string AmountFieldName { get; set; } = nameof( MeterEntity.MeterNumber );

        #region " SINGLETON "

        private static readonly Lazy<MeterBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static MeterBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Meter table based on the
    /// <see cref="IExplicitKeyForeignNatural">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "Meter" )]
    public class MeterNub : ExplicitKeyForeignNaturalNub, IExplicitKeyForeignNatural
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public MeterNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IExplicitKeyForeignNatural CreateNew()
        {
            return new MeterNub();
        }
    }

    /// <summary>
    /// The <see cref="Dapper.Entities.MeterEntity"/>. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class MeterEntity : EntityBase<IExplicitKeyForeignNatural, MeterNub>, IExplicitKeyForeignNatural
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public MeterEntity() : this( new MeterNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public MeterEntity( IExplicitKeyForeignNatural value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public MeterEntity( IExplicitKeyForeignNatural cache, IExplicitKeyForeignNatural store ) : base( new MeterNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public MeterEntity( MeterEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.MeterBuilder.TableName, nameof( IExplicitKeyForeignNatural ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IExplicitKeyForeignNatural CreateNew()
        {
            return new MeterNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IExplicitKeyForeignNatural CreateCopy()
        {
            var destination = this.CreateNew();
            ExplicitKeyForeignNaturalNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IExplicitKeyForeignNatural value )
        {
            ExplicitKeyForeignNaturalNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Meter interface. </param>
        public override void UpdateCache( IExplicitKeyForeignNatural value )
        {
            // first make the copy to notify of any property change.
            ExplicitKeyForeignNaturalNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The Meter table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IExplicitKeyForeignNatural>( key ) : connection.Get<MeterNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.MeterModelId, this.MeterNumber );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> Identifies the meter model. </param>
        /// <param name="meterNumber">  The meter number. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int meterModelId, int meterNumber )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, meterModelId, meterNumber ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IExplicitKeyForeignNatural entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Id, entity.ForeignId ) )
            {
                // update the existing record from the specified entity.
                entity.Id = this.Id;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new MeterNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the Meter entities. </summary>
        /// <value> The Meter entities. </value>
        public static IEnumerable<MeterEntity> Meters { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<MeterEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IExplicitKeyForeignNatural>() ) : Populate( connection.GetAll<MeterNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            Meters = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( Meters ) );
            return Meters?.Any() == true ? Meters.Count() : 0;
        }

        /// <summary>   Populates a list of Meter entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The Meter nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<MeterEntity> Populate( IEnumerable<MeterNub> nubs )
        {
            var l = new List<MeterEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( MeterNub nub in nubs )
                    l.Add( new MeterEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Meter entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The Meter interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<MeterEntity> Populate( IEnumerable<IExplicitKeyForeignNatural> interfaces )
        {
            var l = new List<MeterEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new MeterNub();
                foreach ( IExplicitKeyForeignNatural iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new MeterEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, MeterEntity> _EntityLookupDictionary;

        /// <summary>   The entity lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of Integer, MeterEntity) </returns>
        public static IDictionary<int, MeterEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, MeterEntity>();
                foreach ( MeterEntity entity in Meters )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Dictionary of key lookups. </summary>
        private static IDictionary<KeyValuePair<int, int>, int> _KeyLookupDictionary;

        /// <summary>   The key lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of String, Integer) </returns>
        public static IDictionary<KeyValuePair<int, int>, int> KeyLookupDictionary()
        {
            if ( !(_KeyLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _KeyLookupDictionary = new Dictionary<KeyValuePair<int, int>, int>();
                foreach ( MeterEntity entity in Meters )
                    _KeyLookupDictionary.Add( new KeyValuePair<int, int>( entity.MeterModelId, entity.MeterNumber ), entity.Id );
            }

            return _KeyLookupDictionary;
        }

        /// <summary>   Select Meter entity. </summary>
        /// <remarks>   David, 2020-07-04. </remarks>
        /// <param name="meterModelId"> Identifies the <see cref="Dapper.Entities.MeterModelEntity"/>
        ///                             model. </param>
        /// <param name="meterNumber">  The meter number. </param>
        /// <returns>   A MeterEntity. </returns>
        public static MeterEntity SelectMeterEntity( int meterModelId, int meterNumber )
        {
            return EntityLookupDictionary()[KeyLookupDictionary()[new KeyValuePair<int, int>( meterModelId, meterNumber )]];
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (Meters?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) &&
                   (_KeyLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Bin entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> Identifies the <see cref="Dapper.Entities.MeterModelEntity"/>
        ///                             model. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int meterModelId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{MeterBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( MeterNub.ForeignId )} = @TypeId", new { TypeId = meterModelId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> Identifies the <see cref="Dapper.Entities.MeterModelEntity"/>
        ///                             model. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<MeterEntity> FetchEntities( System.Data.IDbConnection connection, int meterModelId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.MeterBuilder.TableName}] WHERE {nameof( MeterNub.ForeignId )} = @Id", new { Id = meterModelId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<MeterNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by meter number. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterNumber">  The meter number. </param>
        /// <returns>   The total number of entities by meter number. </returns>
        public static int CountEntitiesByMeterNumber( System.Data.IDbConnection connection, int meterNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{MeterBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( MeterNub.Amount )} = @Amount", new { Amount = meterNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by meter number in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterNumber">  The meter number. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Bins in this
        /// collection.
        /// </returns>
        public static IEnumerable<MeterEntity> FetchEntitiesByMeterNumber( System.Data.IDbConnection connection, int meterNumber )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.MeterBuilder.TableName}] WHERE {nameof( MeterNub.Amount )} = @Amount", new { Amount = meterNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<MeterNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> Identifies the <see cref="Dapper.Entities.MeterModelEntity"/>
        ///                             model. </param>
        /// <param name="meterNumber">  The meter number. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int meterModelId, int meterNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{MeterBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( MeterNub.ForeignId )} = @TypeId", new { TypeId = meterModelId } );
            _ = sqlBuilder.Where( $"{nameof( MeterNub.Amount )} = @Amount", new { Amount = meterNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> Identifies the <see cref="Dapper.Entities.MeterModelEntity"/>
        ///                             model. </param>
        /// <param name="meterNumber">  The meter number. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<MeterNub> FetchNubs( System.Data.IDbConnection connection, int meterModelId, int meterNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{MeterBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( MeterNub.ForeignId )} = @TypeId", new { TypeId = meterModelId } );
            _ = sqlBuilder.Where( $"{nameof( MeterNub.Amount )} = @Amount", new { Amount = meterNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<MeterNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Sort Bin exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> Identifies the <see cref="Dapper.Entities.MeterModelEntity"/>
        ///                             model. </param>
        /// <param name="meterNumber">  The meter number. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int meterModelId, int meterNumber )
        {
            return 1 == CountEntities( connection, meterModelId, meterNumber );
        }

        #endregion

        #region " RELATIONS "

        #region " METER "

        /// <summary>   The Meter Model entity. </summary>
        private MeterModelEntity _MeterModelEntity;

        /// <summary>   Gets the Meter Model entity. </summary>
        /// <value> The Meter Model entity. </value>
        public MeterModelEntity MeterModelEntity
        {
            get {
                if ( this._MeterModelEntity is null )
                {
                    this._MeterModelEntity = MeterModelEntity.EntityLookupDictionary()[this.MeterModelId];
                }

                return this._MeterModelEntity;
            }
        }

        /// <summary>   Fetches the Meter Model entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchMeterModel( System.Data.IDbConnection connection )
        {
            if ( !MeterModelEntity.IsEnumerated() )
                _ = MeterModelEntity.TryFetchAll( connection );
            this._MeterModelEntity = MeterModelEntity.EntityLookupDictionary()[this.MeterModelId];
            this.NotifyPropertyChanged( nameof( MeterEntity.MeterModelEntity ) );
            return this.MeterModelEntity is object;
        }

        #endregion

        #endregion

        #region " FIELDS "

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterEntity"/> type.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterEntity"/> type. </value>
        public int ForeignId
        {
            get => this.ICache.ForeignId;

            set {
                if ( !object.Equals( ( object ) this.ForeignId, ( object ) value ) )
                {
                    this.ICache.ForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( MeterEntity.MeterModelId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.MeterModelEntity"/> model.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.MeterModelEntity"/> model. </value>
        public int MeterModelId
        {
            get => this.ForeignId;

            set => this.ForeignId = value;
        }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        public int Amount
        {
            get => this.ICache.Amount;

            set {
                if ( this.Amount != value )
                {
                    this.ICache.Amount = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( MeterEntity.MeterNumber ) );
                }
            }
        }

        /// <summary>   Gets or sets the Meter number. </summary>
        /// <value> The Meter number. </value>
        public int MeterNumber
        {
            get => this.Amount;

            set => this.Amount = value;
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterId">      The entity unique key. </param>
        /// <returns>   A <see cref="Dapper.Entities.MeterEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, MeterEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int meterId )
        {
            string activity = string.Empty;
            var entity = new MeterEntity();
            try
            {
                activity = $"Fetching {nameof( MeterEntity )} by {nameof( MeterNub.Id )} of {meterId}";
                return entity.FetchUsingKey( connection, meterId ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Try fetch using unique index. </summary>
        /// <remarks>   David, 2020-04-28. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="meterModelId"> Identifies the meter model. </param>
        /// <param name="meterNumber">  The meter number. </param>
        /// <returns>   The (Success As Boolean, Details As String, Entity As MeterEntity) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, MeterEntity Entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, int meterModelId, int meterNumber )
        {
            string activity = string.Empty;
            var entity = new MeterEntity();
            try
            {
                activity = $"Fetching {nameof( MeterEntity )} by [{nameof( MeterNub.ForeignId )},{nameof( MeterNub.Amount )}] of [{meterModelId},{meterNumber}]";
                return entity.FetchUsingUniqueIndex( connection, meterModelId, meterNumber ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, details As String, Entities As IEnumerable(Of MeterEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string details, IEnumerable<MeterEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new MeterEntity();
                activity = $"fetching all {nameof( MeterEntity )}'s";
                int elementCount = entity.FetchAllEntities( connection );
                if ( elementCount != EntityLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( MeterEntity.EntityLookupDictionary )} count must equal {nameof( Meters )} count " );
                }
                else if ( elementCount != KeyLookupDictionary().Count )
                {
                    throw new InvalidOperationException( $"{nameof( MeterEntity.KeyLookupDictionary )} count must equal {nameof( Meters )} count " );
                }

                if ( !MeterModelEntity.IsEnumerated() )
                    _ = MeterModelEntity.TryFetchAll( connection );
                return (true, string.Empty, Meters);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<MeterEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }

    /// <summary>   Collection of Meter entities. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class MeterEntityCollection : EntityKeyedCollection<int, IExplicitKeyForeignNatural, MeterNub, MeterEntity>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public MeterEntityCollection() : base()
        {
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override int GetKeyForItem( MeterEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.Id;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( MeterEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<MeterEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( MeterEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            throw new NotImplementedException();
        }
    }
}

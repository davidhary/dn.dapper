using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the Ohm Meter Setting nub and entity. Includes the fields as kept in the data
    /// table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IOhmMeterSetting
    {

        /// <summary>   Gets or sets the id of the Ohm Meter Setting. </summary>
        /// <value> Identifies the Ohm Meter Setting. </value>
        [ExplicitKey]
        int Id { get; set; }

        /// <summary>   Gets or sets the Nplc. </summary>
        /// <value> The Nplc. </value>
        double Nplc { get; set; }

        /// <summary>   Gets or sets the number of averaging filters. </summary>
        /// <value> The number of averaging filters. </value>
        int AveragingFilterCount { get; set; }

        /// <summary>   Gets or sets the averaging filter window. </summary>
        /// <value> The averaging filter window. </value>
        double AveragingFilterWindow { get; set; }

        /// <summary>   Gets or sets the start delay. </summary>
        /// <value> The start delay. </value>
        double StartDelay { get; set; }

        /// <summary>   Gets or sets the end delay. </summary>
        /// <value> The end delay. </value>
        double EndDelay { get; set; }
    }

    /// <summary>   An ohm meter setting builder. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public sealed class OhmMeterSettingBuilder
    {

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( OhmMeterSettingNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValues( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? InsertValues( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? InsertValues( sqlite ) : 0;
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValues( System.Data.SQLite.SQLiteConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var builder = new System.Text.StringBuilder();
            // Id 
            // Nplc
            // AveragingFilterCount 
            // AveragingFilterWindow
            // StartDelay 
            // EndDelay
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (1,  1, 10, 0.001, 0.02, 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (2,  1,  1, 0    , 0.02, 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (3,  1,  1, 0    , 0.05, 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (4,  1,  1, 0    , 0.1 , 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (5,  1,  1, 0    , 0.2 , 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (6,  1,  1, 0    , 0.5 , 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (7,  1,  5, 0.001, 0.001, 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (8,  1,  1, 0    , 0.001, 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (9,  1,  1, 0    , 0.001, 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (10, 1,  1, 0    , 0.001, 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (11, 1,  1, 0    , 0.001, 0); " );
            _ = builder.AppendLine( $"INSERT OR IGNORE INTO [{TableName}] VALUES (12, 1,  1, 0    , 0.001, 0); " );
            return connection.Execute( builder.ToString() );
        }

        /// <summary>   Inserts or ignores the records using an SQL client connection. </summary>
        /// <remarks>   David, 2020-03-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValuesSqlClient( System.Data.IDbConnection connection )
        {
            return InsertValues( connection as System.Data.SqlClient.SqlConnection );
        }

        /// <summary>   Inserts the values sq lite described by connection. </summary>
        /// <remarks>   David, 2020-06-18. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValuesSQLite( System.Data.IDbConnection connection )
        {
            return InsertValues( connection as System.Data.SQLite.SQLiteConnection );
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertValues( System.Data.SqlClient.SqlConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=1) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (1,10,0.001,0.02,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=2) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (2,1,0,0.02,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=3) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (3,1,0,0.05,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=4) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (4,1,0,0.1,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=5) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (5,1,0,0.2,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=6) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (6,1,0,0.5,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=7) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (7,10,0.001,0.02,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=8) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (8,1,0,0.02,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=9) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (9,1,0,0.05,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=10) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (10,1,0,0.1,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=11) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (11,1,0,0.2,0,0); " );
            _ = builder.AppendLine( $"IF Not EXISTS(select * from [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] where [{nameof( OhmMeterSettingNub.Id )}]=12) INSERT INTO [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] VALUES (12,1,0,0.5,0,0); " );
            return connection.Execute( builder.ToString() );
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] (
            [{nameof( OhmMeterSettingNub.Id )}] integer NOT NULL PRIMARY KEY, 
            [{nameof( OhmMeterSettingNub.Nplc )}] float NOT NULL, 
            [{nameof( OhmMeterSettingNub.AveragingFilterCount )}] integer NOT NULL, 
            [{nameof( OhmMeterSettingNub.AveragingFilterWindow )}] float NOT NULL, 
            [{nameof( OhmMeterSettingNub.StartDelay )}] float NOT NULL, 
            [{nameof( OhmMeterSettingNub.EndDelay )}] float NOT NULL); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}](
                [{nameof( OhmMeterSettingNub.Id )}] [int] NOT NULL,
                [{nameof( OhmMeterSettingNub.Nplc )}] [float] NOT NULL,
                [{nameof( OhmMeterSettingNub.AveragingFilterCount )}] [int] NOT NULL,
                [{nameof( OhmMeterSettingNub.AveragingFilterWindow )}] [float] NOT NULL,
                [{nameof( OhmMeterSettingNub.StartDelay )}] [float] NOT NULL,
                [{nameof( OhmMeterSettingNub.EndDelay )}] [float] NOT NULL,
             CONSTRAINT [PK_{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName})] PRIMARY KEY CLUSTERED 
            ([{nameof( OhmMeterSettingNub.Id )}] ASC
            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            ) ON [PRIMARY]
            END; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// Implements the Ohm Meter Setting table <see cref="IOhmMeterSetting">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-22 </para>
    /// </remarks>
    [Table( "OhmMeterSetting" )]
    public class OhmMeterSettingNub : EntityNubBase<IOhmMeterSetting>, IOhmMeterSetting
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public OhmMeterSettingNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOhmMeterSetting CreateNew()
        {
            return new OhmMeterSettingNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOhmMeterSetting CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOhmMeterSetting value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IOhmMeterSetting source, IOhmMeterSetting destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Nplc = source.Nplc;
            destination.AveragingFilterCount = source.AveragingFilterCount;
            destination.AveragingFilterWindow = source.AveragingFilterWindow;
            destination.EndDelay = source.EndDelay;
            destination.Id = source.Id;
            destination.StartDelay = source.StartDelay;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IOhmMeterSetting );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IOhmMeterSetting other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IOhmMeterSetting left, IOhmMeterSetting right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.Nplc, right.Nplc );
                result = result && Equals( left.AveragingFilterCount, right.AveragingFilterCount );
                result = result && Equals( left.AveragingFilterWindow, right.AveragingFilterWindow );
                result = result && Equals( left.EndDelay, right.EndDelay );
                result = result && Equals( left.Id, right.Id );
                result = result && Equals( left.StartDelay, right.StartDelay );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.Nplc, this.AveragingFilterCount, this.AveragingFilterWindow, this.EndDelay, this.Id, this.StartDelay ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Ohm Meter Setting. </summary>
        /// <value> Identifies the Ohm Meter Setting. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary>   Gets or sets the Nplc. </summary>
        /// <value> The Nplc. </value>
        public double Nplc { get; set; }

        /// <summary>   Gets or sets the number of averaging filters. </summary>
        /// <value> The number of averaging filters. </value>
        public int AveragingFilterCount { get; set; }

        /// <summary>   Gets or sets the averaging filter window. </summary>
        /// <value> The averaging filter window. </value>
        public double AveragingFilterWindow { get; set; }

        /// <summary>   Gets or sets the start delay. </summary>
        /// <value> The start delay. </value>
        public double StartDelay { get; set; }

        /// <summary>   Gets or sets the end delay. </summary>
        /// <value> The end delay. </value>
        public double EndDelay { get; set; }

        #endregion

    }

    /// <summary>
    /// the Ohm Meter Setting Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-22 </para>
    /// </remarks>
    public class OhmMeterSettingEntity : EntityBase<IOhmMeterSetting, OhmMeterSettingNub>, IOhmMeterSetting
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public OhmMeterSettingEntity() : this( new OhmMeterSettingNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmMeterSettingEntity( IOhmMeterSetting value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public OhmMeterSettingEntity( IOhmMeterSetting cache, IOhmMeterSetting store ) : base( new OhmMeterSettingNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmMeterSettingEntity( OhmMeterSettingEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.OhmMeterSettingBuilder.TableName, nameof( IOhmMeterSetting ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOhmMeterSetting CreateNew()
        {
            return new OhmMeterSettingNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOhmMeterSetting CreateCopy()
        {
            var destination = this.CreateNew();
            OhmMeterSettingNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOhmMeterSetting value )
        {
            OhmMeterSettingNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The OhmMeterSetting interface. </param>
        public override void UpdateCache( IOhmMeterSetting value )
        {
            // first make the copy to notify of any property change.
            OhmMeterSettingNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The OhmMeterSetting table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IOhmMeterSetting>( key ) : connection.Get<OhmMeterSettingNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Id );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="ohmMeterSettingId">    Identifies the Ohm Meter Setting. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int ohmMeterSettingId )
        {
            return this.FetchUsingKey( connection, ohmMeterSettingId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOhmMeterSetting entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.Id ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new OhmMeterSettingNub() { Id = key } );
        }

        #endregion

        #region " SHARED ENTITIES "

        /// <summary>   Gets or sets the Guard Band Lookup entities. </summary>
        /// <value> the Guard Band Lookup entities. </value>
        public static IEnumerable<OhmMeterSettingEntity> OhmMeterSettings { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSettingEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOhmMeterSetting>() ) : Populate( connection.GetAll<OhmMeterSettingNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            OhmMeterSettings = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( OhmMeterSettings ) );
            return OhmMeterSettings?.Any() == true ? OhmMeterSettings.Count() : 0;
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-04-28. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="ohmMeterSettingId">    Identifies the Ohm Meter Setting. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSettingEntity> FetchEntities( System.Data.IDbConnection connection, int ohmMeterSettingId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.OhmMeterSettingBuilder.TableName}] WHERE {nameof( OhmMeterSettingNub.Id )} = @Id", new { Id = ohmMeterSettingId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return Populate( connection.Query<OhmMeterSettingNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Populates a list of OhmMeterSetting entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The OhmMeterSetting nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSettingEntity> Populate( IEnumerable<OhmMeterSettingNub> nubs )
        {
            var l = new List<OhmMeterSettingEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( OhmMeterSettingNub nub in nubs )
                    l.Add( new OhmMeterSettingEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of OhmMeterSetting entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The OhmMeterSetting interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmMeterSettingEntity> Populate( IEnumerable<IOhmMeterSetting> interfaces )
        {
            var l = new List<OhmMeterSettingEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new OhmMeterSettingNub();
                foreach ( IOhmMeterSetting iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new OhmMeterSettingEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary>   Count entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.CountEntities( OhmMeterSettingBuilder.TableName );
        }

        /// <summary>   Dictionary of entity lookups. </summary>
        private static IDictionary<int, OhmMeterSettingEntity> _EntityLookupDictionary;

        /// <summary>   The entity lookup dictionary. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A Dictionary(Of Integer, OhmMeterSettingEntity) </returns>
        public static IDictionary<int, OhmMeterSettingEntity> EntityLookupDictionary()
        {
            if ( !(_EntityLookupDictionary?.Any()).GetValueOrDefault( false ) )
            {
                _EntityLookupDictionary = new Dictionary<int, OhmMeterSettingEntity>();
                foreach ( OhmMeterSettingEntity entity in OhmMeterSettings )
                    _EntityLookupDictionary.Add( entity.Id, entity );
            }

            return _EntityLookupDictionary;
        }

        /// <summary>   Checks if entities and related dictionaries are populated. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <returns>   True if enumerated, false if not. </returns>
        public static bool IsEnumerated()
        {
            return (OhmMeterSettings?.Any()).GetValueOrDefault( false ) &&
                   (_EntityLookupDictionary?.Any()).GetValueOrDefault( false );
        }

        #endregion

        #region " OHM METER SETTING "

        /// <summary>   Gets or sets the Ohm Meter Setting entity. </summary>
        /// <value> The Ohm Meter Setting entity. </value>
        public OhmMeterSettingEntity OhmMeterSetting { get; private set; }

        /// <summary>   Fetches the Ohm Meter Setting entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchOhmMeterSetting( System.Data.IDbConnection connection )
        {
            var entity = new OhmMeterSettingEntity();
            bool result = entity.FetchUsingKey( connection, this.Id );
            this.OhmMeterSetting = entity;
            this.NotifyPropertyChanged( nameof( OhmMeterSettingEntity.OhmMeterSetting ) );
            return result;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the Ohm Meter Setting. </summary>
        /// <value> Identifies the Ohm Meter Setting. </value>
        public int Id
        {
            get => this.ICache.Id;

            set {
                if ( !object.Equals( this.Id, value ) )
                {
                    this.ICache.Id = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the Nplc. </summary>
        /// <value> The Nplc. </value>
        public double Nplc
        {
            get => this.ICache.Nplc;

            set {
                if ( !object.Equals( this.Nplc, value ) )
                {
                    this.ICache.Nplc = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the number of averaging filters. </summary>
        /// <value> The number of averaging filters. </value>
        public int AveragingFilterCount
        {
            get => this.ICache.AveragingFilterCount;

            set {
                if ( !object.Equals( this.AveragingFilterCount, value ) )
                {
                    this.ICache.AveragingFilterCount = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the averaging filter window. </summary>
        /// <value> The averaging filter window. </value>
        public double AveragingFilterWindow
        {
            get => this.ICache.AveragingFilterWindow;

            set {
                if ( !object.Equals( this.AveragingFilterWindow, value ) )
                {
                    this.ICache.AveragingFilterWindow = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the start delay. </summary>
        /// <value> The start delay. </value>
        public double StartDelay
        {
            get => this.ICache.StartDelay;

            set {
                if ( !object.Equals( this.StartDelay, value ) )
                {
                    this.ICache.StartDelay = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the end delay. </summary>
        /// <value> The end delay. </value>
        public double EndDelay
        {
            get => this.ICache.EndDelay;

            set {
                if ( !object.Equals( this.EndDelay, value ) )
                {
                    this.ICache.EndDelay = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary>   Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="ohmMeterSettingId">    The entity unique key. </param>
        /// <returns>   A <see cref="OhmMeterSettingEntity"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, OhmMeterSettingEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int ohmMeterSettingId )
        {
            string activity = string.Empty;
            var entity = new OhmMeterSettingEntity();
            try
            {
                activity = $"Fetching {nameof( OhmMeterSettingEntity )} by {nameof( OhmMeterSettingNub.Id )} of {ohmMeterSettingId}";
                return entity.FetchUsingKey( connection, ohmMeterSettingId ) ? (true, string.Empty, entity) : (false, $"Failed {activity}", entity);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", entity);
            }
        }

        /// <summary>   Attempts to fetch all entities. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// The (Success As Boolean, details As String, Entities As IEnumerable(Of OhmMeterSettingEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string details, IEnumerable<OhmMeterSettingEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new OhmMeterSettingEntity();
                activity = $"fetching all {nameof( OhmMeterSettingEntity )}'s";
                int elementCount = entity.FetchAllEntities( connection );
                return elementCount != EntityLookupDictionary().Count
                    ? throw new InvalidOperationException( $"{nameof( OhmMeterSettingEntity.EntityLookupDictionary )} count must equal {nameof( OhmMeterSettings )} count " )
                    : (true, string.Empty, OhmMeterSettings);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", Array.Empty<OhmMeterSettingEntity>());
            }
        }

        /// <summary>   Fetches all. </summary>
        /// <remarks>   David, 2020-07-11. </remarks>
        /// <param name="connection">   The connection. </param>
        public static void FetchAll( System.Data.IDbConnection connection )
        {
            if ( !IsEnumerated() )
                _ = TryFetchAll( connection );
        }

        #endregion

    }

    /// <summary>   Collection of ohm meter setting entities. </summary>
    /// <remarks>   David, 2020-06-02. </remarks>
    public class OhmMeterSettingEntityCollection : EntityKeyedCollection<int, IOhmMeterSetting, OhmMeterSettingNub, OhmMeterSettingEntity>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public OhmMeterSettingEntityCollection() : base()
        {
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified OhmMeterSetting.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The OhmMeterSetting from which to extract the key. </param>
        /// <returns>   The key for the specified OhmMeterSetting. </returns>
        protected override int GetKeyForItem( OhmMeterSettingEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.Id;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( OhmMeterSettingEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all OhmMeterSettings from the
        /// <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<OhmMeterSettingEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( OhmMeterSettingEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-06-02. </remarks>
        /// <param name="toleranceCode">    The tolerance code. </param>
        /// <param name="meterId">          Identifies the <see cref="Dapper.Entities.MeterEntity"/>. </param>
        /// <param name="nominalValue">     The nominal value. </param>
        public void Add( string toleranceCode, int meterId, double nominalValue )
        {
            // assume the setting lookup entities were fetched
            this.Add( OhmMeterSettingEntity.EntityLookupDictionary()[OhmMeterSettingLookupEntity.SelectOhmMeterSettingLookupEntity( toleranceCode, meterId, nominalValue ).OhmMeterSettingId] );
        }
    }
}

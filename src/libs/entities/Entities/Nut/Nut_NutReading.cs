namespace isr.Dapper.Entities
{
    /// <summary>   A nut entity. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public partial class NutEntity
    {

        /// <summary>
        /// Gets or sets the <see cref="NutUniqueNutReadingEntityCollection">Nut Readings</see>.
        /// </summary>
        /// <value> The Nut Readings. </value>
        public NutUniqueNutReadingEntityCollection NutReadings { get; private set; }

        /// <summary>   Initializes the nut readings. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <param name="uut">      The <see cref="UutEntity"/>. </param>
        /// <param name="element">  The <see cref="ElementEntity"/>. </param>
        public void InitializeNutReadings( UutEntity uut, ElementEntity element )
        {
            this.NutReadings = new NutUniqueNutReadingEntityCollection( this.AutoId, element, this.NomTypeId, uut );
        }

        /// <summary>   Count nut readings. </summary>
        /// <remarks>   David, 2021-04-27. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The total number of nut readings. </returns>
        public int CountNutReadings( System.Data.IDbConnection connection )
        {
            if ( !MeterEntity.IsEnumerated() )
                _ = MeterEntity.TryFetchAll( connection );
            if ( !ReadingTypeEntity.IsEnumerated() )
                _ = ReadingTypeEntity.TryFetchAll( connection );
            return NutReadingEntity.CountEntities( connection, this.AutoId );
        }

        /// <summary>
        /// Fetches <see cref="NutUniqueNutReadingEntityCollection">Nut Readings</see>.
        /// </summary>
        /// <remarks>   David, 2020-03-31. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of Nut Readings. </returns>
        public int FetchNutReadings( System.Data.IDbConnection connection )
        {
            if ( !MeterEntity.IsEnumerated() )
                _ = MeterEntity.TryFetchAll( connection );
            if ( !ReadingTypeEntity.IsEnumerated() )
                _ = ReadingTypeEntity.TryFetchAll( connection );
            this.NutReadings.Clear();
            this.NutReadings.Populate( NutReadingEntity.FetchEntities( connection, this.AutoId ) );
            return this.NutReadings.Count;
        }

        /// <summary>
        /// Fetches <see cref="NutUniqueNutReadingEntityCollection">Nut Readings</see>.
        /// </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="element">      The <see cref="ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <param name="uut">          The <see cref="UutEntity"/>. </param>
        /// <returns>   The Nut Readings. </returns>
        public static NutUniqueNutReadingEntityCollection FetchNutReadings( System.Data.IDbConnection connection, int nutAutoId, ElementEntity element, int nomTypeId, UutEntity uut )
        {
            if ( !ReadingTypeEntity.IsEnumerated() )
                _ = ReadingTypeEntity.TryFetchAll( connection );
            var Values = new NutUniqueNutReadingEntityCollection( nutAutoId, element, nomTypeId, uut );
            var entities = NutReadingEntity.FetchEntities( connection, nutAutoId );
            Values.Populate( entities );
            return Values;
        }
    }
}

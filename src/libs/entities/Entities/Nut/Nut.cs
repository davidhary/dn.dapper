using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Nut builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class NutBuilder : KeyTwoForeignTimeBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( NutNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the first foreign reference table. </summary>
        /// <value> The name of the first foreign reference table. </value>
        public override string FirstForeignTableName { get; set; } = ElementBuilder.TableName;

        /// <summary>   Gets or sets the name of the first foreign reference table key. </summary>
        /// <value> The name of the first foreign reference table key. </value>
        public override string FirstForeignTableKeyName { get; set; } = nameof( ElementNub.AutoId );

        /// <summary>   Gets or sets the name of the Second foreign reference table. </summary>
        /// <value> The name of the Second foreign reference table. </value>
        public override string SecondForeignTableName { get; set; } = NomTypeBuilder.TableName;

        /// <summary>   Gets or sets the name of the Second foreign reference table key. </summary>
        /// <value> The name of the Second foreign reference table key. </value>
        public override string SecondForeignTableKeyName { get; set; } = nameof( NomTypeNub.Id );

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The new table. </returns>
        public string CreateTable( System.Data.IDbConnection connection )
        {
            // the same Element ID and Nominal Type would repeat for the NUT associated with the next UUT.
            return this.CreateTable( connection, UniqueIndexOptions.None );
        }

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public override string AutoIdFieldName { get; set; } = nameof( NutEntity.AutoId );

        /// <summary>   Gets or sets the name of the First foreign identifier field. </summary>
        /// <value> The name of the First foreign identifier field. </value>
        public override string FirstForeignIdFieldName { get; set; } = nameof( NutEntity.ElementAutoId );

        /// <summary>   Gets or sets the name of the second foreign identifier field. </summary>
        /// <value> The name of the second foreign identifier field. </value>
        public override string SecondForeignIdFieldName { get; set; } = nameof( NutEntity.NomTypeId );

        /// <summary>   Gets or sets the name of the timestamp field. </summary>
        /// <value> The name of the timestamp field. </value>
        public override string TimestampFieldName { get; set; } = nameof( NutEntity.ToString );

        #region " SINGLETON "

        private static readonly Lazy<NutBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static NutBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Nut table based on the <see cref="IKeyTwoForeignTime">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "Nut" )]
    public class NutNub : KeyTwoForeignTimeNub, IKeyTwoForeignTime
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public NutNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyTwoForeignTime CreateNew()
        {
            return new NutNub();
        }
    }

    /// <summary>   The "Dapper.Entities.NutEntity". Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public partial class NutEntity : EntityBase<IKeyTwoForeignTime, NutNub>, IKeyTwoForeignTime
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public NutEntity() : this( new NutNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public NutEntity( IKeyTwoForeignTime value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public NutEntity( IKeyTwoForeignTime cache, IKeyTwoForeignTime store ) : base( new NutNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public NutEntity( NutEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.NutBuilder.TableName, nameof( IKeyTwoForeignTime ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IKeyTwoForeignTime CreateNew()
        {
            return new NutNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyTwoForeignTime CreateCopy()
        {
            var destination = this.CreateNew();
            KeyTwoForeignTimeNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyTwoForeignTime value )
        {
            KeyTwoForeignTimeNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Nut interface. </param>
        public override void UpdateCache( IKeyTwoForeignTime value )
        {
            // first make the copy to notify of any property change.
            KeyTwoForeignTimeNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The Nut table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IKeyTwoForeignTime>( key ) : connection.Get<NutNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.ElementAutoId, this.NomTypeId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>
        ///                                 <see cref="Dapper.Entities.ElementEntity"/> . </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/> of the
        ///                                 <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, elementAutoId, nomTypeId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IKeyTwoForeignTime entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.FirstForeignId, entity.SecondForeignId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The primary key. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new NutNub() { AutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the <see cref="Dapper.Entities.NutEntity"/>'s. </summary>
        /// <value> The <see cref="Dapper.Entities.NutEntity"/>'s. </value>
        public IEnumerable<NutEntity> Nuts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<NutEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IKeyTwoForeignTime>() ) : Populate( connection.GetAll<NutNub>() );
        }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Nuts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( NutEntity.Nuts ) );
            return this.Nuts?.Any() == true ? this.Nuts.Count() : 0;
        }

        /// <summary>   Populates a list of Nut entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nubs"> The Nut nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<NutEntity> Populate( IEnumerable<NutNub> nubs )
        {
            var l = new List<NutEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( NutNub nub in nubs )
                    l.Add( new NutEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Populates a list of Nut entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="interfaces">   The Nut interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<NutEntity> Populate( IEnumerable<IKeyTwoForeignTime> interfaces )
        {
            var l = new List<NutEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new NutNub();
                foreach ( IKeyTwoForeignTime iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new NutEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count Nut entities by unique index; Returns 1 or 0. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>
        ///                                 <see cref="Dapper.Entities.ElementEntity"/> . </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/> of the
        ///                                 <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{NutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutNub.FirstForeignId )} = @firstForeignId", new { firstForeignId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( NutNub.SecondForeignId )} = @secondForeignId ", new { secondForeignId = nomTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches Nut entities by unique index; expected single or none. </summary>
        /// <remarks>   David, 2020-04-30. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>
        ///                                 <see cref="Dapper.Entities.ElementEntity"/> . </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/> of the
        ///                                 <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<NutNub> FetchEntities( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{NutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutNub.FirstForeignId )} = @firstForeignId", new { firstForeignId = elementAutoId } );
            _ = sqlBuilder.Where( $"{nameof( NutNub.SecondForeignId )} = @secondForeignId ", new { secondForeignId = nomTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<NutNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the <see cref="Dapper.Entities.NutEntity"/> Value exists. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>
        ///                                 <see cref="Dapper.Entities.ElementEntity"/> . </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/> of the
        ///                                 <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int elementAutoId, int nomTypeId )
        {
            return 1 == CountEntities( connection, elementAutoId, nomTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NutEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NutEntity"/>. </value>
        public int AutoId
        {
            get => this.ICache.AutoId;

            set {
                if ( !object.Equals( this.AutoId, value ) )
                {
                    this.ICache.AutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets or sets the id of the first foreign key. </summary>
        /// <value> Identifies the first foreign key. </value>
        public int FirstForeignId
        {
            get => this.ICache.FirstForeignId;

            set {
                if ( !object.Equals( ( object ) this.FirstForeignId, ( object ) value ) )
                {
                    this.ICache.FirstForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( NutEntity.ElementAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        public int ElementAutoId
        {
            get => this.FirstForeignId;

            set => this.FirstForeignId = value;
        }

        /// <summary>   Gets or sets the id of the second foreign key. </summary>
        /// <value> Identifies the Second foreign key. </value>
        public int SecondForeignId
        {
            get => this.ICache.SecondForeignId;

            set {
                if ( !object.Equals( ( object ) this.SecondForeignId, ( object ) value ) )
                {
                    this.ICache.SecondForeignId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( NutEntity.NomTypeId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="NomTypeEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </value>
        public int NomTypeId
        {
            get => this.SecondForeignId;

            set => this.SecondForeignId = value;
        }

        /// <summary>   Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTime.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary>   Gets the unique selector. </summary>
        /// <value> The unique selector. </value>
        public NutUniqueKey UniqueSelector => new( this );

        #endregion

    }

    /// <summary>   Collection of Nut entities. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class NutEntityCollection : EntityKeyedCollection<int, IKeyTwoForeignTime, NutNub, NutEntity>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public NutEntityCollection() : base()
        {
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns>   The key for the specified element. </returns>
        protected override int GetKeyForItem( NutEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.AutoId;
        }

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="item"> The object to be added to the end of the
        ///                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        ///                     can be <see langword="null" /> for reference types. </param>
        public new virtual void Add( NutEntity item )
        {
            base.Add( item );
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary>   Populates the given entities. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<NutEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( NutEntity entity in entities )
                    this.Add( entity );
            }
        }

        /// <summary>   Fetches nut readings. </summary>
        /// <remarks>   David, 2020-06-27. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of readings. </returns>
        public int FetchNutReadings( System.Data.IDbConnection connection )
        {
            int count = 0;
            foreach ( NutEntity nut in this )
                count += nut.FetchNutReadings( connection );
            return count;
        }

        /// <summary>   Initializes the nut readings. </summary>
        /// <remarks>   David, 2020-07-02. </remarks>
        /// <param name="uut">      The <see cref="UutEntity"/>. </param>
        /// <param name="elements"> The <see cref="ElementEntityCollection"/>. </param>
        public void InitializeNutReadings( UutEntity uut, ElementEntityCollection elements )
        {
            foreach ( NutEntity nut in this )
                nut.InitializeNutReadings( uut, elements[nut.ElementAutoId] );
        }

        /// <summary>   Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The number of affected records or the total records if none was affected. </returns>
        protected override int BulkUpsertThis( System.Data.IDbConnection connection )
        {
            return NutBuilder.Instance.Upsert( connection, this );
        }
    }

    /// <summary>   Collection of Nut entities uniquely associated with a UUT. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public class UutUniqueNutEntityCollection : NutEntityCollection
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        private UutUniqueNutEntityCollection() : base()
        {
            this._UniqueIndexDictionary = new Dictionary<int, NutUniqueKey>();
            this._PrimaryKeyDictionary = new Dictionary<NutUniqueKey, int>();
            this.NutUniqueKeyReadingBinEntityDictionary = new Dictionary<NutUniqueKey, ReadingBinEntity>();
            this.NutIdentityReadingBinEntityDictionary = new Dictionary<int, ReadingBinEntity>();
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        public UutUniqueNutEntityCollection( int uutAutoId ) : this()
        {
            this.UutAutoId = uutAutoId;
        }

        #endregion

        #region " NUT SELECTION "


        /// <summary>   Dictionary of unique indexes. </summary>
        private readonly IDictionary<int, NutUniqueKey> _UniqueIndexDictionary;

        /// <summary>   Dictionary of primary keys. </summary>
        private readonly IDictionary<NutUniqueKey, int> _PrimaryKeyDictionary;

        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="entity">   The object to be added to the end of the
        ///                         <see cref="T:System.Collections.ObjectModel.Collection`1" />. The
        ///                         value can be <see langword="null" /> for reference types. </param>
        public override void Add( NutEntity entity )
        {
            base.Add( entity );
            this._PrimaryKeyDictionary.Add( new NutUniqueKey( entity ), entity.AutoId );
            this._UniqueIndexDictionary.Add( entity.AutoId, new NutUniqueKey( entity ) );
        }

        /// <summary>
        /// Removes all Nuts from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        public override void Clear()
        {
            base.Clear();
            this._UniqueIndexDictionary.Clear();
            this._PrimaryKeyDictionary.Clear();
        }

        /// <summary>   Selects a <see cref="Dapper.Entities.NutEntity"/> from the collection. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="NomTypeEntity"/>. </param>
        /// <returns>   An <see cref="Dapper.Entities.NutEntity"/>. </returns>
        public NutEntity Nut( int elementAutoId, int nomTypeId )
        {
            int id = this._PrimaryKeyDictionary[new NutUniqueKey( elementAutoId, nomTypeId )];
            return this.Contains( id ) ? this[id] : new NutEntity();
        }

        /// <summary>
        /// Selects a <see cref="Dapper.Entities.NutEntity"/> from the collection using the
        /// <see cref="ElementEntity"/> ordinal number and the specified identified of the
        /// <see cref="NutEntity"/> <see cref="NomTypeEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="elements">             The <see cref="ElementEntity"/>'s. </param>
        /// <param name="elementOrdinalNumber"> The <see cref="ElementEntity"/> ordinal number. </param>
        /// <param name="nomTypeId">            Identifies the <see cref="NomTypeEntity"/>. </param>
        /// <returns>   An <see cref="Dapper.Entities.NutEntity"/>. </returns>
        public NutEntity Nut( ProductUniqueElementEntityCollection elements, int elementOrdinalNumber, int nomTypeId )
        {
            return this.Nut( elements.SelectElementByElementOrdinalNumber( elementOrdinalNumber ).AutoId, nomTypeId );
        }

        /// <summary>
        /// Selects a <see cref="Dapper.Entities.NutEntity"/> from the collection using the
        /// <see cref="ElementEntity"/> ordinal number and the default single (index 0)
        /// <see cref="NomTypeEntity"/> of the <see cref="ElementEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-07-13. </remarks>
        /// <param name="elements">             The <see cref="ElementEntity"/>'s. </param>
        /// <param name="elementOrdinalNumber"> The <see cref="ElementEntity"/> ordinal number. </param>
        /// <returns>   An <see cref="Dapper.Entities.NutEntity"/>. </returns>
        public NutEntity Nut( ProductUniqueElementEntityCollection elements, int elementOrdinalNumber )
        {
            var element = elements.SelectElementByElementOrdinalNumber( elementOrdinalNumber );
            return this.Nut( element.AutoId, element.NomTypes.First().Id );
        }

        /// <summary>   Nut element identifier. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The element id or -1 if not found. </returns>
        public int NutElementId( int nutAutoId )
        {
            return this._UniqueIndexDictionary.ContainsKey( nutAutoId ) ? this._UniqueIndexDictionary[nutAutoId].ElementId : -1;
        }

        /// <summary>   Nut nominal type identifier. </summary>
        /// <remarks>   David, 2020-06-14. </remarks>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public int NutNomTypeId( int nutAutoId )
        {
            return this._UniqueIndexDictionary.ContainsKey( nutAutoId ) ? this._UniqueIndexDictionary[nutAutoId].NomTypeId : -1;
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NutEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NutEntity"/>. </value>
        public int UutAutoId { get; private set; }

        #endregion

        #region " BIN SELECTION "

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.ReadingBinEntity"/>'s by the
        /// <see cref="Dapper.Entities.NutEntity"/> <see cref="NutEntity.AutoId"/>.
        /// </summary>
        /// <value> The reading bins. </value>
        public IDictionary<int, ReadingBinEntity> NutIdentityReadingBinEntityDictionary { get; private set; }

        /// <summary>
        /// Gets or sets the <see cref="Dapper.Entities.ReadingBinEntity"/>'s by the
        /// <see cref="Dapper.Entities.NutEntity"/> <see cref="NutEntity.UniqueSelector"/>.
        /// </summary>
        /// <value> The element reading bin identities. </value>
        public IDictionary<NutUniqueKey, ReadingBinEntity> NutUniqueKeyReadingBinEntityDictionary { get; private set; }

        /// <summary>   Clears the reading bins. </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        public void ClearReadingBins()
        {
            this.NutUniqueKeyReadingBinEntityDictionary.Clear();
            this.NutIdentityReadingBinEntityDictionary.Clear();
        }

        /// <summary>   Adds a reading bin to 'readingBinEntity'. </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <param name="nut">          The "Dapper.Entities.NutEntity". </param>
        /// <param name="readingBin">   The "Dapper.Entities.ReadingBinEntity". </param>
        public void AddReadingBin( NutEntity nut, ReadingBinEntity readingBin )
        {
            this.NutIdentityReadingBinEntityDictionary.Add( nut.AutoId, readingBin );
            this.NutUniqueKeyReadingBinEntityDictionary.Add( new NutUniqueKey( nut ), readingBin );
        }

        /// <summary>   Update or add a reading bin. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="nut">          The "Dapper.Entities.NutEntity". </param>
        /// <param name="readingBin">   The "Dapper.Entities.ReadingBinEntity". </param>
        public void UpaddReadingBin( NutEntity nut, ReadingBinEntity readingBin )
        {
            if ( this.NutIdentityReadingBinEntityDictionary.ContainsKey( nut.AutoId ) )
            {
                this.NutIdentityReadingBinEntityDictionary[nut.AutoId] = readingBin;
            }
            else
            {
                this.NutIdentityReadingBinEntityDictionary.Add( nut.AutoId, readingBin );
            }

            var key = new NutUniqueKey( nut );
            if ( this.NutUniqueKeyReadingBinEntityDictionary.ContainsKey( key ) )
            {
                this.NutUniqueKeyReadingBinEntityDictionary[key] = readingBin;
            }
            else
            {
                this.NutUniqueKeyReadingBinEntityDictionary.Add( key, readingBin );
            }
        }

        /// <summary>   Populates a reading bins. </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public int PopulateReadingBins( System.Data.IDbConnection connection )
        {
            this.ClearReadingBins();
            foreach ( NutEntity nutEntity in this )
                this.AddReadingBin( nutEntity, NutReadingBinEntity.FetchReadingBin( connection, nutEntity.AutoId ) );
            return this.NutIdentityReadingBinEntityDictionary.Count;
        }

        #endregion

    }

    /// <summary>   A nut unique key. </summary>
    /// <remarks>   David, 2020-06-14. </remarks>
    public struct NutUniqueKey : IEquatable<NutUniqueKey>
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="nut">  The nut. </param>
        public NutUniqueKey( NutEntity nut ) : this( nut.ElementAutoId, nut.NomTypeId )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-07-01. </remarks>
        /// <param name="elementAutoId">    Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">        Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        public NutUniqueKey( int elementAutoId, int nomTypeId )
        {
            this.ElementId = elementAutoId;
            this.NomTypeId = nomTypeId;
        }

        /// <summary>   Gets or sets the id of the element. </summary>
        /// <value> The identifier of the element. </value>
        public int ElementId { get; set; }

        /// <summary>   Gets or sets the id of the nom type. </summary>
        /// <value> The identifier of the nom type. </value>
        public int NomTypeId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( NutUniqueKey ) obj );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( NutUniqueKey other )
        {
            return this.ElementId == other.ElementId && this.NomTypeId == other.NomTypeId;
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.ElementId, this.NomTypeId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( NutUniqueKey left, NutUniqueKey right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( NutUniqueKey left, NutUniqueKey right )
        {
            return !(left == right);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Uut-Nut builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class UutNutBuilder : OneToManyBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( UutNutNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = UutBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( UutNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = NutBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( NutNub.AutoId );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( UutNutEntity.UutAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( UutNutEntity.NutAutoId );

        #region " SINGLETON "

        private static readonly Lazy<UutNutBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static UutNutBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Uut+Nut Nub based on the <see cref="IOneToMany">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "UutNut" )]
    public class UutNutNub : OneToManyNub, IOneToMany
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public UutNutNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new UutNutNub();
        }
    }

    /// <summary>   The Uut-Nut Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class UutNutEntity : EntityBase<IOneToMany, UutNutNub>, IOneToMany
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public UutNutEntity() : this( new UutNutNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public UutNutEntity( IOneToMany value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public UutNutEntity( IOneToMany cache, IOneToMany store ) : base( new UutNutNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public UutNutEntity( UutNutEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.UutNutBuilder.TableName, nameof( IOneToMany ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToMany CreateNew()
        {
            return new UutNutNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            OneToManyNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            OneToManyNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Uut+Nut interface. </param>
        public override void UpdateCache( IOneToMany value )
        {
            // first make the copy to notify of any property change.
            OneToManyNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int uutAutoId, int nutAutoId )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, uutAutoId, nutAutoId ).SingleOrDefault();
            return this.Enstore( nub );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.UutAutoId, this.NutAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.UutAutoId, this.NutAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int uutAutoId, int nutAutoId )
        {
            return this.FetchUsingKey( connection, uutAutoId, nutAutoId );
        }

        /// <summary>
        /// Tries to fetch an existing <see cref="Dapper.Entities.NutEntity"/> associated with this
        /// <see cref="Dapper.Entities.UutEntity"/>; otherwise, inserts a new <see cref="Dapper.Entities.NutEntity"/>
        /// .Then
        /// tries to fetch an existing or insert a new <see cref="UutNutEntity"/>.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.UutEntity"/> exists for the specified
        /// <paramref name="uutAutoId"/>
        /// Using this method assumes the Nut has a non-unique <see cref="NutEntity.ElementAutoId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutElementId"> Identifies the <see cref="Dapper.Entities.ElementEntity"/> owning this
        ///                             NUT. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainNut( System.Data.IDbConnection connection, int uutAutoId, int nutElementId, int nomTypeId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            this.NutEntity = FetchNut( connection, uutAutoId, nutElementId, nomTypeId );
            if ( !this.NutEntity.IsClean() )
            {
                // if Nut already exists for this UUT, then this is to be used; otherwise, add a new Nut
                this.NutEntity = new NutEntity() { ElementAutoId = nutElementId, NomTypeId = nomTypeId };
                if ( !this.NutEntity.Insert( connection ) )
                {
                    result = (false, $"Failed inserting {nameof( Dapper.Entities.NutEntity )} with {nameof( Dapper.Entities.NutEntity.ElementAutoId )} of {nutElementId}");
                }
            }

            if ( result.Success )
            {
                this.UutAutoId = uutAutoId;
                this.NutAutoId = this.NutEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( UutNutEntity.UutEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( UutNutEntity )} for [{nameof( Dapper.Entities.UutEntity.AutoId )},{nameof( Dapper.Entities.NutEntity.AutoId )}] of [{uutAutoId},{this.NutAutoId}]");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch an existing or insert a new <see cref="Dapper.Entities.UutEntity"/> and
        /// <see cref="Dapper.Entities.NutEntity"/> entities and fetch an existing or insert a new
        /// <see cref="UutNutEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutElementId"> Identifies the <see cref="Dapper.Entities.ElementEntity"/> owning this
        ///                             Nut. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int uutAutoId, int nutElementId, int nomTypeId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.UutEntity is null || this.UutEntity.AutoId != uutAutoId )
            {
                this.UutEntity = new UutEntity() { AutoId = uutAutoId };
                if ( !this.UutEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.UutEntity )} with {nameof( Dapper.Entities.UutEntity.AutoId )} of {uutAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainNut( connection, uutAutoId, nutElementId, nomTypeId );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( UutNutEntity.UutEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch an existing <see cref="Dapper.Entities.NutEntity"/> associated with this
        /// <see cref="Dapper.Entities.UutEntity"/>; otherwise, inserts a new <see cref="Dapper.Entities.NutEntity"/>
        /// .Then
        /// tries to fetch an existing or insert a new <see cref="UutNutEntity"/>.
        /// </summary>
        /// <remarks>
        /// Assumes that a <see cref="Dapper.Entities.UutEntity"/> exists for the specified
        /// <paramref name="uutAutoId"/>
        /// Using this method assumes the Nut has a non-unique <see cref="NutEntity.ElementAutoId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainNut( System.Data.IDbConnection connection, int uutAutoId, int nutAutoId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            this.NutEntity = new NutEntity() { AutoId = nutAutoId };
            if ( !this.NutEntity.FetchUsingKey( connection ) )
            {
                result = (false, $"Failed fetching {nameof( Dapper.Entities.NutEntity )} with {nameof( Dapper.Entities.NutEntity.AutoId )} of {nutAutoId}");
            }

            if ( result.Success )
            {
                this.UutAutoId = this.UutEntity.AutoId;
                this.NutAutoId = this.NutEntity.AutoId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( UutNutEntity.UutEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( UutNutEntity )} for [{nameof( Dapper.Entities.UutEntity.AutoId )},{nameof( Dapper.Entities.NutEntity.AutoId )}] of [{uutAutoId},{this.NutAutoId}]");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch an existing or insert a new <see cref="Dapper.Entities.UutEntity"/> and
        /// <see cref="Dapper.Entities.NutEntity"/> entities and fetch an existing or insert a new
        /// <see cref="UutNutEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int uutAutoId, int nutAutoId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.UutEntity is null || this.UutEntity.AutoId != uutAutoId )
            {
                this.UutEntity = new UutEntity() { AutoId = uutAutoId };
                if ( !this.UutEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed fetching {nameof( Dapper.Entities.UutEntity )} with {nameof( Dapper.Entities.UutEntity.AutoId )} of {uutAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainNut( connection, uutAutoId, nutAutoId );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( UutNutEntity.UutEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch an existing or insert a new <see cref="Dapper.Entities.UutEntity"/> and
        /// <see cref="Dapper.Entities.NutEntity"/> entities and fetch an existing or insert a new
        /// <see cref="UutNutEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection )
        {
            return this.TryObtain( connection, this.UutAutoId, this.NutAutoId );
        }

        /// <summary>
        /// Fetches an existing or inserts new <see cref="Dapper.Entities.UutEntity"/> and
        /// <see cref="Dapper.Entities.NutEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="UutNutEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutElementId"> Identifies the <see cref="Dapper.Entities.ElementEntity"/> owning this
        ///                             Nut. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool Obtain( System.Data.IDbConnection connection, int uutAutoId, int nutElementId, int nomTypeId )
        {
            var (Success, Details) = this.TryObtain( connection, uutAutoId, nutElementId, nomTypeId );
            return !Success ? throw new InvalidOperationException( Details ) : Success;
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToMany entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId, entity.SecondaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int uutAutoId, int nutAutoId )
        {
            return connection.Delete( new UutNutNub() { PrimaryId = uutAutoId, SecondaryId = nutAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Uut-Nut entities. </summary>
        /// <value> The Uut-Nut entities. </value>
        public IEnumerable<UutNutEntity> UutNuts { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<UutNutEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToMany>() ) : Populate( connection.GetAll<UutNutNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.UutNuts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( UutNutEntity.UutNuts ) );
            return this.UutNuts?.Any() == true ? this.UutNuts.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<UutNutEntity> Populate( IEnumerable<UutNutNub> nubs )
        {
            var l = new List<UutNutEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( UutNutNub nub in nubs )
                    l.Add( new UutNutEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<UutNutEntity> Populate( IEnumerable<IOneToMany> interfaces )
        {
            var l = new List<UutNutEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new UutNutNub();
                foreach ( IOneToMany iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new UutNutEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; returns up to Nut entities count. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{UutNutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutNutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<UutNutEntity> FetchEntities( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.UutNutBuilder.TableName}] WHERE {nameof( UutNutNub.PrimaryId )} = @Id", new { Id = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<UutNutNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by Nut. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The total number of entities by Nut. </returns>
        public static int CountEntitiesByNut( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{UutNutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutNutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by Nuts in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by Nuts in this
        /// collection.
        /// </returns>
        public static IEnumerable<UutNutEntity> FetchEntitiesByNut( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.UutNutBuilder.TableName}] WHERE {nameof( UutNutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<UutNutNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities; returns 1 or 0. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int uutAutoId, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{UutNutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutNutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            _ = sqlBuilder.Where( $"{nameof( UutNutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<UutNutNub> FetchNubs( System.Data.IDbConnection connection, int uutAutoId, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{UutNutBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( UutNutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            _ = sqlBuilder.Where( $"{nameof( UutNutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<UutNutNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Uut Nut exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int uutAutoId, int nutAutoId )
        {
            return 1 == CountEntities( connection, uutAutoId, nutAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the Uut entity. </summary>
        /// <value> The Uut entity. </value>
        public UutEntity UutEntity { get; private set; }

        /// <summary>   Fetches Uut Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The Uut Entity. </returns>
        public UutEntity FetchUutEntity( System.Data.IDbConnection connection )
        {
            var entity = new UutEntity();
            _ = entity.FetchUsingKey( connection, this.UutAutoId );
            this.UutEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Uuts associated with the specified <paramref name="nutAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The total number of Uuts. </returns>
        public static int CountUuts( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.UutNutBuilder.TableName}] WHERE {nameof( UutNutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Uuts associated with the specified <paramref name="nutAutoId"/>; expected a
        /// single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Uuts in this collection.
        /// </returns>
        public static IEnumerable<NutEntity> FetchUuts( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.UutNutBuilder.TableName}] WHERE {nameof( UutNutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<NutEntity>();
            foreach ( UutNutNub nub in connection.Query<UutNutNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new UutNutEntity( nub );
                l.Add( entity.FetchNutEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Uuts associated with the specified <paramref name="nutAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteUuts( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.UutNutBuilder.TableName}] WHERE {nameof( UutNutNub.SecondaryId )} = @SecondaryId", new { SecondaryId = nutAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the "Dapper.Entities.NutEntity". </summary>
        /// <value> the "Dapper.Entities.NutEntity". </value>
        public NutEntity NutEntity { get; private set; }

        /// <summary>   Fetches a Nut entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   the "Dapper.Entities.NutEntity". </returns>
        public NutEntity FetchNutEntity( System.Data.IDbConnection connection )
        {
            var entity = new NutEntity();
            _ = entity.FetchUsingKey( connection, this.NutAutoId );
            this.NutEntity = entity;
            return entity;
        }

        /// <summary>   Count nuts. </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   The total number of nuts. </returns>
        public static int CountNuts( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.UutNutBuilder.TableName}] WHERE {nameof( UutNutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the Nuts in this collection. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Nuts in this collection.
        /// </returns>
        public static IEnumerable<NutEntity> FetchNuts( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.UutNutBuilder.TableName}] WHERE {nameof( UutNutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<NutEntity>();
            foreach ( UutNutNub nub in connection.Query<UutNutNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new UutNutEntity( nub );
                l.Add( entity.FetchNutEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all <see cref="Dapper.Entities.NutEntity"/>'s related to the specified Uut.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteNuts( System.Data.IDbConnection connection, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.UutNutBuilder.TableName}] WHERE {nameof( UutNutNub.PrimaryId )} = @PrimaryId", new { PrimaryId = uutAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches the ordered Nuts in this collection. </summary>
        /// <remarks>   David, 2020-05-18. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered Nuts in this collection.
        /// </returns>
        public static IEnumerable<NutEntity> FetchOrderedNuts( System.Data.IDbConnection connection, string selectQuery, int uutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = uutAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return NutEntity.Populate( connection.Query<NutNub>( template.RawSql, template.Parameters ) );
        }

        /// <summary>   Fetches the ordered Nuts in this collection. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ordered Nuts in this collection.
        /// </returns>
        public static IEnumerable<NutEntity> FetchOrderedNuts( System.Data.IDbConnection connection, int uutAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [UutNut] on [UutNut].SecondaryId = [Nut].AutoId where [UutNut].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{NutBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{NutBuilder.TableName}] Inner Join [{UutNutBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.UutNutBuilder.TableName}].{nameof( UutNutNub.SecondaryId )} = [{isr.Dapper.Entities.NutBuilder.TableName}].{nameof( NutNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE [{isr.Dapper.Entities.UutNutBuilder.TableName}].{nameof( UutNutNub.PrimaryId )} = @Id" );
            _ = queryBuilder.AppendLine( $"ORDER BY [{isr.Dapper.Entities.NutBuilder.TableName}].{nameof( NutNub.FirstForeignId )} ASC, [{isr.Dapper.Entities.NutBuilder.TableName}].{nameof( NutNub.SecondForeignId )} ASC; " );
            return FetchOrderedNuts( connection, queryBuilder.ToString(), uutAutoId );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.NutEntity"/>. </summary>
        /// <remarks>   David, 2020-05-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutElementId"> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   The Nut. </returns>
        public static NutEntity FetchNut( System.Data.IDbConnection connection, string selectQuery, int uutAutoId, int nutElementId, int nomTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { uutAutoId, nutElementId, nomTypeId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<NutNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new NutEntity() : new NutEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches an <see cref="Dapper.Entities.NutEntity"/>. </summary>
        /// <remarks>
        /// David, 2020-05-19.<code>
        /// SELECT [Nut].* FROM [Nut] Inner Join [UutNut] ON [UutNut].SecondaryId = [Nut].AutoId WHERE
        /// ([UutNut].PrimaryId = 7 AND [Nut].FirstForeignId = 13 AND [Nut].SecondForeignId = 1);</code>
        /// </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uutAutoId">    Identifies the <see cref="Dapper.Entities.UutEntity"/>. </param>
        /// <param name="nutElementId"> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </param>
        /// <param name="nomTypeId">    Identifies the <see cref="Dapper.Entities.NomTypeEntity"/>. </param>
        /// <returns>   The Nut. </returns>
        public static NutEntity FetchNut( System.Data.IDbConnection connection, int uutAutoId, int nutElementId, int nomTypeId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [UutNut] on [UutNut].SecondaryId = [Nut].AutoId where [UutNut].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{NutBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{NutBuilder.TableName}] Inner Join [{UutNutBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.UutNutBuilder.TableName}].{nameof( UutNutNub.SecondaryId )} = [{isr.Dapper.Entities.NutBuilder.TableName}].{nameof( NutNub.AutoId )}" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.UutNutBuilder.TableName}].{nameof( UutNutNub.PrimaryId )} = @{nameof( uutAutoId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.NutBuilder.TableName}].{nameof( NutNub.FirstForeignId )} = @{nameof( nutElementId )} " );
            _ = queryBuilder.AppendLine( $"AND [{isr.Dapper.Entities.NutBuilder.TableName}].{nameof( NutNub.SecondForeignId )} = @{nameof( nomTypeId )}); " );
            return FetchNut( connection, queryBuilder.ToString(), uutAutoId, nutElementId, nomTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( UutNutEntity.UutAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the Uut. </summary>
        /// <value> Identifies the Uut. </value>
        public int UutAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( UutNutEntity.NutAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NutEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NutEntity"/>. </value>
        public int NutAutoId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

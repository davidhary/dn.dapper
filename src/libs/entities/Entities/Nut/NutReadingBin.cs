using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   A Nut+ReadingBin builder. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class NutReadingBinBuilder : OneToOneBuilder
    {

        /// <summary>   Gets the name of the table. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        protected override string TableNameThis => TableName;

        /// <summary>   Name of the table. </summary>
        private static string _TableName;

        /// <summary>   Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks>   David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( NutReadingBinNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public override string PrimaryTableName { get; set; } = NutBuilder.TableName;

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public override string PrimaryTableKeyName { get; set; } = nameof( NutNub.AutoId );

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public override string SecondaryTableName { get; set; } = ReadingBinBuilder.TableName;

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public override string SecondaryTableKeyName { get; set; } = nameof( ReadingBinNub.Id );

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public override string PrimaryIdFieldName { get; set; } = nameof( NutReadingBinEntity.NutAutoId );

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public override string SecondaryIdFieldName { get; set; } = nameof( NutReadingBinEntity.ReadingBinId );

        /// <summary>
        /// Inserts or ignores the records described by the <see cref="ReadingBin"/> enumeration type.
        /// </summary>
        /// <remarks>   David, 2020-05-11. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public static int InsertIgnoreDefaultValues( System.Data.IDbConnection connection )
        {
            return ReadingBinBuilder.Instance.InsertIgnore( connection, typeof( ReadingBin ), Array.Empty<int>() );
        }

        #region " SINGLETON "

        private static readonly Lazy<NutReadingBinBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static NutReadingBinBuilder Instance => LazyBuilder.Value;

        #endregion

    }

    /// <summary>
    /// Implements the Nut+ReadingBin Nub based on the <see cref="IOneToOne">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [Table( "NutReadingBin" )]
    public class NutReadingBinNub : OneToOneNub, IOneToOne
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public NutReadingBinNub() : base()
        {
        }

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToOne CreateNew()
        {
            return new NutReadingBinNub();
        }
    }

    /// <summary>   The Nut+ReadingBin Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public class NutReadingBinEntity : EntityBase<IOneToOne, NutReadingBinNub>, IOneToOne
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        public NutReadingBinEntity() : this( new NutReadingBinNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public NutReadingBinEntity( IOneToOne value ) : this( value, null )
        {
        }

        /// <summary>   Constructs an entity that is already stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="cache">    The cache. </param>
        /// <param name="store">    The store. </param>
        public NutReadingBinEntity( IOneToOne cache, IOneToOne store ) : base( new NutReadingBinNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public NutReadingBinEntity( NutReadingBinEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Entities.NutReadingBinBuilder.TableName, nameof( IOneToOne ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public override IOneToOne CreateNew()
        {
            return new NutReadingBinNub();
        }

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToOne CreateCopy()
        {
            var destination = this.CreateNew();
            OneToOneNub.Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToOne value )
        {
            OneToOneNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The Nut+ReadingBin interface. </param>
        public override void UpdateCache( IOneToOne value )
        {
            // first make the copy to notify of any property change.
            OneToOneNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary>   Fetches using key. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="key">          The key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IOneToOne>( key ) : connection.Get<NutReadingBinNub>( key ) );
        }

        /// <summary>   Refetch; Fetch using the given primary key. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.NutAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.NutAutoId );
        }

        /// <summary>   Fetches an existing entity using unique index. </summary>
        /// <remarks>   David, 2020-05-09. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int nutAutoId )
        {
            return this.FetchUsingKey( connection, nutAutoId );
        }

        /// <summary>
        /// Tries to fetch existing or insert a new <see cref="Dapper.Entities.ReadingBinEntity"/> entity and
        /// fetches an existing or inserts a new <see cref="NutReadingBinEntity"/>.
        /// </summary>
        /// <remarks>
        /// Assumes that an <see cref="Dapper.Entities.NutEntity"/> exists for the specified
        /// <paramref name="nutAutoId"/>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="binId">        Identifies the bin. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtainReadingBin( System.Data.IDbConnection connection, int nutAutoId, int binId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            this.ReadingBinEntity = new ReadingBinEntity() { Id = binId };
            if ( !this.ReadingBinEntity.FetchUsingKey( connection ) )
            {
                result = (false, $"Failed obtaining {nameof( Dapper.Entities.ReadingBinEntity )} with {nameof( Dapper.Entities.ReadingBinEntity.Id )} of {binId}");
            }

            if ( result.Success )
            {
                this.NutAutoId = nutAutoId;
                this.ReadingBinId = binId;
                if ( this.Obtain( connection ) )
                {
                    this.NotifyPropertyChanged( nameof( NutReadingBinEntity.ReadingBinEntity ) );
                }
                else
                {
                    result = (false, $"Failed obtaining {nameof( NutReadingBinEntity )} for [{nameof( Dapper.Entities.NutEntity.AutoId )},{nameof( Dapper.Entities.ReadingBinEntity.Id )}] of [{nutAutoId},{binId}]");
                }
            }

            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.NutEntity"/> and
        /// <see cref="Dapper.Entities.ReadingBinEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="NutReadingBinEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="binId">        Identifies the bin. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection, int nutAutoId, int binId )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.NutEntity is null || this.NutEntity.AutoId != nutAutoId )
            {
                // ensure an Nut entity exists.
                this.NutEntity = new NutEntity() { AutoId = nutAutoId };
                if ( !this.NutEntity.FetchUsingKey( connection ) )
                {
                    result = (false, $"Failed obtaining {nameof( Dapper.Entities.NutEntity )} with {nameof( Dapper.Entities.NutEntity.AutoId )} of {nutAutoId}");
                }
            }

            if ( result.Success )
            {
                result = this.TryObtainReadingBin( connection, nutAutoId, binId );
            }

            if ( result.Success )
                this.NotifyPropertyChanged( nameof( NutReadingBinEntity.NutEntity ) );
            return result;
        }

        /// <summary>
        /// Tries to fetch existing or insert new <see cref="Dapper.Entities.NutEntity"/> and
        /// <see cref="Dapper.Entities.ReadingBinEntity"/> entities and fetches an existing or inserts a new
        /// <see cref="NutReadingBinEntity"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryObtain( System.Data.IDbConnection connection )
        {
            return this.TryObtain( connection, this.NutAutoId, this.ReadingBinId );
        }

        /// <summary>   Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entity">       The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOneToOne entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.PrimaryId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary>   Deletes a record using the given primary key. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">       The connection. </param>
        /// <param name="nutAutoId">        Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="readingBinAutoId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int nutAutoId, int readingBinAutoId )
        {
            return connection.Delete( new NutReadingBinNub() { PrimaryId = nutAutoId, SecondaryId = readingBinAutoId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary>   Gets or sets the Nut+ReadingBin entities. </summary>
        /// <value> The Nut+ReadingBin entities. </value>
        public IEnumerable<NutReadingBinEntity> NutReadingBins { get; private set; }

        /// <summary>   Fetches all records into entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="usingNativeTracking">  True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<NutReadingBinEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOneToOne>() ) : Populate( connection.GetAll<NutReadingBinNub>() );
        }

        /// <summary>   Fetches all records. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.NutReadingBins = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( NutReadingBinEntity.NutReadingBins ) );
            return this.NutReadingBins?.Any() == true ? this.NutReadingBins.Count() : 0;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="nubs"> The nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<NutReadingBinEntity> Populate( IEnumerable<NutReadingBinNub> nubs )
        {
            var l = new List<NutReadingBinEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( NutReadingBinNub nub in nubs )
                    l.Add( new NutReadingBinEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary>   Enumerates populate in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="interfaces">   The interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<NutReadingBinEntity> Populate( IEnumerable<IOneToOne> interfaces )
        {
            var l = new List<NutReadingBinEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new NutReadingBinNub();
                foreach ( IOneToOne iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new NutReadingBinEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary>   Count entities; expects one ot zero. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{NutReadingBinBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutReadingBinNub.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities in this collection; expects a single entity or none. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<NutReadingBinEntity> FetchEntities( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.NutReadingBinBuilder.TableName}] WHERE {nameof( NutReadingBinNub.PrimaryId )} = @Id", new { Id = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<NutReadingBinNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Count entities by ReadingBin. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="readingBinAutoId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   The total number of entities by ReadingBin. </returns>
        public static int CountEntitiesByReadingBin( System.Data.IDbConnection connection, int readingBinAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{NutReadingBinBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutReadingBinNub.SecondaryId )} = @SecondaryId", new { SecondaryId = readingBinAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the entities by ReadingBins in this collection. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="readingBinAutoId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities by ReadingBins in this
        /// collection.
        /// </returns>
        public static IEnumerable<NutReadingBinEntity> FetchEntitiesByReadingBin( System.Data.IDbConnection connection, int readingBinAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.NutReadingBinBuilder.TableName}] WHERE {nameof( NutReadingBinNub.SecondaryId )} = @SecondaryId", new { SecondaryId = readingBinAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<NutReadingBinNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary>   Fetches nubs; expects single entity or none. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<NutReadingBinNub> FetchNubs( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{NutReadingBinBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( NutReadingBinNub.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<NutReadingBinNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Determine if the Nut ReadingBin exists. </summary>
        /// <remarks>   David, 2020-03-10. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int nutAutoId )
        {
            return 1 == CountEntities( connection, nutAutoId );
        }

        #endregion

        #region " RELATIONS "

        /// <summary>   Gets or sets the "Dapper.Entities.NutEntity". </summary>
        /// <value> the "Dapper.Entities.NutEntity". </value>
        public NutEntity NutEntity { get; private set; }

        /// <summary>   Fetches Nut entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   the "Dapper.Entities.NutEntity". </returns>
        public NutEntity FetchNutEntity( System.Data.IDbConnection connection )
        {
            var entity = new NutEntity();
            _ = entity.FetchUsingKey( connection, this.NutAutoId );
            this.NutEntity = entity;
            return entity;
        }

        /// <summary>
        /// Count Nuts associated with the specified <paramref name="readingBinAutoId"/>; expected 1.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="readingBinAutoId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   The total number of Nuts. </returns>
        public static int CountNuts( System.Data.IDbConnection connection, int readingBinAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.NutReadingBinBuilder.TableName}] WHERE {nameof( NutReadingBinNub.SecondaryId )} = @SecondaryId", new { SecondaryId = readingBinAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches the Nuts associated with the specified <paramref name="readingBinAutoId"/>; expected
        /// a single entity.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="readingBinAutoId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the Nuts in this collection.
        /// </returns>
        public static IEnumerable<ReadingBinEntity> FetchNuts( System.Data.IDbConnection connection, int readingBinAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.NutReadingBinBuilder.TableName}] WHERE {nameof( NutReadingBinNub.SecondaryId )} = @SecondaryId", new { SecondaryId = readingBinAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ReadingBinEntity>();
            foreach ( NutReadingBinNub nub in connection.Query<NutReadingBinNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new NutReadingBinEntity( nub );
                l.Add( entity.FetchReadingBinEntity( connection ) );
            }

            return l;
        }

        /// <summary>
        /// Deletes all Nuts associated with the specified <paramref name="readingBinAutoId"/>.
        /// </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">       The connection. </param>
        /// <param name="readingBinAutoId"> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteNuts( System.Data.IDbConnection connection, int readingBinAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.NutReadingBinBuilder.TableName}] WHERE {nameof( NutReadingBinNub.SecondaryId )} = @SecondaryId", new { SecondaryId = readingBinAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Gets or sets the ReadingBin entity. </summary>
        /// <value> The ReadingBin entity. </value>
        public ReadingBinEntity ReadingBinEntity { get; private set; }

        /// <summary>   Fetches a ReadingBin Entity. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The ReadingBin Entity. </returns>
        public ReadingBinEntity FetchReadingBinEntity( System.Data.IDbConnection connection )
        {
            var entity = new ReadingBinEntity();
            _ = entity.FetchUsingKey( connection, this.ReadingBinId );
            this.ReadingBinEntity = entity;
            return entity;
        }

        /// <summary>   Count reading bins. </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The total number of reading bins. </returns>
        public static int CountReadingBins( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                        $"SELECT COUNT(*)  FROM [{isr.Dapper.Entities.NutReadingBinBuilder.TableName}] WHERE {nameof( NutReadingBinNub.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>   Fetches the ReadingBins in this collection. </summary>
        /// <remarks>   David, 2020-06-13. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the ReadingBins in this collection.
        /// </returns>
        public static IEnumerable<ReadingBinEntity> FetchReadingBins( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Entities.NutReadingBinBuilder.TableName}] WHERE {nameof( NutReadingBinNub.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            var l = new List<ReadingBinEntity>();
            foreach ( NutReadingBinNub nub in connection.Query<NutReadingBinNub>( selector.RawSql, selector.Parameters ) )
            {
                var entity = new NutReadingBinEntity( nub );
                l.Add( entity.FetchReadingBinEntity( connection ) );
            }

            return l;
        }

        /// <summary>   Deletes all ReadingBin related to the specified Nut. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   An Integer. </returns>
        public static int DeleteReadingBins( System.Data.IDbConnection connection, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template template = sqlBuilder.AddTemplate(
                        $"DELETE FROM [{isr.Dapper.Entities.NutReadingBinBuilder.TableName}] WHERE {nameof( NutReadingBinNub.PrimaryId )} = @PrimaryId", new { PrimaryId = nutAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( template.RawSql, template.Parameters );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.ReadingBinEntity"/>. </summary>
        /// <remarks>   David, 2020-05-23. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The ReadingBin. </returns>
        public static ReadingBinEntity FetchReadingBin( System.Data.IDbConnection connection, string selectQuery, int nutAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            var template = sqlBuilder.AddTemplate( selectQuery.ToString(), new { Id = nutAutoId } );
            if ( template.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( template.RawSql )} null" );
            }
            else if ( template.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( template.Parameters )} null" );
            }

            var nub = connection.Query<ReadingBinNub>( template.RawSql, template.Parameters ).SingleOrDefault();
            return nub is null ? new ReadingBinEntity() : new ReadingBinEntity( nub, nub.CreateCopy() );
        }

        /// <summary>   Fetches a <see cref="Dapper.Entities.ReadingBinEntity"/>. </summary>
        /// <remarks>
        /// David, 2020-05-23.
        /// <code>
        /// SELECT [ReadingBin].*
        /// FROM [ReadingBin] Inner Join [NutReadingBin]
        /// ON [NutReadingBin].[SecondaryId] = [ReadingBin].[AutoId]
        /// WHERE ([NutReadingBin].[PrimaryId] = 8)
        /// </code>
        /// </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="nutAutoId">    Identifies the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns>   The ReadingBin. </returns>
        public static ReadingBinEntity FetchReadingBin( System.Data.IDbConnection connection, int nutAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            // Select [UUT].* From [UUT] Inner Join [NutReadingBin] on [NutReadingBin].SecondaryId = [ReadingBin].AutoId where [NutReadingBin].PrimaryId = 2
            _ = queryBuilder.AppendLine( $"SELECT [{ReadingBinBuilder.TableName}].*" );
            _ = queryBuilder.AppendLine( $"FROM [{ReadingBinBuilder.TableName}] Inner Join [{NutReadingBinBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $"ON [{isr.Dapper.Entities.NutReadingBinBuilder.TableName}].[{nameof( NutReadingBinNub.SecondaryId )}] = [{isr.Dapper.Entities.ReadingBinBuilder.TableName}].[{nameof( ReadingBinNub.Id )}]" );
            _ = queryBuilder.AppendLine( $"WHERE ([{isr.Dapper.Entities.NutReadingBinBuilder.TableName}].[{nameof( NutReadingBinNub.PrimaryId )}] = @Id); " );
            return FetchReadingBin( connection, queryBuilder.ToString(), nutAutoId );
        }


        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        public int PrimaryId
        {
            get => this.ICache.PrimaryId;

            set {
                if ( !object.Equals( ( object ) this.PrimaryId, ( object ) value ) )
                {
                    this.ICache.PrimaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( NutReadingBinEntity.NutAutoId ) );
                }
            }
        }

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.NutEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.NutEntity"/>. </value>
        public int NutAutoId
        {
            get => this.PrimaryId;

            set => this.PrimaryId = value;
        }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        public int SecondaryId
        {
            get => this.ICache.SecondaryId;

            set {
                if ( !object.Equals( ( object ) this.SecondaryId, ( object ) value ) )
                {
                    this.ICache.SecondaryId = value;
                    this.NotifyFieldChanged();
                    this.NotifyPropertyChanged( nameof( NutReadingBinEntity.ReadingBinId ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the <see cref="Dapper.Entities.ReadingBinEntity"/>.
        /// </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ReadingBinEntity"/>. </value>
        public int ReadingBinId
        {
            get => this.SecondaryId;

            set => this.SecondaryId = value;
        }

        #endregion

    }
}

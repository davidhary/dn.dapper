using System;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>   Interface for entities which have an identity key, foreign key and Label. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IKeyForeignLabel
    {

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [Key]
        int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the foreign reference table. </summary>
        /// <value> Identifies the foreign reference table. </value>
        int ForeignId { get; set; }

        /// <summary>   Gets or sets the element Label. </summary>
        /// <value> The Label. </value>
        string Label { get; set; }
    }

    /// <summary>   A builder for entities which have an identity key, foreign key and Label. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public abstract class KeyForeignLabelBuilder
    {

        #region " CRATE TABLE "

        /// <summary>   Gets the name of the table. </summary>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Gets or sets the name of the foreign table. </summary>
        /// <value> The name of the foreign table. </value>
        public abstract string ForeignTableName { get; set; }

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <value> The name of the foreign table key. </value>
        public abstract string ForeignTableKeyName { get; set; }

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <value> The size of the label field. </value>
        public abstract int LabelFieldSize { get; set; }

        /// <summary>   Gets the name of the ForeignId label index. </summary>
        /// <value> The name of the ForeignId label index. </value>
        private string ForeignIdLabelIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignLabelNub.ForeignId )}_{nameof( KeyForeignLabelNub.Label )}";

        /// <summary>   Gets the name of the label index. </summary>
        /// <value> The name of the label index. </value>
        private string LabelIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignLabelNub.Label )}";

        /// <summary>   Gets the name of the ForeignId index. </summary>
        /// <value> The name of the ForeignId index. </value>
        private string ForeignIdIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignLabelNub.ForeignId )}";

        /// <summary>   Options for controlling the unique index. </summary>
        private UniqueIndexOptions? _UniqueIndexOptions;

        /// <summary>   Queries unique index options. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The UniqueIndexOptions. </returns>
        public UniqueIndexOptions QueryUniqueIndexOptions( System.Data.IDbConnection connection )
        {
            if ( !this._UniqueIndexOptions.HasValue )
            {
                this._UniqueIndexOptions = UniqueIndexOptions.None;
                this._UniqueIndexOptions |= (connection.IndexExists( this.ForeignIdLabelIndexName ) ? UniqueIndexOptions.ForeignIdLabel : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.LabelIndexName ) ? UniqueIndexOptions.Label : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.ForeignIdIndexName ) ? UniqueIndexOptions.ForeignId : UniqueIndexOptions.None);
            }

            return this._UniqueIndexOptions.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-05-31. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql, uniqueIndexOptions )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite, uniqueIndexOptions ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
            [{nameof( KeyForeignLabelNub.AutoId )}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
            [{nameof( KeyForeignLabelNub.ForeignId )}] integer NOT NULL, 
            [{nameof( KeyForeignLabelNub.Label )}] nvarchar({this.LabelFieldSize}) NOT NULL, 
            FOREIGN KEY ([{nameof( KeyForeignLabelNub.ForeignId )}]) REFERENCES [{this.ForeignTableName}] ([{this.ForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Label) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.LabelIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyForeignLabelNub.Label )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.ForeignIdIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyForeignLabelNub.ForeignId )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignIdLabel) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.ForeignIdLabelIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyForeignLabelNub.ForeignId )}], [{nameof( KeyForeignLabelNub.Label )}]); ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( KeyForeignLabelNub.AutoId )}] [int] IDENTITY(1,1) NOT NULL,
                [{nameof( KeyForeignLabelNub.ForeignId )}] [int] NOT NULL,
                [{nameof( KeyForeignLabelNub.Label )}] [nvarchar]({this.LabelFieldSize}) NOT NULL,
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED 
            ([{nameof( KeyForeignLabelNub.AutoId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.ForeignTableName}] FOREIGN KEY([{nameof( KeyForeignLabelNub.ForeignId )}])
                REFERENCES [dbo].[{this.ForeignTableName}] ([{this.ForeignTableKeyName}])
                ON UPDATE CASCADE ON DELETE CASCADE; 

            IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.ForeignTableName}]; " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Label) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.LabelIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.LabelIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyForeignLabelNub.Label )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.ForeignIdIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.ForeignIdIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyForeignLabelNub.ForeignId )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignIdLabel) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.ForeignIdLabelIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.ForeignIdLabelIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyForeignLabelNub.ForeignId )}] ASC, [{nameof( KeyForeignLabelNub.Label )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

        #region " CREATE VIEW "

        /// <summary>   Gets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName => $"{this.TableNameThis}View";

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public abstract string AutoIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public abstract string ForeignIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public abstract string LabelFieldName { get; set; }

        /// <summary>   Creates a View. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The view name. </returns>
        public string CreateView( System.Data.IDbConnection connection, bool dropFirst )
        {
            _ = connection.CreateView( this.ViewName, this.BuildViewSelectQuery(), dropFirst );
            return this.ViewName;
        }

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected virtual string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( KeyForeignLabelNub.AutoId )}] AS [{this.AutoIdFieldName}], 
                [{nameof( KeyForeignLabelNub.ForeignId )}] AS [{this.ForeignIdFieldName}], 
                [{nameof( KeyForeignLabelNub.Label )}] AS [{this.LabelFieldName}] 
            FROM [{this.TableNameThis}]".Clean();
        }

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="IKeyForeignLabel">interface</see>
    /// of entities which have an identity key, foreign key and Label.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-27 </para>
    /// </remarks>
    public abstract class KeyForeignLabelNub : EntityNubBase<IKeyForeignLabel>, IKeyForeignLabel
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyForeignLabel CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyForeignLabel value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IKeyForeignLabel source, IKeyForeignLabel destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.AutoId = source.AutoId;
            destination.ForeignId = source.ForeignId;
            destination.Label = source.Label;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [Key]
        public int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the foreign reference table. </summary>
        /// <value> Identifies the foreign reference table. </value>
        public int ForeignId { get; set; }

        /// <summary>   Gets or sets the Ordinal Label. </summary>
        /// <value> The Ordinal Label. </value>
        public string Label { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IKeyForeignLabel );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IKeyForeignLabel other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IKeyForeignLabel left, IKeyForeignLabel right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.AutoId, right.AutoId );
                result = result && Equals( left.ForeignId, right.ForeignId );
                result = result && string.Equals( left.Label, right.Label );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.AutoId, this.ForeignId, this.Label ).GetHashCode();
        }


        #endregion

    }
}

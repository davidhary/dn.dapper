using System;
using System.Collections.Generic;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for entities which have an identity key, two foreign keys and a Real (Double) value
    /// amount.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IKeyTwoForeignReal
    {

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [Key]
        int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the first foreign key. </summary>
        /// <value> Identifies the first foreign key. </value>
        int FirstForeignId { get; set; }

        /// <summary>   Gets or sets the id of the second foreign key. </summary>
        /// <value> Identifies the Second foreign key. </value>
        int SecondForeignId { get; set; }

        /// <summary>   Gets or sets the amount. </summary>
        /// <value> The amount. </value>
        double Amount { get; set; }
    }

    /// <summary>
    /// A builder for entities which have an identity key, two foreign keys and Real (Double) value
    /// amount.
    /// </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public abstract class KeyTwoForeignRealBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Gets the name of the table. </summary>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Gets or sets the name of the first foreign reference table. </summary>
        /// <value> The name of the first foreign reference table. </value>
        public abstract string FirstForeignTableName { get; set; }

        /// <summary>   Gets or sets the name of the first foreign reference table key. </summary>
        /// <value> The name of the first foreign reference table key. </value>
        public abstract string FirstForeignTableKeyName { get; set; }

        /// <summary>   Gets or sets the name of the Second foreign reference table. </summary>
        /// <value> The name of the Second foreign reference table. </value>
        public abstract string SecondForeignTableName { get; set; }

        /// <summary>   Gets or sets the name of the Second foreign reference table key. </summary>
        /// <value> The name of the Second foreign reference table key. </value>
        public abstract string SecondForeignTableKeyName { get; set; }

        /// <summary>   Gets the name of the First + Second Foreign Id index. </summary>
        /// <value> The name of the First + Second Foreign Id index. </value>
        private string FistSecondForeignIdIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyTwoForeignRealNub.FirstForeignId )}_{nameof( KeyTwoForeignRealNub.SecondForeignId )}";

        /// <summary>   Options for controlling the unique index. </summary>
        private UniqueIndexOptions? _UniqueIndexOptions;

        /// <summary>   Queries unique index options. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The UniqueIndexOptions. </returns>
        public UniqueIndexOptions QueryUniqueIndexOptions( System.Data.IDbConnection connection )
        {
            if ( !this._UniqueIndexOptions.HasValue )
            {
                this._UniqueIndexOptions = UniqueIndexOptions.None;
                this._UniqueIndexOptions |= (connection.IndexExists( this.FistSecondForeignIdIndexName ) ? UniqueIndexOptions.FirstSecondForeignId : UniqueIndexOptions.None);
            }

            return this._UniqueIndexOptions.Value;
        }

        /// <summary>   Gets or sets the date time format. </summary>
        /// <value> The date time format. </value>
        public string DateTimeFormat { get; set; } = "yyyy-MM-dd HH:mm:ss.fff";

        /// <summary>   Updates or inserts records. </summary>
        /// <remarks>   David, 2020-06-15. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int Upsert( System.Data.IDbConnection connection, IEnumerable<IKeyTwoForeignReal> entities )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection.Upsert( this.TableNameThis, entities );
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql, uniqueIndexOptions )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite, uniqueIndexOptions ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
            [{nameof( KeyTwoForeignRealNub.AutoId )}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
            [{nameof( KeyTwoForeignRealNub.FirstForeignId )}] integer NOT NULL, 
            [{nameof( KeyTwoForeignRealNub.SecondForeignId )}] integer NOT NULL, 
            [{nameof( KeyTwoForeignRealNub.Amount )}] float NOT NULL, 
            FOREIGN KEY ([{nameof( KeyTwoForeignRealNub.FirstForeignId )}]) REFERENCES [{this.FirstForeignTableName}] ([{this.FirstForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE
            FOREIGN KEY ([{nameof( KeyTwoForeignRealNub.SecondForeignId )}]) REFERENCES [{this.SecondForeignTableName}] ([{this.SecondForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.FirstSecondForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.FistSecondForeignIdIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyTwoForeignRealNub.FirstForeignId )}],[{nameof( KeyTwoForeignRealNub.SecondForeignId )}]); ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( KeyTwoForeignRealNub.AutoId )}] [int] IDENTITY(1,1) NOT NULL,
                [{nameof( KeyTwoForeignRealNub.FirstForeignId )}] [int] NOT NULL,
                [{nameof( KeyTwoForeignRealNub.SecondForeignId )}] [int] NOT NULL,
                [{nameof( KeyTwoForeignRealNub.Amount )}] [float] NOT NULL,
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED 
            ([{nameof( KeyTwoForeignRealNub.AutoId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.FirstForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.FirstForeignTableName}] FOREIGN KEY([{nameof( KeyTwoForeignRealNub.FirstForeignId )}])
                REFERENCES [dbo].[{this.FirstForeignTableName}] ([{this.FirstForeignTableKeyName}])
                ON UPDATE CASCADE ON DELETE CASCADE; 

            IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.FirstForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.FirstForeignTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.SecondForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.SecondForeignTableName}] FOREIGN KEY([{nameof( KeyTwoForeignRealNub.SecondForeignId )}])
                REFERENCES [dbo].[{this.SecondForeignTableName}] ([{this.SecondForeignTableKeyName}])
                ON UPDATE CASCADE ON DELETE CASCADE; 

            IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.SecondForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.SecondForeignTableName}];" );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.FirstSecondForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.FistSecondForeignIdIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.FistSecondForeignIdIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyTwoForeignRealNub.FirstForeignId )}] ASC, [{nameof( KeyTwoForeignRealNub.SecondForeignId )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

        #region " CREATE VIEW "

        /// <summary>   Gets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName => $"{this.TableNameThis}View";

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public abstract string AutoIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the First foreign identifier field. </summary>
        /// <value> The name of the First foreign identifier field. </value>
        public abstract string FirstForeignIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the second foreign identifier field. </summary>
        /// <value> The name of the second foreign identifier field. </value>
        public abstract string SecondForeignIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public abstract string AmountFieldName { get; set; }

        /// <summary>   Creates a View. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The view name. </returns>
        public string CreateView( System.Data.IDbConnection connection, bool dropFirst )
        {
            _ = connection.CreateView( this.ViewName, this.BuildViewSelectQuery(), dropFirst );
            return this.ViewName;
        }

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected virtual string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( KeyTwoForeignRealNub.AutoId )}] AS [{this.AutoIdFieldName}], 
                [{nameof( KeyTwoForeignRealNub.FirstForeignId )}] AS [{this.FirstForeignIdFieldName}], 
                [{nameof( KeyTwoForeignRealNub.SecondForeignId )}] AS [{this.SecondForeignIdFieldName}], 
                [{nameof( KeyTwoForeignRealNub.Amount )}] AS [{this.AmountFieldName}]
            FROM [{this.TableNameThis}]".Clean();
        }

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="IKeyTwoForeignReal">interface</see>
    /// of entities which have an identity key, two foreign keys and Real (Double) value amount.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-27 </para>
    /// </remarks>
    public abstract class KeyTwoForeignRealNub : EntityNubBase<IKeyTwoForeignReal>, IKeyTwoForeignReal
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyTwoForeignReal CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyTwoForeignReal value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IKeyTwoForeignReal source, IKeyTwoForeignReal destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.AutoId = source.AutoId;
            destination.FirstForeignId = source.FirstForeignId;
            destination.SecondForeignId = source.SecondForeignId;
            destination.Amount = source.Amount;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [Key]
        public int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the first foreign reference table. </summary>
        /// <value> Identifies the first foreign reference table. </value>
        public int FirstForeignId { get; set; }

        /// <summary>   Gets or sets the id of the Second foreign reference table. </summary>
        /// <value> Identifies the Second foreign reference table. </value>
        public int SecondForeignId { get; set; }

        /// <summary>   Gets or sets the amount. </summary>
        /// <value> The amount. </value>
        public double Amount { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IKeyTwoForeignReal );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IKeyTwoForeignReal other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IKeyTwoForeignReal left, IKeyTwoForeignReal right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.AutoId, right.AutoId );
                result = result && Equals( left.FirstForeignId, right.FirstForeignId );
                result = result && Equals( left.SecondForeignId, right.SecondForeignId );
                result = result && Equals( left.Amount, right.Amount );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.AutoId, this.FirstForeignId, this.SecondForeignId, this.Amount ).GetHashCode();
        }

        #endregion

    }
}

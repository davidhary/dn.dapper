﻿using System;
using System.Collections.Generic;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the One-To-Many+Range nub and Entity. Includes the fields as kept in the data
    /// table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IOneToManyRange
    {

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> The identifier of the primary. </value>
        [ExplicitKey]
        int PrimaryId { get; set; }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of the secondary. </value>
        [ExplicitKey]
        int SecondaryId { get; set; }

        /// <summary>   Gets or sets the minimum value. </summary>
        /// <value> The minimum value. </value>
        double Min { get; set; }

        /// <summary>   Gets or sets the maximum value. </summary>
        /// <value> The maximum value. </value>
        double Max { get; set; }
    }

    /// <summary>   A builder for entities which have one-to-many relationship and a range. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public abstract class OneToManyRangeBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Gets the name of the table. </summary>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public abstract string PrimaryTableName { get; set; }

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public abstract string PrimaryTableKeyName { get; set; }

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public abstract string SecondaryTableName { get; set; }

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public abstract string SecondaryTableKeyName { get; set; }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int Upsert( System.Data.IDbConnection connection, IEnumerable<IOneToManyRange> entities )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection.Upsert( this.TableNameThis, entities );
        }

        /// <summary>   Inserts or ignores the records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of inserted or total existing records. </returns>
        public int InsertIgnore( System.Data.IDbConnection connection, IEnumerable<IOneToManyRange> entities )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.InsertIgnore( this.TableNameThis, entities );
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
                [{nameof( OneToManyRangeNub.PrimaryId )}] integer NOT NULL, 
                [{nameof( OneToManyRangeNub.SecondaryId )}] integer NOT NULL,
                [{nameof( OneToManyRangeNub.Min )}] float NOT NULL, 
                [{nameof( OneToManyRangeNub.Max )}] float NOT NULL, 
            CONSTRAINT [sqlite_autoindex_{this.TableNameThis}_1] PRIMARY KEY ([{nameof( OneToManyRangeNub.PrimaryId )}], [{nameof( OneToManyRangeNub.SecondaryId )}]),
            FOREIGN KEY ([{nameof( OneToManyRangeNub.PrimaryId )}]) REFERENCES [{this.PrimaryTableName}] ([{this.PrimaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( OneToManyRangeNub.SecondaryId )}]) REFERENCES [{this.SecondaryTableName}] ([{this.SecondaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( OneToManyRangeNub.PrimaryId )}] [int] NOT NULL,
                [{nameof( OneToManyRangeNub.SecondaryId )}] [int] NOT NULL,
                [{nameof( OneToManyRangeNub.Min )}] [float] NOT NULL,
                [{nameof( OneToManyRangeNub.Max )}] [float] NOT NULL,
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED
            ([{nameof( OneToManyRangeNub.PrimaryId )}] ASC, [{nameof( OneToManyRangeNub.SecondaryId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 
            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.SecondaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.SecondaryTableName}] FOREIGN KEY([{nameof( OneToManyRangeNub.SecondaryId )}])
            REFERENCES [dbo].[{this.SecondaryTableName}] ([{this.SecondaryTableKeyName}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.SecondaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.SecondaryTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.PrimaryTableName}') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.PrimaryTableName}] FOREIGN KEY([{nameof( OneToManyRangeNub.PrimaryId )}])
            REFERENCES [dbo].[{this.PrimaryTableName}] ([{this.PrimaryTableKeyName}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.PrimaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.PrimaryTableName}]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

        #region " CREATE VIEW "

        /// <summary>   Gets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName => $"{this.TableNameThis}View";

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public abstract string PrimaryIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public abstract string SecondaryIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the minimum field. </summary>
        /// <value> The name of the minimum field. </value>
        public abstract string MinFieldName { get; set; }

        /// <summary>   Gets or sets the name of the maximum field. </summary>
        /// <value> The name of the maximum field. </value>
        public abstract string MaxFieldName { get; set; }

        /// <summary>   Creates a View. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The view name. </returns>
        public string CreateView( System.Data.IDbConnection connection, bool dropFirst )
        {
            _ = connection.CreateView( this.ViewName, this.BuildViewSelectQuery(), dropFirst );
            return this.ViewName;
        }

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected virtual string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( OneToManyRangeNub.PrimaryId )}] AS [{this.PrimaryIdFieldName}], 
                [{nameof( OneToManyRangeNub.SecondaryId )}] AS [{this.SecondaryIdFieldName}],
                [{nameof( OneToManyRangeNub.Min )}] AS [{this.MinFieldName}],
                [{nameof( OneToManyRangeNub.Max )}] AS [{this.MaxFieldName}]
            FROM [{this.TableNameThis}]".Clean();
        }

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="IOneToManyRange">interface</see>
    /// of entities which are indexed two keys for selecting a range (Min and Max values).
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-27 </para>
    /// </remarks>
    public abstract class OneToManyRangeNub : Entity.EntityNubBase<IOneToManyRange>, IOneToManyRange
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToManyRange CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToManyRange value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given Range. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IOneToManyRange source, IOneToManyRange destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.PrimaryId = source.PrimaryId;
            destination.SecondaryId = source.SecondaryId;
            destination.Min = source.Min;
            destination.Max = source.Max;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> The identifier of the primary. </value>
        [ExplicitKey]
        public int PrimaryId { get; set; }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of the secondary. </value>
        [ExplicitKey]
        public int SecondaryId { get; set; }

        /// <summary>   Gets or sets the minimum value. </summary>
        /// <value> The minimum value. </value>
        public double Min { get; set; }

        /// <summary>   Gets or sets the maximum value. </summary>
        /// <value> The maximum value. </value>
        public double Max { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IOneToManyRange );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IOneToManyRange other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IOneToManyRange left, IOneToManyRange right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.PrimaryId, right.PrimaryId );
                result = result && Equals( left.SecondaryId, right.SecondaryId );
                result = result && Equals( left.Min, right.Min );
                result = result && Equals( left.Max, right.Max );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.PrimaryId, this.SecondaryId, this.Min, this.Max ).GetHashCode();
        }



        #endregion


    }
}
using System;
using System.Collections.Generic;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for entities which have an identity key, foreign key, Natural(Integer) value and
    /// timestamp.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IKeyForeignNaturalTime
    {

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [Key]
        int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the foreign reference table. </summary>
        /// <value> Identifies the foreign reference table. </value>
        int ForeignId { get; set; }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        int Amount { get; set; }

        /// <summary>   Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time. <para>
        /// Computed attribute ignores the property on either insert or update.
        /// https://dapper-tutorial.net/knowledge-base/57673107/what-s-the-difference-between--computed--and--write-false---attributes-
        /// The
        /// <see cref="KeyForeignNaturalTimeBuilder.UpdateTimestamp(System.Data.IDbConnection, int, DateTime)"/>
        /// is provided should the timestamp needs to be updated.         </para> <para>
        /// Note the SQLite stores time in the database in the time zone of the server. For example, if
        /// the server is located in LA, with a time zone of 7 hours relative to GMT (daylight saving
        /// time) storing 11:00:00AM using universal time value of 18:00:00Z, will store the time on the
        /// LA server as 11:00:00. To resolve these issues for both SQL Server the SQLite, time is
        /// converted to UTC and then to a date time format with no time zone specification (no Z).
        /// </para>
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        DateTime Timestamp { get; set; }
    }

    /// <summary>
    /// A builder for entities which have an identity key, foreign key, Natural(Integer) value and
    /// timestamp.
    /// </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public abstract class KeyForeignNaturalTimeBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Gets the name of the table. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Gets or sets the name of the foreign table. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The name of the foreign table. </value>
        public abstract string ForeignTableName { get; set; }

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The name of the foreign table key. </value>
        public abstract string ForeignTableKeyName { get; set; }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int Upsert( System.Data.IDbConnection connection, IEnumerable<IKeyForeignNaturalTime> entities )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection.Upsert( this.TableNameThis, entities );
        }

        /// <summary>   Inserts or ignores entity records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int InsertIgnore( System.Data.IDbConnection connection, IEnumerable<IKeyForeignNaturalTime> entities )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.InsertIgnore( this.TableNameThis, entities );
        }

        /// <summary>   Gets or sets the date time format. </summary>
        /// <value> The date time format. </value>
        public string DateTimeFormat { get; set; } = "yyyy-MM-dd HH:mm:ss.fff";

        /// <summary>   Converts a value to a timestamp format. </summary>
        /// <remarks>   David, 2020-06-21. </remarks>
        /// <param name="value">    The value Date/Time. </param>
        /// <returns>   Time value in the <see cref="DateTimeFormat"/> format. </returns>
        public string ToTimestampFormat( DateTime value )
        {
            return value.ToString( this.DateTimeFormat );
        }

        /// <summary>   Converts a value to an universal timestamp format. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="value">    The value Date/Time. </param>
        /// <returns>   Time value in the <see cref="DateTimeFormat"/> format. </returns>
        public string ToUniversalTimestampFormat( DateTime value )
        {
            return value.ToUniversalTime().ToString( this.DateTimeFormat );
        }

        /// <summary>   Updates the timestamp. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="key">                  The key. </param>
        /// <param name="universalTimestamp">   The universal (UTC) timestamp. </param>
        public void UpdateTimestamp( System.Data.IDbConnection connection, int key, DateTime universalTimestamp )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( $"UPDATE [{this.TableNameThis}] " );
            _ = queryBuilder.Append( $"SET [{nameof( KeyForeignNaturalTimeNub.Timestamp )}] = '{this.ToTimestampFormat( universalTimestamp )}' " );
            _ = queryBuilder.Append( $"WHERE [{nameof( KeyForeignNaturalTimeNub.AutoId )}] = {key}; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
        }

        /// <summary>   Gets the name of the foreign id index. </summary>
        /// <value> The name of the type index. </value>
        private string ForeignIdIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignNaturalTimeNub.ForeignId )}";

        /// <summary>   Gets the name of the amount index. </summary>
        /// <value> The name of the amount index. </value>
        private string AmountIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignNaturalTimeNub.Amount )}";

        /// <summary>   Gets the name of the foreign identifier amount index. </summary>
        /// <value> The name of the foreign identifier amount index. </value>
        private string ForeignIdAmountIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignNaturalTimeNub.ForeignId )}_{nameof( KeyForeignNaturalTimeNub.Amount )}";

        /// <summary>   Options for controlling the unique index. </summary>
        private UniqueIndexOptions? _UniqueIndexOptions;

        /// <summary>   Queries unique index options. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The UniqueIndexOptions. </returns>
        public UniqueIndexOptions QueryUniqueIndexOptions( System.Data.IDbConnection connection )
        {
            if ( !this._UniqueIndexOptions.HasValue )
            {
                this._UniqueIndexOptions = UniqueIndexOptions.None;
                this._UniqueIndexOptions |= (connection.IndexExists( this.ForeignIdAmountIndexName ) ? UniqueIndexOptions.ForeignIdAmount : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.AmountIndexName ) ? UniqueIndexOptions.Amount : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.ForeignIdIndexName ) ? UniqueIndexOptions.ForeignId : UniqueIndexOptions.None);
            }

            return this._UniqueIndexOptions.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql, uniqueIndexOptions )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite, uniqueIndexOptions ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
            [{nameof( KeyForeignNaturalTimeNub.AutoId )}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
            [{nameof( KeyForeignNaturalTimeNub.ForeignId )}] integer NOT NULL, 
            [{nameof( KeyForeignNaturalTimeNub.Amount )}] integer NOT NULL, 
            [{nameof( KeyForeignNaturalTimeNub.Timestamp )}] datetime NOT NULL DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
            FOREIGN KEY ([{nameof( KeyForeignNaturalTimeNub.ForeignId )}]) REFERENCES [{this.ForeignTableName}] ([{this.ForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Amount) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.AmountIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyForeignNaturalTimeNub.Amount )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.ForeignIdIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyForeignNaturalTimeNub.ForeignId )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignIdAmount) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.ForeignIdAmountIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyForeignNaturalTimeNub.ForeignId )}], [{nameof( KeyForeignNaturalTimeNub.Amount )}]); ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( KeyForeignNaturalTimeNub.AutoId )}] [int] IDENTITY(1,1) NOT NULL,
                [{nameof( KeyForeignNaturalTimeNub.ForeignId )}] [int] NOT NULL,
                [{nameof( KeyForeignNaturalTimeNub.Amount )}] [int] NOT NULL,
                [{nameof( KeyForeignNaturalTimeNub.Timestamp )}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED 
            ([{nameof( KeyForeignNaturalTimeNub.AutoId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.ForeignTableName}] FOREIGN KEY([{nameof( KeyForeignNaturalTimeNub.ForeignId )}])
                REFERENCES [dbo].[{this.ForeignTableName}] ([{this.ForeignTableKeyName}])
                ON UPDATE CASCADE ON DELETE CASCADE; 

            IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.ForeignTableName}]; " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Amount) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.AmountIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.AmountIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyForeignNaturalTimeNub.Amount )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.ForeignIdIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.ForeignIdIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyForeignNaturalTimeNub.ForeignId )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignIdLabel) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.ForeignIdAmountIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.ForeignIdAmountIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyForeignNaturalTimeNub.ForeignId )}] ASC, [{nameof( KeyForeignNaturalTimeNub.Amount )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

        #region " CREATE VIEW "

        /// <summary>   Gets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName => $"{this.TableNameThis}View";

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public abstract string AutoIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public abstract string ForeignIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public abstract string AmountFieldName { get; set; }

        /// <summary>   Gets or sets the name of the timestamp field. </summary>
        /// <value> The name of the timestamp field. </value>
        public abstract string TimestampFieldName { get; set; }

        /// <summary>   Creates a View. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The view name. </returns>
        public string CreateView( System.Data.IDbConnection connection, bool dropFirst )
        {
            _ = connection.CreateView( this.ViewName, this.BuildViewSelectQuery(), dropFirst );
            return this.ViewName;
        }

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected virtual string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( KeyForeignNaturalTimeNub.AutoId )}] AS [{this.AutoIdFieldName}], 
                [{nameof( KeyForeignNaturalTimeNub.ForeignId )}] AS [{this.ForeignIdFieldName}], 
                [{nameof( KeyForeignNaturalTimeNub.Amount )}] AS [{this.AmountFieldName}], 
                [{nameof( KeyForeignNaturalTimeNub.Timestamp )}] AS [{this.TimestampFieldName}]  
            FROM [{this.TableNameThis}]".Clean();
        }

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="IKeyForeignNaturalTime">interface</see>
    /// of entities which have an identity key, foreign key, Natural(Integer) value and timestamp.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-27 </para>
    /// </remarks>
    public abstract class KeyForeignNaturalTimeNub : EntityNubBase<IKeyForeignNaturalTime>, IKeyForeignNaturalTime
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyForeignNaturalTime CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyForeignNaturalTime value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IKeyForeignNaturalTime source, IKeyForeignNaturalTime destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.AutoId = source.AutoId;
            destination.ForeignId = source.ForeignId;
            destination.Amount = source.Amount;
            destination.Timestamp = source.Timestamp;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [Key]
        public int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the foreign reference table. </summary>
        /// <value> Identifies the foreign reference table. </value>
        public int ForeignId { get; set; }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        public int Amount { get; set; }

        /// <summary>   Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        public DateTime Timestamp { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IKeyForeignNaturalTime );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IKeyForeignNaturalTime other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IKeyForeignNaturalTime left, IKeyForeignNaturalTime right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.AutoId, right.AutoId );
                result = result && Equals( left.ForeignId, right.ForeignId );
                result = result && Equals( left.Amount, right.Amount );
                result = result && DateTime.Equals( left.Timestamp, right.Timestamp );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.AutoId, this.ForeignId, this.Amount, this.Timestamp ).GetHashCode();
        }


        #endregion

    }
}

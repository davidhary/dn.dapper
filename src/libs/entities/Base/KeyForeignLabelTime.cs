using System;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for entities which have an identity key, foreign key, Label and timestamp.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IKeyForeignLabelTime
    {

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [Key]
        int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the foreign reference table. </summary>
        /// <value> Identifies the foreign reference table. </value>
        int ForeignId { get; set; }

        /// <summary>   Gets or sets the element Label. </summary>
        /// <value> The Label. </value>
        string Label { get; set; }

        /// <summary>   Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time. <para>
        /// Computed attribute ignores the property on either insert or update.
        /// https://dapper-tutorial.net/knowledge-base/57673107/what-s-the-difference-between--computed--and--write-false---attributes-
        /// The
        /// <see cref="KeyForeignLabelTimeBuilder.UpdateTimestamp(System.Data.IDbConnection, int, DateTime)"/>
        /// is provided should the timestamp needs to be updated.         </para> <para>
        /// Note the SQLite stores time in the database in the time zone of the server. For example, if
        /// the server is located in LA, with a time zone of 7 hours relative to GMT (daylight saving
        /// time) storing 11:00:00AM using universal time value of 18:00:00Z, will store the time on the
        /// LA server as 11:00:00. To resolve these issues for both SQL Server the SQLite, time is
        /// converted to UTC and then to a date time format with no time zone specification (no Z).
        /// </para>
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        DateTime Timestamp { get; set; }
    }

    /// <summary>
    /// A builder for entities which have an identity key, foreign key, Label and timestamp.
    /// </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public abstract class KeyForeignLabelTimeBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Gets the name of the table. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Gets or sets the name of the foreign table. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The name of the foreign table. </value>
        public abstract string ForeignTableName { get; set; }

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The name of the foreign table key. </value>
        public abstract string ForeignTableKeyName { get; set; }

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The size of the label field. </value>
        public abstract int LabelFieldSize { get; set; }

        /// <summary>   Gets or sets the date time format. </summary>
        /// <value> The date time format. </value>
        public string DateTimeFormat { get; set; } = "yyyy-MM-dd HH:mm:ss.fff";

        /// <summary>   Converts a value to a timestamp format. </summary>
        /// <remarks>   David, 2020-06-21. </remarks>
        /// <param name="value">    The value Date/Time. </param>
        /// <returns>   Time value in the <see cref="DateTimeFormat"/> format. </returns>
        public string ToTimestampFormat( DateTime value )
        {
            return value.ToString( this.DateTimeFormat );
        }

        /// <summary>   Converts a value to an universal format. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="value">    The value Date/Time. </param>
        /// <returns>   Time value in the <see cref="DateTimeFormat"/> format. </returns>
        public string ToUniversalTimestampFormat( DateTime value )
        {
            return value.ToUniversalTime().ToString( this.DateTimeFormat );
        }

        /// <summary>   Updates the timestamp. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndex">          The unique index. </param>
        /// <param name="universalTimestamp">   The universal (UTC) timestamp. </param>
        public void UpdateTimestamp( System.Data.IDbConnection connection, string uniqueIndex, DateTime universalTimestamp )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( $"UPDATE [{this.TableNameThis}] " );
            _ = queryBuilder.Append( $"SET [{nameof( KeyForeignLabelTimeNub.Timestamp )}] = '{this.ToTimestampFormat( universalTimestamp )}' " );
            _ = queryBuilder.Append( $"WHERE [{nameof( KeyForeignLabelTimeNub.Label )}] = '{uniqueIndex}'; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
        }

        /// <summary>   Updates the timestamp. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="key">                  The key. </param>
        /// <param name="universalTimestamp">   The universal (UTC) timestamp. </param>
        public void UpdateTimestamp( System.Data.IDbConnection connection, int key, DateTime universalTimestamp )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( $"UPDATE [{this.TableNameThis}] " );
            _ = queryBuilder.Append( $"SET [{nameof( KeyForeignLabelTimeNub.Timestamp )}] = '{this.ToTimestampFormat( universalTimestamp )}' " );
            _ = queryBuilder.Append( $"WHERE [{nameof( KeyForeignLabelTimeNub.AutoId )}] = {key}; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
        }

        /// <summary>   Gets the name of the ForeignId label index. </summary>
        /// <value> The name of the ForeignId label index. </value>
        private string ForeignIdLabelIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignLabelTimeNub.ForeignId )}_{nameof( KeyForeignLabelTimeNub.Label )}";

        /// <summary>   Gets the name of the label index. </summary>
        /// <value> The name of the label index. </value>
        private string LabelIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignLabelTimeNub.Label )}";

        /// <summary>   Gets the name of the ForeignId index. </summary>
        /// <value> The name of the ForeignId index. </value>
        private string ForeignIdIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignLabelTimeNub.ForeignId )}";

        /// <summary>   Options for controlling the unique index. </summary>
        private UniqueIndexOptions? _UniqueIndexOptions;

        /// <summary>   Queries unique index options. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The UniqueIndexOptions. </returns>
        public UniqueIndexOptions QueryUniqueIndexOptions( System.Data.IDbConnection connection )
        {
            if ( !this._UniqueIndexOptions.HasValue )
            {
                this._UniqueIndexOptions = UniqueIndexOptions.None;
                this._UniqueIndexOptions |= (connection.IndexExists( this.ForeignIdLabelIndexName ) ? UniqueIndexOptions.ForeignIdLabel : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.LabelIndexName ) ? UniqueIndexOptions.Label : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.ForeignIdIndexName ) ? UniqueIndexOptions.ForeignId : UniqueIndexOptions.None);
            }

            return this._UniqueIndexOptions.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-05-31. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql, uniqueIndexOptions )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite, uniqueIndexOptions ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
            [{nameof( KeyForeignLabelTimeNub.AutoId )}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
            [{nameof( KeyForeignLabelTimeNub.ForeignId )}] integer NOT NULL, 
            [{nameof( KeyForeignLabelTimeNub.Label )}] nvarchar({this.LabelFieldSize}) NOT NULL, 
            [{nameof( KeyForeignLabelTimeNub.Timestamp )}] datetime NOT NULL DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
            FOREIGN KEY ([{nameof( KeyForeignLabelTimeNub.ForeignId )}]) REFERENCES [{this.ForeignTableName}] ([{this.ForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Label) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.LabelIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyForeignLabelTimeNub.Label )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.ForeignIdIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyForeignLabelTimeNub.ForeignId )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignIdLabel) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.ForeignIdLabelIndexName}] ON [{this.TableNameThis}] ([{nameof( KeyForeignLabelTimeNub.ForeignId )}], [{nameof( KeyForeignLabelTimeNub.Label )}]); ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( KeyForeignLabelTimeNub.AutoId )}] [int] IDENTITY(1,1) NOT NULL,
                [{nameof( KeyForeignLabelTimeNub.ForeignId )}] [int] NOT NULL,
                [{nameof( KeyForeignLabelTimeNub.Label )}] [nvarchar]({this.LabelFieldSize}) NOT NULL,
                [{nameof( KeyForeignLabelTimeNub.Timestamp )}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED 
            ([{nameof( KeyForeignLabelTimeNub.AutoId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.ForeignTableName}] FOREIGN KEY([{nameof( KeyForeignLabelTimeNub.ForeignId )}])
                REFERENCES [dbo].[{this.ForeignTableName}] ([{this.ForeignTableKeyName}])
                ON UPDATE CASCADE ON DELETE CASCADE; 

            IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.ForeignTableName}]; ".Clean() );

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Label) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.LabelIndexName}')
                    CREATE UNIQUE NONCLUSTERED INDEX [{this.LabelIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyForeignLabelTimeNub.Label )}] ASC) 
                     WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                     ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.ForeignIdIndexName}')
                    CREATE UNIQUE NONCLUSTERED INDEX [{this.ForeignIdIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyForeignLabelTimeNub.ForeignId )}] ASC) 
                     WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                     ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignIdLabel) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.ForeignIdLabelIndexName}')
                    CREATE UNIQUE NONCLUSTERED INDEX [{this.ForeignIdLabelIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( KeyForeignLabelTimeNub.ForeignId )}] ASC, [{nameof( KeyForeignLabelTimeNub.Label )}] ASC) 
                     WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                     ON [PRIMARY]; ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

        #region " CREATE VIEW "

        /// <summary>   Gets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName => $"{this.TableNameThis}View";

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public abstract string AutoIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public abstract string ForeignIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public abstract string LabelFieldName { get; set; }

        /// <summary>   Gets or sets the name of the timestamp field. </summary>
        /// <value> The name of the timestamp field. </value>
        public abstract string TimestampFieldName { get; set; }

        /// <summary>   Creates a View. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The view name. </returns>
        public string CreateView( System.Data.IDbConnection connection, bool dropFirst )
        {
            _ = connection.CreateView( this.ViewName, this.BuildViewSelectQuery(), dropFirst );
            return this.ViewName;
        }

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected virtual string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( KeyForeignLabelTimeNub.AutoId )}] AS [{this.AutoIdFieldName}], 
                [{nameof( KeyForeignLabelTimeNub.ForeignId )}] AS [{this.ForeignIdFieldName}], 
                [{nameof( KeyForeignLabelTimeNub.Label )}] AS [{this.LabelFieldName}], 
                [{nameof( KeyForeignLabelTimeNub.Timestamp )}] AS [{this.TimestampFieldName}]  
            FROM [{this.TableNameThis}]".Clean();
        }

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="IKeyForeignLabelTime">interface</see>
    /// of entities which have an identity key, foreign key, Label and timestamp.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-27 </para>
    /// </remarks>
    public abstract class KeyForeignLabelTimeNub : EntityNubBase<IKeyForeignLabelTime>, IKeyForeignLabelTime
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IKeyForeignLabelTime CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IKeyForeignLabelTime value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IKeyForeignLabelTime source, IKeyForeignLabelTime destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.AutoId = source.AutoId;
            destination.ForeignId = source.ForeignId;
            destination.Label = source.Label;
            destination.Timestamp = source.Timestamp;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [Key]
        public int AutoId { get; set; }

        /// <summary>   Gets or sets the id of the foreign reference table. </summary>
        /// <value> Identifies the foreign reference table. </value>
        public int ForeignId { get; set; }

        /// <summary>   Gets or sets the Ordinal Label. </summary>
        /// <value> The Ordinal Label. </value>
        public string Label { get; set; }

        /// <summary>   Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the computer <see cref="KeyLabelTimezoneNub.TimezoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        public DateTime Timestamp { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IKeyForeignLabelTime );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IKeyForeignLabelTime other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IKeyForeignLabelTime left, IKeyForeignLabelTime right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.AutoId, right.AutoId );
                result = result && Equals( left.ForeignId, right.ForeignId );
                result = result && string.Equals( left.Label, right.Label );
                result = result && DateTime.Equals( left.Timestamp, right.Timestamp );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.AutoId, this.ForeignId, this.Label, this.Timestamp ).GetHashCode();
        }


        #endregion

    }
}

﻿using System;
using System.Collections.Generic;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;
using isr.Dapper.Entity.EntityExtensions;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the nominal nub and entity. Includes the fields as kept in the data table.
    /// Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Nominal scales are used for labeling variables, without any quantitative value.  “Nominal”
    /// scales could simply be called “labels.”  Here are some examples, below.  Notice that all of
    /// these scales are mutually exclusive (no overlap) and none of them have any numerical
    /// significance.  A good way to remember all of this is that “nominal” sounds a lot like “name”
    /// and nominal scales are kind of like “names” or labels. Such values are typically represented
    /// as Enum structures. <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface INominal
    {

        /// <summary>   Gets or sets the id of the nominal value. </summary>
        /// <value> Identifies the nominal value. </value>
        [ExplicitKey]
        int Id { get; set; }

        /// <summary>   Gets or sets the label of the nominal value . </summary>
        /// <value> The nominal value label. </value>
        string Label { get; set; }

        /// <summary>   Gets or sets the description of the nominal value . </summary>
        /// <value> The nominal value description. </value>
        string Description { get; set; }
    }

    /// <summary>   A builder for table or nominal scale values. </summary>
    /// <remarks>   David, 2020-04-23. </remarks>
    public abstract class NominalBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Gets the name of the table. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Inserts or ignores the records described by the default enumeration type. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <returns>   An Integer. </returns>
        public abstract int InsertIgnoreDefaultRecords( System.Data.IDbConnection connection );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int Upsert( System.Data.IDbConnection connection, IEnumerable<INominal> entities )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection.Upsert( this.TableNameThis, entities );
        }

        /// <summary>   Inserts or ignores the entity records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int InsertIgnore( System.Data.IDbConnection connection, IEnumerable<INominal> entities )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.InsertIgnore( this.TableNameThis, entities );
        }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="type">         The enumeration type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   An Integer. </returns>
        public int InsertIgnore( System.Data.IDbConnection connection, Type type, IEnumerable<int> excluded )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.InsertIgnoreNameDescriptionRecords( this.TableNameThis, typeof( INominal ).EnumerateEntityFieldNames(), type, excluded );
        }

        /// <summary>   Gets the name of the label index. </summary>
        /// <value> The name of the label index. </value>
        private string LabelIndexName => $"UQ_{this.TableNameThis}_{nameof( KeyForeignLabelTimeNub.Label )}";

        /// <summary>   The using unique label. </summary>
        private bool? _UsingUniqueLabel;

        /// <summary>   Indicates if the entity uses a unique Label. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool UsingUniqueLabel( System.Data.IDbConnection connection )
        {
            if ( !this._UsingUniqueLabel.HasValue )
            {
                this._UsingUniqueLabel = connection.IndexExists( this.LabelIndexName );
            }

            return this._UsingUniqueLabel.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
                [{nameof( NominalNub.Id )}] integer NOT NULL PRIMARY KEY, 
                [{nameof( NominalNub.Label )}] nvarchar(50) NOT NULL, 
                [{nameof( NominalNub.Description )}] nvarchar(250) NOT NULL); 
            CREATE UNIQUE INDEX IF NOT EXISTS [{this.LabelIndexName}] ON [{this.TableNameThis}] ([{nameof( NominalNub.Label )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( NominalNub.Id )}] [int] NOT NULL,
                [{nameof( NominalNub.Label )}] [nvarchar](50) NOT NULL,
                [{nameof( NominalNub.Description )}] [nvarchar](250) NOT NULL,
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED 
            ([{nameof( NominalNub.Id )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 
            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.LabelIndexName}')
            CREATE UNIQUE NONCLUSTERED INDEX [{this.LabelIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( NominalNub.Label )}] ASC) 
             WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

    }

    /// <summary>
    /// Implements the nominal scale value table defined by <see cref="INominal">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    [Table( "Nominal" )]
    public abstract class NominalNub : EntityNubBase<INominal>, INominal
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override INominal CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( INominal value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( INominal source, INominal destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Id = source.Id;
            destination.Label = source.Label;
            destination.Description = source.Description;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the nominal value. </summary>
        /// <value> Identifies the nominal scale value. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary>   Gets or sets the nominal value label. </summary>
        /// <value> The nominal value label. </value>
        public string Label { get; set; }

        /// <summary>   Gets or sets the description of the nominal value . </summary>
        /// <value> The nominal value description. </value>
        public string Description { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as INominal );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( INominal other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Compares two entities based on their implemented interfaces. </summary>
        /// <remarks>   David, 2020-04-25. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     Specifies the entity to compare to. </param>
        /// <param name="right">    Specifies the entity to compare. </param>
        /// <returns>   <c>True</c> if the entities are equal. </returns>
        public static bool AreEqual( INominal left, INominal right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.Id, right.Id );
                result = result && string.Equals( left.Label, right.Label );
                result = result && string.Equals( left.Description, right.Description );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.Id, this.Label, this.Description ).GetHashCode();
        }



        #endregion


    }
}
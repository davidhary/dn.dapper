using System;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for entities with an explicit key, foreign key and a Natural(Integer) value.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IExplicitKeyForeignNatural
    {

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [ExplicitKey]
        int Id { get; set; }

        /// <summary>   Gets or sets the id of the foreign reference table. </summary>
        /// <value> Identifies the foreign reference table. </value>
        int ForeignId { get; set; }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        int Amount { get; set; }
    }

    /// <summary>
    /// A builder for entities with an explicit key, foreign key and a Natural(Integer) value.
    /// </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public abstract class ExplicitKeyForeignNaturalBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Gets the name of the table. </summary>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Gets or sets the name of the table. </summary>
        /// <value> The name of the foreign table. </value>
        public abstract string ForeignTableName { get; set; }

        /// <summary>   Gets or sets the name of the foreign table key. </summary>
        /// <value> The name of the foreign table key. </value>
        public abstract string ForeignTableKeyName { get; set; }

        /// <summary>   Gets the name of the foreign id index. </summary>
        /// <value> The name of the type index. </value>
        private string ForeignIdIndexName => $"UQ_{this.TableNameThis}_{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}";

        /// <summary>   Gets the name of the amount index. </summary>
        /// <value> The name of the amount index. </value>
        private string AmountIndexName => $"UQ_{this.TableNameThis}_{nameof( ExplicitKeyForeignNaturalNub.Amount )}";

        /// <summary>   Gets the name of the foreign identifier amount index. </summary>
        /// <value> The name of the foreign identifier amount index. </value>
        private string ForeignIdAmountIndexName => $"UQ_{this.TableNameThis}_{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}_{nameof( ExplicitKeyForeignNaturalNub.Amount )}";

        /// <summary>   Options for controlling the unique index. </summary>
        private UniqueIndexOptions? _UniqueIndexOptions;

        /// <summary>   Queries unique index options. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The UniqueIndexOptions. </returns>
        public UniqueIndexOptions QueryUniqueIndexOptions( System.Data.IDbConnection connection )
        {
            if ( !this._UniqueIndexOptions.HasValue )
            {
                this._UniqueIndexOptions = UniqueIndexOptions.None;
                this._UniqueIndexOptions |= (connection.IndexExists( this.ForeignIdAmountIndexName ) ? UniqueIndexOptions.ForeignIdAmount : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.AmountIndexName ) ? UniqueIndexOptions.Amount : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.ForeignIdIndexName ) ? UniqueIndexOptions.ForeignId : UniqueIndexOptions.None);
            }

            return this._UniqueIndexOptions.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql, uniqueIndexOptions )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite, uniqueIndexOptions ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The new table. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
            [{nameof( ExplicitKeyForeignNaturalNub.Id )}] integer NOT NULL PRIMARY KEY, 
            [{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}] integer NOT NULL, 
            [{nameof( ExplicitKeyForeignNaturalNub.Amount )}] integer NOT NULL, 
            FOREIGN KEY ([{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}]) REFERENCES [{this.ForeignTableName}] ([{this.ForeignTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Amount) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.AmountIndexName}] ON [{this.TableNameThis}] ([{nameof( ExplicitKeyForeignNaturalNub.Amount )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.ForeignIdIndexName}] ON [{this.TableNameThis}] ([{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignIdAmount) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.ForeignIdAmountIndexName}] ON [{this.TableNameThis}] ([{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}], [{nameof( ExplicitKeyForeignNaturalNub.Amount )}]); ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-05-06. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The new table. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( ExplicitKeyForeignNaturalNub.Id )}] [int] NOT NULL,
                [{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}] [int] NOT NULL,
                [{nameof( ExplicitKeyForeignNaturalNub.Amount )}] [int] NOT NULL,
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED 
            ([{nameof( ExplicitKeyForeignNaturalNub.Id )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.ForeignTableName}] FOREIGN KEY([{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}])
                REFERENCES [dbo].[{this.ForeignTableName}] ([{this.ForeignTableKeyName}])
                ON UPDATE CASCADE ON DELETE CASCADE; 

            IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.ForeignTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{this.TableNameThis}'))
                ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.ForeignTableName}]; " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Amount) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.AmountIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.AmountIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( ExplicitKeyForeignNaturalNub.Amount )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignId) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.ForeignIdIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.ForeignIdIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.ForeignIdLabel) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.ForeignIdAmountIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.ForeignIdAmountIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}] ASC, [{nameof( ExplicitKeyForeignNaturalNub.Amount )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

        #region " CREATE VIEW "

        /// <summary>   Gets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName => $"{this.TableNameThis}View";

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public abstract string IdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the foreign identifier field. </summary>
        /// <value> The name of the foreign identifier field. </value>
        public abstract string ForeignIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public abstract string AmountFieldName { get; set; }

        /// <summary>   Creates a View. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The view name. </returns>
        public string CreateView( System.Data.IDbConnection connection, bool dropFirst )
        {
            _ = connection.CreateView( this.ViewName, this.BuildViewSelectQuery(), dropFirst );
            return this.ViewName;
        }

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected virtual string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( ExplicitKeyForeignNaturalNub.Id )}] AS [{this.IdFieldName}], 
                [{nameof( ExplicitKeyForeignNaturalNub.ForeignId )}] AS [{this.ForeignIdFieldName}], 
                [{nameof( ExplicitKeyForeignNaturalNub.Amount )}] AS [{this.AmountFieldName}] 
            FROM [{this.TableNameThis}]".Clean();
        }

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="IExplicitKeyForeignNatural">interface</see>
    /// of entities with an explicit key, foreign key and a Natural(Integer) value.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public abstract class ExplicitKeyForeignNaturalNub : EntityNubBase<IExplicitKeyForeignNatural>, IExplicitKeyForeignNatural
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IExplicitKeyForeignNatural CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IExplicitKeyForeignNatural value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IExplicitKeyForeignNatural source, IExplicitKeyForeignNatural destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Id = source.Id;
            destination.ForeignId = source.ForeignId;
            destination.Amount = source.Amount;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary>   Gets or sets the id of the foreign reference table. </summary>
        /// <value> Identifies the foreign reference table. </value>
        public int ForeignId { get; set; }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        public int Amount { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IExplicitKeyForeignNatural );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IExplicitKeyForeignNatural other ) // Implements IEquatable(Of IExplicitKeyTypeNatural).Equals
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IExplicitKeyForeignNatural left, IExplicitKeyForeignNatural right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.Id, right.Id );
                result = result && Equals( left.ForeignId, right.ForeignId );
                result = result && Equals( left.Amount, right.Amount );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return (this.Id, this.ForeignId, this.Amount ).GetHashCode();
        }


        #endregion

    }
}

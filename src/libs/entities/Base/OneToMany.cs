using System;
using System.Collections.Generic;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the One-To-Many nub and Entity. Includes the fields as kept in the data table.
    /// Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IOneToMany
    {

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        [ExplicitKey]
        int PrimaryId { get; set; }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        [ExplicitKey]
        int SecondaryId { get; set; }
    }

    /// <summary>   A builder for entities which have one-to-many relationship. </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public abstract class OneToManyBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Gets the name of the table. </summary>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public abstract string PrimaryTableName { get; set; }

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public abstract string PrimaryTableKeyName { get; set; }

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public abstract string SecondaryTableName { get; set; }

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public abstract string SecondaryTableKeyName { get; set; }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int Upsert( System.Data.IDbConnection connection, IEnumerable<IOneToMany> entities )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection.Upsert( this.TableNameThis, entities );
        }

        /// <summary>   Inserts or ignores the records described by the entity values. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int InsertIgnore( System.Data.IDbConnection connection, IEnumerable<IOneToMany> entities )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.InsertIgnore( this.TableNameThis, entities );
        }

        /// <summary>   Gets the name of the unique secondary id index. </summary>
        /// <value> The name of the unique secondary id index. </value>
        private string UniqueSecondaryIdIndexName => $"UQ_{this.TableNameThis}_{nameof( OneToManyNub.SecondaryId )}";

        private bool? _usingUniqueSecondaryId;

        /// <summary>   Indicates if the entity uses a unique <see cref="OneToManyNub.SecondaryId"/>. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool UsingUniqueSecondaryId( System.Data.IDbConnection connection )
        {
            if ( !this._usingUniqueSecondaryId.HasValue )
            {
                this._usingUniqueSecondaryId = connection.IndexExists( this.UniqueSecondaryIdIndexName );
            }

            return this._usingUniqueSecondaryId.Value;
        }

        /// <summary>   Creates a table using non unique <see cref="OneToManyNub.SecondaryId"/>. </summary>
        /// <remarks>
        /// This would be the preferred selection for items such as unique lots of parts.
        /// </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table construction command. </returns>
        public string CreateTableNonUniqueSecondaryId( System.Data.IDbConnection connection )
        {
            return this.CreateTable( connection, false );
        }

        /// <summary>   Creates a table using a unique <see cref="OneToManyNub.SecondaryId"/>. </summary>
        /// <remarks>   David, 2020-05-04. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table construction command. </returns>
        public string CreateTableUniqueSecondaryId( System.Data.IDbConnection connection )
        {
            return this.CreateTable( connection, true );
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="uniqueSecondaryId">  True to enforce a unique <see cref="OneToManyNub.SecondaryId"/>. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection, bool uniqueSecondaryId )
        {
            this._usingUniqueSecondaryId = uniqueSecondaryId;
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql, uniqueSecondaryId )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite, uniqueSecondaryId ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueSecondaryId">    True to enforce a unique <see cref="OneToManyNub.SecondaryId"/>. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection, bool uniqueSecondaryId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
                [{nameof( OneToManyNub.PrimaryId )}] integer NOT NULL, 
                [{nameof( OneToManyNub.SecondaryId )}] integer NOT NULL,
            CONSTRAINT [sqlite_autoindex_{this.TableNameThis}_1] PRIMARY KEY ([{nameof( OneToManyNub.PrimaryId )}], [{nameof( OneToManyNub.SecondaryId )}]),
            FOREIGN KEY ([{nameof( OneToManyNub.PrimaryId )}]) REFERENCES [{this.PrimaryTableName}] ([{this.PrimaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( OneToManyNub.SecondaryId )}]) REFERENCES [{this.SecondaryTableName}] ([{this.SecondaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            if ( uniqueSecondaryId )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.UniqueSecondaryIdIndexName}] ON [{this.TableNameThis}] ([{nameof( OneToManyNub.SecondaryId )}]); ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueSecondaryId">    True to enforce a unique <see cref="OneToManyNub.SecondaryId"/>. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection, bool uniqueSecondaryId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( OneToManyNub.PrimaryId )}] [int] NOT NULL,
                [{nameof( OneToManyNub.SecondaryId )}] [int] NOT NULL,
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED
            ([{nameof( OneToManyNub.PrimaryId )}] ASC, [{nameof( OneToManyNub.SecondaryId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 
            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.SecondaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.SecondaryTableName}] FOREIGN KEY([{nameof( OneToManyNub.SecondaryId )}])
            REFERENCES [dbo].[{this.SecondaryTableName}] ([{this.SecondaryTableKeyName}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.SecondaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.SecondaryTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.PrimaryTableName}') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.PrimaryTableName}] FOREIGN KEY([{nameof( OneToManyNub.PrimaryId )}])
            REFERENCES [dbo].[{this.PrimaryTableName}] ([{this.PrimaryTableKeyName}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.PrimaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.PrimaryTableName}]; " );

            if ( uniqueSecondaryId )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.UniqueSecondaryIdIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.UniqueSecondaryIdIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( OneToManyNub.SecondaryId )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

        #region " CREATE VIEW "

        /// <summary>   Gets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName => $"{this.TableNameThis}View";

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public abstract string PrimaryIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public abstract string SecondaryIdFieldName { get; set; }

        /// <summary>   Creates a View. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The view name. </returns>
        public string CreateView( System.Data.IDbConnection connection, bool dropFirst )
        {
            _ = connection.CreateView( this.ViewName, this.BuildViewSelectQuery(), dropFirst );
            return this.ViewName;
        }

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected virtual string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( OneToManyNub.PrimaryId )}] AS [{this.PrimaryIdFieldName}], 
                [{nameof( OneToManyNub.SecondaryId )}] AS [{this.SecondaryIdFieldName}]
            FROM [{this.TableNameThis}]".Clean();
        }

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="IOneToMany">interface</see>
    /// of entities which are indexed and identified by two keys.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-27 </para>
    /// </remarks>
    public abstract class OneToManyNub : EntityNubBase<IOneToMany>, IOneToMany
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IOneToMany CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IOneToMany value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IOneToMany source, IOneToMany destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.PrimaryId = source.PrimaryId;
            destination.SecondaryId = source.SecondaryId;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        [ExplicitKey]
        public int PrimaryId { get; set; }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        [ExplicitKey]
        public int SecondaryId { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IOneToMany );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IOneToMany other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IOneToMany left, IOneToMany right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.PrimaryId, right.PrimaryId );
                result = result && Equals( left.SecondaryId, right.SecondaryId );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.PrimaryId, this.SecondaryId ).GetHashCode();
        }


        #endregion

    }

    /// <summary>   A dual key selector. </summary>
    /// <remarks>   David, 2020-10-02. </remarks>
    public struct DualKeySelector
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IOneToMany value )
        {
            this.PrimaryId = value.PrimaryId;
            this.SecondaryId = value.SecondaryId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IOneToManyId value )
        {
            this.PrimaryId = value.PrimaryId;
            this.SecondaryId = value.SecondaryId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IOneToManyNatural value )
        {
            this.PrimaryId = value.PrimaryId;
            this.SecondaryId = value.SecondaryId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IOneToManyLabel value )
        {
            this.PrimaryId = value.PrimaryId;
            this.SecondaryId = value.SecondaryId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IOneToManyReal value )
        {
            this.PrimaryId = value.PrimaryId;
            this.SecondaryId = value.SecondaryId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IOneToManyRange value )
        {
            this.PrimaryId = value.PrimaryId;
            this.SecondaryId = value.SecondaryId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IKeyForeign value )
        {
            this.PrimaryId = value.AutoId;
            this.SecondaryId = value.ForeignId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IKeyForeignLabel value )
        {
            this.PrimaryId = value.AutoId;
            this.SecondaryId = value.ForeignId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IKeyForeignLabelNatural value )
        {
            this.PrimaryId = value.AutoId;
            this.SecondaryId = value.ForeignId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IKeyForeignLabelTime value )
        {
            this.PrimaryId = value.AutoId;
            this.SecondaryId = value.ForeignId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IKeyForeignNaturalTime value )
        {
            this.PrimaryId = value.AutoId;
            this.SecondaryId = value.ForeignId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IKeyForeignReal value )
        {
            this.PrimaryId = value.AutoId;
            this.SecondaryId = value.ForeignId;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public DualKeySelector( IKeyForeignTime value )
        {
            this.PrimaryId = value.AutoId;
            this.SecondaryId = value.ForeignId;
        }

        /// <summary>   Gets or sets the id of the primary. </summary>
        /// <value> The identifier of the primary. </value>
        public int PrimaryId { get; set; }

        /// <summary>   Gets or sets the id of the secondary. </summary>
        /// <value> The identifier of the secondary. </value>
        public int SecondaryId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="other">    The dual key selector to compare to this object. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="other" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( DualKeySelector other )
        {
            return this.PrimaryId == other.PrimaryId && this.SecondaryId == other.SecondaryId;
        }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( DualKeySelector ) obj );
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.PrimaryId, this.SecondaryId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( DualKeySelector left, DualKeySelector right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( DualKeySelector left, DualKeySelector right )
        {
            return !(left == right);
        }
    }
}

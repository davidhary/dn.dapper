using System;
using System.Collections.Generic;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;
using isr.Dapper.Entity.EntityExtensions;

namespace isr.Dapper.Entities
{

    /// <summary>   Interface for entities with an explicit key and a Natural(Integer) value. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IExplicitKeyLabelNatural
    {

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [ExplicitKey]
        int Id { get; set; }

        /// <summary>   Gets or sets the label. </summary>
        /// <value> The label. </value>
        string Label { get; set; }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        int Amount { get; set; }
    }

    /// <summary>
    /// A builder for entities with an explicit key, type and a Natural(Integer) value.
    /// </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public abstract class ExplicitKeyLabelNaturalBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Gets the name of the table. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Gets or sets the size of the label field. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <value> The size of the label field. </value>
        public abstract int LabelFieldSize { get; set; }

        /// <summary>   Inserts or ignores the records described by the enumeration type. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="type">         The enumeration type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   An Integer. </returns>
        public int InsertIgnoreNameOrdinalRecords( System.Data.IDbConnection connection, Type type, IEnumerable<int> excluded )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.InsertIgnoreNameOrdinalRecords( this.TableNameThis, typeof( IExplicitKeyLabelNatural ).EnumerateEntityFieldNames(), type, excluded );
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int Upsert( System.Data.IDbConnection connection, IEnumerable<IExplicitKeyLabelNatural> entities )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection.Upsert( this.TableNameThis, entities );
        }

        /// <summary>   Inserts or ignores the records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of inserted or total existing records. </returns>
        public int InsertIgnore( System.Data.IDbConnection connection, IEnumerable<IExplicitKeyLabelNatural> entities )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.InsertIgnore( this.TableNameThis, entities );
        }

        /// <summary>   Gets the name of the label index. </summary>
        /// <value> The name of the label index. </value>
        private string LabelIndexName => $"UQ_{this.TableNameThis}_{nameof( ExplicitKeyLabelNaturalNub.Label )}";

        /// <summary>   Gets the name of the amount index. </summary>
        /// <value> The name of the amount index. </value>
        private string AmountIndexName => $"UQ_{this.TableNameThis}_{nameof( ExplicitKeyLabelNaturalNub.Amount )}";

        /// <summary>   Gets the name of the label amount index. </summary>
        /// <value> The name of the label amount index. </value>
        private string LabelAmountIndexName => $"UQ_{this.TableNameThis}_{nameof( ExplicitKeyLabelNaturalNub.Label )}_{nameof( ExplicitKeyLabelNaturalNub.Amount )}";

        /// <summary>   Options for controlling the unique index. </summary>
        private UniqueIndexOptions? _UniqueIndexOptions;

        /// <summary>   Queries unique index options. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The UniqueIndexOptions. </returns>
        public UniqueIndexOptions QueryUniqueIndexOptions( System.Data.IDbConnection connection )
        {
            if ( !this._UniqueIndexOptions.HasValue )
            {
                this._UniqueIndexOptions = UniqueIndexOptions.None;
                this._UniqueIndexOptions |= (connection.IndexExists( this.LabelAmountIndexName ) ? UniqueIndexOptions.LabelAmount : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.LabelIndexName ) ? UniqueIndexOptions.Label : UniqueIndexOptions.None);
                this._UniqueIndexOptions |= (connection.IndexExists( this.AmountIndexName ) ? UniqueIndexOptions.Amount : UniqueIndexOptions.None);
            }

            return this._UniqueIndexOptions.Value;
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql, uniqueIndexOptions )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite, uniqueIndexOptions ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The new table. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
            [{nameof( ExplicitKeyLabelNaturalNub.Id )}] integer NOT NULL PRIMARY KEY, 
            [{nameof( ExplicitKeyLabelNaturalNub.Label )}] nvarchar({this.LabelFieldSize}) NOT NULL, 
            [{nameof( ExplicitKeyLabelNaturalNub.Amount )}] integer NOT NULL); " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Label) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.LabelIndexName}] ON [{this.TableNameThis}] ([{nameof( ExplicitKeyLabelNaturalNub.Label )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Amount) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.AmountIndexName}] ON [{this.TableNameThis}] ([{nameof( ExplicitKeyLabelNaturalNub.Amount )}]); ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.LabelAmount) )
            {
                _ = queryBuilder.AppendLine( @$"CREATE UNIQUE INDEX IF NOT EXISTS [{this.LabelAmountIndexName}] ON [{this.TableNameThis}] ([{nameof( ExplicitKeyLabelNaturalNub.Label )}], [{nameof( ExplicitKeyLabelNaturalNub.Amount )}]); ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-05-05. </remarks>
        /// <param name="connection">           The connection. </param>
        /// <param name="uniqueIndexOptions">   Options for controlling the unique index. </param>
        /// <returns>   The new table. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection, UniqueIndexOptions uniqueIndexOptions )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( ExplicitKeyLabelNaturalNub.Id )}] [int] NOT NULL,
                [{nameof( ExplicitKeyLabelNaturalNub.Label )}] [nvarchar]({this.LabelFieldSize}) NOT NULL,
                [{nameof( ExplicitKeyLabelNaturalNub.Amount )}] [int] NOT NULL,
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED 
            ([{nameof( ExplicitKeyLabelNaturalNub.Id )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; " );
            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Label) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.LabelIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.LabelIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( ExplicitKeyLabelNaturalNub.Label )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.Amount) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.AmountIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.AmountIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( ExplicitKeyLabelNaturalNub.Amount )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            if ( 0 != ( int ) (uniqueIndexOptions & UniqueIndexOptions.LabelAmount) )
            {
                _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND name = N'{this.LabelAmountIndexName}')
                CREATE UNIQUE NONCLUSTERED INDEX [{this.LabelAmountIndexName}] ON [dbo].[{this.TableNameThis}] ([{nameof( ExplicitKeyLabelNaturalNub.Label )}] ASC, [{nameof( ExplicitKeyLabelNaturalNub.Amount )}] ASC) 
                 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
                 ON [PRIMARY]; ".Clean() );
            }

            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

        #region " CREATE VIEW "

        /// <summary>   Gets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName => $"{this.TableNameThis}View";

        /// <summary>   Gets or sets the name of the automatic identifier field. </summary>
        /// <value> The name of the automatic identifier field. </value>
        public abstract string IdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the label field. </summary>
        /// <value> The name of the label field. </value>
        public abstract string LabelFieldName { get; set; }

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public abstract string AmountFieldName { get; set; }

        /// <summary>   Creates a View. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The view name. </returns>
        public string CreateView( System.Data.IDbConnection connection, bool dropFirst )
        {
            _ = connection.CreateView( this.ViewName, this.BuildViewSelectQuery(), dropFirst );
            return this.ViewName;
        }

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected virtual string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( ExplicitKeyLabelNaturalNub.Id )}] AS [{this.IdFieldName}], 
                [{nameof( ExplicitKeyLabelNaturalNub.Label )}] AS [{this.LabelFieldName}], 
                [{nameof( ExplicitKeyLabelNaturalNub.Amount )}] AS [{this.AmountFieldName}]  
            FROM [{this.TableNameThis}]".Clean();
        }

        #endregion
    }

    /// <summary>
    /// Implements the <see cref="IExplicitKeyLabelNatural">interface</see>
    /// of entities with an explicit key, type and a Natural(Integer) value.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-05-01 </para>
    /// </remarks>
    public abstract class ExplicitKeyLabelNaturalNub : EntityNubBase<IExplicitKeyLabelNatural>, IExplicitKeyLabelNatural
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IExplicitKeyLabelNatural CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IExplicitKeyLabelNatural value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IExplicitKeyLabelNatural source, IExplicitKeyLabelNatural destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Id = source.Id;
            destination.Label = source.Label;
            destination.Amount = source.Amount;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the <see cref="Dapper.Entities.ElementEntity"/>. </summary>
        /// <value> Identifies the <see cref="Dapper.Entities.ElementEntity"/>. </value>
        [ExplicitKey]
        public int Id { get; set; }

        /// <summary>   Gets or sets the label. </summary>
        /// <value> The label. </value>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the natural (whole) number representing an arbitrary or ordinal numeric value.
        /// </summary>
        /// <value> The natural amount. </value>
        public int Amount { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IExplicitKeyLabelNatural );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IExplicitKeyLabelNatural other ) // Implements IEquatable(Of IExplicitKeyLabelNatural).Equals
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IExplicitKeyLabelNatural left, IExplicitKeyLabelNatural right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.Id, right.Id );
                result = result && string.Equals( left.Label, right.Label );
                result = result && Equals( left.Amount, right.Amount );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.Id, this.Label, this.Amount ).GetHashCode();
        }

        #endregion

    }
}

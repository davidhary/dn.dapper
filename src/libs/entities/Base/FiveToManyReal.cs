using System;
using System.Collections.Generic;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;

namespace isr.Dapper.Entities
{

    /// <summary>
    /// Interface for the Five-to-Many+Real-Value nub and Entity. Includes the fields as kept in the
    /// data table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IFiveToManyReal
    {

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        [ExplicitKey]
        int PrimaryId { get; set; }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        [ExplicitKey]
        int SecondaryId { get; set; }

        /// <summary>   Gets or sets the id of the Ternary reference. </summary>
        /// <value> The identifier of Ternary reference. </value>
        [ExplicitKey]
        int TernaryId { get; set; }

        /// <summary>   Gets or sets the id of the Quaternary reference. </summary>
        /// <value> The identifier of Quaternary reference. </value>
        [ExplicitKey]
        int QuaternaryId { get; set; }

        /// <summary>   Gets or sets the id of the Quinary reference. </summary>
        /// <value> The identifier of Quinary reference. </value>
        [ExplicitKey]
        int QuinaryId { get; set; }

        /// <summary>   Gets or sets the amount. </summary>
        /// <value> The amount. </value>
        double Amount { get; set; }
    }

    /// <summary>
    /// A builder for entities which have Five-to-Many relationship and a Real (double) value.
    /// </summary>
    /// <remarks>   David, 2020-04-24. </remarks>
    public abstract class FiveToManyRealBuilder
    {

        #region " TABLE BUILDER "

        /// <summary>   Gets the name of the table. </summary>
        /// <value> The table name this. </value>
        protected abstract string TableNameThis { get; }

        /// <summary>   Gets or sets the name of the primary table. </summary>
        /// <value> The name of the primary table. </value>
        public abstract string PrimaryTableName { get; set; }

        /// <summary>   Gets or sets the name of the primary table key. </summary>
        /// <value> The name of the primary table key. </value>
        public abstract string PrimaryTableKeyName { get; set; }

        /// <summary>   Gets or sets the name of the secondary table. </summary>
        /// <value> The name of the secondary table. </value>
        public abstract string SecondaryTableName { get; set; }

        /// <summary>   Gets or sets the name of the secondary table key. </summary>
        /// <value> The name of the secondary table key. </value>
        public abstract string SecondaryTableKeyName { get; set; }

        /// <summary>   Gets or sets the name of the Ternary table. </summary>
        /// <value> The name of the Ternary table. </value>
        public abstract string TernaryTableName { get; set; }

        /// <summary>   Gets or sets the name of the Ternary table key. </summary>
        /// <value> The name of the Ternary table key. </value>
        public abstract string TernaryTableKeyName { get; set; }

        /// <summary>   Gets or sets the name of the Quaternary table. </summary>
        /// <value> The name of the Quaternary table. </value>
        public abstract string QuaternaryTableName { get; set; }

        /// <summary>   Gets or sets the name of the Quaternary table key. </summary>
        /// <value> The name of the Quaternary table key. </value>
        public abstract string QuaternaryTableKeyName { get; set; }

        /// <summary>   Gets or sets the name of the Quinary table. </summary>
        /// <value> The name of the Quinary table. </value>
        public abstract string QuinaryTableName { get; set; }

        /// <summary>   Gets or sets the name of the Quinary table key. </summary>
        /// <value> The name of the Quinary table key. </value>
        public abstract string QuinaryTableKeyName { get; set; }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   An Integer. </returns>
        public int Upsert( System.Data.IDbConnection connection, IEnumerable<IFiveToManyReal> entities )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection.Upsert( this.TableNameThis, entities );
        }

        /// <summary>   Creates a table. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        public string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? this.CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? this.CreateTable( sqlite ) : string.Empty;
        }

        /// <summary>   Creates table for SQLite database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{this.TableNameThis}] (
                [{nameof( FiveToManyRealNub.PrimaryId )}] integer NOT NULL, 
                [{nameof( FiveToManyRealNub.SecondaryId )}] integer NOT NULL,
                [{nameof( FiveToManyRealNub.TernaryId )}] integer NOT NULL,
                [{nameof( FiveToManyRealNub.QuaternaryId )}] integer NOT NULL,
                [{nameof( FiveToManyRealNub.QuinaryId )}] integer NOT NULL,
            [{nameof( FiveToManyRealNub.Amount )}] float NOT NULL, 
            CONSTRAINT [sqlite_autoindex_{this.TableNameThis}_1] PRIMARY KEY ([{nameof( FiveToManyRealNub.PrimaryId )}], [{nameof( FiveToManyRealNub.SecondaryId )}], [{nameof( FiveToManyRealNub.TernaryId )}], [{nameof( FiveToManyRealNub.QuaternaryId )}], [{nameof( FiveToManyRealNub.QuinaryId )}]),
            FOREIGN KEY ([{nameof( FiveToManyRealNub.PrimaryId )}]) REFERENCES [{this.PrimaryTableName}] ([{this.PrimaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( FiveToManyRealNub.SecondaryId )}]) REFERENCES [{this.SecondaryTableName}] ([{this.SecondaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( FiveToManyRealNub.TernaryId )}]) REFERENCES [{this.TernaryTableName}] ([{this.TernaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( FiveToManyRealNub.QuaternaryId )}]) REFERENCES [{this.QuaternaryTableName}] ([{this.QuaternaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( FiveToManyRealNub.QuinaryId )}]) REFERENCES [{this.QuinaryTableName}] ([{this.QuinaryTableKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        /// <summary>   Creates table for SQL Server database. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <returns>   The table name or empty. </returns>
        private string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{this.TableNameThis}](
                [{nameof( FiveToManyRealNub.PrimaryId )}] [int] NOT NULL,
                [{nameof( FiveToManyRealNub.SecondaryId )}] [int] NOT NULL,
                [{nameof( FiveToManyRealNub.TernaryId )}] [int] NOT NULL,
                [{nameof( FiveToManyRealNub.QuaternaryId )}] [int] NOT NULL,
                [{nameof( FiveToManyRealNub.QuinaryId )}] [int] NOT NULL,
                [{nameof( FiveToManyRealNub.Amount )}] [float] NOT NULL,
             CONSTRAINT [PK_{this.TableNameThis}] PRIMARY KEY CLUSTERED
            ([{nameof( FiveToManyRealNub.PrimaryId )}] ASC, [{nameof( FiveToManyRealNub.SecondaryId )}] ASC, [{nameof( FiveToManyRealNub.TernaryId )}] ASC, [{nameof( FiveToManyRealNub.QuaternaryId )}] ASC, [{nameof( FiveToManyRealNub.QuinaryId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END;
            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.QuinaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.QuinaryTableName}] FOREIGN KEY([{nameof( FiveToManyRealNub.QuinaryId )}])
            REFERENCES [dbo].[{this.QuinaryTableName}] ([{this.QuinaryTableKeyName}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.QuinaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.QuinaryTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.QuaternaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.QuaternaryTableName}] FOREIGN KEY([{nameof( FourToManyRealNub.QuaternaryId )}])
            REFERENCES [dbo].[{this.QuaternaryTableName}] ([{this.QuaternaryTableKeyName}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.QuaternaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.QuaternaryTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.TernaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.TernaryTableName}] FOREIGN KEY([{nameof( FiveToManyRealNub.TernaryId )}])
            REFERENCES [dbo].[{this.TernaryTableName}] ([{this.TernaryTableKeyName}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.TernaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.TernaryTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.SecondaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.SecondaryTableName}] FOREIGN KEY([{nameof( FiveToManyRealNub.SecondaryId )}])
            REFERENCES [dbo].[{this.SecondaryTableName}] ([{this.SecondaryTableKeyName}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.SecondaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.SecondaryTableName}];

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.PrimaryTableName}') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}]  WITH CHECK ADD  CONSTRAINT [FK_{this.TableNameThis}_{this.PrimaryTableName}] FOREIGN KEY([{nameof( FiveToManyRealNub.PrimaryId )}])
            REFERENCES [dbo].[{this.PrimaryTableName}] ([{this.PrimaryTableKeyName}])
            ON UPDATE CASCADE ON DELETE CASCADE; 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{this.TableNameThis}_{this.PrimaryTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{this.TableNameThis}]'))
            ALTER TABLE [dbo].[{this.TableNameThis}] CHECK CONSTRAINT [FK_{this.TableNameThis}_{this.PrimaryTableName}]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return this.TableNameThis;
        }

        #endregion

        #region " CREATE VIEW "

        /// <summary>   Gets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName => $"{this.TableNameThis}View";

        /// <summary>   Gets or sets the name of the primary identifier field. </summary>
        /// <value> The name of the primary identifier field. </value>
        public abstract string PrimaryIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the secondary identifier field. </summary>
        /// <value> The name of the secondary identifier field. </value>
        public abstract string SecondaryIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the ternary identifier field. </summary>
        /// <value> The name of the ternary identifier field. </value>
        public abstract string TernaryIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the quaternary identifier field. </summary>
        /// <value> The name of the quaternary identifier field. </value>
        public abstract string QuaternaryIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the quinary identifier field. </summary>
        /// <value> The name of the quinary identifier field. </value>
        public abstract string QuinaryIdFieldName { get; set; }

        /// <summary>   Gets or sets the name of the amount field. </summary>
        /// <value> The name of the amount field. </value>
        public abstract string AmountFieldName { get; set; }

        /// <summary>   Creates a View. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The view name. </returns>
        public string CreateView( System.Data.IDbConnection connection, bool dropFirst )
        {
            _ = connection.CreateView( this.ViewName, this.BuildViewSelectQuery(), dropFirst );
            return this.ViewName;
        }

        /// <summary>   Builds view select query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <returns>   The View Select Query. </returns>
        protected virtual string BuildViewSelectQuery()
        {
            return @$"
            SELECT 
                [{nameof( FiveToManyRealNub.PrimaryId )}] AS [{this.PrimaryIdFieldName}], 
                [{nameof( FiveToManyRealNub.SecondaryId )}] AS [{this.SecondaryIdFieldName}],
                [{nameof( FiveToManyRealNub.TernaryId )}] AS [{this.TernaryIdFieldName}],
                [{nameof( FiveToManyRealNub.QuaternaryId )}] AS [{this.QuaternaryIdFieldName}],
                [{nameof( FiveToManyRealNub.QuinaryId )}] AS [{this.QuinaryIdFieldName}],
                [{nameof( FiveToManyRealNub.Amount )}] AS [{this.AmountFieldName}]
            FROM [{this.TableNameThis}]".Clean();
        }

        #endregion

    }

    /// <summary>
    /// Implements the <see cref="IFiveToManyReal">interface</see>
    /// of entities which are indexed and identified by five keys for selecting a Real (double) value
    /// amount.
    /// </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-27 </para>
    /// </remarks>
    public abstract class FiveToManyRealNub : Entity.EntityNubBase<IFiveToManyReal>, IFiveToManyReal
    {

        #region " I TYPED CLONABLE "

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public override IFiveToManyReal CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public override void CopyFrom( IFiveToManyReal value )
        {
            Copy( value, this );
        }

        /// <summary>   Copies the given value. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="source">       Another instance to copy. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void Copy( IFiveToManyReal source, IFiveToManyReal destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.PrimaryId = source.PrimaryId;
            destination.SecondaryId = source.SecondaryId;
            destination.TernaryId = source.TernaryId;
            destination.QuaternaryId = source.QuaternaryId;
            destination.QuinaryId = source.QuinaryId;
            destination.Amount = source.Amount;
        }

        #endregion

        #region " FIELDS "

        /// <summary>   Gets or sets the id of the primary reference. </summary>
        /// <value> Identifies the primary reference. </value>
        [ExplicitKey]
        public int PrimaryId { get; set; }

        /// <summary>   Gets or sets the id of the Secondary reference. </summary>
        /// <value> The identifier of Secondary reference. </value>
        [ExplicitKey]
        public int SecondaryId { get; set; }

        /// <summary>   Gets or sets the id of the Ternary reference. </summary>
        /// <value> The identifier of Ternary reference. </value>
        [ExplicitKey]
        public int TernaryId { get; set; }

        /// <summary>   Gets or sets the id of the Quaternary reference. </summary>
        /// <value> The identifier of Quaternary reference. </value>
        [ExplicitKey]
        public int QuaternaryId { get; set; }

        /// <summary>   Gets or sets the id of the Quinary reference. </summary>
        /// <value> The identifier of Quinary reference. </value>
        [ExplicitKey]
        public int QuinaryId { get; set; }

        /// <summary>   Gets or sets the amount. </summary>
        /// <value> The amount. </value>
        public double Amount { get; set; }

        #endregion

        #region " EQUALS "

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IFiveToManyReal );
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IFiveToManyReal other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary>   Determines if entities are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IFiveToManyReal left, IFiveToManyReal right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.PrimaryId, right.PrimaryId );
                result = result && Equals( left.SecondaryId, right.SecondaryId );
                result = result && Equals( left.TernaryId, right.TernaryId );
                result = result && Equals( left.QuaternaryId, right.QuaternaryId );
                result = result && Equals( left.QuinaryId, right.QuinaryId );
                result = result && Equals( left.Amount, right.Amount );
                return result;
            }
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.PrimaryId, this.SecondaryId, this.TernaryId, this.QuaternaryId, this.QuinaryId, this.Amount ).GetHashCode();
        }

        #endregion

    }

    /// <summary>   A five key selector. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public struct FiveKeySelector
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public FiveKeySelector( IFiveToManyReal value )
        {
            this.PrimaryId = value.PrimaryId;
            this.SecondaryId = value.SecondaryId;
            this.TernaryId = value.TernaryId;
            this.QuaternaryId = value.QuaternaryId;
            this.QuinaryId = value.QuinaryId;
        }

        /// <summary>   Gets or sets the id of the primary. </summary>
        /// <value> The identifier of the primary. </value>
        public int PrimaryId { get; set; }

        /// <summary>   Gets or sets the id of the secondary. </summary>
        /// <value> The identifier of the secondary. </value>
        public int SecondaryId { get; set; }

        /// <summary>   Gets or sets the id of the ternary. </summary>
        /// <value> The identifier of the ternary. </value>
        public int TernaryId { get; set; }

        /// <summary>   Gets or sets the id of the quaternary. </summary>
        /// <value> The identifier of the quaternary. </value>
        public int QuaternaryId { get; set; }

        /// <summary>   Gets or sets the id of the quinary. </summary>
        /// <value> The identifier of the quinary. </value>
        public int QuinaryId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="other">    The five key selector to compare to this object. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="other" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( FiveKeySelector other )
        {
            return this.PrimaryId == other.PrimaryId && this.SecondaryId == other.SecondaryId 
                   && this.TernaryId == other.TernaryId && this.QuaternaryId == other.QuaternaryId
                   && this.QuinaryId == other.QuinaryId;
        }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( FiveKeySelector ) obj );
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.PrimaryId, this.SecondaryId, this.TernaryId, this.QuaternaryId, this.QuinaryId ).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( FiveKeySelector left, FiveKeySelector right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( FiveKeySelector left, FiveKeySelector right )
        {
            return !(left == right);
        }
    }

    /// <summary>   A five four key selector. </summary>
    /// <remarks>   David, 2020-05-19. </remarks>
    public struct FiveFourKeySelector
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The value. </param>
        public FiveFourKeySelector( IFiveToManyReal value )
        {
            this.SecondaryId = value.SecondaryId;
            this.TernaryId = value.TernaryId;
            this.QuaternaryId = value.QuaternaryId;
            this.QuinaryId = value.QuinaryId;
        }

        /// <summary>   Gets or sets the id of the secondary. </summary>
        /// <value> The identifier of the secondary. </value>
        public int SecondaryId { get; set; }

        /// <summary>   Gets or sets the id of the ternary. </summary>
        /// <value> The identifier of the ternary. </value>
        public int TernaryId { get; set; }

        /// <summary>   Gets or sets the id of the quaternary. </summary>
        /// <value> The identifier of the quaternary. </value>
        public int QuaternaryId { get; set; }

        /// <summary>   Gets or sets the id of the quinary. </summary>
        /// <value> The identifier of the quinary. </value>
        public int QuinaryId { get; set; }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="other">    The five four key selector to compare to this object. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="other" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( FiveFourKeySelector other )
        {
            return this.SecondaryId == other.SecondaryId && this.TernaryId == other.TernaryId
			       && this.QuaternaryId == other.QuaternaryId && this.QuinaryId == other.QuinaryId;
        }

        /// <summary>   Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
        /// represent the same value; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( ( FiveFourKeySelector ) obj );
        }

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            return ( this.SecondaryId, this.TernaryId, this.QuaternaryId, this.QuinaryId).GetHashCode();
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( FiveFourKeySelector left, FiveFourKeySelector right )
        {
            return left.Equals( right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( FiveFourKeySelector left, FiveFourKeySelector right )
        {
            return !(left == right);
        }
    }
}

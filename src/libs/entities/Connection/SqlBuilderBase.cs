using System;
using System.Collections.Generic;

using Dapper;

namespace isr.Dapper.Entities
{

    /// <summary>   A SQL builder base. </summary>
    /// <remarks>
    /// David, 2020-05-26. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public abstract class SqlBuilderBase
    {

        /// <summary>   Selects a builder. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="connection">   The data source connection. </param>
        /// <returns>   A SqlBuilderBase. </returns>
        public static SqlBuilderBase SelectBuilder( System.Data.IDbConnection connection )
        {
            if ( connection is not TransactedConnection transactedConnection )
            {
                if ( connection as System.Data.SQLite.SQLiteConnection is object )
                {
                    return SqliteSqlBuilder.Instance;
                }
                else
                {
                    // System.Data.SqlClient.SqlConnection = TryCast(connection, System.Data.SqlClient.SqlConnection)
                    return SqlServerSqlBuilder.Instance;
                }
            }
            else
            {
                return SelectBuilder( transactedConnection.Connection );
            }
        }

        /// <summary>   Converts a value to an universal time format. </summary>
        /// <remarks>   David, 2020-05-07. </remarks>
        /// <param name="universalTimestamp">   The Universal (UTC) timestamp. </param>
        /// <returns>
        /// Time value in the <see cref="isr.Dapper.Entity.ProviderBase.DefaultDateTimeFormat"/> format.
        /// </returns>
        public static string ToTimestampFormat( DateTime universalTimestamp )
        {
            return Entity.ProviderBase.ToDefaultTimestampFormat( universalTimestamp );
        }

        /// <summary>   Build a create view query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <param name="viewName">     Name of the view. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The built query. </returns>
        public abstract string CreateView( string viewName, string selectQuery, bool dropFirst );

        /// <summary>   Inserts or updates <see cref="IFiveToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFiveToManyNatural"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IFiveToManyNatural> entities );

        /// <summary>   inserts or ignores <see cref="IFiveToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFiveToManyNatural"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IFiveToManyNatural> entities );

        /// <summary>   Inserts or updates <see cref="IFiveToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFiveToManyReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IFiveToManyReal> entities );

        /// <summary>   inserts or ignores <see cref="IFiveToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFiveToManyReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IFiveToManyReal> entities );

        /// <summary>   Inserts or updates <see cref="IFourToMany"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToMany"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IFourToMany> entities );

        /// <summary>   inserts or ignores <see cref="IFourToMany"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToMany"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IFourToMany> entities );

        /// <summary>   Inserts or updates <see cref="IFourToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToManyId"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IFourToManyId> entities );

        /// <summary>   inserts or ignores <see cref="IFourToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToManyId"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IFourToManyId> entities );

        /// <summary>   Inserts or updates <see cref="IFourToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToManyNatural"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IFourToManyNatural> entities );

        /// <summary>   inserts or ignores <see cref="IFourToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToManyNatural"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IFourToManyNatural> entities );

        /// <summary>   Inserts or updates <see cref="IFourToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToManyReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IFourToManyReal> entities );

        /// <summary>   inserts or ignores <see cref="IFourToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToManyReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IFourToManyReal> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<ITwoToMany> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<ITwoToMany> entities );

        /// <summary>   Inserts or updates <see cref="ITwoToManyLabel"/> records. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="ITwoToManyLabel"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<ITwoToManyLabel> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<ITwoToManyLabel> entities );

        /// <summary>   Inserts or updates <see cref="ITwoToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="ITwoToManyReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<ITwoToManyReal> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<ITwoToManyReal> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IThreeToMany> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IThreeToMany> entities );

        /// <summary>   Inserts or updates <see cref="IThreeToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IThreeToManyId"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IThreeToManyId> entities );

        /// <summary>   inserts or ignores <see cref="IThreeToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IThreeToManyReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IThreeToManyId> entities );

        /// <summary>   Inserts or updates <see cref="IThreeToManyLabel"/> records. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IThreeToManyLabel"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IThreeToManyLabel> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IThreeToManyLabel> entities );

        /// <summary>   Inserts or updates <see cref="IThreeToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IThreeToManyReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IThreeToManyReal> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IThreeToManyReal> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IExplicitKeyLabelNatural> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IExplicitKeyLabelNatural> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IExplicitKeyForeignLabel> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IExplicitKeyForeignLabel> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IExplicitKeyForeignNatural> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IExplicitKeyForeignNatural> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyForeign> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyForeign> entities );

        /// <summary>   Inserts or updates <see cref="IKeyForeignLabelNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. Present implementation assumes a unique index. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeignLabelNatural"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyForeignLabelNatural> entities );

        /// <summary>   inserts or ignores <see cref="IKeyForeignLabelNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeignLabelNatural"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyForeignLabelNatural> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyForeignTime> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyForeignTime> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyForeignReal> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyForeignReal> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyTwoForeignReal> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyTwoForeignReal> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyTwoForeignTime> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyTwoForeignTime> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyLabelTime> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyLabelTime> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyLabelTimezone> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyLabelTimezone> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyNaturalTime> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyNaturalTime> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyTime> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyTime> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyForeignLabel> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyForeignLabel> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyForeignLabelTime> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyForeignLabelTime> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IKeyForeignNaturalTime> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IKeyForeignNaturalTime> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<INominal> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<INominal> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IOneToManyId> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IOneToManyId> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IOneToManyNatural> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IOneToManyNatural> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IOneToManyRange> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IOneToManyRange> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IOneToManyReal> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IOneToManyReal> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IOneToManyLabel> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IOneToManyLabel> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IOneToMany> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IOneToMany> entities );

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string Upsert( string tableName, IEnumerable<IOneToOne> entities );

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnore( string tableName, IEnumerable<IOneToOne> entities );

        /// <summary>   Inserts or updates a name description records. </summary>
        /// <remarks>   David, 2020-05-11. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string UpsertNameDescriptionRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded );

        /// <summary>   Inserts or ignores name description records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnoreNameDescriptionRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded );

        /// <summary>   Inserts or replaces name description records. </summary>
        /// <remarks>
        /// Insert or replace failed on some tables (e.g., Measurement) due to a foreign key constraint
        /// while not failing on other tables (e.g., Polarity and Bin). At this point (2/2020) the
        /// difference between these tables remains unclear.
        /// </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of inserted or replaced records. </returns>
        public abstract string RepsertNameDescriptionRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded );

        /// <summary>   Inserts or updates name ordinal records. </summary>
        /// <remarks>   David, 2020-05-06. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string UpsertNameOrdinalRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded );

        /// <summary>   Inserts or ignores a name ordinal records. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnoreNameOrdinalRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded );

        /// <summary>   Inserts or updates name records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of inserted or updated records. </returns>
        public abstract string UpsertNameRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded );

        /// <summary>   Inserts or ignores a name records. </summary>
        /// <remarks>   David, 2020-05-16. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public abstract string InsertIgnoreNameRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded );

        /// <summary>   Inserts or replaces name records. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of inserted or replaced records. </returns>
        public abstract string InsertReplaceNameRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded );
    }
}

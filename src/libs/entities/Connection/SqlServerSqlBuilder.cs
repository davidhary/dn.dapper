using System;
using System.Collections.Generic;
using System.Linq;

using FastEnums;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entity.EntityExtensions;

namespace isr.Dapper.Entities
{

    /// <summary>   A SQL server SQL builder. </summary>
    /// <remarks>
    /// David, 2020-05-26. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class SqlServerSqlBuilder : SqlBuilderBase
    {

        private static readonly Lazy<SqlServerSqlBuilder> LazyBuilder = new();

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static SqlServerSqlBuilder Instance => LazyBuilder.Value;

        /// <summary>   Build a create view query. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <param name="viewName">     Name of the view. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The built query. </returns>
        public override string CreateView( string viewName, string selectQuery, bool dropFirst )
        {
            var queryBuilder = new System.Text.StringBuilder();
            if ( dropFirst )
            {
                _ = queryBuilder.AppendLine( $"DROP VIEW IF EXISTS [{viewName}]; " );
            }

            _ = queryBuilder.AppendLine( @$"IF NOT EXISTS (SELECT * FROM sys.views WHERE name = '{viewName}')
BEGIN
    EXECUTE('CREATE VIEW [{viewName}] AS" );
            _ = queryBuilder.AppendLine( selectQuery );
            _ = queryBuilder.Append( @"')
END; ".Clean() );
            return queryBuilder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IFiveToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IFiveToManyNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFiveToManyNatural ).EnumerateEntityFieldNames();
            foreach ( IFiveToManyNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId} AND [{fieldNames.ElementAtOrDefault( 4 )}] = {entity.QuinaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 5 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 4 )}] = {entity.QuaternaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuinaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IFiveToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IFiveToManyNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFiveToManyNatural ).EnumerateEntityFieldNames();
            foreach ( IFiveToManyNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId} AND [{fieldNames.ElementAtOrDefault( 4 )}] = {entity.QuinaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IFiveToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IFiveToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFiveToManyReal ).EnumerateEntityFieldNames();
            foreach ( IFiveToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId} AND [{fieldNames.ElementAtOrDefault( 4 )}] = {entity.QuinaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 5 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 4 )}] = {entity.QuaternaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuinaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IFiveToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IFiveToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFiveToManyReal ).EnumerateEntityFieldNames();
            foreach ( IFiveToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId} AND [{fieldNames.ElementAtOrDefault( 4 )}] = {entity.QuinaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IThreeToMany> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IThreeToMany ).EnumerateEntityFieldNames();
            foreach ( IThreeToMany entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId}))  " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignore records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IThreeToMany> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IThreeToMany ).EnumerateEntityFieldNames();
            foreach ( IThreeToMany entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId}))  " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IFourToMany"/> records. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IFourToMany> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFourToMany ).EnumerateEntityFieldNames();
            foreach ( IFourToMany entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId} AND [{fieldNames.ElementAtOrDefault( 4 )}] = {entity.QuinaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IFourToMany"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IFourToMany> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFourToMany ).EnumerateEntityFieldNames();
            foreach ( IFourToMany entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId} AND [{fieldNames.ElementAtOrDefault( 4 )}] = {entity.QuinaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.QuinaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IFourToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IFourToManyId> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFourToManyId ).EnumerateEntityFieldNames();
            foreach ( IFourToManyId entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 4 )} = {entity.ForeignId} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId}  AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.ForeignId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IFourToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IFourToManyId> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFourToManyId ).EnumerateEntityFieldNames();
            foreach ( IFourToManyId entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.ForeignId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IFourToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IFourToManyNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFourToManyNatural ).EnumerateEntityFieldNames();
            foreach ( IFourToManyNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 4 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId}  AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IFourToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IFourToManyNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFourToManyNatural ).EnumerateEntityFieldNames();
            foreach ( IFourToManyNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IFourToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-20. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IFourToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFourToManyReal ).EnumerateEntityFieldNames();
            foreach ( IFourToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 4 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId}  AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IFourToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IFourToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IFourToManyReal ).EnumerateEntityFieldNames();
            foreach ( IFourToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId} AND [{fieldNames.ElementAtOrDefault( 3 )}] = {entity.QuaternaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.QuaternaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<ITwoToMany> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( ITwoToMany ).EnumerateEntityFieldNames();
            foreach ( ITwoToMany entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId}))  " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignore records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<ITwoToMany> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( ITwoToMany ).EnumerateEntityFieldNames();
            foreach ( ITwoToMany entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId}))  " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<ITwoToManyLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( ITwoToManyLabel ).EnumerateEntityFieldNames();
            foreach ( ITwoToManyLabel entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}' " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},'{entity.Label}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<ITwoToManyLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( ITwoToManyLabel ).EnumerateEntityFieldNames();
            foreach ( ITwoToManyLabel entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},'{entity.Label}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<ITwoToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( ITwoToManyReal ).EnumerateEntityFieldNames();
            foreach ( ITwoToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<ITwoToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( ITwoToManyReal ).EnumerateEntityFieldNames();
            foreach ( ITwoToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IThreeToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IThreeToManyId> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IThreeToManyId ).EnumerateEntityFieldNames();
            foreach ( IThreeToManyId entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 3 )} = {entity.ForeignId} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.ForeignId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IThreeToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IThreeToManyId> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IThreeToManyId ).EnumerateEntityFieldNames();
            foreach ( IThreeToManyId entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.ForeignId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IThreeToManyLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IThreeToManyLabel ).EnumerateEntityFieldNames();
            foreach ( IThreeToManyLabel entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 3 )} = '{entity.Label}' " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},'{entity.Label}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IThreeToManyLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IThreeToManyLabel ).EnumerateEntityFieldNames();
            foreach ( IThreeToManyLabel entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},'{entity.Label}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IThreeToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IThreeToManyReal ).EnumerateEntityFieldNames();
            foreach ( IThreeToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 3 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IThreeToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IThreeToManyReal ).EnumerateEntityFieldNames();
            foreach ( IThreeToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.TernaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.TernaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IExplicitKeyLabelNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IExplicitKeyLabelNatural ).EnumerateEntityFieldNames();
            foreach ( IExplicitKeyLabelNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id}) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = '{entity.Label}', {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id} " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.Id}, '{entity.Label}', {entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IExplicitKeyLabelNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IExplicitKeyLabelNatural ).EnumerateEntityFieldNames();
            foreach ( IExplicitKeyLabelNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id}) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.Id}, '{entity.Label}', {entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IExplicitKeyForeignLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IExplicitKeyForeignLabel ).EnumerateEntityFieldNames();
            foreach ( IExplicitKeyForeignLabel entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id}) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}' " );
                _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id} " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.Id}, {entity.ForeignId}, '{entity.Label}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IExplicitKeyForeignLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IExplicitKeyForeignLabel ).EnumerateEntityFieldNames();
            foreach ( IExplicitKeyForeignLabel entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id}) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.Id}, {entity.ForeignId}, '{entity.Label}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IExplicitKeyForeignNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IExplicitKeyForeignNatural ).EnumerateEntityFieldNames();
            foreach ( IExplicitKeyForeignNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id}) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id} " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.Id}, {entity.ForeignId}, {entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IExplicitKeyForeignNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IExplicitKeyForeignNatural ).EnumerateEntityFieldNames();
            foreach ( IExplicitKeyForeignNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id}) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.Id}, {entity.ForeignId}, {entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyForeign> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeign ).EnumerateEntityFieldNames();
            foreach ( IKeyForeign entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )}) VALUES ({entity.ForeignId}); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId} " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}; " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )}) VALUES ({entity.ForeignId}); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignore records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyForeign> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeign ).EnumerateEntityFieldNames();
            foreach ( IKeyForeign entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = {entity.ForeignId})) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )}) VALUES ({entity.ForeignId}); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )}) VALUES ({entity.ForeignId}); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IKeyForeignLabelNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. Present implementation assumes a unique index. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeignLabelNatural"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyForeignLabelNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignLabelNatural ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignLabelNatural entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}') " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 3 )} = {entity.Amount} " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}') " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, '{entity.Label}', {entity.Amount}); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}', {fieldNames.ElementAtOrDefault( 3 )} = {entity.Amount} " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, '{entity.Label}', {entity.Amount}); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IKeyForeignLabelNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. Present implementation assumes a unique index. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeignLabelNatural"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyForeignLabelNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignLabelNatural ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignLabelNatural entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = {entity.ForeignId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = '{entity.Label}')) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, '{entity.Label}', {entity.Amount}); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, '{entity.Label}', {entity.Amount}); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IKeyForeignTime"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeignTime"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyForeignTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignTime ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}) " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IKeyForeignTime"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeignTime"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyForeignTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignTime ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = {entity.ForeignId})) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IKeyForeignReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeignReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyForeignReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignReal ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignReal entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount}' " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}) " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId},{entity.Amount}); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount} " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId},{entity.Amount}); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IKeyForeignReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeignReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyForeignReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignReal ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignReal entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId},{entity.Amount}); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId}{entity.Amount}); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IKeyTwoForeignReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyTwoForeignReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyTwoForeignReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyTwoForeignReal ).EnumerateEntityFieldNames();
            foreach ( IKeyTwoForeignReal entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.FirstForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = {entity.SecondForeignId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 3 )} = {entity.Amount}' " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {fieldNames.ElementAtOrDefault( 1 )} = {entity.FirstForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = {entity.SecondForeignId}) " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},{entity.Amount}); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.FirstForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = {entity.SecondForeignId}, {fieldNames.ElementAtOrDefault( 3 )} = {entity.Amount} " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},{entity.Amount}); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IKeyTwoForeignReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyTwoForeignReal"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyTwoForeignReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyTwoForeignReal ).EnumerateEntityFieldNames();
            foreach ( IKeyTwoForeignReal entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.FirstForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = {entity.SecondForeignId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},{entity.Amount}); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},{entity.Amount}); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates <see cref="IKeyTwoForeignTime"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyTwoForeignTime"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyTwoForeignTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyTwoForeignTime ).EnumerateEntityFieldNames();
            foreach ( IKeyTwoForeignTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.FirstForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = {entity.SecondForeignId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 3 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {fieldNames.ElementAtOrDefault( 1 )} = {entity.FirstForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = {entity.SecondForeignId}) " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},'{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.FirstForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = {entity.SecondForeignId}, {fieldNames.ElementAtOrDefault( 3 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},'{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores <see cref="IKeyTwoForeignTime"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyTwoForeignTime"/> entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyTwoForeignTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyTwoForeignTime ).EnumerateEntityFieldNames();
            foreach ( IKeyTwoForeignTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.FirstForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = {entity.SecondForeignId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},'{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.FirstForeignId},{entity.SecondForeignId},'{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyLabelTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyLabelTime ).EnumerateEntityFieldNames();
            foreach ( IKeyLabelTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = '{entity.Label}') " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 3 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = '{entity.Label}') " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ('{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = '{entity.Label}', {fieldNames.ElementAtOrDefault( 2 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )})  VALUES ('{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyLabelTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyLabelTime ).EnumerateEntityFieldNames();
            foreach ( IKeyLabelTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = '{entity.Label}')) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ('{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ('{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyLabelTimezone> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyLabelTimezone ).EnumerateEntityFieldNames();
            foreach ( IKeyLabelTimezone entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = '{entity.Label}') " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 3 )} = '{ToTimestampFormat( entity.Timestamp )}', '{entity.TimezoneId}'" );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = '{entity.Label}') " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ('{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}', '{entity.TimezoneId}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = '{entity.Label}', {fieldNames.ElementAtOrDefault( 2 )} = '{ToTimestampFormat( entity.Timestamp )}', '{entity.TimezoneId}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ('{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}', '{entity.TimezoneId}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyLabelTimezone> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyLabelTimezone ).EnumerateEntityFieldNames();
            foreach ( IKeyLabelTimezone entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = '{entity.Label}')) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ('{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}', '{entity.TimezoneId}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ('{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}', '{entity.TimezoneId}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyNaturalTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyNaturalTime ).EnumerateEntityFieldNames();
            foreach ( IKeyNaturalTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.Amount}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.Amount}) " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.Amount}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.Amount}, {fieldNames.ElementAtOrDefault( 2 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.Amount}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyNaturalTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyNaturalTime ).EnumerateEntityFieldNames();
            foreach ( IKeyNaturalTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = {entity.Amount})) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.Amount}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.Amount}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyTime ).EnumerateEntityFieldNames();
            foreach ( IKeyTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )}) VALUES ('{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )}) VALUES ('{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyTime ).EnumerateEntityFieldNames();
            foreach ( IKeyTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = {entity.Timestamp})) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )}) VALUES ('{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )}) VALUES ('{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyForeignLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignLabel ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignLabel entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}' " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}) " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId}, '{entity.Label}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId}, '{entity.Label}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyForeignLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignLabel ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignLabel entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = {entity.ForeignId})) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId}, '{entity.Label}'; " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )}) VALUES ({entity.ForeignId}, '{entity.Label}'; " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyForeignLabelTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignLabelTime ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignLabelTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}') " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}', {fieldNames.ElementAtOrDefault( 3 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}') " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, '{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}', {fieldNames.ElementAtOrDefault( 3 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, '{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyForeignLabelTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignLabelTime ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignLabelTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = {entity.ForeignId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = '{entity.Label}')) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, '{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, '{entity.Label}', '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IKeyForeignNaturalTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignNaturalTime ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignNaturalTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    // assuming auto id begins at 1, an auto id of 0 means not assigned.
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount}, {fieldNames.ElementAtOrDefault( 3 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE ({fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId} AND {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount}) " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, {entity.Amount}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.ForeignId}, {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount}, {fieldNames.ElementAtOrDefault( 3 )} = '{ToTimestampFormat( entity.Timestamp )}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, {entity.Amount}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IKeyForeignNaturalTime> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IKeyForeignNaturalTime ).EnumerateEntityFieldNames();
            foreach ( IKeyForeignNaturalTime entity in entities.ToArray() )
            {
                if ( entity.AutoId == 0 )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 1 )}] = {entity.ForeignId} AND [{fieldNames.ElementAtOrDefault( 2 )}] = {entity.Amount}  )) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, {entity.Amount}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
                else
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.AutoId}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] ({fieldNames.ElementAtOrDefault( 1 )},{fieldNames.ElementAtOrDefault( 2 )},{fieldNames.ElementAtOrDefault( 3 )}) VALUES ({entity.ForeignId}, {entity.Amount}, '{ToTimestampFormat( entity.Timestamp )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<INominal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( INominal ).EnumerateEntityFieldNames();
            foreach ( INominal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id}) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = '{entity.Label}', {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Description}' " );
                _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id} " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.Id},'{entity.Label}','{entity.Description}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<INominal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( INominal ).EnumerateEntityFieldNames();
            foreach ( INominal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.Id}) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.Id},'{entity.Label}','{entity.Description}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates keyed values. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IOneToManyId> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyId ).EnumerateEntityFieldNames();
            foreach ( IOneToManyId entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = {entity.ForeignId} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.ForeignId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IOneToManyId> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyId ).EnumerateEntityFieldNames();
            foreach ( IOneToManyId entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.ForeignId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates keyed values. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IOneToManyNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyNatural ).EnumerateEntityFieldNames();
            foreach ( IOneToManyNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IOneToManyNatural> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyNatural ).EnumerateEntityFieldNames();
            foreach ( IOneToManyNatural entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Update or insert values isolation. </summary>
        /// <remarks>
        /// David, 2020-05-14.
        /// https://stackoverflow.com/questions/108403/solutions-for-insert-or-update-on-sql-server.
        /// </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public static string UpsertValuesIsolation( string tableName, IEnumerable<IOneToManyRange> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyRange ).EnumerateEntityFieldNames();
            foreach ( IOneToManyRange entity in entities.ToArray() )
            {
                _ = builder.AppendLine( "SET TRANSACTION ISOLATION LEVEL SERIALIZABLE; " );
                _ = builder.AppendLine( "BEGIN TRANSACTION;" );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = {entity.Min}, {fieldNames.ElementAtOrDefault( 3 )} = {entity.Max} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId}) " );
                _ = builder.AppendLine( "If @@ROWCOUNT = 0 BEGIN " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Min},{entity.Max}) " );
                _ = builder.AppendLine( "END " );
                _ = builder.AppendLine( "COMMIT TRANSACTION; " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updated  keyed values. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IOneToManyRange> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyRange ).EnumerateEntityFieldNames();
            foreach ( IOneToManyRange entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = {entity.Min}, {fieldNames.ElementAtOrDefault( 3 )} = {entity.Max} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Min},{entity.Max}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IOneToManyRange> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyRange ).EnumerateEntityFieldNames();
            foreach ( IOneToManyRange entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Min},{entity.Max}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates keyed values. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IOneToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyReal ).EnumerateEntityFieldNames();
            foreach ( IOneToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = {entity.Amount} " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IOneToManyReal> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyReal ).EnumerateEntityFieldNames();
            foreach ( IOneToManyReal entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},{entity.Amount}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates keyed values. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IOneToManyLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyLabel ).EnumerateEntityFieldNames();
            foreach ( IOneToManyLabel entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 2 )} = '{entity.Label}' " );
                _ = builder.AppendLine( $"WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId}) " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},'{entity.Label}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IOneToManyLabel> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToManyLabel ).EnumerateEntityFieldNames();
            foreach ( IOneToManyLabel entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId})) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId},'{entity.Label}'); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IOneToMany> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToMany ).EnumerateEntityFieldNames();
            foreach ( IOneToMany entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} ))  " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignore records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IOneToMany> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToMany ).EnumerateEntityFieldNames();
            foreach ( IOneToMany entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE ([{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId} AND [{fieldNames.ElementAtOrDefault( 1 )}] = {entity.SecondaryId} ))  " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string Upsert( string tableName, IEnumerable<IOneToOne> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToOne ).EnumerateEntityFieldNames();
            foreach ( IOneToOne entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId}) " );
                _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = {entity.SecondaryId} " );
                _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId}; " );
                _ = builder.AppendLine( "ELSE " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignore records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnore( string tableName, IEnumerable<IOneToOne> entities )
        {
            var builder = new System.Text.StringBuilder();
            IEnumerable<string> fieldNames = typeof( IOneToOne ).EnumerateEntityFieldNames();
            foreach ( IOneToOne entity in entities.ToArray() )
            {
                _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {entity.PrimaryId}) " );
                _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({entity.PrimaryId},{entity.SecondaryId}); " );
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates key, name description records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public override string UpsertNameDescriptionRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( Enum value in Enum.GetValues( type ) )
            {
                int enumValue = Convert.ToInt32( value, System.Globalization.CultureInfo.CurrentCulture );
                if ( !excluded.Contains( enumValue ) )
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {enumValue}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = '{Enum.GetName( type, value )}',{fieldNames.ElementAtOrDefault( 2 )} = '{value.Description()}') " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {enumValue} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}]  VALUES ({enumValue},'{Enum.GetName( type, value )}','{value.Description()}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignore name description records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnoreNameDescriptionRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( Enum value in Enum.GetValues( type ) )
            {
                int enumValue = Convert.ToInt32( value, System.Globalization.CultureInfo.CurrentCulture );
                if ( !excluded.Contains( enumValue ) )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {enumValue}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}]  VALUES ({enumValue},'{Enum.GetName( type, value )}','{value.Description()}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or replaces name description records. </summary>
        /// <remarks>
        /// Insert or replace failed on some tables (e.g., Measurement) due to a foreign key constraint
        /// while not failing on other tables (e.g., Polarity and Bin). At this point (2/2020) the
        /// difference between these tables remains unclear.
        /// </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of inserted or replaced records. </returns>
        public override string RepsertNameDescriptionRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( Enum value in Enum.GetValues( type ) )
            {
                int enumValue = Convert.ToInt32( value, System.Globalization.CultureInfo.CurrentCulture );
                if ( !excluded.Contains( enumValue ) )
                {
                    _ = builder.AppendLine( $"INSERT REPLACE INTO [{tableName}] VALUES ({enumValue},'{Enum.GetName( type, value )}','{value.Description()}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates key-name records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public override string UpsertNameRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( Enum value in Enum.GetValues( type ) )
            {
                int enumValue = Convert.ToInt32( value, System.Globalization.CultureInfo.CurrentCulture );
                if ( !excluded.Contains( enumValue ) )
                {
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {enumValue}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = '{Enum.GetName( type, value )}' " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {enumValue} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({enumValue},'{Enum.GetName( type, value )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignores a name records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnoreNameRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( Enum value in Enum.GetValues( type ) )
            {
                int enumValue = Convert.ToInt32( value, System.Globalization.CultureInfo.CurrentCulture );
                if ( !excluded.Contains( enumValue ) )
                {
                    _ = builder.AppendLine( $"IF NOT EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {enumValue}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({enumValue},'{Enum.GetName( type, value )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or replaces name records. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of inserted or replaced records. </returns>
        public override string InsertReplaceNameRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            var builder = new System.Text.StringBuilder();
            foreach ( Enum value in Enum.GetValues( type ) )
            {
                int enumValue = Convert.ToInt32( value, System.Globalization.CultureInfo.CurrentCulture );
                if ( !excluded.Contains( enumValue ) )
                {
                    _ = builder.AppendLine( $"INSERT OR REPLACE INTO [{tableName}] VALUES ({enumValue},'{Enum.GetName( type, value )}'); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or updates key, name ordinal records. </summary>
        /// <remarks>   David, 2020-05-06. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public override string UpsertNameOrdinalRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            var builder = new System.Text.StringBuilder();
            int ordinalValue = 0;
            foreach ( Enum value in Enum.GetValues( type ) )
            {
                int enumValue = Convert.ToInt32( value, System.Globalization.CultureInfo.CurrentCulture );
                if ( !excluded.Contains( enumValue ) )
                {
                    ordinalValue += 1;
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {enumValue}) " );
                    _ = builder.AppendLine( $"UPDATE [{tableName}] SET {fieldNames.ElementAtOrDefault( 1 )} = '{Enum.GetName( type, value )}', {fieldNames.ElementAtOrDefault( 2 )} = {ordinalValue}) " );
                    _ = builder.AppendLine( $"WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {enumValue} " );
                    _ = builder.AppendLine( "ELSE " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}]  VALUES ({enumValue},'{Enum.GetName( type, value )}',{ordinalValue}); " );
                }
            }

            return builder.ToString();
        }

        /// <summary>   Inserts or ignore name ordinal records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>   The number of affected records. </returns>
        public override string InsertIgnoreNameOrdinalRecords( string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            var builder = new System.Text.StringBuilder();
            int ordinalValue = 0;
            foreach ( Enum value in Enum.GetValues( type ) )
            {
                int enumValue = Convert.ToInt32( value, System.Globalization.CultureInfo.CurrentCulture );
                if ( !excluded.Contains( enumValue ) )
                {
                    ordinalValue += 1;
                    _ = builder.AppendLine( $"IF EXISTS(SELECT * FROM [{tableName}] WHERE [{fieldNames.ElementAtOrDefault( 0 )}] = {enumValue}) " );
                    _ = builder.AppendLine( $"INSERT INTO [{tableName}] VALUES ({enumValue},'{Enum.GetName( type, value )}',{ordinalValue}); " );
                }
            }

            return builder.ToString();
        }
    }
}

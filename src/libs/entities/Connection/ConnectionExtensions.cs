using System;
using System.Collections.Generic;

using Dapper;

namespace isr.Dapper.Entities.ConnectionExtensions
{
    /// <summary>   A methods. </summary>
    /// <remarks>   David, 2020-10-06. </remarks>
    public static partial class Methods
    {

        /// <summary>   Executes the command using a transaction operation. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="command">      The command. </param>
        /// <returns>   The number of affected records. </returns>
        public static int ExecuteTransaction( this System.Data.IDbConnection connection, string command )
        {
            return Entity.ConnectionExtensions.Methods.ExecuteTransaction( connection, command );
        }

        /// <summary>   Select builder. </summary>
        /// <remarks>   David, 2020-05-26. </remarks>
        /// <param name="connection">   The data source connection. </param>
        /// <returns>   A SqlBuilderBase. </returns>
        private static SqlBuilderBase SelectBuilder( System.Data.IDbConnection connection )
        {
            return SqlBuilderBase.SelectBuilder( connection );
        }

        /// <summary>   Creates a view. </summary>
        /// <remarks>   David, 2020-08-11. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="viewName">     Name of the view. </param>
        /// <param name="selectQuery">  The select query. </param>
        /// <param name="dropFirst">    True to drop first. </param>
        /// <returns>   The number of affected lines. </returns>
        public static int CreateView( this System.Data.IDbConnection connection, string viewName, string selectQuery, bool dropFirst )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.Execute( SelectBuilder( connection ).CreateView( viewName, selectQuery, dropFirst ) );
        }

        /// <summary>   Inserts or updates <see cref="IFiveToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFiveToManyNatural"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFiveToManyNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores <see cref="IFiveToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFiveToManyNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates <see cref="IFiveToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFiveToManyReal"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFiveToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores <see cref="IFiveToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFiveToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates <see cref="IFourToMany"/> records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToMany"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFourToMany> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores <see cref="IFourToMany"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFourToMany> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates <see cref="IFourToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToManyId"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFourToManyId> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores <see cref="IFourToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFourToManyId> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates <see cref="IFourToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToManyNatural"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFourToManyNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores <see cref="IFourToManyNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFourToManyNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates <see cref="IFourToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IFourToManyReal"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFourToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores <see cref="IFourToManyReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IFourToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or update records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<ITwoToMany> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<ITwoToMany> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<ITwoToManyLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<ITwoToManyLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<ITwoToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<ITwoToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or update records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IThreeToMany> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IThreeToMany> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates <see cref="IThreeToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IThreeToManyId> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores <see cref="IThreeToManyId"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IThreeToManyId> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IThreeToManyLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IThreeToManyLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IThreeToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IThreeToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IExplicitKeyLabelNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IExplicitKeyLabelNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IExplicitKeyForeignLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IExplicitKeyForeignLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IExplicitKeyForeignNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IExplicitKeyForeignNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or update <see cref="IKeyForeign"/> records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeign"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeign> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores <see cref="IKeyForeign"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The <see cref="IKeyForeign"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeign> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates  <see cref="IKeyForeignLabelNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyForeignLabelNatural"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignLabelNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores  <see cref="IKeyForeignLabelNatural"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyForeignLabelNatural"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignLabelNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates  <see cref="IKeyForeignTime"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyForeignTime"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores  <see cref="IKeyForeignTime"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyForeignTime"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates  <see cref="IKeyForeignReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyForeignReal"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores  <see cref="IKeyForeignReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyForeignReal"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates  <see cref="IKeyTwoForeignReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyTwoForeignReal"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyTwoForeignReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores  <see cref="IKeyTwoForeignReal"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyTwoForeignReal"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyTwoForeignReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates  <see cref="IKeyTwoForeignTime"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyTwoForeignTime"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyTwoForeignTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores  <see cref="IKeyTwoForeignTime"/> records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The  <see cref="IKeyTwoForeignTime"/> entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyTwoForeignTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyLabelTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyLabelTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyLabelTimezone> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyLabelTimezone> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyNaturalTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyNaturalTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignLabelTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignLabelTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignNaturalTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IKeyForeignNaturalTime> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<INominal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<INominal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or ignore records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyId> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyId> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or ignore records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyNatural> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyRange> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyRange> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyReal> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates records. </summary>
        /// <remarks>   David, 2020-05-13. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToManyLabel> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or update records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToMany> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToMany> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or update records. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int Upsert( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToOne> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).Upsert( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   inserts or ignores records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="entities">     The entities. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnore( this System.Data.IDbConnection connection, string tableName, IEnumerable<IOneToOne> entities )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnore( tableName, entities ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates names description records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int UpsertNameDescriptionRecords( this System.Data.IDbConnection connection, string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).UpsertNameDescriptionRecords( tableName, fieldNames, type, excluded ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or ignores name description records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnoreNameDescriptionRecords( this System.Data.IDbConnection connection, string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnoreNameDescriptionRecords( tableName, fieldNames, type, excluded ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates name records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int UpsertNameRecords( this System.Data.IDbConnection connection, string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).UpsertNameRecords( tableName, fieldNames, type, excluded ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or ignores a name records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnoreNameRecords( this System.Data.IDbConnection connection, string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnoreNameRecords( tableName, fieldNames, type, excluded ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or updates name ordinal records. </summary>
        /// <remarks>   David, 2020-05-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int UpsertNameOrdinalRecords( this System.Data.IDbConnection connection, string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).UpsertNameOrdinalRecords( tableName, fieldNames, type, excluded ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Inserts or ignore name ordinal records. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="fieldNames">   List of names of the fields. </param>
        /// <param name="type">         The type. </param>
        /// <param name="excluded">     The excluded. </param>
        /// <returns>
        /// The number of affected records or the total number of records in the table if none were
        /// affected.
        /// </returns>
        public static int InsertIgnoreNameOrdinalRecords( this System.Data.IDbConnection connection, string tableName, IEnumerable<string> fieldNames, Type type, IEnumerable<int> excluded )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            int count = connection.Execute( SelectBuilder( connection ).InsertIgnoreNameOrdinalRecords( tableName, fieldNames, type, excluded ) );
            if ( count == 0 )
                count = connection.CountEntities( tableName );
            return count;
        }

        /// <summary>   Count entities. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <returns>   The total number of entities. </returns>
        public static int CountEntities( this System.Data.IDbConnection connection, string tableName )
        {
            return Entity.ConnectionExtensions.Methods.CountEntities( connection, tableName );
        }

        /// <summary>   Queries if a given table exists. </summary>
        /// <remarks>   David, 2020-05-16. </remarks>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool TableExists( this System.Data.IDbConnection connection, string tableName )
        {
            return Entity.ProviderBase.SelectProvider( connection ).TableExists( connection, tableName );
        }

        /// <summary>   Queries if a given index exists. </summary>
        /// <remarks>   David, 2020-05-16. </remarks>
        /// <param name="connection">   The data source connection. </param>
        /// <param name="indexName">    Name of the index. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool IndexExists( this System.Data.IDbConnection connection, string indexName )
        {
            return Entity.ProviderBase.SelectProvider( connection ).IndexExists( connection, indexName );
        }
    }
}

using System;

namespace isr.Dapper.Entity.ExceptionExtensions
{

    /// <summary> Adds exception data for <see cref="System.Data.SQLite.SQLiteException"/> and
    ///           <see cref="System.Data.SqlClient.SqlException"/>. </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class ExceptionExtensionMethods
    {

        /// <summary> Adds an <see cref="System.Data.SQLite.SQLiteException"/> data to 'exception'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception value, System.Data.SQLite.SQLiteException exception )
        {
            if ( exception is object )
            {
                int count = value.Data.Count;
                value.Data.Add( $"{count}-ErrorCode", exception.ErrorCode );
                value.Data.Add( $"{count}-ResultCode", exception.ResultCode );
            }

            return exception is object;
        }

        /// <summary> Adds an <see cref="System.Data.SqlClient.SqlException"/> exception data to 'exception'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception value, System.Data.SqlClient.SqlException exception )
        {
            if ( exception is object )
            {
                int count = value.Data.Count;
                value.Data.Add( $"{count}-ErrorCode", exception.ErrorCode );
                value.Data.Add( $"{count}-LineNumber", exception.LineNumber );
                value.Data.Add( $"{count}-State", exception.State );
                if ( (exception.Errors?.Count) > 0 == true )
                {
                    int errorId = 0;
                    foreach ( System.Data.SqlClient.SqlError err in exception.Errors )
                    {
                        errorId += 1;
                        // error number could repeat a few times. 
                        value.Data.Add( $"{count}.{errorId}-Error{err.Number}", err.ToString() );
                    }
                }
            }

            return exception is object;
        }

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( this Exception exception )
        {
            return AddExceptionData( exception, exception as System.Data.SqlClient.SqlException ) ||
                   AddExceptionData( exception, exception as System.Data.SQLite.SQLiteException );
        }
    }
}

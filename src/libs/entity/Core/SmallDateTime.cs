using System;

namespace isr.Dapper.Entity
{

    /// <summary> Small date time. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-05-18 </para>
    /// </remarks>
    public sealed class SmallDateTime
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private SmallDateTime()
        {
        }

        /// <summary> The minimum value. </summary>
        public static readonly DateTime MinValue = new DateTime( 1900, 1, 1, 0, 0, 0 );

        /// <summary> The maximum value. </summary>
        public static readonly DateTime MaxValue = new DateTime( 2079, 6, 6, 23, 59, 0 );
    }
}

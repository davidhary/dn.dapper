using System;
using System.ComponentModel;
using System.Threading;

using Dapper.Contrib.Extensions;

using isr.Std.Primitives;

namespace isr.Dapper.Entity
{

    /// <summary> Defines the contract that must be implemented by view entities. </summary>
    /// <remarks>
    /// <typeparamref name="TEntityNub"/> Specifies the generic interface type implemented by the
    /// <typeparamref name="TEntityNub"/>. The latter also implements IEquatable(Of TIEntity),
    /// ITypedCloneable(Of TIEntity), Class.<para>
    /// Note that, because of dapper construction of the
    /// <see cref="SqlMapperExtensions.IProxy">proxy</see> for change tracking, having the interface
    /// inherit from other interfaces, such as IEquitable, causes the Dapper Get() function to fail
    /// on constructing the Proxy type.</para><para>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value.</para> <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-25, . from the legacy entity base. </para>
    /// </remarks>
    public abstract class ViewEntityBase<TIEntity, TEntityNub> : INotifyPropertyChanged, IEquatable<ViewEntityBase<TIEntity, TEntityNub>>, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
            where TIEntity : class
            where TEntityNub : class, TIEntity, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the ViewEntityBase. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        protected ViewEntityBase() : base()
        {
        }

        /// <summary> Constructs an entity that is already stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="nub">   The entity nub to use for managing the store and the cache values. </param>
        /// <param name="cache"> The cached value. </param>
        protected ViewEntityBase( TEntityNub nub, TIEntity cache ) : this()
        {
            this.ICache = cache;
            this.Nub = nub;
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.NotifyPropertyChanged( new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-04-26. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void NotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            PropertyChanged?.Invoke( this, e );
        }

        /// <summary> Synchronously (default) notifies field change on a different thread. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void NotifyFieldChanged( PropertyChangedEventArgs e )
        {
            this.NotifyPropertyChanged( e );
        }

        /// <summary> Synchronously (default) notifies Field change on a different thread. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="propertyName"> (Optional) Name of the runtime caller member. </param>
        protected void NotifyFieldChanged( [System.Runtime.CompilerServices.CallerMemberName()] string propertyName = null )
        {
            if ( !string.IsNullOrWhiteSpace( propertyName ) )
            {
                this.NotifyFieldChanged( new PropertyChangedEventArgs( propertyName ) );
            }
        }

        #endregion

        #region " CACHE "

        /// <summary> Clears the cache. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        protected void ClearCache()
        {
            this.UpdateCache( this.Nub.CreateNew() );
        }

        /// <summary> Clears the internal cache and store. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public virtual void Clear()
        {
            this.ClearCache();
        }

        /// <summary> Updates the cache described by value. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="value"> The Part interface. </param>
        public virtual void UpdateCache( TIEntity value )
        {
            // the cache must be restored for keeping the proxy happy for detecting change.
            this.ICache = value;
        }

        /// <summary> Gets the entity cache representing the current values of the entity. </summary>
        /// <remarks>
        /// The dapper entity (see base class) dirty sentinel indicates if the entity changed.
        /// </remarks>
        /// <value> The i cache. </value>
        protected TIEntity ICache { get; set; }

        /// <summary> Gets the nub; Must not be nulled. </summary>
        /// <value> The nub. </value>
        private TEntityNub Nub { get; set; }

        /// <summary> Gets the cache. </summary>
        /// <value> The cache. </value>
        protected TEntityNub Cache => this.ICache as TEntityNub;

        #endregion

        #region " ENTITIES "

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> all. </returns>
        public abstract int FetchAllEntities( System.Data.IDbConnection connection );

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Indicates whether the current entity Cache is equal to another object Cache of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( TIEntity other )
        {
            return other is object && this.Cache.Equals( other );
        }

        /// <summary>   Cache equals. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool CacheEquals( TIEntity other ) => this.Equals( other );

        /// <summary> Compares two entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="left">  Specifies the entity to compare to. </param>
        /// <param name="right"> Specifies the entity to compare. </param>
        /// <returns> <c>True</c> if the entities are equal. </returns>
        public static new bool Equals( object left, object right )
        {
            return Equals( left as ViewEntityBase<TIEntity, TEntityNub>, right as ViewEntityBase<TIEntity, TEntityNub> );
        }

        /// <summary> Compares two entities. </summary>
        /// <remarks> The two entities are the same if they have the same X and Y coordinates. </remarks>
        /// <param name="left">  Specifies the entity to compare to. </param>
        /// <param name="right"> Specifies the entity to compare. </param>
        /// <returns> <c>True</c> if the entities are equal. </returns>
        public static bool Equals( ViewEntityBase<TIEntity, TEntityNub> left, ViewEntityBase<TIEntity, TEntityNub> right )
        {
            return left is object && right is object && left.Equals( right );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as ViewEntityBase<TIEntity, TEntityNub> );
        }

        /// <summary> Compares two entities using their cached values. </summary>
        /// <remarks> The two entities are the same if they have the same X1 and Y1 coordinates. </remarks>
        /// <param name="other"> Specifies the other entity. </param>
        /// <returns> <c>True</c> if the entities are equal. </returns>
        public bool Equals( ViewEntityBase<TIEntity, TEntityNub> other )
        {
            return other is object && this.Equals( other.ICache );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( ViewEntityBase<TIEntity, TEntityNub> left, ViewEntityBase<TIEntity, TEntityNub> right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( ViewEntityBase<TIEntity, TEntityNub> left, ViewEntityBase<TIEntity, TEntityNub> right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return this.Cache.GetHashCode();
        }

        #endregion

        #region " I TYPED CLONEABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-05-14. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public abstract TIEntity CreateNew();

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public abstract TIEntity CreateCopy();

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public abstract void CopyFrom( TIEntity value );

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public object Clone()
        {
            return this.CreateCopy();
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using isr.Std.Primitives;
using Dapper.Contrib.Extensions;
namespace isr.Dapper.Entity
{

    /// <summary>
    /// Defines the contract that must be implemented by a collection of
    /// <see cref="ViewEntityBase{TIEntity, TEntityNub}"/> entities.
    /// </summary>
    /// <remarks>
    /// <typeparamref name="TEntityNub"/> Specifies the generic interface type implemented by the
    /// <typeparamref name="TEntityNub"/>. The latter also implements IEquatable(Of TIEntity),
    /// ITypedCloneable(Of TIEntity), Class.<para>
    /// Note that, because of dapper construction of the
    /// <see cref="SqlMapperExtensions.IProxy">proxy</see> for change tracking, having the interface
    /// inherit from other interfaces, such as IEquitable, causes the Dapper Get() function to fail
    /// on constructing the Proxy type.</para><para>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value.</para> <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public abstract class ViewEntityKeyedCollection<TKey, TIEntity, TEntityNub, TEntity> : System.Collections.ObjectModel.KeyedCollection<TKey, TEntity>
            where TKey : struct
            where TIEntity : class
            where TEntityNub : class, TIEntity, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
            where TEntity : ViewEntityBase<TIEntity, TEntityNub>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        protected ViewEntityKeyedCollection() : base()
        {
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks> David, 2020-08-15. </remarks>
        public new virtual void Clear()
        {
            base.Clear();
        }

        /// <summary> Query if collection contains this item key. </summary>
        /// <remarks> David, 2020-08-15. </remarks>
        /// <param name="item"> The item. </param>
        /// <returns> True if collection contains this item key; otherwise false. </returns>
        public bool ContainsItemKey( TEntity item )
        {
            return this.Contains( this.GetKeyForItem( item ) );
        }

        /// <summary> Adds if not exists. </summary>
        /// <remarks> David, 2020-08-15. </remarks>
        /// <param name="item"> The item. </param>
        public virtual void AddIfNotExists( TEntity item )
        {
            if ( !this.Contains( this.GetKeyForItem( item ) ) )
            {
                this.Add( item );
            }
        }

        /// <summary> Populates the given entities. </summary>
        /// <remarks> David, 2020-08-15. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IList<TEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( TEntity item in entities )
                {
                    this.AddIfNotExists( item );
                }
            }
        }
    }
}

using System;

using Dapper;

namespace isr.Dapper.Entity
{

    /// <summary> A provider base. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-17 </para>
    /// </remarks>
    public abstract class ProviderBase
    {

        #region " PROVIDER SELECTOR "

        /// <summary> Select provider. </summary>
        /// <remarks> David, 2020-05-26. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> A ProviderBase. </returns>
        public static ProviderBase SelectProvider( System.Data.IDbConnection connection )
        {
            return connection as System.Data.SQLite.SQLiteConnection is object
                ? SQLiteProvider.Get()
                : connection as System.Data.SqlClient.SqlConnection is object
                    ? SqlServerProvider.Get()
                    : connection as TransactedConnection is object
                                    ? SelectProvider( (connection as TransactedConnection).Connection )
                                    : SqlServerProvider.Get();
        }

        #endregion

        #region " CONNECTION STRING "

        /// <summary> Gets the connection string. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The connection string. </value>
        public virtual string ConnectionString { get; set; }

        /// <summary> Gets the filename of the file. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The name of the file. </value>
        public string FileName { get; set; }

        /// <summary> Gets the name of the database. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The name of the database. </value>
        public virtual string DatabaseName { get; private set; }

        /// <summary> Builds master connection string. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
        /// <returns> A String. </returns>
        public abstract string BuildMasterConnectionString( bool includeInitialCatalog );

        #endregion

        #region " IDB CONNECTION "

        /// <summary> Gets a connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        public abstract System.Data.IDbConnection GetConnection();

        /// <summary> Queries if a given connection exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool ConnectionExists()
        {
            return this.GetConnection().Exists();
        }

        /// <summary> Can open connection. </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) CanOpenConnection()
        {
            return this.GetConnection().CanOpen();
        }

        /// <summary> Query if this object is connection exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> <c>true</c> if connection exists; otherwise <c>false</c> </returns>
        public abstract bool IsConnectionExists();

        /// <summary> Executes the transaction operation. </summary>
        /// <remarks> David, 2020-05-26. </remarks>
        /// <param name="command"> The command. </param>
        /// <returns> An Integer. </returns>
        public int ExecuteTransaction( string command )
        {
            return ExecuteTransaction( this.GetConnection(), command );
        }

        /// <summary> Executes the transaction operation. </summary>
        /// <remarks> David, 2020-05-26. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="command">    The command. </param>
        /// <returns> An Integer. </returns>
        public static int ExecuteTransaction( System.Data.IDbConnection connection, string command )
        {
            int result = -1;
            bool wasClosed = connection.IsClosed();
            try
            {
                if ( wasClosed )
                {
                    connection.Open();
                }

                using var transaction = connection.BeginTransaction();
                try
                {
                    result = connection.Execute( command, transaction: transaction );
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( wasClosed )
                {
                    connection.Close();
                }
            }

            return result;
        }

        #endregion

        #region " TABLES "

        /// <summary> Queries if a given table exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool TableExists( System.Data.IDbConnection connection, string tableName );

        /// <summary> Queries if a given table exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="tableName"> Name of the table. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TableExists( string tableName )
        {
            return this.TableExists( this.GetConnection(), tableName );
        }

        /// <summary> Drop table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool DropTable( System.Data.IDbConnection connection, string tableName );

        /// <summary> Executes the script from the specified 'FileName'. </summary>
        /// <remarks> David, 2020-03-25. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="fileName">   The name of the file. </param>
        /// <returns> The number of affected records. </returns>
        public static int ExecuteScript( System.Data.IDbConnection connection, string fileName )
        {
            if ( connection is null )
            {
                throw new ArgumentNullException( nameof( connection ) );
            }

            if ( fileName is null )
            {
                throw new ArgumentNullException( nameof( fileName ) );
            }

            try
            {
                return connection.Execute( System.IO.File.ReadAllText( fileName ) );
            }
            catch
            {
                throw;
            }
        }

        /// <summary> Executes the script from the specified 'FileName'. </summary>
        /// <remarks> David, 2020-03-25. </remarks>
        /// <param name="fileName"> The name of the file. </param>
        /// <returns> The number of affected records. </returns>
        public int ExecuteScript( string fileName )
        {
            return ExecuteScript( this.GetConnection(), fileName );
        }

        /// <summary> Queries if a given index exists. </summary>
        /// <remarks> David, 2020-05-26. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract bool IndexExists( System.Data.IDbConnection connection, string tableName );

        /// <summary> Queries if a given index exists. </summary>
        /// <remarks> David, 2020-05-26. </remarks>
        /// <param name="tableName"> Name of the table. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool IndexExists( string tableName )
        {
            return this.IndexExists( this.GetConnection(), tableName );
        }

        /// <summary> Counts the number of records in the specified table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The data source connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string tableName )
        {
            if ( connection is null )
            {
                throw new ArgumentNullException( nameof( connection ) );
            }

            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"select count(*) from [{tableName}]; " );
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( queryBuilder.ToString() );
            return selector.RawSql is null
                ? throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" )
                : connection.ExecuteScalar<int>( selector.RawSql );
        }

        /// <summary> Reseed identity. </summary>
        /// <remarks> David, 2020-06-01. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> An Integer. </returns>
        public abstract int ReseedIdentity( System.Data.IDbConnection connection, string tableName );

        /// <summary> Reseed identity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="tableName"> Name of the table. </param>
        /// <returns> An Integer. </returns>
        public int ReseedIdentity( string tableName )
        {
            return this.ReseedIdentity( this.GetConnection(), tableName );
        }

        #endregion

        #region " VIEWS "

        /// <summary> Drop view. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="viewName">   Name of the view. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract bool DropView( System.Data.IDbConnection connection, string viewName );

        /// <summary> Queries if a given View exists. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="viewName">   Name of the View. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool ViewExists( System.Data.IDbConnection connection, string viewName );

        /// <summary> Queries if a given View exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="viewName"> Name of the View. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool ViewExists( string viewName )
        {
            return this.ViewExists( this.GetConnection(), viewName );
        }

        #endregion

        #region " DATABASE "

        /// <summary> Queries if a given database exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="databaseName"> Filename of the connection file. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool DatabaseExists( System.Data.IDbConnection connection, string databaseName );

        /// <summary> Queries if a given database exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool DatabaseExists()
        {
            return this.DatabaseExists( this.GetConnection(), this.DatabaseName );
        }

        /// <summary> Creates a database. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <param name="databaseName">           Name of the database. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract bool CreateDatabase( string masterConnectionString, string databaseName );

        /// <summary> Drop connections. </summary>
        /// <remarks> David, 2020-04-02. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <param name="databaseName">           Filename of the connection file. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract bool DropConnections( string masterConnectionString, string databaseName );

        /// <summary> Drop database. </summary>
        /// <remarks> David, 2020-03-31. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <param name="databaseName">           Filename of the connection file. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public abstract bool DropDatabase( string masterConnectionString, string databaseName );

        /// <summary> Compact database. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="databaseName"> Filename of the connection file. </param>
        /// <returns> An Integer. </returns>
        public abstract int CompactDatabase( System.Data.IDbConnection connection, string databaseName );

        #endregion


        #region " PROVIDER "

        /// <summary> Type of the provider. </summary>
        private ProviderType _ProviderType;

        /// <summary> Gets the type of the provider. </summary>
        /// <value> The type of the provider. </value>
        public ProviderType ProviderType
        {
            get {
                if ( this._ProviderType == ProviderType.None )
                {
                    this._ProviderType = IdentifyProvider( this.GetConnection() );
                }

                return this._ProviderType;
            }
        }

        /// <summary> Identify provider. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> A ProviderType. </returns>
        public static ProviderType IdentifyProvider( System.Data.IDbConnection connection )
        {
            return connection as System.Data.SqlClient.SqlConnection is object
                ? ProviderType.SqlServer
                : connection as System.Data.SQLite.SQLiteConnection is object ? ProviderType.SQLite : default;
        }

        #endregion

        #region " FILE SYSTEM "

        /// <summary> Creates a directory. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        /// <returns> The new directory. </returns>
        public static System.IO.DirectoryInfo CreateDirectory( string path )
        {
            if ( !System.IO.Directory.Exists( path ) )
            {
                _ = System.IO.Directory.CreateDirectory( path );
            }

            System.IO.DirectoryInfo di;
            di = new System.IO.DirectoryInfo( path );
            return di;
        }

        /// <summary> Creates a directory. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new directory. </returns>
        public System.IO.DirectoryInfo CreateDirectory()
        {
            var fi = new System.IO.FileInfo( this.FileName );
            return CreateDirectory( fi.DirectoryName );
        }

        #endregion

        #region " DATE TIME "

        /// <summary> Gets or sets the  default date time format. </summary>
        /// <value> The date time format. </value>
        public static string DefaultDateTimeFormat { get; set; } = "yyyy-MM-dd HH:mm:ss.fff";

        /// <summary> Converts a value to a default label. </summary>
        /// <remarks> David, 2020-05-26. </remarks>
        /// <param name="value"> The value Date/Time. </param>
        /// <returns> Time value in the <see cref="DefaultDateTimeFormat"/> format. </returns>
        public static string ToDefaultTimestampFormat( DateTime value )
        {
            return ToTimestampFormat( value, DefaultDateTimeFormat );
        }

        /// <summary> Gets or sets the date time format. </summary>
        /// <value> The date time format. </value>
        public string DateTimeFormat { get; set; } = "yyyy-MM-dd HH:mm:ss.fff";

        /// <summary> Converts a value to a timestamp format. </summary>
        /// <remarks> David, 2020-05-26. </remarks>
        /// <param name="value">  The value Date/Time. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> Time value in the <see cref="DateTimeFormat"/> format. </returns>
        public static string ToTimestampFormat( DateTime value, string format )
        {
            return value.ToString( format );
        }


        #endregion

    }

    /// <summary> Values that represent provider types. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public enum ProviderType
    {

        /// <summary> An enum constant representing the none option. </summary>
        None,

        /// <summary> An enum constant representing the sq lite option. </summary>
        SQLite,

        /// <summary> An enum constant representing the SQL server option. </summary>
        SqlServer
    }
}

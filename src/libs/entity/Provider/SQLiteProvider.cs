using System;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Runtime.CompilerServices;

using Dapper;

namespace isr.Dapper.Entity
{

    /// <summary> An SQLite provider. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-17 </para>
    /// </remarks>
    public class SQLiteProvider : ProviderBase
    {

        #region " SINGLETON "

        private static readonly Lazy<SQLiteProvider> LazyProviderBase = new();

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        [System.Runtime.InteropServices.ComVisible( false )]
        public static ProviderBase Get()
        {
            return LazyProviderBase.Value;
        }

        #endregion

        #region " CREATE PROVIDER "

        /// <summary> Gets or sets the connection string format. </summary>
        /// <value> The connection string format. </value>
        public static string ConnectionStringFormat { get; set; } = "Data Source={0};Mode=ReadWriteCreate;datetimeformat=CurrentCulture";

        /// <summary> Extracts database file name from the connection string. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        /// <returns> A String. </returns>
        public static string ExtractDatabaseFilename( string connectionString )
        {
            string fileName = string.Empty;
            string dataSourcePrefix = "Data Source=";
            foreach ( string element in connectionString.Split( ';' ) )
            {
                if ( element.StartsWith( dataSourcePrefix, StringComparison.OrdinalIgnoreCase ) )
                {
                    fileName = element[dataSourcePrefix.Length..];
                    break;
                }
            }

            return fileName;
        }

        /// <summary> Builds connection string. </summary>
        /// <remarks> David, 2020-06-18. </remarks>
        /// <param name="filename"> Filename of the file. </param>
        /// <returns> A String. </returns>
        public static string BuildConnectionString( string filename )
        {
            return string.Format( ConnectionStringFormat, filename );
        }

        /// <summary> Creates a new SQLiteProvider. </summary>
        /// <remarks> David, 2020-06-18. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        /// <returns> A SQLiteProvider. </returns>
        public static SQLiteProvider Create( string connectionString )
        {
            return new SQLiteProvider() { ConnectionString = connectionString };
        }

        #endregion

        #region " DATABASE CONSTRUCTION "

        /// <summary> Creates the database. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <param name="databaseName">           Name of the database. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool CreateDatabase( string masterConnectionString, string databaseName )
        {
            SQLiteConnection.CreateFile( this.FileName );
            return true;
        }

        /// <summary> Drop connections. </summary>
        /// <remarks> David, 2020-04-02. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <param name="databaseName">           Filename of the connection file. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool DropConnections( string masterConnectionString, string databaseName )
        {
            return true;
        }

        /// <summary> Drop database. </summary>
        /// <remarks> David, 2020-03-31. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <param name="databaseName">           Filename of the connection file. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool DropDatabase( string masterConnectionString, string databaseName )
        {
            var fi = new FileInfo( this.FileName );
            if ( fi.Exists )
            {
                fi.Delete();
            }

            return true;
        }

        #endregion

        #region " SQLITE CONNECTION "

        private SQLiteConnection _ConnectionInternal;

        private SQLiteConnection ConnectionInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ConnectionInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ConnectionInternal != null )
                {
                    this._ConnectionInternal.StateChange -= this.Connection_StateChange;
                }

                this._ConnectionInternal = value;
                if ( this._ConnectionInternal != null )
                {
                    this._ConnectionInternal.StateChange += this.Connection_StateChange;
                }
            }
        }

        /// <summary> Gets or sets the connection string. </summary>
        /// <value> The connection string. </value>
        public override string ConnectionString
        {
            get => base.ConnectionString;

            set {
                if ( !string.Equals( value, this.ConnectionString ) )
                {
                    base.ConnectionString = value;
                    if ( !string.IsNullOrEmpty( value ) )
                    {
                        this.FileName = ExtractDatabaseFilename( value );
                        var fi = new FileInfo( this.FileName );
                        if ( !fi.Directory.Exists )
                        {
                            fi.Directory.Create();
                        }
                    }
                }
            }
        }

        /// <summary> Gets the connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        private SQLiteConnection GetConnectionThis()
        {
            return new SQLiteConnection( this.ConnectionString );
        }

        /// <summary> Query if this object is connection exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> <c>true</c> if connection exists; otherwise <c>false</c> </returns>
        public override bool IsConnectionExists()
        {
            return this.GetConnectionThis().Exists();
        }

        /// <summary> Connection state change. </summary>
        /// <remarks>
        /// Used to turn on foreign keys for cascading operations.
        /// https://stackoverflow.com/questions/13641250/sqlite-delete-cascade-not-working.
        /// </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      State change event information. </param>
        private void Connection_StateChange( object sender, StateChangeEventArgs e )
        {
            switch ( e.CurrentState )
            {
                case ConnectionState.Open:
                    {
                        // required only for SQLite, which defaults to OFF on open.
                        _ = this.ConnectionInternal.Execute( "PRAGMA foreign_keys = ON; " );
                        break;
                    }
            }
        }

        /// <summary> Builds master connection string. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
        /// <returns> A String. </returns>
        public override string BuildMasterConnectionString( bool includeInitialCatalog )
        {
            return this.FileName;
        }

        #endregion

        #region " DATABASE "

        /// <summary> Gets a connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        public override IDbConnection GetConnection()
        {
            this.ConnectionInternal = this.GetConnectionThis();
            return this.ConnectionInternal;
        }

        /// <summary> Queries if a given database exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="databaseName"> The database name. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool DatabaseExists( IDbConnection connection, string databaseName )
        {
            return File.Exists( this.FileName );
        }

        /// <summary> Compact database. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="databaseName"> The database name. </param>
        /// <returns> An Integer. </returns>
        public override int CompactDatabase( IDbConnection connection, string databaseName )
        {
            return connection.Execute( $"VACUUM {databaseName}; " );
        }

        #endregion

        #region " TABLES "

        /// <summary> Drop table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool DropTable( IDbConnection connection, string tableName )
        {
            return 1 < connection.Execute( $"DROP TABLE IF EXISTS {tableName}" );
        }

        /// <summary> Gets or sets the name of the sq lite master table. </summary>
        /// <value> The name of the SQLite master table. </value>
        public static string SQLiteMasterTableName { get; set; } = "sqlite_master";

        /// <summary> Queries if a given table exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool TableExists( IDbConnection connection, string tableName )
        {
            return 1 == connection.ExecuteScalar<int>( $"SELECT count(*) FROM {SQLiteMasterTableName} WHERE type = 'table' AND name = '{tableName}'; " );
        }

        /// <summary> Queries if a given index exists. </summary>
        /// <remarks> David, 2020-05-26. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="indexName">  Name of the table. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool IndexExists( IDbConnection connection, string indexName )
        {
            return 1 == connection.ExecuteScalar<int>( $"SELECT count(*) FROM {SQLiteMasterTableName} WHERE type = 'index' AND name = '{indexName}'; " );
        }

        /// <summary> Reseed identity. </summary>
        /// <remarks> David, 2020-06-01. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> An Integer. </returns>
        public override int ReseedIdentity( IDbConnection connection, string tableName )
        {
            return connection.Execute( $"UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='{tableName}'; " );
        }

        #endregion

        #region " VIEWS "

        /// <summary> Drop view. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="viewName">   Name of the view. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool DropView( IDbConnection connection, string viewName )
        {
            return 1 < connection.Execute( $"DROP VIEW IF EXISTS {viewName}" );
        }

        /// <summary> Queries if a given View exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="viewName">   Name of the View. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool ViewExists( IDbConnection connection, string viewName )
        {
            return 1 == connection.ExecuteScalar<int>( $"SELECT count(*) FROM {SQLiteMasterTableName} WHERE type = 'view' AND name = '{viewName}'; " );
        }

        #endregion

    }
}

using System;
using System.Runtime.CompilerServices;

using Dapper;

namespace isr.Dapper.Entity
{

    /// <summary> An SqlServer provider. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-26 </para>
    /// </remarks>
    public class SqlServerProvider : ProviderBase
    {

        private static readonly Lazy<SqlServerProvider> LazyProviderBase = new();

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        [System.Runtime.InteropServices.ComVisible( false )]
        public static ProviderBase Get()
        {
            return LazyProviderBase.Value;
        }

        #region " DATABASE CONSTRUCTION "

        /// <summary> Creates a new SystemDataSqlServerProvider. </summary>
        /// <remarks> David, 2020-03-11. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        /// <returns> A SqlServerProvider. </returns>
        public static SqlServerProvider Create( string connectionString )
        {
            return new SqlServerProvider() { ConnectionString = connectionString };
        }

        /// <summary> Creates a database. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <param name="databaseName">           Name of the database. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool CreateDatabase( string masterConnectionString, string databaseName )
        {
            var provider = Create( masterConnectionString );
            return CreateDatabase( provider.GetConnection(), databaseName );
        }

        /// <summary> Creates the database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="masterConnection"> The master connection. </param>
        /// <param name="databaseName">     Name of the database. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool CreateDatabase( System.Data.IDbConnection masterConnection, string databaseName )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( $@"
SET NOCOUNT ON; 
DECLARE @DBNAME VARCHAR(255)
SET @DBNAME = 'TestDB'
DECLARE @CREATE_TEMPLATE VARCHAR(MAX)
SET @CREATE_TEMPLATE = 'CREATE DATABASE {{DBNAME}}'; 
if exists (select * from master.dbo.sysdatabases where name='{databaseName}') drop database {databaseName}; 
DECLARE @device_directory NVARCHAR(520)
SELECT @device_directory = SUBSTRING(filename, 1, CHARINDEX(N'master.mdf', LOWER(filename)) - 1) FROM master.dbo.sysaltfiles WHERE dbid = 1 AND fileid = 1; 
EXECUTE (N'CREATE DATABASE {databaseName}
  ON PRIMARY (NAME = N''{databaseName}'', FILENAME = N''' + @device_directory + N'{databaseName}.mdf'')
  LOG ON (NAME = N''{databaseName}_log'',  FILENAME = N''' + @device_directory + N'{databaseName}_log.ldf'')'); 
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [{databaseName}].[dbo].[sp_fulltext_database] @action = 'enable'
end
ALTER DATABASE [{databaseName}] SET ANSI_NULL_DEFAULT OFF; 
ALTER DATABASE [{databaseName}] SET ANSI_NULLS OFF; 
ALTER DATABASE [{databaseName}] SET ANSI_PADDING OFF; 
ALTER DATABASE [{databaseName}] SET ANSI_WARNINGS OFF; 
ALTER DATABASE [{databaseName}] SET ARITHABORT OFF; 
ALTER DATABASE [{databaseName}] SET AUTO_CLOSE OFF; 
ALTER DATABASE [{databaseName}] SET AUTO_SHRINK OFF; 
ALTER DATABASE [{databaseName}] SET AUTO_UPDATE_STATISTICS ON; 
ALTER DATABASE [{databaseName}] SET CURSOR_CLOSE_ON_COMMIT OFF; 
ALTER DATABASE [{databaseName}] SET CURSOR_DEFAULT GLOBAL; 
ALTER DATABASE [{databaseName}] SET CONCAT_NULL_YIELDS_NULL OFF; 
ALTER DATABASE [{databaseName}] SET NUMERIC_ROUNDABORT OFF; 
ALTER DATABASE [{databaseName}] SET QUOTED_IDENTIFIER OFF; 
ALTER DATABASE [{databaseName}] SET RECURSIVE_TRIGGERS OFF; 
ALTER DATABASE [{databaseName}] SET  DISABLE_BROKER; 
ALTER DATABASE [{databaseName}] SET AUTO_UPDATE_STATISTICS_ASYNC OFF; 
ALTER DATABASE [{databaseName}] SET DATE_CORRELATION_OPTIMIZATION OFF; 
ALTER DATABASE [{databaseName}] SET TRUSTWORTHY OFF; 
ALTER DATABASE [{databaseName}] SET ALLOW_SNAPSHOT_ISOLATION OFF; 
ALTER DATABASE [{databaseName}] SET PARAMETERIZATION SIMPLE; 
ALTER DATABASE [{databaseName}] SET READ_COMMITTED_SNAPSHOT OFF; 
ALTER DATABASE [{databaseName}] SET HONOR_BROKER_PRIORITY OFF; 
ALTER DATABASE [{databaseName}] SET RECOVERY SIMPLE; 
ALTER DATABASE [{databaseName}] SET  MULTI_USER; 
ALTER DATABASE [{databaseName}] SET PAGE_VERIFY CHECKSUM;  
ALTER DATABASE [{databaseName}] SET DB_CHAINING OFF; 
ALTER DATABASE [{databaseName}] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ); 
ALTER DATABASE [{databaseName}] SET TARGET_RECOVERY_TIME = 0 SECONDS; 
ALTER DATABASE [{databaseName}] SET DELAYED_DURABILITY = DISABLED; 
ALTER DATABASE [{databaseName}] SET QUERY_STORE = OFF;
ALTER DATABASE [{databaseName}] SET  READ_WRITE; " );
            _ = masterConnection.Execute( queryBuilder.ToString() );
            return true;
        }

        /// <summary> Drop connections. </summary>
        /// <remarks> David, 2020-03-31. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <param name="databaseName">           Filename of the connection file. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool DropConnections( string masterConnectionString, string databaseName )
        {
            var provider = Create( masterConnectionString );
            return DropConnections( provider.GetConnection(), databaseName );
        }

        /// <summary> Drop connections. </summary>
        /// <remarks> David, 2020-04-02. </remarks>
        /// <param name="masterConnection"> The master connection. </param>
        /// <param name="databaseName">     Name of the database. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool DropConnections( System.Data.IDbConnection masterConnection, string databaseName )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $@"
DECLARE @DatabaseName nvarchar(50)
SET @DatabaseName = N'{databaseName}'

DECLARE @SQL varchar(max)

SELECT @SQL = COALESCE(@SQL,'') + 'Kill ' + Convert(varchar, SPId) + ';'
FROM MASTER..SysProcesses
WHERE DBId = DB_ID(@DatabaseName) AND SPId <> @@SPId

EXEC(@SQL)" );
            return -1 <= masterConnection.Execute( builder.ToString() );
        }

        /// <summary> Drop database. </summary>
        /// <remarks> David, 2020-03-31. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        /// <param name="databaseName">           Filename of the connection file. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool DropDatabase( string masterConnectionString, string databaseName )
        {
            var provider = Create( masterConnectionString );
            return DropDatabase( provider.GetConnection(), databaseName );
        }

        /// <summary> Drop database. </summary>
        /// <remarks> David, 2020-03-31. </remarks>
        /// <param name="masterConnection"> The master connection. </param>
        /// <param name="databaseName">     Name of the database. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool DropDatabase( System.Data.IDbConnection masterConnection, string databaseName )
        {
            return -1 <= masterConnection.Execute( $"DROP DATABASE IF EXISTS {databaseName}" );
        }

        /// <summary> Compact database. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="databaseName"> The database name. </param>
        /// <returns> An Integer. </returns>
        public override int CompactDatabase( System.Data.IDbConnection connection, string databaseName )
        {
            return connection.Execute( $"DBCC SHRINKDATABASE ({databaseName}, 10); " );
        }

        #endregion

        #region " SQL Connection "

        private System.Data.SqlClient.SqlConnection _ConnectionInternal;

        private System.Data.SqlClient.SqlConnection ConnectionInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ConnectionInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ConnectionInternal != null )
                {
                    this._ConnectionInternal.StateChange -= this.Connection_StateChange;
                }

                this._ConnectionInternal = value;
                if ( this._ConnectionInternal != null )
                {
                    this._ConnectionInternal.StateChange += this.Connection_StateChange;
                }
            }
        }

        /// <summary> Gets the connection. </summary>
        /// <value> The connection. </value>
        public System.Data.SqlClient.SqlConnection Connection => this.ConnectionInternal;

        /// <summary> Gets the connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        private System.Data.SqlClient.SqlConnection GetConnectionThis()
        {
            return new System.Data.SqlClient.SqlConnection( this.ConnectionString );
        }

        /// <summary> Query if this object is connection exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> <c>true</c> if connection exists; otherwise <c>false</c> </returns>
        public override bool IsConnectionExists()
        {
            return this.GetConnectionThis().Exists();
        }

        /// <summary> Connection state change. </summary>
        /// <remarks>
        /// Used to turn on foreign keys for cascading operations.
        /// https://stackoverflow.com/questions/13641250/SqlServer-delete-cascade-not-working.
        /// </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      State change event information. </param>
        private void Connection_StateChange( object sender, System.Data.StateChangeEventArgs e )
        {
            switch ( e.CurrentState )
            {
                case System.Data.ConnectionState.Open:
                    {
                        break;
                    }
                    // required only for SQLite, which defaults to OFF on open.
                    // _connection.Execute("PRAGMA foreign_keys = ON; ")
            }
        }

        /// <summary> Gets or sets the connection string builder. </summary>
        /// <value> The connection string builder. </value>
        protected System.Data.Common.DbConnectionStringBuilder ConnectionStringBuilder { get; set; } = new System.Data.Common.DbConnectionStringBuilder();

        /// <summary> Gets or sets the connection string. </summary>
        /// <value> The connection string. </value>
        public override string ConnectionString
        {
            get => this.ConnectionStringBuilder.ConnectionString;

            set {
                if ( !string.Equals( value, this.ConnectionString ) )
                {
                    this.ConnectionStringBuilder.ConnectionString = value;
                }
            }
        }

        /// <summary> Gets the name of the database. </summary>
        /// <value> The name of the database. </value>
        public override string DatabaseName => this.ConnectionStringBuilder["initial catalog"].ToString();

        /// <summary> The data source key. </summary>
        private const string _DataSourceKey = "data source";

        /// <summary> Gets the data source. </summary>
        /// <value> The data source. </value>
        public string DataSource => this.ConnectionStringBuilder.ContainsKey( _DataSourceKey ) ? this.ConnectionStringBuilder[_DataSourceKey].ToString() : string.Empty;

        /// <summary> The initial catalog key. </summary>
        private const string _InitialCatalogKey = "initial catalog";

        /// <summary> The password key. </summary>
        private const string _PasswordKey = "password";

        /// <summary> The password. </summary>
        private string _Password;

        /// <summary> The user identifier key. </summary>
        private const string _UserIdKey = "user id";

        /// <summary> The integrated security key. </summary>
        private const string _IntegratedSecurityKey = "integrated security";

        /// <summary> The persist security information key. </summary>
        private const string _PersistSecurityInfoKey = "persist security info";

        /// <summary>
        /// Gets the sentinel indicating if the provider uses integrated security (i.e., Windows
        /// credentials).
        /// </summary>
        /// <value> True if using integrated security. </value>
        public bool UsingIntegratedSecurity => string.Equals( this.IntegratedSecurity, "SSPI", StringComparison.OrdinalIgnoreCase ) || string.Equals( this.IntegratedSecurity, "True", StringComparison.OrdinalIgnoreCase ) || string.Equals( this.IntegratedSecurity, "Yes", StringComparison.OrdinalIgnoreCase );

        /// <summary> Gets the integrated security. </summary>
        /// <value> The integrated security. </value>
        public string IntegratedSecurity => this.ConnectionStringBuilder.ContainsKey( _IntegratedSecurityKey ) ? this.ConnectionStringBuilder[_IntegratedSecurityKey].ToString() : string.Empty;

        /// <summary> The credentials. </summary>
        private System.Data.SqlClient.SqlCredential _Credentials;

        /// <summary> Gets the credentials. </summary>
        /// <value> The credentials. </value>
        public System.Data.SqlClient.SqlCredential Credentials
        {
            get {
                if ( this._Credentials is null )
                {
                    var ss = new System.Security.SecureString();
                    this._Password = this.ConnectionStringBuilder.ContainsKey( _PasswordKey ) ? this.ConnectionStringBuilder[_PasswordKey].ToString() : string.Empty;
                    foreach ( char cc in this._Password.ToCharArray() )
                    {
                        ss.AppendChar( cc );
                    }

                    ss.MakeReadOnly();
                    string userId = this.ConnectionStringBuilder.ContainsKey( _UserIdKey ) ? this.ConnectionStringBuilder[_UserIdKey].ToString() : string.Empty;
                    if ( !(string.IsNullOrEmpty( userId ) || string.IsNullOrEmpty( ss.ToString() )) )
                    {
                        this._Credentials = new System.Data.SqlClient.SqlCredential( userId, ss );
                    }
                }

                return this._Credentials;
            }
        }

        /// <summary> Builds integrated security connection string. </summary>
        /// <remarks> David, 2020-07-14. </remarks>
        /// <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
        /// <returns> A String. </returns>
        public string BuildIntegratedSecurityConnectionString( bool includeInitialCatalog )
        {
            var connectionBuilder = new System.Data.Common.DbConnectionStringBuilder() { { _DataSourceKey, this.ConnectionStringBuilder[_DataSourceKey] }, { _IntegratedSecurityKey, "SSPI" }, { _PersistSecurityInfoKey, "False" } };
            if ( includeInitialCatalog )
            {
                connectionBuilder.Add( _InitialCatalogKey, this.ConnectionStringBuilder[_InitialCatalogKey] );
            }

            return connectionBuilder.ConnectionString;
        }

        /// <summary> Builds integrated security connection string. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
        /// <returns> A String. </returns>
        public string BuildConnectionString( bool includeInitialCatalog )
        {
            var connectionBuilder = new System.Data.Common.DbConnectionStringBuilder() { { _DataSourceKey, this.ConnectionStringBuilder[_DataSourceKey] }, { _UserIdKey, this.Credentials.UserId }, { _PasswordKey, this._Password }, { _PersistSecurityInfoKey, "False" } };
            if ( includeInitialCatalog )
            {
                connectionBuilder.Add( _InitialCatalogKey, this.ConnectionStringBuilder[_InitialCatalogKey] );
            }

            return connectionBuilder.ConnectionString;
        }

        /// <summary> Builds master connection string. </summary>
        /// <remarks> David, 2020-07-14. </remarks>
        /// <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
        /// <returns> A String. </returns>
        public override string BuildMasterConnectionString( bool includeInitialCatalog )
        {
            return this.BuildIntegratedSecurityConnectionString( includeInitialCatalog );
        }

        #endregion

        #region " IDB Connection "

        /// <summary> Gets a connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The connection. </returns>
        public override System.Data.IDbConnection GetConnection()
        {
            this.ConnectionInternal = this.GetConnectionThis();
            return this.ConnectionInternal;
        }

        /// <summary> Gets or sets the using information schema tables. </summary>
        /// <value> The using information schema tables. </value>
        public static bool UsingInformationSchemaTables { get; set; }

        /// <summary> Queries if a given database exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="databaseName"> Filename of the connection file. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool DatabaseExists( System.Data.IDbConnection connection, string databaseName )
        {
            return 1 == connection.ExecuteScalar<int>( $"SELECT count(*) FROM master.dbo.sysdatabases WHERE name = N'{databaseName}'" );
        }

        #endregion

        #region " TABLES "

        /// <summary> Drop table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool DropTable( System.Data.IDbConnection connection, string tableName )
        {
            return 1 < connection.Execute( $"DROP TABLE IF EXISTS {tableName}" );
        }

        /// <summary> Queries if a given table exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool TableExists( System.Data.IDbConnection connection, string tableName )
        {
            return UsingInformationSchemaTables ? 1 == connection.ExecuteScalar<int>( $"SELECT count(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'{tableName}'" ) : 1 == connection.ExecuteScalar<int>( $"SELECT count(*) FROM sys.tables WHERE name = N'{tableName}'; " );
        }

        /// <summary> Queries if a given index exists. </summary>
        /// <remarks> David, 2020-05-26. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="indexName">  Name of the table. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool IndexExists( System.Data.IDbConnection connection, string indexName )
        {
            return 1 == connection.ExecuteScalar<int>( $"SELECT count(*) FROM sys.indexes WHERE name = N'{indexName}'; " );
        }

        /// <summary> Reseed identity. </summary>
        /// <remarks> David, 2020-06-01. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> An Integer. </returns>
        public override int ReseedIdentity( System.Data.IDbConnection connection, string tableName )
        {
            return connection.Execute( $@"DBCC CHECKIDENT ([{tableName}],RESEED); 
DBCC CHECKIDENT ([{tableName}]); " );
        }

        #endregion

        #region " VIEWS "

        /// <summary> Drop view. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="viewName">   Name of the view. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public override bool DropView( System.Data.IDbConnection connection, string viewName )
        {
            return 1 < connection.Execute( $"DROP VIEW IF EXISTS {viewName}" );
        }

        /// <summary> Queries if a given View exists. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="viewName">   Name of the View. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool ViewExists( System.Data.IDbConnection connection, string viewName )
        {
            return 1 == connection.ExecuteScalar<int>( $"SELECT count(*) FROM sys.views WHERE name = N'{viewName}'; " );
        }

        #endregion

        #region " LOGIN "

        /// <summary>
        /// Establishes login credentials for the specified principal and sets it as the database owner.
        /// </summary>
        /// <remarks>
        /// Creates new server and database principals if none exist or if database principal is not
        /// longer valid.
        /// </remarks>
        /// <param name="masterProvider"> The master provider. </param>
        /// <param name="databaseName">   Name of the database. </param>
        /// <param name="principalName">  Name of the principal. </param>
        /// <param name="password">       The password. </param>
        /// <returns> The new login. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public static (bool Success, string Details) EstablishLoginCredentials( SqlServerProvider masterProvider, string databaseName, string principalName, System.Security.SecureString password )
        {
            string activity = $"Establishing {nameof( System.Data.IDbConnection )} for '{principalName}'";
            try
            {
                // checks if we have a user login issue.
                System.Data.IDbConnection connection = new System.Data.SqlClient.SqlConnection( masterProvider.ConnectionString );
                activity = $"finding {principalName} at {nameof( SqlServerProvider.ServerPrincipalExists )}; or {nameof( SqlServerProvider.QueryUserLoginMatch )}";
                if ( !ServerPrincipalExists( connection, principalName ) )
                {
                    activity = $"invoking {nameof( SqlServerProvider.DropDatabaseUser )} for {principalName}";
                    _ = DropDatabaseUser( connection, databaseName, principalName );
                    activity = $"invoking {nameof( SqlServerProvider.AddLoginPrincipal )} for {principalName}";
                    _ = AddLoginPrincipal( connection, databaseName, principalName, password );
                }
                else if ( !QueryUserLoginMatch( connection, databaseName, principalName ) )
                {
                    // there is a mismatch between the login principal and the database user, which needs to be changed
                    activity = $"invoking {nameof( SqlServerProvider.DropDatabaseUser )} for {principalName}";
                    _ = DropDatabaseUser( connection, databaseName, principalName );
                }

                if ( !DatabasePrincipalExists( connection, databaseName, principalName ) )
                {
                    activity = $"invoking {nameof( SqlServerProvider.CreateDatabaseUser )} for {principalName}";
                    _ = CreateDatabaseUser( connection, databaseName, principalName );
                }

                activity = $"invoking {nameof( SqlServerProvider.QueryUserLoginMatch )} for {principalName}";
                if ( QueryUserLoginMatch( connection, databaseName, principalName ) )
                {
                    activity = $"invoking {nameof( SqlServerProvider.AlterDatabaseOwnerRole )} for {principalName}";
                    _ = AlterDatabaseOwnerRole( connection, true, databaseName, principalName );
                    return (true, string.Empty);
                }
                else
                {
                    return (false, $"Failed establishing login credentials for user {principalName}");
                }
            }
            catch ( Exception ex )
            {
                return (false, $"Exception {activity};. {ex}");
            }
        }

        /// <summary> Name of the server principals table. </summary>
        public const string ServerPrincipalsTableName = "SYS.SERVER_PRINCIPALS";

        /// <summary> Name of the database principals table. </summary>
        public const string DatabasePrincipalsTableName = "SYS.DATABASE_PRINCIPALS";

        /// <summary> Queries if a given server principal exists. </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool ServerPrincipalExists( System.Data.IDbConnection connection, string principalName )
        {
            return 1 == connection.ExecuteScalar<int>( $"SELECT count(*) FROM {ServerPrincipalsTableName} WHERE name = N'{principalName}'" );
        }

        /// <summary> Queries if a given database principal exists. </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="databaseName">  Name of the database. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool DatabasePrincipalExists( System.Data.IDbConnection connection, string databaseName, string principalName )
        {
            return 1 == connection.ExecuteScalar<int>( $@"USE [{databaseName}];
SELECT count(*) FROM {DatabasePrincipalsTableName} WHERE name = N'{principalName}'" );
        }

        /// <summary>
        /// Fetch the server principal security identifier (SID) or empty string if principal does not
        /// exist.
        /// </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> A String. </returns>
        public static string ServerPrincipalSecurityId( System.Data.IDbConnection connection, string principalName )
        {
            return ServerPrincipalExists( connection, principalName ) ? connection.ExecuteScalar<string>( $"SELECT CAST(SID AS VARCHAR(MAX)) FROM {ServerPrincipalsTableName} WHERE name = N'{principalName}'" ) : string.Empty;
        }

        /// <summary>
        /// Fetch the database principal security identifier (SID) or empty string if principal does not
        /// exist.
        /// </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="databaseName">  Name of the database. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> A String. </returns>
        public static string DatabasePrincipalId( System.Data.IDbConnection connection, string databaseName, string principalName )
        {
            return ServerPrincipalExists( connection, principalName ) ? connection.ExecuteScalar<string>( $@"USE [{databaseName}];
SELECT CAST(SID AS VARCHAR(MAX)) FROM  {DatabasePrincipalsTableName} WHERE name = N'{principalName}'" ) : string.Empty;
        }

        /// <summary> Drop Principal. </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> An Integer. </returns>
        public static int DropLoginPrincipal( System.Data.IDbConnection connection, string principalName )
        {
            return connection.Execute( $"IF EXISTS (SELECT * FROM {ServerPrincipalsTableName} WHERE name = N'{principalName}') DROP LOGIN [{principalName}]; " );
        }

        /// <summary> Adds a login principal. </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="databaseName">  Filename of the connection file. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <param name="password">      The password. </param>
        /// <returns> An Integer. </returns>
        public static int AddLoginPrincipal( System.Data.IDbConnection connection, string databaseName, string principalName, System.Security.SecureString password )
        {
            return connection.Execute( $"CREATE LOGIN [{principalName}] WITH PASSWORD=N'{password}', DEFAULT_DATABASE=[{databaseName}], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF; " );
        }

        /// <summary> Drop User. </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="databaseName">  The database name. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> An Integer. </returns>
        public static int DropDatabaseUser( System.Data.IDbConnection connection, string databaseName, string principalName )
        {
            return connection.Execute( $@"USE [{databaseName}];
IF EXISTS (SELECT * FROM {DatabasePrincipalsTableName} WHERE name = N'{principalName}') DROP USER [{principalName}]; " );
        }

        /// <summary>
        /// Adds a database user <paramref name="principalName"/> to the same login name w/o assigning a
        /// role.
        /// </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="databaseName">  Filename of the connection file. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> An Integer. </returns>
        public static int CreateDatabaseUser( System.Data.IDbConnection connection, string databaseName, string principalName )
        {
            return connection.Execute( $@"USE [{databaseName}];
IF  NOT EXISTS (SELECT * FROM {DatabasePrincipalsTableName} WHERE name = N'{principalName}') 
CREATE USER [{principalName}] FOR LOGIN [{principalName}] WITH DEFAULT_SCHEMA=[dbo]; " );
        }

        /// <summary> The database owner role. </summary>
        private const string _DatabaseOwnerRole = "db_owner";

        /// <summary> The database reader role. </summary>
        private const string _DatabaseReaderRole = "db_datareader";

        /// <summary> The database writer role. </summary>
        private const string _DatabaseWriterRole = "db_datawriter";

        /// <summary> The add role member stored procedure. </summary>
        private const string _AddRoleMemberStoredProcedure = "sp_addrolemember";

        /// <summary> The drop role member stored procedure. </summary>
        private const string _DropRoleMemberStoredProcedure = "sp_droprolemember";

        /// <summary> Alter database owner role. </summary>
        /// <remarks> David, 2020-03-25. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="add">           True to add. </param>
        /// <param name="databaseName">  Filename of the connection file. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> An Integer. </returns>
        public static int AlterDatabaseOwnerRole( System.Data.IDbConnection connection, bool add, string databaseName, string principalName )
        {
            string action = add ? _AddRoleMemberStoredProcedure : _DropRoleMemberStoredProcedure;
            return connection.Execute( $@"USE [{databaseName}];
EXEC {action} '{_DatabaseOwnerRole}', '{principalName}'" );
        }

        /// <summary> Alter database scriber role. </summary>
        /// <remarks> David, 2020-03-25. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="add">           True to add. </param>
        /// <param name="databaseName">  The database name. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> An Integer. </returns>
        public static int AlterDatabaseScriberRole( System.Data.IDbConnection connection, bool add, string databaseName, string principalName )
        {
            string action = add ? _AddRoleMemberStoredProcedure : _DropRoleMemberStoredProcedure;
            return connection.Execute( $@"USE [{databaseName}];
EXEC {action} '{_DatabaseReaderRole}', '{principalName}'
        EXEC {action} '{_DatabaseWriterRole}', '{principalName}'" );
        }

        /// <summary> Checks if the database principal has an owner roll. </summary>
        /// <remarks> David, 2020-04-02. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool QueryDatabasePrincipalOwnerRoll( System.Data.IDbConnection connection, string principalName )
        {
            return 1 == CountDatabasePrincipalRoles( connection, _DatabaseOwnerRole, principalName );
        }

        /// <summary> Count database principal roles. </summary>
        /// <remarks> David, 2020-04-02. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="role">          The role. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> The total number of database principal roles. </returns>
        public static int CountDatabasePrincipalRoles( System.Data.IDbConnection connection, string role, string principalName )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $@"WITH RoleMembers (member_principal_id, role_principal_id)    
AS  
( 
  SELECT  
   rm1.member_principal_id,  
   rm1.role_principal_id      
  FROM sys.database_role_members rm1 (NOLOCK)  
   UNION ALL  
  SELECT 
   d.member_principal_id, 
   rm.role_principal_id      
  FROM sys.database_role_members rm (NOLOCK)   
   INNER JOIN RoleMembers AS d     
   ON rm.member_principal_id = d.role_principal_id  
)  
select distinct rp.name as database_role, mp.name as database_userl  
from RoleMembers drm  
  join sys.database_principals rp on (drm.role_principal_id = rp.principal_id)  
  join sys.database_principals mp on (drm.member_principal_id = mp.principal_id) " );
            _ = string.IsNullOrEmpty( role )
                ? builder.AppendLine( $"where rp.name = '{role}' and mp.name = '{principalName}' " )
                : builder.AppendLine( $"where mp.name = '{principalName}' " );

            _ = builder.AppendLine( "order by rp.name " );
            return connection.Execute( builder.ToString() );
        }

        /// <summary>
        /// Queries if there is match between the server login principal and the database user. If a
        /// match, the user can login.
        /// </summary>
        /// <remarks> David, 2020-03-17. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="databaseName">  Name of the database. </param>
        /// <param name="principalName"> Name of the principal. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool QueryUserLoginMatch( System.Data.IDbConnection connection, string databaseName, string principalName )
        {
            string loginId = ServerPrincipalSecurityId( connection, principalName );
            string userId = DatabasePrincipalId( connection, databaseName, principalName );
            return !string.IsNullOrEmpty( loginId ) && string.Equals( loginId, userId, StringComparison.InvariantCulture );
        }

        #endregion

    }
}

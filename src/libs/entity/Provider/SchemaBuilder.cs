using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;

using isr.Dapper.Entity.ConnectionExtensions;

namespace isr.Dapper.Entity
{

    /// <summary> The database schema builder. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-09-25 </para>
    /// </remarks>
    public class SchemaBuilder
    {

        /// <summary> Gets the provider. </summary>
        /// <value> The provider. </value>
        public ProviderBase Provider { get; set; }

        #region " TABLES "

        /// <summary> Verifies that tables exist. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private void AssertTablesExist()
        {
            using var connection = this.Provider.GetConnection();
            foreach ( string tableName in this.TableNames )
            {
                if ( !this.Provider.TableExists( connection, tableName ) )
                {
                    throw new System.Data.DataException( $"table {tableName} should exist" );
                }
            }
        }

        /// <summary> Gets the build tables. </summary>
        /// <value> The build tables. </value>
        public IDictionary<string, TableMaker> EnumeratedTables { get; private set; } = new Dictionary<string, TableMaker>();

        /// <summary> Gets a list of names of the tables. </summary>
        /// <value> A list of names of the tables. </value>
        public IEnumerable<string> TableNames => this.EnumeratedTables.Keys;

        /// <summary> Makes a table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="maker">      The maker. </param>
        /// <returns> A TableCategory. </returns>
        private TableCategory MakeTableThis( System.Data.IDbConnection connection, TableMaker maker )
        {
            int actionCount = 0;
            string queryCommand;
            string tableName = string.Empty;
            maker.Failed = false;
            int minimumInsertCount = 0;
            if ( this.Provider.TableExists( connection, maker.TableName ) )
            {
                maker.Category |= TableCategory.Existed;
                if ( maker.ExistsAction is object )
                {
                    actionCount = maker.ExistsAction.Invoke( connection );
                }

                if ( maker.ExistsBuildAction is object )
                {
                    queryCommand = maker.ExistsBuildAction.Invoke( connection );
                }

                if ( maker.ExistsUpdateAction is object )
                {
                    if ( !maker.ExistsUpdateAction.Invoke( connection, maker.ExistsUpdateValue ) )
                    {
                        throw new System.InvalidOperationException( $"{nameof( isr.Dapper.Entity.TableMaker.ExistsUpdateAction )} failed on table {maker.TableName}" );
                    }
                }
            }
            else
            {
                minimumInsertCount = 1;
                maker.Category &= ~TableCategory.Existed;
                if ( maker.CreateAction is object )
                {
                    tableName = maker.CreateAction.Invoke( connection );
                    maker.Category = string.Equals( tableName, maker.TableName, StringComparison.OrdinalIgnoreCase )
                        ? maker.Category | TableCategory.Created
                        : throw new InvalidOperationException( $"Created table name {tableName} does not match maker name {maker.TableName}" );
                }

                if ( maker.PostCreateAction is object )
                {
                    if ( !maker.PostCreateAction.Invoke( connection, maker.PostCreateValue ) )
                    {
                        throw new System.InvalidOperationException( $"{nameof( isr.Dapper.Entity.TableMaker.PostCreateAction )} failed on table {maker.TableName}" );
                    }
                }
            }

            if ( maker.InsertAction is object )
            {
                // the action could return -1 if no rows were affected.
                actionCount = Math.Max( 0, maker.InsertAction.Invoke( connection ) );
                if ( actionCount < minimumInsertCount )
                {
                    throw new System.InvalidOperationException( $"{nameof( isr.Dapper.Entity.TableMaker.InsertAction )} into table {maker.TableName} yielded {actionCount} records" );
                }
            }

            if ( maker.InsertFileValuesAction is object )
            {
                // the action could return -1 if no rows were affected.
                actionCount = Math.Max( 0, maker.InsertFileValuesAction.Invoke( connection, maker.ValuesFileName ) );
                // this is required in case the insert replaced no values.
                if ( actionCount < minimumInsertCount )
                {
                    throw new System.InvalidOperationException( $"{nameof( isr.Dapper.Entity.TableMaker.InsertFileValuesAction )} into table {maker.TableName} yielded 0 records" );
                }
            }

            if ( maker.InsertValuesAction is object )
            {
                if ( !maker.InsertValuesAction.Invoke( connection, maker.Values ) )
                {
                    throw new System.InvalidOperationException( $"{nameof( isr.Dapper.Entity.TableMaker.InsertValuesAction )}  failed on table {maker.TableName}" );
                }
            }

            return maker.Category;
        }

        /// <summary> Makes a table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="maker">      The maker. </param>
        /// <returns> A TableCategory. </returns>
        private TableCategory MakeTable( System.Data.IDbConnection connection, TableMaker maker )
        {
            try
            {
                _ = this.MakeTableThis( connection, maker );
            }
            catch
            {
                maker.Failed = true;
                throw;
            }
            finally
            {
                if ( maker.Failed && TableCategory.Existed != (TableCategory.Existed & maker.Category) )
                {
                    // if failed and not existed, make sure to drop this table
                    try
                    {
                        _ = this.Provider.DropTable( connection, maker.TableName );
                    }
                    finally
                    {
                    }
                }
            }

            return maker.Category;
        }

        /// <summary> Adds tables. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private void AddTables()
        {
            using var connection = this.Provider.GetConnection();
            connection.Open();
            foreach ( KeyValuePair<string, TableMaker> kvp in this.EnumeratedTables )
            {
                _ = this.MakeTable( connection, kvp.Value );
            }
        }

        /// <summary> Fetches all. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="maker">      The maker. </param>
        private void FetchAll( System.Data.IDbConnection connection, TableMaker maker )
        {
            if ( this.Provider.TableExists( connection, maker.TableName ) )
            {
                if ( maker.FetchAllAction is object )
                {
                    maker.FetchAllAction.Invoke( connection );
                }
            }
        }

        /// <summary> Fetches all. </summary>
        /// <remarks> David, 2020-07-11. </remarks>
        public void FetchAll()
        {
            using var connection = this.Provider.GetConnection();
            connection.Open();
            foreach ( KeyValuePair<string, TableMaker> kvp in this.EnumeratedTables )
            {
                this.FetchAll( connection, kvp.Value );
            }
        }

        /// <summary> Deletes all records. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="category"> The category. </param>
        public void DeleteAllRecords( TableCategory category )
        {
            // exclude lookup tables on first round
            this.DeleteAllRecords( category, TableCategory.Lookup );
            this.DeleteAllRecords( category, TableCategory.None );
        }

        /// <summary> Deletes all records. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="category">         The category. </param>
        /// <param name="excludedCategory"> Category the excluded belongs to. </param>
        private void DeleteAllRecords( TableCategory category, TableCategory excludedCategory )
        {
            using var connection = this.Provider.GetConnection();
            connection.Open();
            var categories = new TableCategory[] { TableCategory.Created, TableCategory.Existed };
            foreach ( TableCategory tableCategory in categories )
            {
                if ( tableCategory == (category & tableCategory) )
                {
                    // select the tables in the reverse order they were enumerated.
                    foreach ( KeyValuePair<string, TableMaker> kvp in this.EnumeratedTables.Reverse() )
                    {
                        if ( excludedCategory != (kvp.Value.Category & excludedCategory) && tableCategory == (kvp.Value.Category & tableCategory) && this.Provider.TableExists( connection, kvp.Key ) )
                        {
                            // clear the records if the table is of the specified category and not excluded
                            _ = connection.Execute( $"delete from {kvp.Key}" );
                        }
                    }
                }
            }
        }

        /// <summary> Drop all tables. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="ignoreErrors"> True to ignore errors. </param>
        public void DropAllTables( bool ignoreErrors )
        {
            this.DropTables( TableCategory.Lookup, ignoreErrors );
            this.DropTables( TableCategory.None, ignoreErrors );
        }

        /// <summary> Drop all tables per specified category. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="category">     The category. </param>
        /// <param name="ignoreErrors"> True to ignore errors. </param>
        public void DropAllTables( TableCategory category, bool ignoreErrors )
        {
            // set the excluded categories
            var c = ~category;
            // exclude lookup tables on first round
            if ( TableCategory.Lookup == (TableCategory.Lookup & category) )
            {
                // if dropping lookup tables, start first by excluding the lookup tables.
                c |= TableCategory.Lookup;
                this.DropTables( c, ignoreErrors );
                c &= ~TableCategory.Lookup;
            }

            this.DropTables( c, ignoreErrors );
        }

        /// <summary> Drop tables except those excluded. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="excludedCategory"> Category the excluded belongs to. </param>
        /// <param name="ignoreErrors">     True to ignore errors. </param>
        private void DropTables( TableCategory excludedCategory, bool ignoreErrors )
        {
            using var connection = this.Provider.GetConnection();
            connection.Open();
            // select the tables in the reverse order they were enumerated.
            foreach ( KeyValuePair<string, TableMaker> kvp in this.EnumeratedTables.Reverse() )
            {
                if ( (excludedCategory == TableCategory.None || excludedCategory != (kvp.Value.Category & excludedCategory)) && this.Provider.TableExists( connection, kvp.Key ) )
                {
                    try
                    {
                        _ = this.Provider.DropTable( connection, kvp.Key );
                    }
                    catch
                    {
                        if ( !ignoreErrors )
                        {
                            throw;
                        }
                    }
                }
            }
        }

        #endregion

        #region " VIEWS "

        /// <summary> Verifies that Views exist. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private void AssertViewsExist()
        {
            using var connection = this.Provider.GetConnection();
            foreach ( string ViewName in this.ViewNames )
            {
                if ( !this.Provider.ViewExists( connection, ViewName ) )
                {
                    throw new System.Data.DataException( $"View {ViewName} should exist" );
                }
            }
        }

        /// <summary> Gets the build Views. </summary>
        /// <value> The build Views. </value>
        public IDictionary<string, ViewMaker> EnumeratedViews { get; private set; } = new Dictionary<string, ViewMaker>();

        /// <summary> Gets a list of names of the Views. </summary>
        /// <value> A list of names of the Views. </value>
        public IEnumerable<string> ViewNames => this.EnumeratedViews.Keys;

        /// <summary> Makes a View. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="maker">      The maker. </param>
        /// <returns> A ViewCategory. </returns>
        private (bool Success, string Details) MakeViewThis( System.Data.IDbConnection connection, ViewMaker maker )
        {
            string viewName = maker.ViewName;
            if ( maker.CreateAction is object )
            {
                viewName = maker.CreateAction.Invoke( connection );
                return string.Equals( viewName, maker.ViewName, StringComparison.OrdinalIgnoreCase ) ? (true, string.Empty) : (false, $"Created View name {viewName} does not match maker name {maker.ViewName}");
            }
            else
            {
                return (true, string.Empty);
            }
        }

        /// <summary> Makes a View. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="maker">      The maker. </param>
        private void MakeView( System.Data.IDbConnection connection, ViewMaker maker )
        {
            var result = default( (bool Success, string Details) );
            var exists = default( bool );
            try
            {
                exists = connection.ViewExists( maker.ViewName );
                result = this.MakeViewThis( connection, maker );
                if ( !result.Success )
                {
                    throw new InvalidOperationException( result.Details );
                }
            }
            catch
            {
                result = (false, $"Exception creating View {maker.ViewName};. ");
                throw;
            }
            finally
            {
                if ( !(result.Success || exists) )
                {
                    // if failed and not existed, make sure to drop this View
                    try
                    {
                        _ = this.Provider.DropView( connection, maker.ViewName );
                    }
                    finally
                    {
                    }
                }
            }
        }

        /// <summary> Adds Views. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        private void AddViews()
        {
            using var connection = this.Provider.GetConnection();
            connection.Open();
            foreach ( KeyValuePair<string, ViewMaker> kvp in this.EnumeratedViews )
            {
                this.MakeView( connection, kvp.Value );
            }
        }

        #endregion

        #region " SCHEMA "

        /// <summary> Builds the schema. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        public void BuildSchema()
        {
            this.AddTables();
            this.AddViews();
        }

        /// <summary> Assert schema objects exist. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public void AssertSchemaObjectsExist()
        {
            this.AssertTablesExist();
            this.AssertViewsExist();
        }

        #endregion

    }

    /// <summary> A view maker. </summary>
    /// <remarks> David, 2020-07-04. </remarks>
    public class ViewMaker
    {

        /// <summary> Gets or sets the name of the view. </summary>
        /// <value> The name of the view. </value>
        public string ViewName { get; set; }

        /// <summary> Gets or sets the create action. </summary>
        /// <value> The create action. </value>
        public Func<System.Data.IDbConnection, string> CreateAction { get; set; }
    }
}

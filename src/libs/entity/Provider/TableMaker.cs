using System;
using System.Collections.Generic;

namespace isr.Dapper.Entity
{

    /// <summary>   A table maker. </summary>
    /// <remarks>   David, 2020-10-02. </remarks>
    public class TableMaker
    {

        /// <summary> Gets or sets the name of the table. </summary>
        /// <value> The name of the table. </value>
        public string TableName { get; set; }

        /// <summary> Gets or sets the category. </summary>
        /// <value> The category. </value>
        public TableCategory Category { get; set; }

        /// <summary> Gets or sets the create action. </summary>
        /// <value> The create action. </value>
        public Func<System.Data.IDbConnection, string> CreateAction { get; set; }

        /// <summary> Gets or sets the insert action. </summary>
        /// <value> The insert action. </value>
        public Func<System.Data.IDbConnection, int> InsertAction { get; set; }

        /// <summary> Gets or sets the insert file values action. </summary>
        /// <value> The insert file values action. </value>
        public Func<System.Data.IDbConnection, string, int> InsertFileValuesAction { get; set; }

        /// <summary> Gets or sets the filename of the values file. </summary>
        /// <value> The filename of the values file. </value>
        public string ValuesFileName { get; set; }

        /// <summary> Gets or sets the exists action. </summary>
        /// <value> The exists action. </value>
        public Func<System.Data.IDbConnection, int> ExistsAction { get; set; }

        /// <summary> Gets or sets the exists update action. </summary>
        /// <value> The exists update action. </value>
        public Func<System.Data.IDbConnection, string, bool> ExistsUpdateAction { get; set; }

        /// <summary> Gets or sets the exists update value. </summary>
        /// <value> The exists update value. </value>
        public string ExistsUpdateValue { get; set; }

        /// <summary> Gets or sets the exists build action. </summary>
        /// <value> The exists build action. </value>
        public Func<System.Data.IDbConnection, string> ExistsBuildAction { get; set; }

        /// <summary> Gets or sets the post create action. </summary>
        /// <value> The post create action. </value>
        public Func<System.Data.IDbConnection, string, bool> PostCreateAction { get; set; }

        /// <summary> Gets or sets the post create value. </summary>
        /// <value> The post create value. </value>
        public string PostCreateValue { get; set; }

        /// <summary> Gets or sets the insert values action. </summary>
        /// <value> The insert values action. </value>
        public Func<System.Data.IDbConnection, IEnumerable<string>, bool> InsertValuesAction { get; set; }

        /// <summary> Gets or sets the values. </summary>
        /// <value> The values. </value>
        public IEnumerable<string> Values { get; set; }

        /// <summary> Gets or sets the failed. </summary>
        /// <value> The failed. </value>
        public bool Failed { get; set; }

        /// <summary> Gets or sets the fetch all action. </summary>
        /// <value> The fetch all action. </value>
        public Action<System.Data.IDbConnection> FetchAllAction { get; set; }
    }

    /// <summary> A bit-field of flags for specifying a table category. </summary>
    /// <remarks> David, 2020-07-11. </remarks>
    [Flags]
    public enum TableCategory
    {

        /// <summary> An enum constant representing the none option. </summary>
        None = 0,

        /// <summary> An enum constant representing the lookup option. </summary>
        Lookup = 1 << 0,

        /// <summary> An enum constant representing the existed option. </summary>
        Existed = 1 << 1,

        /// <summary> An enum constant representing the created option. </summary>
        Created = 1 << 2
    }
}

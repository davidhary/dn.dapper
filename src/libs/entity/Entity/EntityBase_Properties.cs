using System;
using System.Collections.Generic;
using System.Linq;

using isr.Std.Primitives;

namespace isr.Dapper.Entity
{
    public partial class EntityBase<TIEntity, TEntityNub>
        where TIEntity : class
        where TEntityNub : class, TIEntity, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
    {

        /// <summary> Enumerates the entity field names in this collection. </summary>
        /// <remarks> David, 2020-05-14. </remarks>
        /// <param name="type"> The type. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entity field names in this
        /// collection.
        /// </returns>
        public static IList<string> EnumerateEntityFieldNames( Type type )
        {
            var l = new List<string>();
            foreach ( System.Reflection.PropertyInfo entityProperty in type.GetProperties() )
            {
                l.Add( entityProperty.Name );
            }

            return l;
        }

        /// <summary> Enumerates the entity field names in this collection. </summary>
        /// <remarks> David, 2020-05-14. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entity field names in this
        /// collection.
        /// </returns>
        public IList<string> EnumerateEntityFieldNames()
        {
            var l = new List<string>();
            foreach ( System.Reflection.PropertyInfo entityProperty in typeof( TEntityNub ).GetProperties() )
            {
                l.Add( entityProperty.Name );
            }

            return l;
        }

        /// <summary>
        /// Enumerates the entity property names excluding computer and key properties.
        /// </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entity property names in this
        /// collection.
        /// </returns>
        private IList<string> EnumerateEntityPropertyNames()
        {
            var l = new List<string>();
            foreach ( System.Reflection.PropertyInfo entityProperty in typeof( TEntityNub ).GetProperties() )
            {
                if ( !(global::Dapper.Contrib.Extensions.Methods.IsKeyProperty( entityProperty ) || global::Dapper.Contrib.Extensions.Methods.IsComputedProperty( entityProperty )) )
                {
                    // ignore key or computed properties
                    l.Add( entityProperty.Name );
                }
            }

            return l;
        }

        /// <summary> List of names of the entity properties. </summary>
        private List<string> _EntityPropertyNames;

        /// <summary> Enumerates entity property names in this collection. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process entity property names in this
        /// collection.
        /// </returns>
        public IList<string> EntityPropertyNames()
        {
            if ( !(this._EntityPropertyNames?.Any()) == true )
            {
                this._EntityPropertyNames = new List<string>( this.EnumerateEntityPropertyNames() );
            }

            return this._EntityPropertyNames;
        }

        /// <summary> Builds parenthesis entity property names. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <param name="prefix"> The prefix. </param>
        /// <returns> A String. </returns>
        private string BuildParenthesisEntityPropertyNames( string prefix )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( $"(" );
            foreach ( string name in this.EntityPropertyNames() )
            {
                if ( builder.Length > 1 )
                {
                    _ = builder.Append( "," );
                }

                _ = builder.Append( $"{prefix}{name}" );
            }

            _ = builder.Append( $")" );
            return builder.ToString();
        }

        /// <summary> List of names of the parenthesis at entity properties. </summary>
        private string _ParenthesisAtEntityPropertyNames;

        /// <summary> Parenthesis At Entity property names. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <returns> A String. </returns>
        public string ParenthesisAtEntityPropertyNames()
        {
            if ( string.IsNullOrEmpty( this._ParenthesisAtEntityPropertyNames ) )
            {
                this._ParenthesisAtEntityPropertyNames = this.BuildParenthesisEntityPropertyNames( "@" );
            }

            return this._ParenthesisAtEntityPropertyNames;
        }

        /// <summary> List of names of the parenthesis entity properties. </summary>
        private string _ParenthesisEntityPropertyNames;

        /// <summary> Parenthesis Entity property names. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <returns> A String. </returns>
        public string ParenthesisEntityPropertyNames()
        {
            if ( string.IsNullOrEmpty( this._ParenthesisEntityPropertyNames ) )
            {
                this._ParenthesisEntityPropertyNames = this.BuildParenthesisEntityPropertyNames( string.Empty );
            }

            return this._ParenthesisEntityPropertyNames;
        }
    }
}

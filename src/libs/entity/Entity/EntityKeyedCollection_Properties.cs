using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;

using isr.Std.Primitives;

namespace isr.Dapper.Entity
{
    public partial class EntityKeyedCollection<TKey, TIEntity, TEntityNub, TEntity>
        where TKey : struct
        where TIEntity : class
        where TEntityNub : class, TIEntity, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
        where TEntity : EntityBase<TIEntity, TEntityNub>
    {

        #region " PROPERTIES "

        /// <summary>
        /// Enumerates the entity property names excluding computer and key properties.
        /// </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entity property names in this
        /// collection.
        /// </returns>
        private IList<string> EnumerateEntityPropertyNames()
        {
            var l = new List<string>();
            foreach ( System.Reflection.PropertyInfo entityProperty in typeof( TEntityNub ).GetProperties() )
            {
                if ( !(global::Dapper.Contrib.Extensions.Methods.IsKeyProperty( entityProperty ) || global::Dapper.Contrib.Extensions.Methods.IsComputedProperty( entityProperty )) )
                {
                    // ignore key or computed properties
                    l.Add( entityProperty.Name );
                }
            }

            return l;
        }

        /// <summary> List of names of the entity properties. </summary>
        private List<string> _EntityPropertyNames;

        /// <summary> Enumerates entity property names in this collection. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process entity property names in this
        /// collection.
        /// </returns>
        public IList<string> EntityPropertyNames()
        {
            if ( !(this._EntityPropertyNames?.Any()) == true )
            {
                this._EntityPropertyNames = new List<string>( this.EnumerateEntityPropertyNames() );
            }

            return this._EntityPropertyNames;
        }

        /// <summary> Builds parenthesis entity property names. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <param name="prefix"> The prefix. </param>
        /// <returns> A String. </returns>
        private string BuildParenthesisEntityPropertyNames( string prefix )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( $"(" );
            foreach ( string name in this.EntityPropertyNames() )
            {
                if ( builder.Length > 1 )
                {
                    _ = builder.Append( "," );
                }

                _ = builder.Append( $"{prefix}{name}" );
            }

            _ = builder.Append( $")" );
            return builder.ToString();
        }

        /// <summary> List of names of the parenthesis at entity properties. </summary>
        private string _ParenthesisAtEntityPropertyNames;

        /// <summary> Parenthesis At Entity property names. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <returns> A String. </returns>
        public string ParenthesisAtEntityPropertyNames()
        {
            if ( string.IsNullOrEmpty( this._ParenthesisAtEntityPropertyNames ) )
            {
                this._ParenthesisAtEntityPropertyNames = this.BuildParenthesisEntityPropertyNames( "@" );
            }

            return this._ParenthesisAtEntityPropertyNames;
        }

        /// <summary> List of names of the parenthesis entity properties. </summary>
        private string _ParenthesisEntityPropertyNames;

        /// <summary> Parenthesis Entity property names. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <returns> A String. </returns>
        public string ParenthesisEntityPropertyNames()
        {
            if ( string.IsNullOrEmpty( this._ParenthesisEntityPropertyNames ) )
            {
                this._ParenthesisEntityPropertyNames = this.BuildParenthesisEntityPropertyNames( string.Empty );
            }

            return this._ParenthesisEntityPropertyNames;
        }

        #endregion

        #region " BULK OPERATIONS "

        /// <summary> Builds bulk insert command. </summary>
        /// <remarks>
        /// David, 2020-05-13.
        /// https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
        /// </remarks>
        /// <param name="tableName"> Name of the table. </param>
        /// <returns> A String. </returns>
        public string BuildBulkInsertCommand( string tableName )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $"Insert {tableName}{this.ParenthesisEntityPropertyNames()}" );
            _ = builder.AppendLine( $"Values {this.ParenthesisAtEntityPropertyNames()}" );
            return builder.ToString();
        }

        /// <summary> Bulk insert. </summary>
        /// <remarks>
        /// David, 2020-05-13.
        /// https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
        /// </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <param name="entities">   The entities. </param>
        public void BulkInsert( System.Data.IDbConnection connection, string tableName, IEnumerable<TIEntity> entities )
        {
            System.Data.IDbTransaction transaction = null;
            bool wasOpen = connection.IsOpen();
            try
            {
                if ( !wasOpen )
                {
                    connection.Open();
                }

                string command = this.BuildBulkInsertCommand( tableName );
                transaction = connection.BeginTransaction();
                _ = connection.Execute( command, entities, transaction );
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                if ( !wasOpen )
                {
                    connection.Close();
                }
            }
        }


        #endregion

    }
}

using System;

using isr.Std.Primitives;

namespace isr.Dapper.Entity
{

    /// <summary>
    /// Defines the contract that must be implemented by entity 'Nub' POCO Classes.
    /// </summary>
    /// <remarks>
    /// <typeparamref name="TIEntity"/> Specifies the generic interface type implemented by class
    /// <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-25, . from the legacy entity base. </para>
    /// </remarks>
    public abstract class EntityNubBase<TIEntity> : IEquatable<TIEntity>, ITypedCloneable<TIEntity> where TIEntity : class
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the EntityBase class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        protected EntityNubBase() : base()
        {
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public abstract bool Equals( TIEntity other );

        /// <summary> Determines whether the specified object is equal to the current object. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as TIEntity );
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public abstract override int GetHashCode();

        #endregion

        #region " I TYPED CLONEABLE "

        /// <summary>   Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-05-14. </remarks>
        /// <returns>   The new instance the entity 'Nub'. </returns>
        public abstract TIEntity CreateNew();

        /// <summary>   Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <returns>   The copy of the entity 'Nub'. </returns>
        public abstract TIEntity CreateCopy();

        /// <summary>   Copies the given entity into this class. </summary>
        /// <remarks>   David, 2020-04-27. </remarks>
        /// <param name="value">    The instance from which to copy. </param>
        public abstract void CopyFrom( TIEntity value );

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public object Clone()
        {
            return this.CreateCopy();
        }

        #endregion

    }
}

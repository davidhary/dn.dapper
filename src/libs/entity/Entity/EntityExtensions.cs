using System;
using System.Collections.Generic;

namespace isr.Dapper.Entity.EntityExtensions
{
    /// <summary>   <see cref="isr.Dapper.Entity.EntityExtensions"/> methods. </summary>
    /// <remarks>   David, 2020-10-02. </remarks>
    public static class Methods
    {

        /// <summary> Enumerate entity field names. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="type"> The type. </param>
        /// <returns> A list of. </returns>
        public static IList<string> EnumerateEntityFieldNames( this Type type )
        {
            var l = new List<string>();
            foreach ( System.Reflection.PropertyInfo entityProperty in type.GetProperties() )
            {
                l.Add( entityProperty.Name );
            }

            return l;
        }
    }
}

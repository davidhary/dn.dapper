using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Std.Primitives;

namespace isr.Dapper.Entity
{

    /// <summary>
    /// Defines the contract that must be implemented by a collection of
    /// <see cref="EntityBase{TIEntity, TEntityNub}"/> entities.
    /// </summary>
    /// <remarks>
    /// <typeparamref name="TEntityNub"/> Specifies the generic interface type implemented by the
    /// <typeparamref name="TEntityNub"/>. The latter also implements IEquatable(Of TIEntity),
    /// ITypedCloneable(Of TIEntity), Class.<para>
    /// Note that, because of dapper construction of the
    /// <see cref="SqlMapperExtensions.IProxy">proxy</see> for change tracking, having the interface
    /// inherit from other interfaces, such as IEquitable, causes the Dapper Get() function to fail
    /// on constructing the Proxy type.</para><para>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value.</para> <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public abstract partial class EntityKeyedCollection<TKey, TIEntity, TEntityNub, TEntity> : System.Collections.ObjectModel.KeyedCollection<TKey, TEntity>
            where TKey : struct
            where TIEntity : class
            where TEntityNub : class, TIEntity, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
            where TEntity : EntityBase<TIEntity, TEntityNub>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        protected EntityKeyedCollection() : base()
        {
            this._UnsavedKeys = new List<TKey>();
        }

        /// <summary>
        /// Cleans all entities marking them as
        /// <see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/> stored.
        /// </summary>
        /// <remarks> David, 2020-05-14. </remarks>
        public void Clean()
        {
            foreach ( TEntity entity in this )
            {
                _ = entity.UpdateStore();
            }
        }

        /// <summary> The unsaved keys. </summary>
        private readonly List<TKey> _UnsavedKeys;

        /// <summary> Gets the unsaved keys. </summary>
        /// <value> The unsaved keys. </value>
        public IList<TKey> UnsavedKeys => this._UnsavedKeys;

        /// <summary> Clears the unsaved keys. </summary>
        /// <remarks> David, 2020-06-19. </remarks>
        protected void ClearUnsavedKeys()
        {
            this._UnsavedKeys.Clear();
        }

        /// <summary> Adds an unsaved key. </summary>
        /// <remarks> David, 2020-06-19. </remarks>
        /// <param name="key"> The key. </param>
        protected void AddUnsavedKey( TKey key )
        {
            this._UnsavedKeys.Add( key );
        }

        /// <summary> Inserts or updates all entities using the given connection. </summary>
        /// <remarks>
        /// David, 2020-05-13. Entities that failed to save are enumerated in <see cref="UnsavedKeys"/>
        /// </remarks>
        /// <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        [CLSCompliant( false )]
        public virtual bool Upsert( TransactedConnection transcactedConnection )
        {
            this.ClearUnsavedKeys();
            // Dictionary is instantiated only after the collection has values.
            if ( !this.Any() )
            {
                return true;
            }

            foreach ( KeyValuePair<TKey, TEntity> keyValue in this.Dictionary )
            {
                if ( !keyValue.Value.Upsert( transcactedConnection ) )
                {
                    this.AddUnsavedKey( keyValue.Key );
                }
            }
            // success if no unsaved keys
            return !this.UnsavedKeys.Any();
        }

        /// <summary> Inserts or updates all entities using the given connection. </summary>
        /// <remarks>
        /// David, 2020-05-13. Entities that failed to save are enumerated in <see cref="UnsavedKeys"/>
        /// </remarks>
        /// <param name="connection"> The connection to save. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Upsert( System.Data.IDbConnection connection )
        {
            if ( this.Any() )
            {
                if ( connection is TransactedConnection transactedConnection )
                {
                    _ = this.Upsert( transactedConnection );
                }
                else
                {
                    bool wasOpen = connection.IsOpen();
                    try
                    {
                        if ( !wasOpen )
                        {
                            connection.Open();
                        }

                        using var transaction = connection.BeginTransaction();
                        try
                        {
                            _ = this.Upsert( new TransactedConnection( connection, transaction ) );
                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                            throw;
                        }
                        finally
                        {
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        if ( !wasOpen )
                        {
                            connection.Close();
                        }
                    }
                }
            }
            // success if no unsaved keys
            return !this.UnsavedKeys.Any();
        }

        /// <summary> Inserts or updates all entities using the given connection. </summary>
        /// <remarks>
        /// David, 2020-05-13. Entities that failed to save are enumerated in <see cref="UnsavedKeys"/>
        /// </remarks>
        /// <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
        /// <param name="force">                 True to force an update even if entity is not 'Dirty'. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        [CLSCompliant( false )]
        public virtual bool Upsert( TransactedConnection transcactedConnection, bool force )
        {
            // Dictionary is instantiated only after the collection has values.
            if ( !this.Any() )
            {
                return true;
            }

            this.ClearUnsavedKeys();
            foreach ( KeyValuePair<TKey, TEntity> keyValue in this.Dictionary )
            {
                if ( !keyValue.Value.Upsert( transcactedConnection, force ) )
                {
                    this.AddUnsavedKey( keyValue.Key );
                }
            }
            // success if no unsaved keys
            return !this.UnsavedKeys.Any();
        }

        /// <summary> Inserts or updates all entities using the given connection. </summary>
        /// <remarks>
        /// David, 2020-05-13. Entities that failed to save are enumerated in <see cref="UnsavedKeys"/>
        /// </remarks>
        /// <param name="connection"> The connection to save. </param>
        /// <param name="force">      True to force an update even if entity is not 'Dirty'. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Upsert( System.Data.IDbConnection connection, bool force )
        {
            if ( this.Any() )
            {
                if ( connection is TransactedConnection transactedConnection )
                {
                    _ = this.Upsert( transactedConnection, force );
                }
                else
                {
                    bool wasOpen = connection.IsOpen();
                    try
                    {
                        if ( !wasOpen )
                        {
                            connection.Open();
                        }

                        using var transaction = connection.BeginTransaction();
                        try
                        {
                            _ = this.Upsert( new TransactedConnection( connection, transaction ), force );
                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                            throw;
                        }
                        finally
                        {
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        if ( !wasOpen )
                        {
                            connection.Close();
                        }
                    }
                }
            }
            // success if no unsaved keys
            return !this.UnsavedKeys.Any();
        }

        /// <summary> Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks> David, 2020-05-14. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The number of affected records or the total records if none was affected. </returns>
        protected abstract int BulkUpsertThis( System.Data.IDbConnection connection );

        /// <summary> Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks> David, 2020-07-10. </remarks>
        /// <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
        /// <returns> The number of affected records or the total records if none was affected. </returns>
        [CLSCompliant( false )]
        public virtual int BulkUpsert( TransactedConnection transcactedConnection )
        {
            var count = default( int );
            if ( this.Any() )
            {
                this.ClearUnsavedKeys();
                count = this.BulkUpsertThis( transcactedConnection );
            }

            return count;
        }

        /// <summary> Inserts or updates all entities using the given connection and the . </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The number of affected records or the total records if none was affected. </returns>
        public int BulkUpsert( System.Data.IDbConnection connection )
        {
            var count = default( int );
            if ( this.Any() )
            {
                if ( connection is TransactedConnection transactedConnection )
                {
                    count = this.BulkUpsert( transactedConnection );
                }
                else
                {
                    bool wasOpen = connection.IsOpen();
                    try
                    {
                        if ( !wasOpen )
                        {
                            connection.Open();
                        }

                        using var transaction = connection.BeginTransaction();
                        try
                        {
                            count = this.BulkUpsert( new TransactedConnection( connection, transaction ) );
                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                            throw;
                        }
                        finally
                        {
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        if ( !wasOpen )
                        {
                            connection.Close();
                        }
                    }
                }
            }

            return count;
        }
    }
}

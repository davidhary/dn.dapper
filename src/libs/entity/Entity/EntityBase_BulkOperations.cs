using System;
using System.Collections.Generic;

using Dapper;

using isr.Std.Primitives;

namespace isr.Dapper.Entity
{
    public partial class EntityBase<TIEntity, TEntityNub>
        where TIEntity : class
        where TEntityNub : class, TIEntity, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
    {

        /// <summary> Builds bulk insert command. </summary>
        /// <remarks>
        /// David, 2020-05-13.
        /// https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
        /// </remarks>
        /// <param name="tableName"> Name of the table. </param>
        /// <returns> A String. </returns>
        public string BuildBulkInsertCommand( string tableName )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $"Insert {tableName}{this.ParenthesisEntityPropertyNames()}" );
            _ = builder.AppendLine( $"Values {this.ParenthesisAtEntityPropertyNames()}" );
            return builder.ToString();
        }

        /// <summary> Bulk insert. </summary>
        /// <remarks>
        /// David, 2020-05-13.
        /// https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
        /// </remarks>
        /// <param name="transcactedConnection"> The <see cref="T:Dapper.TransactedConnection" />. </param>
        /// <param name="tableName">             Name of the table. </param>
        /// <param name="entities">              The entities. </param>
        /// <returns> An Integer. </returns>
        [CLSCompliant( false )]
        public int BulkInsert( TransactedConnection transcactedConnection, string tableName, IList<TIEntity> entities )
        {
            string command = this.BuildBulkInsertCommand( tableName );
            return transcactedConnection.Execute( command, entities, transcactedConnection.Transaction );
        }

        /// <summary> Bulk insert. </summary>
        /// <remarks>
        /// David, 2020-05-13.
        /// https://stackoverflow.com/questions/10689779/bulk-inserts-taking-longer-than-expected-using-dapper/12609410#12609410.
        /// </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <param name="entities">   The entities. </param>
        /// <returns> An Integer. </returns>
        public int BulkInsert( System.Data.IDbConnection connection, string tableName, IList<TIEntity> entities )
        {
            int count;
            if ( connection is TransactedConnection transactedConnection )
            {
                count = this.BulkInsert( transactedConnection, tableName, entities );
            }
            else
            {
                bool wasOpen = connection.IsOpen();
                try
                {
                    if ( !wasOpen )
                    {
                        connection.Open();
                    }

                    using var transaction = connection.BeginTransaction();
                    try
                    {
                        count = this.BulkInsert( new TransactedConnection( connection, transaction ), tableName, entities );
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if ( !wasOpen )
                    {
                        connection.Close();
                    }
                }
            }

            return count;
        }

    }
}

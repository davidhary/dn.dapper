using System;
using System.ComponentModel;
using System.Threading;

using Dapper.Contrib.Extensions;

using isr.Std.Primitives;

namespace isr.Dapper.Entity
{

    /// <summary> Defines the contract that must be implemented by entities. </summary>
    /// <remarks>
    /// <typeparamref name="TEntityNub"/> Specifies the generic interface type implemented by the
    /// <typeparamref name="TEntityNub"/>. The latter also implements IEquatable(Of TIEntity),
    /// ITypedCloneable(Of TIEntity), Class.<para>
    /// Note that, because of dapper construction of the
    /// <see cref="SqlMapperExtensions.IProxy">proxy</see> for change tracking, having the interface
    /// inherit from other interfaces, such as IEquitable, causes the Dapper Get() function to fail
    /// on constructing the Proxy type.</para><para>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value.</para> <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-25, . from the legacy entity base. </para>
    /// </remarks>
    public abstract partial class EntityBase<TIEntity, TEntityNub> : INotifyPropertyChanged, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
            where TIEntity : class
            where TEntityNub : class, TIEntity, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the EntityBase class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        protected EntityBase() : base()
        {
        }

        /// <summary> Constructs an entity that was not yet stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="nub">   The entity nub to use for managing the store and the cache values. </param>
        /// <param name="cache"> The cached value. </param>
        // this tags the entity is new
        protected EntityBase( TEntityNub nub, TIEntity cache ) : this( nub, cache, null )
        {
        }

        /// <summary> Constructs an entity that is already stored. </summary>
        /// <remarks>
        /// The <paramref name="nub"/> is required because with native tracking, the
        /// <see cref="IsDirtyProxy"/> property is added to the cached interface, which can no longer be
        /// cast to the
        /// <typeparamref name="TEntityNub"/>.
        /// </remarks>
        /// <param name="nub">   The entity nub to use for managing the store and the cache values. </param>
        /// <param name="cache"> The cached value. </param>
        /// <param name="store"> The stored value. </param>
        protected EntityBase( TEntityNub nub, TIEntity cache, TIEntity store ) : this()
        {
            this.ICache = cache;
            // marks this as new
            this.IStore = store;
            this.Nub = nub;
        }

        /// <summary>   Specialized constructor for use only by derived class. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="entity">   The entity. </param>
        protected EntityBase( EntityBase<TIEntity, TEntityNub> entity ) : this( entity.Nub, entity.ICache, entity.IStore )
        {
        }

        #endregion

        #region " NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-04-26. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void NotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            PropertyChanged?.Invoke( this, e );
        }

        /// <summary> Synchronously (default) notifies field change on a different thread. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void NotifyFieldChanged( PropertyChangedEventArgs e )
        {
            this.NotifyPropertyChanged( e );
        }

        /// <summary> Synchronously (default) notifies Field change on a different thread. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="propertyName"> (Optional) Name of the runtime caller member. </param>
        protected void NotifyFieldChanged( [System.Runtime.CompilerServices.CallerMemberName()] string propertyName = null )
        {
            if ( !string.IsNullOrWhiteSpace( propertyName ) )
            {
                this.NotifyFieldChanged( new PropertyChangedEventArgs( propertyName ) );
            }
        }


        #endregion

        #region " PERSISTENCE and CHANGE TRACKING "

        /// <summary>   Gets or sets the proxy. </summary>
        /// <remarks>
        /// Change tracking works for Get and Update of interface types.<para>
        /// Dapper native change tracking does not work when fetching entities as classes. </para><para>
        /// When <see cref="UsingNativeTracking"/> is <c>True</c> this class stores and retrieves records
        /// as interfaces, otherwise, records are accessed using the <see cref="ICache"/> cast as
        /// <typeparamref name="TEntityNub"/> and update tracking is based on the class structures.
        /// Entity classes must be used for database access when the table name is specified in the class
        /// Table attribute and is different from the class interface name.</para>
        /// </remarks>
        /// <value> The proxy holding the dirty status of an entity. </value>
        private SqlMapperExtensions.IProxy IProxy { get; set; }

        /// <summary>
        /// Gets or sets an indication if the entity has changed since it was stored using the
        /// <see cref="IProxy"/>.
        /// </summary>
        /// <value> The indication that the entity has changed since it was stored. </value>
        protected bool IsDirtyProxy
        {
            get {
                this.IProxy = this.ICache as SqlMapperExtensions.IProxy;
                return this.IProxy is object && this.IProxy.IsDirty;
            }
            set {
                this.IProxy = this.ICache as SqlMapperExtensions.IProxy;
                if ( this.IProxy is object )
                {
                    this.IProxy.IsDirty = value;
                }
            }
        }

        /// <summary>
        /// Determines if the entity has changed since it was stored by comparing the
        /// <see cref="EntityBase{TIEntity, TEntityNub}.IStore">database value</see> <see cref="ICache">cache value</see> .
        /// </summary>
        /// <remarks> David, 2020-04-25. </remarks>
        /// <returns> True if dirty cache, false if not. </returns>
        public bool IsDirtyCache()
        {
            return !this.IsNew() && !this.Equals( this.IStore );
        }

        /// <summary> Determines if the entity has changed since it was stored. </summary>
        /// <remarks> David, 2020-04-25. </remarks>
        /// <returns> True if dirty, false if not. </returns>
        public bool IsDirty()
        {
            return this.IsDirtyProxy || this.IsDirtyCache();
        }

        /// <summary> Determines if the entity is new using the <see cref="IProxy"/>. </summary>
        /// <remarks> David, 2020-04-25. </remarks>
        /// <returns> True if new proxy, false if not. </returns>
        protected bool IsNewProxy()
        {
            this.IProxy = this.ICache as SqlMapperExtensions.IProxy;
            return this.IProxy is null;
        }

        /// <summary> Determines if the entity has a new store. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> <c>True</c>  if the entity store is new; Otherwise, <c>False</c>. </returns>
        protected bool IsNewStore()
        {
            return this.IStore is null;
        }

        /// <summary> Determines if the entity is new. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> <c>True</c>  if the entity is new; Otherwise, <c>False</c>. </returns>
        public bool IsNew()
        {
            return this.IsNewProxy() && this.IsNewStore();
        }

        /// <summary> Determines if the entity is not new or dirty. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> <c>True</c>  if the entity is not new or dirty; Otherwise, <c>False</c>. </returns>
        public bool IsClean()
        {
            return !(this.IsNew() || this.IsDirty());
        }

        /// <summary>   Persistence state. </summary>
        /// <remarks>   David, 2020-10-19. </remarks>
        /// <returns>   A PersistenceState. </returns>
        public PersistenceState PersistenceState()
        {
            return this.IsNew() ? Entity.PersistenceState.New : this.IsDirty() ? Entity.PersistenceState.Modified : Entity.PersistenceState.Clean;
        }

        #endregion

        #region " STORE AND CACHE "

        /// <summary> Updates the store, which holds a record of the stored fields. </summary>
        /// <remarks> David, 2020-05-09. </remarks>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
        /// </returns>
        public bool UpdateStore()
        {
            // must create a copy otherwise the IStore is the same as ICache
            this.Nub.CopyFrom( this.ICache );
            this.IStore = this.Nub.CreateCopy();
            return this.IsClean();
        }

        /// <summary> Clears the cache. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        protected void ClearCache()
        {
            this.UpdateCache( this.Nub.CreateNew() );
        }

        /// <summary> Clears the internal cache and store. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public virtual void Clear()
        {
            this.ClearCache();
            this.ClearStore();
        }

        /// <summary> Clears the store. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        protected void ClearStore()
        {
            this.IStore = null;
        }

        /// <summary> Updates the cache described by value. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="value"> The Part interface. </param>
        public virtual void UpdateCache( TIEntity value )
        {
            // the cache must be restored for keeping the proxy happy for detecting change.
            this.ICache = value;
        }

        /// <summary> Gets the entity as last stored in or fetched from the data store. </summary>
        /// <remarks>
        /// The dapper entity (see base class) dirty sentinel indicates if the entity changed.
        /// </remarks>
        /// <value> The i part. </value>
        private TIEntity IStore { get; set; }

        /// <summary> Gets the entity cache representing the current values of the entity. </summary>
        /// <remarks>
        /// The dapper entity (see base class) dirty sentinel indicates if the entity changed.
        /// </remarks>
        /// <value> The i cache. </value>
        protected TIEntity ICache { get; set; }

        /// <summary> Gets the nub; Must not be nulled. </summary>
        /// <remarks>
        /// The Nub was added to allow casting the <typeparamref name="TIEntity"/>
        /// to <typeparamref name="TEntityNub"/> with native tracking, which sets the
        /// <see cref="ICache"/> to <see cref="IProxy"/> adding the <see cref="IsDirtyProxy"/> field to
        /// the <see cref="ICache"/> thus breaking the interface implementation between to the type
        /// parameters.
        /// </remarks>
        /// <value> The nub. </value>
        private TEntityNub Nub { get; set; }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary> Gets or sets the using native tracking. </summary>
        /// <remarks>
        /// Allows using interface storage and retrieval. this assumes that the database table name
        /// matches the Interface name.
        /// </remarks>
        /// <value> The using native tracking. </value>
        public bool UsingNativeTracking { get; set; }

        /// <summary> Refetch; Fetch using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool FetchUsingKey( System.Data.IDbConnection connection );

        /// <summary>
        /// Updates the <see cref="ICache"/> and the <see cref="IStore"/> if <paramref name="entity"/> is
        /// not null.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="entity"> The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
        /// </returns>
        public bool Enstore( TIEntity entity )
        {
            if ( entity is object )
            {
                this.UpdateCache( entity );
                return this.UpdateStore();
            }
            else
            {
                this.ClearCache();
                this.ClearStore();
                return false;
            }
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool FetchUsingUniqueIndex( System.Data.IDbConnection connection );

        /// <summary>
        /// Inserts the entity as set in entity <see cref="ICache"/> using given connection thus
        /// preserving tracking. Must be overridden by entities with computed fields in order to fetch
        /// the stored entity.
        /// </summary>
        /// <remarks> David, 2020-04-25. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
        /// </returns>
        public virtual bool Insert( System.Data.IDbConnection connection )
        {
            if ( connection is not object )
            {
                throw new ArgumentNullException( nameof( connection ) );
            }

            this.ClearStore();

            // With an identity key, SQLite Insert return a numeric primary key; SQL server returns a numeric Identity key.
            // With a primary key, SQLite insert returns the row number; SQL Server insert returns zero, in which case, 
            // it is assumed the insert succeeded if an exception has not been thrown.
            _ = this.UsingNativeTracking ? connection.Insert( this.ICache ) : connection.Insert( this.ICache as TEntityNub );

            return this.UpdateStore();
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="ICache"/> using given connection thus
        /// preserving tracking. Must be overridden by entities with computed fields in order to fetch
        /// the stored entity.
        /// </summary>
        /// <remarks> David, 2020-05-21. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="entity">     The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
        /// </returns>
        public virtual bool Insert( System.Data.IDbConnection connection, TIEntity entity )
        {
            this.ICache = entity;
            return this.Insert( connection );
        }

        /// <summary>
        /// Obtains the entity using the unique index or unique key if not indexed; inserts if entity
        /// does not exist.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
        /// </returns>
        public virtual bool Obtain( System.Data.IDbConnection connection )
        {
            var temp = this.CreateCopy();
            return this.FetchUsingUniqueIndex( connection ) || this.Insert( connection, temp );
        }

        /// <summary> Updates or inserts the entity. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Upsert( System.Data.IDbConnection connection )
        {
            return this.IsNew() ? this.Obtain( connection ) : !this.IsDirty() || this.Update( connection );
        }

        /// <summary> Updates or inserts the entity optionally forcing an update. </summary>
        /// <remarks> David, 2020-05-13. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="force">      True to force an update overriding the proxy settings. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool Upsert( System.Data.IDbConnection connection, bool force )
        {
            // Return If(Me.IsNew, Me.Insert(connection), Me.Update(connection, force))
            return this.IsNew() ? this.Obtain( connection ) : this.Update( connection, force );
        }

        /// <summary> Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="entity">     The entity. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool Upsert( System.Data.IDbConnection connection, TIEntity entity );

        /// <summary> Deletes a record of this entity and clears the entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if the entity was deleted; otherwise <c>false</c> </returns>
        public bool Delete( System.Data.IDbConnection connection )
        {
            if ( this.IsClean() )
            {
                this.ClearStore();

                // if ( connection.Delete( this.Cache ) )
                if ( this.UsingNativeTracking ? connection.Delete( this.ICache ) : connection.Delete( this.ICache as TEntityNub ) )
                {
                    this.ClearCache();
                }

                return this.IsNew();
            }
            else
            {
                throw new InvalidOperationException( "Entity was not stored" );
            }
        }

        /// <summary> Updates this. </summary>
        /// <remarks> David, 2020-06-03. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        private bool UpdateThis( System.Data.IDbConnection connection )
        {
            return this.UsingNativeTracking ? connection.Update( this.ICache ) : this.IsDirty() && connection.Update( this.ICache as TEntityNub );
        }

        /// <summary> Updates the entity; refetches if the entity was updated. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
        /// </returns>
        public bool Update( System.Data.IDbConnection connection )
        {
            if ( this.UpdateThis( connection ) )
            {
                // at this time, Is Dirty gets cleared only after a refetch
                return this.FetchUsingKey( connection );
            }
            else
            {
                return this.IsClean();
            }
        }

        /// <summary> Updates the entity; refetches if the entity was updated. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="force">      True to force an update overriding the proxy settings. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="IsClean()"/>); otherwise <c>false</c>
        /// </returns>
        public bool Update( System.Data.IDbConnection connection, bool force )
        {
            bool updated;
            if ( this.UsingNativeTracking )
            {
                this.IsDirtyProxy = force;
                updated = connection.Update( this.ICache );
            }
            else
            {
                updated = force ? connection.Update( this.ICache as TEntityNub ) : this.UpdateThis( connection );
            }

            if ( updated )
            {
                // at this time, Is Dirty gets cleared only after a refetch
                return this.FetchUsingKey( connection );
            }
            else
            {
                return this.IsClean();
            }
        }

        /// <summary> Updates the entity. </summary>
        /// <remarks> David, 2020-05-09. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="updateMode"> The update mode. </param>
        /// <returns> <c>true</c> if the record was updated; otherwise <c>false</c> </returns>
        public bool Update( System.Data.IDbConnection connection, UpdateModes updateMode )
        {
            bool updated = false;
            if ( 0 == (UpdateModes.ForceUpdate & updateMode) )
            {
                updated = this.UsingNativeTracking ? connection.Update( this.ICache ) : this.IsDirty() && connection.Update( this.ICache as TEntityNub ); // this.Cache );
            }
            else if ( UpdateModes.ForceUpdate == (UpdateModes.ForceUpdate & updateMode) )
            {
                if ( this.UsingNativeTracking )
                {
                    this.IsDirtyProxy = true;
                    updated = connection.Update( this.ICache );
                }
                else
                {
                    updated = connection.Update( this.ICache as TEntityNub ); //  this.Cache );
                }
            }

            if ( updated && UpdateModes.Refetch == (UpdateModes.Refetch & updateMode) )
            {
                // at this time, Is Dirty gets cleared only after a refetch
                _ = this.FetchUsingKey( connection );
            }

            return updated;
        }

        #endregion

        #region " ENTITIES "

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> all. </returns>
        public abstract int FetchAllEntities( System.Data.IDbConnection connection );

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Indicates whether the current entity store is equal to another object store of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object store is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool StoreEquals( TIEntity other ) => this.Equals( this.IStore as TEntityNub, other );

        /// <summary>   Compares two entities. </summary>
        /// <remarks>   David, 2020-10-19. </remarks>
        /// <param name="nub">      The entity nub to use for managing the store and the cache values. </param>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>   <c>True</c> if the entities are equal. </returns>
        public bool Equals( TEntityNub nub, TIEntity other ) => other is object && nub is object && nub.Equals( other );

        /// <summary>
        /// Indicates whether the current entity Cache is equal to another object Cache of the same type.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns> <see langword="true" /> if the current object cache is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool CacheEquals( TIEntity other )
        {
            if ( this.UsingNativeTracking )
            {
                // Because native tracking copies the Proxy into the cache, casting the cache from 
                // interface to Nub returns nothing because the proxy adds the <see cref="IsDirtyProxy"/> 
                // field to <typeparamref name="TIEntity"/> when using native tracking,
                this.Nub.CopyFrom( this.ICache );
                return this.Nub.Equals( other );
            }
            else
            {
                return (this.ICache as TEntityNub).Equals( other );
            }
        }

        // Cache is derived from NUB we have had issues with this code when the cast was not explicit.
        // public bool CacheEquals( TIEntity other ) => other is object && this.ICache.Equals( other );

        /// <summary>
        /// Indicates whether the current entity Cache is equal to another object Cache of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public bool Equals( TIEntity other ) => this.CacheEquals( other );

        // Cache is derived from NUB we have had issues with this code when the cast was not explicit.
        // public bool Equals( TIEntity other ) => other is object && this.ICache.Equals( other );

        /// <summary> Compares two entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="left">  Specifies the entity to compare to. </param>
        /// <param name="right"> Specifies the entity to compare. </param>
        /// <returns> <c>True</c> if the entities are equal. </returns>
        public static new bool Equals( object left, object right )
        {
            return Equals( left as EntityBase<TIEntity, TEntityNub>, right as EntityBase<TIEntity, TEntityNub> );
        }

        /// <summary> Compares two entities. </summary>
        /// <remarks> The two entities are the same if they have the same X and Y coordinates. </remarks>
        /// <param name="left">  Specifies the entity to compare to. </param>
        /// <param name="right"> Specifies the entity to compare. </param>
        /// <returns> <c>True</c> if the entities are equal. </returns>
        public static bool Equals( EntityBase<TIEntity, TEntityNub> left, EntityBase<TIEntity, TEntityNub> right )
        {
            return left is object && right is object && left.Equals( right );
        }

        /// <summary>
        /// Determines whether the cached values of the specified <see cref="T:System.Object" /> is equal
        /// to the cached values of the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="obj">  The <see cref="T:System.Object" /> to compare with the current
        ///                     <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj ) => this.Equals( obj as TIEntity );

        /// <summary>
        /// Compares two entities using their cached
        /// <see cref="EntityBase{TIEntity, TEntityNub}.ICache"/> values and persistence
        /// <see cref="EntityBase{TIEntity, TEntityNub}.PersistenceState"/> states.
        /// </summary>
        /// <remarks>   The two entities are the same if they have the same X1 and Y1 coordinates. </remarks>
        /// <param name="other">    Specifies the other entity. </param>
        /// <returns>   <c>True</c> if the entities are equal. </returns>

        public bool EntityEquals( EntityBase<TIEntity, TEntityNub> other ) => other is object &&
                                                                              this.CacheEquals( other.ICache ) &&
                                                                              (this.PersistenceState() == other.PersistenceState());

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( EntityBase<TIEntity, TEntityNub> left, EntityBase<TIEntity, TEntityNub> right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( EntityBase<TIEntity, TEntityNub> left, EntityBase<TIEntity, TEntityNub> right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return this.ICache.GetHashCode();
        }

        #endregion

        #region " VALIDATE "

        /// <summary> Validates this entity as new. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="identifier"> The identifier. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ValidateNewEntity( string identifier )
        {
            (bool Success, string Details) result = (true, string.Empty);
            bool expectedOutcome;
            string outcomeName;
            outcomeName = nameof( isr.Dapper.Entity.EntityBase<TIEntity, TEntityNub>.IsNew );

            expectedOutcome = this.IsNew();
            if ( result.Success )
            {
                result = (expectedOutcome, expectedOutcome ? string.Empty : $"{identifier} should be tagged as '{outcomeName}'");
            }

            outcomeName = $"not {nameof( isr.Dapper.Entity.EntityBase<TIEntity, TEntityNub>.IsDirty )}";

            expectedOutcome = !this.IsDirty();
            if ( result.Success )
            {
                result = (expectedOutcome, expectedOutcome ? string.Empty : $"{identifier} should be tagged as '{outcomeName}'");
            }

            outcomeName = $"not {nameof( isr.Dapper.Entity.EntityBase<TIEntity, TEntityNub>.IsClean )}";

            expectedOutcome = !this.IsClean();
            if ( result.Success )
            {
                result = (expectedOutcome, expectedOutcome ? string.Empty : $"{identifier} should be tagged as '{outcomeName}'");
            }

            return result;
        }

        /// <summary> Validates this entity as stored. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="identifier"> The identifier. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ValidateStoredEntity( string identifier )
        {
            (bool Success, string Details) result = (true, string.Empty);
            bool expectedOutcome;
            string outcomeName;

            outcomeName = $"not {nameof( isr.Dapper.Entity.EntityBase<TIEntity, TEntityNub>.IsNew )}";
            expectedOutcome = !this.IsNew();
            if ( result.Success )
            {
                result = (expectedOutcome, expectedOutcome ? string.Empty : $"{identifier} should be tagged as '{outcomeName}'");
            }

            outcomeName = $"not {nameof( isr.Dapper.Entity.EntityBase<TIEntity, TEntityNub>.IsDirty )}";
            expectedOutcome = !this.IsDirty();
            if ( result.Success )
            {
                result = (expectedOutcome, expectedOutcome ? string.Empty : $"{identifier} should be tagged as '{outcomeName}'");
            }

            outcomeName = nameof( isr.Dapper.Entity.EntityBase<TIEntity, TEntityNub>.IsClean );
            expectedOutcome = this.IsClean();
            if ( result.Success )
            {
                result = (expectedOutcome, expectedOutcome ? string.Empty : $"{identifier} should be tagged as '{outcomeName}'");
            }

            return result;
        }

        /// <summary> Validates this entity as changed. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="identifier"> The identifier. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ValidateChangedEntity( string identifier )
        {
            (bool Success, string Details) result = (true, string.Empty);
            bool expectedOutcome;
            string outcomeName;

            outcomeName = $"not {nameof( isr.Dapper.Entity.EntityBase<TIEntity, TEntityNub>.IsNew )}";
            expectedOutcome = !this.IsNew();
            if ( result.Success )
            {
                result = (expectedOutcome, expectedOutcome ? string.Empty : $"{identifier} should be tagged as '{outcomeName}'");
            }

            outcomeName = nameof( isr.Dapper.Entity.EntityBase<TIEntity, TEntityNub>.IsDirty );
            expectedOutcome = this.IsDirty();
            if ( result.Success )
            {
                result = (expectedOutcome, expectedOutcome ? string.Empty : $"{identifier} should be tagged as '{outcomeName}'");
            }

            outcomeName = $"not {nameof( isr.Dapper.Entity.EntityBase<TIEntity, TEntityNub>.IsClean )}";
            expectedOutcome = !this.IsClean();
            if ( result.Success )
            {
                result = (expectedOutcome, expectedOutcome ? string.Empty : $"{identifier} should be tagged as '{outcomeName}'");
            }

            return result;
        }

        #endregion

        #region " I TYPED CLONEABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-05-14. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public abstract TIEntity CreateNew();

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public abstract TIEntity CreateCopy();

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public abstract void CopyFrom( TIEntity value );

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public object Clone()
        {
            return this.CreateCopy();
        }

        #endregion

    }

    /// <summary> A bit-field of flags for specifying update modes. </summary>
    /// <remarks> David, 2020-05-09. </remarks>
    [Flags]
    public enum UpdateModes
    {

        /// <summary> . </summary>
        [Description( "Update only" )]
        UpdateOnly = 0,

        /// <summary> . </summary>
        [Description( "Force Update" )]
        ForceUpdate = 1,

        /// <summary> . </summary>
        [Description( "Refetch after update" )]
        Refetch = 2
    }

    /// <summary>   Values that represent persistence states. </summary>
    /// <remarks>   David, 2020-10-19. </remarks>
    public enum PersistenceState
    {
        /// <summary>   An enum constant representing the new option. </summary>
        New,
        /// <summary>   An enum constant representing the clean option. </summary>
        Clean,
        /// <summary>   An enum constant representing the modified option. </summary>
        Modified
    }

}


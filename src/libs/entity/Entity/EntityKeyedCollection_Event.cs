using System;
using System.ComponentModel;
using System.Threading;

using isr.Std.Primitives;

namespace isr.Dapper.Entity
{
    public partial class EntityKeyedCollection<TKey, TIEntity, TEntityNub, TEntity> : INotifyPropertyChanged
        where TKey : struct
        where TIEntity : class
        where TEntityNub : class, TIEntity, IEquatable<TIEntity>, ITypedCloneable<TIEntity>
        where TEntity : EntityBase<TIEntity, TEntityNub>
    {

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.NotifyPropertyChanged( new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-04-26. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void NotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            PropertyChanged?.Invoke( this, e );
        }

        /// <summary> Synchronously (default) notifies field change on a different thread. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void NotifyFieldChanged( PropertyChangedEventArgs e )
        {
            this.NotifyPropertyChanged( e );
        }

        /// <summary> Synchronously (default) notifies Field change on a different thread. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="propertyName"> (Optional) Name of the runtime caller member. </param>
        protected void NotifyFieldChanged( [System.Runtime.CompilerServices.CallerMemberName()] string propertyName = null )
        {
            if ( !string.IsNullOrWhiteSpace( propertyName ) )
            {
                this.NotifyFieldChanged( new PropertyChangedEventArgs( propertyName ) );
            }
        }

    }
}

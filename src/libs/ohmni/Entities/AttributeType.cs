using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;
using isr.Dapper.Entity.EntityExtensions;

namespace isr.Dapper.Ohmni
{

    /// <summary>
    /// Interface for the Ohmni Attribute Type nub and entity. Includes the fields as kept in the
    /// data table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IOhmniAttributeType
    {

        /// <summary> Gets the identifier of the Attribute type. </summary>
        /// <value> The identifier of the Attribute type. </value>
        [ExplicitKey]
        int AttributeTypeId { get; set; }

        /// <summary> Gets the name. </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary> Gets the description. </summary>
        /// <value> The description. </value>
        string Description { get; set; }
    }

    /// <summary> The Ohmni Attribute Type builder. </summary>
    /// <remarks> David, 2020-04-24. </remarks>
    public sealed class OhmniAttributeTypeBuilder
    {

        /// <summary> Name of the table. </summary>
        private static string _TableName;

        /// <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( OhmniAttributeTypeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary>
        /// Inserts or ignores the values described by the <see cref="AttributeType"/> enumeration type.
        /// </summary>
        /// <remarks> David, 2020-05-14. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The number of inserted records or total number of records. </returns>
        public static int InsertIgnoreValues( System.Data.IDbConnection connection )
        {
            return InsertIgnoreValues( connection, typeof( AttributeType ), new int[] { ( int ) AttributeType.None } );
        }

        /// <summary> Inserts or ignores the values described by the enumeration type. </summary>
        /// <remarks> David, 2020-05-14. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="type">       The enumeration type. </param>
        /// <param name="excluded">   The excluded. </param>
        /// <returns> The number of inserted records or total number of records. </returns>
        public static int InsertIgnoreValues( System.Data.IDbConnection connection, Type type, IEnumerable<int> excluded )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : connection.InsertIgnoreNameDescriptionRecords( TableName, typeof( IOhmniAttributeType ).EnumerateEntityFieldNames(), type, excluded );
        }

        /// <summary> Creates a table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary> Creates table for SQLite database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}] (
                [{nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.AttributeTypeId )}] integer NOT NULL PRIMARY KEY, 
                [{nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.Name )}] nvarchar(50) NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.Description )}] nvarchar(250) NOT NULL); 
            CREATE UNIQUE INDEX IF NOT EXISTS [UQ_{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}] ON [{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.Name )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary> Creates table for SQL Server database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}](
                [{nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.AttributeTypeId )}] [int] NOT NULL,
                [{nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.Name )}] [nvarchar](50) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.Description )}] [nvarchar](250) NOT NULL,
             CONSTRAINT [PK_{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}] PRIMARY KEY CLUSTERED 
            ([{nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.AttributeTypeId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 
            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}]') AND name = N'UQ_{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}')
            CREATE UNIQUE NONCLUSTERED INDEX [UQ_{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}] ON [dbo].[{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.Name )}] ASC) 
             WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// Implements the AttributeType table <see cref="IOhmniAttributeType">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-22 </para>
    /// </remarks>
    [Table( "AttributeType" )]
    public class OhmniAttributeTypeNub : EntityNubBase<IOhmniAttributeType>, IOhmniAttributeType
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public OhmniAttributeTypeNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override IOhmniAttributeType CreateNew()
        {
            return new OhmniAttributeTypeNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override IOhmniAttributeType CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( IOhmniAttributeType value )
        {
            Copy( value, this );
        }

        /// <summary> Copies the given value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">      Another instance to copy. </param>
        /// <param name="destination"> Destination for the. </param>
        public static void Copy( IOhmniAttributeType source, IOhmniAttributeType destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.AttributeTypeId = source.AttributeTypeId;
            destination.Name = source.Name;
            destination.Description = source.Description;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary> Determines whether the specified object is equal to the current object. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <param name="other"> The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IOhmniAttributeType );
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IOhmniAttributeType other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary> Determines if entities are equal. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IOhmniAttributeType left, IOhmniAttributeType right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.AttributeTypeId, right.AttributeTypeId );
                result = result && string.Equals( left.Name, right.Name );
                result = result && string.Equals( left.Description, right.Description );
                return result;
            }
        }

        /// <summary> Serves as the default hash function. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.AttributeTypeId, this.Name, this.Description ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the Attribute type. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The identifier of the Attribute type. </value>
        [ExplicitKey]
        public int AttributeTypeId { get; set; }

        /// <summary> Gets or sets the name. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the description. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The description. </value>
        public string Description { get; set; }

        #endregion

    }

    /// <summary> The AttributeType Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-22 </para>
    /// </remarks>
    public class OhmniAttributeTypeEntity : EntityBase<IOhmniAttributeType, OhmniAttributeTypeNub>, IOhmniAttributeType
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public OhmniAttributeTypeEntity() : this( new OhmniAttributeTypeNub() )
        {
        }

        /// <summary> Constructs an entity that was not yet stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmniAttributeTypeEntity( IOhmniAttributeType value ) : this( value, null )
        {
        }

        /// <summary> Constructs an entity that is already stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="cache"> The cache. </param>
        /// <param name="store"> The store. </param>
        public OhmniAttributeTypeEntity( IOhmniAttributeType cache, IOhmniAttributeType store ) : base( new OhmniAttributeTypeNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmniAttributeTypeEntity( OhmniAttributeTypeEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName, nameof( isr.Dapper.Ohmni.IOhmniAttributeType ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override IOhmniAttributeType CreateNew()
        {
            return new OhmniAttributeTypeNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override IOhmniAttributeType CreateCopy()
        {
            var destination = this.CreateNew();
            OhmniAttributeTypeNub.Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( IOhmniAttributeType value )
        {
            OhmniAttributeTypeNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> the Meter Model interface. </param>
        public override void UpdateCache( IOhmniAttributeType value )
        {
            // first make the copy to notify of any property change.
            OhmniAttributeTypeNub.Copy( value, this );

            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary> Fetches using key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        the Meter Model table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<IOhmniAttributeType>( key ) : connection.Get<OhmniAttributeTypeNub>( key ) );
        }

        /// <summary> Refetch; Fetch using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.AttributeTypeId );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.Name );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-04-28. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="name">       Name of the Attribute Type. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string name )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, name ).SingleOrDefault();
            return nub is object && this.Enstore( nub );
        }

        /// <summary> Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="entity">     The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOhmniAttributeType entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.Name ) )
            {
                // update the existing record from the specified entity.
                entity.AttributeTypeId = this.AttributeTypeId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary> Deletes a record using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        The primary key. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new OhmniAttributeTypeNub() { AttributeTypeId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary> Gets or sets the Attribute Type entities. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Attribute Type entities. </value>
        public IEnumerable<OhmniAttributeTypeEntity> AttributeTypes { get; private set; }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">          The connection. </param>
        /// <param name="usingNativeTracking"> True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmniAttributeTypeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOhmniAttributeType>() ) : Populate( connection.GetAll<OhmniAttributeTypeNub>() );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.AttributeTypes = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.OhmniAttributeTypeEntity.AttributeTypes ) );
            return this.AttributeTypes?.Any() == true ? this.AttributeTypes.Count() : 0;
        }

        /// <summary> Populates a list of Attribute Type entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="nubs"> The entity nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmniAttributeTypeEntity> Populate( IEnumerable<OhmniAttributeTypeNub> nubs )
        {
            var l = new List<OhmniAttributeTypeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( OhmniAttributeTypeNub nub in nubs )
                    l.Add( new OhmniAttributeTypeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary> Populates a list of Attribute type entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="interfaces"> The entity interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmniAttributeTypeEntity> Populate( IEnumerable<IOhmniAttributeType> interfaces )
        {
            var l = new List<OhmniAttributeTypeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new OhmniAttributeTypeNub();
                foreach ( IOhmniAttributeType iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new OhmniAttributeTypeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary> Count entities; returns 1 or 0. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="name">       The <see cref="IOhmniAttributeType"/> name. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string name )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.Name )} = @name", new { name } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Fetches nubs; expects single entity or none. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="name">       The <see cref="IOhmniAttributeType"/> name. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmniAttributeTypeNub> FetchNubs( System.Data.IDbConnection connection, string name )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Ohmni.OhmniAttributeTypeBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.OhmniAttributeTypeNub.Name )} = @name", new { name } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<OhmniAttributeTypeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Determine if the AttributeType exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="name">       The <see cref="IOhmniAttributeType"/> name. </param>
        /// <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string name )
        {
            return 1 == CountEntities( connection, name );
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the Attribute type. </summary>
        /// <value> The identifier of the Attribute type. </value>
        public int AttributeTypeId
        {
            get => this.ICache.AttributeTypeId;

            set {
                if ( !object.Equals( ( object ) this.AttributeTypeId, ( object ) value ) )
                {
                    this.ICache.AttributeTypeId = value;
                    this.NotifyFieldChanged();
                    this.NotifyFieldChanged( nameof( isr.Dapper.Ohmni.OhmniAttributeTypeEntity.AttributeType ) );
                }
            }
        }

        /// <summary> Gets or sets the AttributeType. </summary>
        /// <value> The AttributeType. </value>
        public AttributeType AttributeType
        {
            get => ( AttributeType ) this.AttributeTypeId;

            set {
                if ( !object.Equals( this.AttributeType, value ) )
                {
                    this.AttributeTypeId = ( int ) value;
                }
            }
        }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name
        {
            get => this.ICache.Name;

            set {
                if ( !string.Equals( this.Name, value ) )
                {
                    this.ICache.Name = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the Description. </summary>
        /// <value> The Description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

    }

    /// <summary> Values that represent attribute types. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public enum AttributeType
    {

        /// <summary> . </summary>
        [Description( "Attribute Type" )]
        None = 0,

        /// <summary> . </summary>
        [Description( "Nominal Value" )]
        NominalValue = 1,

        /// <summary> . </summary>
        [Description( "Tolerance" )]
        Tolerance = 2,

        /// <summary> . </summary>
        [Description( "Tracking Tolerance" )]
        TrackingTolerance = 3,

        /// <summary> . </summary>
        [Description( "TCR" )]
        Tcr = 4,

        /// <summary> . </summary>
        [Description( "Tracking TCR" )]
        TrackingTcr = 5
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Ohmni
{

    /// <summary>
    /// Interface for the <see cref="OhmniPartAttributeRangeNub">Part Attribute Range nub</see> and
    /// <see cref="OhmniPartAttributeRangeEntity">entity</see>. Includes the fields as kept in the
    /// data table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value. <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IOhmniPartAttributeRange
    {

        /// <summary> Gets the identifier of the Part. </summary>
        /// <value> The identifier of the Part. </value>
        [ExplicitKey]
        int PartAutoId { get; set; }

        /// <summary> Gets the identifier of the Attribute type. </summary>
        /// <value> The identifier of the structure type. </value>
        [ExplicitKey]
        int AttributeTypeId { get; set; }

        /// <summary> Gets the minimum Value. </summary>
        /// <value> The minimum Value. </value>
        double Min { get; set; }

        /// <summary> Gets the maximum Value. </summary>
        /// <value> The minimum Value. </value>
        double Max { get; set; }
    }

    /// <summary> A part attribute range builder. </summary>
    /// <remarks> David, 2020-04-24. </remarks>
    public sealed class OhmniPartAttributeRangeBuilder
    {

        /// <summary> Name of the table. </summary>
        private static string _TableName;

        /// <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( OhmniPartAttributeRangeNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary> Gets or sets the name of the Attribute Type table. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="NotImplementedException">   Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The name of the AttributeType table. </value>
        public static string AttributeTypeTableName { get; set; } = OhmniAttributeTypeBuilder.TableName;

        /// <summary> Gets or sets the name of the Attribute Type table foreign key. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="NotImplementedException">   Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The name of the AttributeType table foreign key. </value>
        public static string AttributeTypeForeignKeyName { get; set; } = nameof( IOhmniAttributeType.AttributeTypeId );

        /// <summary> Creates a table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary> Creates table for SQLite database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}] (
            [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.PartAutoId )}] integer NOT NULL, 
            [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.AttributeTypeId )}] integer NOT NULL, 
            [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.Min )}] float NOT NULL, 
            [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.Max )}] float NOT NULL, 
            CONSTRAINT [sqlite_autoindex_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}_1] PRIMARY KEY ([{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.PartAutoId )}], [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.AttributeTypeId )}]),
            FOREIGN KEY ([{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.PartAutoId )}]) REFERENCES [{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )}]) ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.AttributeTypeId )}]) REFERENCES [{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeTableName}] ([{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeForeignKeyName}]) ON UPDATE CASCADE ON DELETE CASCADE);
            " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary> Creates table for SQL Server database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}](
                [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.PartAutoId )}] [int] NOT NULL,
                [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.AttributeTypeId )}] [int] NOT NULL,
                [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.Min )}] [float] NOT NULL,
                [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.Max )}] [float] NOT NULL,
             CONSTRAINT [PK_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}] PRIMARY KEY CLUSTERED 
            ([{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.PartAutoId )}] ASC, [{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.AttributeTypeId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END
            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}] WITH CHECK ADD CONSTRAINT [FK_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeTableName}] FOREIGN KEY([{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.AttributeTypeId )}])
            REFERENCES [dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeTableName}] ([{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeForeignKeyName}])
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeTableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeTableName}]

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}_{isr.Dapper.Ohmni.CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}_{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] FOREIGN KEY([{nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )}])
            REFERENCES [dbo].[{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )}])
            ON UPDATE CASCADE ON DELETE CASCADE 
            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}_{isr.Dapper.Ohmni.CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}_{isr.Dapper.Ohmni.CincoPartBuilder.TableName}]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// Implements the PartAttributeRange table <see cref="IOhmniPartAttributeRange">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-22 </para>
    /// </remarks>
    [Table( "PartAttributeRange" )]
    public class OhmniPartAttributeRangeNub : EntityNubBase<IOhmniPartAttributeRange>, IOhmniPartAttributeRange
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public OhmniPartAttributeRangeNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override IOhmniPartAttributeRange CreateNew()
        {
            return new OhmniPartAttributeRangeNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override IOhmniPartAttributeRange CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( IOhmniPartAttributeRange value )
        {
            Copy( value, this );
        }

        /// <summary> Copies the given value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">      Another instance to copy. </param>
        /// <param name="destination"> Destination for the. </param>
        public static void Copy( IOhmniPartAttributeRange source, IOhmniPartAttributeRange destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.AttributeTypeId = source.AttributeTypeId;
            destination.Max = source.Max;
            destination.Min = source.Min;
            destination.PartAutoId = source.PartAutoId;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary> Determines whether the specified object is equal to the current object. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <param name="other"> The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as IOhmniPartAttributeRange );
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( IOhmniPartAttributeRange other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary> Determines if entities are equal. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( IOhmniPartAttributeRange left, IOhmniPartAttributeRange right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.AttributeTypeId, right.AttributeTypeId );
                result = result && Equals( left.Max, right.Max );
                result = result && Equals( left.Min, right.Min );
                result = result && Equals( left.PartAutoId, right.PartAutoId );
                return result;
            }
        }

        /// <summary> Serves as the default hash function. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.AttributeTypeId, this.Max, this.Min, this.PartAutoId ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the Part. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <value> The identifier of the Part. </value>
        [ExplicitKey]
        public int PartAutoId { get; set; }

        /// <summary> Gets or sets the identifier of the Part Attribute type. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <value> The identifier of the Part Attribute type. </value>
        [ExplicitKey]
        public int AttributeTypeId { get; set; }

        /// <summary> Gets or sets the Part Attribute minimum value. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <value> The Part Attribute minimum value. </value>
        public double Min { get; set; }

        /// <summary> Gets or sets the Part Attribute Maximum value. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <value> The Part Attribute Maximum value. </value>
        public double Max { get; set; }

        #endregion

    }

    /// <summary>
    /// The PartAttributeRange Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-22 </para>
    /// </remarks>
    public class OhmniPartAttributeRangeEntity : EntityBase<IOhmniPartAttributeRange, OhmniPartAttributeRangeNub>, IOhmniPartAttributeRange
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public OhmniPartAttributeRangeEntity() : this( new OhmniPartAttributeRangeNub() )
        {
        }

        /// <summary> Constructs an entity that was not yet stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmniPartAttributeRangeEntity( IOhmniPartAttributeRange value ) : this( value, null )
        {
        }

        /// <summary> Constructs an entity that is already stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="cache"> The cache. </param>
        /// <param name="store"> The store. </param>
        public OhmniPartAttributeRangeEntity( IOhmniPartAttributeRange cache, IOhmniPartAttributeRange store ) : base( new OhmniPartAttributeRangeNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public OhmniPartAttributeRangeEntity( OhmniPartAttributeRangeEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName, nameof( isr.Dapper.Ohmni.IOhmniPartAttributeRange ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override IOhmniPartAttributeRange CreateNew()
        {
            return new OhmniPartAttributeRangeNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override IOhmniPartAttributeRange CreateCopy()
        {
            var destination = this.CreateNew();
            OhmniPartAttributeRangeNub.Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( IOhmniPartAttributeRange value )
        {
            OhmniPartAttributeRangeNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> the Meter Model interface. </param>
        public override void UpdateCache( IOhmniPartAttributeRange value )
        {
            // first make the copy to notify of any property change.
            OhmniPartAttributeRangeNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary> Refetch; Fetch using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">      The connection. </param>
        /// <param name="partAutoId">      The identifier of the Part. </param>
        /// <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int partAutoId, int attributeTypeId )
        {
            this.ClearStore();
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{OhmniPartAttributeRangeBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.PartAutoId )} = @Id", new { Id = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.AttributeTypeId )} = @TypeId", new { TypeId = attributeTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return this.Enstore( connection.QueryFirstOrDefault<OhmniPartAttributeRangeNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary> Refetch; Fetch using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PartAutoId, this.AttributeTypeId );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PartAutoId, this.AttributeTypeId );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">      The connection. </param>
        /// <param name="partAutoId">      The identifier of the Part. </param>
        /// <param name="attributeTypeId"> The Part Attribute value. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int partAutoId, int attributeTypeId )
        {
            this.ClearStore();
            var nub = FetchEntities( connection, partAutoId, attributeTypeId ).SingleOrDefault();
            return nub is object && this.Enstore( nub );
        }

        /// <summary> Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="entity">     The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, IOhmniPartAttributeRange entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.PartAutoId, entity.AttributeTypeId ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary> Deletes a record using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">      The connection. </param>
        /// <param name="partAutoId">      The identifier of the Part. </param>
        /// <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int partAutoId, int attributeTypeId )
        {
            return connection.Delete( new OhmniPartAttributeRangeNub() { PartAutoId = partAutoId, AttributeTypeId = attributeTypeId } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary> Gets or sets the PartAttributeRange entities. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The PartAttributeRange entities. </value>
        public IEnumerable<OhmniPartAttributeRangeEntity> PartAttributeRanges { get; private set; }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">          The connection. </param>
        /// <param name="usingNativeTracking"> True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmniPartAttributeRangeEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<IOhmniPartAttributeRange>() ) : Populate( connection.GetAll<OhmniPartAttributeRangeNub>() );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.PartAttributeRanges = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeEntity.PartAttributeRanges ) );
            return this.PartAttributeRanges?.Any() == true ? this.PartAttributeRanges.Count() : 0;
        }

        /// <summary> Count PartAttributeRanges. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="partAutoId"> The identifier of the Part. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.PartAutoId )} = @Id",
                                                                  new { Id = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="partAutoId"> The identifier of the Part. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<OhmniPartAttributeRangeEntity> FetchEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.PartAutoId )} = @Id",
                                            new { Id = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<OhmniPartAttributeRangeNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary> Populates a list of Part Attribute Range entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="nubs"> The Part Attribute Range nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmniPartAttributeRangeEntity> Populate( IEnumerable<OhmniPartAttributeRangeNub> nubs )
        {
            var l = new List<OhmniPartAttributeRangeEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( OhmniPartAttributeRangeNub nub in nubs )
                    l.Add( new OhmniPartAttributeRangeEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary> Populates a list of Part Attribute Range entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="interfaces"> The Part Attribute Range interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<OhmniPartAttributeRangeEntity> Populate( IEnumerable<IOhmniPartAttributeRange> interfaces )
        {
            var l = new List<OhmniPartAttributeRangeEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new OhmniPartAttributeRangeNub();
                foreach ( IOhmniPartAttributeRange iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new OhmniPartAttributeRangeEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary> Count Part Attributes; Returns 1 or 0. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">      The connection. </param>
        /// <param name="partAutoId">      The identifier of the Part. </param>
        /// <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId, int attributeTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{OhmniPartAttributeRangeBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeEntity.PartAutoId )} = @Id", new { Id = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeEntity.AttributeTypeId )} = @TypeId", new { TypeId = attributeTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary>
        /// Fetches Part Attributes by Part number and Part Attribute Part Attribute value;
        /// expected single or none.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">      The connection. </param>
        /// <param name="partAutoId">      The identifier of the Part. </param>
        /// <param name="attributeTypeId"> The identifier of the Part Attribute type. </param>
        /// <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
        public static IEnumerable<OhmniPartAttributeRangeNub> FetchEntities( System.Data.IDbConnection connection, int partAutoId, int attributeTypeId )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{OhmniPartAttributeRangeBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeEntity.PartAutoId )} = @Id", new { Id = partAutoId } );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeEntity.AttributeTypeId )} = @TypeId", new { TypeId = attributeTypeId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<OhmniPartAttributeRangeNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Determine if the PartAttributeRange exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">         The connection. </param>
        /// <param name="partAutoId">         The identifier of the Part. </param>
        /// <param name="partAttributeRange"> The part attribute range. </param>
        /// <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int partAutoId, int partAttributeRange )
        {
            return 1 == CountEntities( connection, partAutoId, partAttributeRange );
        }

        #endregion

        #region " RELATIONS "

        /// <summary> Gets or sets the Part entity. </summary>
        /// <value> The Part entity. </value>
        public CincoPartEntity PartEntity { get; private set; }

        /// <summary> Fetches a Part Entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchPartEntity( System.Data.IDbConnection connection )
        {
            this.PartEntity = new CincoPartEntity();
            return this.PartEntity.FetchUsingKey( connection, this.PartAutoId );
        }

        /// <summary> Gets or sets the AttributeType entity. </summary>
        /// <value> The AttributeType entity. </value>
        public OhmniAttributeTypeEntity AttributeTypeEntity { get; private set; }

        /// <summary> Fetches a AttributeType Entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchAttributeTypeEntity( System.Data.IDbConnection connection )
        {
            this.AttributeTypeEntity = new OhmniAttributeTypeEntity();
            return this.AttributeTypeEntity.FetchUsingKey( connection, this.AttributeTypeId );
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the Part. </summary>
        /// <value> The identifier of the Part. </value>
        public int PartAutoId
        {
            get => this.ICache.PartAutoId;

            set {
                if ( !object.Equals( this.PartAutoId, value ) )
                {
                    this.ICache.PartAutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the identifier of the Part Attribute type. </summary>
        /// <value> The identifier of the Part Attribute type. </value>
        public int AttributeTypeId
        {
            get => this.ICache.AttributeTypeId;

            set {
                if ( !object.Equals( this.AttributeTypeId, value ) )
                {
                    this.ICache.AttributeTypeId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the Part Attribute minimum value. </summary>
        /// <value> The Part Attribute minimum value. </value>
        public double Min
        {
            get => this.ICache.Min;

            set {
                if ( !object.Equals( this.Min, value ) )
                {
                    this.ICache.Min = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the Part Attribute Maximum value. </summary>
        /// <value> The Part Attribute Maximum value. </value>
        public double Max
        {
            get => this.ICache.Max;

            set {
                if ( !object.Equals( this.Max, value ) )
                {
                    this.ICache.Max = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " FIELDS: CUSTOM "

        /// <summary> Gets or sets the AttributeType. </summary>
        /// <value> The AttributeType. </value>
        public AttributeType AttributeType
        {
            get => ( AttributeType ) this.AttributeTypeId;

            set {
                if ( !object.Equals( this.AttributeTypeId, value ) )
                {
                    this.AttributeTypeId = ( int ) value;
                }
            }
        }

        /// <summary> Gets or sets the range. </summary>
        /// <value> The range. </value>
        public Std.Primitives.RangeR Range
        {
            get => new( this.Min, this.Max );

            set {
                if ( value is null )
                {
                    this.Min = 0d;
                    this.Max = 0d;
                }
                else
                {
                    this.Min = value.Min;
                    this.Max = value.Max;
                }
            }
        }

        #endregion

    }

    /// <summary> Collection of part attribute Ranges. </summary>
    /// <remarks> David, 2020-04-30. </remarks>
    public class OhmniPartAttributeRangeCollection : System.Collections.ObjectModel.KeyedCollection<int, OhmniPartAttributeRangeEntity>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns> The key for the specified element. </returns>
        protected override int GetKeyForItem( OhmniPartAttributeRangeEntity item )
        {
            return item is null ? throw new ArgumentNullException() : item.AttributeTypeId;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <param name="partAutoId"> Identifier for the part. </param>
        public OhmniPartAttributeRangeCollection( int partAutoId ) : base()
        {
            this.PartAutoId = partAutoId;
        }

        /// <summary> Gets or sets the identifier of the part automatic. </summary>
        /// <value> The identifier of the part automatic. </value>
        public int PartAutoId { get; private set; }

        /// <summary> Populates the given entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="entities"> The entities. </param>
        public void Populate( IEnumerable<OhmniPartAttributeRangeEntity> entities )
        {
            if ( entities?.Any() == true )
            {
                foreach ( OhmniPartAttributeRangeEntity entity in entities )
                    this.Add( entity );
            }
        }
    }
}


using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Ohmni
{

    /// <summary>
    /// Interface for the ReleaseHistory nub and entity. Includes the fields as kept in the data
    /// table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value. <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ICincoReleaseHistory
    {

        /// <summary> Gets the version number. </summary>
        /// <value> The version number. </value>
        [ExplicitKey]
        string VersionNumber { get; set; }

        /// <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time. <para>
        /// Computed attribute ignores the property on either insert or update.
        /// https://dapper-tutorial.net/knowledge-base/57673107/what-s-the-difference-between--computed--and--write-false---attributes-
        /// The <see cref="isr.Dapper.Ohmni.CincoReleaseHistoryEntity.UpdateLocalTimeStamp(System.Data.IDbConnection, DateTime)"/> is
        /// provided should the timestamp needs to be updated.         </para> <para>
        /// Note the SQLite stores time in the database in the time zone of the server. For example, if
        /// the server is located in LA, with a time zone of 7 hours relative to GMT (daylight saving
        /// time) storing 11:00:00AM using universal time value of 18:00:00Z, will store the time on the
        /// LA server as 11:00:00. To resolve these issues for both SQL Server the SQLite, time is
        /// converted to UTC and then to a date time format with no time zone specification (no Z).
        /// </para>
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        DateTime Timestamp { get; set; }

        /// <summary> Gets the is active. </summary>
        /// <value> The is active. </value>
        bool IsActive { get; set; }

        /// <summary> Gets the description. </summary>
        /// <value> The description. </value>
        string Description { get; set; }
    }

    /// <summary> A release history builder. </summary>
    /// <remarks> David, 2020-04-24. </remarks>
    public sealed class CincoReleaseHistoryBuilder
    {

        private static string _tableName;

        /// <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _tableName ) )
                {
                    _tableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( CincoReleaseHistoryNub ), typeof( TableAttribute ) )).Name;
                }

                return _tableName;
            }
        }

        /// <summary> Upsert revision. </summary>
        /// <remarks> David, 2020-03-25. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="recordData"> Comma separated values for the revision and the description. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool UpsertRevision( System.Data.IDbConnection connection, string recordData )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            if ( string.IsNullOrEmpty( recordData ) )
                throw new ArgumentNullException( nameof( recordData ) );
            var values = recordData.Split();
            if ( values.Count() == 1 )
                values = new string[] { values[0], "Unspecified release info" };
            ICincoReleaseHistory releaseHistory = new CincoReleaseHistoryNub() { VersionNumber = values[0], Description = values[1], IsActive = true };
            var entity = new CincoReleaseHistoryEntity();
            return entity.Upsert( connection, releaseHistory );
        }

        /// <summary> Clears the active bit of all records. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        public static void ClearActiveBit( System.Data.IDbConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"UPDATE [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] SET [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.IsActive )}] = 0; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
        }

        /// <summary> Gets or sets the date time format. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="NotImplementedException">   Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The date time format. </value>
        public static string DateTimeFormat { get; set; } = "yyyy-MM-dd HH:mm:ss.fff";

        /// <summary> Converts a value to a timestamp format. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <param name="value"> The value Date/Time. </param>
        /// <returns> Time value in the <see cref="DateTimeFormat"/> format. </returns>
        public static string ToTimestampFormat( DateTime value )
        {
            return value.ToString( DateTimeFormat );
        }

        /// <summary> Updates the timestamp. </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">         The connection. </param>
        /// <param name="key">                The key. </param>
        /// <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
        public static void UpdateTimestamp( System.Data.IDbConnection connection, string key, DateTime universalTimestamp )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"UPDATE [{TableName}] " );
            _ = queryBuilder.Append( @$"SET [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Timestamp )}] = '{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.ToTimestampFormat( universalTimestamp )}' " );
            _ = queryBuilder.Append( @$"WHERE [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}] = '{key}'; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
        }

        /// <summary> Updates the release history. </summary>
        /// <remarks> David, 2020-03-25. </remarks>
        /// <param name="connection">     The connection. </param>
        /// <param name="revisionRecord"> The revision record. </param>
        /// <returns> An Integer. </returns>
        public static bool UpdateReleaseHistory( System.Data.IDbConnection connection, string revisionRecord )
        {
            var values = revisionRecord.Split( ',' );
            return 0 <= UpdateReleaseHistory( connection, values[0], DateTime.UtcNow, values[1] );
        }

        /// <summary> Updates the release history. </summary>
        /// <remarks> David, 2020-05-02. </remarks>
        /// <param name="connection">         The connection. </param>
        /// <param name="versionNumber">      The version number. </param>
        /// <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
        /// <param name="description">        The description. </param>
        /// <returns> An Integer. </returns>
        public static int UpdateReleaseHistory( System.Data.IDbConnection connection, string versionNumber, DateTime universalTimestamp, string description )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? UpdateReleaseHistory( sql, versionNumber, universalTimestamp, description )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? UpdateReleaseHistory( sqlite, versionNumber, universalTimestamp, description ) : -1;
        }

        /// <summary> Updates the release history. </summary>
        /// <remarks> David, 2020-05-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">         The connection. </param>
        /// <param name="versionNumber">      The version number. </param>
        /// <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
        /// <param name="description">        The description. </param>
        /// <returns> An Integer. </returns>
        public static int UpdateReleaseHistory( System.Data.SQLite.SQLiteConnection connection, string versionNumber, DateTime universalTimestamp, string description )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( $"UPDATE [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] SET [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.IsActive )}] = 0; " );
            string dateTimeValue = ToTimestampFormat( universalTimestamp );
            _ = queryBuilder.Append( @$"INSERT OR IGNORE INTO [{TableName}] VALUES ('{versionNumber}','{dateTimeValue}','1','{description}'); " );
            return connection.ExecuteTransaction( queryBuilder.ToString().Clean() );
        }

        /// <summary> Updates the release history. </summary>
        /// <remarks> David, 2020-05-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">         The connection. </param>
        /// <param name="versionNumber">      The version number. </param>
        /// <param name="universalTimestamp"> The universal (UTC) timestamp. </param>
        /// <param name="description">        The description. </param>
        /// <returns> An Integer. </returns>
        public static int UpdateReleaseHistory( System.Data.SqlClient.SqlConnection connection, string versionNumber, DateTime universalTimestamp, string description )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"UPDATE [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] SET [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.IsActive )}] = 0;
            begin
                declare @rev as nvarchar(max) = '{versionNumber}'
                IF NOT EXISTS (SELECT * FROM [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] WHERE [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}]=@rev)
                    begin
                        INSERT INTO [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}],[{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Timestamp )}],[{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.IsActive )}],[{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Description )}])
                            VALUES (@rev,'{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.ToTimestampFormat( universalTimestamp )}',1,'{description}')
                    end
                IF EXISTS (SELECT * FROM [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] WHERE [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}]=@rev)
                    begin
                        UPDATE [dbo].[{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] SET [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.IsActive )}] = 1 WHERE [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}] = @rev
                    end 
            end;" );
            return connection.ExecuteTransaction( queryBuilder.ToString().Clean() );
        }

        /// <summary> Creates a table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary> Creates table for SQLite database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] (
                [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}] nvarchar(16) NOT NULL PRIMARY KEY, 
                [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Timestamp )}] datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP), 
                [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.IsActive )}] bit NOT NULL DEFAULT True, 
                [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Description )}] nvarchar(250) NOT NULL); 
            CREATE UNIQUE INDEX [UQ_{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}] ON [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}]); 
            CREATE UNIQUE INDEX [UQ_{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Timestamp )}] ON [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Timestamp )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary> Creates table for SQL Server database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] (
                [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}] nvarchar(16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL PRIMARY KEY CLUSTERED, 
                [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Timestamp )}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()), 
                [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.IsActive )}] bit NOT NULL DEFAULT ((1)), 
                [{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Description )}] nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
            ) ON [PRIMARY]; 

            CREATE UNIQUE NONCLUSTERED INDEX [UQ_{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}]
                ON [dbo].[{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.VersionNumber )}])
                WITH (PAD_INDEX=OFF
                ,STATISTICS_NORECOMPUTE=OFF
                ,IGNORE_DUP_KEY=OFF
                ,ALLOW_ROW_LOCKS=ON
                ,ALLOW_PAGE_LOCKS=ON) ON [PRIMARY];

            CREATE UNIQUE NONCLUSTERED INDEX [UQ_{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Timestamp )}]
                ON [dbo].[{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.Timestamp )}])
                WITH (PAD_INDEX=OFF
                ,STATISTICS_NORECOMPUTE=OFF
                ,IGNORE_DUP_KEY=OFF
                ,ALLOW_ROW_LOCKS=ON
                ,ALLOW_PAGE_LOCKS=ON) ON [PRIMARY]
            END; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// Implements the ReleaseHistory table <see cref="ICincoReleaseHistory">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-19 </para>
    /// </remarks>
    [Table( "ReleaseHistory" )]
    public class CincoReleaseHistoryNub : EntityNubBase<ICincoReleaseHistory>, ICincoReleaseHistory
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoReleaseHistoryNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoReleaseHistory CreateNew()
        {
            return new CincoReleaseHistoryNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoReleaseHistory CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoReleaseHistory value )
        {
            Copy( value, this );
        }

        /// <summary> Copies the given value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">      Another instance to copy. </param>
        /// <param name="destination"> Destination for the. </param>
        public static void Copy( ICincoReleaseHistory source, ICincoReleaseHistory destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Description = source.Description;
            destination.IsActive = source.IsActive;
            destination.VersionNumber = source.VersionNumber;
            destination.Timestamp = source.Timestamp;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary> Determines whether the specified object is equal to the current object. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <param name="other"> The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as ICincoReleaseHistory );
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( ICincoReleaseHistory other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary> Determines if entities are equal. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( ICincoReleaseHistory left, ICincoReleaseHistory right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && string.Equals( left.VersionNumber, right.VersionNumber );
                result = result && DateTime.Equals( left.Timestamp, right.Timestamp );
                result = result && string.Equals( left.Description, right.Description );
                result = result && Equals( left.IsActive, right.IsActive );
                return result;
            }
        }

        /// <summary> Serves as the default hash function. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.VersionNumber, this.Timestamp, this.Description, this.IsActive ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the version number. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The version number. </value>
        [ExplicitKey]
        public string VersionNumber { get; set; }

        /// <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time. <para>
        /// Computed attribute ignores the property on either insert or update.
        /// https://dapper-tutorial.net/knowledge-base/57673107/what-s-the-difference-between--computed--and--write-false---attributes-
        /// The <see cref="isr.Dapper.Ohmni.CincoReleaseHistoryEntity.UpdateLocalTimeStamp(System.Data.IDbConnection, DateTime)"/> is
        /// provided should the timestamp needs to be updated.         </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        public DateTime Timestamp { get; set; }

        /// <summary> Gets or sets the is active. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The is active. </value>
        public bool IsActive { get; set; }

        /// <summary> Gets or sets the description. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The description. </value>
        public string Description { get; set; }

        #endregion

    }

    /// <summary>
    /// The Release History Entity. Implements access to the database using Dapper.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-19 </para>
    /// </remarks>
    public class CincoReleaseHistoryEntity : EntityBase<ICincoReleaseHistory, CincoReleaseHistoryNub>, ICincoReleaseHistory
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoReleaseHistoryEntity() : this( new CincoReleaseHistoryNub() )
        {
        }

        /// <summary> Constructs an entity that was not yet stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoReleaseHistoryEntity( ICincoReleaseHistory value ) : this( value, null )
        {
        }

        /// <summary> Constructs an entity that is already stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="cache"> The cache. </param>
        /// <param name="store"> The store. </param>
        public CincoReleaseHistoryEntity( ICincoReleaseHistory cache, ICincoReleaseHistory store ) : base( new CincoReleaseHistoryNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoReleaseHistoryEntity( CincoReleaseHistoryEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName, nameof( isr.Dapper.Ohmni.ICincoReleaseHistory ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoReleaseHistory CreateNew()
        {
            return new CincoReleaseHistoryNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoReleaseHistory CreateCopy()
        {
            var destination = this.CreateNew();
            CincoReleaseHistoryNub.Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoReleaseHistory value )
        {
            CincoReleaseHistoryNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> the Meter Model interface. </param>
        public override void UpdateCache( ICincoReleaseHistory value )
        {
            // first make the copy to notify of any property change.
            CincoReleaseHistoryNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary> Fetches using key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        The Release History table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, string key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<ICincoReleaseHistory>( key ) : connection.Get<CincoReleaseHistoryNub>( key ) );
        }

        /// <summary> Refetch; Fetch using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.VersionNumber );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.VersionNumber );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="versionNumber"> The version number. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string versionNumber )
        {
            return this.FetchUsingKey( connection, versionNumber );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary> Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="entity">     The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ICincoReleaseHistory entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingKey( connection, entity.VersionNumber ) )
            {
                // update the existing record from the specified entity.
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary> Deletes a record using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        The primary key. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, string key )
        {
            return connection.Delete( new CincoReleaseHistoryNub() { VersionNumber = key } );
        }

        /// <summary> Updates the time stamp. </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="timestamp">  The Local timestamp. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool UpdateLocalTimeStamp( System.Data.IDbConnection connection, DateTime timestamp )
        {
            CincoReleaseHistoryBuilder.UpdateTimestamp( connection, this.VersionNumber, timestamp.ToUniversalTime() );
            return this.FetchUsingKey( connection );
        }

        /// <summary> Updates the time stamp. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="timestamp">  The UTC timestamp. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool UpdateTimeStamp( System.Data.IDbConnection connection, DateTime timestamp )
        {
            CincoReleaseHistoryBuilder.UpdateTimestamp( connection, this.VersionNumber, timestamp );
            return this.FetchUsingKey( connection );
        }

        #endregion

        #region " ENTITIES "

        /// <summary> Gets or sets the Attribute Type entities. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <value> The Attribute Type entities. </value>
        public IEnumerable<CincoReleaseHistoryEntity> ReleaseHistories { get; private set; }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">          The connection. </param>
        /// <param name="usingNativeTracking"> True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoReleaseHistoryEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ICincoReleaseHistory>() ) : Populate( connection.GetAll<CincoReleaseHistoryNub>() );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.ReleaseHistories = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoReleaseHistoryEntity.ReleaseHistories ) );
            return this.ReleaseHistories?.Any() == true ? this.ReleaseHistories.Count() : 0;
        }

        /// <summary> Populates a list of entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="nubs"> The entity nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoReleaseHistoryEntity> Populate( IEnumerable<CincoReleaseHistoryNub> nubs )
        {
            var l = new List<CincoReleaseHistoryEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( CincoReleaseHistoryNub nub in nubs )
                    l.Add( new CincoReleaseHistoryEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary> Populates a list of entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="interfaces"> The entity interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoReleaseHistoryEntity> Populate( IEnumerable<ICincoReleaseHistory> interfaces )
        {
            var l = new List<CincoReleaseHistoryEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new CincoReleaseHistoryNub();
                foreach ( ICincoReleaseHistory iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new CincoReleaseHistoryEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary> Count Release Histories by <see cref="IsActive"/>. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="isActive">   The is active. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, bool isActive )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                @$"SELECT COUNT(*) FROM [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.IsActive )} = @IsActive",
                                        new { isActive } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="isActive">   The is active. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoReleaseHistoryNub> FetchEntities( System.Data.IDbConnection connection, bool isActive )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Ohmni.CincoReleaseHistoryBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoReleaseHistoryNub.IsActive )} = @IsActive", new { isActive } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<CincoReleaseHistoryNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Determine if the ReleaseHistory exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="isActive">   The <see cref="CincoReleaseHistoryEntity"/> active state. </param>
        /// <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, bool isActive )
        {
            return 1 == CountEntities( connection, isActive );
        }

        #endregion

        #region " ACTIVE RELEASE "

        /// <summary> Fetches the active entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool FetchActive( System.Data.IDbConnection connection )
        {
            _ = FetchEntities( connection, true );
            var entities = FetchEntities( connection, true );
            if ( entities.Any() )
            {
                this.UpdateCache( entities.Single() );
                return this.FetchUsingKey( connection );
            }
            else
            {
                return false;
            }
        }

        /// <summary> Query if 'expectedVersion' is expected current release. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="expectedVersion"> The expected version. </param>
        /// <returns> <c>true</c> if expected current release; otherwise <c>false</c> </returns>
        public bool IsExpectedCurrentRelease( string expectedVersion )
        {
            return string.Equals( this.VersionNumber, expectedVersion, StringComparison.OrdinalIgnoreCase );
        }

        /// <summary> Returns True if the database version verifies. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">      The connection. </param>
        /// <param name="expectedVersion"> The expected version. </param>
        /// <returns> <c>True</c> if current release; <c>False</c> otherwise. </returns>
        public static (bool Success, string Details) TryVerifyDatabaseVersion( System.Data.IDbConnection connection, string expectedVersion )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            (bool Success, string Details) outcome = (true, string.Empty);
            try
            {
                var entity = new CincoReleaseHistoryEntity();
                if ( entity.FetchActive( connection ) )
                {
                    if ( !entity.IsExpectedCurrentRelease( expectedVersion ) )
                    {
                        outcome = (false, $"Active database version {entity.VersionNumber} does not match the program data version {expectedVersion}");
                    }
                }
                else
                {
                    outcome = (false, "Release history has no active records");
                }
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                outcome = (false, $"Exception fetching active release;. {ex}");
            }

            return outcome;
        }

        /// <summary> Fetches active release history version. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> The active release history version. </returns>
        public static Version FetchActiveReleaseHistoryVersion( System.Data.IDbConnection connection )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : new Version( FetchEntities( connection, true ).Single().VersionNumber );
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the version number. </summary>
        /// <value> The version number. </value>
        public string VersionNumber
        {
            get => this.ICache.VersionNumber;

            set {
                if ( !string.Equals( this.VersionNumber, value ) )
                {
                    this.ICache.VersionNumber = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the UTC timestamp. </summary>
        /// <remarks> Stored in universal time. </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTime.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the is active. </summary>
        /// <value> The is active. </value>
        public bool IsActive
        {
            get => this.ICache.IsActive;

            set {
                if ( !object.Equals( this.IsActive, value ) )
                {
                    this.ICache.IsActive = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the description. </summary>
        /// <value> The description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " FIELDS: CUSTOM "

        /// <summary> Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        private string _timestampCaptionFormat;

        /// <summary> Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._timestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._timestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.Timestamp.ToString( this.TimestampCaptionFormat );

        #endregion

    }
}

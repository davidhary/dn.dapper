
using System;

namespace isr.Dapper.Ohmni
{
    public partial class CincoSessionEntity
    {

        #region " BUILD "

        /// <summary> Inserts test level values described for <see cref="CincoTestLevel"/>. </summary>
        /// <remarks> David, 2020-03-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> An Integer. </returns>
        public static int InsertTestLevelValues( System.Data.IDbConnection connection )
        {
            return OhmniTestLevelBuilder.InsertIgnoreValues( connection, typeof( CincoTestLevel ), Array.Empty<int>() );
        }

        #endregion

        #region " SHARED "

        /// <summary>   Parse test level. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    True to show or False to hide the control. </param>
        /// <param name="default">  The default value. </param>
        /// <returns>   A TestLevel. </returns>
        public static CincoTestLevel ParseTestLevel( string value, CincoTestLevel @default )
        {
            if ( !Enum.TryParse( value, out CincoTestLevel tl ) )
                tl = @default;
            return tl;
        }

        #endregion

        #region " FIELDS: CUSTOM "

        /// <summary> Gets or sets the test level. </summary>
        /// <value> The test level. </value>
        public CincoTestLevel TestLevel
        {
            get => ( CincoTestLevel ) this.TestLevelId;

            set {
                if ( !object.Equals( this.TestLevel, value ) )
                {
                    this.TestLevelId = ( int ) value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

    }

    /// <summary> Values that represent Ohmni Cinco test levels. </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    [Flags]
    public enum CincoTestLevel
    {

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Normal" )]
        Normal = 0,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Four Point Probe" )]
        FourPointProbe = 1,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "TaN Voltage" )]
        Voltage = 2
    }
}

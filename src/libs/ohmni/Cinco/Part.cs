using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entities.ConnectionExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Ohmni
{

    /// <summary>
    /// Interface for the part nub and entity. Includes the part fields as kept in the part table.
    /// Allows tracking of property change.
    /// </summary>
    /// <remarks>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value. <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ICincoPart
    {

        /// <summary> Gets the identifier of the part. </summary>
        /// <value> The identifier of the part. </value>
        [Key]
        int PartAutoId { get; set; }

        /// <summary> Gets the part number. </summary>
        /// <value> The part number. </value>
        string PartNumber { get; set; }

        /// <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        DateTime Timestamp { get; set; }
    }

    /// <summary> A part builder. </summary>
    /// <remarks> David, 2020-04-24. </remarks>
    public sealed class CincoPartBuilder
    {

        /// <summary> Name of the table. </summary>
        private static string _TableName;

        /// <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( CincoPartNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary> Gets the name of the label index. </summary>
        /// <value> The name of the label index. </value>
        private static string LabelIndexName => $"UQ_{TableName}";

        /// <summary> Gets the name of the sq lite label index. </summary>
        /// <value> The name of the sq lite label index. </value>
        private static string SQLiteLabelIndexName => $"UQ_{isr.Dapper.Ohmni.CincoPartBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoPartNub.PartNumber )}";

        /// <summary> The using unique label. </summary>
        private static bool? _UsingUniqueLabel;

        /// <summary> Indicates if the entity uses a unique Label. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool UsingUniqueLabel( System.Data.IDbConnection connection )
        {
            if ( !_UsingUniqueLabel.HasValue )
            {
                _UsingUniqueLabel = connection as System.Data.SQLite.SQLiteConnection is object ? connection.IndexExists( SQLiteLabelIndexName ) : connection.IndexExists( LabelIndexName );
            }

            return _UsingUniqueLabel.Value;
        }

        /// <summary> Creates a table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary> Creates table for SQLite database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            ;
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] (
                [{nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
                [{nameof( isr.Dapper.Ohmni.CincoPartNub.PartNumber )}] nvarchar(50) NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoPartNub.Timestamp )}] datetime NOT NULL DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW'))); 
            CREATE UNIQUE INDEX IF NOT EXISTS [{isr.Dapper.Ohmni.CincoPartBuilder.SQLiteLabelIndexName}] ON [{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoPartNub.PartNumber )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary> Creates table for SQL Server database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Ohmni.CincoPartBuilder.TableName}') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Ohmni.CincoPartBuilder.TableName}](
                [{nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )}] [int] IDENTITY(1,1) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoPartNub.PartNumber )}] [nvarchar](50) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoPartNub.Timestamp )}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
             CONSTRAINT [PK_Part] PRIMARY KEY CLUSTERED 
            ([{nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )}] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]
            END; 
            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Ohmni.CincoPartBuilder.TableName}') AND name = N'{isr.Dapper.Ohmni.CincoPartBuilder.LabelIndexName}')
            CREATE UNIQUE NONCLUSTERED INDEX [{isr.Dapper.Ohmni.CincoPartBuilder.LabelIndexName}] ON [dbo].[{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoPartNub.PartNumber )}] ASC)
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
            ON [PRIMARY]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// The part table implementation based on the <see cref="ICincoPart">part interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-18 </para><para>
    /// David, 2018-09-17 </para>
    /// </remarks>
    [Table( "Part" )]
    public class CincoPartNub : EntityNubBase<ICincoPart>, ICincoPart
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoPartNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoPart CreateNew()
        {
            return new CincoPartNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoPart CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoPart value )
        {
            Copy( value, this );
        }

        /// <summary> Copies the given value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">      Another instance to copy. </param>
        /// <param name="destination"> Destination for the. </param>
        public static void Copy( ICincoPart source, ICincoPart destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.PartAutoId = source.PartAutoId;
            destination.PartNumber = source.PartNumber;
            destination.Timestamp = source.Timestamp;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary> Determines whether the specified object is equal to the current object. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <param name="other"> The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as ICincoPart );
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( ICincoPart other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary> Determines if entities are equal. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( ICincoPart left, ICincoPart right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.PartAutoId, right.PartAutoId );
                result = result && string.Equals( left.PartNumber, right.PartNumber );
                result = result && DateTime.Equals( left.Timestamp, right.Timestamp );
                return result;
            }
        }

        /// <summary> Serves as the default hash function. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.PartAutoId, this.PartNumber, this.Timestamp ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the part. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The identifier of the part. </value>
        [Key]
        public int PartAutoId { get; set; }

        /// <summary> Gets or sets the part number. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The part number. </value>
        public string PartNumber { get; set; }

        /// <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        public DateTime Timestamp { get; set; }

        #endregion

    }

    /// <summary> The part entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-14 </para>
    /// </remarks>
    public partial class CincoPartEntity : EntityBase<ICincoPart, CincoPartNub>, ICincoPart
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoPartEntity() : this( new CincoPartNub() )
        {
        }

        /// <summary>   Constructs an entity that was not yet stored. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoPartEntity( ICincoPart value ) : this( value, null )
        {
        }

        /// <summary> Constructs an entity that is already stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="cache"> The cache. </param>
        /// <param name="store"> The store. </param>
        public CincoPartEntity( ICincoPart cache, ICincoPart store ) : base( new CincoPartNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoPartEntity( CincoPartEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Ohmni.CincoPartBuilder.TableName, nameof( isr.Dapper.Ohmni.ICincoPart ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoPart CreateNew()
        {
            return new CincoPartNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoPart CreateCopy()
        {
            var destination = this.CreateNew();
            CincoPartNub.Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoPart value )
        {
            CincoPartNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> the Meter Model interface. </param>
        public override void UpdateCache( ICincoPart value )
        {
            // first make the copy to notify of any property change.
            CincoPartNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary> Fetches using key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        the Meter Model table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<ICincoPart>( key ) : connection.Get<CincoPartNub>( key ) );
        }

        /// <summary> Refetch; Fetch using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.PartAutoId );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.PartNumber );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-04-28. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string partNumber )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, partNumber ).SingleOrDefault();
            return nub is object && this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary> Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks> David, 2020-05-16. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="entity">     The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ICincoPart entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.PartNumber ) )
            {
                // update the existing record from the specified entity.
                entity.PartAutoId = this.PartAutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary> Deletes a record using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        The primary key. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new CincoPartNub() { PartAutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary> Gets or sets the Part entities. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Part entities. </value>
        public IEnumerable<CincoPartEntity> Parts { get; private set; }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">          The connection. </param>
        /// <param name="usingNativeTracking"> True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoPartEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ICincoPart>() ) : Populate( connection.GetAll<CincoPartNub>() );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Parts = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoPartEntity.Parts ) );
            return this.Parts?.Any() == true ? this.Parts.Count() : 0;
        }

        /// <summary> Attempts to fetch all entities. </summary>
        /// <remarks> David, 2020-05-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entities As IEnumerable(Of PartEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, IEnumerable<CincoPartEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new CincoPartEntity();
                activity = "fetching all Parts";
                // fetch all Parts
                _ = entity.FetchAllEntities( connection );
                return (true, string.Empty, entity.Parts);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", new List<CincoPartEntity>());
            }
        }

        /// <summary> Populates a list of entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="nubs"> The entity nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoPartEntity> Populate( IEnumerable<CincoPartNub> nubs )
        {
            var l = new List<CincoPartEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( CincoPartNub nub in nubs )
                    l.Add( new CincoPartEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary> Populates a list of entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="interfaces"> The entity interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoPartEntity> Populate( IEnumerable<ICincoPart> interfaces )
        {
            var l = new List<CincoPartEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new CincoPartNub();
                foreach ( ICincoPart iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new CincoPartEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND  "

        /// <summary> Count entities; returns 1 or 0. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string partNumber )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoPartNub.PartNumber )} = @partNumber", new { partNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Fetches nubs; expects single entity or none. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoPartNub> FetchNubs( System.Data.IDbConnection connection, string partNumber )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoPartNub.PartNumber )} = @partNumber", new { partNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<CincoPartNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Determine if the part exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string partNumber )
        {
            return 1 == CountEntities( connection, partNumber );
        }

        #endregion

        #region " RELATIONS: LOTS "

        /// <summary> Gets or sets the lot entities. </summary>
        /// <value> The lot entities. </value>
        public IEnumerable<CincoLotEntity> Lots { get; private set; }

        /// <summary> Fetches the lots. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The lots. </returns>
        public int FetchLots( System.Data.IDbConnection connection )
        {
            this.Lots = CincoLotEntity.FetchEntities( connection, this.PartAutoId );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoPartEntity.Lots ) );
            return this.Lots?.Any() == true ? this.Lots.Count() : 0;
        }

        /// <summary> Fetches the lots. </summary>
        /// <remarks>
        /// https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
        /// https://dapper-tutorial.net/querymultiple.
        /// </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="partAutoId"> The identifier of the part. </param>
        /// <returns> The lots. </returns>
        public int FetchLots( System.Data.IDbConnection connection, int partAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"select * from [{CincoPartBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $" where {nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )} = @Id; " );
            _ = queryBuilder.AppendLine( $"select * from [{CincoLotBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $" where {nameof( isr.Dapper.Ohmni.CincoLotNub.PartAutoId )} = @Id; " );
            var gridReader = connection.QueryMultiple( queryBuilder.ToString(), new { Id = partAutoId } );
            ICincoPart entity = gridReader.ReadSingle<CincoPartNub>();
            if ( entity is null )
            {
                this.Lots = new List<CincoLotEntity>();
            }
            else
            {
                if ( !CincoPartNub.AreEqual( entity, this.ICache ) )
                {
                    this.UpdateCache( entity );
                    _ = this.UpdateStore();
                }

                this.Lots = CincoLotEntity.Populate( gridReader.Read<CincoLotNub>() );
            }

            return this.Lots?.Any() == true ? this.Lots.Count() : 0;
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the part. </summary>
        /// <value> The identifier of the part. </value>
        public int PartAutoId
        {
            get => this.ICache.PartAutoId;

            set {
                if ( !object.Equals( this.PartAutoId, value ) )
                {
                    this.ICache.PartAutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the part number. </summary>
        /// <value> The part number. </value>
        public string PartNumber
        {
            get => this.ICache.PartNumber;

            set {
                if ( !string.Equals( this.PartNumber, value ) )
                {
                    this.ICache.PartNumber = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTime.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " FIELDS: CUSTOM "

        /// <summary> Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        /// <summary> The timestamp caption format. </summary>
        private string _TimestampCaptionFormat;

        /// <summary> Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._TimestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._TimestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.Timestamp.ToString( this.TimestampCaptionFormat );

        #endregion

    }
}

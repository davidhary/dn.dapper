using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Ohmni
{

    /// <summary>
    /// Interface for the Cinco session nub and entity. Includes the fields as kept in the data
    /// table. Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value. <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ICincoSession
    {

        /// <summary> Gets the identifier of the session. </summary>
        /// <value> The identifier of the session. </value>
        [Key]
        int SessionAutoId { get; set; }

        /// <summary> Gets the identifier of the station. </summary>
        /// <value> The identifier of the station. </value>
        int StationAutoId { get; set; }

        /// <summary> Gets the identifier of the test level. </summary>
        /// <value> The identifier of the test level. </value>
        int TestLevelId { get; set; }

        /// <summary> Gets the session number within the station. </summary>
        /// <value> The session  number. </value>
        int SessionNumber { get; set; }

        /// <summary> Gets the station name. </summary>
        /// <value> The station name. </value>
        string StationName { get; set; }

        /// <summary> Gets the description. </summary>
        /// <value> The description. </value>
        string Description { get; set; }

        /// <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        DateTime Timestamp { get; set; }
    }

    /// <summary> The Cinco session builder. </summary>
    /// <remarks> David, 2020-04-23. </remarks>
    public sealed class CincoSessionBuilder
    {

        /// <summary> Name of the table. </summary>
        private static string _TableName;

        /// <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( CincoSessionNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary> Gets or sets the indication of using a unique session number. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="NotImplementedException">   Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The using unique station session number. </value>
        public static bool UsingUniqueStationSessionNumber { get; private set; } = true;

        /// <summary> Creates a table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary> Creates table for SQLite database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] (
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionAutoId )}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )}] integer NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.TestLevelId )}] integer NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )}] integer NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationName )}] nvarchar(50) NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.Description )}] nvarchar(250) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.Timestamp )}] datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
            FOREIGN KEY ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )}]) REFERENCES [{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )}])	ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.TestLevelId )}])	REFERENCES [{isr.Dapper.Ohmni.OhmniTestLevelBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.TestLevelId )}]) ON UPDATE CASCADE ON DELETE CASCADE); 
            CREATE INDEX [UQ_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] ON [{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )}], [{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )}]); 
            CREATE INDEX [UQ_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] ON [{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationName )}], [{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary> Creates table for SQL Server database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}](
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionAutoId )}] [int] IDENTITY(1,1) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )}] [int] NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.TestLevelId )}] [int] NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )}] [int] NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationName )}] [nvarchar](50) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.Description )}] [nvarchar](250) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoSessionNub.Timestamp )}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
             CONSTRAINT [PK_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] PRIMARY KEY CLUSTERED 
            ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionAutoId )}] ASC)
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END; 
            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}]') AND name = N'UQ_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}')
            CREATE NONCLUSTERED INDEX [UQ_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] ON [dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )}] ASC, [{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )}] ASC) 
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]; 

            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}]') AND name = N'UQ_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}')
            CREATE NONCLUSTERED INDEX [UQ_Stationession] ON [dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationName )}] ASC, [{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )}] ASC) 
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] FOREIGN KEY([{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )}])
            REFERENCES [dbo].[{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )}])
            ON UPDATE CASCADE ON DELETE CASCADE; 

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}]; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}_{isr.Dapper.Ohmni.OhmniTestLevelBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}_{isr.Dapper.Ohmni.OhmniTestLevelBuilder.TableName}] FOREIGN KEY([{nameof( isr.Dapper.Ohmni.CincoSessionNub.TestLevelId )}])
            REFERENCES [dbo].[{isr.Dapper.Ohmni.OhmniTestLevelBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoSessionNub.TestLevelId )}])
            ON UPDATE CASCADE ON DELETE CASCADE; 

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}_{isr.Dapper.Ohmni.OhmniTestLevelBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}]'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}_{isr.Dapper.Ohmni.OhmniTestLevelBuilder.TableName}]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary>
    /// Implements the Cinco session table <see cref="ICincoSession">interface</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-19 </para>
    /// </remarks>
    [Table( "Session" )]
    public class CincoSessionNub : EntityNubBase<ICincoSession>, ICincoSession
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoSessionNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoSession CreateNew()
        {
            return new CincoSessionNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoSession CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoSession value )
        {
            Copy( value, this );
        }

        /// <summary> Copies the given value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">      Another instance to copy. </param>
        /// <param name="destination"> Destination for the. </param>
        public static void Copy( ICincoSession source, ICincoSession destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.Description = source.Description;
            destination.SessionAutoId = source.SessionAutoId;
            destination.SessionNumber = source.SessionNumber;
            destination.StationAutoId = source.StationAutoId;
            destination.StationName = source.StationName;
            destination.TestLevelId = source.TestLevelId;
            destination.Timestamp = source.Timestamp;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary> Determines whether the specified object is equal to the current object. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <param name="other"> The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as ICincoSession );
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( ICincoSession other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary> Determines if entities are equal. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( ICincoSession left, ICincoSession right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && string.Equals( left.Description, right.Description );
                result = result && Equals( left.SessionAutoId, right.SessionAutoId );
                result = result && Equals( left.SessionNumber, right.SessionNumber );
                result = result && Equals( left.StationAutoId, right.StationAutoId );
                result = result && string.Equals( left.StationName, right.StationName );
                result = result && Equals( left.TestLevelId, right.TestLevelId );
                result = result && DateTime.Equals( left.Timestamp, right.Timestamp );
                return result;
            }
        }

        /// <summary> Serves as the default hash function. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.Description, this.SessionAutoId, this.SessionNumber, this.StationAutoId, this.StationName, this.TestLevelId, this.Timestamp ).GetHashCode();
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the session. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The identifier of the session. </value>
        [Key]
        public int SessionAutoId { get; set; }

        /// <summary> Gets or sets the identifier of the station. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The identifier of the station. </value>
        public int StationAutoId { get; set; }

        /// <summary> Gets or sets the identifier of the test level. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The identifier of the test level. </value>
        public int TestLevelId { get; set; }

        /// <summary> Gets or sets the session number within the station. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The session number. </value>
        public int SessionNumber { get; set; }

        /// <summary> Gets or sets the station name. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The station name. </value>
        public string StationName { get; set; }

        /// <summary> Gets or sets the description. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The description. </value>
        public string Description { get; set; }

        /// <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        public DateTime Timestamp { get; set; }

        #endregion

    }

    /// <summary> The Session Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-19 </para>
    /// </remarks>
    public partial class CincoSessionEntity : EntityBase<ICincoSession, CincoSessionNub>, ICincoSession
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoSessionEntity() : this( new CincoSessionNub() )
        {
        }

        /// <summary> Constructs an entity that was not yet stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoSessionEntity( ICincoSession value ) : this( value, null )
        {
        }

        /// <summary> Constructs an entity that is already stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="cache"> The cache. </param>
        /// <param name="store"> The store. </param>
        public CincoSessionEntity( ICincoSession cache, ICincoSession store ) : base( new CincoSessionNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoSessionEntity( CincoSessionEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Ohmni.CincoSessionBuilder.TableName, nameof( isr.Dapper.Ohmni.ICincoSession ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoSession CreateNew()
        {
            return new CincoSessionNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoSession CreateCopy()
        {
            var destination = this.CreateNew();
            CincoSessionNub.Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoSession value )
        {
            CincoSessionNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> the Meter Model interface. </param>
        public override void UpdateCache( ICincoSession value )
        {
            // first make the copy to notify of any property change.
            CincoSessionNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary> Fetches using key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        The Session table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<ICincoSession>( key ) : connection.Get<CincoSessionNub>( key ) );
        }

        /// <summary> Refetch; Fetch using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.SessionAutoId );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.StationAutoId, this.SessionNumber );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="stationAutoId"> The Station auto id. </param>
        /// <param name="sessionNumber"> The session number. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, int stationAutoId, int sessionNumber )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, stationAutoId, sessionNumber ).SingleOrDefault();
            return nub is object && this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary> Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="entity">     The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ICincoSession entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            bool fetched = CincoSessionBuilder.UsingUniqueStationSessionNumber ? this.FetchUsingUniqueIndex( connection, entity.StationAutoId, entity.SessionNumber ) : this.FetchUsingKey( connection, entity.SessionAutoId );
            if ( fetched )
            {
                // update the existing record from the specified entity.
                entity.SessionAutoId = this.SessionAutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary> Deletes a record using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        The primary key. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new CincoSessionNub() { SessionAutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary> Gets or sets the Session entities. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Session entities. </value>
        public IEnumerable<CincoSessionEntity> Sessions { get; private set; }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">          The connection. </param>
        /// <param name="usingNativeTracking"> True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoSessionEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ICincoSession>() ) : Populate( connection.GetAll<CincoSessionNub>() );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Sessions = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoSessionEntity.Sessions ) );
            return this.Sessions?.Any() == true ? this.Sessions.Count() : 0;
        }

        /// <summary> Count Sessions. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">    The connection. </param>
        /// <param name="stationAutoId"> Identifier for the Station. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )} = @Id", new { Id = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">    The connection. </param>
        /// <param name="stationAutoId"> Identifier for the Station. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoSessionEntity> FetchEntities( System.Data.IDbConnection connection, int stationAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                @$"SELECT * FROM [{isr.Dapper.Ohmni.CincoSessionBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )} = @Id
ORDER BY {nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )}", new { Id = stationAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<CincoSessionNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary> Populates a list of Session entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="nubs"> The entity nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoSessionEntity> Populate( IEnumerable<CincoSessionNub> nubs )
        {
            var l = new List<CincoSessionEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( CincoSessionNub nub in nubs )
                    l.Add( new CincoSessionEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary> Populates a list of Session entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="interfaces"> The entity interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoSessionEntity> Populate( IEnumerable<ICincoSession> interfaces )
        {
            var l = new List<CincoSessionEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new CincoSessionNub();
                foreach ( ICincoSession iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new CincoSessionEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary> Count entities; returns 1 or 0. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">    The connection. </param>
        /// <param name="stationAutoId"> The Station auto id. </param>
        /// <param name="sessionNumber"> The Session number within this station. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int stationAutoId, int sessionNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{CincoSessionBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )} = @Id", new { Id = stationAutoId } );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )} = @SessionNumber", new { SessionNumber = sessionNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Count entities; returns 1 or 0. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="stationAutoId"> The Station auto id. </param>
        /// <param name="sessionNumber"> The Session number within this station. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntitiesAlternative( System.Data.IDbConnection connection, int stationAutoId, int sessionNumber )
        {
            var selectSql = new System.Text.StringBuilder( $"SELECT * FROM [{CincoSessionBuilder.TableName}]" );
            _ = selectSql.Append( $" WHERE (@{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )} IS NULL OR {nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )} = @Id)" );
            _ = selectSql.Append( $" AND (@{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )} IS NULL OR {nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )} =  @SessionNumber)" );
            _ = selectSql.Append( "; " );
            return connection.ExecuteScalar<int>( selectSql.ToString(), new { Id = stationAutoId, SessionNumber = sessionNumber } );
        }

        /// <summary> Fetches nubs; expects single entity or none. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">    The connection. </param>
        /// <param name="stationAutoId"> The Station auto id. </param>
        /// <param name="sessionNumber"> The Session number within this station. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoSessionNub> FetchNubs( System.Data.IDbConnection connection, int stationAutoId, int sessionNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{CincoSessionBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.CincoSessionNub.StationAutoId )} = @Id", new { Id = stationAutoId } );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.CincoSessionNub.SessionNumber )} = @SessionNumber", new { SessionNumber = sessionNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<CincoSessionNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Determine if the Session exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="stationAutoId"> The Station auto id. </param>
        /// <param name="sessionNumber"> The Session number within this station. </param>
        /// <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, int stationAutoId, int sessionNumber )
        {
            return 1 == CountEntities( connection, stationAutoId, sessionNumber );
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the session. </summary>
        /// <value> The identifier of the session. </value>
        public int SessionAutoId
        {
            get => this.ICache.SessionAutoId;

            set {
                if ( !object.Equals( this.SessionAutoId, value ) )
                {
                    this.ICache.SessionAutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the identifier of the station. </summary>
        /// <value> The identifier of the station. </value>
        public int StationAutoId
        {
            get => this.ICache.StationAutoId;

            set {
                if ( !object.Equals( this.StationAutoId, value ) )
                {
                    this.ICache.StationAutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the identifier of the test level. </summary>
        /// <value> The identifier of the test level. </value>
        public int TestLevelId
        {
            get => this.ICache.TestLevelId;

            set {
                if ( !object.Equals( this.TestLevelId, value ) )
                {
                    this.ICache.TestLevelId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the session number within the station. </summary>
        /// <value> The session number. </value>
        public int SessionNumber
        {
            get => this.ICache.SessionNumber;

            set {
                if ( !object.Equals( this.SessionNumber, value ) )
                {
                    this.ICache.SessionNumber = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the station name. </summary>
        /// <value> The station name. </value>
        public string StationName
        {
            get => this.ICache.StationName;

            set {
                if ( !string.Equals( this.StationName, value ) )
                {
                    this.ICache.StationName = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the description. </summary>
        /// <value> The description. </value>
        public string Description
        {
            get => this.ICache.Description;

            set {
                if ( !string.Equals( this.Description, value ) )
                {
                    this.ICache.Description = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTime.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " FIELDS: CUSTOM "

        /// <summary> Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        /// <summary> The timestamp caption format. </summary>
        private string _TimestampCaptionFormat;

        /// <summary> Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._TimestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._TimestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.Timestamp.ToString( this.TimestampCaptionFormat );

        /// <summary> Gets the session identity. </summary>
        /// <value> The session identity. </value>
        public string Identity => $"{this.StationName}:{this.SessionNumber}";

        #endregion

        #region " SHARED FUNCTIONS "

        /// <summary> Attempts to fetch the entity using the entity unique key. </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="sessionAutoId"> The entity unique key. </param>
        /// <returns> The (Success As Boolean, Details As String, Entity As TestSessionEntity) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, CincoSessionEntity Entity) TryFetchUsingKey( System.Data.IDbConnection connection, int sessionAutoId )
        {
            string activity = string.Empty;
            var entity = new CincoSessionEntity();
            bool success = true;
            string details = string.Empty;
            try
            {
                activity = $"Fetching session by id {sessionAutoId}";
                if ( !entity.FetchUsingKey( connection, sessionAutoId ) )
                {
                    success = false;
                    details = $"Failed {activity}";
                }
            }
            catch ( Exception ex )
            {
                success = false;
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                details = $"Exception {activity};. {ex}";
            }

            return (success, details, entity);
        }

        /// <summary> Try fetch using unique index. </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection">    The connection. </param>
        /// <param name="stationAutoId"> The Station auto id. </param>
        /// <param name="sessionNumber"> The session number. </param>
        /// <returns> The (Success As Boolean, Details As String, Entity As TestSessionEntity) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, CincoSessionEntity Entity) TryFetchUsingUniqueIndex( System.Data.IDbConnection connection, int stationAutoId, int sessionNumber )
        {
            string activity = string.Empty;
            var entity = new CincoSessionEntity();
            bool success = true;
            string details = string.Empty;
            try
            {
                activity = $"Fetching session by station id {stationAutoId} and session number {sessionNumber}";
                if ( !entity.FetchUsingUniqueIndex( connection, stationAutoId, sessionNumber ) )
                {
                    success = false;
                    details = $"Failed {activity}";
                }
            }
            catch ( Exception ex )
            {
                success = false;
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                details = $"Exception {activity};. {ex}";
            }

            return (success, details, entity);
        }

        /// <summary> Attempts to fetch all entities. </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Entities As IEnumerable(Of TestSessionEntity))
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details, IEnumerable<CincoSessionEntity> Entities) TryFetchAll( System.Data.IDbConnection connection )
        {
            string activity = string.Empty;
            try
            {
                var entity = new CincoSessionEntity();
                activity = "fetching all Sessions";
                // fetch all Sessions
                _ = entity.FetchAllEntities( connection );
                return (true, string.Empty, entity.Sessions);
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                return (false, $"Exception {activity};. {ex}", new List<CincoSessionEntity>());
            }
        }

        #endregion

    }
}

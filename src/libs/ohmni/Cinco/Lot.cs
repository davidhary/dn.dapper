using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Ohmni
{

    /// <summary>
    /// Interface for the Lot nub and entity. Includes the fields as kept in the data table. Allows
    /// tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value. <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ICincoLot
    {

        /// <summary> Gets the identifier of the lot. </summary>
        /// <value> The identifier of the lot. </value>
        [Key]
        int LotAutoId { get; set; }

        /// <summary> Gets the identifier of the part. </summary>
        /// <value> The identifier of the part. </value>
        int PartAutoId { get; set; }

        /// <summary> Gets the lot number. </summary>
        /// <value> The lot number. </value>
        string LotNumber { get; set; }

        /// <summary> Gets the part number. </summary>
        /// <value> The part number. </value>
        string PartNumber { get; set; }

        /// <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        DateTime Timestamp { get; set; }
    }

    /// <summary> A lot builder. </summary>
    /// <remarks> David, 2020-04-24. </remarks>
    public sealed class CincoLotBuilder
    {

        /// <summary> Name of the table. </summary>
        private static string _TableName;

        /// <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( CincoLotNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary> Creates a table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary> Creates table for SQLite database. </summary>
        /// <remarks>
        /// Uses the compact UNIX epoch time storage, which counts milliseconds relative to 1970-01-01.
        /// </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] (
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.LotAutoId )}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.PartAutoId )}] integer NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.LotNumber )}] nvarchar(50) NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.PartNumber )}] nvarchar(50) NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.Timestamp )}] datetime NOT NULL DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
            FOREIGN KEY ([{nameof( isr.Dapper.Ohmni.CincoLotNub.PartAutoId )}]) REFERENCES [{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoLotNub.PartAutoId )}]) ON UPDATE CASCADE ON DELETE CASCADE); 
            CREATE UNIQUE INDEX IF NOT EXISTS [UQ_{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] ON [{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoLotNub.LotNumber )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary> Creates table for SQL Server database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Ohmni.CincoLotBuilder.TableName}') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Ohmni.CincoLotBuilder.TableName}](
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.LotAutoId )}] [int] IDENTITY(1,1) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.PartAutoId )}] [int] NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.LotNumber )}] [nvarchar](50) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.PartNumber )}] [nvarchar](50) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoLotNub.Timestamp )}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
             CONSTRAINT [PK_{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] PRIMARY KEY CLUSTERED 
            ([{nameof( isr.Dapper.Ohmni.CincoLotNub.LotAutoId )}] ASC) 
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            ) ON [PRIMARY]
            END; 

            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Ohmni.CincoLotBuilder.TableName}') AND name = N'UQ_{isr.Dapper.Ohmni.CincoLotBuilder.TableName}')
            CREATE UNIQUE NONCLUSTERED INDEX [UQ_{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] ON [dbo].[{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoLotNub.LotNumber )}] ASC) 
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]; 

            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.CincoLotBuilder.TableName}_{isr.Dapper.Ohmni.CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Ohmni.CincoLotBuilder.TableName}'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.CincoLotBuilder.TableName}]  WITH CHECK ADD  CONSTRAINT [FK_{isr.Dapper.Ohmni.CincoLotBuilder.TableName}_{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] FOREIGN KEY([{nameof( isr.Dapper.Ohmni.CincoLotNub.PartAutoId )}])
            REFERENCES [dbo].[{isr.Dapper.Ohmni.CincoPartBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )}])
            ON UPDATE CASCADE ON DELETE CASCADE; 

            IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_{isr.Dapper.Ohmni.CincoLotBuilder.TableName}_{isr.Dapper.Ohmni.CincoPartBuilder.TableName}]') AND parent_object_id = OBJECT_ID(N'[dbo].{isr.Dapper.Ohmni.CincoLotBuilder.TableName}'))
            ALTER TABLE [dbo].[{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] CHECK CONSTRAINT [FK_{isr.Dapper.Ohmni.CincoLotBuilder.TableName}_{isr.Dapper.Ohmni.CincoPartBuilder.TableName}]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary> Implements the Lot table <see cref="ICincoLot">interface</see>. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-18 </para>
    /// </remarks>
    [Table( "Lot" )]
    public class CincoLotNub : EntityNubBase<ICincoLot>, ICincoLot
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoLotNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoLot CreateNew()
        {
            return new CincoLotNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoLot CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoLot value )
        {
            Copy( value, this );
        }

        /// <summary> Copies the given value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">      Another instance to copy. </param>
        /// <param name="destination"> Destination for the. </param>
        public static void Copy( ICincoLot source, ICincoLot destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.LotAutoId = source.LotAutoId;
            destination.LotNumber = source.LotNumber;
            destination.PartAutoId = source.PartAutoId;
            destination.PartNumber = source.PartNumber;
            destination.Timestamp = source.Timestamp;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary> Determines whether the specified object is equal to the current object. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <param name="other"> The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as ICincoLot );
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( ICincoLot other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary> Determines if entities are equal. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( ICincoLot left, ICincoLot right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && Equals( left.LotAutoId, right.LotAutoId );
                result = result && string.Equals( left.LotNumber, right.LotNumber );
                result = result && Equals( left.PartAutoId, right.PartAutoId );
                result = result && string.Equals( left.PartNumber, right.PartNumber );
                result = result && DateTime.Equals( left.Timestamp, right.Timestamp );
                return result;
            }
        }

        /// <summary> Serves as the default hash function. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.LotAutoId, this.LotNumber, this.PartAutoId, this.PartNumber, this.Timestamp ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the lot. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The identifier of the lot. </value>
        [Key]
        public int LotAutoId { get; set; }

        /// <summary> Gets or sets the identifier of the part. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The identifier of the part. </value>
        public int PartAutoId { get; set; }

        /// <summary> Gets or sets the lot number. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The lot number. </value>
        public string LotNumber { get; set; }

        /// <summary> Gets or sets the part number. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The part number. </value>
        public string PartNumber { get; set; }

        /// <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        public DateTime Timestamp { get; set; }

        #endregion

    }

    /// <summary> The Lot Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-18 </para>
    /// </remarks>
    public class CincoLotEntity : EntityBase<ICincoLot, CincoLotNub>, ICincoLot
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoLotEntity() : this( new CincoLotNub() )
        {
        }

        /// <summary> Constructs an entity that was not yet stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoLotEntity( ICincoLot value ) : this( value, null )
        {
        }

        /// <summary> Constructs an entity that is already stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="cache"> The cache. </param>
        /// <param name="store"> The store. </param>
        public CincoLotEntity( ICincoLot cache, ICincoLot store ) : base( new CincoLotNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoLotEntity( CincoLotEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Ohmni.CincoLotBuilder.TableName, nameof( isr.Dapper.Ohmni.ICincoLot ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoLot CreateNew()
        {
            return new CincoLotNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoLot CreateCopy()
        {
            var destination = this.CreateNew();
            CincoLotNub.Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoLot value )
        {
            CincoLotNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> the Meter Model interface. </param>
        public override void UpdateCache( ICincoLot value )
        {
            // first make the copy to notify of any property change.
            CincoLotNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary> Fetches using key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        the Meter Model table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<ICincoLot>( key ) : connection.Get<CincoLotNub>( key ) );
        }

        /// <summary> Refetch; Fetch using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.LotAutoId );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.LotNumber );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-04-28. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="lotNumber">  The Lot number. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string lotNumber )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, lotNumber ).SingleOrDefault();
            return nub is object && this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary> Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="entity">     The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ICincoLot entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            if ( this.FetchUsingUniqueIndex( connection, entity.LotNumber ) )
            {
                // update the existing record from the specified entity.
                entity.LotAutoId = this.LotAutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                // insert a new record based on the specified entity values.
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary> Deletes a record using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        The primary key. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new CincoLotNub() { LotAutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary> Gets or sets the Lot entities. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Lot entities. </value>
        public IEnumerable<CincoLotEntity> Lots { get; private set; }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">          The connection. </param>
        /// <param name="usingNativeTracking"> True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoLotEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ICincoLot>() ) : Populate( connection.GetAll<CincoLotNub>() );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Lots = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoLotEntity.Lots ) );
            return this.Lots?.Any() == true ? this.Lots.Count() : 0;
        }

        /// <summary> Populates a list of entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="nubs"> The entity nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoLotEntity> Populate( IEnumerable<CincoLotNub> nubs )
        {
            var l = new List<CincoLotEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( CincoLotNub nub in nubs )
                    l.Add( new CincoLotEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary> Populates a list of entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="interfaces"> The entity interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoLotEntity> Populate( IEnumerable<ICincoLot> interfaces )
        {
            var l = new List<CincoLotEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new CincoLotNub();
                foreach ( ICincoLot iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new CincoLotEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        /// <summary> Count lots. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="partAutoId"> Identifier for the part. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoLotNub.PartAutoId )} = @Id", new { Id = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Count lots. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountLots( System.Data.IDbConnection connection, string partNumber )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT COUNT(*) FROM [{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoLotNub.PartNumber )} = @partNumber", new { partNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="partAutoId"> Identifier for the part. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoLotEntity> FetchEntities( System.Data.IDbConnection connection, int partAutoId )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoLotNub.PartAutoId )} = @Id", new { Id = partAutoId } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<CincoLotNub>( selector.RawSql, selector.Parameters ) );
        }

        /// <summary> Fetches the entities for the specified part number. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoLotEntity> FetchEntities( System.Data.IDbConnection connection, string partNumber )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Ohmni.CincoLotBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoLotNub.PartNumber )} = @partNumber", new { partNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return Populate( connection.Query<CincoLotNub>( selector.RawSql, selector.Parameters ) );
        }

        #endregion

        #region " FIND "

        /// <summary> Count entities; returns 1 or 0. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="lotNumber">  The lot number. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string lotNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select count(*) from [{CincoLotBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.CincoLotNub.LotNumber )} = @lotNumber", new { lotNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Count entities; returns 1 or 0. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="lotNumber">  The lot number. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntitiesAlternative( System.Data.IDbConnection connection, string lotNumber )
        {
            var selectSql = new System.Text.StringBuilder( $"SELECT * FROM [{CincoLotBuilder.TableName}]" );
            _ = selectSql.Append( $"WHERE (@{nameof( isr.Dapper.Ohmni.CincoLotNub.LotNumber )} IS NULL OR {nameof( isr.Dapper.Ohmni.CincoLotNub.LotNumber )} = @LotNumber)" );
            _ = selectSql.Append( "; " );
            return connection.ExecuteScalar<int>( selectSql.ToString(), new { lotNumber } );
        }

        /// <summary> Fetches nubs; expects single entity or none. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="lotNumber">  The lot number. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoLotNub> FetchNubs( System.Data.IDbConnection connection, string lotNumber )
        {
            var sqlBuilder = new SqlBuilder();
            var selector = sqlBuilder.AddTemplate( @$"select * from [{CincoLotBuilder.TableName}] /**where**/" );
            _ = sqlBuilder.Where( $"{nameof( isr.Dapper.Ohmni.CincoLotNub.LotNumber )} = @lotNumber", new { lotNumber } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<CincoLotNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Determine if the lot exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="lotNumber">  The lot number. </param>
        /// <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string lotNumber )
        {
            return 1 == CountEntities( connection, lotNumber );
        }

        #endregion

        #region " RELATIONS "

        /// <summary> Gets or sets the part entity. </summary>
        /// <value> The part entity. </value>
        public CincoPartEntity PartEntity { get; private set; }

        /// <summary> Fetches a part Entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The part. </returns>
        public CincoPartEntity FetchPartEntity( System.Data.IDbConnection connection )
        {
            var entity = new CincoPartEntity();
            _ = entity.FetchUsingKey( connection, this.PartAutoId );
            this.PartEntity = entity;
            return entity;
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the lot. </summary>
        /// <value> The identifier of the lot. </value>
        public int LotAutoId
        {
            get => this.ICache.LotAutoId;

            set {
                if ( !object.Equals( this.LotAutoId, value ) )
                {
                    this.ICache.LotAutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the identifier of the part. </summary>
        /// <value> The identifier of the part. </value>
        public int PartAutoId
        {
            get => this.ICache.PartAutoId;

            set {
                if ( !object.Equals( this.PartAutoId, value ) )
                {
                    this.ICache.PartAutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the lot number. </summary>
        /// <value> The lot number. </value>
        public string LotNumber
        {
            get => this.ICache.LotNumber;

            set {
                if ( !string.Equals( this.LotNumber, value ) )
                {
                    this.ICache.LotNumber = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the part number. </summary>
        /// <value> The part number. </value>
        public string PartNumber
        {
            get => this.ICache.PartNumber;

            set {
                if ( !string.Equals( this.PartNumber, value ) )
                {
                    this.ICache.PartNumber = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTimeOffset.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " TIMESTAMP "

        /// <summary> Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        /// <summary> The timestamp caption format. </summary>
        private string _TimestampCaptionFormat;

        /// <summary> Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._TimestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._TimestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.Timestamp.ToString( this.TimestampCaptionFormat );

        #endregion

    }
}

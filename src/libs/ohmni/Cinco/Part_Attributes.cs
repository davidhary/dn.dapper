
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Dapper;

using isr.Dapper.Entities;

namespace isr.Dapper.Ohmni
{
    public partial class CincoPartEntity
    {

        #region " BUILD "

        /// <summary> Inserts a test level values described by connection. </summary>
        /// <remarks> David, 2020-03-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> An Integer. </returns>
        public static int InsertAttributeTypeValues( System.Data.IDbConnection connection )
        {
            return OhmniAttributeTypeBuilder.InsertIgnoreValues( connection, typeof( AnnealingAttributeType ), new int[] { ( int ) AnnealingAttributeType.None } );
        }

        /// <summary> Inserts a records described by connection. </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> An Integer. </returns>
        public static int InsertSpecificationValues( System.Data.IDbConnection connection )
        {
            return PartNamingTypeBuilder.Instance.InsertIgnore( connection, typeof( PartNamingType ), Array.Empty<int>() );
        }

        #endregion

        #region " ATTRIBUTE RANGES "

        /// <summary> Gets or sets the part attribute ranges. </summary>
        /// <value> The part attribute ranges. </value>
        public IEnumerable<OhmniPartAttributeRangeEntity> AttributeRangeEntities { get; private set; }

        /// <summary> Fetches the PartAttributeRanges. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The PartAttributeRanges. </returns>
        public int FetchPartAttributeRanges( System.Data.IDbConnection connection )
        {
            return this.Populate( OhmniPartAttributeRangeEntity.FetchEntities( connection, this.PartAutoId ) );
        }

        /// <summary> Fetches the PartAttributeRanges. </summary>
        /// <remarks>
        /// https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
        /// https://dapper-tutorial.net/querymultiple.
        /// </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="partAutoId"> The identifier of the part. </param>
        /// <returns> The PartAttributeRanges. </returns>
        public int FetchPartAttributeRanges( System.Data.IDbConnection connection, int partAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"select * from [{CincoPartBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $" where {nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )} = @Id; " );
            _ = queryBuilder.AppendLine( $"select * from [{OhmniPartAttributeRangeBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $" where {nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeNub.PartAutoId )} = @Id; " );
            var gridReader = connection.QueryMultiple( queryBuilder.ToString(), new { Id = partAutoId } );
            ICincoPart entity = gridReader.ReadSingle<CincoPartNub>();
            if ( entity is null )
            {
                return this.Populate( new List<OhmniPartAttributeRangeEntity>() );
            }
            else
            {
                if ( !CincoPartNub.AreEqual( entity, this.ICache ) )
                {
                    this.UpdateCache( entity );
                    _ = this.UpdateStore();
                }

                return this.Populate( OhmniPartAttributeRangeEntity.Populate( gridReader.Read<OhmniPartAttributeRangeNub>() ) );
            }
        }

        /// <summary> Gets or sets the Part Attribute Ranges. </summary>
        /// <value> The Part Range attribute values. </value>
        public CincoPartAttributeRangeCollection AttributeRanges { get; private set; }

        /// <summary> Fetches attribute ranges. </summary>
        /// <remarks> David, 2020-03-31. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The Ranges. </returns>
        public int FetchRanges( System.Data.IDbConnection connection )
        {
            return this.Populate( OhmniPartAttributeRangeEntity.FetchEntities( connection, this.PartAutoId ) );
        }

        /// <summary> Fetches attribute values. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="partAutoId"> Identifier for the part. </param>
        /// <returns> The Ranges. </returns>
        public static CincoPartAttributeRangeCollection FetchRanges( System.Data.IDbConnection connection, int partAutoId )
        {
            var Ranges = new CincoPartAttributeRangeCollection( partAutoId );
            Ranges.Populate( OhmniPartAttributeRangeEntity.FetchEntities( connection, partAutoId ) );
            return Ranges;
        }

        /// <summary> Populates the given entities. </summary>
        /// <remarks> David, 2020-05-21. </remarks>
        /// <param name="entities"> The entities. </param>
        /// <returns> The number of entities. </returns>
        private int Populate( IEnumerable<OhmniPartAttributeRangeEntity> entities )
        {
            this.AttributeRangeEntities = entities;
            this.AttributeRanges = new CincoPartAttributeRangeCollection( this.PartAutoId );
            this.AttributeRanges.Populate( entities );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoPartEntity.AttributeRangeEntities ) );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoPartEntity.AttributeRanges ) );
            return entities?.Any() == true ? entities.Count() : 0;
        }

        #endregion

        #region " ATTRIBUTE VALUES "

        /// <summary> Gets or sets the part attribute values. </summary>
        /// <value> The part attribute values. </value>
        public IEnumerable<OhmniPartAttributeValueEntity> AttributeValueEntities { get; private set; }

        /// <summary> Fetches the PartAttributeValues. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The PartAttributeValues. </returns>
        public int FetchPartAttributeValues( System.Data.IDbConnection connection )
        {
            return this.Populate( OhmniPartAttributeValueEntity.FetchEntities( connection, this.PartAutoId ) );
        }

        /// <summary> Fetches the PartAttributeValues. </summary>
        /// <remarks>
        /// https://www.codeproject.com/Articles/1260540/Tutorial-on-Handling-Multiple-Resultsets-and-Multi
        /// https://dapper-tutorial.net/querymultiple.
        /// </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="partAutoId"> The identifier of the part. </param>
        /// <returns> The PartAttributeValues. </returns>
        public int FetchPartAttributeValues( System.Data.IDbConnection connection, int partAutoId )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.AppendLine( $"select * from [{CincoPartBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $" where {nameof( isr.Dapper.Ohmni.CincoPartNub.PartAutoId )} = @Id; " );
            _ = queryBuilder.AppendLine( $"select * from [{OhmniPartAttributeValueBuilder.TableName}]" );
            _ = queryBuilder.AppendLine( $" where {nameof( isr.Dapper.Ohmni.OhmniPartAttributeValueNub.PartAutoId )} = @Id; " );
            var gridReader = connection.QueryMultiple( queryBuilder.ToString(), new { Id = partAutoId } );
            ICincoPart entity = gridReader.ReadSingle<CincoPartNub>();
            if ( entity is null )
            {
                return this.Populate( new List<OhmniPartAttributeValueEntity>() );
            }
            else
            {
                if ( !CincoPartNub.AreEqual( entity, this.ICache ) )
                {
                    this.UpdateCache( entity );
                    _ = this.UpdateStore();
                }

                return this.Populate( OhmniPartAttributeValueEntity.Populate( gridReader.Read<OhmniPartAttributeValueNub>() ) );
            }
        }

        /// <summary> Gets or sets the Part Attribute Values. </summary>
        /// <value> The Part Value attribute values. </value>
        public CincoPartAttributeValueCollection AttributeValues { get; private set; }

        /// <summary> Fetches attribute Values. </summary>
        /// <remarks> David, 2020-03-31. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The Values. </returns>
        public int FetchValues( System.Data.IDbConnection connection )
        {
            return this.Populate( OhmniPartAttributeValueEntity.FetchEntities( connection, this.PartAutoId ) );
        }

        /// <summary> Fetches attribute values. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="partAutoId"> Identifier for the part. </param>
        /// <returns> The Values. </returns>
        public static CincoPartAttributeValueCollection FetchValues( System.Data.IDbConnection connection, int partAutoId )
        {
            var Values = new CincoPartAttributeValueCollection( partAutoId );
            Values.Populate( OhmniPartAttributeValueEntity.FetchEntities( connection, partAutoId ) );
            return Values;
        }

        /// <summary> Populates the given entities. </summary>
        /// <remarks> David, 2020-05-21. </remarks>
        /// <param name="entities"> The entities. </param>
        /// <returns> The number of entities. </returns>
        private int Populate( IEnumerable<OhmniPartAttributeValueEntity> entities )
        {
            this.AttributeValueEntities = entities;
            this.AttributeValues = new CincoPartAttributeValueCollection( this.PartAutoId );
            this.AttributeValues.Populate( entities );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoPartEntity.AttributeValueEntities ) );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoPartEntity.AttributeValues ) );
            return entities?.Any() == true ? entities.Count() : 0;
        }

        #endregion

    }

    /// <summary>
    /// Values that represent annealing attribute types for the attribute type table.
    /// </summary>
    /// <remarks> David, 2020-10-02. </remarks>
    public enum AnnealingAttributeType
    {

        /// <summary> . </summary>
        [Description( "Attribute Type" )]
        None = 0,

        /// <summary> . </summary>
        [Description( "Nominal Value" )]
        NominalValue = 1,

        /// <summary> . </summary>
        [Description( "Tolerance" )]
        Tolerance = 2,

        /// <summary> . </summary>
        [Description( "Tracking Tolerance" )]
        TrackingTolerance = 3,

        /// <summary> . </summary>
        [Description( "TCR" )]
        Tcr = 4,

        /// <summary> . </summary>
        [Description( "Tracking TCR" )]
        TrackingTcr = 5,

        /// <summary> . </summary>
        [Description( "TaN Value" )]
        TanValue = 6,

        /// <summary> . </summary>
        [Description( "Annealing Shift" )]
        AnnealingShift = TanValue + 1
    }

    /// <summary> Collection of ohmni cinco part attribute values. </summary>
    /// <remarks> David, 2020-04-30. </remarks>
    public class CincoPartAttributeValueCollection : OhmniPartAttributeValueCollection
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="partAutoId"> Identifier for the part. </param>
        public CincoPartAttributeValueCollection( int partAutoId ) : base( partAutoId )
        {
        }

        /// <summary> Converts a name to a key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns> Name as an Integer. </returns>
        private int ToKey( string name )
        {
            return ( int ) (Enum.Parse( typeof( AnnealingAttributeType ), name ));
        }

        /// <summary> Gets or sets the attribute value. </summary>
        /// <value> The attribute value. Returns <see cref="Double.NaN"/> if value does not exist. </value>
        protected double AttributeValueGetter( [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            int key = this.ToKey( name );
            return this.Contains( key ) ? this[key].Value : double.NaN;
        }

        /// <summary>   Attribute value setter. </summary>
        /// <remarks>   David, 2020-10-03. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void AttributeValueSetter( double value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            int key = this.ToKey( name );
            if ( this.Contains( key ) )
            {
                this[key].Value = value;
            }
            else
            {
                this.Add( new OhmniPartAttributeValueEntity() { PartAutoId = PartAutoId, AttributeTypeId = key, Value = value } );
            }
        }

        /// <summary> Gets or sets the nominal value. </summary>
        /// <value> The nominal value. </value>
        public double NominalValue
        {
            get => this.AttributeValueGetter();

            set => this.AttributeValueSetter( value );
        }

        /// <summary> Gets or sets the tolerance. </summary>
        /// <value> The tolerance. </value>
        public double Tolerance
        {
            get => this.AttributeValueGetter();

            set => this.AttributeValueSetter( value );
        }

        /// <summary> Gets or sets the tracking tolerance. </summary>
        /// <value> The tracking tolerance. </value>
        public double TrackingTolerance
        {
            get => this.AttributeValueGetter();

            set => this.AttributeValueSetter( value );
        }

        /// <summary> Gets or sets the tcr. </summary>
        /// <value> The tcr. </value>
        public double Tcr
        {
            get => this.AttributeValueGetter();

            set => this.AttributeValueSetter( value );
        }

        /// <summary> Gets or sets the tracking tcr. </summary>
        /// <value> The tracking tcr. </value>
        public double TrackingTcr
        {
            get => this.AttributeValueGetter();

            set => this.AttributeValueSetter( value );
        }
    }

    /// <summary> Collection of ohmni cinco part attribute ranges. </summary>
    /// <remarks> David, 2020-04-30. </remarks>
    public class CincoPartAttributeRangeCollection : OhmniPartAttributeRangeCollection
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="partAutoId"> Identifier for the part. </param>
        public CincoPartAttributeRangeCollection( int partAutoId ) : base( partAutoId )
        {
        }

        /// <summary> Converts a name to a key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="name"> The name. </param>
        /// <returns> Name as an Integer. </returns>
        private int ToKey( string name )
        {
            return ( int ) (Enum.Parse( typeof( AnnealingAttributeType ), name ));
        }

        /// <summary> Gets or sets the attribute range. </summary>
        /// <value>
        /// The attribute value. Returns <see cref="isr.Std.Primitives.RangeR.Empty"/> if value does not
        /// exist.
        /// </value>
        protected Std.Primitives.RangeR AttributeRangeGetter( [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            int key = this.ToKey( name );
            return this.Contains( key ) ? this[key].Range : Std.Primitives.RangeR.Empty;
        }

        /// <summary>   Attribute range setter. </summary>
        /// <remarks>   David, 2020-10-03. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="name">     (Optional) The name. </param>
        protected void AttributeRangeSetter( Std.Primitives.RangeR value, [System.Runtime.CompilerServices.CallerMemberName()] string name = "" )
        {
            int key = this.ToKey( name );
            if ( this.Contains( key ) )
            {
                this[key].Range = value;
            }
            else
            {
                this.Add( new OhmniPartAttributeRangeEntity() { PartAutoId = PartAutoId, AttributeTypeId = key, Range = value } );
            }
        }

        /// <summary> Gets or sets the tangent value. </summary>
        /// <value> The tangent value. </value>
        public Std.Primitives.RangeR TanValue
        {
            get => this.AttributeRangeGetter();

            set => this.AttributeRangeSetter( value );
        }

        /// <summary> Gets or sets the annealing shift. </summary>
        /// <value> The annealing shift. </value>
        public Std.Primitives.RangeR AnnealingShift
        {
            get => this.AttributeRangeGetter();

            set => this.AttributeRangeSetter( value );
        }
    }
}

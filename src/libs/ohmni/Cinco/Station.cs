using System;
using System.Collections.Generic;
using System.Linq;

using Dapper;
using Dapper.Contrib.Extensions;

using isr.Dapper.Entities.TrimExtensions;
using isr.Dapper.Entity;

namespace isr.Dapper.Ohmni
{

    /// <summary>
    /// Interface for the Station nub and entity. Includes the fields as kept in the data table.
    /// Allows tracking of property changes.
    /// </summary>
    /// <remarks>
    /// Update tracking of table with default values requires fetching the inserted record if a
    /// default value is set to a non-default value. <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface ICincoStation
    {

        /// <summary> Gets the identifier of the station. </summary>
        /// <value> The identifier of the station. </value>
        [Key]
        int StationAutoId { get; set; }

        /// <summary> Gets the station name. </summary>
        /// <value> The station name. </value>
        string StationName { get; set; }

        /// <summary> Gets the name of the computer. </summary>
        /// <value> The name of the computer. </value>
        string ComputerName { get; set; }

        /// <summary> Gets the timezone identity. </summary>
        /// <remarks>
        /// The entity <see cref="TimeZoneId"/> is derived from the
        /// <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name, which, in-fact, is the timezone
        /// identity. The Standard Name is used by the
        /// <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
        /// <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
        /// Name).
        /// </remarks>
        /// <value> The timezone identity. </value>
        string TimeZoneId { get; set; }

        /// <summary> Gets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        DateTime Timestamp { get; set; }
    }

    /// <summary> A station builder. </summary>
    /// <remarks> David, 2020-04-24. </remarks>
    public sealed class CincoStationBuilder
    {

        /// <summary> Name of the table. </summary>
        private static string _TableName;

        /// <summary> Retrieves the table name from the class <see cref="TableAttribute"/>. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <value> A String. </value>
        public static string TableName
        {
            get {
                if ( string.IsNullOrWhiteSpace( _TableName ) )
                {
                    _TableName = (( TableAttribute ) Attribute.GetCustomAttribute( typeof( CincoStationNub ), typeof( TableAttribute ) )).Name;
                }

                return _TableName;
            }
        }

        /// <summary> Gets or sets the indication of using unique station. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="NotImplementedException">   Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The name of the using unique station. </value>
        public static bool UsingUniqueStationName { get; private set; } = true;

        /// <summary> Creates a table. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        public static string CreateTable( System.Data.IDbConnection connection )
        {
            return connection is System.Data.SqlClient.SqlConnection sql
                ? CreateTable( sql )
                : connection is System.Data.SQLite.SQLiteConnection sqlite ? CreateTable( sqlite ) : string.Empty;
        }

        /// <summary> Creates table for SQLite database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SQLite.SQLiteConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"CREATE TABLE IF NOT EXISTS [{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] (
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.StationAutoId )}] integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.StationName )}] nvarchar(50) NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.ComputerName )}] nvarchar(50) NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.TimeZoneId )}] nvarchar(50) NOT NULL, 
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.Timestamp )}] datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP));
            CREATE UNIQUE INDEX [UQ_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoStationNub.ComputerName )}] ON [{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoStationNub.ComputerName )}]); 
            CREATE UNIQUE INDEX [UQ_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoStationNub.StationName )}] ON [{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoStationNub.StationName )}]); " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }

        /// <summary> Creates table for SQL Server database. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> The table name or empty. </returns>
        private static string CreateTable( System.Data.SqlClient.SqlConnection connection )
        {
            var queryBuilder = new System.Text.StringBuilder();
            _ = queryBuilder.Append( @$"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoStationBuilder.TableName}]') AND type in (N'U'))
            BEGIN
            CREATE TABLE [dbo].[{isr.Dapper.Ohmni.CincoStationBuilder.TableName}](
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.StationAutoId )}] [int] IDENTITY(1,1) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.StationName )}] [nvarchar](50) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.ComputerName )}] [nvarchar](50) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.TimeZoneId )}] [nvarchar](50) NOT NULL,
                [{nameof( isr.Dapper.Ohmni.CincoStationNub.Timestamp )}] [datetime2](2) NOT NULL DEFAULT (sysutcdatetime()),
             CONSTRAINT [PK_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] PRIMARY KEY CLUSTERED 
            ([{nameof( isr.Dapper.Ohmni.CincoStationNub.StationAutoId )}] ASC) 
              WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
              ON [PRIMARY]
            END;
            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoStationBuilder.TableName}]') AND name = N'UQ_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoStationNub.ComputerName )}')
            CREATE UNIQUE NONCLUSTERED INDEX [UQ_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoStationNub.ComputerName )}] ON [dbo].[{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoStationNub.ComputerName )}] ASC) 
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
             ON [PRIMARY]; 
            IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[{isr.Dapper.Ohmni.CincoStationBuilder.TableName}]') AND name = N'UQ_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoStationNub.StationName )}')
            CREATE UNIQUE NONCLUSTERED INDEX [UQ_{isr.Dapper.Ohmni.CincoStationBuilder.TableName}_{nameof( isr.Dapper.Ohmni.CincoStationNub.StationName )}] ON [dbo].[{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] ([{nameof( isr.Dapper.Ohmni.CincoStationNub.StationName )}] ASC) 
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
             ON [PRIMARY]; " );
            _ = connection.Execute( queryBuilder.ToString().Clean() );
            return TableName;
        }
    }

    /// <summary> Implements the Station table <see cref="ICincoStation">interface</see>. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-19 </para>
    /// </remarks>
    [Table( "Station" )]
    public class CincoStationNub : EntityNubBase<ICincoStation>, ICincoStation
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Dapper.Entity.EntityBase`2" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoStationNub() : base()
        {
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoStation CreateNew()
        {
            return new CincoStationNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoStation CreateCopy()
        {
            var destination = this.CreateNew();
            Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoStation value )
        {
            Copy( value, this );
        }

        /// <summary> Copies the given value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">      Another instance to copy. </param>
        /// <param name="destination"> Destination for the. </param>
        public static void Copy( ICincoStation source, ICincoStation destination )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( destination is null )
                throw new ArgumentNullException( nameof( destination ) );
            destination.ComputerName = source.ComputerName;
            destination.StationAutoId = source.StationAutoId;
            destination.StationName = source.StationName;
            destination.Timestamp = source.Timestamp;
            destination.TimeZoneId = source.TimeZoneId;
        }

        #endregion

        #region " I EQUATABLE "

        /// <summary> Determines whether the specified object is equal to the current object. </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <param name="other"> The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object other )
        {
            return this.Equals( other as ICincoStation );
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks> David, 2020-04-27. </remarks>
        /// <param name="other"> An object to compare with this object. </param>
        /// <returns>
        /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
        /// parameter; otherwise, <see langword="false" />.
        /// </returns>
        public override bool Equals( ICincoStation other )
        {
            return other is object && AreEqual( other, this );
        }

        /// <summary> Determines if entities are equal. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
        public static bool AreEqual( ICincoStation left, ICincoStation right )
        {
            if ( left is null )
                throw new ArgumentNullException( nameof( left ) );
            bool result = right is object;
            if ( right is null )
            {
                return false;
            }
            else
            {
                result = result && string.Equals( left.ComputerName, right.ComputerName );
                result = result && Equals( left.StationAutoId, right.StationAutoId );
                result = result && string.Equals( left.StationName, right.StationName );
                result = result && string.Equals( left.TimeZoneId, right.TimeZoneId );
                result = result && DateTime.Equals( left.Timestamp, right.Timestamp );
                return result;
            }
        }

        /// <summary> Serves as the default hash function. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return ( this.ComputerName, this.StationAutoId, this.StationName, this.TimeZoneId, this.Timestamp ).GetHashCode();
        }


        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the station. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The identifier of the station. </value>
        [Key]
        public int StationAutoId { get; set; }

        /// <summary> Gets or sets the station name. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The station name. </value>
        public string StationName { get; set; }

        /// <summary> Gets or sets the name of the computer. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The name of the computer. </value>
        public string ComputerName { get; set; }

        /// <summary> Gets or sets the timezone identity. </summary>
        /// <remarks>
        /// The entity <see cref="isr.Dapper.Ohmni.CincoStationNub.TimeZoneId"/> is derived from the
        /// <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name, which, in-fact, is the timezone
        /// identity. The Standard Name is used by the
        /// <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
        /// <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
        /// Name).
        /// </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The timezone identity. </value>
        public string TimeZoneId { get; set; }

        /// <summary> Gets or sets the UTC timestamp; Update-able database-created default. </summary>
        /// <remarks>
        /// Stored in universal time. Use the station <see cref="CincoStationEntity.TimeZoneId"/> to
        /// convert to local time.
        /// </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The UTC timestamp. </value>
        [Computed()]
        public DateTime Timestamp { get; set; }

        #endregion

        #region " ENTITY LOCAL TIMES "

        /// <summary> Information describing the locale time zone. </summary>
        private TimeZoneInfo _LocaleTimeZoneInfo;

        /// <summary> Returns the information describing the time zone in the entity locale. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A TimeZoneInfo. </returns>
        public TimeZoneInfo LocaleTimeZoneInfo()
        {
            if ( this._LocaleTimeZoneInfo is null )
            {
                if ( string.IsNullOrWhiteSpace( this.TimeZoneId ) )
                    this.TimeZoneId = TimeZoneInfo.Local.Id;
                this._LocaleTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById( this.TimeZoneId );
            }

            return this._LocaleTimeZoneInfo;
        }

        /// <summary>
        /// Returns the time now in the entity locale based on the Locale
        /// <see cref="System.TimeZoneInfo"/>.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The time now in the entity locale. </returns>
        public DateTimeOffset LocaleTimeNow()
        {
            return TimeZoneInfo.ConvertTime( DateTimeOffset.Now, this.LocaleTimeZoneInfo() );
        }

        /// <summary> Returns the <see cref="Timestamp"/> in the entity local. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The <see cref="Timestamp"/> entity locale. </returns>
        public DateTimeOffset LocaleTimestamp()
        {
            return TimeZoneInfo.ConvertTimeFromUtc( this.Timestamp, this.LocaleTimeZoneInfo() );
        }

        /// <summary> Converts an universalTime to a locale timestamp. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="universalTime"> The universal time. </param>
        /// <returns> UniversalTime as a DateTimeOffset. </returns>
        public DateTimeOffset ToLocaleTimestamp( DateTime universalTime )
        {
            return TimeZoneInfo.ConvertTimeFromUtc( universalTime, this.LocaleTimeZoneInfo() );
        }

        #endregion

    }

    /// <summary> The Station Entity. Implements access to the database using Dapper. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-09-19 </para>
    /// </remarks>
    public partial class CincoStationEntity : EntityBase<ICincoStation, CincoStationNub>, ICincoStation
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public CincoStationEntity() : this( new CincoStationNub() )
        {
        }

        /// <summary> Constructs an entity that was not yet stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoStationEntity( ICincoStation value ) : this( value, null )
        {
        }

        /// <summary> Constructs an entity that is already stored. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="cache"> The cache. </param>
        /// <param name="store"> The store. </param>
        public CincoStationEntity( ICincoStation cache, ICincoStation store ) : base( new CincoStationNub(), cache, store )
        {
            this.Initialize();
        }

        /// <summary>   Constructs an entity that might have been stored. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        /// <param name="value">    The instance from which to construct the class. </param>
        public CincoStationEntity( CincoStationEntity value ) : base( value )
        {
            this.Initialize();
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2020-10-17. </remarks>
        private void Initialize()
        {
            this.UsingNativeTracking = String.Equals( isr.Dapper.Ohmni.CincoStationBuilder.TableName, nameof( isr.Dapper.Ohmni.ICincoStation ).TrimStart( 'I' ) );
        }

        #endregion

        #region " I TYPED CLONABLE "

        /// <summary> Creates a new instance of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The new instance the entity 'Nub'. </returns>
        public override ICincoStation CreateNew()
        {
            return new CincoStationNub();
        }

        /// <summary> Creates a copy of the class using the entity 'Nub'. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The copy of the entity 'Nub'. </returns>
        public override ICincoStation CreateCopy()
        {
            var destination = this.CreateNew();
            CincoStationNub.Copy( this, destination );
            return destination;
        }

        /// <summary> Copies the given entity into this class. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The instance from which to copy. </param>
        public override void CopyFrom( ICincoStation value )
        {
            CincoStationNub.Copy( value, this );
        }

        #endregion

        #region " OVERRIDES "

        /// <summary>
        /// Update the cached value, which also notifies of the entity property changes.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="value"> The Station interface. </param>
        public override void UpdateCache( ICincoStation value )
        {
            // first make the copy to notify of any property change.
            CincoStationNub.Copy( value, this );
            // this is required to restore the cache interface for tracking
            base.UpdateCache( value );
        }

        #endregion

        #region " DATABASE ACCESS "

        /// <summary> Fetches using key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        The Station table primary key. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingKey( System.Data.IDbConnection connection, int key )
        {
            this.ClearStore();
            return this.Enstore( this.UsingNativeTracking ? connection.Get<ICincoStation>( key ) : connection.Get<CincoStationNub>( key ) );
        }

        /// <summary> Refetch; Fetch using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool FetchUsingKey( System.Data.IDbConnection connection )
        {
            return this.FetchUsingKey( connection, this.StationAutoId );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool FetchUsingUniqueIndex( System.Data.IDbConnection connection )
        {
            return this.FetchUsingUniqueIndex( connection, this.ComputerName );
        }

        /// <summary> Fetches an existing entity using unique index. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="computerName"> Name of the computer. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public bool FetchUsingUniqueIndex( System.Data.IDbConnection connection, string computerName )
        {
            this.ClearStore();
            var nub = FetchNubs( connection, computerName ).SingleOrDefault();
            return nub is object && this.Enstore( nub );
        }

        /// <summary>
        /// Inserts the entity as set in entity <see cref="P:isr.Dapper.Entity.EntityModel`2.ICache" />
        /// using given connection thus preserving tracking. Fetches the stored entity to update the
        /// computed value.
        /// </summary>
        /// <remarks> David, 2020-04-30. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Insert( System.Data.IDbConnection connection )
        {
            _ = base.Insert( connection );
            return this.FetchUsingKey( connection );
        }

        /// <summary> Obtains a default entity using the given connection. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool ObtainDefault( System.Data.IDbConnection connection )
        {
            if ( string.IsNullOrEmpty( this.ComputerName ) )
                this.ComputerName = Environment.MachineName;
            if ( string.IsNullOrEmpty( this.StationName ) )
                this.StationName = this.ComputerName;
            if ( string.IsNullOrWhiteSpace( this.TimeZoneId ) )
                this.TimeZoneId = TimeZoneInfo.Local.StandardName;
            return this.Obtain( connection );
        }

        /// <summary>   Attempts to obtain from the given data. </summary>
        /// <remarks>   David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="connection">           The connection. </param>
        /// <param name="computerName">         Name of the computer. </param>
        /// <param name="addStationPredicate">  The add station predicate; typically opens a message box
        ///                                     requesting operator confirmation. </param>
        /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryObtain( System.Data.IDbConnection connection, string computerName, Func<bool> addStationPredicate )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            if ( string.IsNullOrWhiteSpace( computerName ) )
                throw new ArgumentNullException( nameof( computerName ) );
            this.ComputerName = computerName;
            return this.FetchUsingUniqueIndex( connection )
                ? this.IsClean() : addStationPredicate.Invoke() && this.ObtainDefault( connection );
        }

        /// <summary> Updates or inserts the entity using the specified entity information. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="entity">     The entity. </param>
        /// <returns>
        /// <c>true</c> if it succeeds (<see cref="EntityBase{TIEntity, TEntityNub}.IsClean()"/>);
        /// otherwise <c>false</c>
        /// </returns>
        public override bool Upsert( System.Data.IDbConnection connection, ICincoStation entity )
        {
            if ( entity is null )
                throw new ArgumentNullException( nameof( entity ) );
            if ( ReferenceEquals( this, entity ) )
                throw new InvalidOperationException( $"{nameof( entity )} must not be identical to the specified entity" );
            // try fetching an existing record
            bool fetched = CincoStationBuilder.UsingUniqueStationName ? this.FetchUsingUniqueIndex( connection, entity.StationName ) : this.FetchUsingKey( connection, entity.StationAutoId );
            if ( fetched )
            {
                // update the existing record from the specified entity.
                // insert a new record based on the specified entity values.
                entity.StationAutoId = this.StationAutoId;
                this.UpdateCache( entity );
                _ = this.Update( connection );
            }
            else
            {
                this.UpdateCache( entity );
                _ = this.Insert( connection );
            }

            return this.IsClean();
        }

        /// <summary> Deletes a record using the given primary key. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="key">        The primary key. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Delete( System.Data.IDbConnection connection, int key )
        {
            return connection.Delete( new CincoStationNub() { StationAutoId = key } );
        }

        #endregion

        #region " ENTITIES "

        /// <summary> Gets or sets the Station entities. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Station entities. </value>
        public IEnumerable<CincoStationEntity> Stations { get; private set; }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">          The connection. </param>
        /// <param name="usingNativeTracking"> True to using native tracking. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the entities in this collection.
        /// </returns>
        public static IEnumerable<CincoStationEntity> FetchAllEntities( System.Data.IDbConnection connection, bool usingNativeTracking )
        {
            return usingNativeTracking ? Populate( connection.GetAll<ICincoStation>() ) : Populate( connection.GetAll<CincoStationNub>() );
        }

        /// <summary> Fetches all records into entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <returns> all. </returns>
        public override int FetchAllEntities( System.Data.IDbConnection connection )
        {
            this.Stations = FetchAllEntities( connection, this.UsingNativeTracking );
            this.NotifyPropertyChanged( nameof( isr.Dapper.Ohmni.CincoStationEntity.Stations ) );
            return this.Stations?.Any() == true ? this.Stations.Count() : 0;
        }

        /// <summary> Populates a list of Station entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="nubs"> The Station nubs. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoStationEntity> Populate( IEnumerable<CincoStationNub> nubs )
        {
            var l = new List<CincoStationEntity>();
            if ( nubs?.Any() == true )
            {
                foreach ( CincoStationNub nub in nubs )
                    l.Add( new CincoStationEntity( nub, nub.CreateCopy() ) );
            }

            return l;
        }

        /// <summary> Populates a list of Station entities. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="interfaces"> The Station interfaces. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process populate in this collection.
        /// </returns>
        public static IEnumerable<CincoStationEntity> Populate( IEnumerable<ICincoStation> interfaces )
        {
            var l = new List<CincoStationEntity>();
            if ( interfaces?.Any() == true )
            {
                var nub = new CincoStationNub();
                foreach ( ICincoStation iFace in interfaces )
                {
                    nub.CopyFrom( iFace );
                    l.Add( new CincoStationEntity( iFace, nub.CreateCopy() ) );
                }
            }

            return l;
        }

        #endregion

        #region " FIND "

        /// <summary> Count entities; returns 1 or 0. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="computerName"> Name of the computer. </param>
        /// <returns> The total number of entities. </returns>
        public static int CountEntities( System.Data.IDbConnection connection, string computerName )
        {
            // Dim selector As SqlBuilder.Template = builder.AddTemplate($"SELECT COUNT(*) FROM [{StationNub.TableName}]
            // WHERE {NameOf(StationNub.ComputerName)} = @ComputerName", new {ComputerName = ComputerName})

            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                @$"SELECT COUNT(*) FROM [{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoStationNub.ComputerName )} = @ComputerName",
                                            new { computerName } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.ExecuteScalar<int>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Fetches nubs; expects single entity or none. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="computerName"> The computer name. </param>
        /// <returns> An IStation . </returns>
        public static IEnumerable<CincoStationNub> FetchNubs( System.Data.IDbConnection connection, string computerName )
        {
            var sqlBuilder = new SqlBuilder();
            SqlBuilder.Template selector = sqlBuilder.AddTemplate(
                $"SELECT * FROM [{isr.Dapper.Ohmni.CincoStationBuilder.TableName}] WHERE {nameof( isr.Dapper.Ohmni.CincoStationNub.ComputerName )} = @ComputerName", new { computerName } );
            if ( selector.RawSql is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.RawSql )} null" );
            }
            else if ( selector.Parameters is null )
            {
                throw new InvalidOperationException( $"{nameof( selector.Parameters )} null" );
            }

            return connection.Query<CincoStationNub>( selector.RawSql, selector.Parameters );
        }

        /// <summary> Determine if the Station exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="computerName"> The computer name. </param>
        /// <returns> <c>true</c> if exists; otherwise <c>false</c> </returns>
        public static bool IsExists( System.Data.IDbConnection connection, string computerName )
        {
            return 1 == CountEntities( connection, computerName );
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the identifier of the station. </summary>
        /// <value> The identifier of the station. </value>
        public int StationAutoId
        {
            get => this.ICache.StationAutoId;

            set {
                if ( !object.Equals( this.StationAutoId, value ) )
                {
                    this.ICache.StationAutoId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string StationName
        {
            get => this.ICache.StationName;

            set {
                if ( !string.Equals( this.StationName, value ) )
                {
                    this.ICache.StationName = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the name of the computer. </summary>
        /// <value> The name of the computer. </value>
        public string ComputerName
        {
            get => this.ICache.ComputerName;

            set {
                if ( !string.Equals( this.ComputerName, value ) )
                {
                    this.ICache.ComputerName = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the timezone identity. </summary>
        /// <remarks>
        /// The entity <see cref="TimeZoneId"/> is derived from the
        /// <see cref="System.TimeZone.CurrentTimeZone"/> Standard Name, which, in-fact, is the timezone
        /// identity. The Standard Name is used by the
        /// <see cref="System.TimeZoneInfo.FindSystemTimeZoneById(String)"/> to select the
        /// <see cref="System.TimeZoneInfo">time zone info</see> for the specified identity (Standard
        /// Name).
        /// </remarks>
        /// <value> The timezone identity. </value>
        public string TimeZoneId
        {
            get => this.ICache.TimeZoneId;

            set {
                if ( !string.Equals( this.TimeZoneId, value ) )
                {
                    this.ICache.TimeZoneId = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets or sets the UTC timestamp. </summary>
        /// <remarks> Stored in universal time. </remarks>
        /// <value> The UTC timestamp. </value>
        public DateTime Timestamp
        {
            get => this.ICache.Timestamp;

            set {
                if ( !DateTime.Equals( this.Timestamp, value ) )
                {
                    this.ICache.Timestamp = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        #endregion

        #region " ENTITY LOCAL TIMES "

        /// <summary> Information describing the locale time zone. </summary>
        private TimeZoneInfo _LocaleTimeZoneInfo;

        /// <summary> Returns the information describing the time zone in the entity locale. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> A TimeZoneInfo. </returns>
        public TimeZoneInfo LocaleTimeZoneInfo()
        {
            if ( this._LocaleTimeZoneInfo is null )
            {
                if ( string.IsNullOrWhiteSpace( this.TimeZoneId ) )
                    this.TimeZoneId = TimeZoneInfo.Local.Id;
                this._LocaleTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById( this.TimeZoneId );
            }

            return this._LocaleTimeZoneInfo;
        }

        /// <summary>
        /// Returns the time now in the entity locale based on the Locale
        /// <see cref="System.TimeZoneInfo"/>.
        /// </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The time now in the entity locale. </returns>
        public DateTimeOffset LocaleTimeNow()
        {
            return TimeZoneInfo.ConvertTime( DateTimeOffset.Now, this.LocaleTimeZoneInfo() );
        }

        /// <summary> Returns the <see cref="Timestamp"/> in the entity local. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The <see cref="Timestamp"/> entity locale. </returns>
        public DateTimeOffset LocaleTimestamp()
        {
            return TimeZoneInfo.ConvertTimeFromUtc( this.Timestamp, this.LocaleTimeZoneInfo() );
        }

        /// <summary> Converts an universalTime to a locale timestamp. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="universalTime"> The universal time. </param>
        /// <returns> UniversalTime as a DateTimeOffset. </returns>
        public DateTimeOffset ToLocaleTimestamp( DateTime universalTime )
        {
            return TimeZoneInfo.ConvertTimeFromUtc( universalTime, this.LocaleTimeZoneInfo() );
        }

        #endregion

        #region " TIMESTAMP "

        /// <summary> Gets or sets the default timestamp caption format. </summary>
        /// <value> The default timestamp caption format. </value>
        public static string DefaultTimestampCaptionFormat { get; set; } = "YYYYMMDD.HHmmss.f";

        /// <summary> The timestamp caption format. </summary>
        private string _TimestampCaptionFormat;

        /// <summary> Gets or sets the timestamp caption format. </summary>
        /// <value> The timestamp caption format. </value>
        public string TimestampCaptionFormat
        {
            get => this._TimestampCaptionFormat;

            set {
                if ( !string.Equals( this.TimestampCaptionFormat, value ) )
                {
                    this._TimestampCaptionFormat = value;
                    this.NotifyFieldChanged();
                }
            }
        }

        /// <summary> Gets the timestamp caption. </summary>
        /// <value> The timestamp caption. </value>
        public string TimestampCaption => this.Timestamp.ToString( this.TimestampCaptionFormat );

        #endregion

    }
}

# About

isr.Dapper.EntityUI is a .Net library adding Dapper Entity extensions to Windows Controls.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Dapper.EntityUI is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Dapper Repository].

[Dapper Repository]: https://bitbucket.org/davidhary/dn.dapper


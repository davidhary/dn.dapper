using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using isr.Dapper.Entities;

namespace isr.Dapper.EntityUI.ListControlExtensions
{
    /// <summary>   <see cref=" isr.Dapper.EntityUI.ListControlExtensions"/> methods. </summary>
    /// <remarks>   David, 2020-10-03. </remarks>
    public static class ListControlExtensionMethods
    {

        #region " LIST CONTROL "

        /// <summary> Selected identity. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <param name="control"> The control. </param>
        /// <returns> An Integer. </returns>
        public static int SelectedIdentity( this ListControl control )
        {
            return ( int ) control.SelectedValue;
        }

        /// <summary> Selected type. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The control. </param>
        /// <returns> A T. </returns>
        public static T SelectedType<T>( this ComboBox control ) where T : class
        {
            return control is null ? throw new ArgumentNullException( nameof( control ) ) : control.SelectedItem as T;
        }

        /// <summary> Select value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The value. </param>
        public static void SelectValue( this ListControl control, int value )
        {
            if ( control is object )
            {
                control.SelectedValue = value;
            }
        }

        #endregion

        #region " I KeyLabelTime "

        /// <summary> List <see cref="IKeyLabelTime"/> entities. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="entities"> The entities. </param>
        /// <param name="control">  The control. </param>
        /// <returns> The number of entities. </returns>
        public static int ListEntities( this IEnumerable<IKeyLabelTime> entities, ListControl control )
        {
            if ( entities is null )
            {
                throw new ArgumentNullException( nameof( entities ) );
            }

            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            bool wasEnabled = control.Enabled;
            string label = control.Text;
            control.Enabled = false;
            control.DataSource = null;
            control.ValueMember = nameof( isr.Dapper.Entities.IKeyLabelTime.AutoId );
            control.DisplayMember = nameof( isr.Dapper.Entities.IKeyLabelTime.Label );
            // converting to array ensures that values displayed in two controls are not connected.
            control.DataSource = entities;
            if ( !string.IsNullOrEmpty( label ) )
            {
                var entity = entities.SelectEntity( label );
                if ( entity is object )
                {
                    control.SelectedValue = entity.AutoId;
                }
                else
                {
                    control.Text = label;
                }
            }
            // doing this the control may display text even if none selected! control.SelectedIndex = -1
            control.Enabled = wasEnabled;
            control.Invalidate();
            return entities.Count();
        }

        /// <summary> Select entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="entities"> The entities. </param>
        /// <param name="label">    The label. </param>
        /// <returns> An IKeyLabelTime. </returns>
        public static IKeyLabelTime SelectEntity( this IEnumerable<IKeyLabelTime> entities, string label )
        {
            var entity = entities.Where( x => string.Equals( x.Label, label, StringComparison.OrdinalIgnoreCase ) ).FirstOrDefault();
            return entity;
        }

        /// <summary>   An  <see cref="IEnumerable{IKeyLabelTime}"/> extension method that selects a value for the control . </summary>
        /// <remarks>   David, 2021-05-10. </remarks>
        /// <param name="entities"> The entities. </param>
        /// <param name="control">  The control. </param>
        public static void SelectValue( this IEnumerable<IKeyLabelTime> entities, ListControl control )
        {
            string label = control.Text;
            if ( !string.IsNullOrEmpty( label ) )
            {
                var entity = entities.SelectEntity( label );
                if ( entity is object )
                {
                    control.SelectedValue = entity.AutoId;
                }
            }
        }


        #endregion

        #region " I NOMINAL "

        /// <summary> List <see cref="INominal"/> entities. </summary>
        /// <remarks> David, 2020-06-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="entities"> The entities. </param>
        /// <param name="control">  The control. </param>
        /// <returns> The number of entities. </returns>
        public static int ListEntities( this IEnumerable<INominal> entities, ListControl control )
        {
            if ( entities is null )
            {
                throw new ArgumentNullException( nameof( entities ) );
            }

            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            bool wasEnabled = control.Enabled;
            string label = control.Text;
            control.Enabled = false;
            control.DataSource = null;
            control.ValueMember = nameof( isr.Dapper.Entities.INominal.Id );
            control.DisplayMember = nameof( isr.Dapper.Entities.INominal.Label );
            // converting to array ensures that values displayed in two controls are not connected.
            control.DataSource = entities;
            if ( !string.IsNullOrEmpty( label ) )
            {
                var entity = entities.SelectEntity( label );
                if ( entity is object )
                {
                    control.SelectedValue = entity.Id;
                }
                else
                {
                    control.Text = label;
                }
            }
            // doing this the control may display text even if none selected! control.SelectedIndex = -1
            control.Enabled = wasEnabled;
            control.Invalidate();
            return entities.Count();
        }

        /// <summary> Select entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="entities"> The entities. </param>
        /// <param name="label">    The label. </param>
        /// <returns> An IKeyLabelTime. </returns>
        public static INominal SelectEntity( this IEnumerable<INominal> entities, string label )
        {
            var entity = entities.Where( x => string.Equals( x.Label, label, StringComparison.OrdinalIgnoreCase ) ).FirstOrDefault();
            return entity;
        }

        /// <summary>   An  <see cref="IEnumerable{INominal}"/> extension method that selects a value for the control . </summary>
        /// <remarks>   David, 2021-05-10. </remarks>
        /// <param name="entities"> The entities. </param>
        /// <param name="control">  The control. </param>
        public static void SelectValue( this IEnumerable<INominal> entities, ListControl control )
        {
            string label = control.Text;
            if ( !string.IsNullOrEmpty( label ) )
            {
                var entity = entities.SelectEntity( label );
                if ( entity is object )
                {
                    control.SelectedValue = entity.Id;
                }
            }
        }

        #endregion

    }
}

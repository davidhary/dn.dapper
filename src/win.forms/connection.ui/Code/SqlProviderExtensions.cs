using System;

using Dapper;

using isr.Dapper.Entity;

namespace isr.Dapper.ConnectionUI.SqlProviderExtensions
{

    /// <summary>   <see cref="isr.Dapper.ConnectionUI.SqlProviderExtensions"/> methods. </summary>
    /// <remarks>   David, 2020-10-03. </remarks>
    public static class Methods
    {

        /// <summary> Establish master provider. </summary>
        /// <remarks> David, 2020-04-01. </remarks>
        /// <param name="provider">              The provider. </param>
        /// <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, MasterProvider As SqlServerProvider)
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public static (bool Success, string Details, SqlServerProvider MasterProvider) EstablishMasterProvider( this SqlServerProvider provider, bool includeInitialCatalog )
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = $"invoking { nameof( isr.Dapper.Entity.SqlServerProvider.BuildMasterConnectionString )}";
            string masterConnectionString;
            SqlServerProvider masterProvider = null;
            // check if user has access to the credentials tables
            try
            {
                masterConnectionString = provider.BuildMasterConnectionString( includeInitialCatalog );
                activity = $"invoking {nameof( SqlServerProvider.Create )}({masterConnectionString})";
                masterProvider = SqlServerProvider.Create( masterConnectionString );
                activity = $"1st invoking {nameof( SqlServerProvider.ServerPrincipalExists )} for {provider.Credentials.UserId}";
                result = SqlServerProvider.ServerPrincipalExists( masterProvider.GetConnection(), provider.Credentials.UserId ) ? (true, string.Empty) : (false, $"{activity} for '{provider.Credentials.UserId}' returned false");
            }
            catch ( System.Data.SqlClient.SqlException ex ) when ( ex.ErrorCode == -2146232060 )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                result = (false, $"Exception {activity}'. {ex}");
            }
            catch ( Exception ex )
            {
                _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                result = (false, $"Exception {activity}'. {ex}");
            }

            if ( !result.Success )
            {
                activity = $"invoking {nameof( Microsoft.Data.ConnectionUI.Dialog.Dialogs.ShowSqlDataSourceConnectionDialog )}({provider.ConnectionString})";
                try
                {
                    // get a new connection using SA credentials
                    (bool Success, string ConnectionString) r;
                    r = Microsoft.Data.ConnectionUI.Dialog.Dialogs.ShowSqlDataSourceConnectionDialog( provider.ConnectionString );
                    if ( r.Success )
                    {
                        activity = $"invoking {nameof( SqlServerProvider.Create )}";
                        masterProvider = SqlServerProvider.Create( r.ConnectionString );
                        activity = $"2nd invoking {nameof( SqlServerProvider.ServerPrincipalExists )}({provider.Credentials.UserId})";
                        result = SqlServerProvider.ServerPrincipalExists( masterProvider.GetConnection(), provider.Credentials.UserId ) ? (true, string.Empty) : (false, $"{activity} for '{provider.Credentials.UserId}' returned false");
                    }
                    else
                    {
                        masterProvider = null;
                    }
                }
                catch ( Exception ex )
                {
                    _ = isr.Dapper.Entity.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( ex );
                    result = (false, $"Exception {activity}'. {ex}");
                }
            }

            return (result.Success, result.Details, masterProvider);
        }

        /// <summary> Establishes login credentials. </summary>
        /// <remarks> David, 2020-03-21. </remarks>
        /// <param name="provider">              The provider. </param>
        /// <param name="includeInitialCatalog"> True to include, false to exclude the initial catalog. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) EstablishLoginCredentials( this SqlServerProvider provider, bool includeInitialCatalog )
        {
            (bool Success, string Details) result;
            (bool Success, string Details, SqlServerProvider MasterProvider) r;
            r = provider.EstablishMasterProvider( includeInitialCatalog );
            result = (r.Success, r.Details);
            if ( result.Success )
            {
                result = SqlServerProvider.EstablishLoginCredentials( r.MasterProvider, provider.DatabaseName, provider.Credentials.UserId, provider.Credentials.Password );
            }

            return result;
        }

        /// <summary> Query if database exists. </summary>
        /// <remarks> David, 2020-03-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when failed to establish master
        /// credentials. </exception>
        /// <param name="provider"> The provider. </param>
        /// <returns> True if database exists, false if not. </returns>
        public static bool IsDatabaseExists( this SqlServerProvider provider )
        {
            (bool Success, string Details, SqlServerProvider MasterProvider) r;
            r = provider.EstablishMasterProvider( false );
            return r.Success
                ? provider.DatabaseExists( r.MasterProvider.GetConnection(), provider.DatabaseName )
                : throw new InvalidOperationException( $"Failed establishing master provider credentials;. {r.Details}" );
        }

        /// <summary>
        /// Establish login credentials for this connection and determine if connection can open.
        /// </summary>
        /// <remarks> David, 2020-03-21. </remarks>
        /// <param name="provider"> The provider. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public static (bool Success, string Details) EstablishConnectionCredentials( this SqlServerProvider provider )
        {
            _ = provider.ConnectionString;
            (bool Success, string Details) result = (true, string.Empty);
            (bool Success, string Details) establishLoginResult = (false, string.Empty);
            try
            {
                result = provider.GetConnection().CanOpen();
            }
            catch ( System.Data.SqlClient.SqlException ex ) when ( ex.ErrorCode == -2146232060 )
            {
                // checks if we have a user login issue
                establishLoginResult = provider.EstablishLoginCredentials( false );
            }
            catch ( Exception )
            {
                // checks if we have a user login issue
                establishLoginResult = provider.EstablishLoginCredentials( false );
            }

            if ( !result.Success && establishLoginResult.Success )
            {
                result = provider.GetConnection().CanOpen();
            }

            return result;
        }
    }
}

namespace isr.Dapper.Morphe.MSTest
{
    /// <summary>   A settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( Settings ) )]
    internal class Settings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public Settings() : base( System.Reflection.Assembly.GetAssembly( typeof( Settings ) ) )
        { }

        private bool _CheckUnpublishedMessageLogFileSize;

        public bool CheckUnpublishedMessageLogFileSize
        {
            get => this._CheckUnpublishedMessageLogFileSize;
            set {
                if ( !bool.Equals( this.CheckUnpublishedMessageLogFileSize, value ) )
                {
                    this._CheckUnpublishedMessageLogFileSize = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _Dummy;

        /// <summary>   Gets or sets the dummy. </summary>
        /// <value> The dummy. </value>
        public string Dummy
        {
            get => this._Dummy;
            set {
                if ( !string.Equals( value, this.Dummy ) )
                {
                    this._Dummy = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

    }
}

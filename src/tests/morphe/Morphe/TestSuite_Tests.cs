using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using isr.Dapper.Entities;
using isr.Dapper.Entities.MSTest;

namespace isr.Dapper.Morphe.MSTest
{
    public partial class TestSuite
    {

        #region " LOOKUP TABLES "

        /// <summary> (Unit Test Method) tests measurement bin entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void MeasurementBinEntityTest()
        {
            Auditor.AssertReadingBinEntity( this.GetProvider(), ReadingBin.Good );
        }

        /// <summary> (Unit Test Method) tests part specification type entity. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        [TestMethod]
        public void PartSpecificationTypeEntityTest()
        {
            Auditor.AssertPartSpecificationTypeEntity( this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests Polarity entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void PolarityEntityTest()
        {
            Auditor.AssertPolarityEntity( this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests Structure Type entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void StructureTypeEntityTest()
        {
            Auditor.AssertStructureTypeEntity( this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests the Substrate Type entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void SubstrateTypeEntityTest()
        {
            Auditor.AssertSubstrateTypeEntity( this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests the test level entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void TestTypeEntityTest()
        {
            Auditor.AssertTestTypeEntity( this.GetProvider(), TestType.Trim );
        }

        /// <summary> (Unit Test Method) tests Tolerance entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void ToleranceEntityTest()
        {
            Auditor.AssertToleranceEntity( this.GetProvider() );
        }

        #endregion

        #region " PLATFORM "

#if false 
        ''' <summary> (Unit Test Method) tests revision entity. </summary>
        ''' <remarks> David, 2020-05-08. </remarks>
        <TestMethod>
        Public Sub RevisionEntityTest()
            Auditor.AssertRevisionEntity(TestSuite.TestInfo, Me.GetProvider, TestSuite.BuiltDatabaseVersion);
        End Sub

        ''' <summary> (Unit Test Method) tests Platform entity. </summary>
        <TestMethod>
        Public Sub PlatformEntityTest()
            Auditor.AssertPlatformEntity(TestSuite.TestInfo, Me.GetProvider);
        End Sub

        ''' <summary> (Unit Test Method) tests station entity. </summary>
        <TestMethod>
        Public Sub StationEntityTest()
            Auditor.AssertStationEntity(TestSuite.TestInfo, Me.GetProvider, True);
        End Sub
#endif

        #endregion

        #region " SESSION "

        /// <summary> (Unit Test Method) tests session entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void SessionEntityTest()
        {
            var lotNumbers = new string[] { "1725356", "1770169" };
            _ = Auditor.AssertAddingSessionEntity( this.GetProvider(), $"{lotNumbers[0]}:1" );
        }

        /// <summary> (Unit Test Method) tests part entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void PartEntityTest()
        {
            var partNumbers = new string[] { "PFC-W0402LF-03-1000-B-4013", "PFC-W0402LF-03-1001-B-4013", "PFC-D1206LF-03-3202-1002-DB-2728", "PFC-D1206LF-03-6122-2442-FB-2728", "WIN-T0603LF-03-3091-B", "WIN-T0603LF-03-2002-B", "WIN-T0603LF-03-1002-B" };
            Auditor.AssertPartEntity( this.GetProvider(), partNumbers );
        }

        /// <summary> (Unit Test Method) tests lot entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void LotEntityTest()
        {
            // PFC-D1206LF-03-6122-2442-FB-2728  1725356 1770169
            // PFC-W0402LF-03-1000-B-4013  1749153  1749156 1725105
            // PFC-D1206LF-03-3202-1002-DB-2728 1733251 1733247  1733248  1733249
            string partNumber = "PFC-D1206LF-03-6122-2442-FB-2728";
            var lotNumbers = new string[] { "1725356", "1770169" };
            Auditor.AssertLotEntity( this.GetProvider(), partNumber, lotNumbers );
        }

        /// <summary> (Unit Test Method) tests substrate entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void SubstrateEntityTest()
        {
            string partNumber = "PFC-D1206LF-03-3202-1002-DB-2728";
            var lotNumbers = new string[] { "1733251", "1733247" };
            Auditor.AssertSubstrateEntity( this.GetProvider(), partNumber, lotNumbers[0] );
        }

        /// <summary> (Unit Test Method) tests Structure entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void StructureEntityTest()
        {
            string partNumber = "PFC-D1206LF-03-3202-1002-DB-2728";
            var lotNumbers = new string[] { "1733251", "1733247" };
            Auditor.AssertSubstrateEntity( this.GetProvider(), partNumber, lotNumbers[0] );
        }

#if false
        ''' <summary> (Unit Test Method) tests Structure session entity. </summary>
        <TestMethod>
        Public Sub StructureSessionEntityTest()
            string partNumber = "PFC-D1206LF-03-6122-2442-FB-2728";
            string[] lotNumbers = New String[] { "1725356", "1770169"};
            string lotNumber = lotNumbers( 0 );
            Auditor.AssertSessionStructure(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumber, $"{lotNumber}:1");
        End Sub
#endif
        #endregion

    }
}

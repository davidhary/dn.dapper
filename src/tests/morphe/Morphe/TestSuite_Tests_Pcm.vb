'---------------------------------------------------------------------------------------------------
' file:	.\Morphe\TestSuite_Tests_Pcm.vb
'
' summary:	Test suite tests PCM class
'---------------------------------------------------------------------------------------------------
Imports Dapper.Contrib.Extensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities
Namespace Global.isr.Dapper.Entities.Tests.Ohmni.Morphe

    Partial Public Class TestSuite

        #Region " PCM "
#If PCM Then

        ''' <summary> (Unit Test Method) tests Cross Edge Type entity. </summary>
        <TestMethod>
        Public Sub CrossEdgeEntityTest()
            Auditor.AssertCrossEdgeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Line Edge Type entity. </summary>
        <TestMethod>
        Public Sub LineEdgeEntityTest()
            Auditor.AssertLineEdgeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests line structure nominal entity. </summary>
        <TestMethod>
        Public Sub LineStructureNominalEntityTest()
            Auditor.AssertLineStructureNominalEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests line edge resistance entity. </summary>
        <TestMethod>
        Public Sub LineEdgeResistanceEntityTest()
            Auditor.AssertLineEdgeResistanceEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Cross edge resistance entity. </summary>
        <TestMethod>
        Public Sub CrossEdgeResistanceEntityTest()
            Auditor.AssertCrossEdgeResistanceEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests line structure entity. </summary>
        <TestMethod>
        Public Sub LineStructureEntityTest()
            Auditor.AssertLineStructureEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Structure line entity. </summary>
        <TestMethod>
        Public Sub StructureLineEntityTest()
            Auditor.AssertStructureLineEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Structure line nominal entity. </summary>
        <TestMethod>
        Public Sub StructureLineNominalEntityTest()
            Auditor.AssertStructureLineNominalEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests substrate line nominal entity. </summary>
        <TestMethod>
        Public Sub SubstrateLineNominalEntityTest()
            Auditor.AssertSubstrateLineNominalEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub
#End If
        #End Region

    End Class

End Namespace

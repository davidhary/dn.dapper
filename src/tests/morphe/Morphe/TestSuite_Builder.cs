using System;

using Dapper;

using isr.Dapper.Entity;
using isr.Dapper.Entities;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Morphe.MSTest
{
    public partial class TestSuite
    {

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="provider"> The provider. </param>
        public static void Initialize( ProviderBase provider )
        {
            Console.Out.WriteLine( $".NET {Environment.Version}" );
            Console.Out.WriteLine( $"Dapper {typeof( SqlMapper ).AssemblyQualifiedName}" );
            Console.Out.WriteLine( $"Connection string: {provider.ConnectionString}" );
            Provider = provider;
            // test naked connection
            Assert.IsTrue( Provider.IsConnectionExists(), $"connection {Provider.GetConnection()} does not exist" );
            // test connection with event handling
            using ( var connection = Provider.GetConnection() )
            {
                Assert.IsTrue( connection.Exists(), "connection does not exist" );
            }

            if ( provider is null )
            {
                SchemaBuilder = new SchemaBuilder();
            }
            else
            {
                SchemaBuilder = new SchemaBuilder() { Provider = provider };
                EnumerateSchemaObjects();
            }

            BuildDatabase( provider.BuildMasterConnectionString( false ) );
            SchemaBuilder.AssertSchemaObjectsExist();
        }

        /// <summary> Cleanups this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public static void Cleanup()
        {
            if ( TableCategory.None != AppSettings.Instance.MorpheTestsInfo.DeleteOption )
            {
                SchemaBuilder.DeleteAllRecords( AppSettings.Instance.MorpheTestsInfo.DeleteOption );
            }

            if ( TableCategory.None != AppSettings.Instance.MorpheTestsInfo.DropTablesOption )
            {
                SchemaBuilder.DropAllTables( AppSettings.Instance.MorpheTestsInfo.DropTablesOption, AppSettings.Instance.MorpheTestsInfo.IgnoreDropTableErrors );
            }
        }

        /// <summary> Gets or sets the provider. </summary>
        /// <value> The provider. </value>
        public static ProviderBase Provider { get; private set; }

        /// <summary> Gets or sets the schema builder. </summary>
        /// <value> The schema builder. </value>
        public static SchemaBuilder SchemaBuilder { get; private set; }

        /// <summary> Enumerate schema objects. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        public static void EnumerateSchemaObjects()
        {
            try
            {
                EnumerateSchemaObjectsThis();
            }
            catch
            {
                SchemaBuilder.EnumeratedTables.Clear();
                throw;
            }
        }

        /// <summary> Enumerate schema objects this. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        private static void EnumerateSchemaObjectsThis()
        {
            string providerFileLabel = Provider.ProviderType == ProviderType.SQLite ? AppSettings.Instance.MorpheTestsInfo.SQLiteFileLabel : AppSettings.Instance.MorpheTestsInfo.SqlServerFileLabel;
            var schemaBuilder = SchemaBuilder;
            schemaBuilder.EnumeratedTables.Clear();
            var tb = new TableMaker();

            // CORE LOOKUP TABLES 
            tb = new TableMaker() {
                TableName = ReadingBinBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ReadingBinBuilder.Instance.CreateTable,
                InsertAction = ReadingBinBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = PolarityBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = PolarityBuilder.Instance.CreateTable,
                InsertAction = PolarityBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = TestTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = TestTypeBuilder.Instance.CreateTable,
                InsertAction = TestTypeBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = ToleranceBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ToleranceBuilder.CreateTable,
                InsertAction = ToleranceBuilder.InsertValues
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // FILE IMPORT 
            tb = new TableMaker() {
                TableName = FileImportStateBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = FileImportStateBuilder.Instance.CreateTable,
                InsertAction = FileImportStateBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // IMPORT FILE
            tb = new TableMaker() {
                TableName = ImportFileBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => ImportFileBuilder.CreateTable( x, false )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
#if False
                InsertFileValuesAction = Entities.ImportFileBuilder.InsertFileValues,
                ValuesFileName = System.IO.Path.Combine( TestSuiteSettings.Get.SchemaFolderName,
                                                         String.Format( AppSettings.Instance.MorpheTestsInfo.AddImportFileRecordsFileNameFormat, providerFileLabel ) )};
#endif

            // STATION
            tb = new TableMaker() {
                TableName = ComputerBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => ComputerBuilder.Instance.CreateTable( x, true )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // if the station is part of a platform, make the station label non-unique to allow same name for different platforms.
            tb = new TableMaker() {
                TableName = StationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => StationBuilder.Instance.CreateTable( x, true )
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = ComputerStationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ComputerStationBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = SessionBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = SessionBuilder.Instance.CreateTableNonUniqueLabel
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART 
            tb = new TableMaker() {
                TableName = PartBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PartBuilder.Instance.CreateTableUniqueLabel
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = PartNamingTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = x => {
                    var s = new PartNamingTypeBuilder();
                    return s.CreateTable( x );
                },
                InsertAction = x => {
                    var s = new PartNamingTypeBuilder();
                    return s.InsertIgnoreDefaultRecords( x );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = LotBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = LotBuilder.Instance.CreateTableUniqueLabel
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = PartLotBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PartLotBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SUBSTRATE
            tb = new TableMaker() {
                TableName = GradeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = GradeBuilder.Instance.CreateTable,
                InsertAction = GradeBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = SubstrateTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = SubstrateTypeBuilder.Instance.CreateTable,
                InsertAction = SubstrateTypeBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = SubstrateBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ( x ) => {
                    var s = new SubstrateBuilder();
                    return s.CreateTable( x, UniqueIndexOptions.None );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SUB-STRUCTURES
            tb = new TableMaker() {
                TableName = CrossEdgeBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CrossEdgeBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = LineEdgeBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = LineEdgeBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

#if PCM
            tb = New TableMaker {TableName = LineStructureBuilder.TableName, Category = TableCategory.None,
                                CreateAction = LineStructureBuilder.CreateTable};
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
#endif

            // STRUCTURES
            tb = new TableMaker() {
                TableName = StructureTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = StructureTypeBuilder.Instance.CreateTable,
                InsertAction = StructureTypeBuilder.Instance.InsertIgnoreDefaultRecords
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = StructureBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ( x ) => {
                    var s = new StructureBuilder();
                    return s.CreateTable( x, UniqueIndexOptions.None );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = SessionStructureBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = SessionStructureBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PCM STRUCTURES
            tb = new TableMaker() {
                TableName = CrossEdgeResistanceBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CrossEdgeResistanceBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
            tb = new TableMaker() {
                TableName = LineEdgeResistanceBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = LineEdgeResistanceBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

#if PCM 
            tb = New TableMaker {TableName = StructureLineBuilder.TableName, Category = TableCategory.None,
                                 CreateAction = AddressOf StructureLineBuilder.CreateTable};
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = New TableMaker {TableName = LineStructureNominalBuilder.TableName, Category = TableCategory.None,
                                 CreateAction = AddressOf LineStructureNominalBuilder.CreateTable};
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = New TableMaker {TableName = StructureLineNominalBuilder.TableName, Category = TableCategory.None,
                                 CreateAction = AddressOf StructureLineNominalBuilder.CreateTable};
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            tb = New TableMaker {TableName = SubstrateLineStructureNominalBuilder.TableName, Category = TableCategory.None,
                                 CreateAction = AddressOf SubstrateLineStructureNominalBuilder.CreateTable};
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
#endif

            // REVISION
            BuiltDatabaseVersion = AppSettings.Instance.MorpheTestsInfo.DatabaseRevision;
            tb = new TableMaker() {
                TableName = RevisionBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = RevisionBuilder.Instance.CreateTableUniqueLabel,
                PostCreateAction = RevisionBuilder.UpsertRevision,
                PostCreateValue = BuiltDatabaseVersion
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
        }

        /// <summary> Gets or sets the built database version. </summary>
        /// <value> The built database version. </value>
        public static string BuiltDatabaseVersion { get; set; }

        /// <summary> Creates data base. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void CreateDatabase( string masterConnectionString )
        {
            if ( AppSettings.Instance.MorpheTestsInfo.CreateDatabase || !Provider.DatabaseExists() )
            {
                if ( System.IO.File.Exists( Provider.FileName ) )
                    System.IO.File.Delete( Provider.FileName );
                // using the temp db.
                //  _ = CreateDatabaseThis( masterConnectionString, AppSettings.Instance.MorpheTestsInfo.SchemaFileNameFormat );
                //  _ = CreateDatabaseThis( masterConnectionString, AppSettings.Instance.MorpheTestsInfo.SchemaDataFileNameFormat );
                _ = CreateDatabaseThis( masterConnectionString, string.Empty );
            }
        }

        /// <summary>   Creates database. </summary>
        /// <remarks>   David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString">       The master connection string. </param>
        /// <param name="createDatabaseFileNameFormat"> The create database file name format. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        private static bool CreateDatabaseThis( string masterConnectionString, string createDatabaseFileNameFormat )
        {
            bool result = Provider.CreateDatabase( masterConnectionString, Provider.DatabaseName );
            int count;
            string schemaFileName;
            if ( result && !string.IsNullOrEmpty( createDatabaseFileNameFormat ) )
            {
                string providerFileLabel = Provider.ProviderType == ProviderType.SQLite ? AppSettings.Instance.MorpheTestsInfo.SQLiteFileLabel : AppSettings.Instance.MorpheTestsInfo.SqlServerFileLabel;
                schemaFileName = System.IO.Path.Combine( AppSettings.Instance.MorpheTestsInfo.SchemaFolderName, string.Format( createDatabaseFileNameFormat, providerFileLabel ) );
                count = Provider.ExecuteScript( schemaFileName );
                result = count > 0;
            }
            return result;
        }

        /// <summary> Create or clear database and add all tables. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void BuildDatabase( string masterConnectionString )
        {
            CreateDatabase( masterConnectionString );
            if ( AppSettings.Instance.MorpheTestsInfo.ClearDatabase )
                SchemaBuilder.DropAllTables( AppSettings.Instance.MorpheTestsInfo.IgnoreDropTableErrors );
            SchemaBuilder.BuildSchema();
        }
    }
}

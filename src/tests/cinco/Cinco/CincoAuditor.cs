using System;
using System.Linq;

using Dapper.Contrib.Extensions;

using FastEnums;
using isr.Dapper.Entity;
using isr.Dapper.Entities;
using isr.Dapper.Ohmni;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Cinco.MSTest
{

    /// <summary> An Cinco auditor. </summary>
    /// <remarks>
    /// David, 2020-02-25 <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed class CincoAuditor
    {

        #region " ATTRIBUTE TYPE ENTITY "

        /// <summary> Assert the Attribute Type entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertAttributeTypeEntity( ProviderBase provider )
        {
            int id = 100;
            var entity = new OhmniAttributeTypeEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = OhmniAttributeTypeBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            bool expectedFetched = false;
            r = entity.ValidateNewEntity( $"{nameof( OhmniAttributeTypeEntity )} with '{nameof( OhmniAttributeTypeEntity.AttributeTypeId )}' of  {id}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualFetched = entity.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {id} should not exist" );
            r = entity.ValidateNewEntity( $"{nameof( OhmniAttributeTypeEntity )} with '{nameof( OhmniAttributeTypeEntity.AttributeTypeId )}' of  {id}" );
            Assert.IsTrue( r.Success, r.Details );
            id = 1;
            actualFetched = entity.FetchUsingKey( connection, id );
            Assert.IsTrue( actualFetched, $"key {id} should exist" );
            r = entity.ValidateStoredEntity( $"Stored {nameof( OhmniAttributeTypeEntity )} with '{nameof( OhmniAttributeTypeEntity.AttributeTypeId )}' of  {id}" );
            Assert.IsTrue( r.Success, r.Details );
            string name = AttributeType.TrackingTolerance.ToString();
            entity.Name = name;
            r = entity.ValidateChangedEntity( $"Changed {nameof( OhmniAttributeTypeEntity )} '{nameof( OhmniAttributeTypeEntity.Name )}' to {name}" );
            Assert.IsTrue( r.Success, r.Details );
            actualFetched = entity.FetchUsingUniqueIndex( connection );
            Assert.IsTrue( actualFetched, $"Name {name} should exist" );
            Assert.AreEqual( name, entity.Name, $"Name {name} should exist" );
            r = entity.ValidateStoredEntity( $"{nameof( OhmniAttributeTypeEntity )} with '{nameof( OhmniAttributeTypeEntity.Name )}' of {name}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 2;
            int actualCount = entity.FetchAllEntities( connection );
            Assert.IsTrue( actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} Attribute types" );
        }

        #endregion

        #region " LOT "

        /// <summary> Assert adding lot entity. </summary>
        /// <remarks> David, 2020-05-18. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <param name="lotNumber">  The lot number. </param>
        /// <returns> An OhmniLotEntity. </returns>
        public static CincoLotEntity AssertAddingLotEntity( ProviderBase provider, string partNumber, string lotNumber )
        {
            var lot = new CincoLotEntity();
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                string tableName = CincoLotBuilder.TableName;
                Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
                Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
                var partEntity = AssertAddingPartEntity( provider, partNumber );
                lot = new CincoLotEntity() { LotNumber = lotNumber, PartAutoId = partEntity.PartAutoId, PartNumber = partEntity.PartNumber };
                bool actualInserted = lot.Obtain( connection );
                bool expectedInserted = true;
                Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( isr.Dapper.Ohmni.CincoLotEntity )}  with '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )}' of {lot.LotNumber} should have been inserted" );
                r = lot.ValidateStoredEntity( $"Insert {nameof( isr.Dapper.Ohmni.CincoLotEntity )} with '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )}' of {lotNumber}" );
                Assert.IsTrue( r.Success, r.Details );
            }

            return lot;
        }

        /// <summary> Asserts lot entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertLotEntity( ProviderBase provider )
        {
            int id = 3;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = CincoLotBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<CincoLotNub>();
            _ = connection.DeleteAll<CincoPartNub>();
            string partNumber = "Part1";
            int partId = 1;
            var part = new CincoPartEntity() { PartNumber = partNumber };
            _ = part.Insert( connection );
            partId = part.PartAutoId;
            bool expectedFetched = false;
            bool actualFetched = false;
            int expectedCount = 0;
            var lot = new CincoLotEntity();
            actualFetched = lot.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( isr.Dapper.Ohmni.CincoLotEntity )} with '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotAutoId )}' of {id} should not exist" );
            r = lot.ValidateNewEntity( $"New {nameof( isr.Dapper.Ohmni.CincoLotEntity )} with '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotAutoId )}' of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            lot.Clear();
            string lotNumber = "lot1";
            lot = new CincoLotEntity() { LotNumber = lotNumber, PartAutoId = partId, PartNumber = partNumber };
            bool actualInserted = lot.Insert( connection );
            expectedCount += 1;
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( isr.Dapper.Ohmni.CincoLotEntity )}  with '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )}' of {lot.LotNumber} should have been inserted" );
            r = lot.ValidateStoredEntity( $"Insert {nameof( isr.Dapper.Ohmni.CincoLotEntity )} with '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )}' of {lotNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            _ = lot.FetchUsingKey( connection );
            var expectedTimestamp = DateTime.UtcNow;
            var actualTimeStamp = lot.Timestamp;
            Asserts.Instance.AreEqual( expectedTimestamp, actualTimeStamp, TimeSpan.FromSeconds( 10d ), "Expected timestamp" );
            _ = lot.FetchPartEntity( connection );
            Assert.AreEqual( part.PartNumber, lot.PartEntity.PartNumber, $"Fetch {nameof( isr.Dapper.Ohmni.CincoPartEntity )} using '{nameof( isr.Dapper.Ohmni.CincoLotEntity.PartAutoId )}' of {lot.PartAutoId} should match the {nameof( isr.Dapper.Ohmni.CincoPartEntity )} '{nameof( isr.Dapper.Ohmni.CincoPartEntity.PartNumber )} '" );
            r = lot.PartEntity.ValidateStoredEntity( $"Fetched {nameof( isr.Dapper.Ohmni.CincoPartEntity )} from  {nameof( isr.Dapper.Ohmni.CincoLotEntity )} '[{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )},{nameof( isr.Dapper.Ohmni.CincoLotEntity.PartNumber )}]' of [{lotNumber},{partNumber}]" );
            Assert.IsTrue( r.Success, r.Details );
            lotNumber = "Lot2";
            lot.LotNumber = lotNumber;
            r = lot.ValidateChangedEntity( $"Changed {nameof( isr.Dapper.Ohmni.CincoLotEntity )} with '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )}' of {lotNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualUpdate = lot.Update( connection, UpdateModes.Refetch );
            bool expectedUpdate = true;
            Assert.AreEqual( expectedUpdate, actualUpdate, $"{nameof( isr.Dapper.Ohmni.CincoLotEntity )} with '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )}' of {lot.LotNumber} should be updated" );
            r = lot.ValidateStoredEntity( $"Updated {nameof( isr.Dapper.Ohmni.CincoLotEntity )} with '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )}' of {lotNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            actualUpdate = lot.Update( connection, UpdateModes.Refetch );
            expectedUpdate = false;
            Assert.AreEqual( expectedUpdate, actualUpdate, $"Updated {nameof( isr.Dapper.Ohmni.CincoLotEntity )} with unchanged '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )}' of {lot.LotNumber} should be clean" );
            r = lot.ValidateStoredEntity( $"Updated {nameof( isr.Dapper.Ohmni.CincoLotEntity )} with unchanged '{nameof( isr.Dapper.Ohmni.CincoLotEntity.LotNumber )}' of {lot.LotNumber}" );
            Assert.IsTrue( r.Success, r.Details );

            // Insert must be done on a new lot; setting the lot number and inserting leaves the proxy 'dirty'. lot.LotNumber = "Lot1"
            lot = new CincoLotEntity() { LotNumber = "Lot1", PartAutoId = partId, PartNumber = partNumber };
            actualInserted = lot.Insert( connection );
            expectedCount += 1;
            Assert.AreEqual( expectedCount, CincoLotEntity.CountLots( connection, lot.PartNumber ), $"{expectedCount} lots should be counted for {lot.PartNumber}" );
            Assert.IsTrue( actualInserted, $"Lot {lot.LotNumber} should have been inserted" );
            Assert.AreEqual( 1, CincoLotEntity.CountEntities( connection, lot.LotNumber ), $"Single lot should be counted for {lot.PartNumber}.{lot.LotNumber}" );
            Assert.IsTrue( CincoLotEntity.IsExists( connection, lot.LotNumber ), $"Lot {lot.PartNumber}.{lot.LotNumber} should {nameof( CincoLotEntity.IsExists )}" );
            lotNumber = lot.LotNumber;
            int partAutoId = lot.PartAutoId;
            actualFetched = lot.Obtain( connection );
            expectedFetched = true;
            Assert.AreEqual( expectedFetched, actualFetched, $"Lot {lot.PartNumber}.{lot.LotNumber} should have been obtained" );
            Assert.AreEqual( partNumber, lot.PartNumber, $"Obtained Lot {lot.PartNumber}.{lot.LotNumber} should match part number" );
            Assert.AreEqual( partAutoId, lot.PartAutoId, $"Obtained Part auto id {lot.PartAutoId} should match part auto id" );
            _ = connection.DeleteAll<CincoLotNub>();
            expectedCount = 10;
            for ( int i = 1, loopTo = expectedCount; i <= loopTo; i++ )
            {
                lot = new CincoLotEntity() { LotNumber = $"Lot{i}", PartAutoId = part.PartAutoId, PartNumber = part.PartNumber };
                actualInserted = lot.Insert( connection );
                expectedInserted = true;
                Assert.AreEqual( expectedInserted, actualInserted, $"Lot {lot.LotNumber} should have been inserted" );
            }

            int actualCount = lot.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple Lots should have been inserted" );
            lotNumber = lot.LotNumber;
            bool actualDelete = lot.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Lot {lotNumber} should be deleted" );
            expectedCount -= 1;
            ICincoLot lot0 = lot.Lots.ElementAtOrDefault( 0 );
            actualDelete = CincoLotEntity.Delete( connection, lot0.LotAutoId );
            expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Lot {lot0.LotNumber} should be deleted" );
            expectedCount -= 1;
            part = new CincoPartEntity();
            _ = part.FetchUsingKey( connection, partId );
            actualCount = part.FetchLots( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple Lots should have been fetched for part {part.PartNumber}" );
            var samePart = new CincoPartEntity();
            _ = samePart.FetchLots( connection, part.PartAutoId );
            actualCount = samePart.Lots.Count();
            Assert.AreEqual( part.PartAutoId, samePart.PartAutoId, $"same part should be fetched" );
            Assert.AreEqual( part.PartNumber, samePart.PartNumber, $"same part should be fetched" );
            Assert.AreEqual( expectedCount, actualCount, $"multiple Lots should have been fetched" );
        }

        #endregion

        #region " PART "

        /// <summary> Assert add part entity. </summary>
        /// <remarks> David, 2020-05-17. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <returns> An OhmniPartEntity. </returns>
        public static CincoPartEntity AssertAddingPartEntity( ProviderBase provider, string partNumber )
        {
            var part = new CincoPartEntity();
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                part.PartNumber = partNumber;
                bool actualInserted = part.Obtain( connection );
                Assert.AreEqual( partNumber, part.PartNumber, true, $"{nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} of {partNumber} must equals the saved number {part.PartNumber}" );
                r = part.ValidateStoredEntity( $"Inserted {nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} of {partNumber}" );
                Assert.IsTrue( r.Success, r.Details );
            }

            return part;
        }

        /// <summary> Asserts part entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertPartEntity( ProviderBase provider )
        {
            int id = 3;
            var part = new CincoPartEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            int expectedCount = 0;
            string tableName = CincoPartBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<CincoPartNub>();
            bool expectedFetched = false;
            bool actualFetched = part.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( PartEntity )} with {nameof( PartEntity.AutoId )} of {id} should not exist" );
            r = part.ValidateNewEntity( $"New {nameof( PartEntity )} with {nameof( PartEntity.AutoId )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            string partNumber = "Part1";
            part.PartNumber = partNumber;
            bool actualInserted = part.Insert( connection );
            Assert.AreEqual( partNumber, part.PartNumber, $"{nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} of {partNumber} must equals the saved number {part.PartNumber}" );
            r = part.ValidateStoredEntity( $"Inserted {nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} of {partNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            _ = part.FetchUsingKey( connection );
            var expectedTimestamp = DateTime.UtcNow;
            var actualTimeStamp = part.Timestamp;
            Asserts.Instance.AreEqual( expectedTimestamp, actualTimeStamp, TimeSpan.FromSeconds( 10d ), "Expected timestamp" );
            expectedCount += 1;
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"Part {part.PartNumber} should have been inserted" );
            partNumber = "Part2";
            part.PartNumber = partNumber;
            r = part.ValidateChangedEntity( $"Changed {nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} to {partNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualUpdated = part.Update( connection, UpdateModes.Refetch );
            bool expectedUpdated = true;
            Assert.AreEqual( expectedUpdated, actualUpdated, $"{nameof( PartEntity )} with {part.PartNumber} update should match" );
            actualUpdated = part.Update( connection, UpdateModes.Refetch );
            Assert.IsFalse( actualUpdated, $"Part {part.PartNumber} should not be updated" );
            int actualCount = CincoPartEntity.CountEntities( connection, part.PartNumber );
            Assert.AreEqual( expectedCount, actualCount, $"Single part should be counted for {part.PartNumber}" );
            Assert.IsTrue( CincoPartEntity.IsExists( connection, part.PartNumber ), $"Part {part.PartNumber} should {nameof( PartEntity.IsExists )}" );

            // set part proxy dirty
            actualUpdated = part.Update( connection, UpdateModes.Refetch | UpdateModes.ForceUpdate );
            Assert.IsTrue( actualUpdated, $"Part {part.PartNumber} should be updated after setting proxy dirty" );
            actualUpdated = part.Update( connection );
            Assert.IsTrue( actualUpdated, $"Part {part.PartNumber} should be clean after update" );
            partNumber = part.PartNumber;
            int AutoId = part.PartAutoId;
            actualFetched = part.Obtain( connection );
            expectedFetched = true;
            Assert.AreEqual( expectedFetched, actualFetched, $"Part {part.PartNumber} should have been obtained" );
            Assert.AreEqual( partNumber, part.PartNumber, $"Obtained Part number {part.PartNumber} should match" );
            Assert.AreEqual( AutoId, part.PartAutoId, $"Obtained Part auto id {part.PartAutoId} should match" );
            _ = connection.DeleteAll<CincoPartNub>();
            expectedCount = 10;
            for ( int i = 1, loopTo = expectedCount; i <= loopTo; i++ )
            {
                part = new CincoPartEntity() { PartNumber = $"Part{i}" };
                actualInserted = part.Insert( connection );
                expectedInserted = true;
                Assert.AreEqual( expectedInserted, actualInserted, $"Part {part.PartNumber} should have been inserted" );
            }

            actualCount = part.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple parts should have been inserted" );
            partNumber = part.PartNumber;
            bool actualDelete = part.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Part {partNumber} should be deleted" );
            var part0 = part.Parts.ElementAtOrDefault( 0 );
            actualDelete = CincoPartEntity.Delete( connection, part0.PartAutoId );
            expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Part {part0.PartNumber} should be deleted" );
            bool expectedUniquePartNumber = true;
            bool actualUniqueLabel = CincoPartBuilder.UsingUniqueLabel( connection );
            Assert.AreEqual( expectedUniquePartNumber, actualUniqueLabel, $"{nameof( CincoPartEntity )} '{nameof( CincoPartBuilder.UsingUniqueLabel )}' should match" );
        }

        #endregion

        #region " PART ATTRIBUTE RANGE "

        /// <summary> Asserts part attribute range. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertPartAttributeRangeEntity( ProviderBase provider )
        {
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = OhmniPartAttributeRangeBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<CincoPartNub>();
            _ = connection.DeleteAll<OhmniPartAttributeRangeNub>();
            var part = new CincoPartEntity() { PartNumber = "Part1" };
            _ = part.Insert( connection );
            var attributeType = AttributeType.Tcr;
            var partAttributeRange = new OhmniPartAttributeRangeEntity() {
                PartAutoId = part.PartAutoId,
                AttributeTypeId = ( int ) attributeType,
                Min = 0d,
                Max = 0.1d
            };
            bool actualInserted = partAttributeRange.Insert( connection );
            int expectedCount = 0;
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeEntity )} should have been inserted" );
            r = partAttributeRange.ValidateStoredEntity( $"Inserted {nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeEntity )} for part {part.PartNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            expectedCount += 1;
            int count = part.FetchPartAttributeRanges( connection );
            Assert.AreEqual( expectedCount, count, $"Part entity should bring the correct number of {nameof( isr.Dapper.Ohmni.OhmniPartAttributeRangeEntity )}" );
            _ = connection.DeleteAll<OhmniPartAttributeRangeNub>();
        }

        #endregion

        #region " PART ATTRIBUTE REAL "

        /// <summary> Asserts part attribute Value. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertPartAttributeValueEntity( ProviderBase provider )
        {
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = OhmniPartAttributeValueBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<CincoPartNub>();
            _ = connection.DeleteAll<OhmniPartAttributeValueNub>();
            var part = new CincoPartEntity() { PartNumber = "Part1" };
            _ = part.Insert( connection );
            var attributeType = AttributeType.NominalValue;
            var partAttributeValue = new OhmniPartAttributeValueEntity() { PartAutoId = part.PartAutoId, AttributeTypeId = ( int ) attributeType, Value = 1d };
            bool actualInserted = partAttributeValue.Insert( connection );
            int expectedCount = 0;
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( isr.Dapper.Ohmni.OhmniPartAttributeValueEntity )} should have been inserted" );
            r = partAttributeValue.ValidateStoredEntity( $"Inserted {nameof( isr.Dapper.Ohmni.OhmniPartAttributeValueEntity )} for part {part.PartNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            expectedCount += 1;
            int count = part.FetchPartAttributeValues( connection );
            Assert.AreEqual( expectedCount, count, $"Part entity should bring the correct number of {nameof( isr.Dapper.Ohmni.OhmniPartAttributeValueEntity )}" );
            _ = connection.DeleteAll<OhmniPartAttributeValueNub>();
        }

        #endregion

        #region " RELEASE HISTORY "

        /// <summary> Asserts release history entity. </summary>
        /// <remarks>
        /// Time zone issue with SQL Light.  Storing the SQL value in UTC returns back time in the
        /// timezone of the 'server'.
        /// </remarks>
        /// <param name="site">                  The site. </param>
        /// <param name="provider">              The provider. </param>
        /// <param name="activeDatabaseVersion"> The active database version. </param>
        public static void AssertReleaseHistoryEntity( ProviderBase provider, string activeDatabaseVersion )
        {
            string expectedActiveVersion = "1.0.0";
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = CincoReleaseHistoryBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            bool expectedFetched = false;
            bool actualFetched = false;
            var releaseHistory = new CincoReleaseHistoryEntity();
            actualFetched = releaseHistory.FetchUsingKey( connection, expectedActiveVersion );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( CincoReleaseHistoryEntity )} {nameof( CincoReleaseHistoryEntity.VersionNumber )} Of {expectedActiveVersion} should Not exist" );
            r = releaseHistory.ValidateNewEntity( $"New {nameof( CincoReleaseHistoryEntity )} With {nameof( CincoReleaseHistoryEntity.VersionNumber )} Of {expectedActiveVersion}" );
            Assert.IsTrue( r.Success, r.Details );
            releaseHistory.Clear();

            // clear the table; the table maybe empty, in which case the action returns False.
            _ = connection.DeleteAll<CincoReleaseHistoryNub>();
            int expectedCount = 0;
            int actualCount = CincoReleaseHistoryBuilder.UpdateReleaseHistory( connection, activeDatabaseVersion, DateTime.UtcNow, "Latest release" );
            Assert.IsTrue( actualCount >= 0, $"Release {activeDatabaseVersion} should exist Or updated" );
            expectedCount += 1;
            expectedActiveVersion = activeDatabaseVersion;
            actualFetched = releaseHistory.FetchUsingKey( connection, expectedActiveVersion );
            expectedFetched = true;
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( CincoReleaseHistoryEntity )} With {nameof( CincoReleaseHistoryEntity.VersionNumber )} Of {expectedActiveVersion} should exist" );
            r = releaseHistory.ValidateStoredEntity( $"Existing {nameof( CincoReleaseHistoryEntity )} With {nameof( CincoReleaseHistoryEntity.VersionNumber )} Of {expectedActiveVersion}" );
            Assert.IsTrue( r.Success, r.Details );
            bool expectedUpdate = true;
            bool actualUpdate = true;
            actualUpdate = releaseHistory.Update( connection, UpdateModes.Refetch );
            Assert.IsFalse( actualUpdate, $"{nameof( CincoReleaseHistoryEntity )} With {nameof( CincoReleaseHistoryEntity.VersionNumber )} Of {expectedActiveVersion} should Not be updated" );
            r = releaseHistory.ValidateStoredEntity( $"Unchanged {nameof( CincoReleaseHistoryEntity )} With {nameof( CincoReleaseHistoryEntity.VersionNumber )} Of {expectedActiveVersion}" );
            Assert.IsTrue( r.Success, r.Details );
            bool expectedInserted = true;
            bool actualInserted = false;
            bool expectedActive = true;
            string notActiveVersion = "0.0.1000";
            releaseHistory = new CincoReleaseHistoryEntity() {
                VersionNumber = notActiveVersion,
                IsActive = expectedActive,
                Description = "alpha release",
                Timestamp = DateTime.UtcNow.Subtract( TimeSpan.FromDays( expectedCount ) )
            };
            actualInserted = releaseHistory.Insert( connection );
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( CincoReleaseHistoryEntity )} With {nameof( CincoReleaseHistoryEntity.VersionNumber )} Of {releaseHistory.VersionNumber} should have been inserted" );
            Assert.AreEqual( expectedActive, releaseHistory.IsActive, $"{nameof( CincoReleaseHistoryEntity )} '{nameof( releaseHistory.IsActive )}' should have been set true" );
            r = releaseHistory.ValidateStoredEntity( $"Added {nameof( CincoReleaseHistoryEntity )} with {nameof( CincoReleaseHistoryEntity.VersionNumber )} of {releaseHistory.VersionNumber} should be clean" );
            Assert.IsTrue( r.Success, r.Details );
            expectedCount += 1;
            actualFetched = releaseHistory.FetchUsingKey( connection );
            expectedFetched = true;
            Assert.AreEqual( expectedFetched, actualFetched, $"release {notActiveVersion} should exist" );
            actualUpdate = releaseHistory.Update( connection, UpdateModes.Refetch );
            Assert.IsFalse( actualUpdate, $"Release History {releaseHistory.VersionNumber} should not be updated" );
            expectedActive = false;
            releaseHistory.IsActive = expectedActive;
            expectedUpdate = true;
            actualUpdate = releaseHistory.Update( connection, UpdateModes.Refetch );
            Assert.AreEqual( expectedUpdate, actualUpdate, $"{nameof( CincoReleaseHistoryEntity )} with {nameof( CincoReleaseHistoryEntity.VersionNumber )} of {releaseHistory.VersionNumber} should be updated" );
            r = releaseHistory.ValidateStoredEntity( $"Updated {nameof( CincoReleaseHistoryEntity )} with {nameof( CincoReleaseHistoryEntity.VersionNumber )} of {releaseHistory.VersionNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            // prevent unique timestamp exception
            var localTimestamp = releaseHistory.Timestamp.ToLocalTime().Subtract( TimeSpan.FromSeconds( 6d ) );
            _ = releaseHistory.UpdateLocalTimeStamp( connection, localTimestamp );
            // SQL Server time stored with a precision scale of 2 for 6 bytes.
            Asserts.Instance.AreEqual( localTimestamp, releaseHistory.Timestamp.ToLocalTime(), TimeSpan.FromMilliseconds( 11d ), $"{nameof( CincoReleaseHistoryEntity )} with {nameof( CincoReleaseHistoryEntity.Timestamp )} timestamp should match" );
            notActiveVersion = "0.1.1000";
            expectedActive = false;
            releaseHistory = new CincoReleaseHistoryEntity() { VersionNumber = notActiveVersion, IsActive = expectedActive, Description = "beta release" };
            actualInserted = releaseHistory.Insert( connection );
            expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"Release History {releaseHistory.VersionNumber} should have been inserted" );

            // prevent unique timestamp exception
            localTimestamp = releaseHistory.Timestamp.ToLocalTime().Subtract( TimeSpan.FromSeconds( 4d ) );
            _ = releaseHistory.UpdateLocalTimeStamp( connection, localTimestamp );
            expectedCount += 1;
            releaseHistory = new CincoReleaseHistoryEntity() { VersionNumber = "0.2.1000", IsActive = false, Description = "release candidate" };
            actualInserted = releaseHistory.Insert( connection );
            expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"Release History {releaseHistory.VersionNumber} should have been inserted" );
            // prevent unique timestamp exception
            localTimestamp = releaseHistory.Timestamp.ToLocalTime().Subtract( TimeSpan.FromSeconds( 2d ) );
            _ = releaseHistory.UpdateLocalTimeStamp( connection, localTimestamp );
            expectedCount += 1;
            actualCount = releaseHistory.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple ReleaseHistorys should have been inserted" );
            string VersionNumber = releaseHistory.VersionNumber;
            bool actualDelete = releaseHistory.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Release History {VersionNumber} should be deleted" );
            expectedCount -= 1;
            actualCount = releaseHistory.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple ReleaseHistorys should have been inserted" );

            // find inactive release history.
            int recordIndex = expectedCount - 1;
            ICincoReleaseHistory releaseHistory1 = releaseHistory.ReleaseHistories.ElementAtOrDefault( recordIndex );
            while ( releaseHistory1.IsActive && recordIndex != 0 )
            {
                recordIndex -= 1;
                releaseHistory1 = releaseHistory.ReleaseHistories.ElementAtOrDefault( recordIndex );
            }

            actualDelete = CincoReleaseHistoryEntity.Delete( connection, releaseHistory1.VersionNumber );
            expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Release History {releaseHistory1.VersionNumber} should be deleted" );
            expectedCount -= 1;

            // test active release
            var outcome = CincoReleaseHistoryEntity.TryVerifyDatabaseVersion( connection, expectedActiveVersion );
            Assert.IsTrue( outcome.Success, $"version {expectedActiveVersion} should be active; {outcome.Details}" );
            outcome = CincoReleaseHistoryEntity.TryVerifyDatabaseVersion( connection, notActiveVersion );
            Assert.IsFalse( outcome.Success, $"version {notActiveVersion} should not be active" );
        }

        #endregion

        #region " SESSION: OHMNI "

        /// <summary> Assert session entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="testLevelId">   Identifier for the test level. </param>
        /// <param name="testLevelName"> Name of the test level. </param>
        public static void AssertSessionEntity( ProviderBase provider, int testLevelId, string testLevelName )
        {
            int id = 3;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = CincoSessionBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<CincoStationNub>();
            _ = connection.DeleteAll<CincoSessionNub>();
            string expectedTimeZoneId = Entities.MSTest.Auditor.TimeZoneStandardName;
            string expectedComputerName = Entities.MSTest.Auditor.ComputerName;
            var station = new CincoStationEntity() { StationName = expectedComputerName, ComputerName = expectedComputerName, TimeZoneId = expectedTimeZoneId };
            bool actualInserted = station.Obtain( connection );
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"Station {station.StationName} should have been inserted" );
            bool actualFetched;
            bool expectedFetched = false;
            var session = new CincoSessionEntity();
            actualFetched = session.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( CincoSessionEntity )} with '{nameof( CincoSessionEntity.SessionAutoId )}' of {id} should not exist" );
            r = session.ValidateNewEntity( $"New {nameof( CincoSessionEntity )} with '{nameof( CincoSessionEntity.SessionAutoId )}' of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            session.Clear();
            int expectedTestLevel = testLevelId;
            session = new CincoSessionEntity() {
                StationAutoId = station.StationAutoId,
                StationName = station.StationName,
                TestLevelId = expectedTestLevel,
                Description = $"Session {testLevelName}"
            };
            actualInserted = session.Insert( connection );
            expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"Session {session.StationAutoId} should have been inserted" );
            r = session.ValidateStoredEntity( $"Inserted {nameof( CincoSessionEntity )} for '{nameof( CincoSessionEntity.StationName )}' of {station.StationName}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 1;
            int actualCount = session.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple Sessions should have been inserted" );
        }

        /// <summary> Assert session entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="site">      The site. </param>
        /// <param name="provider">  The provider. </param>
        /// <param name="testLevel"> The test level. </param>
        public static void AssertSessionEntity( ProviderBase provider, CincoTestLevel testLevel )
        {
            AssertSessionEntity( provider, ( int ) testLevel, testLevel.Description() );
        }

        /// <summary> Assert add session entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="stationAutoId"> Identifier for the station automatic. </param>
        /// <param name="stationName">   Name of the station. </param>
        /// <param name="testLevel">     The test level. </param>
        /// <returns> An Integer. </returns>
        public static int AssertAddSessionEntity( ProviderBase provider, int stationAutoId, string stationName, int testLevel )
        {
            int id = 3;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = CincoSessionBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<CincoSessionNub>();
            bool actualFetched;
            var session = new CincoSessionEntity();
            actualFetched = session.FetchUsingKey( connection, id );
            Assert.IsFalse( actualFetched, $"{nameof( CincoSessionEntity )} with '{nameof( CincoSessionEntity.SessionAutoId )}' of {id} should not exist" );
            r = session.ValidateNewEntity( $"New {nameof( CincoSessionEntity )} with '{nameof( CincoSessionEntity.SessionAutoId )}' of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            session.Clear();
            session = new CincoSessionEntity() {
                StationAutoId = stationAutoId,
                StationName = stationName,
                TestLevelId = testLevel,
                Description = $"Session {testLevel}"
            };
            bool actualInserted = session.Insert( connection );
            Assert.IsTrue( actualInserted, $"{nameof( CincoSessionEntity )} for '{nameof( CincoSessionEntity.StationName )}' of {stationName} should have been inserted" );
            r = session.ValidateStoredEntity( $"Insert {nameof( CincoSessionEntity )} for '{nameof( CincoSessionEntity.StationName )}' of {stationName}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 1;
            int actualCount = session.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple Sessions should have been inserted" );
            return session.SessionAutoId;
        }

        #endregion

        #region " STATION "

        /// <summary> Asserts station entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">                        The site. </param>
        /// <param name="provider">                    The provider. </param>
        /// <param name="expectedUsingNativeTracking"> True to expected using native tracking. </param>
        public static void AssertStationEntity( ProviderBase provider, bool expectedUsingNativeTracking )
        {
            string expectedComputerName = Entities.MSTest.Auditor.ComputerName;
            string expectedTimeZoneId = Entities.MSTest.Auditor.TimeZoneStandardName;
            int id = 4;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = CincoStationBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<CincoStationNub>();
            var station = new CincoStationEntity() { UsingNativeTracking = expectedUsingNativeTracking };

            // connection.DeleteAll(Of StationNub)()
            bool expectedFetched = false;
            bool actualFetched = station.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {id} should not exist" );
            r = station.ValidateNewEntity( $"New {nameof( CincoStationEntity )} with key {id}" );
            Assert.IsTrue( r.Success, r.Details );
            station.Clear();
            bool expectedInserted = true;
            bool actualInserted = false;
            station = new CincoStationEntity() { StationName = expectedComputerName, ComputerName = expectedComputerName, TimeZoneId = expectedTimeZoneId };
            station.UsingNativeTracking = expectedUsingNativeTracking;
            actualInserted = station.Insert( connection );
            Assert.AreEqual( expectedInserted, actualInserted, $"Station {station.StationName} should have been inserted" );
            Assert.AreEqual( expectedTimeZoneId, station.TimeZoneId, $"Station {nameof( station.TimeZoneId )} should have been set true" );
            r = station.ValidateStoredEntity( $"Inserted {nameof( CincoStationEntity )} with {nameof( CincoStationEntity.ComputerName )} of {expectedComputerName}" );
            Assert.IsTrue( r.Success, r.Details );
            _ = station.FetchUsingKey( connection );
            var expectedLocalTimestamp = DateTime.UtcNow;
            var actualLocalTimeStamp = station.Timestamp;
            Asserts.Instance.AreEqual( expectedLocalTimestamp, actualLocalTimeStamp, TimeSpan.FromSeconds( 10d ), "Expected local time timestamp" );
            DateTimeOffset expectedLocaleTimeStamp = actualLocalTimeStamp;
            var actualLocaleTimeStamp = station.Timestamp;
            Asserts.Instance.AreEqual( expectedLocaleTimeStamp, actualLocaleTimeStamp, TimeSpan.FromSeconds( 0d ), "Expected locale time timestamp" );
            station.StationName = "New Name";
            r = station.ValidateChangedEntity( $"Changed {nameof( CincoStationEntity )} with {nameof( CincoStationEntity.ComputerName )} of {station.ComputerName}" );
            Assert.IsTrue( r.Success, r.Details );

            bool actualUpdate = station.Update( connection, UpdateModes.Refetch );
            Assert.IsTrue( actualUpdate, $"Changed {nameof( CincoStationEntity )} with {nameof( CincoStationEntity.ComputerName )} of {station.ComputerName} should be updated" );
            r = station.ValidateStoredEntity( $"Updated #1 {nameof( CincoStationEntity )} with {nameof( CincoStationEntity.ComputerName )} of {station.ComputerName}" );
            Assert.IsTrue( r.Success, r.Details );

            actualUpdate = station.Update( connection, UpdateModes.Refetch );
            actualUpdate = station.Update( connection, UpdateModes.Refetch );
            Assert.IsFalse( actualUpdate, $"Station {station.ComputerName} should not be updated" );
            r = station.ValidateStoredEntity( $"Updated #2 {nameof( CincoStationEntity )} with {nameof( CincoStationEntity.ComputerName )} of {station.ComputerName}" );
            Assert.IsTrue( r.Success, r.Details );

            station = new CincoStationEntity() { StationName = "Station1", ComputerName = "Computer1", TimeZoneId = expectedTimeZoneId };
            station.UsingNativeTracking = expectedUsingNativeTracking;
            actualInserted = station.Insert( connection );
            expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( CincoStationEntity )} with {nameof( CincoStationEntity.ComputerName )} of {station.ComputerName} should have been inserted" );

            station = new CincoStationEntity() { StationName = "Station2", ComputerName = "Computer2", TimeZoneId = expectedTimeZoneId };
            station.UsingNativeTracking = expectedUsingNativeTracking;
            actualInserted = station.Insert( connection );
            expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( CincoStationEntity )} with {nameof( CincoStationEntity.ComputerName )} of {station.ComputerName} should have been inserted" );

            int expectedCount = 3;
            int actualCount = station.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple {nameof( CincoStationEntity )}'s should have been inserted" );
            string stationName = station.StationName;
            bool actualDelete = station.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"{nameof( CincoStationEntity )} with {nameof( CincoStationEntity.StationName )} of {station.StationName} should be deleted" );
            station.Clear();

            // get active station
            station.ComputerName = expectedComputerName;
            _ = station.Obtain( connection );
            Assert.AreEqual( expectedComputerName, station.ComputerName, $"{nameof( CincoStationEntity )} with {nameof( CincoStationEntity.StationName )} of {station.StationName} should be the active station" );
            stationName = station.StationName;
            string newStationName = $"{stationName}.Changed";
            string expectedStationName = newStationName;
            id = station.StationAutoId;
            if ( connection.State == System.Data.ConnectionState.Closed )
                connection.Open();
            using ( var transaction = connection.BeginTransaction() )
            {
                try
                {
                    station.StationName = newStationName;
                    r = station.ValidateChangedEntity( $"Changed {nameof( CincoStationEntity )} '{nameof( CincoStationEntity.StationName )}' to {newStationName}" );
                    Assert.IsTrue( r.Success, r.Details );
                    var transactedConnection = new global::Dapper.TransactedConnection( connection, transaction );
                    expectedStationName = newStationName;
                    actualUpdate = station.Update( transactedConnection, UpdateModes.Refetch );
                    Assert.IsTrue( actualUpdate, $"Changed {nameof( CincoStationEntity )} '{nameof( CincoStationEntity.StationName )}' to {newStationName} should be updated" );
                    transaction.Commit();
                    _ = station.FetchUsingKey( connection );
                    Assert.AreEqual( expectedStationName, station.StationName, $"{nameof( CincoStationEntity )} '{nameof( CincoStationEntity.StationName )}' should be restored after transaction rollback" );
                }
                catch
                {
                    throw;
                }
            }

            using ( var transaction = connection.BeginTransaction() )
            {
                try
                {
                    expectedStationName = station.StationName;
                    station.StationName = stationName;
                    r = station.ValidateChangedEntity( $"Changed {nameof( CincoStationEntity )} '{nameof( CincoStationEntity.StationName )}' to {newStationName}" );
                    Assert.IsTrue( r.Success, r.Details );
                    var transactedConnection = new global::Dapper.TransactedConnection( connection, transaction );
                    actualUpdate = station.Update( transactedConnection, UpdateModes.Refetch );
                    Assert.IsTrue( actualUpdate, $"Changed {nameof( CincoStationEntity )} '{nameof( CincoStationEntity.StationName )}' to {newStationName} should be updated" );
                    transaction.Rollback();
                    _ = station.FetchUsingKey( connection );
                    Assert.AreEqual( expectedStationName, station.StationName, $"{nameof( CincoStationEntity )} '{nameof( CincoStationEntity.StationName )}' should be restored after transaction rollback" );
                }
                catch
                {
                    throw;
                }
            }
        }

        /// <summary> Asserts adding a station entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        /// <returns> A (AutoId As Integer, Name As String) </returns>
        public static (int AutoId, string Name) AssertAddStationEntity( ProviderBase provider )
        {
            string expectedComputerName = Entities.MSTest.Auditor.ComputerName;
            string expectedTimeZoneId = Entities.MSTest.Auditor.TimeZoneStandardName;
            int id = 4;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = CincoStationBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<CincoStationNub>();
            var station = new CincoStationEntity();

            // connection.DeleteAll(Of StationNub)()
            bool expectedFetched = false;
            bool actualFetched = station.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {id} should not exist" );
            r = station.ValidateNewEntity( $"New {nameof( CincoStationEntity )} with key {id}" );
            Assert.IsTrue( r.Success, r.Details );
            station.Clear();
            bool expectedInserted = true;
            bool actualInserted = false;
            station = new CincoStationEntity() { StationName = expectedComputerName, ComputerName = expectedComputerName, TimeZoneId = expectedTimeZoneId };
            actualInserted = station.Insert( connection );
            Assert.AreEqual( expectedInserted, actualInserted, $"Station {station.StationName} should have been inserted" );
            Assert.AreEqual( expectedTimeZoneId, station.TimeZoneId, $"Station {nameof( station.TimeZoneId )} should have been set" );
            r = station.ValidateStoredEntity( $"Inserted {nameof( CincoStationEntity )} with {nameof( CincoStationEntity.ComputerName )} of {station.ComputerName}" );
            Assert.IsTrue( r.Success, r.Details );
            _ = station.FetchUsingKey( connection );
            var expectedLocalTimestamp = DateTime.UtcNow;
            var actualLocalTimeStamp = station.Timestamp;
            Asserts.Instance.AreEqual( expectedLocalTimestamp, actualLocalTimeStamp, TimeSpan.FromSeconds( 10d ), "Expected local time timestamp" );
            DateTimeOffset expectedLocaleTimeStamp = actualLocalTimeStamp;
            var actualLocaleTimeStamp = station.LocaleTimestamp();
            Asserts.Instance.AreEqual( expectedLocaleTimeStamp, actualLocaleTimeStamp, TimeSpan.FromSeconds( 0d ), "Expected locale time timestamp" );
            return (station.StationAutoId, station.StationName);
        }

        #endregion

        #region " TEST LEVEL "

        /// <summary> Asserts the test level entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">                 The site. </param>
        /// <param name="provider">             The provider. </param>
        /// <param name="testLevelId">          Identifier for the test level. </param>
        /// <param name="changedTestLevelName"> Name of the changed test level. </param>
        public static void AssertTestLevelEntity( ProviderBase provider, int testLevelId, string changedTestLevelName )
        {
            int id = 10;
            var testLevel = new OhmniTestLevelEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = OhmniTestLevelBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            bool expectedFetched = false;
            r = testLevel.ValidateNewEntity( $"{nameof( OhmniTestLevelEntity )} with {nameof( OhmniTestLevelEntity.TestLevelId )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualFetched = testLevel.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {id} should not exist" );
            r = testLevel.ValidateNewEntity( $"{nameof( OhmniTestLevelEntity )} with {nameof( OhmniTestLevelEntity.TestLevelId )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            id = testLevelId;
            actualFetched = testLevel.FetchUsingKey( connection, id );
            Assert.IsTrue( actualFetched, $"key {id} should exist" );
            r = testLevel.ValidateStoredEntity( $"Existing {nameof( OhmniTestLevelEntity )} with {nameof( OhmniTestLevelEntity.TestLevelId )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            string existingName = testLevel.Name;
            actualFetched = testLevel.FetchUsingUniqueIndex( connection, existingName );
            Assert.IsTrue( actualFetched, $"Name {existingName} should exist" );
            r = testLevel.ValidateStoredEntity( $"Existing {nameof( OhmniTestLevelEntity )} with {nameof( OhmniTestLevelEntity.Name )} equals {existingName}" );
            Assert.IsTrue( r.Success, r.Details );
            string name = changedTestLevelName;
            if ( !string.Equals( testLevel.Name, changedTestLevelName ) )
            {
                testLevel.Name = name;
                r = testLevel.ValidateChangedEntity( $"Changed {nameof( OhmniTestLevelEntity )} with {nameof( OhmniTestLevelEntity.TestLevelId )} to {id}" );
                Assert.IsTrue( r.Success, r.Details );
                // try to fetch using new name; 
                actualFetched = testLevel.FetchUsingUniqueIndex( connection );
                Assert.IsFalse( actualFetched, $"Name {name} should not exist" );
            }

            int expectedCount = 2;
            int actualCount = testLevel.FetchAllEntities( connection );
            Assert.IsTrue( actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} {nameof( OhmniTestLevelEntity )}s" );
        }

        /// <summary> Asserts the test level entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="site">        The site. </param>
        /// <param name="provider">    The provider. </param>
        /// <param name="testLevelId"> Identifier for the test level. </param>
        public static void AssertTestLevelEntity( ProviderBase provider, TestLevel testLevelId )
        {
            AssertTestLevelEntity( provider, ( int ) testLevelId, $"{testLevelId}.Changed" );
        }

        /// <summary> Asserts the test level entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="site">        The site. </param>
        /// <param name="provider">    The provider. </param>
        /// <param name="testLevelId"> Identifier for the test level. </param>
        public static void AssertTestLevelEntity( ProviderBase provider, CincoTestLevel testLevelId )
        {
            AssertTestLevelEntity( provider, ( int ) testLevelId, $"{testLevelId}.Changed" );
        }

        #endregion

    }
}

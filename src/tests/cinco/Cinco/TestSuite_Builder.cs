using System;

using Dapper;

using isr.Dapper.Entity;
using isr.Dapper.Ohmni;

using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace isr.Dapper.Cinco.MSTest
{
    public partial class TestSuite
    {

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="provider"> The provider. </param>
        public static void Initialize( ProviderBase provider )
        {
            Console.WriteLine( $".NET {Environment.Version}" );
            Console.WriteLine( $"Dapper {typeof( SqlMapper ).AssemblyQualifiedName}" );
            Console.WriteLine( $"Connection string: {provider.ConnectionString}" );
            Provider = provider;
            // test naked connection
            Assert.IsTrue( Provider.IsConnectionExists(), $"connection {Provider.GetConnection()} does not exist" );
            // test connection with event handling
            using ( var connection = Provider.GetConnection() )
            {
                Assert.IsTrue( connection.Exists(), "connection does not exist" );
            }

            if ( provider is null )
            {
                SchemaBuilder = new SchemaBuilder();
            }
            else
            {
                SchemaBuilder = new SchemaBuilder() { Provider = provider };
                EnumerateSchemaObjects( provider.ProviderType );
            }
            // this prevents testing.
            BuildDatabase( provider.BuildMasterConnectionString( false ) );
            SchemaBuilder.AssertSchemaObjectsExist();
        }

        /// <summary> Cleanups this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public static void Cleanup()
        {
            if ( TableCategory.None != AppSettings.Instance.CincoTestsInfo.DeleteOption )
            {
                SchemaBuilder.DeleteAllRecords( AppSettings.Instance.CincoTestsInfo.DeleteOption );
            }

            if ( TableCategory.None != AppSettings.Instance.CincoTestsInfo.DropTablesOption )
            {
                SchemaBuilder.DropAllTables( AppSettings.Instance.CincoTestsInfo.DropTablesOption, AppSettings.Instance.CincoTestsInfo.IgnoreDropTableErrors );
            }
        }

        /// <summary> Gets or sets the provider. </summary>
        /// <value> The provider. </value>
        public static ProviderBase Provider { get; private set; }

        /// <summary> Gets or sets the schema builder. </summary>
        /// <value> The schema builder. </value>
        public static SchemaBuilder SchemaBuilder { get; private set; }

        /// <summary> Enumerate schema objects. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="providerType"> Type of the provider. </param>
        public static void EnumerateSchemaObjects( ProviderType providerType )
        {
            try
            {
                EnumerateSchemaObjectsThis( providerType );
            }
            catch
            {
                SchemaBuilder.EnumeratedTables.Clear();
                throw;
            }
        }

        /// <summary> Enumerate schema objects this. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="providerType"> Type of the provider. </param>
        private static void EnumerateSchemaObjectsThis( ProviderType providerType )
        {
            string providerFileLabel = providerType == ProviderType.SQLite ? AppSettings.Instance.CincoTestsInfo.SQLiteFileLabel : AppSettings.Instance.CincoTestsInfo.SqlServerFileLabel;
            var schemaBuilder = SchemaBuilder;
            schemaBuilder.EnumeratedTables.Clear();
            var tb = new TableMaker();

            // ATTRIBUTE TYPE 
            tb = new TableMaker() {
                TableName = OhmniAttributeTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmniAttributeTypeBuilder.CreateTable,
                InsertAction = CincoPartEntity.InsertAttributeTypeValues
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // TEST LEVEL
            tb = new TableMaker() {
                TableName = OhmniTestLevelBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmniTestLevelBuilder.CreateTable,
                InsertAction = CincoSessionEntity.InsertTestLevelValues
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // STATION
            tb = new TableMaker() {
                TableName = CincoStationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CincoStationBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION
            tb = new TableMaker() {
                TableName = CincoSessionBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CincoSessionBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART 
            tb = new TableMaker() {
                TableName = CincoPartBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CincoPartBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART ATTRIBUTE RANGE
            OhmniPartAttributeRangeBuilder.AttributeTypeTableName = OhmniAttributeTypeBuilder.TableName;
            isr.Dapper.Ohmni.OhmniPartAttributeRangeBuilder.AttributeTypeForeignKeyName = nameof( isr.Dapper.Ohmni.IOhmniAttributeType.AttributeTypeId );

            tb = new TableMaker() {
                TableName = OhmniPartAttributeRangeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmniPartAttributeRangeBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // LOT
            tb = new TableMaker() {
                TableName = CincoLotBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = CincoLotBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // VERSION
            BuiltDatabaseVersion = AppSettings.Instance.CincoTestsInfo.DatabaseRevision;
            tb = new TableMaker() {
                TableName = CincoReleaseHistoryBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = CincoReleaseHistoryBuilder.CreateTable,
                PostCreateAction = CincoReleaseHistoryBuilder.UpsertRevision,
                PostCreateValue = $"{BuiltDatabaseVersion},Initial Release"
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );
        }

        /// <summary> Gets or sets the built database version. </summary>
        /// <value> The built database version. </value>
        public static string BuiltDatabaseVersion { get; set; }

        /// <summary> Creates data base. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void CreateDatabase( string masterConnectionString )
        {
            if ( AppSettings.Instance.CincoTestsInfo.CreateDatabase || !Provider.DatabaseExists() )
            {
                if ( System.IO.File.Exists( Provider.FileName ) )
                    System.IO.File.Delete( Provider.FileName );
                // using the temp db.
                //  _ = CreateDatabaseThis( masterConnectionString, AppSettings.Instance.CincoTestsInfo.SchemaFileNameFormat );
                //  _ = CreateDatabaseThis( masterConnectionString, AppSettings.Instance.CincoTestsInfo.SchemaDataFileNameFormat );
                _ = CreateDatabaseThis( masterConnectionString, string.Empty );
            }
        }

        /// <summary>   Creates database. </summary>
        /// <remarks>   David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString">       The master connection string. </param>
        /// <param name="createDatabaseFileNameFormat"> The create database file name format. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        private static bool CreateDatabaseThis( string masterConnectionString, string createDatabaseFileNameFormat )
        {
            bool result = Provider.CreateDatabase( masterConnectionString, Provider.DatabaseName );
            int count;
            string schemaFileName;
            if ( result && !string.IsNullOrEmpty( createDatabaseFileNameFormat ) )
            {
                string providerFileLabel = Provider.ProviderType == ProviderType.SQLite ? AppSettings.Instance.CincoTestsInfo.SQLiteFileLabel : AppSettings.Instance.CincoTestsInfo.SqlServerFileLabel;
                schemaFileName = System.IO.Path.Combine( AppSettings.Instance.CincoTestsInfo.SchemaFolderName, string.Format( createDatabaseFileNameFormat, providerFileLabel ) );
                count = Provider.ExecuteScript( schemaFileName );
                result = count > 0;
            }
            return result;
        }

        /// <summary> Create or clear database and add all tables. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void BuildDatabase( string masterConnectionString )
        {
            CreateDatabase( masterConnectionString );
            if ( AppSettings.Instance.CincoTestsInfo.ClearDatabase )
                SchemaBuilder.DropAllTables( AppSettings.Instance.CincoTestsInfo.IgnoreDropTableErrors );
            SchemaBuilder.BuildSchema();
        }
    }
}

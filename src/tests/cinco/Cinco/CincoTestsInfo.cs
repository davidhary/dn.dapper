
using isr.Dapper.Entity;

namespace isr.Dapper.Cinco.MSTest
{

    /// <summary> Test information for the Polynomial Fit Tests. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2/12/2018 </para></remarks>
    [isr.Json.SettingsSection( nameof( CincoTestsInfo ) )]
    internal class CincoTestsInfo : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public CincoTestsInfo() : base( System.Reflection.Assembly.GetAssembly( typeof( TestSiteSettings ) ) )
        { }

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public virtual bool Verbose { get; set; }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public virtual bool Enabled { get; set; }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public virtual bool All { get; set; }

        #endregion

        #region " SQLite SETTINGS "

        /// <summary> Gets or sets the sq lite connection string. </summary>
        /// <value> The SQLite connection string. </value>
        public string SQLiteConnectionString { get; set; }

        #endregion

        #region " SqlServer SETTINGS "

        /// <summary> Gets or sets the connection string. </summary>
        /// <value> The connection string. </value>
        public string SqlServerConnectionString { get; set; }

        #endregion

        #region " BUILD SETTINGS "

        /// <summary>
        /// Gets or sets the delete option determining which, if any, tables types are deleted.
        /// </summary>
        /// <value> The delete option. </value>
        public TableCategory DeleteOption { get; set; }

        /// <summary> Gets or sets the drop tables option. </summary>
        /// <value> The drop tables option. </value>
        public TableCategory DropTablesOption { get; set; }

        /// <summary>
        /// Gets or sets the clear database sentinel determining if the database tables are dropped
        /// before new tables are created.
        /// </summary>
        /// <value> The clear database sentinel. </value>
        public bool ClearDatabase { get; set; }

        /// <summary> Gets or sets the add tables. </summary>
        /// <value> The add tables. </value>
        public bool AddTables { get; set; }

        /// <summary> Gets or sets the ignore drop table errors. </summary>
        /// <value> The ignore drop table errors. </value>
        public bool IgnoreDropTableErrors { get; set; }

        /// <summary> Gets or sets the create database sentinel. </summary>
        /// <value> The create database. </value>
        public bool CreateDatabase { get; set; }

        /// <summary> Gets or sets the pathname of the schema folder. </summary>
        /// <value> The pathname of the schema folder. </value>
        public string SchemaFolderName { get; set; }

        /// <summary> Gets or sets the SQL server file label. </summary>
        /// <value> The SQL server file label. </value>
        public string SqlServerFileLabel { get; set; }

        /// <summary> Gets or sets the sq lite file label. </summary>
        /// <value> The sq lite file label. </value>
        public string SQLiteFileLabel { get; set; }

        /// <summary> Gets or sets the guard band lookup file name format. </summary>
        /// <value> The guard band lookup file name format. </value>
        public string GuardBandLookupFileNameFormat { get; set; }

        /// <summary> Gets or sets the add ohm meter setting lookup file name format. </summary>
        /// <value> The add ohm meter setting lookup file name format. </value>
        public string AddOhmMeterSettingLookupFileNameFormat { get; set; }

        /// <summary>   Gets or sets the schema file name format. </summary>
        /// <value> The schema file name format. </value>
        public string SchemaFileNameFormat { get; set; }

        /// <summary>   Gets or sets the schema data file name format. </summary>
        /// <value> The schema data file name format. </value>
        public string SchemaDataFileNameFormat { get; set; }

        /// <summary> Gets or sets the clear data upon initialize. </summary>
        /// <value> The clear data upon initialize. </value>
        public bool ClearDataUponInitialize { get; set; }

        /// <summary> Gets or sets the database revision. </summary>
        /// <value> The database revision. </value>
        public string DatabaseRevision { get; set; }

        #endregion

    }
}

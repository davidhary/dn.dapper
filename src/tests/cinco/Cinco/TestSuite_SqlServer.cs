using System;

using isr.Dapper.Entity;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Cinco.MSTest
{

    /// <summary> An Ohmni Cinco Test Suite for SQL Server Dapper Entities. </summary>
    /// <remarks>
    /// The test suites here implement TestSuiteBase so that each provider runs the entire set of
    /// tests without declarations per method If we want to support a new provider, they need only be
    /// added here - not in multiple places. (c) 2018 Integrated Scientific Resources, Inc. All
    /// rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-08-16 </para>
    /// </remarks>
    [TestClass(), TestCategory( "SqlServer" )]
    public class SqlServerTestSuite : TestSuite
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                BaseClassInitialize( testContext );
                Initialize( GetSharedProvider() );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            Cleanup();
        }

        #endregion

        #region " PROVIDER "

        /// <summary> Gets the provider. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The provider. </returns>
        public override ProviderBase GetProvider()
        {
            return GetSharedProvider();
        }

        /// <summary> The SQL server provider. </summary>
        private static SqlServerProvider _SqlServerProvider;

        /// <summary> Gets the shared provider. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The shared provider. </returns>
        public static SqlServerProvider GetSharedProvider()
        {
            if ( _SqlServerProvider is null )
            {
                _SqlServerProvider = SqlServerProvider.Create( AppSettings.Instance.CincoTestsInfo.SqlServerConnectionString );
            }

            return _SqlServerProvider;
        }

        #endregion

    }
}

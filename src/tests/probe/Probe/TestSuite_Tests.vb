'---------------------------------------------------------------------------------------------------
' file:		Ohmni\Probe\TestSuite_Tests.vb
'
' summary:	Test suite tests class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices
Imports isr.Entities.Tests

Imports isr.Core.EnumExtensions

Namespace Global.isr.Dapper.Entities.Tests.Ohmni.Probe

    Partial Public MustInherit Class TestSuite

#Region " LOOKUP "

        ''' <summary> (Unit Test Method) tests MeterModel entity. </summary>
        <TestMethod>
        Public Sub MultimeterModelEntityTest()
            Auditor.AssertMultimeterModelEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Multimeter entity. </summary>
        <TestMethod>
        Public Sub MultimeterEntityTest()
            Auditor.AssertMultimeterEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Nominal Reading type entity. </summary>
        <TestMethod>
        Public Sub NomReadingTypeEntityTest()
            Auditor.AssertNomReadingTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests the Part Specification Type entity. </summary>
        <TestMethod>
        Public Sub PartSpecificationTypeEntityTest()
            Auditor.AssertPartSpecificationTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests Structure Type entity. </summary>
        <TestMethod>
        Public Sub StructureTypeEntityTest()
            Auditor.AssertStructureTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests the Substrate Type entity. </summary>
        <TestMethod>
        Public Sub SubstrateTypeEntityTest()
            Auditor.AssertSubstrateTypeEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests the test level entity. </summary>
        <TestMethod>
        Public Sub TestTypeEntityTest()
            Auditor.AssertTestTypeEntity(TestSuite.TestInfo, Me.GetProvider, isr.Dapper.Entities.TestType.Trim)
        End Sub

        ''' <summary> (Unit Test Method) tests tolerance entity. </summary>
        <TestMethod>
        Public Sub ToleranceEntityTest()
            Auditor.AssertToleranceEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

#End Region

#Region " LOT "

        ''' <summary> (Unit Test Method) tests lot entity. </summary>
        <TestMethod>
        Public Sub LotEntityTest()
            ' PFC-D1206LF-03-6122-2442-FB-2728  1725356 1770169
            ' PFC-W0402LF-03-1000-B-4013  1749153  1749156 1725105
            ' PFC-D1206LF-03-3202-1002-DB-2728 1733251 1733247  1733248  1733249
            Dim partNumber As String = "PFC-D1206LF-03-6122-2442-FB-2728"
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Auditor.AssertLotEntity(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumbers)
        End Sub

        <TestMethod>
        Public Sub LotOhmMeterSetupEntityTest()
            Dim partNumber As String = "PFC-W0402LF-03-1000-B-4013"
            Dim part As PartEntity = Auditor.AssertAddingPartEntity(TestSuite.TestInfo, Me.GetProvider, partNumber)
            Dim lot As LotEntity = Auditor.AssertAddingLotEntity(TestSuite.TestInfo, Me.GetProvider, part.AutoId, "Lot1")
            Auditor.AssertLotOhmMeterSetupEntity(TestSuite.TestInfo, Me.GetProvider, lot.AutoId)
        End Sub

#End Region

#Region " PLATFORM "

        ''' <summary> (Unit Test Method) tests the release history entity. </summary>
        <TestMethod>
        Public Sub RevisionEntityTest()
            Auditor.AssertRevisionEntity(TestSuite.TestInfo, Me.GetProvider, TestSuite.CurrentDatabaseVersion)
        End Sub

        ''' <summary> (Unit Test Method) tests station entity. </summary>
        <TestMethod>
        Public Sub StationEntityTest()
            Auditor.AssertStationEntity(TestSuite.TestInfo, Me.GetProvider, True)
        End Sub

#End Region

#Region " SESSION "

        ''' <summary> (Unit Test Method) tests Guard Band Lookup entity. </summary>
        <TestMethod>
        Public Sub GuardBandLookupEntityTest()
            Auditor.AssertMeterGuardBandLookupEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests ohm meter setup entity. </summary>
        <TestMethod>
        Public Sub OhmMeterSetupEntityTest()
            Auditor.AssertOhmMeterSetupEntity(TestSuite.TestInfo, Me.GetProvider)
        End Sub

        ''' <summary> (Unit Test Method) tests session entity. </summary>
        <TestMethod>
        Public Sub SessionEntityTest()
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Auditor.AssertAddingSessionEntity(TestSuite.TestInfo, Me.GetProvider, $"{lotNumbers(1)}:1")
        End Sub

        ''' <summary> Tests lot session entity. </summary>
        ''' <remarks> David, 2020-05-05. </remarks>
        <TestMethod>
        Public Sub LotSessionEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-6122-2442-FB-2728"
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Dim lotNumber As String = lotNumbers(0)
            Auditor.AssertAddingLotSessionEntities(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumber, 2)
        End Sub

#End Region

#Region " PART "

        ''' <summary> (Unit Test Method) tests part entity. </summary>
        <TestMethod>
        Public Sub PartEntityTest()
            Dim partNumbers As String() = New String() {"PFC-W0402LF-03-1000-B-4013",
                                                        "PFC-W0402LF-03-1001-B-4013",
                                                        "PFC-D1206LF-03-3202-1002-DB-2728",
                                                        "PFC-D1206LF-03-6122-2442-FB-2728",
                                                        "WIN-T0603LF-03-3091-B",
                                                        "WIN-T0603LF-03-2002-B",
                                                        "WIN-T0603LF-03-1002-B"}
            Auditor.AssertPartEntity(TestSuite.TestInfo, Me.GetProvider, partNumbers)
        End Sub

#End Region

#Region " SUBSTRATE "

        ''' <summary> (Unit Test Method) tests substrate entity. </summary>
        ''' <remarks> David, 3/10/2020. </remarks>
        <TestMethod>
        Public Sub SubstrateEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-3202-1002-DB-2728"
            Dim lotNumbers As String() = New String() {"1733251", "1733247"}
            Auditor.AssertSubstrateEntity(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumbers(0))
        End Sub

#End Region

#Region " STRUCTURE "

        ''' <summary> (Unit Test Method) tests Structure entity. </summary>
        <TestMethod>
        Public Sub StructureEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-3202-1002-DB-2728"
            Dim lotNumbers As String() = New String() {"1733251", "1733247"}
            Auditor.AssertSubstrateEntity(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumbers(0))
        End Sub

        ''' <summary> (Unit Test Method) tests Structure session entity. </summary>
        <TestMethod>
        Public Sub SessionStructureEntityTest()
            Dim partNumber As String = "PFC-D1206LF-03-6122-2442-FB-2728"
            Dim lotNumbers As String() = New String() {"1725356", "1770169"}
            Dim lotNumber As String = lotNumbers(0)
            Auditor.AssertSessionStructure(TestSuite.TestInfo, Me.GetProvider, partNumber, lotNumber, $"{lotNumber}:1")
        End Sub

#End Region

    End Class

End Namespace


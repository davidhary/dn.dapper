using isr.Dapper.Entity;
using isr.Dapper.Entities.MSTest;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace isr.Dapper.Probe.MSTest
{

    /// <summary> An Ohmni Probe Test Suite for Dapper Entities. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-26 </para>
    /// </remarks>
    [TestClass()]
    public abstract partial class TestSuite
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void BaseClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " PROVIDER "

        /// <summary> Gets the provider. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <returns> The provider. </returns>
        public abstract ProviderBase GetProvider();

        /// <summary> (Unit Test Method) queries if a given connection exists. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void ConnectionExists()
        {
            isr.Dapper.Entities.MSTest.Auditor.AssertConnectionExists( this.GetProvider() );
        }

        /// <summary> (Unit Test Method) tests tables exist. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public virtual void TablesExistTest()
        {
            isr.Dapper.Entities.MSTest.Auditor.AssertTablesExist( this.GetProvider(), SchemaBuilder.TableNames );
        }

        #endregion

    }
}

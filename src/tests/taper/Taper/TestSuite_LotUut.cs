using isr.Dapper.Entities.MSTest;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Entities.Taper.Tests
{
    public abstract partial class TestSuite
    {

        #region " NUT + UUT "

        /// <summary> (Unit Test Method) tests nut entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void LotNutEntityTest()
        {
            Assert.Inconclusive("Traits needs to be set for the modified platform meter design");
            // tests adding Nut to a UUT with readings and bin.
            string productNumber = "D1206LF";
            string partNumber = "PFC-D1206LF-03-3202-1002-DB-2728";
            var lotNumbers = new string[] { "1733251", "1733247" };
            Auditor.AssertLotNutEntity(TestInfo, this.GetProvider(), productNumber, partNumber, lotNumbers);
        }

        /// <summary> (Unit Test Method) tests uut entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        [TestMethod]
        public void LotUutEntityTest()
        {
            string partNumber = "PFC-D1206LF-03-3202-1002-DB-2728";
            var lotNumbers = new string[] { "1733251", "1733247" };
            _ = Auditor.AssertAddingLots(TestInfo, this.GetProvider(), partNumber, lotNumbers);
            // tests adding a UUT to the Lot and sorting using a bin number
            Auditor.AssertLotUutEntity(TestInfo, this.GetProvider(), lotNumbers);
        }

        #endregion


    }
}

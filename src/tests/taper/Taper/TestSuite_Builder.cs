using System;

using Dapper;

using isr.Dapper.Entity;
using isr.Dapper.Entities;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Taper.MSTest
{
    public partial class TestSuite
    {

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="provider"> The provider. </param>
        public static void Initialize( ProviderBase provider )
        {
            Console.Out.WriteLine( $".NET {Environment.Version}" );
            Console.Out.WriteLine( $"Dapper {typeof( SqlMapper ).AssemblyQualifiedName}" );
            Console.Out.WriteLine( $"Connection string: {provider.ConnectionString}" );
            Provider = provider;
            // test naked connection
            Assert.IsTrue( Provider.IsConnectionExists(), $"connection {Provider.GetConnection()} does not exist" );
            // test connection with event handling
            using ( var connection = Provider.GetConnection() )
            {
                Assert.IsTrue( connection.Exists(), "connection does not exist" );
            }

            if ( provider is null )
            {
                SchemaBuilder = new SchemaBuilder();
            }
            else
            {
                SchemaBuilder = new SchemaBuilder() { Provider = provider };
                EnumerateSchemaObjects( provider.ProviderType );
            }
            // this prevents testing.
            BuildDatabase( provider.BuildMasterConnectionString( false ) );
            SchemaBuilder.AssertSchemaObjectsExist();
            if ( AppSettings.Instance.TaperTestsInfo.ClearDataUponInitialize )
            {
                AssertClearData( provider );
            }

            InitializeLookupEntities( provider );
        }

        /// <summary> Try clear data. </summary>
        /// <remarks> David, 2020-06-13. </remarks>
        /// <param name="provider"> The provider. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        internal static (bool Success, string Details) TryClearData( ProviderBase provider )
        {
            string providerFileLabel = provider.ProviderType == ProviderType.SQLite
                                                                        ? AppSettings.Instance.TaperTestsInfo.SQLiteFileLabel
                                                                        : AppSettings.Instance.TaperTestsInfo.SqlServerFileLabel;
            string schemaFileName = System.IO.Path.Combine( AppSettings.Instance.TaperTestsInfo.SchemaFolderName,
                                                            string.Format( AppSettings.Instance.TaperTestsInfo.ClearDataFileNameFormat, providerFileLabel ) );
            int count;
            count = provider.ExecuteScript( schemaFileName );
            return count >= 0 ? (true, string.Empty) : (false, $"Failed clearing '{provider.DatabaseName}' data from {schemaFileName}");
        }

        /// <summary> Assert clear data. </summary>
        /// <remarks> David, 2020-03-25. </remarks>
        /// <param name="provider"> The provider. </param>
        internal static void AssertClearData( ProviderBase provider )
        {
            (bool Success, string Details) r;
            r = TryClearData( provider );
            Assert.IsTrue( r.Success, $"database could not be created because {r.Details}" );
        }

        /// <summary> Cleanups this object. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        public static void Cleanup()
        {
            if ( TableCategory.None != AppSettings.Instance.TaperTestsInfo.DeleteOption )
            {
                SchemaBuilder.DeleteAllRecords( AppSettings.Instance.TaperTestsInfo.DeleteOption );
            }

            if ( TableCategory.None != AppSettings.Instance.TaperTestsInfo.DropTablesOption )
            {
                SchemaBuilder.DropAllTables( AppSettings.Instance.TaperTestsInfo.DropTablesOption, AppSettings.Instance.TaperTestsInfo.IgnoreDropTableErrors );
            }
        }

        /// <summary> Gets or sets the provider. </summary>
        /// <value> The provider. </value>
        public static ProviderBase Provider { get; private set; }

        /// <summary> The Schema builder. </summary>
        /// <value> The schema builder. </value>
        public static SchemaBuilder SchemaBuilder { get; private set; }

        /// <summary> Enumerate tables and view. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="providerType"> Type of the provider. </param>
        public static void EnumerateSchemaObjects( ProviderType providerType )
        {
            try
            {
                EnumerateSchemaObjectsThis( providerType );
            }
            catch
            {
                SchemaBuilder.EnumeratedTables.Clear();
                throw;
            }
        }

        /// <summary> Enumerate schema objects this. </summary>
        /// <remarks> David, 2020-07-04. </remarks>
        /// <param name="providerType"> Type of the provider. </param>
        private static void EnumerateSchemaObjectsThis( ProviderType providerType )
        {
            string providerFileLabel = providerType == ProviderType.SQLite ? AppSettings.Instance.TaperTestsInfo.SQLiteFileLabel : AppSettings.Instance.TaperTestsInfo.SqlServerFileLabel;
            var schemaBuilder = SchemaBuilder;
            schemaBuilder.EnumeratedTables.Clear();
            var tb = new TableMaker();

            // BUCKET BIN 
            tb = new TableMaker() {
                TableName = BucketBinBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = BucketBinBuilder.Instance.CreateTable,
                InsertAction = BucketBinBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = BucketBinEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // LOT TRAIT TYPE 
            tb = new TableMaker() {
                TableName = LotTraitTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = LotTraitTypeBuilder.Instance.CreateTable,
                InsertAction = LotTraitTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = LotTraitTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // METER MODEL
            tb = new TableMaker() {
                TableName = MeterModelBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = MeterModelBuilder.Instance.CreateTable,
                InsertAction = MeterModelBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = MeterModelEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // NOMINAL TYPE 
            tb = new TableMaker() {
                TableName = NomTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = NomTypeBuilder.Instance.CreateTable,
                InsertAction = NomTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = NomTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // NOMINAL TRAIT TYPE 
            tb = new TableMaker() {
                TableName = NomTraitTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = NomTraitTypeBuilder.Instance.CreateTable,
                InsertAction = NomTraitTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = NomTraitTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT SORT TRAIT TYPE
            tb = new TableMaker() {
                TableName = ProductSortTraitTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ProductSortTraitTypeBuilder.Instance.CreateTable,
                InsertAction = ProductSortTraitTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = ProductSortTraitTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT TRAIT TYPE
            tb = new TableMaker() {
                TableName = ProductTraitTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ProductTraitTypeBuilder.Instance.CreateTable,
                InsertAction = ProductTraitTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = ProductTraitTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // READING BIN
            tb = new TableMaker() {
                TableName = ReadingBinBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ReadingBinBuilder.Instance.CreateTable,
                InsertAction = ReadingBinBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = ReadingBinEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // READING TYPE
            tb = new TableMaker() {
                TableName = ReadingTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ReadingTypeBuilder.Instance.CreateTable,
                InsertAction = ReadingTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = ReadingTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SAMPLE TRAIT TYPE
            tb = new TableMaker() {
                TableName = SampleTraitTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = SampleTraitTypeBuilder.Instance.CreateTable,
                InsertAction = SampleTraitTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = SampleTraitTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION TRAIT TYPE 
            tb = new TableMaker() {
                TableName = SessionTraitTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = SessionTraitTypeBuilder.Instance.CreateTable,
                InsertAction = SessionTraitTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = SessionTraitTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION TYPE 
            tb = new TableMaker() {
                TableName = SessionTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = SessionTypeBuilder.Instance.CreateTable,
                InsertAction = SessionTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = SessionTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // TOLERANCE
            tb = new TableMaker() {
                TableName = ToleranceBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ToleranceBuilder.CreateTable,
                InsertAction = ToleranceBuilder.InsertValues,
                FetchAllAction = ToleranceEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // YIELD TRAIT TYPE
            tb = new TableMaker() {
                TableName = YieldTraitTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = YieldTraitTypeBuilder.Instance.CreateTable,
                InsertAction = YieldTraitTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = YieldTraitTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // METER 
            MeterBuilder.Instance.ForeignTableName = MeterModelBuilder.TableName;
            MeterBuilder.Instance.ForeignTableKeyName = nameof( MeterModelNub.Id );
            tb = new TableMaker() {
                TableName = MeterBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = x => {
                    var s = new MeterBuilder();
                    return s.CreateTable( x, UniqueIndexOptions.ForeignIdAmount );
                },
                InsertAction = MeterBuilder.InsertValues,
                FetchAllAction = MeterEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // ELEMENT TYPE
            tb = new TableMaker() {
                TableName = ElementTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ElementTypeBuilder.Instance.CreateTable,
                InsertAction = ElementTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = ElementTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // ELEMENT
            // the element has a non-unique label. the element label is unique for the part, which is enforced in code.
            tb = new TableMaker() {
                TableName = ElementBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new ElementBuilder();
                    return s.CreateTable( x, UniqueIndexOptions.None );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // ELEMENT NOMINAL TYPE
            tb = new TableMaker() {
                TableName = ElementNomTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ElementNomTypeBuilder.Instance.CreateTableNonUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // METER GUARD BAND LOOKUP
            MeterGuardBandLookupBuilder.MeterTableName = MeterBuilder.TableName;
            MeterGuardBandLookupBuilder.MeterTableKeyName = nameof( MeterNub.Id );
            MeterGuardBandLookupBuilder.NomTypeTableName = NomTypeBuilder.TableName;
            MeterGuardBandLookupBuilder.NomTypeTableKeyName = nameof( NomTypeNub.Id );
            tb = new TableMaker() {
                TableName = MeterGuardBandLookupBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = MeterGuardBandLookupBuilder.CreateTable,
                InsertFileValuesAction = MeterGuardBandLookupBuilder.InsertFileValues,
                ValuesFileName = System.IO.Path.Combine( AppSettings.Instance.TaperTestsInfo.SchemaFolderName, string.Format( AppSettings.Instance.TaperTestsInfo.GuardBandLookupFileNameFormat, providerFileLabel ) ),
                FetchAllAction = MeterGuardBandLookupEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // OHM METER SETTINGS
            tb = new TableMaker() {
                TableName = OhmMeterSettingBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmMeterSettingBuilder.CreateTable,
                InsertAction = OhmMeterSettingBuilder.InsertValues,
                FetchAllAction = OhmMeterSettingEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // OHM METER SETTINGS LOOKUP
            OhmMeterSettingLookupBuilder.MeterTableName = MeterBuilder.TableName;
            OhmMeterSettingLookupBuilder.MeterTableKeyName = nameof( MeterNub.Id );
            tb = new TableMaker() {
                TableName = OhmMeterSettingLookupBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmMeterSettingLookupBuilder.CreateTable,
                InsertFileValuesAction = OhmMeterSettingLookupBuilder.InsertFileValues,
                ValuesFileName = System.IO.Path.Combine( AppSettings.Instance.TaperTestsInfo.SchemaFolderName, string.Format( AppSettings.Instance.TaperTestsInfo.AddOhmMeterSettingLookupFileNameFormat, providerFileLabel ) ),
                FetchAllAction = OhmMeterSettingLookupEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // OHM METER SETUP
            tb = new TableMaker() {
                TableName = OhmMeterSetupBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = OhmMeterSetupBuilder.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SAMPLE TRAIT
            tb = new TableMaker() {
                TableName = SampleTraitBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new SampleTraitBuilder();
                    return s.CreateTable( x );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // YIELD TRAIT
            tb = new TableMaker() {
                TableName = YieldTraitBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new YieldTraitBuilder();
                    return s.CreateTable( x );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // COMPUTER
            tb = new TableMaker() {
                TableName = ComputerBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new ComputerBuilder();
                    return s.CreateTable( x, true );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // STATION
            tb = new TableMaker() {
                TableName = StationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new StationBuilder();
                    return s.CreateTable( x, true );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // COMPUTER STATION
            ComputerStationBuilder.Instance.PrimaryTableName = ComputerBuilder.TableName;
            ComputerStationBuilder.Instance.PrimaryTableKeyName = nameof( ComputerNub.AutoId );
            ComputerStationBuilder.Instance.SecondaryTableName = StationBuilder.TableName;
            ComputerStationBuilder.Instance.SecondaryTableKeyName = nameof( StationNub.AutoId );
            tb = new TableMaker() {
                TableName = ComputerStationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ComputerStationBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION
            tb = new TableMaker() {
                TableName = SessionBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new SessionBuilder();
                    return s.CreateTable( x, true );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION OHM METER SETUP
            SessionOhmMeterSetupBuilder.Instance.PrimaryTableName = SessionBuilder.TableName;
            SessionOhmMeterSetupBuilder.Instance.PrimaryTableKeyName = nameof( SessionNub.AutoId );
            SessionOhmMeterSetupBuilder.Instance.SecondaryTableName = MeterBuilder.TableName;
            SessionOhmMeterSetupBuilder.Instance.SecondaryTableKeyName = nameof( MeterNub.Id );
            SessionOhmMeterSetupBuilder.Instance.ForeignIdTableName = OhmMeterSetupBuilder.TableName;
            SessionOhmMeterSetupBuilder.Instance.ForeignIdTableKeyName = nameof( OhmMeterSetupNub.AutoId );
            tb = new TableMaker() {
                TableName = SessionOhmMeterSetupBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = SessionOhmMeterSetupBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION SAMPLE
            tb = new TableMaker() {
                TableName = SessionSampleBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = SessionSampleBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION YIELD
            tb = new TableMaker() {
                TableName = SessionYieldBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = SessionYieldBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );


            // STATION SESSION
            tb = new TableMaker() {
                TableName = StationSessionBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = StationSessionBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION TRAIT
            SessionTraitBuilder.Instance.PrimaryTableName = SessionBuilder.TableName;
            SessionTraitBuilder.Instance.PrimaryTableKeyName = nameof( SessionNub.AutoId );
            SessionTraitBuilder.Instance.SecondaryTableName = SessionTraitTypeBuilder.TableName;
            SessionTraitBuilder.Instance.SecondaryTableKeyName = nameof( SessionTraitTypeNub.Id );
            tb = new TableMaker() {
                TableName = SessionTraitBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = SessionTraitBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART
            tb = new TableMaker() {
                TableName = PartBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new PartBuilder();
                    return s.CreateTable( x, true );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART ELEMENT EQUATION
            tb = new TableMaker() {
                TableName = PartElementEquationBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PartElementEquationBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART NAMING TYPE
            tb = new TableMaker() {
                TableName = PartNamingTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = x => {
                    var s = new PartNamingTypeBuilder();
                    return s.CreateTable( x );
                },
                InsertAction = x => PartNamingTypeBuilder.Instance.InsertIgnoreDefaultRecords( x ),
                FetchAllAction = PartNamingTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART NAMING
            PartNamingBuilder.Instance.PrimaryTableName = PartBuilder.TableName;
            PartNamingBuilder.Instance.PrimaryTableKeyName = nameof( PartNub.AutoId );
            PartNamingBuilder.Instance.SecondaryTableName = PartNamingTypeBuilder.TableName;
            PartNamingBuilder.Instance.SecondaryTableKeyName = nameof( PartNamingTypeNub.Id );
            tb = new TableMaker() {
                TableName = PartNamingBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PartNamingBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT 
            tb = new TableMaker() {
                TableName = ProductBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ProductBuilder.Instance.CreateTableUniqueLabel
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT TEXT TYPE
            tb = new TableMaker() {
                TableName = ProductTextTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = ProductTextTypeBuilder.Instance.CreateTable,
                InsertAction = ProductTextTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = ProductTextTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT TEXT 
            tb = new TableMaker() {
                TableName = ProductTextBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ProductTextBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT TRAIT
            ProductTraitBuilder.Instance.PrimaryTableName = ProductBuilder.TableName;
            ProductTraitBuilder.Instance.PrimaryTableKeyName = nameof( ProductNub.AutoId );
            ProductTraitBuilder.Instance.SecondaryTableName = ProductTraitTypeBuilder.TableName;
            ProductTraitBuilder.Instance.SecondaryTableKeyName = nameof( ProductTraitTypeNub.Id );
            tb = new TableMaker() {
                TableName = ProductTraitBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ProductTraitBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT PART
            ProductPartBuilder.Instance.PrimaryTableName = ProductBuilder.TableName;
            ProductPartBuilder.Instance.PrimaryTableKeyName = nameof( ProductNub.AutoId );
            ProductPartBuilder.Instance.SecondaryTableName = PartBuilder.TableName;
            ProductPartBuilder.Instance.SecondaryTableKeyName = nameof( PartNub.AutoId );
            tb = new TableMaker() {
                TableName = ProductPartBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ProductPartBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT SORT
            ProductSortBuilder.Instance.ForeignTableName = ProductBuilder.TableName;
            ProductSortBuilder.Instance.ForeignTableKeyName = nameof( ProductNub.AutoId );
            tb = new TableMaker() {
                TableName = ProductSortBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new ProductSortBuilder();
                    return s.CreateTable( x, UniqueIndexOptions.ForeignIdLabel );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT SORT TRAIT
            ProductSortTraitBuilder.Instance.PrimaryTableName = ProductSortBuilder.TableName;
            ProductSortTraitBuilder.Instance.PrimaryTableKeyName = nameof( ProductSortNub.AutoId );
            ProductSortTraitBuilder.Instance.SecondaryTableName = ProductSortTraitTypeBuilder.TableName;
            ProductSortTraitBuilder.Instance.SecondaryTableKeyName = nameof( ProductSortTraitTypeNub.Id );
            tb = new TableMaker() {
                TableName = ProductSortTraitBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ProductSortTraitBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT ELEMENT
            ProductElementBuilder.Instance.PrimaryTableName = ProductBuilder.TableName;
            ProductElementBuilder.Instance.PrimaryTableKeyName = nameof( ProductNub.AutoId );
            ProductElementBuilder.Instance.SecondaryTableName = ElementBuilder.TableName;
            ProductElementBuilder.Instance.SecondaryTableKeyName = nameof( ElementNub.AutoId );
            tb = new TableMaker() {
                TableName = ProductElementBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ProductElementBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // LOT
            tb = new TableMaker() {
                TableName = LotBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new LotBuilder();
                    return s.CreateTable( x, true );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART LOT
            PartLotBuilder.Instance.PrimaryTableName = PartBuilder.TableName;
            PartLotBuilder.Instance.PrimaryTableKeyName = nameof( PartNub.AutoId );
            PartLotBuilder.Instance.SecondaryTableName = LotBuilder.TableName;
            PartLotBuilder.Instance.SecondaryTableKeyName = nameof( LotNub.AutoId );
            tb = new TableMaker() {
                TableName = PartLotBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PartLotBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // LOT TRAIT
            LotTraitBuilder.Instance.PrimaryTableName = LotBuilder.TableName;
            LotTraitBuilder.Instance.PrimaryTableKeyName = nameof( LotNub.AutoId );
            LotTraitBuilder.Instance.SecondaryTableName = LotTraitTypeBuilder.TableName;
            LotTraitBuilder.Instance.SecondaryTableKeyName = nameof( LotTraitTypeNub.Id );
            tb = new TableMaker() {
                TableName = LotTraitBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = LotTraitBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // LOT SESSION
            LotSessionBuilder.Instance.PrimaryTableName = LotBuilder.TableName;
            LotSessionBuilder.Instance.PrimaryTableKeyName = nameof( LotNub.AutoId );
            LotSessionBuilder.Instance.SecondaryTableName = SessionBuilder.TableName;
            LotSessionBuilder.Instance.SecondaryTableKeyName = nameof( SessionNub.AutoId );
            tb = new TableMaker() {
                TableName = LotSessionBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = LotSessionBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // NOMINAL TRAIT
            tb = new TableMaker() {
                TableName = NomTraitBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = x => {
                    var s = new NomTraitBuilder();
                    return s.CreateTable( x );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PART METER ELEMENT NOMINAL
            PartNomBuilder.Instance.PrimaryTableName = PartBuilder.TableName;
            PartNomBuilder.Instance.PrimaryTableKeyName = nameof( PartNub.AutoId );
            PartNomBuilder.Instance.SecondaryTableName = MeterBuilder.TableName;
            PartNomBuilder.Instance.SecondaryTableKeyName = nameof( MeterNub.Id );
            PartNomBuilder.Instance.TernaryTableName = ElementBuilder.TableName;
            PartNomBuilder.Instance.TernaryTableKeyName = nameof( ElementNub.AutoId );
            PartNomBuilder.Instance.QuaternaryTableName = NomTraitBuilder.TableName;
            PartNomBuilder.Instance.QuaternaryTableKeyName = nameof( NomTraitNub.AutoId );
            tb = new TableMaker() {
                TableName = PartNomBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = PartNomBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // PRODUCT BINNING
            tb = new TableMaker() {
                TableName = ProductBinningBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ProductBinningBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // NUT
            tb = new TableMaker() {
                TableName = NutBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = NutBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // UUT TYPE
            tb = new TableMaker() {
                TableName = UutTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = UutTypeBuilder.Instance.CreateTable,
                InsertAction = UutTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = UutTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // UUT TRAIT TYPE
            tb = new TableMaker() {
                TableName = UutTraitTypeBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = UutTraitTypeBuilder.Instance.CreateTable,
                InsertAction = UutTraitTypeBuilder.Instance.InsertIgnoreDefaultRecords,
                FetchAllAction = UutTraitTypeEntity.FetchAll
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // UUT
            // the uut has a non-unique number. the UUT number is unique for the lot, which is enforced in code.
            UutBuilder.Instance.ForeignTableName = UutTypeBuilder.TableName;
            UutBuilder.Instance.ForeignTableKeyName = nameof( UutTypeNub.Id );
            tb = new TableMaker() {
                TableName = UutBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = ( x ) => {
                    var s = new UutBuilder();
                    return s.CreateTable( x, UniqueIndexOptions.None );
                }
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // UUT TRAIT
            tb = new TableMaker() {
                TableName = UutTraitBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = UutTraitBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // UUT NUT
            UutNutBuilder.Instance.PrimaryTableName = UutBuilder.TableName; // "Dut"
            UutNutBuilder.Instance.PrimaryTableKeyName = nameof( UutNub.AutoId ); // "DutAutoId"
            UutNutBuilder.Instance.SecondaryTableName = NutBuilder.TableName;
            UutNutBuilder.Instance.SecondaryTableKeyName = nameof( NutNub.AutoId );
            tb = new TableMaker() {
                TableName = UutNutBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = UutNutBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // NUT READING 
            NutReadingBuilder.Instance.PrimaryTableName = NutBuilder.TableName;
            NutReadingBuilder.Instance.PrimaryTableKeyName = nameof( NutNub.AutoId );
            NutReadingBuilder.Instance.SecondaryTableName = ReadingTypeBuilder.TableName;
            NutReadingBuilder.Instance.SecondaryTableKeyName = nameof( ReadingTypeNub.Id );
            tb = new TableMaker() {
                TableName = NutReadingBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = NutReadingBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // NUT READING BIN
            NutReadingBinBuilder.Instance.PrimaryTableName = NutBuilder.TableName;
            NutReadingBinBuilder.Instance.PrimaryTableKeyName = nameof( NutNub.AutoId );
            NutReadingBinBuilder.Instance.SecondaryTableName = ReadingBinBuilder.TableName;
            NutReadingBinBuilder.Instance.SecondaryTableKeyName = nameof( ReadingBinNub.Id );
            tb = new TableMaker() {
                TableName = NutReadingBinBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = NutReadingBinBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // UUT PRODUCT SORT
            UutProductSortBuilder.Instance.PrimaryTableName = UutBuilder.TableName;
            UutProductSortBuilder.Instance.PrimaryTableKeyName = nameof( UutNub.AutoId );
            UutProductSortBuilder.Instance.SecondaryTableName = ProductSortBuilder.TableName;
            UutProductSortBuilder.Instance.SecondaryTableKeyName = nameof( ProductSortNub.AutoId );
            tb = new TableMaker() {
                TableName = UutProductSortBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = UutProductSortBuilder.Instance.CreateTable
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // SESSION UUT
            SessionUutBuilder.Instance.PrimaryTableName = SessionBuilder.TableName;
            SessionUutBuilder.Instance.PrimaryTableKeyName = nameof( SessionNub.AutoId );
            SessionUutBuilder.Instance.SecondaryTableName = UutBuilder.TableName;
            SessionUutBuilder.Instance.SecondaryTableKeyName = nameof( UutNub.AutoId );
            tb = new TableMaker() {
                TableName = SessionUutBuilder.TableName,
                Category = TableCategory.None,
                CreateAction = SessionUutBuilder.Instance.CreateTableUniqueSecondaryId
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // REVISION
            CurrentDatabaseVersion = AppSettings.Instance.TaperTestsInfo.DatabaseRevision;
            tb = new TableMaker() {
                TableName = RevisionBuilder.TableName,
                Category = TableCategory.Lookup,
                CreateAction = RevisionBuilder.Instance.CreateTableUniqueLabel,
                PostCreateAction = RevisionBuilder.UpsertRevision,
                PostCreateValue = CurrentDatabaseVersion,
                ExistsUpdateAction = RevisionBuilder.UpsertRevision,
                ExistsUpdateValue = CurrentDatabaseVersion
            };
            schemaBuilder.EnumeratedTables.Add( tb.TableName, tb );

            // VIEW MAKER
            var vm = new ViewMaker();

            // COMPUTER VIEW
            vm = new ViewMaker() {
                ViewName = ComputerBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ComputerBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // COMPUTER STATION VIEW
            vm = new ViewMaker() {
                ViewName = ComputerStationBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ComputerStationBuilder();
                    return s.CreateView( x, true );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // ELEMENT VIEW
            vm = new ViewMaker() {
                ViewName = ElementBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ElementBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // ELEMENT NOM TYPE VIEW
            vm = new ViewMaker() {
                ViewName = ElementNomTypeBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ElementNomTypeBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // LOT VIEW
            vm = new ViewMaker() {
                ViewName = LotBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new LotBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // LOT SESSION VIEW
            vm = new ViewMaker() {
                ViewName = LotSessionBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new LotSessionBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // LOT TRAIT VIEW
            vm = new ViewMaker() {
                ViewName = LotTraitBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new LotTraitBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // METER VIEW
            vm = new ViewMaker() {
                ViewName = MeterBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new MeterBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // NOM TRAIT VIEW
            vm = new ViewMaker() {
                ViewName = NomTraitBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new NomTraitBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // NUT VIEW
            vm = new ViewMaker() {
                ViewName = NutBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new NutBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // NUT READING VIEW
            vm = new ViewMaker() {
                ViewName = NutReadingBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new NutReadingBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // NUT READING BIN VIEW
            vm = new ViewMaker() {
                ViewName = NutReadingBinBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new NutReadingBinBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PART VIEW
            vm = new ViewMaker() {
                ViewName = PartBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new PartBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PART ELEMENT EQUATION VIEW
            vm = new ViewMaker() {
                ViewName = PartElementEquationBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new PartElementEquationBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PART LOT VIEW
            vm = new ViewMaker() {
                ViewName = PartLotBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new PartLotBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PART NAMING VIEW
            vm = new ViewMaker() {
                ViewName = PartNamingBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new PartNamingBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PART NOM VIEW
            vm = new ViewMaker() {
                ViewName = PartNomBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new PartNomBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PRODUCT VIEW
            vm = new ViewMaker() {
                ViewName = ProductBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ProductBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PRODUCT BINNING VIEW
            vm = new ViewMaker() {
                ViewName = ProductBinningBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ProductBinningBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PRODUCT ELEMENT VIEW
            vm = new ViewMaker() {
                ViewName = ProductElementBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ProductElementBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PRODUCT PART VIEW
            vm = new ViewMaker() {
                ViewName = ProductPartBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ProductPartBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PRODUCT SORT VIEW
            vm = new ViewMaker() {
                ViewName = ProductSortBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ProductSortBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PRODUCT SORT TRAIT VIEW
            vm = new ViewMaker() {
                ViewName = ProductSortTraitBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ProductSortTraitBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PRODUCT TEXT VIEW
            vm = new ViewMaker() {
                ViewName = ProductTextBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ProductTextBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // PRODUCT TRAIT VIEW
            vm = new ViewMaker() {
                ViewName = ProductTraitBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new ProductTraitBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // REVISION VIEW
            vm = new ViewMaker() {
                ViewName = RevisionBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new RevisionBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // SESSION VIEW
            vm = new ViewMaker() {
                ViewName = SessionBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new SessionBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // SESSION TRAIT VIEW
            vm = new ViewMaker() {
                ViewName = SessionTraitBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new SessionTraitBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // SESSION OHM METER SETUP VIEW
            vm = new ViewMaker() {
                ViewName = SessionOhmMeterSetupBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new SessionOhmMeterSetupBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // SESSION SAMPLE VIEW
            vm = new ViewMaker() {
                ViewName = SessionSampleBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new SessionSampleBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // SESSION UUT VIEW
            vm = new ViewMaker() {
                ViewName = SessionUutBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new SessionUutBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // SESSION YIELD VIEW
            vm = new ViewMaker() {
                ViewName = SessionYieldBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new SessionYieldBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // STATION VIEW
            vm = new ViewMaker() {
                ViewName = StationBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new StationBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // STATION SESSION VIEW
            vm = new ViewMaker() {
                ViewName = StationSessionBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new StationSessionBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // UUT VIEW
            vm = new ViewMaker() {
                ViewName = UutBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new UutBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // UUT NUT VIEW
            vm = new ViewMaker() {
                ViewName = UutNutBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new UutNutBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // UUT PRODUCT SORT VIEW
            vm = new ViewMaker() {
                ViewName = UutProductSortBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new UutProductSortBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // UUT TRAIT VIEW
            vm = new ViewMaker() {
                ViewName = UutTraitBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new UutTraitBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );

            // YIELD TRAIT VIEW
            vm = new ViewMaker() {
                ViewName = YieldTraitBuilder.Instance.ViewName,
                CreateAction = ( x ) => {
                    var s = new YieldTraitBuilder();
                    return s.CreateView( x, false );
                }
            };
            schemaBuilder.EnumeratedViews.Add( vm.ViewName, vm );
        }

        /// <summary> Gets or sets the built database version. </summary>
        /// <value> The built database version. </value>
        public static string CurrentDatabaseVersion { get; set; }

        /// <summary> Creates data base. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void CreateDatabase( string masterConnectionString )
        {
            if ( AppSettings.Instance.TaperTestsInfo.CreateDatabase || !Provider.DatabaseExists() )
            {
                if ( System.IO.File.Exists( Provider.FileName ) )
                    System.IO.File.Delete( Provider.FileName );
                // using the temp db.
                _ = CreateDatabaseThis( masterConnectionString, string.Empty );
            }
        }

        /// <summary>   Creates database. </summary>
        /// <remarks>   David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString">       The master connection string. </param>
        /// <param name="createDatabaseFileNameFormat"> The create database file name format. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        private static bool CreateDatabaseThis( string masterConnectionString, string createDatabaseFileNameFormat )
        {
            bool result = Provider.CreateDatabase( masterConnectionString, Provider.DatabaseName );
            int count;
            string schemaFileName;
            if ( result && !string.IsNullOrEmpty( createDatabaseFileNameFormat ) )
            {
                string providerFileLabel = Provider.ProviderType == ProviderType.SQLite ? AppSettings.Instance.TaperTestsInfo.SQLiteFileLabel : AppSettings.Instance.TaperTestsInfo.SqlServerFileLabel;
                schemaFileName = System.IO.Path.Combine( AppSettings.Instance.TaperTestsInfo.SchemaFolderName, string.Format( createDatabaseFileNameFormat, providerFileLabel ) );
                count = Provider.ExecuteScript( schemaFileName );
                result = count > 0;
            }
            return result;
        }

        /// <summary> Create or clear database and add all tables. </summary>
        /// <remarks> David, 2020-03-23. </remarks>
        /// <param name="masterConnectionString"> The master connection string. </param>
        public static void BuildDatabase( string masterConnectionString )
        {
            CreateDatabase( masterConnectionString );
            if ( AppSettings.Instance.TaperTestsInfo.ClearDatabase )
                SchemaBuilder.DropAllTables( AppSettings.Instance.TaperTestsInfo.IgnoreDropTableErrors );
            SchemaBuilder.BuildSchema();
        }

        /// <summary> Initializes the lookup entities. </summary>
        /// <remarks> David, 2020-07-11. </remarks>
        /// <param name="provider"> The provider. </param>
        public static void InitializeLookupEntities( ProviderBase provider )
        {
            if ( provider is null )
                return;
            SchemaBuilder = new SchemaBuilder() { Provider = provider };
            EnumerateSchemaObjects( provider.ProviderType );
            SchemaBuilder.FetchAll();
        }

        /// <summary> Initializes the lookup entities. </summary>
        /// <remarks> David, 2020-07-11. </remarks>
        public void InitializeLookupEntities()
        {
            InitializeLookupEntities( this.GetProvider() );
        }
    }
}

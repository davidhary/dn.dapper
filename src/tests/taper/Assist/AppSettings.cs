using System;

using isr.Dapper.Taper.MSTest;

namespace isr.Dapper.Taper.MSTest
{
    /// <summary>   An application settings. </summary>
    /// <remarks>   David, 2021-01-28. </remarks>
    public class AppSettings : isr.Json.JsonAppSettingsBase
    {

        #region " CONSTRUCTION "

        private AppSettings() : base()
        {
            this.Settings = new Settings();
            base.AddSettingsSection( this.Settings );
            this.TestSiteSettings = new TestSiteSettings();
            base.AddSettingsSection( this.TestSiteSettings );
            this.TaperTestsInfo = new TaperTestsInfo();
            base.AddSettingsSection( this.TaperTestsInfo );
            this.ReadSettings();
        }

        /// <summary>   The lazy instance. </summary>
        private static readonly Lazy<AppSettings> LazyInstance = new( () => new AppSettings() );

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static AppSettings Instance => LazyInstance.Value;

        #endregion

        /// <summary>   Gets the full name of the application settings file. </summary>
        /// <value> The full name of the application settings file. </value>
        public string AppSettingsFullFileName => this.Settings.FullFileName;

        /// <summary>   Gets or sets options for controlling the operation. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The settings. </value>
        internal Settings Settings { get; }

        /// <summary>   Gets or sets the test site settings. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The test site settings. </value>
        public TestSiteSettings TestSiteSettings { get; }

        /// <summary>   Gets information describing the Dapper.Taper tests. </summary>
        /// <value> Information describing the Dapper.Taper tests. </value>
        internal TaperTestsInfo TaperTestsInfo { get; }

    }

}

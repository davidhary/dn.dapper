using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Dapper.Contrib.Extensions;

using isr.Std.Primitives;

using FastEnums;
using isr.Dapper.Entity;
using isr.Dapper.Entity.ConnectionExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Entities.MSTest
{

    /// <summary> An auditor: Owner of the actual test code. </summary>
    /// <remarks> David, 2020-03-26. </remarks>
    public sealed partial class Auditor
    {
        /// <summary> Assert Lot ohm meter setup entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">      The site. </param>
        /// <param name="provider">  The provider. </param>
        /// <param name="lotAutoId"> Identifier for the <see cref="LotEntity"/> identity. </param>
        public static void AssertLotOhmMeterSetupEntity( ProviderBase provider, int lotAutoId )
        {
            AssertTablesExist( provider, new string[] { OhmMeterSettingLookupBuilder.TableName, OhmMeterSetupBuilder.TableName, OhmMeterSettingBuilder.TableName, LotOhmMeterSetupBuilder.TableName } );
            int ohmMeterSettingLookupId = 0;
            var ohmMeterSettingLookup = new OhmMeterSettingLookupEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = ohmMeterSettingLookup.FetchUsingKey( connection, ohmMeterSettingLookupId );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {ohmMeterSettingLookupId} should not exist" );
            r = ohmMeterSettingLookup.ValidateNewEntity( $"New {nameof( isr.Dapper.Entities.IOhmMeterSettingLookup )} with {nameof( isr.Dapper.Entities.IOhmMeterSettingLookup.OhmMeterSettingId )} equals {ohmMeterSettingLookupId}" );
            Assert.IsTrue( r.Success, r.Details );
            ohmMeterSettingLookup.Clear();
            var meterEntities = MeterEntity.FetchAllEntities( connection, false );
            int expectedCount = 5;
            Assert.AreEqual( expectedCount, meterEntities.Count(), $"There should be enough meter entities" );
            string expectedToleranceCode = "A";
            double resistance = 399d;
            OhmMeterSettingLookupNub ohmMeterSettingLookupNub;
            bool actualExists;
            var ohmMeterSettingEntity = new OhmMeterSettingEntity();
            var ohmMeterSetupEntity = new OhmMeterSetupEntity();
            int actualCount;

            // clear the table
            _ = connection.DeleteAll<LotOhmMeterSetupNub>();
            _ = connection.DeleteAll<OhmMeterSetupNub>();
            int id = 1;
            ohmMeterSettingEntity = new OhmMeterSettingEntity() { Id = id };
            actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex( connection );
            Assert.IsTrue( actualExists, $"{nameof( OhmMeterSettingEntity )} should exist for auto id = {id}" );
            actualCount = OhmMeterSettingEntity.CountEntities( connection );
            expectedCount = 6; // at least 6
            Assert.IsTrue( actualCount >= expectedCount, $"{nameof( OhmMeterSettingEntity )} should have at least {expectedCount} records" );
            var LotOhmMeterSetupEntity = new LotOhmMeterSetupEntity();
            expectedCount = 0;
            actualCount = connection.CountEntities( LotOhmMeterSetupBuilder.TableName );
            Assert.AreEqual( expectedCount, actualCount, $"Lot ohm meter setup nub record count should match" );
            foreach ( MeterEntity MeterEntity in meterEntities )
            {
                if ( MeterEntity.MeterModelId == ( int ) MeterModel.CLT10 )
                    continue;
                ohmMeterSettingLookupNub = OhmMeterSettingLookupEntity.FetchNubs( connection, expectedToleranceCode, MeterEntity.Id, resistance ).First();
                ohmMeterSettingEntity = new OhmMeterSettingEntity() { Id = ohmMeterSettingLookupNub.OhmMeterSettingId };
                actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex( connection );
                Assert.IsTrue( actualExists, $"ohm meter setting condition should exist for id = {ohmMeterSettingEntity.Id}" );
                ohmMeterSetupEntity = new OhmMeterSetupEntity();
                ohmMeterSetupEntity.Copy( ohmMeterSettingEntity );
                actualCount = OhmMeterSetupEntity.CountEntities( connection );
                expectedCount = actualCount;
                expectedCount += 1;
                actualExists = ohmMeterSetupEntity.Insert( connection );
                Assert.IsTrue( actualExists, $"A new resistance ohm meter setup condition should be stored for meter {MeterEntity.MeterModelId} number {MeterEntity.MeterNumber}" );
                actualCount = OhmMeterSetupEntity.CountEntities( connection );
                Assert.IsTrue( actualCount >= expectedCount, $"Resistance ohm meter setup condition should have at least {expectedCount} records" );
                LotOhmMeterSetupEntity = new LotOhmMeterSetupEntity() {
                    LotAutoId = lotAutoId,
                    OhmMeterSetupAutoId = ohmMeterSetupEntity.AutoId,
                    MeterId = MeterEntity.Id
                };
                actualExists = LotOhmMeterSetupEntity.Insert( connection );
                Assert.IsTrue( actualExists, $"A new Lot ohm meter setup condition should be stored for meter {MeterEntity.MeterModelId} and Lot id {lotAutoId}" );
                actualCount = Entities.LotOhmMeterSetupEntity.CountEntities( connection, lotAutoId );
                Assert.IsTrue( actualCount >= expectedCount, $"Lot ohm meter setup condition should have at least {expectedCount} records" );
            }
        }


    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Dapper.Contrib.Extensions;

using isr.Std.Primitives;

using FastEnums;
using isr.Dapper.Entity;
using isr.Dapper.Entity.ConnectionExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Entities.MSTest
{

    /// <summary> An auditor: Owner of the actual test code. </summary>
    /// <remarks> David, 2020-03-26. </remarks>
    public sealed partial class Auditor
    {

        #region " NUT "

        /// <summary> Assert lot Nut entity. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="productNumber"> The product number. </param>
        /// <param name="partNumber">    The part number. </param>
        /// <param name="lotNumbers">    The lot numbers. </param>
        public static void AssertLotNutEntity( ProviderBase provider, string productNumber, string partNumber, IEnumerable<string> lotNumbers )
        {
            NutReadingBinEntity nutReadingBin;
            NutEntity nut;
            ElementEntity element;
            UutEntity uut;

            (bool Success, string Details) r;
            using var connection = provider.GetConnection();

            // Use 2002 meter to allow selection of equations.
            int meterNumber = 1;
            int meterModel = ( int ) MeterModel.K2002;
            if ( MeterEntity.Meters is not object ) _ = MeterEntity.TryFetchAll( connection );
            PlatformMeter platformMeter = new( MeterEntity.SelectMeterEntity( meterModel, meterNumber ) );
            PlatformMeter[] platformMeters = new PlatformMeter[] { platformMeter };
            int meterId = platformMeter.MeterId;

            _ = connection.DeleteAll<PartNub>();
            _ = connection.DeleteAll<LotNub>();
            _ = connection.DeleteAll<UutNub>();
            _ = connection.DeleteAll<LotUutNub>();
            _ = connection.DeleteAll<UutProductSortNub>();
            _ = connection.DeleteAll<NutNub>();
            _ = connection.DeleteAll<ElementNub>();
            int expectedElementCount = 4;
            var product = AssertAddingProductEntity( provider, productNumber );
            var productElement = new ProductElementEntity() { ProductAutoId = product.AutoId };
            productElement.AddElements( connection, productNumber );
            int actualCount = product.FetchElements( connection );
            Assert.AreEqual( expectedElementCount, actualCount, $"inserted {nameof( ProductEntity )} {productNumber} {nameof( ProductEntity.Elements )} count should match" );
            var part = AssertAddingPartEntity( provider, partNumber );
            var productPart = new ProductPartEntity() { ProductAutoId = product.AutoId, PartAutoId = part.AutoId };
            _ = productPart.Obtain( connection );
            r = productPart.ValidateStoredEntity( $"Obtained {nameof( isr.Dapper.Entities.ProductPartEntity )} With {nameof( isr.Dapper.Entities.ProductPartEntity.ProductAutoId )} of {product.AutoId} and {nameof( isr.Dapper.Entities.ProductPartEntity.PartAutoId )} of {part.AutoId}" );
            Assert.IsTrue( r.Success, r.Details );
            _ = AssertAddingElementTraits( provider, product, part, platformMeter );
            int lastUutNumber = 0;
            string lotNumber = lotNumbers.ElementAtOrDefault( 0 );
            uut = AssertAddingLotUutEntity( provider, meterId, lotNumber, lastUutNumber );
            lastUutNumber = uut.UutNumber;
            foreach ( ElementInfo elementInfo in ProductElementEntity.SelectElementInfo( productNumber ) )
            {
                element = part.Elements.SelectElementByElementLabel( elementInfo.Label );
                expectedElementCount += 1;
                nut = AssertAddingNutEntity( provider, uut.AutoId, element.AutoId, elementInfo.NomTypeId );
                nut.InitializeNutReadings( uut, element );
                _ = nut.FetchNutReadings( connection );
                nutReadingBin = AssertStoringNutBin( provider, nut.AutoId, ( int ) ReadingBin.Good );
                nut = AssertAddingNutReadings( provider, element, uut, nut.AutoId );
            }

            lotNumber = lotNumbers.ElementAtOrDefault( 1 );
            lastUutNumber = 0;
            uut = AssertAddingLotUutEntity( provider, meterId, lotNumber, lastUutNumber );
            lastUutNumber = uut.UutNumber;
            foreach ( ElementInfo elementInfo in ProductElementEntity.SelectElementInfo( productNumber ) )
            {
                element = product.Elements.SelectElementByElementLabel( elementInfo.Label );
                expectedElementCount += 1;
                nut = AssertAddingNutEntity( provider, uut.AutoId, element.AutoId, elementInfo.NomTypeId );
                nut.InitializeNutReadings( uut, element );
                _ = nut.FetchNutReadings( connection );
                nutReadingBin = AssertStoringNutBin( provider, nut.AutoId, ( int ) ReadingBin.Good );
                nut = AssertAddingNutReadings( provider, element, uut, nut.AutoId );
            }

            uut = AssertAddingLotUutEntity( provider, meterId, lotNumber, lastUutNumber );
            lastUutNumber = uut.UutNumber;
            foreach ( ElementInfo elementInfo in ProductElementEntity.SelectElementInfo( productNumber ) )
            {
                element = product.Elements.SelectElementByElementLabel( elementInfo.Label );
                expectedElementCount += 1;
                nut = AssertAddingNutEntity( provider, uut.AutoId, element.AutoId, elementInfo.NomTypeId );
                nut.InitializeNutReadings( uut, element );
                _ = nut.FetchNutReadings( connection );
                nutReadingBin = AssertStoringNutBin( provider, nut.AutoId, ( int ) ReadingBin.Good );
                nut = AssertAddingNutReadings( provider, element, uut, nut.AutoId );
            }
        }


        #endregion


        #region " LOT UUT "

        /// <summary>   Assert adding uut entity. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="meterId">          Identifier for the meter. </param>
        /// <param name="lotNumber">        The lot number. </param>
        /// <param name="lastUutNumber">    The last uut number. </param>
        /// <returns>   An UutEntity. </returns>
        public static UutEntity AssertAddingLotUutEntity( ProviderBase provider, int meterId, string lotNumber, int lastUutNumber )
        {
            UutEntity uut;
            LotUutEntity lotUut;
            int uutNumber;
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                string tableName = UutBuilder.TableName;
                Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
                Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
                tableName = LotUutBuilder.TableName;
                Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
                Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
                bool actualObtained;
                lotUut = new LotUutEntity();
                r = lotUut.TryObtain( connection, lotNumber, -1, ( int ) UutType.Chip );
                actualObtained = r.Success;
                Assert.IsTrue( r.Success, $"{nameof( LotUutEntity )} with '{nameof( LotUutEntity.LotAutoId )}' of {lotNumber} should have been inserted; {r.Details}" );
                r = lotUut.ValidateStoredEntity( $"Insert {nameof( LotUutEntity )} with '{nameof( LotUutEntity.LotAutoId )}' of {lotNumber}" );
                Assert.IsTrue( r.Success, r.Details );
                uutNumber = lotUut.UutEntity.UutNumber;
                Assert.AreEqual( lastUutNumber + 1, uutNumber, $"{nameof( LotUutEntity )} {nameof( UutEntity )} {nameof( UutEntity.UutNumber )} should match" );
                r = lotUut.TryObtainUut( connection, lotUut.LotAutoId, uutNumber, ( int ) UutType.Chip );
                Assert.IsTrue( r.Success, r.Details );
                Assert.AreEqual( uutNumber, lotUut.UutEntity.UutNumber, $"Fetch {nameof( UutEntity )} {nameof( UutEntity.UutNumber )} should match" );
                uut = lotUut.FetchUutEntity( connection );
                r = uut.ValidateStoredEntity( $"Fetch {nameof( UutEntity )} from {nameof( LotUutEntity )} with '{nameof( LotUutEntity.UutAutoId )}' of {lotUut.UutAutoId}" );
                Assert.IsTrue( r.Success, r.Details );
                uut = LotUutEntity.FetchLastUut( connection, lotUut.LotAutoId );
                Assert.AreEqual( uutNumber, uut.UutNumber, $"Fetched last {nameof( UutEntity )} {nameof( UutEntity.UutNumber )} should match" );
                r = uut.ValidateStoredEntity( $"Fetched last {nameof( UutEntity )}" );
                Assert.IsTrue( r.Success, r.Details );
                _ = uut.FetchTraits( connection );
                uut.Traits.UutTrait.MeterId = meterId;
                _ = uut.Traits.Upsert( connection );
            }

            return uut;
        }

        /// <summary> Assert adding uut entity. </summary>
        /// <remarks> David, 2020-05-18. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="lotAutoId">     Identifier for the <see cref="LotEntity"/>. </param>
        /// <param name="lastUutNumber"> The last uut number. </param>
        /// <returns> An UutEntity. </returns>
        public static UutEntity AssertAddingLotUutEntity( ProviderBase provider, int lotAutoId, int lastUutNumber )
        {
            int uutNumber;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = UutBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            tableName = LotUutBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            var lotUut = new LotUutEntity() { LotAutoId = lotAutoId };
            r = lotUut.TryObtainUut( connection, lotAutoId, -1, ( int ) UutType.Chip );
            Assert.IsTrue( r.Success, r.Details );
            r = lotUut.ValidateStoredEntity( $"Insert {nameof( LotUutEntity )} with  [{nameof( LotUutEntity.LotAutoId )},[{nameof( LotUutEntity.UutAutoId )}] of [{lotAutoId},{lotUut.UutAutoId}]" );
            Assert.IsTrue( r.Success, r.Details );
            uutNumber = lotUut.UutEntity.UutNumber;
            Assert.AreEqual( lastUutNumber + 1, uutNumber, $"{nameof( LotUutEntity )} {nameof( UutEntity )} {nameof( UutEntity.UutNumber )} should match" );
            r = lotUut.TryObtainUut( connection, lotAutoId, uutNumber, ( int ) UutType.Chip );
            Assert.IsTrue( r.Success, r.Details );
            Assert.AreEqual( uutNumber, lotUut.UutEntity.UutNumber, $"Fetch {nameof( UutEntity )} {nameof( UutEntity.UutNumber )} should match" );
            return lotUut.UutEntity;
        }

        /// <summary> Assert uut entity. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="lotNumbers"> The lot numbers. </param>
        public static void AssertLotUutEntity( ProviderBase provider, string[] lotNumbers )
        {
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<UutNub>();
            _ = connection.DeleteAll<LotUutNub>();
            _ = connection.DeleteAll<UutProductSortNub>();

            // Use 2002 meter to allow selection of equations.
            int meterNumber = 1;
            int meterModel = ( int ) MeterModel.K2002;
            if ( MeterEntity.Meters is not object ) _ = MeterEntity.TryFetchAll( connection );
            PlatformMeter platformMeter = new( MeterEntity.SelectMeterEntity( meterModel, meterNumber ) );
            PlatformMeter[] platformMeters = new PlatformMeter[] { platformMeter };
            int meterId = platformMeter.MeterId;

            int expectedLastUutNumber = 0;
            UutEntity uut = null;
            foreach ( var lotNumber in lotNumbers )
            {
                expectedLastUutNumber = 0;
                uut = AssertAddingLotUutEntity( provider, meterId, lotNumber, expectedLastUutNumber );
                expectedLastUutNumber = uut.UutNumber;
                _ = AssertAddingLotUutEntity( provider, meterId, lotNumber, expectedLastUutNumber );
            }
        }

        #endregion

    }

}

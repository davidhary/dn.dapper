using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Dapper.Contrib.Extensions;

using FastEnums;
using isr.Std.Primitives;

using isr.Dapper.Entity;
using isr.Dapper.Entity.ConnectionExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Dapper.Entities.MSTest
{

    /// <summary> An auditor: Owner of the actual test code. </summary>
    /// <remarks> David, 2020-03-26. </remarks>
    public sealed partial class Auditor
    {

        #region " PROVIDER TESTS "

        /// <summary> Asserts connection exists. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="provider"> The provider. </param>
        public static void AssertConnectionExists( ProviderBase provider )
        {
            using var connection = provider.GetConnection();
            Assert.IsTrue( provider.ConnectionExists(), $"connection {connection.ConnectionString} should exist" );
        }

        /// <summary> Asserts tables exist. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="provider">   The provider. </param>
        /// <param name="tableNames"> List of names of the tables. </param>
        public static void AssertTablesExist( ProviderBase provider, IEnumerable<string> tableNames )
        {
            using var connection = provider.GetConnection();
            foreach ( string name in tableNames )
                Assert.IsTrue( provider.TableExists( connection, name ), $"table {name} should exist" );
        }

        /// <summary> Assert table exist. </summary>
        /// <remarks> David, 2020-05-22. </remarks>
        /// <param name="site">      The site. </param>
        /// <param name="provider">  The provider. </param>
        /// <param name="tableName"> Name of the table. </param>
        public static void AssertTableExist( ProviderBase provider, string tableName )
        {
            using var connection = provider.GetConnection();
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
        }

        #endregion

        #region " DAPPER FRAMEWORK TESTS "

        /// <summary>   Assert inserted identity key. </summary>
        /// <remarks>   David, 2020-05-08. </remarks>
        /// <param name="provider">     The provider. </param>
        /// <param name="partNumber">   The part number. </param>
        public static void AssertInsertedIdentityKey( ProviderBase provider, String partNumber )
        {
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<PartNub>();
            Assert.IsNull( connection.Get<PartNub>( 3 ) );
            var part = new PartNub() { Label = partNumber };
            int id = ( int ) connection.Insert( part );
            Assert.AreEqual( part.AutoId, id );
            var partFetched = connection.Get<PartNub>( id );
            Assert.AreEqual( part.AutoId, partFetched.AutoId, "Fetched part has incorrect key" );
        }

        #endregion

        #region " ELEMENT "

        /// <summary> Gets the element labels. </summary>
        /// <value> The element labels. </value>
        public static IEnumerable<string> ElementLabels => new string[] { "R1", "R2", "D1", "M1" };

        /// <summary> Select element nominal value. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="label"> The label. </param>
        /// <returns> A Double. </returns>
        public static double SelectElementNominalValue( string label )
        {
            double r1 = 100d;
            double r2 = 200d;
            switch ( label ?? "" )
            {
                case "R1":
                    {
                        return r1;
                    }

                case "R2":
                    {
                        return r2;
                    }

                case "D1":
                    {
                        return r1 + r2;
                    }

                case "M1":
                    {
                        return r1 / r2;
                    }

                default:
                    {
                        throw new System.ArgumentException( $"Unhandled [{nameof( isr.Dapper.Entities.ElementEntity )}].[{nameof( isr.Dapper.Entities.ElementEntity.Label )}] of {label}" );
                    }
            }
        }

        /// <summary> Select element tolerance. </summary>
        /// <remarks> David, 2020-06-01. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="label"> The label. </param>
        /// <returns> A Double. </returns>
        public static double SelectElementTolerance( string label )
        {
            switch ( label ?? "" )
            {
                case "R1":
                    {
                        return 0.01d;
                    }

                case "R2":
                    {
                        return 0.01d;
                    }

                case "D1":
                    {
                        return 0.01d;
                    }

                case "M1":
                    {
                        return 0.01d;
                    }

                default:
                    {
                        throw new System.ArgumentException( $"Unhandled [{nameof( isr.Dapper.Entities.ElementEntity )}].[{nameof( isr.Dapper.Entities.ElementEntity.Label )}] of {label}" );
                    }
            }
        }

        /// <summary> Select element equation. </summary>
        /// <remarks> David, 2020-07-16. </remarks>
        /// <param name="label">        The label. </param>
        /// <param name="nominalValue"> The nominal value. </param>
        /// <param name="tolerance">    The tolerance. </param>
        /// <returns> A String such as |R1|R2|/|==|0.33333|0.001|. </returns>
        public static string SelectElementEquation( string label, double nominalValue, double tolerance )
        {
            string equationFormat = "|R1|R2|/|==|{0:G5}|{1}|";
            return string.Equals( label, "M1" ) ? string.Format( equationFormat, nominalValue, tolerance ) : string.Empty;
        }

        /// <summary> Select element current source. </summary>
        /// <remarks> David, 2020-06-01. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="label"> The label. </param>
        /// <returns> A Double. </returns>
        public static double SelectElementCurrentSource( string label )
        {
            switch ( label ?? "" )
            {
                case "R1":
                    {
                        return 0.001d;
                    }

                case "R2":
                    {
                        return 0.001d;
                    }

                case "D1":
                    {
                        return 0.001d;
                    }

                case "M1":
                    {
                        return 0.0d;
                    }

                default:
                    {
                        throw new System.ArgumentException( $"Unhandled [{nameof( isr.Dapper.Entities.ElementEntity )}].[{nameof( isr.Dapper.Entities.ElementEntity.Label )}] of {label}" );
                    }
            }
        }

        /// <summary> Select element type. </summary>
        /// <remarks> David, 2020-06-01. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="label"> The label. </param>
        /// <returns> An ElementType. </returns>
        public static ElementType SelectElementType( string label )
        {
            switch ( label ?? "" )
            {
                case "R1":
                    {
                        return ElementType.Resistor;
                    }

                case "R2":
                    {
                        return ElementType.Resistor;
                    }

                case "D1":
                    {
                        return ElementType.CompoundResistor;
                    }

                case "M1":
                    {
                        return ElementType.Equation;
                    }

                default:
                    {
                        throw new System.ArgumentException( $"Unhandled [{nameof( isr.Dapper.Entities.ElementEntity )}].[{nameof( isr.Dapper.Entities.ElementEntity.Label )}] of {label}" );
                    }
            }
        }

        /// <summary>   Assert adding element Traits. </summary>
        /// <remarks>   David, 2020-05-21. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="part">             The part. </param>
        /// <param name="element">          The element. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        public static void AssertAddingElementNomTraits( ProviderBase provider, PartEntity part, ElementEntity element, PlatformMeter platformMeter )
        {
            NomTraitEntity nomTraitEntity;
            NomTraitEntity sigmaLevelNomTrait;
            NomTraitEntity toleranceNomTrait;
            NomTraitEntity guardBandFactorNomTrait;
            NomTraitEntity lowerGuardedSpecificationLimitNomTrait;
            NomTraitEntity upperGuardedSpecificationLimitNomTrait;

            // var meterIdentities = new int[] { meterId };
            var nomType = element.NomTypes.First().NomType;
            PartElementEquationEntity partElementEquation;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();

            var nomTraitSelector = new MeterElementNomTypeSelector( platformMeter.MeterId, element.AutoId, ( int ) nomType );

            double tolerance = SelectElementTolerance( element.Label );
            double nominalValue = SelectElementNominalValue( element.Label );
            double lowerOverflowLimit = 0.1d * nominalValue;
            double upperOverflowLimit = 10d * nominalValue;
            string equation = SelectElementEquation( element.Label, nominalValue, tolerance );
            if ( !string.IsNullOrEmpty( equation ) )
            {
                partElementEquation = new PartElementEquationEntity() { PartAutoId = part.AutoId, ElementAutoId = element.AutoId, Equation = equation };
                _ = partElementEquation.Obtain( connection );
                r = partElementEquation.ValidateStoredEntity( $"{nameof( PartElementEquationEntity )} with [{nameof( PartElementEquationEntity.PartAutoId )},{nameof( PartElementEquationEntity.ElementAutoId )},{nameof( PartElementEquationEntity.Equation )}] of [{part.AutoId},{element.AutoId},{equation}]" );
                Assert.IsTrue( r.Success, r.Details );
                _ = part.FetchElementEquations( connection );
                Assert.AreEqual( equation, part.ElementEquations.Equation( element.AutoId ), $"{nameof( isr.Dapper.Entities.PartUniqueElementEquationEntityCollection )}.{nameof( isr.Dapper.Entities.PartUniqueElementEquationEntityCollection.Equation )} should match" );
            }

            var rnd = new Random( DateTime.UtcNow.Millisecond );
            double expectedNominalPower = 0.25d;
            double sigmaPerformanceLevel = 6d;
            double guardBandFactor = 0.1d * ( double ) ( int ) (10d * (1d + rnd.NextDouble()));
            var expectedSpecificationRange = new RangeR( nominalValue * (1d - tolerance), nominalValue * (1d + tolerance) );
            double expectedGuardedSpecificationLimit = tolerance * (1d - guardBandFactor / sigmaPerformanceLevel);
            var expectedGuardedSpecificationRange = new RangeR( nominalValue * (1d - expectedGuardedSpecificationLimit), nominalValue * (1d + expectedGuardedSpecificationLimit) );
            int expectedTraitCount = 0;
            if ( !part.NomTraits.Contains( nomTraitSelector ) )
                part.NomTraits.Add( new PartNomEntityCollection( part, platformMeter.Meter, element, ( int ) nomType ) );
            var elementMeterNomTraits = part.NomTraits[nomTraitSelector];
            var nomTrait = elementMeterNomTraits.NomTrait;
            nomTrait.SigmaPerformanceLevel = sigmaPerformanceLevel;
            sigmaLevelNomTrait = elementMeterNomTraits.Entity( ( int ) NomTraitType.SigmaPerformanceLevel );
            expectedTraitCount += 1;
            nomTrait.NominalPower = expectedNominalPower;
            expectedTraitCount += 1;
            nomTrait.LowerOverflowLimit = lowerOverflowLimit;
            expectedTraitCount += 1;
            nomTrait.UpperOverflowLimit = upperOverflowLimit;
            expectedTraitCount += 1;
            nomTrait.NominalValue = nominalValue;
            nomTraitEntity = elementMeterNomTraits.Entity( ( int ) NomTraitType.NominalValue );
            expectedTraitCount += 1;
            nomTrait.Tolerance = tolerance;
            toleranceNomTrait = elementMeterNomTraits.Entity( ( int ) NomTraitType.Tolerance );
            expectedTraitCount += 1;
            nomTrait.GuardBandFactor = guardBandFactor;
            guardBandFactorNomTrait = elementMeterNomTraits.Entity( ( int ) NomTraitType.GuardBandFactor );
            expectedTraitCount += 1;
            nomTrait.LowerGuardedSpecificationLimit = expectedGuardedSpecificationRange.Min;
            lowerGuardedSpecificationLimitNomTrait = elementMeterNomTraits.Entity( ( int ) NomTraitType.LowerGuardedSpecificationLimit );
            expectedTraitCount += 1;
            nomTrait.UpperGuardedSpecificationLimit = expectedGuardedSpecificationRange.Max;
            upperGuardedSpecificationLimitNomTrait = elementMeterNomTraits.Entity( ( int ) NomTraitType.UpperGuardedSpecificationLimit );
            expectedTraitCount += 1;
            Assert.AreEqual( expectedTraitCount, part.NomTraits[nomTraitSelector].Count, $"{nameof( PartNomEntityCollection )} element count should match" );

            part.NomTraits.Upsert( connection );
            Assert.AreEqual( expectedTraitCount, part.NomTraits[nomTraitSelector].Count, $"{nameof( PartNomEntityCollection )} element count should match after upsert" );
            r = sigmaLevelNomTrait.ValidateStoredEntity( $"{nameof( NomTraitEntity )} Meter-specific {nameof( nomTrait.SigmaPerformanceLevel )} should be clean" );
            Assert.IsTrue( r.Success, r.Details );

            _ = part.FetchNomTraits( connection, new PlatformMeter[] { platformMeter } );
            Assert.AreEqual( expectedTraitCount, part.NomTraits[nomTraitSelector].Count, $"{nameof( PartNomEntityCollection )} element count should match after fetch" );
            Assert.IsTrue( nomTrait.NominalValue.HasValue, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.NominalValue )} should have a value" );
            Assert.AreEqual( nominalValue, nomTrait.NominalValue.Value, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.NominalValue )} value should match" );
            Assert.IsTrue( nomTrait.NominalPower.HasValue, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.NominalPower )} should have a value" );
            Assert.AreEqual( expectedNominalPower, nomTrait.NominalPower.Value, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.NominalPower )} value should match" );
            Assert.IsTrue( nomTrait.Tolerance.HasValue, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.Tolerance )} should have a value" );
            Assert.AreEqual( tolerance, nomTrait.Tolerance.Value, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.Tolerance )} value should match" );
            Assert.IsTrue( nomTrait.SigmaPerformanceLevel.HasValue, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.SigmaPerformanceLevel )} should have a value" );
            Assert.AreEqual( sigmaPerformanceLevel, nomTrait.SigmaPerformanceLevel.Value, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.SigmaPerformanceLevel )} value should match" );
            Assert.IsTrue( nomTrait.GuardBandFactor.HasValue, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.GuardBandFactor )} should have a value" );
            Assert.AreEqual( guardBandFactor, nomTrait.GuardBandFactor.Value, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.GuardBandFactor )} value should match" );
            Assert.AreEqual( expectedGuardedSpecificationLimit, nomTrait.EstimatedGuardedSpecificationLimit, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.EstimatedGuardedSpecificationLimit )} value should match" );
            Assert.AreEqual( expectedSpecificationRange.Min, nomTrait.EstimatedSpecificationRange.Min, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.EstimatedSpecificationRange )} minimum value should match" );
            Assert.AreEqual( expectedSpecificationRange.Max, nomTrait.EstimatedSpecificationRange.Max, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.EstimatedSpecificationRange )} maximum value should match" );
            Assert.AreEqual( expectedGuardedSpecificationRange.Min, nomTrait.EstimatedGuardedSpecificationRange.Min, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.EstimatedGuardedSpecificationRange )} minimum value should match" );
            Assert.AreEqual( expectedGuardedSpecificationRange.Max, nomTrait.EstimatedGuardedSpecificationRange.Max, $"{nameof( PartNomEntityCollection )} {nameof( NomTrait.EstimatedGuardedSpecificationRange )} maximum value should match" );
        }

        /// <summary> Assert adding element texts. </summary>
        /// <remarks> David, 2020-07-06. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        /// <param name="element">  The element. </param>
        public static void AssertAddingElementTexts( ProviderBase provider, ElementEntity element )
        {
            AssertTablesExist( provider, new string[] { ElementTextTypeBuilder.TableName, ElementNomTextBuilder.TableName } );
            using var connection = provider.GetConnection();
            _ = element.FetchNomTexts( connection );
            if ( ( ElementType ) element.ElementTypeId == ElementType.CompoundResistor || ( ElementType ) element.ElementTypeId == ElementType.Resistor )
            {
                element.NomTexts[( int ) NomType.Resistance].ElementText.ScanList = "@(1)";
                Assert.IsTrue( (element.NomTexts?.Any()).GetValueOrDefault( false ),
                                      $"{nameof( isr.Dapper.Entities.ElementEntity )} should have {nameof( isr.Dapper.Entities.ElementEntity.NomTexts )}" );
                _ = element.FetchNomTexts( connection );
                Assert.IsTrue( (element.NomTexts?.Any()).GetValueOrDefault( false ),
                                      $"{nameof( isr.Dapper.Entities.ElementEntity )} should have {nameof( isr.Dapper.Entities.ElementEntity.NomTexts )}" );
            }
        }

        #endregion

        #region " GRADE "

        /// <summary> Asserts <see cref="GradeEntity"/> test conditions. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <param name="site">        The site. </param>
        /// <param name="provider">    The provider. </param>
        /// <param name="gradeNumber"> The grade number. </param>
        /// <param name="gradeLabel">  The grade label. </param>
        /// <param name="gradeCount">  Number of grades. </param>
        public static void AssertGradeEntity( ProviderBase provider, int gradeNumber, string gradeLabel, int gradeCount )
        {
            AssertTablesExist( provider, new string[] { GradeBuilder.TableName } );
            using var connection = provider.GetConnection();
            var SubstrateTanTcrGrade = new GradeEntity();
            _ = SubstrateTanTcrGrade.FetchUsingKey( connection, gradeNumber );
            string expectedLabel = gradeLabel;
            Assert.AreEqual( expectedLabel, SubstrateTanTcrGrade.Description, $"{nameof( isr.Dapper.Entities.GradeEntity )}.{nameof( isr.Dapper.Entities.GradeEntity.Label )}" );
            int expectedGradeNumber = gradeNumber;
            expectedLabel = gradeLabel;
            _ = SubstrateTanTcrGrade.FetchUsingUniqueIndex( connection, expectedLabel );
            Assert.AreEqual( expectedGradeNumber, SubstrateTanTcrGrade.Id, $"{nameof( isr.Dapper.Entities.GradeEntity )}.{nameof( isr.Dapper.Entities.GradeEntity.Id )}" );
            int expectedCount = gradeCount;
            int count = SubstrateTanTcrGrade.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, count, $"{nameof( isr.Dapper.Entities.GradeEntity )} entity correct number of {nameof( isr.Dapper.Entities.GradeEntity )}" );
        }

        /// <summary> Asserts <see cref="GradeEntity"/> test conditions. </summary>
        /// <remarks> David, 2020-04-23. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        internal static void AssertGradeEntity( ProviderBase provider )
        {
            AssertGradeEntity( provider, ( int ) Grade.Three, Grade.Three.Description(), 8 );
            AssertGradeEntity( provider, ( int ) Grade.Four, Grade.Four.Description(), 8 );
        }

        #endregion

        #region " FILE IMPORT STATE "

        /// <summary> Asserts the file import state entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">                The site. </param>
        /// <param name="provider">            The provider. </param>
        /// <param name="expectedRecordCount"> Number of expected records. </param>
        public static void AssertFileImportStateEntity( ProviderBase provider, int expectedRecordCount )
        {
            AssertTablesExist( provider, new string[] { FileImportStateBuilder.TableName } );
            int id = 10;
            var fileImportState = new FileImportStateEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = fileImportState.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( FileImportState )} with {nameof( FileImportStateNub.Id )} of {id} should not exist" );
            r = fileImportState.ValidateNewEntity( $"New {nameof( FileImportState )} with {nameof( FileImportStateNub.Id )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            actualFetched = fileImportState.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( FileImportState )} with {nameof( FileImportStateNub.Id )} of {id} should not exist" );
            r = fileImportState.ValidateNewEntity( $"New {nameof( FileImportState )} with {nameof( FileImportStateNub.Id )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            id = 1;
            expectedFetched = true;
            actualFetched = fileImportState.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( FileImportState )} with {nameof( FileImportStateNub.Id )} of {id} should exist" );
            r = fileImportState.ValidateStoredEntity( $"Fetched {nameof( FileImportState )} with {nameof( FileImportStateNub.Id )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            string label = FileImportState.Imported.ToString();
            fileImportState.Label = label;
            r = fileImportState.ValidateChangedEntity( $"Changed {nameof( FileImportState )} with {nameof( FileImportStateNub.Label )} to {label}" );
            Assert.IsTrue( r.Success, r.Details );
            actualFetched = fileImportState.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( FileImportState )} with {nameof( FileImportStateNub.Label )} with {label} should exist" );
            Assert.AreEqual( label, fileImportState.Label, $"{nameof( FileImportState )} {nameof( FileImportStateNub.Label )} should match" );
            r = fileImportState.ValidateStoredEntity( $"Fetched {nameof( FileImportState )} with {nameof( FileImportStateNub.Label )} of {label}" );
            Assert.IsTrue( r.Success, r.Details );
            int actualCount = fileImportState.FetchAllEntities( connection );
            Assert.AreEqual( expectedRecordCount, actualCount, $"{nameof( FileImportState )} record count should match" );
        }

        #endregion

        #region " IMPORT FILE "

        /// <summary> Asserts ImportFile entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="importFileFolder"> Pathname of the import file folder. </param>
        /// <param name="importFileName">   Filename of the import file. </param>
        public static void AssertImportFileEntity( ProviderBase provider, string importFileFolder, string importFileName )
        {
            AssertTablesExist( provider, new string[] { FileImportStateBuilder.TableName, ImportFileBuilder.TableName } );
            int id = 3;
            var importFile = new ImportFileEntity();
            using var connection = provider.GetConnection();
            int expectedCount = 0;
            _ = connection.DeleteAll<ImportFileNub>();
            bool expectedFetched = false;
            bool actualFetched = importFile.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( ImportFileEntity )} with {nameof( ImportFileEntity.Id )} of {id} should not exist" );
            (bool Success, string Details) r;
            r = importFile.ValidateNewEntity( $"New {nameof( ImportFileEntity )} with {nameof( ImportFileEntity.Id )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            importFile.Clear();
            id = 1;
            string fileName = Path.Combine( importFileFolder, importFileName );
            importFile = new ImportFileEntity() {
                Id = id,
                FileName = fileName,
                FileImportStateId = ( int ) FileImportState.Ready,
                Details = string.Empty,
                FileTimestamp = DateTime.UtcNow,
                ImportTimestamp = DateTime.UtcNow
            };
            bool actualInserted = importFile.Insert( connection );
            expectedCount += 1;
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} of {fileName} should have been inserted" );
            r = importFile.ValidateStoredEntity( $"Insert {nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} of {fileName}" );
            Assert.IsTrue( r.Success, r.Details );
            fileName = "Filename2";
            importFile.FileName = fileName;
            r = importFile.ValidateChangedEntity( $"Changed {nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} to {fileName}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualUpdate = importFile.Update( connection, UpdateModes.ForceUpdate | UpdateModes.Refetch );
            bool expectedUpdate = true;
            Assert.AreEqual( expectedUpdate, actualUpdate, $"{nameof( ImportFileEntity )} with changed {nameof( ImportFileEntity.FileName )} of {fileName} should be updated" );
            r = importFile.ValidateStoredEntity( $"Updated {nameof( ImportFileEntity )} with changed {nameof( ImportFileEntity.FileName )} of {fileName}" );
            Assert.IsTrue( r.Success, r.Details );
            actualUpdate = importFile.Update( connection, UpdateModes.UpdateOnly );
            expectedUpdate = false;
            Assert.AreEqual( expectedUpdate, actualUpdate, $"Unchanged {nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} of {fileName} should be clear--not updated" );
            r = importFile.ValidateStoredEntity( $"Unchanged {nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} of {fileName}" );
            Assert.IsTrue( r.Success, r.Details );
            Assert.AreEqual( 1, ImportFileEntity.CountEntities( connection, importFile.FileName ), $"Single {nameof( ImportFileEntity )} should be counted for {nameof( ImportFileEntity.FileName )} of {fileName}" );
            Assert.IsTrue( ImportFileEntity.IsExists( connection, importFile.FileName ), $"{nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} of {fileName} should {nameof( ImportFileEntity.IsExists )}" );
            fileName = importFile.FileName;
            int ImportFileAutoId = importFile.Id;
            actualFetched = importFile.Obtain( connection );
            expectedFetched = true;
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} of {fileName} should have been obtained" );
            Assert.AreEqual( fileName, importFile.FileName, $"Obtained {nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} should match" );
            Assert.AreEqual( ImportFileAutoId, importFile.Id, $"Obtained {nameof( ImportFileEntity )} with {nameof( ImportFileEntity.Id )} of {fileName} should match" );
            _ = connection.DeleteAll<ImportFileNub>();
            expectedCount = 10;
            for ( int i = 1, loopTo = expectedCount; i <= loopTo; i++ )
            {
                importFile.FileName = $"FileName{i}";
                id += 1;
                importFile.Id = id;
                actualInserted = importFile.Insert( connection );
                expectedInserted = true;
                Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} of {fileName} should have been inserted" );
            }

            int actualCount = importFile.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple {nameof( ImportFileEntity )}'s should have been inserted" );
            fileName = importFile.FileName;
            bool actualDelete = importFile.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"{nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} of {fileName} should be deleted" );
            IImportFile ImportFile0 = importFile.ImportFiles.ElementAtOrDefault( 0 );
            actualDelete = ImportFileEntity.Delete( connection, ImportFile0.Id );
            expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"{nameof( ImportFileEntity )} with {nameof( ImportFileEntity.FileName )} of {fileName} should be deleted" );
        }

        #endregion

        #region " LOT "

        /// <summary> Asserts <see cref="LotTraitTypeEntity"/> test conditions. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertLotTraitTypeEntity( ProviderBase provider )
        {
            AssertTablesExist( provider, new string[] { LotTraitTypeBuilder.TableName } );
            int id = ( int ) LotTraitType.None;
            var lotTraitType = new LotTraitTypeEntity();
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = lotTraitType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( LotTraitTypeEntity )} With {nameof( LotTraitTypeEntity.Id )} Of {id} should Not exist" );
            (bool Success, string Details) r;
            r = lotTraitType.ValidateNewEntity( $"New {nameof( LotTraitTypeEntity )} With {nameof( LotTraitTypeEntity.Id )} Of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            lotTraitType.Clear();
            id = ( int ) LotTraitType.CertifiedQuantity;
            expectedFetched = true;
            actualFetched = lotTraitType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( LotTraitTypeEntity )} With {nameof( LotTraitTypeEntity.Id )} Of {id}should exist" );
            r = lotTraitType.ValidateStoredEntity( $"Fetched {nameof( LotTraitTypeEntity )} With {nameof( LotTraitTypeEntity.Id )} Of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            lotTraitType.Label = LotTraitType.CertifiedQuantity.ToString();
            actualFetched = lotTraitType.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( LotTraitTypeEntity )} by {nameof( LotTraitTypeEntity.Label )} Of {LotTraitType.CertifiedQuantity} should exist" );
            r = lotTraitType.ValidateStoredEntity( $"Fetched {nameof( LotTraitTypeEntity )} by {nameof( LotTraitTypeEntity.Label )} Of {LotTraitType.CertifiedQuantity}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = Enum.GetValues( typeof( LotTraitType ) ).Length - 1;
            int actualCount = lotTraitType.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Expected {expectedCount} {nameof( LotTraitTypeEntity )}s" );
        }

        /// <summary> Assert adding lot entity. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partAutoId"> Identifier for the part entity. </param>
        /// <param name="lotNumber">  The lot number. </param>
        /// <returns> A LotEntity. </returns>
        public static LotEntity AssertAddingLotEntity( ProviderBase provider, int partAutoId, string lotNumber )
        {
            AssertTablesExist( provider, new string[] { LotBuilder.TableName } );
            var lot = new LotEntity();
            using ( var connection = provider.GetConnection() )
            {
                lot = new LotEntity() { LotNumber = lotNumber };
                bool actualObtained = lot.Obtain( connection );
                bool expectedObtained = true;
                Assert.AreEqual( expectedObtained, actualObtained, $"{nameof( LotEntity )}  with '{nameof( LotEntity.LotNumber )}' of {lot.LotNumber} should have been obtained" );
                (bool Success, string Details) r;
                r = lot.ValidateStoredEntity( $"Obtain {nameof( LotEntity )} with '{nameof( LotEntity.LotNumber )}' of {lotNumber}" );
                Assert.IsTrue( r.Success, r.Details );
                var partLot = new PartLotEntity() { LotAutoId = lot.AutoId, PartAutoId = partAutoId };
                _ = partLot.Obtain( connection );
                r = partLot.ValidateStoredEntity( $"Inserted {nameof( PartLotEntity )} with [{nameof( PartLotEntity.LotAutoId )},{nameof( PartLotEntity.PartAutoId )}] of {lot.AutoId},{partAutoId}]" );
                Assert.IsTrue( r.Success, r.Details );
                var part = partLot.FetchPartEntity( connection );
                r = part.ValidateStoredEntity( $"Fetched {nameof( PartEntity )} with {nameof( PartLotEntity.PartAutoId )} of {partLot.PartAutoId}" );
                Assert.IsTrue( r.Success, r.Details );
                Assert.AreEqual( part.PartNumber, PartLotEntity.FetchParts( connection, lot.AutoId ).First().PartNumber, $"Fetch {nameof( isr.Dapper.Entities.PartLotEntity )}.{nameof( isr.Dapper.Entities.PartLotEntity.PartEntity )} should match" );
                Assert.IsTrue( PartLotEntity.FetchLots( connection, partAutoId ).Contains( lot ), $"Fetched {nameof( isr.Dapper.Entities.PartLotEntity )}.{nameof( isr.Dapper.Entities.PartLotEntity.LotEntity )}'s should contain lot" );
            }

            return lot;
        }

        /// <summary> Assert adding lots. </summary>
        /// <remarks> David, 2020-06-29. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <param name="lotNumbers"> The lot numbers. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process assert adding lots in this collection.
        /// </returns>
        public static IEnumerable<LotEntity> AssertAddingLots( ProviderBase provider, string partNumber, string[] lotNumbers )
        {
            var part = AssertAddingPartEntity( provider, partNumber );
            var lots = new List<LotEntity>();
            foreach ( string lotNumber in lotNumbers )
                lots.Add( AssertAddingLotEntity( provider, part.AutoId, lotNumber ) );
            return lots;
        }

        /// <summary> Asserts lot entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <param name="lotNumbers"> The lot numbers. </param>
        public static void AssertLotEntity( ProviderBase provider, string partNumber, IEnumerable<string> lotNumbers )
        {
            AssertTablesExist( provider, new string[] { LotBuilder.TableName } );
            int id = 3;
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<LotNub>();
            _ = connection.DeleteAll<PartNub>();
            int partId = 1;
            var part = new PartEntity() { PartNumber = partNumber };
            _ = part.Insert( connection );
            partId = part.AutoId;
            bool expectedFetched = false;
            bool actualFetched = false;
            int expectedCount = 0;
            var lot = new LotEntity();
            actualFetched = lot.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( LotEntity )} with '{nameof( LotEntity.AutoId )}' of {id} should not exist" );
            (bool Success, string Details) r;
            r = lot.ValidateNewEntity( $"New {nameof( LotEntity )} with '{nameof( LotEntity.AutoId )}' of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            lot.Clear();
            Assert.IsTrue( LotBuilder.Instance.UsingUniqueLabel( connection ), $"{nameof( LotEntity )} should be using a unique {nameof( LotEntity.LotNumber )} " );
            string lotNumber = lotNumbers.ElementAtOrDefault( 0 );
            lot = new LotEntity() { LotNumber = lotNumber };
            bool actualInserted = lot.Insert( connection );
            expectedCount += 1;
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( LotEntity )}  with '{nameof( LotEntity.LotNumber )}' of {lot.LotNumber} should have been inserted" );
            r = lot.ValidateStoredEntity( $"Insert {nameof( LotEntity )} with '{nameof( LotEntity.LotNumber )}' of {lotNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            _ = lot.FetchUsingKey( connection );
            var expectedTimestamp = DateTime.UtcNow;
            var actualTimeStamp = lot.Timestamp;
            Asserts.Instance.AreEqual( expectedTimestamp, actualTimeStamp, TimeSpan.FromSeconds( 10d ), "Expected timestamp" );
            var partLot = new PartLotEntity() { LotAutoId = lot.AutoId, PartAutoId = part.AutoId };
            _ = partLot.Insert( connection );
            r = partLot.ValidateStoredEntity( $"Inserted {nameof( PartLotEntity )} with [{nameof( LotEntity.LotNumber )},{nameof( PartEntity.PartNumber )}] of {lotNumber},{partNumber}]" );
            Assert.IsTrue( r.Success, r.Details );
            _ = partLot.FetchPartEntity( connection );
            Assert.AreEqual( part.PartNumber, partLot.PartEntity.PartNumber, $"Fetched {nameof( PartLotEntity )} using '{nameof( PartLotEntity.PrimaryId )}' of {lot.AutoId} should match the {nameof( PartEntity )} '{nameof( PartEntity.PartNumber )} '" );
            r = partLot.PartEntity.ValidateStoredEntity( $"Fetched {nameof( PartLotEntity )} using '{nameof( PartLotEntity.PrimaryId )}' of {lot.AutoId} should match the {nameof( PartEntity )} '{nameof( PartEntity.PartNumber )} of [{lotNumber},{partNumber}]" );
            Assert.IsTrue( r.Success, r.Details );

            lotNumber = lotNumbers.ElementAtOrDefault( 1 );
            lot.LotNumber = lotNumber;


            r = lot.ValidateChangedEntity( $"Changed {nameof( LotEntity )} with '{nameof( LotEntity.LotNumber )}' of {lotNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualUpdate = lot.Update( connection, UpdateModes.Refetch );
            bool expectedUpdate = true;
            Assert.AreEqual( expectedUpdate, actualUpdate, $"{nameof( LotEntity )} with '{nameof( LotEntity.LotNumber )}' of {lot.LotNumber} should be updated" );
            r = lot.ValidateStoredEntity( $"Updated {nameof( LotEntity )} with '{nameof( LotEntity.LotNumber )}' of {lotNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            actualUpdate = lot.Update( connection, UpdateModes.Refetch );
            expectedUpdate = false;
            Assert.AreEqual( expectedUpdate, actualUpdate, $"Updated {nameof( LotEntity )} with unchanged '{nameof( LotEntity.LotNumber )}' of {lot.LotNumber} should be clean" );
            r = lot.ValidateStoredEntity( $"Updated {nameof( LotEntity )} with unchanged '{nameof( LotEntity.LotNumber )}' of {lot.LotNumber}" );
            Assert.IsTrue( r.Success, r.Details );

            // Insert must be done on a new lot; setting the lot number and inserting leaves the proxy 'dirty'. lot.LotNumber = lotNumbers(0)
            lot = new LotEntity() { LotNumber = lotNumbers.ElementAtOrDefault( 0 ), AutoId = partId };
            actualInserted = lot.Insert( connection );
            partLot = new PartLotEntity() { LotAutoId = lot.AutoId, PartAutoId = part.AutoId };
            _ = partLot.Insert( connection );
            r = partLot.ValidateStoredEntity( $"Inserted {nameof( PartLotEntity )} with [{nameof( LotEntity.LotNumber )},{nameof( PartEntity.PartNumber )}] of {lotNumber},{partNumber}]" );
            Assert.IsTrue( r.Success, r.Details );
            expectedCount += 1;
            Assert.AreEqual( expectedCount, PartLotEntity.CountLots( connection, part.AutoId ), $"{expectedCount} lots should be counted for {part.PartNumber}" );
            Assert.AreEqual( 1, PartLotEntity.CountParts( connection, lot.AutoId ), $"Single part should be counted for lot #{lot.LotNumber}" );
            Assert.IsTrue( PartLotEntity.IsExists( connection, part.AutoId, lot.AutoId ), $"{nameof( PartLotEntity )} with [{nameof( PartLotEntity.PrimaryId )},{nameof( PartLotEntity.SecondaryId )}] of [{part.AutoId}.{lot.AutoId} should exist" );
            lotNumber = lot.LotNumber;
            int AutoId = lot.AutoId;
            actualFetched = lot.Obtain( connection );
            expectedFetched = true;
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( LotEntity )} with {nameof( LotEntity.LotNumber )} {lot.LotNumber} should have been obtained" );
            Assert.AreEqual( lotNumber, lot.LotNumber, $"Obtained {nameof( LotEntity )} with {nameof( LotEntity.LotNumber )} of {lot.LotNumber} should match lot number" );
            Assert.AreEqual( AutoId, lot.AutoId, $"Obtained Part auto id {lot.AutoId} should match part auto id" );
            _ = connection.DeleteAll<LotNub>();
            _ = connection.DeleteAll<PartLotNub>();
            expectedCount = 10;
            for ( int i = 1, loopTo = expectedCount; i <= loopTo; i++ )
            {
                lotNumber = $"Lot{i}";
                partLot = new PartLotEntity();
                actualInserted = partLot.Obtain( connection, part.AutoId, lotNumber );
                Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( PartLotEntity )} for {lot.LotNumber} should have been inserted" );
            }

            int actualCount = lot.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Lot count should match after inserting {expectedCount} lots" );
            var lot0 = lot.Lots.First();
            Assert.IsTrue( lot0.IsClean(), $"Fetched entity {lot0.LotNumber} should be clean" );
            lotNumber = lot0.LotNumber;
            bool actualDelete = lot0.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Lot {lotNumber} should be deleted" );
            expectedCount -= 1;
            actualCount = lot.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Lot count should match after deletion of one lot" );
            lot0 = lot.Lots.First();
            Assert.IsTrue( lot0.IsClean(), $"Fetched entity {lot0.LotNumber} should be clean" );
            actualDelete = LotEntity.Delete( connection, lot0.AutoId );
            expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Lot {lot0.LotNumber} should be deleted" );
            expectedCount -= 1;
            part = new PartEntity();
            _ = part.FetchUsingKey( connection, partId );
            actualCount = PartLotEntity.CountLots( connection, part.AutoId );
            Assert.AreEqual( expectedCount, actualCount, $"multiple Lots should have been fetched for part {part.PartNumber}" );
        }

        #endregion

        #region " LOT TRAIT "

        /// <summary> Assert adding Lot Traits. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">      The site. </param>
        /// <param name="provider">  The provider. </param>
        /// <param name="lotAutoId"> Identifier for the <see cref="LotEntity"/> identity. </param>
        /// <returns> A LotEntity. </returns>
        public static LotEntity AssertAddingLotTraits( ProviderBase provider, int lotAutoId )
        {
            AssertTablesExist( provider, new string[] { LotBuilder.TableName, LotTraitBuilder.TableName, LotTraitTypeBuilder.TableName } );
            var Lot = new LotEntity() { AutoId = lotAutoId };
            using ( var connection = provider.GetConnection() )
            {
                int expectedTraitCount = 0;
                int certifiedQuantity = 100;
                var LotTraitEntity = new LotTraitEntity() { LotAutoId = lotAutoId, LotTraitTypeId = ( int ) LotTraitType.CertifiedQuantity, Amount = certifiedQuantity };
                _ = LotTraitEntity.Insert( connection );
                expectedTraitCount += 1;
                _ = Lot.FetchUsingKey( connection );
                (bool Success, string Details) r;
                r = Lot.ValidateStoredEntity( $"Fetch {nameof( LotEntity )} with '{nameof( LotEntity.AutoId )}' of {lotAutoId}" );
                Assert.IsTrue( r.Success, r.Details );
                Assert.AreEqual( expectedTraitCount, Lot.FetchTraits( connection ), $"{nameof( LotEntity )} {nameof( LotEntity.Traits )} count should match" );
                Assert.AreEqual( certifiedQuantity, Lot.Traits.LotTrait.CertifiedQuantity, $"{nameof( LotEntity )} {nameof( LotEntity.Traits )} {nameof( LotTrait.CertifiedQuantity )} should match" );
                int modifiedCertifiedQuantity = certifiedQuantity + 1;
                Lot.Traits.LotTrait.CertifiedQuantity = modifiedCertifiedQuantity;
                Assert.IsTrue( Lot.Upsert( connection ), $"{nameof( LotEntity )} {nameof( LotEntity.Traits )} {nameof( LotTrait.CertifiedQuantity )} should be updated" );
                Assert.AreEqual( expectedTraitCount, Lot.FetchTraits( connection ), $"{nameof( LotEntity )} {nameof( LotEntity.Traits )} count should match after second fetch" );
                Assert.AreEqual( modifiedCertifiedQuantity, Lot.Traits.LotTrait.CertifiedQuantity, $"{nameof( LotEntity )} {nameof( LotEntity.Traits )} {nameof( LotTrait.CertifiedQuantity )} should match after change" );
            }

            return Lot;
        }

        /// <summary> Assert adding Lot Traits. </summary>
        /// <remarks> David, 2020-06-17. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <param name="lotNumber">  The lot number. </param>
        public static void AssertAddingLotTraits( ProviderBase provider, string partNumber, string lotNumber )
        {
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<LotNub>();
            _ = connection.DeleteAll<LotTraitNub>();
            var part = AssertAddingPartEntity( provider, partNumber );
            var Lot = AssertAddingLotEntity( provider, part.AutoId, lotNumber );
            Lot = AssertAddingLotTraits( provider, Lot.AutoId );
        }

        #endregion

        #region " METER GUARD BAND LOOKUP ENTITY "

        /// <summary> Asserts <see cref="MeterGuardBandLookupEntity"/> test conditions. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertMeterGuardBandLookupEntity( ProviderBase provider )
        {
            AssertTablesExist( provider, new string[] { MeterGuardBandLookupBuilder.TableName } );
            int guardBandLookupId = 0;
            var MeterGuardBandLookup = new MeterGuardBandLookupEntity();
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = MeterGuardBandLookup.FetchUsingKey( connection, guardBandLookupId );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {guardBandLookupId} should not exist" );
            (bool Success, string Details) r;
            r = MeterGuardBandLookup.ValidateNewEntity( $"New {nameof( isr.Dapper.Entities.IMeterGuardBandLookup )} with {nameof( isr.Dapper.Entities.IMeterGuardBandLookup.Id )} of {guardBandLookupId}" );
            Assert.IsTrue( r.Success, r.Details );
            guardBandLookupId = 1;
            expectedFetched = true;
            actualFetched = MeterGuardBandLookup.FetchUsingKey( connection, guardBandLookupId );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {guardBandLookupId} should exist" );
            r = MeterGuardBandLookup.ValidateStoredEntity( $"Stored {nameof( isr.Dapper.Entities.IMeterGuardBandLookup )} with {nameof( isr.Dapper.Entities.IMeterGuardBandLookup.Id )} of {guardBandLookupId}" );
            Assert.IsTrue( r.Success, r.Details );
            MeterGuardBandLookup.Clear();
            // Dim actualCount As Integer = MeterGuardBandLookupEntity.MeterGuardBandLookups.Count
            _ = MeterGuardBandLookupEntity.TryFetchAll( connection );
            Assert.IsTrue( MeterGuardBandLookupEntity.MeterGuardBandLookups.Any(), $"{nameof( isr.Dapper.Entities.IMeterGuardBandLookup )} should have entities" );
            double value;
            bool actualExists;
            MeterGuardBandLookupNub fetchedNub;
            foreach ( MeterGuardBandLookupEntity nub in MeterGuardBandLookupEntity.MeterGuardBandLookups )
            {
                value = nub.MinimumInclusiveValue is null
                    ? nub.MaximumExclusiveValue.Value - 1d
                    : nub.MaximumExclusiveValue is null
                        ? nub.MinimumInclusiveValue.Value + 1d
                        : 0.5d * (nub.MaximumExclusiveValue.Value + nub.MinimumInclusiveValue.Value);

                actualExists = MeterGuardBandLookupEntity.IsExists( connection, nub.ToleranceCode, nub.MeterId, nub.NomTypeId, value );
                Assert.IsTrue( actualExists, $"Record for {nub.Id} should exist" );
                fetchedNub = MeterGuardBandLookupEntity.FetchNubs( connection, nub.ToleranceCode, nub.MeterId, nub.NomTypeId, value ).First();
                Assert.IsNotNull( fetchedNub, $"Fetched entity for {nub.Id} should not be null" );
                Assert.AreEqual( nub.Id, fetchedNub.Id, $"Fetched entity id values should match {nub.ToleranceCode} {fetchedNub.ToleranceCode}" );
                Assert.IsTrue( MeterGuardBandLookupNub.AreEqual( nub, fetchedNub ), $"Fetched entity with id {fetchedNub.Id} should equal the origin entity with id {nub.Id} " );
            }
        }

        #endregion

        #region " METER ENTITY "

        /// <summary> Asserts <see cref="MeterEntity"/> test conditions. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertMeterEntity( ProviderBase provider )
        {
            AssertTablesExist( provider, new string[] { MeterBuilder.TableName } );
            int meterModelId = ( int ) MeterModel.None;
            var meter = new MeterEntity();
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = meter.FetchUsingKey( connection, meterModelId );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {meterModelId} should not exist" );
            (bool Success, string Details) r;
            r = meter.ValidateNewEntity( $"New {nameof( isr.Dapper.Entities.MeterEntity )} with {nameof( isr.Dapper.Entities.MeterEntity.Id )} of {meterModelId}" );
            Assert.IsTrue( r.Success, r.Details );
            meter.Clear();
            int expectedCount = 0;
            for ( int meterNumber = 1; meterNumber <= 2; meterNumber++ )
            {
                foreach ( var currentMeterModelId in new int[] { ( int ) MeterModel.K2002, ( int ) MeterModel.K7510 } )
                {
                    meterModelId = currentMeterModelId;
                    expectedFetched = true;
                    actualFetched = meter.FetchUsingUniqueIndex( connection, meterModelId, meterNumber );
                    Assert.AreEqual( expectedFetched, actualFetched, $"key {meterModelId} should exist" );
                    r = meter.ValidateStoredEntity( $"Fetched {nameof( isr.Dapper.Entities.MeterEntity )} with [{nameof( isr.Dapper.Entities.MeterEntity.MeterNumber )},{nameof( isr.Dapper.Entities.MeterEntity.MeterModelId )}] of [{meterNumber},{meterModelId}]" );
                    Assert.IsTrue( r.Success, r.Details );
                    expectedCount += 1;
                }
            }

            for ( int meterNumber = 1; meterNumber <= 2; meterNumber++ )
            {
                foreach ( var currentmeterModelId in new int[] { ( int ) MeterModel.CLT10 } )
                {
                    meterModelId = currentmeterModelId;
                    expectedFetched = true;
                    actualFetched = meter.FetchUsingUniqueIndex( connection, meterModelId, meterNumber );
                    Assert.AreEqual( expectedFetched, actualFetched, $"key {meterModelId} should exist" );
                    r = meter.ValidateStoredEntity( $"Fetched {nameof( isr.Dapper.Entities.MeterEntity )} with [{nameof( isr.Dapper.Entities.MeterEntity.MeterNumber )},{nameof( isr.Dapper.Entities.MeterEntity.MeterModelId )}] of [{meterNumber},{meterModelId}]" );
                    Assert.IsTrue( r.Success, r.Details );
                    expectedCount += 1;
                }
            }

            int actualCount = meter.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Expected {expectedCount} meters" );
        }

        #endregion

        #region " METER MODEL ENTITY "

        /// <summary> Asserts <see cref="MeterModelEntity"/> test conditions. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertMeterModelEntity( ProviderBase provider )
        {
            AssertTablesExist( provider, new string[] { MeterModelBuilder.TableName } );
            int id = ( int ) MeterModel.None;
            var meterModel = new MeterModelEntity();
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = meterModel.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( isr.Dapper.Entities.MeterModelEntity )} with {nameof( isr.Dapper.Entities.MeterModelEntity.Id )} of {id} should not exist" );
            (bool Success, string Details) r;
            r = meterModel.ValidateNewEntity( $"New {nameof( isr.Dapper.Entities.MeterModelEntity )} with {nameof( isr.Dapper.Entities.MeterModelEntity.Id )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            meterModel.Clear();
            id = ( int ) MeterModel.K2002;
            expectedFetched = true;
            actualFetched = meterModel.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( isr.Dapper.Entities.MeterModelEntity )} with {nameof( isr.Dapper.Entities.MeterModelEntity.Id )} of {id} should exist" );
            r = meterModel.ValidateStoredEntity( $"Fetched  {nameof( isr.Dapper.Entities.MeterModelEntity )} with {nameof( isr.Dapper.Entities.MeterModelEntity.Id )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            meterModel.Label = MeterModel.K2002.ToString();
            actualFetched = meterModel.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( isr.Dapper.Entities.MeterModelEntity )} with {nameof( isr.Dapper.Entities.MeterModelEntity.Label )} of {isr.Dapper.Entities.MeterModel.K2002} should exist" );
            r = meterModel.ValidateStoredEntity( $"Fetched  {nameof( isr.Dapper.Entities.MeterModelEntity )} by {nameof( isr.Dapper.Entities.MeterModelEntity.Label )} equals {isr.Dapper.Entities.MeterModel.K2002}" );
            Assert.IsTrue( r.Success, r.Details );
            id = ( int ) MeterModel.K7510;
            expectedFetched = true;
            actualFetched = meterModel.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( isr.Dapper.Entities.MeterModelEntity )} with {nameof( isr.Dapper.Entities.MeterModelEntity.Id )} of {id} should exist" );
            r = meterModel.ValidateStoredEntity( $"Fetched  {nameof( isr.Dapper.Entities.MeterModelEntity )} with {nameof( isr.Dapper.Entities.MeterModelEntity.Id )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            meterModel.Label = MeterModel.K7510.ToString();
            actualFetched = meterModel.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( isr.Dapper.Entities.MeterModelEntity )} by name {nameof( isr.Dapper.Entities.MeterModelEntity.Label )} of {isr.Dapper.Entities.MeterModel.K7510} should exist" );
            r = meterModel.ValidateStoredEntity( $"Fetched {nameof( isr.Dapper.Entities.MeterModelEntity )} by name {nameof( isr.Dapper.Entities.MeterModelEntity.Label )} equals {isr.Dapper.Entities.MeterModel.K7510}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = Enum.GetValues( typeof( MeterModel ) ).Length - 1;
            int actualCount = meterModel.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Expected {expectedCount} meter models" );
        }

        #endregion

        #region " READING TYPE ENTITY "

        /// <summary> Asserts <see cref="ReadingTypeEntity"/> test conditions. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertReadingTypeEntity( ProviderBase provider )
        {
            int id = ( int ) Entities.ReadingType.None;
            var readingType = new ReadingTypeEntity();
            using var connection = provider.GetConnection();
            string tableName = ReadingTypeBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            bool expectedFetched = false;
            bool actualFetched = readingType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( ReadingTypeEntity )} With {nameof( ReadingTypeEntity.Id )} Of {id} should Not exist" );
            (bool Success, string Details) r;
            r = readingType.ValidateNewEntity( $"New {nameof( ReadingTypeEntity )} With {nameof( ReadingTypeEntity.Id )} Of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            readingType.Clear();
            id = ( int ) Entities.ReadingType.Amount;
            expectedFetched = true;
            actualFetched = readingType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( ReadingTypeEntity )} With {nameof( ReadingTypeEntity.Id )} Of {id}should exist" );
            r = readingType.ValidateStoredEntity( $"Fetched {nameof( ReadingTypeEntity )} With {nameof( ReadingTypeEntity.Id )} Of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            readingType.Label = Entities.ReadingType.Amount.ToString();
            actualFetched = readingType.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( ReadingTypeEntity )} by {nameof( ReadingTypeEntity.Label )} Of {Entities.ReadingType.Amount} should exist" );
            r = readingType.ValidateStoredEntity( $"Fetched {nameof( ReadingTypeEntity )} by {nameof( ReadingTypeEntity.Label )} Of {Entities.ReadingType.Amount}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = Enum.GetValues( typeof( ReadingType ) ).Length - 1;
            int actualCount = readingType.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Expected {expectedCount} {nameof( ReadingTypeEntity )}s" );
        }

        #endregion

        #region " NUT "

        /// <summary> Assert adding Nut entity. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">      The site. </param>
        /// <param name="provider">  The provider. </param>
        /// <param name="uutAutoId"> Identifier for the uut automatic. </param>
        /// <param name="elementId"> Identifier for the element. </param>
        /// <param name="nomTypeId"> Identifier for the <see cref="NomTypeEntity"/>. </param>
        /// <returns> A <see cref="Dapper.Entities.NutEntity"/>. </returns>
        public static NutEntity AssertAddingNutEntity( ProviderBase provider, int uutAutoId, int elementId, int nomTypeId )
        {
            AssertTablesExist( provider, new string[] { NutBuilder.TableName, UutNutBuilder.TableName } );
            NutEntity nut;
            UutNutEntity UutNut;
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                UutNut = new UutNutEntity();
                r = UutNut.TryObtain( connection, uutAutoId, elementId, nomTypeId );
                Assert.IsTrue( r.Success, $"{nameof( UutNutEntity )} with '{nameof( UutNutEntity.UutAutoId )}' of {uutAutoId} should have been inserted; {r.Details}" );

                r = UutNut.ValidateStoredEntity( $"Insert {nameof( UutNutEntity )} with '{nameof( UutNutEntity.UutAutoId )}' of {uutAutoId}" );
                Assert.IsTrue( r.Success, r.Details );

                nut = UutNut.FetchNutEntity( connection );
                r = nut.ValidateStoredEntity( $"Fetch {nameof( NutEntity )} with '{nameof( NutEntity.ElementAutoId )}' of {elementId}" );
                Assert.IsTrue( r.Success, r.Details );
            }

            return nut;
        }

        /// <summary> Assert adding Nut readings. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        /// <param name="element">  The element. </param>
        /// <param name="uut">      The uut. </param>
        /// <param name="nutId">    Identifier of the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <returns> A NutEntity. </returns>
        public static NutEntity AssertAddingNutReadings( ProviderBase provider, ElementEntity element, UutEntity uut, int nutId )
        {
            AssertTablesExist( provider, new string[] { NutBuilder.TableName, NutReadingBuilder.TableName, ReadingTypeBuilder.TableName } );
            (bool Success, string Details) r;
            var nut = new NutEntity() { AutoId = nutId };
            using ( var connection = provider.GetConnection() )
            {

                // Use 2002 meter to allow selection of equations.
                int meterNumber = 1;
                int meterModel = ( int ) MeterModel.K2002;
                if ( MeterEntity.Meters is not object ) _ = MeterEntity.TryFetchAll( connection );
                PlatformMeter platformMeter = new( MeterEntity.SelectMeterEntity( meterModel, meterNumber ) );
                PlatformMeter[] platformMeters = new PlatformMeter[] { platformMeter };
                int meterId = platformMeter.MeterId;

                _ = nut.FetchUsingKey( connection );
                r = nut.ValidateStoredEntity( $"Fetch {nameof( NutEntity )} with '{nameof( NutEntity.AutoId )}' of {nutId}" );
                Assert.IsTrue( r.Success, r.Details );
                var nomTrait = element.NomTraitsCollection[new MeterNomTypeSelector( platformMeter.MeterId, nut.NomTypeId )].NomTrait;
                double nominalValue = nomTrait.NominalValue.Value;
                var guardedSpecificationRange = nomTrait.GuardedSpecificationRange;
                var overflowRange = nomTrait.OverflowRange;
                int expectedNutReadingCount = 0;
                var nominalValueReadingType = ReadingType.Amount;
                double reading = nominalValue + 1d;
                var nutReadingEntity = new NutReadingEntity() { NutAutoId = nutId, ReadingTypeId = ( int ) nominalValueReadingType, Amount = reading };
                _ = nutReadingEntity.Insert( connection );
                r = nutReadingEntity.ValidateStoredEntity( $"Insert {nameof( NutReadingEntity )} with '{nameof( NutReadingEntity.ReadingTypeId )}' of {nominalValueReadingType}" );
                Assert.IsTrue( r.Success, r.Details );
                expectedNutReadingCount += 1;
                var statusReadingType = ReadingType.Status;
                int status = 101;
                nutReadingEntity = new NutReadingEntity() { NutAutoId = nutId, ReadingTypeId = ( int ) statusReadingType, Amount = status };
                _ = nutReadingEntity.Insert( connection );
                r = nutReadingEntity.ValidateStoredEntity( $"Insert {nameof( NutReadingEntity )} with '{nameof( NutReadingEntity.ReadingTypeId )}' of {statusReadingType}" );
                Assert.IsTrue( r.Success, r.Details );
                expectedNutReadingCount += 1;
                var binNumberReadingType = ReadingType.BinNumber;
                int binNumber = 11;
                nutReadingEntity = new NutReadingEntity() { NutAutoId = nutId, ReadingTypeId = ( int ) binNumberReadingType, Amount = binNumber };
                _ = nutReadingEntity.Insert( connection );
                r = nutReadingEntity.ValidateStoredEntity( $"Insert {nameof( NutReadingEntity )} with '{nameof( NutReadingEntity.ReadingTypeId )}' of {binNumberReadingType}" );
                Assert.IsTrue( r.Success, r.Details );
                expectedNutReadingCount += 1;
                _ = uut.FetchTraits( connection );
                nut.InitializeNutReadings( uut, element );
                _ = nut.FetchNutReadings( connection );
                Assert.AreEqual( expectedNutReadingCount, nut.NutReadings.Count, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} count should match" );
                Assert.IsTrue( nut.NutReadings.MetaReading.Amount.HasValue, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.Amount )} should have a value" );
                Assert.AreEqual( reading, nut.NutReadings.MetaReading.Amount.Value, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.Amount )} should match" );
                Assert.IsTrue( nut.NutReadings.MetaReading.Status.HasValue, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.Status )} should have a value" );
                Assert.AreEqual( status, ( object ) nut.NutReadings.MetaReading.Status, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.Status )} should match" );
                Assert.IsTrue( nut.NutReadings.MetaReading.BinNumber.HasValue, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.BinNumber )} should have a value" );
                Assert.AreEqual( binNumber, ( object ) nut.NutReadings.MetaReading.BinNumber, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.BinNumber )} should match" );
            }

            return nut;
        }

        /// <summary>   Reading getter. </summary>
        /// <remarks>   David, 2020-07-16. </remarks>
        /// <param name="nut">      The nut. </param>
        /// <param name="element">  The element. </param>
        /// <param name="meterId">  Identifier for the meter. </param>
        /// <param name="bin">      The bin. </param>
        /// <returns>   A (Reading As Double, Status As Integer) </returns>
        public static (double Reading, int Status) ReadingGetter( NutEntity nut, ElementEntity element, int meterId, ReadingBin bin )
        {
            var rand = new Random( DateTime.UtcNow.Millisecond );
            var readingAmount = default( double );
            int readingStatus = 0;
            RangeR guardedToleranceBand;
            RangeR overflowRange;
            int nomTypeId = nut.NomTypeId;
            var nomTrait = element.NomTraitsCollection[new MeterNomTypeSelector( meterId, nomTypeId )].NomTrait;
            _ = nomTrait.NominalValue.Value;
            guardedToleranceBand = nomTrait.GuardedSpecificationRange;
            overflowRange = nomTrait.OverflowRange;
            switch ( bin )
            {
                case ReadingBin.Good:
                    {
                        readingStatus = 0;
                        readingAmount = guardedToleranceBand.Min + rand.NextDouble() * guardedToleranceBand.Span;
                        break;
                    }

                case ReadingBin.High:
                    {
                        readingStatus = 1;
                        readingAmount = guardedToleranceBand.Max + rand.NextDouble() * guardedToleranceBand.Span;
                        break;
                    }

                case ReadingBin.Low:
                    {
                        readingStatus = 4;
                        readingAmount = guardedToleranceBand.Min - rand.NextDouble() * guardedToleranceBand.Span;
                        break;
                    }

                case ReadingBin.Overflow:
                    {
                        readingStatus = 8;
                        readingAmount = overflowRange.Min * (1d - 0.1d * rand.NextDouble());
                        break;
                    }
            }

            return (readingAmount, readingStatus);
        }

        /// <summary> Assert adding nut readings. </summary>
        /// <remarks> David, 2020-06-27. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        /// <param name="nut">      The nut. </param>
        /// <param name="element">  The element. </param>
        /// <param name="bin">      The bin. </param>
        /// <returns> A NutEntity. </returns>
        public static NutEntity AssertAddingNutReadings( ProviderBase provider, NutEntity nut, ElementEntity element, ReadingBin bin )
        {
            AssertTablesExist( provider, new string[] { NutBuilder.TableName, NutReadingBuilder.TableName } );
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                // Use 2002 meter to allow selection of equations.
                int meterNumber = 1;
                int meterModel = ( int ) MeterModel.K2002;
                if ( MeterEntity.Meters is not object ) _ = MeterEntity.TryFetchAll( connection );
                PlatformMeter platformMeter = new( MeterEntity.SelectMeterEntity( meterModel, meterNumber ) );
                PlatformMeter[] platformMeters = new PlatformMeter[] { platformMeter };
                int meterId = platformMeter.MeterId;

                int expectedNutReadingCount = 0;
                var readingType = ReadingType.Amount;
                double readingAmount;
                int readingStatus = 0;
                var (Amount, Status) = ReadingGetter( nut, element, meterId, bin );
                readingStatus = Status;
                readingAmount = Amount;
                readingType = ReadingType.Amount;
                var nutReadingEntity = new NutReadingEntity() { NutAutoId = nut.AutoId, ReadingTypeId = ( int ) readingType, Amount = readingAmount };
                _ = nutReadingEntity.Insert( connection );
                r = nutReadingEntity.ValidateStoredEntity( $"Insert {nameof( NutReadingEntity )} with '{nameof( NutReadingEntity.ReadingTypeId )}' of {readingType}" );
                Assert.IsTrue( r.Success, r.Details );

                if ( nut.NutReadings is null )
                    _ = nut.FetchNutReadings( connection );
                nut.NutReadings.Add( nutReadingEntity );
                expectedNutReadingCount += 1;
                readingType = ReadingType.Status;
                nutReadingEntity = new NutReadingEntity() { NutAutoId = nut.AutoId, ReadingTypeId = ( int ) readingType, Amount = readingStatus };
                _ = nutReadingEntity.Insert( connection );
                r = nutReadingEntity.ValidateStoredEntity( $"Insert {nameof( NutReadingEntity )} with '{nameof( NutReadingEntity.ReadingTypeId )}' of {readingType}" );
                Assert.IsTrue( r.Success, r.Details );

                nut.NutReadings.Add( nutReadingEntity );
                expectedNutReadingCount += 1;
                readingType = ReadingType.BinNumber;
                nutReadingEntity = new NutReadingEntity() { NutAutoId = nut.AutoId, ReadingTypeId = ( int ) readingType, Amount = ( double ) bin };
                _ = nutReadingEntity.Insert( connection );
                r = nutReadingEntity.ValidateStoredEntity( $"Insert {nameof( NutReadingEntity )} with '{nameof( NutReadingEntity.ReadingTypeId )}' of {readingType}" );
                Assert.IsTrue( r.Success, r.Details );

                nut.NutReadings.Add( nutReadingEntity );
                expectedNutReadingCount += 1;
                _ = nut.FetchNutReadings( connection );
                Assert.AreEqual( expectedNutReadingCount, nut.NutReadings.Count, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} count should match" );
                Assert.AreEqual( readingAmount, ( object ) nut.NutReadings.MetaReading.Amount, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.Amount )} should match" );
                Assert.IsTrue( nut.NutReadings.MetaReading.Status.HasValue, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.Status )} should have a value" );
                Assert.AreEqual( readingStatus, nut.NutReadings.MetaReading.Status.Value, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.Status )} should match" );
                Assert.IsTrue( nut.NutReadings.MetaReading.BinNumber.HasValue, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.BinNumber )} should have a value" );
                Assert.AreEqual( ( int ) bin, nut.NutReadings.MetaReading.BinNumber.Value, $"{nameof( NutEntity )} {nameof( NutEntity.NutReadings )} {nameof( MetaReading.BinNumber )} should match" );
            }

            return nut;
        }

        /// <summary> Assert storing Nut bin. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        /// <param name="nutId">    Identifier of the <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="binId">    Identifier for the bin. </param>
        /// <returns> An NutReadingBinEntity. </returns>
        public static NutReadingBinEntity AssertStoringNutBin( ProviderBase provider, int nutId, int binId )
        {
            AssertTablesExist( provider, new string[] { NutBuilder.TableName, NutReadingBinBuilder.TableName } );
            var nutReadingBin = new NutReadingBinEntity() { NutAutoId = nutId, ReadingBinId = binId };
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                r = nutReadingBin.TryObtain( connection );
                Assert.IsTrue( r.Success, r.Details );
            }

            return nutReadingBin;
        }

        /// <summary> Assert adding Nut Bins. </summary>
        /// <remarks> David, 2020-05-30. </remarks>
        /// <param name="site">        The site. </param>
        /// <param name="provider">    The provider. </param>
        /// <param name="nut">         The <see cref="Dapper.Entities.NutEntity"/>. </param>
        /// <param name="element">     The element. </param>
        /// <param name="expectedBin"> The expected bin. </param>
        /// <returns> A <see cref="Dapper.Entities.NutEntity"/>. </returns>
        public static NutReadingBinEntity AssertAddingNutBin( ProviderBase provider, NutEntity nut, ElementEntity element, ReadingBin expectedBin )
        {
            AssertTablesExist( provider, new string[] { NutBuilder.TableName, NutReadingBuilder.TableName } );
            int meterId = 1;
            var reading = nut.NutReadings.MetaReading.Amount;
            ReadingBin bin;
            var nomTrait = element.NomTraitsCollection[new MeterNomTypeSelector( meterId, nut.NomTypeId )].NomTrait;
            bin = nomTrait.DoBin( reading );
            Assert.AreEqual( expectedBin, bin, $"[{nameof( isr.Dapper.Entities.NutEntity )}] bin number of {element.Label} should match" );
            return AssertStoringNutBin( provider, nut.AutoId, ( int ) bin );
        }

        /// <summary>   Assert Nut entity. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="productNumber">    The product number. </param>
        /// <param name="partNumber">       The part number. </param>
        /// <param name="sessionLabels">    The session labels. </param>
        public static void AssertNutEntity( ProviderBase provider, string productNumber, string partNumber, IEnumerable<string> sessionLabels )
        {
            NutReadingBinEntity nutReadingBin;
            NutEntity nut;
            ElementEntity element;
            UutEntity uut;

            (bool Success, string Details) r;
            using var connection = provider.GetConnection();

            // Use 2002 meter to allow selection of equations.
            int meterNumber = 1;
            int meterModel = ( int ) MeterModel.K2002;
            if ( MeterEntity.Meters is not object ) _ = MeterEntity.TryFetchAll( connection );
            PlatformMeter platformMeter = new( MeterEntity.SelectMeterEntity( meterModel, meterNumber ) );
            PlatformMeter[] platformMeters = new PlatformMeter[] { platformMeter };
            int meterId = platformMeter.MeterId;

            _ = connection.DeleteAll<PartNub>();
            _ = connection.DeleteAll<LotNub>();
            _ = connection.DeleteAll<UutNub>();
            _ = connection.DeleteAll<SessionUutNub>();
            _ = connection.DeleteAll<UutProductSortNub>();
            _ = connection.DeleteAll<NutNub>();
            _ = connection.DeleteAll<ElementNub>();
            int expectedElementCount = 4;

            var product = AssertAddingProductEntity( provider, productNumber );
            var productElement = new ProductElementEntity() { ProductAutoId = product.AutoId };
            productElement.AddElements( connection, productNumber );
            int actualCount = product.FetchElements( connection );
            Assert.AreEqual( expectedElementCount, actualCount, $"inserted {nameof( ProductEntity )} {productNumber} {nameof( ProductEntity.Elements )} count should match" );

            var part = AssertAddingPartEntity( provider, partNumber );
            var productPart = new ProductPartEntity() { ProductAutoId = product.AutoId, PartAutoId = part.AutoId };
            _ = productPart.Obtain( connection );
            r = productPart.ValidateStoredEntity( $"Obtained {nameof( isr.Dapper.Entities.ProductPartEntity )} With {nameof( isr.Dapper.Entities.ProductPartEntity.ProductAutoId )} of {product.AutoId} and {nameof( isr.Dapper.Entities.ProductPartEntity.PartAutoId )} of {part.AutoId}" );
            Assert.IsTrue( r.Success, r.Details );
            _ = AssertAddingElementTraits( provider, product, part, platformMeter );

            int lastUutNumber = 0;
            string sessionLabel = sessionLabels.ElementAtOrDefault( 0 );
            uut = AssertAddingUutEntity( provider, meterId, sessionLabel, lastUutNumber );
            lastUutNumber = uut.UutNumber;
            foreach ( ElementInfo elementInfo in ProductElementEntity.SelectElementInfo( productNumber ) )
            {
                element = part.Elements.SelectElementByElementLabel( elementInfo.Label );
                expectedElementCount += 1;
                nut = AssertAddingNutEntity( provider, uut.AutoId, element.AutoId, elementInfo.NomTypeId );
                nut.InitializeNutReadings( uut, element );
                _ = nut.FetchNutReadings( connection );
                nutReadingBin = AssertStoringNutBin( provider, nut.AutoId, ( int ) ReadingBin.Good );
                nut = AssertAddingNutReadings( provider, element, uut, nut.AutoId );
            }

            sessionLabel = sessionLabels.ElementAtOrDefault( 1 );
            lastUutNumber = 0;
            uut = AssertAddingUutEntity( provider, meterId, sessionLabel, lastUutNumber );
            lastUutNumber = uut.UutNumber;
            foreach ( ElementInfo elementInfo in ProductElementEntity.SelectElementInfo( productNumber ) )
            {
                element = part.Elements.SelectElementByElementLabel( elementInfo.Label );
                expectedElementCount += 1;
                nut = AssertAddingNutEntity( provider, uut.AutoId, element.AutoId, elementInfo.NomTypeId );
                nut.InitializeNutReadings( uut, element );
                _ = nut.FetchNutReadings( connection );
                nutReadingBin = AssertStoringNutBin( provider, nut.AutoId, ( int ) ReadingBin.Good );
                nut = AssertAddingNutReadings( provider, element, uut, nut.AutoId );
            }

            uut = AssertAddingUutEntity( provider, meterId, sessionLabel, lastUutNumber );
            lastUutNumber = uut.UutNumber;
            foreach ( ElementInfo elementInfo in ProductElementEntity.SelectElementInfo( productNumber ) )
            {
                element = part.Elements.SelectElementByElementLabel( elementInfo.Label );
                expectedElementCount += 1;
                nut = AssertAddingNutEntity( provider, uut.AutoId, element.AutoId, elementInfo.NomTypeId );
                nut.InitializeNutReadings( uut, element );
                _ = nut.FetchNutReadings( connection );
                nutReadingBin = AssertStoringNutBin( provider, nut.AutoId, ( int ) ReadingBin.Good );
                nut = AssertAddingNutReadings( provider, element, uut, nut.AutoId );
            }
        }

        #endregion

        #region " OHM METER SETUP TESTS "

        /// <summary> Asserts Ohm Meter Setting lookup and setup. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertOhmMeterSetupEntity( ProviderBase provider )
        {
            AssertTablesExist( provider, new string[] { OhmMeterSettingLookupBuilder.TableName, OhmMeterSetupBuilder.TableName, OhmMeterSettingBuilder.TableName } );
            int ohmMeterSettingLookupId = 0;
            var ohmMeterSettingsLookupEntity = new OhmMeterSettingLookupEntity();
            var ohmMeterSettingEntity = new OhmMeterSettingEntity();
            var ohmMeterSetupEntity = new OhmMeterSetupEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = ohmMeterSettingsLookupEntity.FetchUsingKey( connection, ohmMeterSettingLookupId );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {ohmMeterSettingLookupId} should not exist" );
            r = ohmMeterSettingsLookupEntity.ValidateNewEntity( $"New {nameof( isr.Dapper.Entities.IOhmMeterSettingLookup )} with {nameof( isr.Dapper.Entities.IOhmMeterSettingLookup.OhmMeterSettingId )} equals {ohmMeterSettingLookupId}" );
            Assert.IsTrue( r.Success, r.Details );

            ohmMeterSettingsLookupEntity.Clear();
            var meterEntities = MeterEntity.FetchAllEntities( connection, false );
            int expectedCount = 6;
            Assert.AreEqual( expectedCount, meterEntities.Count(), $"There should be enough {nameof( isr.Dapper.Entities.MeterEntity )}s" );

            string expectedToleranceCode = "A";
            double resistance = 399d;
            bool actualExists;
            int actualCount;

            // clear the table
            if ( provider.TableExists( connection, SessionOhmMeterSetupBuilder.TableName ) )
                _ = connection.DeleteAll<SessionOhmMeterSetupNub>();
            _ = connection.DeleteAll<OhmMeterSetupNub>();
            int id = 1;
            ohmMeterSettingEntity = new OhmMeterSettingEntity() { Id = id };
            actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex( connection );
            Assert.IsTrue( actualExists, $"{nameof( OhmMeterSettingEntity )} should exist for auto id = {id}" );

            actualCount = OhmMeterSettingEntity.CountEntities( connection );
            expectedCount = 6; // at least 6
            Assert.IsTrue( actualCount >= expectedCount, $"{nameof( OhmMeterSettingEntity )} should have at least {expectedCount} records" );

            _ = connection.DeleteAll<OhmMeterSetupNub>();
            _ = OhmMeterSettingLookupEntity.TryFetchAll( connection );
            foreach ( MeterEntity MeterEntity in meterEntities )
            {
                if ( MeterEntity.MeterModelId == ( int ) MeterModel.CLT10 )
                    continue;
                ohmMeterSettingsLookupEntity = OhmMeterSettingLookupEntity.SelectOhmMeterSettingLookupEntity( expectedToleranceCode, MeterEntity.Id, resistance );
                ohmMeterSettingEntity = new OhmMeterSettingEntity() { Id = ohmMeterSettingsLookupEntity.OhmMeterSettingId };
                actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex( connection );
                Assert.IsTrue( actualExists, $"{nameof( ohmMeterSettingEntity )} with '{nameof( ohmMeterSettingEntity.Id )}' equals {ohmMeterSettingEntity.Id} should exist" );
                ohmMeterSetupEntity = new OhmMeterSetupEntity();
                ohmMeterSetupEntity.Copy( ohmMeterSettingEntity );
                actualCount = OhmMeterSetupEntity.CountEntities( connection );
                expectedCount = actualCount;
                expectedCount += 1;
                actualExists = ohmMeterSetupEntity.Insert( connection );
                Assert.IsTrue( actualExists, $"A new {nameof( OhmMeterSetupEntity )} should be stored for meter ['{nameof( OhmMeterSettingLookupEntity.MeterId )}'] of [{MeterEntity.Id}]" );
                actualCount = OhmMeterSetupEntity.CountEntities( connection );
                Assert.IsTrue( actualCount >= expectedCount, $"Resistance test condition should have at least {expectedCount} records" );
            }
        }

        #endregion

        #region " PART "

        /// <summary> Assert adding part entity. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <returns> A PartEntity. </returns>
        public static PartEntity AssertAddingPartEntity( ProviderBase provider, string partNumber )
        {
            AssertTablesExist( provider, new string[] { PartBuilder.TableName } );
            var part = new PartEntity();
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                part.PartNumber = partNumber;
                _ = part.Obtain( connection );
                Assert.AreEqual( partNumber, part.PartNumber, $"{nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} of {partNumber} must equals the saved number {part.PartNumber}" );
                r = part.ValidateStoredEntity( $"Obtained {nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} of {partNumber}" );
                Assert.IsTrue( r.Success, r.Details );
                _ = part.FetchNamings( connection );
            }

            return part;
        }

        /// <summary> Asserts part entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">        The site. </param>
        /// <param name="provider">    The provider. </param>
        /// <param name="partNumbers"> The part numbers. </param>
        public static void AssertPartEntity( ProviderBase provider, IEnumerable<string> partNumbers )
        {
            AssertTablesExist( provider, new string[] { PartBuilder.TableName } );
            int id = 3;
            var part = new PartEntity();
            var part1 = new PartEntity();
            Assert.IsTrue( part.IsNew(), $"An instantiated {nameof( PartEntity )} must be new" );
            Assert.IsFalse( part.IsDirty(), $"An instantiated {nameof( PartEntity )} must not be 'Dirty'" );
            Assert.IsFalse( part.IsClean(), $"An instantiated {nameof( PartEntity )} must not be 'Clean (stored)'" );
            Assert.AreEqual( Entity.PersistenceState.New, part.PersistenceState(), $"{nameof( PartEntity )}.{nameof( EntityBase<IKeyLabelTime, PartNub>.PersistenceState ) } should be equals" );

            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            int expectedCount = 0;
            _ = connection.DeleteAll<PartNub>();
            bool expectedFetched = false;
            bool actualFetched = part.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( PartEntity )} with {nameof( PartEntity.AutoId )} of {id} should not exist" );

            r = part.ValidateNewEntity( $"New {nameof( PartEntity )} with {nameof( PartEntity.AutoId )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            bool expectedUniqueLabel = true;
            bool actualUniqueLabel = PartBuilder.Instance.UsingUniqueLabel( connection );
            Assert.AreEqual( expectedUniqueLabel, actualUniqueLabel, $"{nameof( isr.Dapper.Entities.PartEntity )} '{nameof( isr.Dapper.Entities.PartBuilder.UsingUniqueLabel )}' should match" );

            string partNumber = partNumbers.ElementAtOrDefault( 0 );
            part.PartNumber = partNumber;

            // test equality
            part1.PartNumber = partNumber;
            Assert.IsTrue( part1.Equals( part ), $"Two {nameof( PartEntity )}'s set with the same {nameof( PartEntity.PartNumber )} must equal" );
            Assert.AreEqual( part1.GetHashCode(), part.GetHashCode(), $"Hash codes of two {nameof( PartEntity )}'s set with the same {nameof( PartEntity.PartNumber )} must equal" );
            Assert.AreEqual( part.PersistenceState(), part1.PersistenceState(),
                $"The {nameof( PartEntity )}.{nameof( EntityBase<IKeyLabelTime, PartNub>.PersistenceState ) } of two new part should be equal" );


            bool actualInserted = part.Insert( connection );
            Assert.AreEqual( partNumber, part.PartNumber, $"{nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} of {partNumber} must equals the saved number {part.PartNumber}" );
            r = part.ValidateStoredEntity( $"Inserted {nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} of {partNumber}" );
            Assert.AreNotEqual( part.PersistenceState(), part1.PersistenceState(),
                $"The {nameof( PartEntity )}.{nameof( EntityBase<IKeyLabelTime, PartNub>.PersistenceState ) } of a stored parts should be different for the new part" );



            Assert.IsTrue( r.Success, r.Details );
            _ = part.FetchUsingKey( connection );
            //var expectedTimestamp = DateTime.Now;
            //var actualTimeStamp = part.Timestamp.ToLocalTime();
            var expectedTimestamp = DateTime.UtcNow;
            var actualTimeStamp = part.Timestamp;
            Asserts.Instance.AreEqual( expectedTimestamp, actualTimeStamp, TimeSpan.FromSeconds( 10d ), "Expected timestamp" );

            expectedCount += 1;
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"Part {part.PartNumber} should have been inserted" );

            partNumber = partNumbers.ElementAtOrDefault( 1 );
            part.PartNumber = partNumber;
            r = part.ValidateChangedEntity( $"Changed {nameof( PartEntity )} with {nameof( PartEntity.PartNumber )} to {partNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualUpdated = part.Update( connection, UpdateModes.Refetch );
            Assert.IsTrue( actualUpdated, $"Part {part.PartNumber} should be updated" );
            actualUpdated = part.Update( connection, UpdateModes.Refetch );
            Assert.IsFalse( actualUpdated, $"Part {part.PartNumber} should not be updated" );
            Assert.AreEqual( 1, PartEntity.CountEntities( connection, part.PartNumber ), $"Single part should be counted for {part.PartNumber}" );
            Assert.IsTrue( PartEntity.IsExists( connection, part.PartNumber ), $"Part {part.PartNumber} should {nameof( PartEntity.IsExists )}" );

            // set part proxy dirty
            actualUpdated = part.Update( connection, UpdateModes.Refetch | UpdateModes.ForceUpdate );
            Assert.IsTrue( actualUpdated, $"Part {part.PartNumber} should be updated after setting proxy dirty" );
            actualUpdated = part.Update( connection );
            Assert.IsTrue( actualUpdated, $"Part {part.PartNumber} should be clean after update" );

            partNumber = part.PartNumber;
            int AutoId = part.AutoId;
            actualFetched = part.Obtain( connection );
            expectedFetched = true;
            Assert.AreEqual( expectedFetched, actualFetched, $"Part {part.PartNumber} should have been obtained" );
            Assert.AreEqual( partNumber, part.PartNumber, $"Obtained Part number {part.PartNumber} should match" );
            Assert.AreEqual( AutoId, part.AutoId, $"Obtained Part auto id {part.AutoId} should match" );

            _ = connection.DeleteAll<PartNub>();
            expectedCount = partNumbers.Count();
            for ( int i = 1, loopTo = expectedCount; i <= loopTo; i++ )
            {
                part = new PartEntity() { PartNumber = partNumbers.ElementAtOrDefault( i - 1 ) };
                actualInserted = part.Insert( connection );
                expectedInserted = true;
                Assert.AreEqual( expectedInserted, actualInserted, $"Part {part.PartNumber} should have been inserted" );
            }

            int actualCount = part.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple parts should have been inserted" );
            var part0 = part.Parts.First();
            Assert.IsTrue( part0.IsClean(), $"fetched part should be clean" );

            partNumber = part0.PartNumber;
            bool actualDelete = part0.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Part {partNumber} should be deleted" );

            expectedCount -= 1;
            actualCount = part.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Count should match after deleting one part" );
            part0 = part.Parts.First();
            Assert.IsTrue( part0.IsClean(), $"fetched part should be clean" );

            actualDelete = PartEntity.Delete( connection, part0.AutoId );
            expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Part {part0.PartNumber} should be deleted" );
            expectedCount -= 1;
            actualCount = part.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Count should match after deleting another part" );
        }

        #endregion

        #region " PART SPECIFICATION TYPE "

        /// <summary> Assert part specification type entity. </summary>
        /// <remarks> David, 2020-05-25. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertPartSpecificationTypeEntity( ProviderBase provider )
        {
            AssertTablesExist( provider, new string[] { PartNamingTypeBuilder.TableName } );
            int id = 100;
            var entity = new PartNamingTypeEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            r = entity.ValidateNewEntity( $"{nameof( PartNamingTypeEntity )} with '{nameof( PartNamingTypeEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualFetched = entity.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( PartNamingTypeEntity )} with '{nameof( PartNamingTypeEntity.Id )}' of '{id}' should not exist" );
            r = entity.ValidateNewEntity( $"{nameof( PartNamingTypeEntity )} with '{nameof( PartNamingTypeEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            id = 1;
            actualFetched = entity.FetchUsingKey( connection, id );
            Assert.IsTrue( actualFetched, $"{typeof( isr.Dapper.Entities.PartNamingTypeEntity ).Name}.{nameof( isr.Dapper.Entities.PartNamingTypeEntity.Id )}={id} should exist" );
            r = entity.ValidateStoredEntity( $"Existing {nameof( PartNamingTypeEntity )} with '{nameof( PartNamingTypeEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            string label = PartNamingType.TrackingCode.ToString();
            entity.Label = label;
            r = entity.ValidateChangedEntity( $"Changed {nameof( PartNamingTypeEntity )} with '{nameof( PartNamingTypeEntity.Label )}' to '{label}'" );
            Assert.IsTrue( r.Success, r.Details );
            actualFetched = entity.FetchUsingUniqueIndex( connection );
            Assert.IsTrue( actualFetched, $"{typeof( isr.Dapper.Entities.PartNamingTypeEntity ).Name}.{nameof( isr.Dapper.Entities.PartNamingTypeEntity.Label )}={label} should exist" );
            Assert.AreEqual( label, entity.Label, $"{typeof( isr.Dapper.Entities.PartNamingTypeEntity ).Name}.{nameof( isr.Dapper.Entities.PartNamingTypeEntity.Label )} should match" );
            r = entity.ValidateStoredEntity( $"Fetched {nameof( PartNamingTypeEntity )} with '{nameof( PartNamingTypeEntity.Label )}' of '{label}'" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 2;
            int actualCount = entity.FetchAllEntities( connection );
            Assert.IsTrue( actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} {typeof( PartNamingTypeEntity ).Name} records" );
        }

        #endregion

        #region " PART SPECIFICATION "

        /// <summary> Assert adding part specifications. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partAutoId"> Identifier for the part entity. </param>
        /// <returns> A PartEntity. </returns>
        public static PartEntity AssertAddingPartSpecifications( ProviderBase provider, int partAutoId )
        {
            AssertTablesExist( provider, new string[] { PartBuilder.TableName, PartNamingBuilder.TableName, PartNamingTypeBuilder.TableName } );
            var part = new PartEntity() { AutoId = partAutoId };
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                int expectedElementCount = 0;
                string familyCode = "PFC";
                var partSpecificationEntity = new PartNamingEntity() { PartAutoId = partAutoId, PartNamingTypeId = ( int ) PartNamingType.FamilyCode, Label = familyCode };
                _ = partSpecificationEntity.Insert( connection );
                expectedElementCount += 1;
                string productNumber = "D1206";
                partSpecificationEntity = new PartNamingEntity() { PartAutoId = partAutoId, PartNamingTypeId = ( int ) PartNamingType.ProductNumber, Label = productNumber };
                _ = partSpecificationEntity.Insert( connection );
                expectedElementCount += 1;
                _ = part.FetchUsingKey( connection );
                r = part.ValidateStoredEntity( $"Fetch {nameof( PartEntity )} with '{nameof( PartEntity.AutoId )}' of {partAutoId}" );
                Assert.IsTrue( r.Success, r.Details );
                Assert.AreEqual( expectedElementCount, part.FetchNamings( connection ), $"{nameof( PartEntity )} {nameof( PartEntity.Namings )} count should match" );
                Assert.AreEqual( familyCode, part.Namings.PartNaming.FamilyCode, $"{nameof( PartEntity )} {nameof( PartEntity.Namings )} {nameof( PartNaming.FamilyCode )} should match" );
                Assert.AreEqual( productNumber, part.Namings.PartNaming.ProductNumber, $"{nameof( PartEntity )} {nameof( PartEntity.Namings )} {nameof( PartNaming.ProductNumber )} should match" );
                string modifiedFamilyCode = "SPC";
                part.Namings.PartNaming.FamilyCode = modifiedFamilyCode;
                Assert.IsTrue( part.Namings.Upsert( connection ), $"{nameof( PartEntity )} {nameof( PartEntity.Namings )} {nameof( PartNaming.FamilyCode )} should be updated" );
                Assert.AreEqual( expectedElementCount, part.FetchNamings( connection ), $"{nameof( PartEntity )} {nameof( PartEntity.Namings )} count should match after second fetch" );
                Assert.AreEqual( modifiedFamilyCode, part.Namings.PartNaming.FamilyCode, $"{nameof( PartEntity )} {nameof( PartEntity.Namings )} {nameof( PartNaming.FamilyCode )} should match" );
                string toleranceCode = "F";
                part.Namings.PartNaming.ToleranceCode = toleranceCode;
                Assert.IsTrue( part.Namings.Upsert( connection ), $"{nameof( PartEntity )} {nameof( PartEntity.Namings )} {nameof( PartNaming.ToleranceCode )} should be updated" );
                expectedElementCount += 1;
                Assert.AreEqual( expectedElementCount, part.FetchNamings( connection ), $"{nameof( PartEntity )} {nameof( PartEntity.Namings )} count should match after third fetch" );
                Assert.AreEqual( toleranceCode, part.Namings.PartNaming.ToleranceCode, $"{nameof( PartEntity )} {nameof( PartEntity.Namings )} {nameof( PartNaming.ToleranceCode )} should match" );
            }

            return part;
        }

        /// <summary> Assert part specifications. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertPartSpecifications( ProviderBase provider )
        {
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<PartNub>();
            _ = connection.DeleteAll<PartNamingNub>();
            string partNumber = "PFC-D1206LF-02-1001-3301-FB";
            var part = AssertAddingPartEntity( provider, partNumber );
            part = AssertAddingPartSpecifications( provider, part.AutoId );
            partNumber = "part2";
            part = AssertAddingPartEntity( provider, partNumber );
            part = AssertAddingPartSpecifications( provider, part.AutoId );
        }

        #endregion

        #region " POLARITY "

        /// <summary> Asserts <see cref="PolarityEntity"/> test conditions. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertPolarityEntity( ProviderBase provider )
        {
            int id = 3;
            var polarity = new PolarityEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = PolarityBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            bool expectedFetched = false;
            bool actualFetched = polarity.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( PolarityEntity )} With '{nameof( PolarityEntity.Id )}' of '{id}' should not exist" );
            r = polarity.ValidateNewEntity( $"{nameof( PolarityEntity )} with '{nameof( PolarityEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            polarity.Clear();
            id = 0;
            expectedFetched = true;
            actualFetched = polarity.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( PolarityEntity )} with '{nameof( PolarityEntity.Id )}' of '{id}' should exist" );
            r = polarity.ValidateStoredEntity( $"Fetched {nameof( PolarityEntity )} with '{nameof( PolarityEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            polarity.Label = Polarity.Negative.ToString();
            actualFetched = polarity.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( PolarityEntity )} with '{nameof( PolarityEntity.Label )}' of {Polarity.Negative} should exist" );
            r = polarity.ValidateStoredEntity( $"Fetched {nameof( PolarityEntity )} with '{nameof( PolarityEntity.Label )}' of '{polarity.Label}'" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 2;
            int actualCount = polarity.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Expected {expectedCount} Polarities" );
        }

        #endregion

        #region " PLATFORM "

        /// <summary> Asserts Platform entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertPlatformEntity( ProviderBase provider )
        {
            string expectedPlatformLabel = Auditor.ComputerName;
            string expectedTimeZoneName = Auditor.TimeZoneStandardName;
            int id = 4;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = PlatformBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<PlatformNub>();
            var platform = new PlatformEntity();

            // connection.DeleteAll(Of PlatformNub)()
            bool expectedFetched = false;
            bool actualFetched = platform.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( PlatformEntity )} with {nameof( PlatformEntity.AutoId )} of {id} should not exist" );
            r = platform.ValidateNewEntity( $"{nameof( PlatformEntity )} with {nameof( PlatformEntity.AutoId )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            platform.Clear();
            bool expectedInserted = true;
            bool actualInserted = false;
            platform = new PlatformEntity() { Label = expectedPlatformLabel, TimezoneId = expectedTimeZoneName };
            actualInserted = platform.Insert( connection );
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} of {expectedPlatformLabel} should have been inserted" );
            Assert.AreEqual( expectedTimeZoneName, platform.TimezoneId, $"{nameof( PlatformEntity )} with {nameof( PlatformEntity.TimezoneId )} should match" );
            r = platform.ValidateStoredEntity( $"Inserted {nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} of {expectedPlatformLabel}" );
            Assert.IsTrue( r.Success, r.Details );
            _ = platform.FetchUsingKey( connection );
            var expectedLocalTimestamp = DateTime.UtcNow;
            var actualLocalTimeStamp = platform.Timestamp;
            Asserts.Instance.AreEqual( expectedLocalTimestamp, actualLocalTimeStamp, TimeSpan.FromSeconds( 10d ), "Expected local time timestamp" );
            DateTimeOffset expectedLocaleTimeStamp = actualLocalTimeStamp;
            var actualLocaleTimeStamp = platform.LocaleTimestamp();
            Asserts.Instance.AreEqual( expectedLocaleTimeStamp, actualLocaleTimeStamp, TimeSpan.FromSeconds( 0d ), "Expected locale time timestamp" );
            string newLabel = "new label";
            platform.Label = newLabel;
            r = platform.ValidateChangedEntity( $"Inserted {nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} changed to {newLabel}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualUpdate = platform.Update( connection, UpdateModes.Refetch );
            bool expectedUpdate = true;
            Assert.AreEqual( expectedUpdate, actualUpdate, $"Changed {nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} changed to {newLabel} should be updated" );
            r = platform.ValidateStoredEntity( $"Updated {nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} changed to {newLabel}" );
            Assert.IsTrue( r.Success, r.Details );
            expectedUpdate = false;
            actualUpdate = platform.Update( connection, UpdateModes.Refetch );
            Assert.AreEqual( expectedUpdate, actualUpdate, $"Unchanged {nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} of {newLabel} should not be updated" );
            r = platform.ValidateStoredEntity( $"Stored {nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} of {newLabel}" );
            Assert.IsTrue( r.Success, r.Details );
            platform.Label = expectedPlatformLabel;
            actualUpdate = platform.Update( connection, UpdateModes.Refetch );
            Assert.AreEqual( expectedUpdate, actualUpdate, $"Changed {nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} changed to {expectedPlatformLabel} should be updated" );
            r = platform.ValidateStoredEntity( $"Updated {nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} changed to {expectedPlatformLabel}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 1;
            for ( int i = 1; i <= 2; i++ )
            {
                newLabel = $"Platform{i}";
                platform = new PlatformEntity() { Label = newLabel, TimezoneId = expectedTimeZoneName };
                actualInserted = platform.Insert( connection );
                expectedInserted = true;
                Assert.AreEqual( expectedInserted, actualInserted, $"New {nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} of {newLabel} should have been inserted" );
                expectedCount += 1;
            }

            int actualCount = platform.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple {nameof( PlatformEntity )}'s should have been inserted" );
            string platformLabel = platform.Label;
            bool actualDelete = platform.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"{nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} of {platformLabel} should be deleted" );
            platform.Clear();

            // get active Platform
            platform.Label = expectedPlatformLabel;
            _ = platform.Obtain( connection );
            Assert.AreEqual( expectedPlatformLabel, platform.Label, $"{nameof( PlatformEntity )} with {nameof( PlatformEntity.Label )} of {expectedPlatformLabel}  should be the active Platform" );
        }

        #endregion

        #region " PRODUCT BINNING "

        /// <summary> Assert adding product binning entities. </summary>
        /// <remarks> David, 2020-05-23. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when operation failed to execute. </exception>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        /// <param name="product">  The product entity. </param>
        /// <param name="elements"> The elements. </param>
        /// <param name="sorts">    The sorts. </param>
        /// <returns> A ProductUniqueBinningEntityCollection. </returns>
        public static ProductUniqueBinningEntityCollection AssertAddingProductBinningEntities(
                                                                                              ProviderBase provider,
                                                                                              ProductEntity product,
                                                                                              ProductUniqueElementEntityCollection elements,
                                                                                              ProductUniqueProductSortEntityCollection sorts )
        {
            AssertTableExist( provider, ProductPartBuilder.TableName );
            using var connection = provider.GetConnection();
            _ = product.FetchProductBinning( connection );
            product.ProductBinnings.Populate( connection, elements, sorts );
            int expectedCount = product.ProductBinnings.Count;
            product.ProductBinnings.Clear();
            _ = product.FetchProductBinning( connection );
            product.ProductBinnings.Populate( connection, elements, sorts );
            return expectedCount != product.ProductBinnings.Count
                ? throw new InvalidOperationException( $"{nameof( ProductUniqueBinningEntityCollection )} count {product.ProductBinnings.Count} should be {expectedCount} after upsert" )
                : product.ProductBinnings;
        }

        #endregion

        #region " PRODUCT SORTS "

        /// <summary> Assert adding product sort entities. </summary>
        /// <remarks> David, 2020-05-22. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        /// <param name="product">  The product entity. </param>
        /// <param name="type">     The type. </param>
        /// <returns> A ProductUniqueProductSortEntityCollection. </returns>
        public static ProductUniqueProductSortEntityCollection AssertAddingProductSortEntities( ProviderBase provider, ProductEntity product, Type type )
        {
            AssertTableExist( provider, ProductPartBuilder.TableName );
            using var connection = provider.GetConnection();

            // insert sort labels for this product
            _ = ProductSortBuilder.Instance.InsertSortLabels( connection, product.AutoId, type );
            int expectedCount = Enum.GetValues( type ).Length;

            // fetch the product sort and their traits if any.
            int actualCount = product.FetchProductSorts( connection );
            Assert.AreEqual( expectedCount, actualCount, $"[{nameof( ProductEntity )}].[{nameof( ProductEntity.ProductSorts )}] count should match" );
            actualCount = connection.CountEntities( ProductSortBuilder.TableName );
            Assert.AreEqual( expectedCount, product.ProductSorts.Count, $"[{nameof( isr.Dapper.Entities.ProductSortBuilder )}].[{isr.Dapper.Entities.ProductSortBuilder.TableName}] record count should match" );
            expectedCount = product.ProductSorts.Count;
            _ = product.ProductSorts.Upsert( connection );
            actualCount = connection.CountEntities( ProductSortBuilder.TableName );
            Assert.AreEqual( expectedCount, actualCount, $"[{nameof( isr.Dapper.Entities.ProductUniqueProductSortEntityCollection )}] count should match after second Upsert" );
            expectedCount = product.ProductSorts.Count;
            _ = ProductSortBuilder.Instance.InsertSortLabels( connection, product.AutoId, type );
            actualCount = connection.CountEntities( ProductSortBuilder.TableName );
            Assert.AreEqual( expectedCount, actualCount, $"{nameof( isr.Dapper.Entities.ProductUniqueProductSortEntityCollection )} count should match after second insert of sort labels" );
            return product.ProductSorts;
        }

        /// <summary> Assert adding product sort entities. </summary>
        /// <remarks> David, 2020-05-23. </remarks>
        /// <param name="site">                      The site. </param>
        /// <param name="provider">                  The provider. </param>
        /// <param name="product">                   The product entity. </param>
        /// <param name="overflowFailureCountLimit"> The overflow failure count limit. </param>
        /// <returns> A ProductUniqueProductSortEntityCollection. </returns>
        public static ProductUniqueProductSortEntityCollection AssertAddingProductSortEntities( ProviderBase provider, ProductEntity product, int overflowFailureCountLimit )
        {
            ProductUniqueProductSortEntityCollection sorts;
            ProductSortEntity productSort;
            string sortLabel = ProductSort.Good.Description();
            AssertTableExist( provider, ProductPartBuilder.TableName );
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                sorts = AssertAddingProductSortEntities( provider, product, typeof( ProductSort ) );
                _ = product.AssignProductSortBuckets();

                // validate the bucket traits
                sortLabel = ProductSort.Good.Description();
                int digitalOutput = 0;
                int ContinuousFailureCountLimit = 0;
                BucketBin bucketBin;
                // validate
                foreach ( var currentSortLabel in typeof( ProductSort ).Descriptions() )
                {
                    sortLabel = currentSortLabel;
                    productSort = sorts.ProductSort( sortLabel );
                    r = productSort.ValidateStoredEntity( $"{nameof( isr.Dapper.Entities.ProductSortEntity )} with {nameof( isr.Dapper.Entities.ProductSortEntity.Label )} of {sortLabel}" );
                    Assert.IsTrue( r.Success, r.Details );
                    Assert.IsNotNull( productSort.ProductSortTraits, $"[{nameof( productSort.ProductSortTraits )}] for {sortLabel} should not be null" );
                    bucketBin = ( BucketBin ) productSort.ProductSortTraits.ProductSortTrait.BucketBin.GetValueOrDefault( ( int ) BucketBin.Unknown );
                    digitalOutput = BucketBinEntity.DigitalOutputMapper[bucketBin];
                    Assert.IsTrue( productSort.ProductSortTraits.ProductSortTrait.DigitalOutput.HasValue, $"{nameof( ProductSortTrait.DigitalOutput )} for {sortLabel} should have a value" );
                    Assert.AreEqual( digitalOutput, productSort.ProductSortTraits.ProductSortTrait.DigitalOutput.Value, $"{nameof( ProductSortTrait.DigitalOutput )} for {sortLabel} should match" );
                    ContinuousFailureCountLimit = BucketBinEntity.ContinuousFailureCountLimitMapper[bucketBin];
                    Assert.IsTrue( productSort.ProductSortTraits.ProductSortTrait.ContinuousFailureCountLimit.HasValue, $"{nameof( ProductSortTrait.ContinuousFailureCountLimit )} for {sortLabel} should have a value" );
                    Assert.AreEqual( ContinuousFailureCountLimit, productSort.ProductSortTraits.ProductSortTrait.ContinuousFailureCountLimit.Value, $"{nameof( ProductSortTrait.ContinuousFailureCountLimit )} for {sortLabel} should match" );
                }
            }

            return sorts;
        }

        #endregion

        #region " PRODUCT PART "

        /// <summary> Assert adding product part entity. </summary>
        /// <remarks> David, 2020-05-22. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="productAutoId"> Identifier for the product automatic. </param>
        /// <param name="partAutoId">    Identifier for the part entity. </param>
        /// <returns> A PartEntity. </returns>
        public static ProductPartEntity AssertAddingProductPartEntity( ProviderBase provider, int productAutoId, int partAutoId )
        {
            AssertTableExist( provider, ProductPartBuilder.TableName );
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            var productPart = new ProductPartEntity() { PartAutoId = partAutoId, ProductAutoId = productAutoId };
            _ = productPart.Insert( connection );
            r = productPart.ValidateStoredEntity( $"Inserted {nameof( ProductPartEntity )} With [{nameof( ProductPartEntity.PartAutoId )},{nameof( ProductPartEntity.ProductAutoId )}] Of [{partAutoId},{productAutoId}]" );
            Assert.IsTrue( r.Success, r.Details );
            return productPart;
        }

        /// <summary> Assert adding product part entity. </summary>
        /// <remarks> David, 2020-05-22. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="productAutoId"> Identifier for the product automatic. </param>
        /// <param name="partNumber">    The part number. </param>
        /// <returns> A PartEntity. </returns>
        public static ProductPartEntity AssertAddingProductPartEntity( ProviderBase provider, int productAutoId, string partNumber )
        {
            AssertTableExist( provider, PartBuilder.TableName );
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            var part = new PartEntity() { PartNumber = partNumber };
            bool actualInserted = part.Insert( connection );
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( PartEntity )}  With '{nameof( PartEntity.PartNumber )}' of {part.PartNumber} should have been inserted" );
            r = part.ValidateStoredEntity( $"Insert {nameof( PartEntity )} with '{nameof( PartEntity.PartNumber )}' of {partNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            var productPart = new ProductPartEntity() { PartAutoId = part.AutoId, ProductAutoId = productAutoId };
            _ = productPart.Insert( connection );
            r = productPart.ValidateStoredEntity( $"Inserted {nameof( ProductPartEntity )} with [{nameof( ProductPartEntity.PartAutoId )},{nameof( ProductPartEntity.ProductAutoId )}] of {part.AutoId},{productAutoId}]" );
            Assert.IsTrue( r.Success, r.Details );
            return productPart;
        }

        #endregion

        #region " PRODUCT PRODUCT "

        /// <summary> Assert adding product entity. </summary>
        /// <remarks> David, 2020-05-22. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="productNumber"> The product number. </param>
        /// <returns> A ProductEntity. </returns>
        public static ProductEntity AssertAddingProductEntity( ProviderBase provider, string productNumber )
        {
            AssertTableExist( provider, ProductBuilder.TableName );
            var product = new ProductEntity();
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                product = new ProductEntity() { ProductNumber = productNumber };
                _ = product.Obtain( connection );
                r = product.ValidateStoredEntity( $"Insert {nameof( ProductEntity )} with '{nameof( ProductEntity.ProductNumber )}' of {productNumber}" );
                Assert.IsTrue( r.Success, r.Details );
                _ = product.FetchTraits( connection );
                _ = product.FetchProductSorts( connection );
                _ = product.FetchTexts( connection );
                string scanList = ProductEntity.SelectScanList( productNumber );
                if ( !string.IsNullOrEmpty( scanList ) )
                {
                    product.Texts.ProductText.ScanList = scanList;
                    _ = product.Texts.Upsert( connection );
                }

                int expectedCount = 3;
                double minimumCp = 1.31d;
                double minimumCpk = 1.32d;
                double minimumCpm = 1.33d;
                product.Traits.ProductTrait.MinimumCp = minimumCp;
                product.Traits.ProductTrait.MinimumCpk = minimumCpk;
                product.Traits.ProductTrait.MinimumCpm = minimumCpm;
                Assert.AreEqual( expectedCount, product.Traits.Count, $"[{nameof( isr.Dapper.Entities.ProductEntity )}].[{nameof( isr.Dapper.Entities.ProductEntity.Traits )}].[Count] should match" );
                _ = product.Traits.Upsert( connection );
                Assert.AreEqual( expectedCount, product.Traits.Count, $"[{nameof( isr.Dapper.Entities.ProductEntity )}].[{nameof( isr.Dapper.Entities.ProductEntity.Traits )}].[Count] should match after upsert" );
                _ = product.FetchTraits( connection );
                Assert.AreEqual( expectedCount, product.Traits.Count, $"[{nameof( isr.Dapper.Entities.ProductEntity )}].[{nameof( isr.Dapper.Entities.ProductEntity.Traits )}].[Count] should match after fetch" );
                Assert.AreEqual( minimumCp, product.Traits.ProductTrait.MinimumCp, $"[{nameof( isr.Dapper.Entities.ProductEntity )}].[{nameof( isr.Dapper.Entities.ProductEntity.Traits )}].[{nameof( isr.Dapper.Entities.ProductTrait.MinimumCp )}] should match after fetch" );
                Assert.AreEqual( minimumCpk, product.Traits.ProductTrait.MinimumCpk, $"[{nameof( isr.Dapper.Entities.ProductEntity )}].[{nameof( isr.Dapper.Entities.ProductEntity.Traits )}].[{nameof( isr.Dapper.Entities.ProductTrait.MinimumCpk )}] should match after fetch" );
                Assert.AreEqual( minimumCpm, product.Traits.ProductTrait.MinimumCpm, $"[{nameof( isr.Dapper.Entities.ProductEntity )}].[{nameof( isr.Dapper.Entities.ProductEntity.Traits )}].[{nameof( isr.Dapper.Entities.ProductTrait.MinimumCpm )}] should match after fetch" );
            }

            return product;
        }

        #endregion

        #region " PRODUCT UUT NUT "

        /// <summary>   Assert adding uut nut entities. </summary>
        /// <remarks>   David, 2020-06-24. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <param name="uut">              The uut. </param>
        /// <returns>   An UutEntity. </returns>
        public static UutEntity AssertAddingUutNutEntities( ProviderBase provider, PlatformMeter platformMeter, UutEntity uut )
        {
            NutEntity nut;
            using ( var connection = provider.GetConnection() )
            {
                int expectedNutCount = 0;
                foreach ( var element in platformMeter.Elements() )
                {
                    foreach ( NomTypeEntity nomTypeEntity in element.NomTypes )
                    {
                        nut = AssertAddingNutEntity( provider, uut.AutoId, element.AutoId, nomTypeEntity.Id );
                        expectedNutCount += 1;
                    }
                }
                Assert.AreEqual( expectedNutCount, uut.FetchNuts( connection ), $"{nameof( isr.Dapper.Entities.UutEntity.Nuts )} count should match" );
                uut.InitializeNutReadingEntitiesCollection( platformMeter );
                _ = uut.FetchNutReadings( connection );
                foreach ( var currentNut in uut.Nuts )
                {
                    nut = currentNut;
                    Assert.IsNotNull( nut.NutReadings, $"{nameof( isr.Dapper.Entities.NutEntity.NutReadings )} should not be null" );
                }

                uut.UutState = (uut.Nuts?.Any()).GetValueOrDefault( false ) ? UutState.Ready : UutState.Undefined;
            }

            return uut;
        }

        /// <summary>   Assert adding uut nut readings. </summary>
        /// <remarks>   David, 2020-07-09. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <param name="uut">              The uut. </param>
        /// <param name="bins">             The bins. </param>
        /// <returns>   An UutEntity. </returns>
        public static UutEntity AssertAddingUutNutReadings( ProviderBase provider, PlatformMeter platformMeter, UutEntity uut, IEnumerable<ReadingBin> bins )
        {
            NutEntity nut;
            ElementEntity element;
            using ( var connection = provider.GetConnection() )
            {
                Assert.AreEqual( bins.Count(), uut.FetchNuts( connection ), $"{nameof( isr.Dapper.Entities.UutEntity.Nuts )} count should match" );
                uut.InitializeNutReadingEntitiesCollection( platformMeter );
                _ = uut.FetchNutReadings( connection );
                var elementLables = ElementLabels;
                for ( int elementIndex = 0, loopTo = bins.Count() - 1; elementIndex <= loopTo; elementIndex++ )
                {
                    element = platformMeter.Elements().SelectElementByElementLabel( elementLables.ElementAtOrDefault( elementIndex ) );
                    nut = uut.Nuts.Nut( platformMeter.PrimaryElementId, ( int ) platformMeter.PrimaryNominalType );
                    nut = AssertAddingNutReadings( provider, nut, element, bins.ElementAtOrDefault( elementIndex ) );
                }
            }

            return uut;
        }

        /// <summary>   Assert adding uut nut bins. </summary>
        /// <remarks>   David, 2020-07-09. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <param name="uut">              The uut. </param>
        /// <param name="expectedNutCount"> Number of expected nuts. </param>
        /// <param name="bins">             The bins. </param>
        /// <returns>   An UutEntity. </returns>
        public static UutEntity AssertAddingUutNutBins( ProviderBase provider, PlatformMeter platformMeter, UutEntity uut, int expectedNutCount, ReadingBin[] bins )
        {
            NutEntity nut;
            ElementEntity element;
            using ( var connection = provider.GetConnection() )
            {
                int binIndex = 0;
                Assert.AreEqual( expectedNutCount, uut.FetchNuts( connection ), $"{nameof( isr.Dapper.Entities.UutEntity.Nuts )} count should match" );
                uut.InitializeNutReadingEntitiesCollection( platformMeter );
                _ = uut.FetchNutReadings( connection );
                var elementLables = ElementLabels;
                string productNumber = "D1206LF";
                foreach ( ElementInfo elementInfo in ProductElementEntity.SelectElementInfo( productNumber ) )
                {
                    element = platformMeter.Elements().SelectElementByElementLabel( elementInfo.Label );
                    nut = uut.Nuts.Nut( element.AutoId, elementInfo.NomTypeId );
                    int binId = AssertAddingNutBin( provider, nut, element, bins[binIndex] ).ReadingBinId;
                    // uut.Nuts.UpaddReadingBin(nut, ReadingBinEntity.ReadingBins(bins(binIndex)))
                    uut.Nuts.UpaddReadingBin( nut, ReadingBinEntity.EntityLookupDictionary()[binId] );
                    binIndex += 1;
                }
            }

            uut.UutState = UutState.Measured;
            return uut;
        }

        #endregion

        #region " PRODUCT ELEMENTS "

        /// <summary>   Assert adding product element entities and element traits. </summary>
        /// <remarks>   David, 2020-05-25. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="product">          The product entity. </param>
        /// <param name="part">             The part. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   An ElementEntity. </returns>
        public static int AssertAddingProductElementEntities( ProviderBase provider, ProductEntity product, PartEntity part, PlatformMeter platformMeter )
        {
            using var connection = provider.GetConnection();
            string productNumber = product.ProductNumber;
            var productElement = new ProductElementEntity() { ProductAutoId = product.AutoId };
            productElement.AddElements( connection, productNumber );
            _ = product.FetchElements( connection );
            _ = part.FetchElements( connection, new PlatformMeter[] { platformMeter } );
            foreach ( var element in part.Elements )
                AssertAddingElementNomTraits( provider, part, element, platformMeter );
            return product.Elements.Count;
        }

        /// <summary>   Assert adding element traits. </summary>
        /// <remarks>   David, 2020-06-30. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="product">          The product entity. </param>
        /// <param name="part">             The part. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   An Integer. </returns>
        public static int AssertAddingElementTraits( ProviderBase provider, ProductEntity product, PartEntity part, PlatformMeter platformMeter )
        {
            using var connection = provider.GetConnection();
            _ = product.FetchElements( connection );
            _ = part.FetchElements( connection, new PlatformMeter[] { platformMeter } );
            foreach ( var element in part.Elements )
                AssertAddingElementNomTraits( provider, part, element, platformMeter );
            return part.Elements.Count;
        }

        #endregion

        #region " PRODUCT UUT SORT "

        /// <summary> Asset insert uut sort. </summary>
        /// <remarks> David, 2020-05-28. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="uut">           The <see cref="UutEntity"/>. </param>
        /// <param name="productSortId"> Identifier for the product sort. </param>
        /// <returns> An UutProductSortEntity. </returns>
        public static UutProductSortEntity AssetInsertUutSort( ProviderBase provider, UutEntity uut, int productSortId )
        {
            UutProductSortEntity uutSort;
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                uutSort = new UutProductSortEntity() { UutAutoId = uut.AutoId, ProductSortAutoId = productSortId };
                _ = uutSort.Insert( connection );
                r = uutSort.ValidateStoredEntity( $"{nameof( isr.Dapper.Entities.UutProductSortEntity )}" );
                Assert.IsTrue( r.Success, r.Details );
            }

            uut.UutState = UutState.Complete;
            return uutSort;
        }

        #endregion

        #region " PRODUCT  "

        /// <summary> Assert product entity. </summary>
        /// <remarks> David, 2020-05-22. </remarks>
        /// <param name="site">               The site. </param>
        /// <param name="provider">           The provider. </param>
        /// <param name="productTestMonitor"> The product test monitor. </param>
        public static void AssertProductEntity( ProviderBase provider, SessionMonitor productTestMonitor )
        {
            ProductUniqueElementEntityCollection elements;
            ProductUniqueProductSortEntityCollection sorts;
            UutEntity uut;
            ProductUniqueBinningEntityCollection binnings;
            ProductSortEntity productSort;
            UutProductSortEntity uutSort;
            var uutMonitor = productTestMonitor.UutMonitor;
            using var connection = provider.GetConnection();
            int lotAutoId = productTestMonitor.Lot.AutoId;
            elements = productTestMonitor.Elements;
            sorts = productTestMonitor.Sorts;
            binnings = productTestMonitor.Binnings;
            int expectedLastUutNumber = 0;
            uutMonitor.PrepareNextUut( connection, lotAutoId, UutType.Chip );
            Assert.AreEqual( expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match" );
            uut = uutMonitor.ActiveUUTEntity;
            Assert.AreEqual( UutState.Ready, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedLastUutNumber = uut.UutNumber;

            // ALL GOOD
            var bins = new ReadingBin[] { ReadingBin.Good, ReadingBin.Good, ReadingBin.Good, ReadingBin.Good };
            uut = AssertAddingUutNutReadings( provider, uutMonitor.PlatformMeter, uut, bins );
            uut = AssertAddingUutNutBins( provider, uutMonitor.PlatformMeter, uut, uut.Nuts.Count, bins );
            var expectedSort = ProductSort.Good;
            string expectedSortLabel = expectedSort.Description();
            var expectedSortEntity = sorts.ProductSort( expectedSortLabel );
            productSort = uutMonitor.SortUut( binnings, sorts );
            Assert.AreEqual( expectedSortLabel, productSort.Label, $"{nameof( ProductSortEntity )} {nameof( ProductSortEntity.Label )} should match" );
            Assert.AreEqual( UutState.Present, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            uutSort = AssetInsertUutSort( provider, uut, productSort.AutoId );
            Assert.AreEqual( UutState.Complete, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            int expectedContinuousFailureAlertCount = 0;
            bool expectedFailureCountAlert = false;
            int expectedFailureCount = 0;
            Assert.AreEqual( expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.LastSortEntity )}].[{nameof( isr.Dapper.Entities.ProductSortEntity.AutoId )}] should match" );
            Assert.AreEqual( expectedFailureCount, uutMonitor.ContinuousFailureCount,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureCount )}] should match" );
            Assert.AreEqual( expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}][{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureAlert )}] should match" );
            ;

            // R1 OVERFLOW 
            uutMonitor.PrepareNextUut( connection, lotAutoId, UutType.Chip );
            Assert.AreEqual( expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match" );
            uut = uutMonitor.ActiveUUTEntity;
            Assert.AreEqual( UutState.Ready, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedLastUutNumber = uut.UutNumber;
            bins = new ReadingBin[] { ReadingBin.Overflow, ReadingBin.Good, ReadingBin.Good, ReadingBin.Good };
            uut = AssertAddingUutNutReadings( provider, uutMonitor.PlatformMeter, uut, bins );
            uut = AssertAddingUutNutBins( provider, uutMonitor.PlatformMeter, uut, uut.Nuts.Count, bins );
            expectedSort = ProductSort.ReadingOverflow;
            expectedSortLabel = expectedSort.Description();
            expectedSortEntity = sorts.ProductSort( expectedSortLabel );

            // override Failure count limit for testing
            expectedSortEntity.ProductSortTraits.ProductSortTrait.ContinuousFailureCountLimit = 2;
            productSort = uutMonitor.SortUut( binnings, sorts );
            Assert.AreEqual( expectedSortLabel, productSort.Label, $"{nameof( ProductSortEntity )} {nameof( ProductSortEntity.Label )} should match" );
            Assert.AreEqual( UutState.Absent, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            uutSort = AssetInsertUutSort( provider, uut, productSort.AutoId );
            Assert.AreEqual( UutState.Complete, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedFailureCountAlert = false;
            expectedFailureCount += 1;
            Assert.AreEqual( expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.LastSortEntity )}].[{nameof( isr.Dapper.Entities.ProductSortEntity.AutoId )}] should match" );
            Assert.AreEqual( expectedFailureCount, uutMonitor.ContinuousFailureCount,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureCount )}] should match" );
            Assert.AreEqual( expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}][{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureAlert )}] should match" );
            ;

            // R2 OVERFLOW 
            uutMonitor.PrepareNextUut( connection, lotAutoId, UutType.Chip );
            Assert.AreEqual( expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match" );
            uut = uutMonitor.ActiveUUTEntity;
            Assert.AreEqual( UutState.Ready, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedLastUutNumber = uut.UutNumber;
            bins = new ReadingBin[] { ReadingBin.Good, ReadingBin.Overflow, ReadingBin.Good, ReadingBin.Good };
            uut = AssertAddingUutNutReadings( provider, uutMonitor.PlatformMeter, uut, bins );
            uut = AssertAddingUutNutBins( provider, uutMonitor.PlatformMeter, uut, uut.Nuts.Count, bins );
            expectedSort = ProductSort.ReadingOverflow;
            expectedSortLabel = expectedSort.Description();
            expectedSortEntity = sorts.ProductSort( expectedSortLabel );
            productSort = uutMonitor.SortUut( binnings, sorts );
            Assert.AreEqual( UutState.Absent, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            Assert.AreEqual( expectedSortLabel, productSort.Label, $"{nameof( ProductSortEntity )} {nameof( ProductSortEntity.Label )} should match" );
            uutSort = AssetInsertUutSort( provider, uut, productSort.AutoId );
            Assert.AreEqual( UutState.Complete, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedFailureCountAlert = true;
            expectedFailureCount += 1;
            expectedContinuousFailureAlertCount += 1;
            Assert.AreEqual( expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.LastSortEntity )}].[{nameof( isr.Dapper.Entities.ProductSortEntity.AutoId )}] should match" );
            Assert.AreEqual( expectedFailureCount, uutMonitor.ContinuousFailureCount,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureCount )}] should match" );
            Assert.AreEqual( expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}][{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureAlert )}] should match" );
            ;

            // R2 LOW
            uutMonitor.PrepareNextUut( connection, lotAutoId, UutType.Chip );
            Assert.AreEqual( expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match" );
            uut = uutMonitor.ActiveUUTEntity;
            Assert.AreEqual( UutState.Ready, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedLastUutNumber = uut.UutNumber;
            bins = new ReadingBin[] { ReadingBin.Good, ReadingBin.Low, ReadingBin.Good, ReadingBin.Good };
            uut = AssertAddingUutNutReadings( provider, uutMonitor.PlatformMeter, uut, bins );
            uut = AssertAddingUutNutBins( provider, uutMonitor.PlatformMeter, uut, uut.Nuts.Count, bins );
            expectedSort = ProductSort.ResistorGuardFailed;
            expectedSortLabel = expectedSort.Description();
            expectedSortEntity = sorts.ProductSort( expectedSortLabel );
            productSort = uutMonitor.SortUut( binnings, sorts );
            Assert.AreEqual( UutState.Present, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            Assert.AreEqual( expectedSortLabel, productSort.Label, $"{nameof( ProductSortEntity )} {nameof( ProductSortEntity.Label )} should match" );
            uutSort = AssetInsertUutSort( provider, uut, productSort.AutoId );
            Assert.AreEqual( UutState.Complete, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedFailureCountAlert = false;
            expectedFailureCount = 1;
            Assert.AreEqual( expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.LastSortEntity )}].[{nameof( isr.Dapper.Entities.ProductSortEntity.AutoId )}] should match" );
            Assert.AreEqual( expectedFailureCount, uutMonitor.ContinuousFailureCount,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureCount )}] should match" );
            Assert.AreEqual( expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}][{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureAlert )}] should match" );
            ;

            // D1 HIGH
            uutMonitor.PrepareNextUut( connection, lotAutoId, UutType.Chip );
            Assert.AreEqual( expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match" );
            uut = uutMonitor.ActiveUUTEntity;
            Assert.AreEqual( UutState.Ready, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedLastUutNumber = uut.UutNumber;
            bins = new ReadingBin[] { ReadingBin.Good, ReadingBin.Good, ReadingBin.High, ReadingBin.Good };
            uut = AssertAddingUutNutReadings( provider, uutMonitor.PlatformMeter, uut, bins );
            uut = AssertAddingUutNutBins( provider, uutMonitor.PlatformMeter, uut, uut.Nuts.Count, bins );
            expectedSort = ProductSort.CompoundResistorGuardFailed;
            expectedSortLabel = expectedSort.Description();
            expectedSortEntity = sorts.ProductSort( expectedSortLabel );
            productSort = uutMonitor.SortUut( binnings, sorts );
            Assert.AreEqual( UutState.Present, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            Assert.AreEqual( expectedSortLabel, productSort.Label, $"{nameof( ProductSortEntity )} {nameof( ProductSortEntity.Label )} should match" );
            uutSort = AssetInsertUutSort( provider, uut, productSort.AutoId );
            Assert.AreEqual( UutState.Complete, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedFailureCountAlert = false;
            expectedFailureCount = 1;
            Assert.AreEqual( expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.LastSortEntity )}].[{nameof( isr.Dapper.Entities.ProductSortEntity.AutoId )}] should match" );
            Assert.AreEqual( expectedFailureCount, uutMonitor.ContinuousFailureCount,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureCount )}] should match" );
            Assert.AreEqual( expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}][{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureAlert )}] should match" );
            ;

            // M1 LOW
            uutMonitor.PrepareNextUut( connection, lotAutoId, UutType.Chip );
            Assert.AreEqual( expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match" );
            uut = uutMonitor.ActiveUUTEntity;
            Assert.AreEqual( UutState.Ready, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedLastUutNumber = uut.UutNumber;
            bins = new ReadingBin[] { ReadingBin.Good, ReadingBin.Good, ReadingBin.Good, ReadingBin.Low };
            uut = AssertAddingUutNutReadings( provider, uutMonitor.PlatformMeter, uut, bins );
            uut = AssertAddingUutNutBins( provider, uutMonitor.PlatformMeter, uut, uut.Nuts.Count, bins );
            expectedSort = ProductSort.ComputedValueGuardFailed;
            expectedSortLabel = expectedSort.Description();
            expectedSortEntity = sorts.ProductSort( expectedSortLabel );
            productSort = uutMonitor.SortUut( binnings, sorts );
            Assert.AreEqual( UutState.Present, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            Assert.AreEqual( expectedSortLabel, productSort.Label, $"{nameof( ProductSortEntity )} {nameof( ProductSortEntity.Label )} should match" );
            uutSort = AssetInsertUutSort( provider, uut, productSort.AutoId );
            Assert.AreEqual( UutState.Complete, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedFailureCountAlert = false;
            expectedFailureCount = 1;
            Assert.AreEqual( expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.LastSortEntity )}].[{nameof( isr.Dapper.Entities.ProductSortEntity.AutoId )}] should match" );
            Assert.AreEqual( expectedFailureCount, uutMonitor.ContinuousFailureCount,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureCount )}] should match" );
            Assert.AreEqual( expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}][{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureAlert )}] should match" );
            ;

            // R1 HIGH M1 LOW
            uutMonitor.PrepareNextUut( connection, lotAutoId, UutType.Chip );
            Assert.AreEqual( expectedLastUutNumber + 1, uutMonitor.ActiveUUTEntity.UutNumber, $"{uutMonitor.ActiveUUTEntity.UutNumber} should match" );
            uut = uutMonitor.ActiveUUTEntity;
            Assert.AreEqual( UutState.Ready, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedLastUutNumber = uut.UutNumber;
            bins = new ReadingBin[] { ReadingBin.High, ReadingBin.Good, ReadingBin.Good, ReadingBin.Low };
            uut = AssertAddingUutNutReadings( provider, uutMonitor.PlatformMeter, uut, bins );
            uut = AssertAddingUutNutBins( provider, uutMonitor.PlatformMeter, uut, uut.Nuts.Count, bins );
            expectedSort = ProductSort.ResistorGuardFailed;
            expectedSortLabel = expectedSort.Description();
            expectedSortEntity = sorts.ProductSort( expectedSortLabel );
            productSort = uutMonitor.SortUut( binnings, sorts );
            Assert.AreEqual( UutState.Present, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            Assert.AreEqual( expectedSortLabel, productSort.Label, $"{nameof( ProductSortEntity )} {nameof( ProductSortEntity.Label )} should match" );
            uutSort = AssetInsertUutSort( provider, uut, productSort.AutoId );
            Assert.AreEqual( UutState.Complete, uut.UutState, $"{nameof( UutEntity )} {nameof( UutEntity.UutState )} should match" );
            expectedFailureCountAlert = false;
            expectedFailureCount = 1;
            Assert.AreEqual( expectedSortEntity.AutoId, uutMonitor.LastSortEntity.AutoId,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.LastSortEntity )}].[{nameof( isr.Dapper.Entities.ProductSortEntity.AutoId )}] should match" );
            Assert.AreEqual( expectedFailureCount, uutMonitor.ContinuousFailureCount,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}].[{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureCount )}] should match" );
            Assert.AreEqual( expectedFailureCountAlert, uutMonitor.ContinuousFailureAlert,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}][{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailureAlert )}] should match" );
            Assert.AreEqual( expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}][{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailuresStack )}].Count should match" );
            expectedContinuousFailureAlertCount = 0;
            uutMonitor.ClearContinuousFailures();
            Assert.AreEqual( expectedContinuousFailureAlertCount, uutMonitor.ContinuousFailuresStack.Count,
                                        $"[{nameof( isr.Dapper.Entities.LotEntity )}][{nameof( isr.Dapper.Entities.UutMonitor.ContinuousFailuresStack )}].Count should match after clear" );
            int expectedDeleteCount = 0;
            int actualDeleteCount = UutProductSortEntity.DeleteUnsortedUuts( connection, productTestMonitor.Session );
            Assert.AreEqual( expectedDeleteCount, actualDeleteCount,
                                        $"Unsorted [{nameof( isr.Dapper.Entities.UutEntity )}] count should match after clear" );
        }

        /// <summary>   Assert product entity. </summary>
        /// <remarks>   David, 2020-05-22. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="productNumber">    The product number. </param>
        /// <param name="partNumber">       The part number. </param>
        /// <param name="lotNumber">        The lot number. </param>
        /// <param name="sessionNumber">    The session number. </param>
        public static void AssertProductEntity( ProviderBase provider, string productNumber, string partNumber, string lotNumber, int sessionNumber )
        {
            AssertTablesExist( provider, new string[] { PartBuilder.TableName, ProductBuilder.TableName, ProductPartBuilder.TableName, LotBuilder.TableName } );
            SessionMonitor productTestMonitor;
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<PartNub>();
            _ = connection.ReseedTableIdentity( PartBuilder.TableName );
            _ = connection.DeleteAll<ProductNub>();
            _ = connection.ReseedTableIdentity( ProductBuilder.TableName );
            _ = connection.DeleteAll<ProductPartNub>();
            _ = connection.DeleteAll<LotNub>();
            _ = connection.ReseedTableIdentity( LotBuilder.TableName );

            // Use 2002 meter to allow selection of equations.
            int meterNumber = 1;
            int meterModel = ( int ) MeterModel.K2002;
            if ( MeterEntity.Meters is not object ) _ = MeterEntity.TryFetchAll( connection );
            PlatformMeter platformMeter = new( MeterEntity.SelectMeterEntity( meterModel, meterNumber ) );
            PlatformMeter[] platformMeters = new PlatformMeter[] { platformMeter };
            int meterId = platformMeter.MeterId;
            productTestMonitor = AssetAddingSessionMonitor( provider, productNumber, partNumber, lotNumber, sessionNumber, platformMeter );
            // this is not working because the platform meter elements are empty.
            // TO_FIX: AssertProductEntity( provider, productTestMonitor );
        }

        #endregion

        #region " READING BIN ENTITY "

        /// <summary> Assert Reading bin entity. </summary>
        /// <remarks> David, 2020-03-30. </remarks>
        /// <param name="site">               The site. </param>
        /// <param name="provider">           The provider. </param>
        /// <param name="goodBinNumber">      The good bin number. </param>
        /// <param name="goodBinNumberLabel"> The good bin number label. </param>
        /// <param name="binCount">           Number of bins. </param>
        public static void AssertReadingBinEntity( ProviderBase provider, int goodBinNumber, string goodBinNumberLabel, int binCount )
        {
            using var connection = provider.GetConnection();
            string tableName = ReadingBinBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            var binEntity = new ReadingBinEntity();
            _ = binEntity.FetchUsingKey( connection, goodBinNumber );
            string expectedLabel = goodBinNumberLabel;
            Assert.AreEqual( expectedLabel, binEntity.Label, $"{nameof( isr.Dapper.Entities.ReadingBinEntity )}.{nameof( isr.Dapper.Entities.ReadingBinEntity.Label )}" );
            int expectedCount = binCount;
            int count = binEntity.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, count, $"{nameof( isr.Dapper.Entities.ReadingBinNub )} entity correct number Of {nameof( isr.Dapper.Entities.ReadingBinNub )}" );
        }

        /// <summary> Assert Reading bin entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="goodBinNumber"> The good bin number. </param>
        public static void AssertReadingBinEntity( ProviderBase provider, ReadingBin goodBinNumber )
        {
            AssertReadingBinEntity( provider, ( int ) goodBinNumber, goodBinNumber.ToString(), Enum.GetValues( typeof( ReadingBin ) ).Length );
        }

        /// <summary> Assert Reading bin entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="goodBinNumber"> The good bin number. </param>
        public static void AssertReadingBinEntity( ProviderBase provider, QualityReadingBin goodBinNumber )
        {
            AssertReadingBinEntity( provider, ( int ) goodBinNumber, goodBinNumber.ToString(), Enum.GetValues( typeof( QualityReadingBin ) ).Length );
        }

        #endregion

        #region " REVISION "

        /// <summary> Assert update revision entity. </summary>
        /// <remarks> David, 2020-10-02. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertUpdateRevisionEntity( ProviderBase provider )
        {
            AssertTablesExist( provider, new string[] { RevisionBuilder.TableName } );
            DateTime timestamp;
            // timestamp = DateTime.Parse("2020-06-22 03:22:00.700")
            timestamp = DateTime.Parse( "2020-06-22 14:43:01.100" );
            using var connection = provider.GetConnection();
            RevisionBuilder.Instance.UpdateTimestamp( connection, 2, timestamp );
        }

        /// <summary> Asserts revision entity. </summary>
        /// <remarks>
        /// Time zone issue with SQL Light.  Storing the SQL value in UTC returns back time in the
        /// timezone of the 'server'.
        /// </remarks>
        /// <param name="site">                  The site. </param>
        /// <param name="provider">              The provider. </param>
        /// <param name="activeDatabaseVersion"> The active database version. </param>
        public static void AssertRevisionEntity( ProviderBase provider, string activeDatabaseVersion )
        {
            AssertTablesExist( provider, new string[] { RevisionBuilder.TableName } );
            string expectedActiveVersion = "1.0.0";
            string newActiveVersion = "0.0.1000";
            var revisionEntity = new RevisionEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();

            // clear previous if exists and reset identity
            bool exists = RevisionEntity.IsExists( connection, newActiveVersion );
            revisionEntity = new RevisionEntity();
            _ = revisionEntity.FetchUsingUniqueIndex( connection, activeDatabaseVersion );
            r = revisionEntity.ValidateStoredEntity( $"Existing {nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} Of {newActiveVersion}" );
            Assert.IsTrue( r.Success, r.Details );

            // delete the entity
            _ = revisionEntity.Delete( connection );

            // reseed the entity
            _ = connection.ReseedTableIdentity( RevisionBuilder.TableName );
            bool expectedFetched = false;
            bool actualFetched = false;
            revisionEntity = new RevisionEntity();
            actualFetched = revisionEntity.FetchUsingUniqueIndex( connection, expectedActiveVersion );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( RevisionEntity )} {nameof( RevisionEntity.Revision )} Of {expectedActiveVersion} should Not exist" );

            r = revisionEntity.ValidateNewEntity( $"New {nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} Of {expectedActiveVersion}" );
            Assert.IsTrue( r.Success, r.Details );

            revisionEntity.Clear();

            // clear the table; the table maybe empty, in which case the action returns False.
            _ = connection.DeleteAll<RevisionNub>();
            _ = connection.ReseedTableIdentity( RevisionBuilder.TableName );
            int expectedCount = 0;
            bool actualInserted = RevisionBuilder.UpsertRevision( connection, activeDatabaseVersion );
            Assert.IsTrue( actualInserted, $"Release {activeDatabaseVersion} should exist Or updated" );

            expectedCount += 1;
            expectedActiveVersion = activeDatabaseVersion;
            actualFetched = revisionEntity.FetchUsingUniqueIndex( connection, expectedActiveVersion );
            expectedFetched = true;
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} Of {expectedActiveVersion} should exist" );

            r = revisionEntity.ValidateStoredEntity( $"Existing {nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} Of {expectedActiveVersion}" );
            Assert.IsTrue( r.Success, r.Details );

            var previousActiveTimestamp = revisionEntity.Timestamp;
            string previousActiveVersion = revisionEntity.Revision;
            bool actualUpdate = true;
            actualUpdate = revisionEntity.Update( connection, UpdateModes.Refetch );
            Assert.IsFalse( actualUpdate, $"{nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} Of {expectedActiveVersion} should Not be updated" );

            r = revisionEntity.ValidateStoredEntity( $"Unchanged {nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} Of {expectedActiveVersion}" );
            Assert.IsTrue( r.Success, r.Details );

            _ = revisionEntity.FetchActive( connection );
            Assert.AreEqual( previousActiveTimestamp, revisionEntity.Timestamp, $"Active {nameof( RevisionEntity )} With {nameof( RevisionEntity.Timestamp )} should match previous value" );
            Assert.AreEqual( previousActiveVersion, revisionEntity.Revision, $"Active {nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} should match previous value" );

            var endTime = DateTime.UtcNow.Add( TimeSpan.FromSeconds( 1d ) );
            System.Threading.SpinWait.SpinUntil( () => DateTime.UtcNow > endTime );
            bool expectedInserted = true;
            expectedActiveVersion = newActiveVersion;
            revisionEntity = new RevisionEntity() { Revision = newActiveVersion };
            actualInserted = revisionEntity.Insert( connection );
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} Of {revisionEntity.Revision} should have been inserted" );

            var newTimestamp = revisionEntity.Timestamp;
            _ = revisionEntity.FetchActive( connection );
            Assert.AreEqual( newActiveVersion, revisionEntity.Revision, $"New active {nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} should match previous value" );
            Assert.AreEqual( newTimestamp, revisionEntity.Timestamp, $"New active {nameof( RevisionEntity )} With {nameof( RevisionEntity.Timestamp )} should match previous value" );

            // timestamp is in universal time.
            // set timestamp of the current version to precede the previous version by one second.
            RevisionBuilder.Instance.UpdateTimestamp( connection, revisionEntity.AutoId, previousActiveTimestamp.Subtract( TimeSpan.FromSeconds( 1d ) ) );
            // refetch the active revision; it should switch
            revisionEntity = new RevisionEntity();
            _ = revisionEntity.FetchActive( connection );
            Assert.AreEqual( previousActiveVersion, revisionEntity.Revision, $"Reactivated {nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} should match previous value" );
            Assert.AreEqual( previousActiveTimestamp, revisionEntity.Timestamp, $"Reactivated {nameof( RevisionEntity )} With {nameof( RevisionEntity.Timestamp )} should match previous value" );

            newTimestamp = previousActiveTimestamp.Add( TimeSpan.FromSeconds( 2d ) );
            RevisionBuilder.Instance.UpdateTimestamp( connection, newActiveVersion, newTimestamp );
            _ = revisionEntity.FetchActive( connection );
            Assert.AreEqual( newActiveVersion, revisionEntity.Revision, $"Restored new active {nameof( RevisionEntity )} With {nameof( RevisionEntity.Revision )} should match previous value" );
            Assert.AreEqual( newTimestamp, revisionEntity.Timestamp, $"Restored new active {nameof( RevisionEntity )} With {nameof( RevisionEntity.Timestamp )} should match previous value" );
        }

        #endregion

        #region " SESSION "

        /// <summary>   Assert session type entity. </summary>
        /// <remarks>   David, 2021-06-03. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertSessionTypeEntity( ProviderBase provider )
        {
            AssertTablesExist( provider, new string[] { SessionTypeBuilder.TableName } );
            int id = ( int ) Entities.SessionType.None;
            var SessionType = new SessionTypeEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = SessionType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( SessionTypeEntity )} With {nameof( SessionTypeEntity.Id )} Of {id} should Not exist" );
            r = SessionType.ValidateNewEntity( $"New {nameof( SessionTypeEntity )} With {nameof( SessionTypeEntity.Id )} Of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            SessionType.Clear();
            id = ( int ) Entities.SessionType.Production;
            expectedFetched = true;
            actualFetched = SessionType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( SessionTypeEntity )} With {nameof( SessionTypeEntity.Id )} Of {id}should exist" );
            r = SessionType.ValidateStoredEntity( $"Fetched {nameof( SessionTypeEntity )} With {nameof( SessionTypeEntity.Id )} Of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            SessionType.Label = Entities.SessionType.Production.ToString();
            actualFetched = SessionType.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( SessionTypeEntity )} by {nameof( SessionTypeEntity.Label )} Of {Entities.SessionType.Production} should exist" );
            r = SessionType.ValidateStoredEntity( $"Fetched {nameof( SessionTypeEntity )} by {nameof( SessionTypeEntity.Label )} Of {Entities.SessionType.Production}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = Enum.GetValues( typeof( SessionType ) ).Length - 1;
            int actualCount = SessionType.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Expected {expectedCount} {nameof( SessionTypeEntity )}s" );
        }

        /// <summary> Asserts <see cref="SessionTraitTypeEntity"/> test conditions. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertSessionTraitTypeEntity( ProviderBase provider )
        {
            AssertTablesExist( provider, new string[] { SessionTraitTypeBuilder.TableName } );
            int id = ( int ) SessionTraitType.None;
            var sessionTraitType = new SessionTraitTypeEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = sessionTraitType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( SessionTraitTypeEntity )} With {nameof( SessionTraitTypeEntity.Id )} Of {id} should Not exist" );
            r = sessionTraitType.ValidateNewEntity( $"New {nameof( SessionTraitTypeEntity )} With {nameof( SessionTraitTypeEntity.Id )} Of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            sessionTraitType.Clear();
            id = ( int ) SessionTraitType.SessionNumber;
            expectedFetched = true;
            actualFetched = sessionTraitType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( SessionTraitTypeEntity )} With {nameof( SessionTraitTypeEntity.Id )} Of {id}should exist" );
            r = sessionTraitType.ValidateStoredEntity( $"Fetched {nameof( SessionTraitTypeEntity )} With {nameof( SessionTraitTypeEntity.Id )} Of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            sessionTraitType.Label = SessionTraitType.SessionNumber.ToString();
            actualFetched = sessionTraitType.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( SessionTraitTypeEntity )} by {nameof( SessionTraitTypeEntity.Label )} Of {SessionTraitType.SessionNumber} should exist" );
            r = sessionTraitType.ValidateStoredEntity( $"Fetched {nameof( SessionTraitTypeEntity )} by {nameof( SessionTraitTypeEntity.Label )} Of {SessionTraitType.SessionNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = Enum.GetValues( typeof( SessionTraitType ) ).Length - 1;
            int actualCount = sessionTraitType.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"Expected {expectedCount} {nameof( SessionTraitTypeEntity )}s" );
        }

        /// <summary> Assert adding session entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">         The site. </param>
        /// <param name="provider">     The provider. </param>
        /// <param name="sessionLabel"> The session label. </param>
        /// <returns> An Integer. </returns>
        public static SessionEntity AssertAddingSessionEntity( ProviderBase provider, string sessionLabel )
        {
            AssertTablesExist( provider, new string[] { SessionBuilder.TableName } );
            int id = 3;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<SessionNub>();
            bool actualFetched;
            var session = new SessionEntity();
            actualFetched = session.FetchUsingKey( connection, id );
            Assert.IsFalse( actualFetched, $"{nameof( SessionEntity )} with {nameof( SessionEntity.AutoId )} of {id} should not exist" );
            r = session.ValidateNewEntity( $"{nameof( SessionEntity )} with {nameof( SessionEntity.AutoId )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            session.Clear();
            session = new SessionEntity() { Label = sessionLabel };
            bool actualInserted = session.Insert( connection );
            Assert.IsTrue( actualInserted, $"{nameof( SessionEntity )} with {nameof( SessionEntity.Label )} of {sessionLabel} should have been inserted" );
            r = session.ValidateStoredEntity( $"Inserted {nameof( SessionEntity )} with {nameof( SessionEntity.Label )} of {sessionLabel}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 1;
            int actualCount = session.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple {nameof( SessionEntity )}'s should have been inserted" );
            return session;
        }

        /// <summary> Assert adding lot session entities. </summary>
        /// <remarks> David, 2020-07-13. </remarks>
        /// <param name="site">         The site. </param>
        /// <param name="provider">     The provider. </param>
        /// <param name="partNumber">   The part number. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <param name="sessionCount"> Number of sessions. </param>
        public static void AssertAddingLotSessionEntities( ProviderBase provider, string partNumber, string lotNumber, int sessionCount )
        {
            AssertTablesExist( provider, new string[] { SessionBuilder.TableName, LotBuilder.TableName, LotSessionBuilder.TableName, PartBuilder.TableName } );
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<SessionNub>();
            _ = connection.DeleteAll<LotNub>();
            _ = connection.DeleteAll<PartNub>();
            int partId = 1;
            var part = new PartEntity() { PartNumber = partNumber };
            _ = part.Insert( connection );
            partId = part.AutoId;
            r = part.ValidateStoredEntity( $"Inserted {nameof( PartEntity )} with '{nameof( PartEntity.PartNumber )}' of {partNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            var lot = new LotEntity() { LotNumber = lotNumber, AutoId = partId };
            bool actualInserted = lot.Insert( connection );
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( LotEntity )}  with '{nameof( LotEntity.LotNumber )}' of {lot.LotNumber} should have been inserted" );
            r = lot.ValidateStoredEntity( $"Insert {nameof( LotEntity )} with '{nameof( LotEntity.LotNumber )}' of {lotNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 0;
            SessionEntity session;
            LotSessionEntity lotSession;
            string label;
            expectedInserted = true;
            for ( int i = 1; i <= 2; i++ )
            {
                label = $"{lot.LotNumber}:{expectedCount + 1}";
                session = new SessionEntity() { Label = label };
                actualInserted = session.Insert( connection );
                Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( SessionEntity )} with {nameof( SessionEntity.Label )} of {label} should have been inserted" );
                r = session.ValidateStoredEntity( $"{nameof( SessionEntity )} with {nameof( SessionEntity.Label )} of {label}" );
                Assert.IsTrue( r.Success, r.Details );
                lotSession = new LotSessionEntity() { LotAutoId = lot.AutoId, SessionAutoId = session.AutoId };
                actualInserted = lotSession.Insert( connection );
                Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( LotSessionEntity )} with [{nameof( LotSessionEntity.LotAutoId )},{nameof( LotSessionEntity.SessionAutoId )}] of [{lot.AutoId},{session.AutoId}] should have been inserted" );
                r = lotSession.ValidateStoredEntity( $"Inserted {nameof( LotSessionEntity )} with [{nameof( LotSessionEntity.LotAutoId )},{nameof( LotSessionEntity.SessionAutoId )}] of [{lot.AutoId},{session.AutoId}]" );
                Assert.IsTrue( r.Success, r.Details );
                expectedCount += 1;
                _ = lotSession.FetchUsingKey( connection, lot.AutoId, session.AutoId );
                Assert.AreEqual( lot.AutoId, lotSession.LotAutoId, $"{nameof( LotSessionEntity )} [{nameof( LotSessionEntity.LotAutoId )}] should match" );
                Assert.AreEqual( session.AutoId, lotSession.SessionAutoId, $"{nameof( LotSessionEntity )} [{nameof( LotSessionEntity.SessionAutoId )}] should match" );
            }

            int actualCount = LotSessionEntity.CountEntities( connection, lot.AutoId );
            Assert.AreEqual( expectedCount, actualCount, $"{nameof( LotSessionEntity )}'s count should match" );
        }

        #endregion

        #region " SESSION MONITOR "

        /// <summary>   Asset adding Session Monitor. </summary>
        /// <remarks>   David, 2020-05-28. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="productNumber">    The product number. </param>
        /// <param name="partNumber">       The part number. </param>
        /// <param name="lotNumber">        The lot number. </param>
        /// <param name="sessionNumber">    The session number. </param>
        /// <param name="platformMeter">    The platform meter. </param>
        /// <returns>   An Entities.ProductTestMonitor. </returns>
        public static SessionMonitor AssetAddingSessionMonitor( ProviderBase provider, string productNumber, string partNumber,
                                                                string lotNumber, int sessionNumber, PlatformMeter platformMeter )
        {
            AssertTablesExist( provider, new string[] { ProductBuilder.TableName, PartBuilder.TableName, ProductPartBuilder.TableName, LotBuilder.TableName } );
            var productTestMonitor = SessionMonitor.Instance;
            PartEntity part;
            // Dim element As ElementEntity
            ProductPartEntity productPart;
            ProductEntity product;
            LotEntity lot;
            SessionEntity session;
            using ( var connection = provider.GetConnection() )
            {
                product = AssertAddingProductEntity( provider, productNumber );
                part = AssertAddingPartEntity( provider, partNumber );
                productTestMonitor.Part = part;
                productPart = AssertAddingProductPartEntity( provider, product.AutoId, part.AutoId );
                int expectedElementCount = AssertAddingProductElementEntities( provider, product, part, platformMeter );
                productTestMonitor.Elements = part.Elements;
                int overflowContinuousFailureCountLimit = 2;
                productTestMonitor.Sorts = AssertAddingProductSortEntities( provider, product, overflowContinuousFailureCountLimit );
                productTestMonitor.Binnings = AssertAddingProductBinningEntities( provider, product, productTestMonitor.Elements, productTestMonitor.Sorts );
                lot = AssertAddingLotEntity( provider, part.AutoId, lotNumber );
                session = AssertAddingSessionEntity( provider, $"{lot.LotNumber}.{sessionNumber}" );
                _ = session.FetchUuts( connection );
                productTestMonitor.Lot = lot;
                productTestMonitor.UutMonitor = new UutMonitor( platformMeter );
                productTestMonitor.Uuts = session.Uuts;
            }
            return productTestMonitor;
        }

        #endregion

        #region " SESSION OHM METER "

        /// <summary> Assert session ohm meter setup entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="sessionAutoId"> Identifier for the session automatic identity. </param>
        public static void AssertSessionOhmMeterSetupEntity( ProviderBase provider, int sessionAutoId )
        {
            AssertTablesExist( provider, new string[] { OhmMeterSettingLookupBuilder.TableName, OhmMeterSetupBuilder.TableName, OhmMeterSettingBuilder.TableName, SessionOhmMeterSetupBuilder.TableName } );
            int ohmMeterSettingLookupId = 0;
            var ohmMeterSettingLookup = new OhmMeterSettingLookupEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            bool expectedFetched = false;
            bool actualFetched = ohmMeterSettingLookup.FetchUsingKey( connection, ohmMeterSettingLookupId );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {ohmMeterSettingLookupId} should not exist" );
            r = ohmMeterSettingLookup.ValidateNewEntity( $"New {nameof( isr.Dapper.Entities.IOhmMeterSettingLookup )} with {nameof( isr.Dapper.Entities.IOhmMeterSettingLookup.OhmMeterSettingId )} equals {ohmMeterSettingLookupId}" );
            Assert.IsTrue( r.Success, r.Details );
            ohmMeterSettingLookup.Clear();
            var meterEntities = MeterEntity.FetchAllEntities( connection, false );
            int expectedCount = 6;
            Assert.AreEqual( expectedCount, meterEntities.Count(), $"There should be enough meter entities" );
            string expectedToleranceCode = "A";
            double resistance = 399d;
            OhmMeterSettingLookupNub ohmMeterSettingLookupNub;
            bool actualExists;
            var ohmMeterSettingEntity = new OhmMeterSettingEntity();
            var ohmMeterSetupEntity = new OhmMeterSetupEntity();
            int actualCount;

            // clear the table
            _ = connection.DeleteAll<SessionOhmMeterSetupNub>();
            _ = connection.DeleteAll<OhmMeterSetupNub>();
            int id = 1;
            ohmMeterSettingEntity = new OhmMeterSettingEntity() { Id = id };
            actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex( connection );
            Assert.IsTrue( actualExists, $"{nameof( OhmMeterSettingEntity )} should exist for auto id = {id}" );
            actualCount = OhmMeterSettingEntity.CountEntities( connection );
            expectedCount = 6; // at least 6
            Assert.IsTrue( actualCount >= expectedCount, $"{nameof( OhmMeterSettingEntity )} should have at least {expectedCount} records" );
            var sessionOhmMeterSetupEntity = new SessionOhmMeterSetupEntity();
            expectedCount = 0;
            actualCount = connection.CountEntities( SessionOhmMeterSetupBuilder.TableName );
            Assert.AreEqual( expectedCount, actualCount, $"Session ohm meter setup nub record count should match" );
            foreach ( MeterEntity meterEntity in meterEntities )
            {
                if ( meterEntity.MeterModelId == ( int ) MeterModel.CLT10 )
                    continue;
                ohmMeterSettingLookupNub = OhmMeterSettingLookupEntity.FetchNubs( connection, expectedToleranceCode, meterEntity.Id, resistance ).First();
                ohmMeterSettingEntity = new OhmMeterSettingEntity() { Id = ohmMeterSettingLookupNub.OhmMeterSettingId };
                actualExists = ohmMeterSettingEntity.FetchUsingUniqueIndex( connection );
                Assert.IsTrue( actualExists, $"ohm meter setting condition should exist for id = {ohmMeterSettingEntity.Id}" );
                ohmMeterSetupEntity = new OhmMeterSetupEntity();
                ohmMeterSetupEntity.Copy( ohmMeterSettingEntity );
                actualCount = OhmMeterSetupEntity.CountEntities( connection );
                expectedCount = actualCount;
                expectedCount += 1;
                actualExists = ohmMeterSetupEntity.Insert( connection );
                Assert.IsTrue( actualExists, $"A new resistance ohm meter setup condition should be stored for meter {meterEntity.MeterModelId} number {meterEntity.MeterNumber}" );
                actualCount = OhmMeterSetupEntity.CountEntities( connection );
                Assert.IsTrue( actualCount >= expectedCount, $"Resistance ohm meter setup condition should have at least {expectedCount} records" );
                sessionOhmMeterSetupEntity = new SessionOhmMeterSetupEntity() {
                    SessionAutoId = sessionAutoId,
                    OhmMeterSetupAutoId = ohmMeterSetupEntity.AutoId,
                    MeterId = meterEntity.Id
                };
                actualExists = sessionOhmMeterSetupEntity.Insert( connection );
                Assert.IsTrue( actualExists, $"A new session ohm meter setup condition should be stored for meter {meterEntity.MeterModelId} and session id {sessionAutoId}" );
                actualCount = SessionOhmMeterSetupEntity.CountEntities( connection, sessionAutoId );
                Assert.IsTrue( actualCount >= expectedCount, $"Session ohm meter setup condition should have at least {expectedCount} records" );
            }
        }

        /// <summary> Asserts station session ohm meter setup entity. </summary>
        /// <remarks> David, 2020-06-21. </remarks>
        /// <param name="site">         The site. </param>
        /// <param name="provider">     The provider. </param>
        /// <param name="sessionLabel"> The session label. </param>
        public static void AssertSessionOhmMeterSetupEntity( ProviderBase provider, string sessionLabel )
        {
            var session = AssertAddingSessionEntity( provider, sessionLabel );
            AssertSessionOhmMeterSetupEntity( provider, session.AutoId );
        }

        #endregion

        #region " SESSION TRAIT "

        /// <summary> Assert adding Session Traits. </summary>
        /// <remarks> David, 2020-05-12. </remarks>
        /// <param name="site">          The site. </param>
        /// <param name="provider">      The provider. </param>
        /// <param name="sessionAutoId"> Identifier for the session automatic identity. </param>
        /// <returns> A SessionEntity. </returns>
        public static SessionEntity AssertAddingSessionTraits( ProviderBase provider, int sessionAutoId )
        {
            AssertTablesExist( provider, new string[] { SessionBuilder.TableName, SessionTraitBuilder.TableName, SessionTraitTypeBuilder.TableName } );
            var session = new SessionEntity() { AutoId = sessionAutoId };
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                int expectedTraitCount = 0;
                int sessionNumber = 1;
                var sessionTraitEntity = new SessionTraitEntity() { SessionAutoId = sessionAutoId, SessionTraitTypeId = ( int ) SessionTraitType.SessionNumber, Amount = sessionNumber };
                _ = sessionTraitEntity.Insert( connection );
                expectedTraitCount += 1;
                int testTypeId = ( int ) TestType.Final;
                sessionTraitEntity = new SessionTraitEntity() { SessionAutoId = sessionAutoId, SessionTraitTypeId = ( int ) SessionTraitType.SessionType, Amount = testTypeId };
                _ = sessionTraitEntity.Insert( connection );
                expectedTraitCount += 1;
                _ = session.FetchUsingKey( connection );
                r = session.ValidateStoredEntity( $"Fetch {nameof( SessionEntity )} with '{nameof( SessionEntity.AutoId )}' of {sessionAutoId}" );
                Assert.IsTrue( r.Success, r.Details );
                Assert.AreEqual( expectedTraitCount, session.FetchTraits( connection ), $"{nameof( SessionEntity )} {nameof( SessionEntity.Traits )} count should match" );
                Assert.AreEqual( sessionNumber, session.Traits.SessionTrait.SessionNumber, $"{nameof( SessionEntity )} {nameof( SessionEntity.Traits )} {nameof( SessionTrait.SessionNumber )} should match" );
                Assert.AreEqual( testTypeId, session.Traits.SessionTrait.SessionType, $"{nameof( SessionEntity )} {nameof( SessionEntity.Traits )} {nameof( SessionTrait.SessionType )} should match" );
                int modifiedSessionNumber = sessionNumber + 1;
                session.Traits.SessionTrait.SessionNumber = modifiedSessionNumber;
                Assert.IsTrue( session.Upsert( connection ), $"{nameof( SessionEntity )} {nameof( SessionEntity.Traits )} {nameof( SessionTrait.SessionNumber )} should be updated" );
                Assert.AreEqual( expectedTraitCount, session.FetchTraits( connection ), $"{nameof( SessionEntity )} {nameof( SessionEntity.Traits )} count should match after second fetch" );
                Assert.AreEqual( modifiedSessionNumber, session.Traits.SessionTrait.SessionNumber, $"{nameof( SessionEntity )} {nameof( SessionEntity.Traits )} {nameof( SessionTrait.SessionNumber )} should match after change" );
            }

            return session;
        }

        /// <summary> Assert adding Session Traits. </summary>
        /// <remarks> David, 2020-06-17. </remarks>
        /// <param name="site">         The site. </param>
        /// <param name="provider">     The provider. </param>
        /// <param name="sessionLabel"> The session label. </param>
        public static void AssertAddingSessionTraits( ProviderBase provider, string sessionLabel )
        {
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<SessionNub>();
            _ = connection.DeleteAll<SessionTraitNub>();
            var Session = AssertAddingSessionEntity( provider, sessionLabel );
            Session = AssertAddingSessionTraits( provider, Session.AutoId );
        }

        #endregion

        #region " STATION "

        private static string _ComputerName;

        /// <summary>   Gets the name of the computer. </summary>
        /// <value> The name of the computer. </value>
        public static string ComputerName
        {
            get {
                if ( string.IsNullOrEmpty( _ComputerName ) )
                {
                    _ComputerName = Environment.MachineName;
                }
                return _ComputerName;
            }
        }

        private static string _TimeZoneStandardName;

        /// <summary>   Gets the name of the time zone standard. </summary>
        /// <value> The name of the time zone standard. </value>
        public static string TimeZoneStandardName
        {
            get {
                if ( string.IsNullOrEmpty( _TimeZoneStandardName ) )
                {
                    _TimeZoneStandardName = TimeZoneInfo.Local.StandardName;
                }
                return _TimeZoneStandardName;
            }
        }


        /// <summary> Asserts station entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">                        The site. </param>
        /// <param name="provider">                    The provider. </param>
        /// <param name="expectedUsingNativeTracking"> True to expected using native tracking. </param>
        public static void AssertStationEntity( ProviderBase provider, bool expectedUsingNativeTracking )
        {
            AssertTablesExist( provider, new string[] { ComputerBuilder.TableName, StationBuilder.TableName } );
            string expectedComputerName = Auditor.ComputerName;
            string expectedTimeZone = Auditor.TimeZoneStandardName;
            int id = 4;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            var computerStationEntity = new ComputerStationEntity();
            r = computerStationEntity.TryObtain( connection, expectedComputerName, expectedComputerName, () => { return true; } );
            Assert.IsTrue( r.Success, r.Details );
            var station = computerStationEntity.FetchStationEntity( connection );
            r = station.ValidateStoredEntity( $"{nameof( StationEntity )}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualUpdate;
            string stationName = station.StationName;
            string newStationName = $"{stationName}.Changed";
            string expectedStationName = newStationName;
            id = station.AutoId;
            if ( connection.State == System.Data.ConnectionState.Closed )
                connection.Open();
            using ( var transaction = connection.BeginTransaction() )
            {
                try
                {
                    station.StationName = newStationName;
                    r = station.ValidateChangedEntity( $"Changed {nameof( StationEntity )} '{nameof( StationEntity.StationName )}' to {newStationName}" );
                    Assert.IsTrue( r.Success, r.Details );
                    var transactedConnection = new global::Dapper.TransactedConnection( connection, transaction );
                    expectedStationName = newStationName;
                    actualUpdate = station.Update( transactedConnection, UpdateModes.Refetch );
                    Assert.IsTrue( actualUpdate, $"Changed {nameof( StationEntity )} '{nameof( StationEntity.StationName )}' to {newStationName} should be updated" );
                    transaction.Commit();
                    _ = station.FetchUsingKey( connection );
                    Assert.AreEqual( expectedStationName, station.StationName, $"{nameof( StationEntity )} '{nameof( StationEntity.StationName )}' should be restored after transaction rollback" );
                }
                catch
                {
                    throw;
                }
            }

            using ( var transaction = connection.BeginTransaction() )
            {
                try
                {
                    expectedStationName = station.StationName;
                    station.StationName = stationName;
                    r = station.ValidateChangedEntity( $"Changed {nameof( StationEntity )} '{nameof( StationEntity.StationName )}' to {newStationName}" );
                    Assert.IsTrue( r.Success, r.Details );
                    var transactedConnection = new global::Dapper.TransactedConnection( connection, transaction );
                    actualUpdate = station.Update( transactedConnection, UpdateModes.Refetch );
                    Assert.IsTrue( actualUpdate, $"Changed {nameof( StationEntity )} '{nameof( StationEntity.StationName )}' to {newStationName} should be updated" );
                    transaction.Rollback();
                    _ = station.FetchUsingKey( connection );
                    Assert.AreEqual( expectedStationName, station.StationName, $"{nameof( StationEntity )} '{nameof( StationEntity.StationName )}' should be restored after transaction rollback" );
                }
                catch
                {
                    throw;
                }
            }
        }

        /// <summary> Asserts adding a station entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        /// <returns> A (AutoId As Integer, Name As String) </returns>
        public static (int AutoId, string Name) AssertAddingStationEntity( ProviderBase provider )
        {
            string expectedComputerName = Auditor.ComputerName;
            string expectedTimeZone = Auditor.TimeZoneStandardName;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = StationBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );

            _ = connection.DeleteAll<StationNub>();
            var computerStationEntity = new ComputerStationEntity();
            r = computerStationEntity.TryObtain( connection, expectedComputerName, expectedComputerName, () => { return true; } );
            Assert.IsTrue( r.Success, r.Details );

            var station = computerStationEntity.FetchStationEntity( connection );
            r = station.ValidateStoredEntity( $"{nameof( StationEntity )}" );
            Assert.IsTrue( r.Success, r.Details );
            return (station.AutoId, station.StationName);
        }

        #endregion

        #region " STATION SESSION "

        /// <summary> Assert station session entity. </summary>
        /// <remarks> David, 2020-07-06. </remarks>
        /// <param name="site">         The site. </param>
        /// <param name="provider">     The provider. </param>
        /// <param name="sessionLabel"> The session label. </param>
        public static void AssertStationSessionEntity( ProviderBase provider, string sessionLabel )
        {
            AssertTablesExist( provider, new string[] { StationSessionBuilder.TableName } );
            (int StationAutoId, string StationName) = AssertAddingStationEntity( provider );
            var session = AssertAddingSessionEntity( provider, sessionLabel );
            StationSessionEntity stationSession;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            stationSession = new StationSessionEntity() { StationAutoId = StationAutoId, SessionAutoId = session.AutoId };
            _ = stationSession.Obtain( connection );
            r = stationSession.ValidateStoredEntity( $"{nameof( StationSessionEntity )} with [{nameof( StationSessionEntity.StationAutoId )},{nameof( StationSessionEntity.SessionAutoId )}] or [{StationAutoId},{session.AutoId}] " );
            Assert.IsTrue( r.Success, r.Details );
        }

        #endregion

        #region " STRUCTURE TYPE "

        /// <summary> Asserts the Structure Type entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertStructureTypeEntity( ProviderBase provider )
        {
            int id = 10;
            var structureType = new StructureTypeEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = StructureTypeBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            bool expectedFetched = false;
            bool actualFetched = structureType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( StructureTypeEntity )} with '{nameof( StructureTypeEntity.Id )}' of '{id}' should not exist" );
            r = structureType.ValidateNewEntity( $"{nameof( StructureTypeEntity )} with '{nameof( StructureTypeEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            id = ( int ) StructureType.SplitCross;
            actualFetched = structureType.FetchUsingKey( connection, id );
            Assert.IsTrue( actualFetched, $"{nameof( StructureTypeEntity )} with '{nameof( StructureTypeEntity.Id )}' of '{id}' should exist" );
            r = structureType.ValidateStoredEntity( $"Fetched {nameof( StructureTypeEntity )} with '{nameof( StructureTypeEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            string label = StructureType.GreekCross.ToString();
            structureType.Label = label;
            r = structureType.ValidateChangedEntity( $"Changed {nameof( StructureTypeEntity )} with '{nameof( StructureTypeEntity.Label )}' of '{label}'" );
            Assert.IsTrue( r.Success, r.Details );
            actualFetched = structureType.FetchUsingUniqueIndex( connection );
            Assert.IsTrue( actualFetched, $"{nameof( StructureTypeEntity )} with '{nameof( StructureTypeEntity.Label )}' of '{label}' be fetched" );
            Assert.AreEqual( label, structureType.Label, $"{nameof( StructureTypeEntity )} with '{nameof( StructureTypeEntity.Label )}' of '{label}' should exist" );
            r = structureType.ValidateStoredEntity( $"Fetched {nameof( StructureTypeEntity )} with '{nameof( StructureTypeEntity.Label )}' of '{label}'" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 3;
            int actualCount = structureType.FetchAllEntities( connection );
            Assert.IsTrue( actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} PC Structure types" );
        }

        #endregion

        #region " STRUCTURE "

        /// <summary> Asserts Structure entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <param name="lotNumber">  The lot number. </param>
        public static void AssertStructureEntity( ProviderBase provider, string partNumber, string lotNumber )
        {
            int id = 3;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = StructureBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<PartNub>();
            _ = connection.DeleteAll<LotNub>();
            _ = connection.DeleteAll<SubstrateNub>();
            _ = connection.DeleteAll<StructureNub>();
            var part = new PartEntity() { PartNumber = partNumber };
            _ = part.Insert( connection );
            var lot = new LotEntity() { LotNumber = lotNumber, AutoId = part.AutoId };
            _ = lot.Insert( connection );
            var substrate = new SubstrateEntity() { Amount = 1, SubstrateTypeId = ( int ) SubstrateType.Wafer };
            _ = substrate.Insert( connection );
            bool expectedFetched = false;
            var structureEntity = new StructureEntity();
            bool actualFetched = structureEntity.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( StructureEntity )} with {nameof( StructureEntity.AutoId )} of {id} should not exist" );
            r = structureEntity.ValidateNewEntity( $"{nameof( StructureEntity )} with {nameof( StructureEntity.AutoId )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            structureEntity.Clear();
            var structureType = StructureType.GreekCross;
            int expectedCount = 3;
            int ordinalNumber = 1;
            structureEntity = new StructureEntity() { Amount = ordinalNumber, ForeignId = ( int ) structureType };
            bool actualInserted = structureEntity.Insert( connection );
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( StructureEntity )} with {nameof( StructureEntity.Amount )} of {ordinalNumber} should have been inserted" );
            Assert.AreEqual( ordinalNumber, structureEntity.Amount, $"{nameof( StructureEntity )} with {nameof( StructureEntity.Amount )} should match" );
            r = structureEntity.ValidateStoredEntity( $"Inserted {nameof( StructureEntity )} with {nameof( StructureEntity.Amount )} of {ordinalNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            ordinalNumber += 1;
            structureEntity = new StructureEntity() { Amount = ordinalNumber, ForeignId = ( int ) structureType };
            actualInserted = structureEntity.Insert( connection );
            Assert.AreEqual( expectedInserted, actualInserted, $"Structure {structureEntity.Amount} should have been inserted" );
            Assert.AreEqual( ordinalNumber, structureEntity.Amount, $"Structure Number should match" );
            r = structureEntity.ValidateStoredEntity( $"Inserted {nameof( StructureEntity )} with {nameof( StructureEntity.Amount )} of {ordinalNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            ordinalNumber += 1;
            structureEntity.Amount = ordinalNumber;
            r = structureEntity.ValidateChangedEntity( $"{nameof( StructureEntity )} with {nameof( StructureEntity.Amount )} to {ordinalNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualUpdate = structureEntity.Update( connection, UpdateModes.Refetch );
            bool expectedUpdate = true;
            Assert.AreEqual( expectedUpdate, actualUpdate, $"{nameof( StructureEntity )} with {nameof( StructureEntity.Amount )} of {ordinalNumber} should be updated" );
            r = structureEntity.ValidateStoredEntity( $"Updated {nameof( StructureEntity )} with {nameof( StructureEntity.Amount )} of {ordinalNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            actualUpdate = structureEntity.Update( connection, UpdateModes.Refetch );
            Assert.IsFalse( actualUpdate, $"Unchanged {nameof( StructureEntity )} with {nameof( StructureEntity.Amount )} of {ordinalNumber} should not be updated" );
            r = structureEntity.ValidateStoredEntity( $"Unchanged {nameof( StructureEntity )} with {nameof( StructureEntity.Amount )} of {ordinalNumber}" );
            Assert.IsTrue( r.Success, r.Details );
            ordinalNumber += 1;
            structureEntity.Amount = ordinalNumber;
            expectedUpdate = true;
            actualUpdate = structureEntity.Update( connection, UpdateModes.Refetch );
            Assert.AreEqual( expectedUpdate, actualUpdate, $"Structure {structureEntity.Amount} should be updated" );
            actualUpdate = structureEntity.Update( connection, UpdateModes.Refetch );
            Assert.IsFalse( actualUpdate, $"Structure {structureEntity.Amount} should not be updated; using cache" );
            _ = connection.DeleteAll<StructureNub>();
            for ( int i = 1, loopTo = expectedCount; i <= loopTo; i++ )
            {
                // this won't work; proxy is left dirty after the first insert; structureEntity.OrdinalNumber = i
                structureEntity = new StructureEntity() { Amount = i, ForeignId = ( int ) structureType };
                actualInserted = structureEntity.Insert( connection );
                Assert.IsTrue( actualInserted, $"Structure {structureEntity.Amount} should have been inserted" );
            }

            int actualCount = structureEntity.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple Structures should have been inserted" );
            bool actualDelete = structureEntity.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Structure Entity {ordinalNumber} should be deleted" );
            expectedCount -= 1;
            structureEntity = structureEntity.Structures.ElementAtOrDefault( 0 );
            int deletectedOrdinalNumber = structureEntity.Amount;
            actualDelete = StructureEntity.Delete( connection, structureEntity.AutoId );
            expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Structure {deletectedOrdinalNumber} should be deleted" );
            expectedCount -= 1;
            actualCount = structureEntity.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple Structures should have been inserted" );
        }

        #endregion

        #region " STRUCTURE SESSION "

        /// <summary> Asserts session Structure. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">         The site. </param>
        /// <param name="provider">     The provider. </param>
        /// <param name="partNumber">   The part number. </param>
        /// <param name="lotNumber">    The lot number. </param>
        /// <param name="sessionLabel"> The session label. </param>
        public static void AssertSessionStructure( ProviderBase provider, string partNumber, string lotNumber, string sessionLabel )
        {
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = SessionStructureBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<PartNub>();
            _ = connection.DeleteAll<LotNub>();
            _ = connection.DeleteAll<StructureNub>();
            _ = connection.DeleteAll<StationNub>();
            _ = connection.DeleteAll<SessionNub>();
            var part = new PartEntity() { PartNumber = partNumber };
            _ = part.Insert( connection );
            var lot = new LotEntity() { LotNumber = lotNumber, AutoId = part.AutoId };
            _ = lot.Insert( connection );
            var substrate = new SubstrateEntity() { Amount = 1, SubstrateTypeId = ( int ) SubstrateType.Wafer };
            _ = substrate.Insert( connection );
            var structureType = StructureType.SplitCross;
            int ordinalNumber = 1;
            var structureEntity = new StructureEntity() { Amount = ordinalNumber, ForeignId = ( int ) structureType };
            _ = structureEntity.Insert( connection );
            string expectedComputerName = Auditor.ComputerName;
            string expectedTimeZone = Auditor.TimeZoneStandardName;
            var computerStationEntity = new ComputerStationEntity();
            r = computerStationEntity.TryObtain( connection, expectedComputerName, expectedComputerName, () => { return true; } );
            Assert.IsTrue( r.Success, r.Details );
            var station = computerStationEntity.FetchStationEntity( connection );
            r = station.ValidateStoredEntity( $"{nameof( StationEntity )}" );
            Assert.IsTrue( r.Success, r.Details );
            var session = new SessionEntity() { Timestamp = DateTime.UtcNow, SessionLabel = sessionLabel };
            _ = session.Insert( connection );
            var sessionStructure = new SessionStructureEntity() { StructureAutoId = structureEntity.AutoId, SessionAutoId = session.AutoId };
            bool actualInserted = sessionStructure.Insert( connection );
            Assert.IsTrue( actualInserted, $"{nameof( SessionStructureEntity )} should have been inserted" );
            r = sessionStructure.ValidateStoredEntity( $"Inserted {nameof( SessionStructureEntity )}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualFetched = sessionStructure.FetchUsingKey( connection );
            Assert.IsTrue( actualFetched, $"{nameof( sessionStructure )} should have been fetched" );
            Assert.AreEqual( sessionStructure.SessionAutoId, session.AutoId, $"{nameof( SessionStructureEntity.SessionAutoId )} {nameof( SessionEntity.AutoId )} should match" );
            Assert.AreEqual( sessionStructure.StructureAutoId, structureEntity.AutoId, $"{nameof( SessionStructureEntity.StructureAutoId )} {nameof( StructureEntity.AutoId )} should match" );
            _ = sessionStructure.FetchSessionEntity( connection );
            Assert.AreEqual( session.AutoId, sessionStructure.SessionAutoId, $"{nameof( isr.Dapper.Entities.SessionStructureEntity )} should bring the correct {nameof( isr.Dapper.Entities.SessionEntity )} " );
            Assert.AreEqual( sessionStructure.StructureAutoId, structureEntity.AutoId, $"Structure Session should point to the correct Structure" );
            _ = connection.DeleteAll<SessionStructureNub>();
        }

        #endregion

        #region " SUBSTRATE TYPE "

        /// <summary> Executes the Substrate Type entity test operation. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertSubstrateTypeEntity( ProviderBase provider )
        {
            int id = 10;
            var entity = new SubstrateTypeEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = SubstrateTypeBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            bool expectedFetched = false;
            r = entity.ValidateNewEntity( $"{nameof( SubstrateTypeEntity )} with '{nameof( SubstrateTypeEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualFetched = entity.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {id} should not exist" );
            r = entity.ValidateNewEntity( $"{nameof( SubstrateTypeEntity )} with '{nameof( SubstrateTypeEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            id = 1;
            actualFetched = entity.FetchUsingKey( connection, id );
            Assert.IsTrue( actualFetched, $" {nameof( SubstrateTypeEntity )} with '{nameof( SubstrateTypeEntity.Id )}' of '{id}' should exist" );
            r = entity.ValidateStoredEntity( $"Existing {nameof( SubstrateTypeEntity )} with '{nameof( SubstrateTypeEntity.Id )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            string label = SubstrateType.Wafer.ToString();
            entity.Label = label;
            r = entity.ValidateChangedEntity( $"Changed {nameof( SubstrateTypeEntity )} with '{nameof( SubstrateTypeEntity.Label )}' of '{label}'" );
            Assert.IsTrue( r.Success, r.Details );
            actualFetched = entity.FetchUsingUniqueIndex( connection );
            Assert.IsTrue( actualFetched, $"{nameof( SubstrateTypeEntity )} with '{nameof( SubstrateTypeEntity.Label )}' of '{label}' should exist" );
            Assert.AreEqual( label, entity.Label, $"{nameof( SubstrateTypeEntity )} with '{nameof( SubstrateTypeEntity.Label )}' of '{label}' should match" );
            r = entity.ValidateStoredEntity( $"Fetched {nameof( SubstrateTypeEntity )} with '{nameof( SubstrateTypeEntity.Label )}' of '{label}'" );
            Assert.IsTrue( r.Success, r.Details );
            int expectedCount = 2;
            int actualCount = entity.FetchAllEntities( connection );
            Assert.IsTrue( actualCount >= expectedCount, $"Expected at least {expectedCount} found {actualCount} substrate types" );
        }

        #endregion

        #region " SUBSTRATE "

        /// <summary> Asserts substrate entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="partNumber"> The part number. </param>
        /// <param name="lotNumber">  The lot number. </param>
        public static void AssertSubstrateEntity( ProviderBase provider, string partNumber, string lotNumber )
        {
            int id = 3;
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = SubstrateBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            _ = connection.DeleteAll<PartNub>();
            _ = connection.DeleteAll<LotNub>();
            var part = new PartEntity() { PartNumber = partNumber };
            _ = part.Insert( connection );
            var lot = new LotEntity() { LotNumber = lotNumber, AutoId = part.AutoId };
            _ = lot.Insert( connection );
            _ = connection.DeleteAll<SubstrateNub>();
            bool expectedFetched = false;
            bool actualFetched = false;
            var substrate = new SubstrateEntity();
            actualFetched = substrate.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( SubstrateEntity )} with  {nameof( SubstrateEntity.AutoId )} of {id} should not exist" );
            r = substrate.ValidateNewEntity( $"{nameof( SubstrateEntity )} with  {nameof( SubstrateEntity.AutoId )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            substrate.Clear();
            id = 1;
            int expectedCount = 0;
            int actualCount = 0;
            substrate = new SubstrateEntity() { Amount = id, ForeignId = ( int ) SubstrateType.Plate };
            bool actualInserted = substrate.Insert( connection );
            bool expectedInserted = true;
            Assert.AreEqual( expectedInserted, actualInserted, $"{nameof( SubstrateEntity )} with {nameof( SubstrateEntity.Amount )} of {id} should have been inserted" );
            r = substrate.ValidateStoredEntity( $"Inserted {nameof( SubstrateEntity )} with {nameof( SubstrateEntity.Amount )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            expectedCount += 1;
            id = 2;
            substrate.Amount = id;
            r = substrate.ValidateChangedEntity( $"{nameof( SubstrateEntity )} {nameof( SubstrateEntity.Amount )} changed to {id}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualUpdate = substrate.Update( connection, UpdateModes.Refetch );
            bool expectedUpdate = true;
            Assert.AreEqual( expectedUpdate, actualUpdate, $"Changed {nameof( SubstrateEntity )} with {nameof( SubstrateEntity.Amount )} of {id} should be updated" );
            r = substrate.ValidateStoredEntity( $"Updated {nameof( SubstrateEntity )} with {nameof( SubstrateEntity.Amount )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            actualUpdate = substrate.Update( connection, UpdateModes.Refetch );
            Assert.IsFalse( actualUpdate, $"Unchanged {nameof( SubstrateEntity )} with {nameof( SubstrateEntity.Amount )} of {id} should not be updated" );
            r = substrate.ValidateStoredEntity( $"Unchanged {nameof( SubstrateEntity )} with {nameof( SubstrateEntity.Amount )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            int OrdinalNumber = substrate.Amount;
            actualFetched = substrate.Obtain( connection );
            expectedFetched = true;
            Assert.AreEqual( expectedFetched, actualFetched, $"Substrate {substrate.Amount} should have been obtained" );
            Assert.AreEqual( OrdinalNumber, substrate.Amount, $"Obtained Substrate {substrate.Amount} should match substrate number" );
            _ = connection.DeleteAll<SubstrateNub>();
            expectedCount = 10;
            for ( int i = 1, loopTo = expectedCount; i <= loopTo; i++ )
            {
                substrate.Amount = i;
                actualInserted = substrate.Insert( connection );
                expectedInserted = true;
                Assert.AreEqual( expectedInserted, actualInserted, $"Substrate {substrate.Amount} should have been inserted" );
            }

            actualCount = substrate.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"multiple Substrates should have been inserted" );
            OrdinalNumber = substrate.Amount;
            bool actualDelete = substrate.Delete( connection );
            bool expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Substrate {OrdinalNumber} should be deleted" );
            expectedCount -= 1;
            IKeyForeignNaturalTime substrate0 = substrate.Substrates.ElementAtOrDefault( 0 );
            actualDelete = SubstrateEntity.Delete( connection, substrate0.AutoId );
            expectedDelete = true;
            Assert.AreEqual( expectedDelete, actualDelete, $"Substrate {substrate0.Amount} should be deleted" );
            expectedCount -= 1;
        }

        #endregion

        #region " TEST TYPE "

        /// <summary> Asserts the test Type entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">               The site. </param>
        /// <param name="provider">           The provider. </param>
        /// <param name="existingTestTypeId"> Identifier for the existing test type. </param>
        /// <param name="expectedCount">      Number of expected. </param>
        public static void AssertTestTypeEntity( ProviderBase provider, int existingTestTypeId, int expectedCount )
        {
            int id = 10;
            var testType = new TestTypeEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = TestTypeBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );
            bool expectedFetched = false;
            r = testType.ValidateNewEntity( $"{nameof( isr.Dapper.Entities.TestTypeEntity )} with key {testType.Id}" );
            Assert.IsTrue( r.Success, r.Details );
            bool actualFetched = testType.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"key {id} should not exist" );
            r = testType.ValidateNewEntity( $"{nameof( isr.Dapper.Entities.TestTypeEntity )} with {nameof( isr.Dapper.Entities.TestTypeEntity.Id )} of {id}" );
            Assert.IsTrue( r.Success, r.Details );
            id = existingTestTypeId;
            actualFetched = testType.FetchUsingKey( connection, id );
            Assert.IsTrue( actualFetched, $"{nameof( isr.Dapper.Entities.TestTypeEntity )} key {id} should exist" );
            r = testType.ValidateStoredEntity( $"Existing {nameof( isr.Dapper.Entities.TestTypeEntity )}  {id}" );
            Assert.IsTrue( r.Success, r.Details );
            string existingLabel = testType.Label;
            actualFetched = testType.FetchUsingUniqueIndex( connection, existingLabel );
            Assert.IsTrue( actualFetched, $"{nameof( isr.Dapper.Entities.TestTypeEntity )} with {nameof( isr.Dapper.Entities.TestTypeEntity.Label )} of {existingLabel} should exist" );
            r = testType.ValidateStoredEntity( $"Existing {nameof( isr.Dapper.Entities.TestTypeEntity )} with {nameof( isr.Dapper.Entities.TestTypeEntity.Label )} of {existingLabel}" );
            Assert.IsTrue( r.Success, r.Details );
            string changedLabel = $"{existingLabel}.changed";
            if ( !string.Equals( testType.Label, changedLabel ) )
            {
                testType.Label = changedLabel;
                r = testType.ValidateChangedEntity( $"Changed {nameof( isr.Dapper.Entities.TestTypeEntity )} with {nameof( isr.Dapper.Entities.TestTypeEntity.Id )} of {id}" );
                Assert.IsTrue( r.Success, r.Details );
                // try to fetch using new name, which was not saved yet; 
                actualFetched = testType.FetchUsingUniqueIndex( connection );
                Assert.IsFalse( actualFetched,
                    $"{nameof( isr.Dapper.Entities.TestTypeEntity )} with {nameof( isr.Dapper.Entities.TestTypeEntity.Label )} of {changedLabel} should not exist" );
            }

            int actualCount = testType.FetchAllEntities( connection );
            Assert.AreEqual( expectedCount, actualCount, $"{nameof( isr.Dapper.Entities.TestTypeEntity )}s items should equal expected" );
        }

        /// <summary> Asserts the test Type entity. </summary>
        /// <remarks> David, 2020-04-28. </remarks>
        /// <param name="site">       The site. </param>
        /// <param name="provider">   The provider. </param>
        /// <param name="testTypeId"> Identifier for the test type. </param>
        public static void AssertTestTypeEntity( ProviderBase provider, TestType testTypeId )
        {
            AssertTestTypeEntity( provider, ( int ) testTypeId, Enum.GetValues( typeof( TestType ) ).Length - 1 );
        }

        #endregion

        #region " TOLERANCE "

        /// <summary> Asserts Tolerance entity. </summary>
        /// <remarks> David, 2020-03-26. </remarks>
        /// <param name="site">     The site. </param>
        /// <param name="provider"> The provider. </param>
        public static void AssertToleranceEntity( ProviderBase provider )
        {
            string id = string.Empty;
            var tolerance = new ToleranceEntity();
            (bool Success, string Details) r;
            using var connection = provider.GetConnection();
            string tableName = ToleranceBuilder.TableName;
            Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
            Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );

            // test equality and cast to Entity Nub.
            Assert.IsTrue( tolerance.CacheEquals( tolerance ), $"{nameof( ToleranceEntity )} should equals itself" );
            _ = tolerance.UpdateStore();
            Assert.IsTrue( tolerance.StoreEquals( tolerance ), $"{nameof( ToleranceEntity )} should equals itself" );

            bool expectedFetched = false;
            bool actualFetched = tolerance.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"key '{id}' should not exist" );
            r = tolerance.ValidateNewEntity( $"New {nameof( ToleranceEntity )} with '{nameof( ToleranceEntity.ToleranceCode )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            tolerance.Clear();
            id = "A";
            double expectedLowerLimit = -0.0005d;
            double expectedUpperLimit = 0.0005d;
            string expectedCaption = "±0.05%";
            expectedFetched = true;
            actualFetched = tolerance.FetchUsingKey( connection, id );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( ToleranceEntity )} with '{nameof( ToleranceEntity.ToleranceCode )}' of  {id} should exist" );
            r = tolerance.ValidateStoredEntity( $"Fetched {nameof( ToleranceEntity )} with '{nameof( ToleranceEntity.ToleranceCode )}' of '{id}'" );
            Assert.IsTrue( r.Success, r.Details );
            Assert.AreEqual( expectedLowerLimit, tolerance.LowerLimit, $"{nameof( tolerance.LowerLimit )} should match" );
            Assert.AreEqual( expectedUpperLimit, tolerance.UpperLimit, $"{nameof( tolerance.UpperLimit )} should match" );
            Assert.AreEqual( expectedCaption, tolerance.Caption, $"{nameof( tolerance.Caption )} should match" );
            tolerance.Caption = expectedCaption;
            expectedFetched = true;
            actualFetched = tolerance.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"{nameof( ToleranceEntity )} with '{nameof( ToleranceEntity.CompoundCaption )}' of '{expectedCaption}' should exist" );
            r = tolerance.ValidateStoredEntity( $"Fetched {nameof( ToleranceEntity )} with '{nameof( ToleranceEntity.CompoundCaption )}' of '{expectedCaption}'" );
            Assert.IsTrue( r.Success, r.Details );
            tolerance.Caption = expectedCaption + "0";
            expectedFetched = false;
            actualFetched = tolerance.FetchUsingUniqueIndex( connection );
            Assert.AreEqual( expectedFetched, actualFetched, $"caption {expectedCaption} should not exist" );
            int expectedCount = 19;
            int actualCount = tolerance.FetchAllEntities( connection );
            Assert.IsTrue( expectedCount <= actualCount, $"Expected {expectedCount} tolerance count should not exceed actual {actualCount}" );
            bool expectedUniqueLabel = true;
            bool actualUniqueLabel = ToleranceBuilder.UsingUniqueCaption( connection );
            Assert.AreEqual( expectedUniqueLabel, actualUniqueLabel, $"{nameof( ToleranceBuilder )} '{nameof( ToleranceBuilder.UsingUniqueCaption )}' should match" );
        }

        #endregion

        #region " UUT (SESSION) "

        /// <summary>   Assert adding uut entity. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="meterId">          Identifier for the meter. </param>
        /// <param name="sessionLabel">     The session label. </param>
        /// <param name="lastUutNumber">    The last uut number. </param>
        /// <returns>   An UutEntity. </returns>
        public static UutEntity AssertAddingUutEntity( ProviderBase provider, int meterId, string sessionLabel, int lastUutNumber )
        {
            UutEntity uut;
            SessionUutEntity sessionUut;
            int uutNumber;
            (bool Success, string Details) r;
            using ( var connection = provider.GetConnection() )
            {
                string tableName = UutBuilder.TableName;
                Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
                Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );

                tableName = SessionUutBuilder.TableName;
                Console.Out.WriteLine( $"table {tableName} {(provider.TableExists( connection, tableName ) ? "exists" : "not found")}" );
                Assert.IsTrue( provider.TableExists( connection, tableName ), $"table {tableName} should exist" );

                bool actualObtained;
                sessionUut = new SessionUutEntity();
                r = sessionUut.TryObtain( connection, sessionLabel, -1, ( int ) UutType.Chip );
                actualObtained = r.Success;
                Assert.IsTrue( r.Success,
                    $"{nameof( SessionUutEntity )} with '{nameof( SessionUutEntity.SessionAutoId )}' of {sessionLabel} should have been inserted; {r.Details}" );

                r = sessionUut.ValidateStoredEntity( $"Insert {nameof( SessionUutEntity )} with '{nameof( SessionUutEntity.SessionAutoId )}' of {sessionLabel}" );
                Assert.IsTrue( r.Success, r.Details );

                uutNumber = sessionUut.UutEntity.UutNumber;
                Assert.AreEqual( lastUutNumber + 1, uutNumber, $"{nameof( SessionUutEntity )} {nameof( UutEntity )} {nameof( UutEntity.UutNumber )} should match" );

                r = sessionUut.TryObtainUut( connection, sessionUut.SessionAutoId, uutNumber, ( int ) UutType.Chip );
                Assert.IsTrue( r.Success, r.Details );
                Assert.AreEqual( uutNumber, sessionUut.UutEntity.UutNumber, $"Fetch {nameof( UutEntity )} {nameof( UutEntity.UutNumber )} should match" );

                uut = sessionUut.FetchUutEntity( connection );
                r = uut.ValidateStoredEntity(
                    $"Fetch {nameof( UutEntity )} from {nameof( SessionUutEntity )} with '{nameof( SessionUutEntity.UutAutoId )}' of {sessionUut.UutAutoId}" );
                Assert.IsTrue( r.Success, r.Details );

                uut = SessionUutEntity.FetchLastUut( connection, sessionUut.SessionAutoId );
                Assert.AreEqual( uutNumber, uut.UutNumber, $"Fetched last {nameof( UutEntity )} {nameof( UutEntity.UutNumber )} should match" );

                r = uut.ValidateStoredEntity( $"Fetched last {nameof( UutEntity )}" );
                Assert.IsTrue( r.Success, r.Details );
                _ = uut.FetchTraits( connection );
                uut.Traits.UutTrait.MeterId = meterId;
                _ = uut.Traits.Upsert( connection );
            }

            return uut;
        }

        /// <summary>   Assert uut entity. </summary>
        /// <remarks>   David, 2020-05-12. </remarks>
        /// <param name="site">             The site. </param>
        /// <param name="provider">         The provider. </param>
        /// <param name="sessionLabels">    The session labels. </param>
        public static void AssertUutEntity( ProviderBase provider, string[] sessionLabels )
        {
            using var connection = provider.GetConnection();
            _ = connection.DeleteAll<UutNub>();
            _ = connection.DeleteAll<SessionUutNub>();
            _ = connection.DeleteAll<UutProductSortNub>();

            // Use 2002 meter to allow selection of equations.
            int meterNumber = 1;
            int meterModel = ( int ) MeterModel.K2002;
            if ( MeterEntity.Meters is not object ) _ = MeterEntity.TryFetchAll( connection );
            PlatformMeter platformMeter = new( MeterEntity.SelectMeterEntity( meterModel, meterNumber ) );
            PlatformMeter[] platformMeters = new PlatformMeter[] { platformMeter };
            int meterId = platformMeter.MeterId;

            int expectedLastUutNumber = 0;
            UutEntity uut = null;
            foreach ( var sessionLabel in sessionLabels )
            {
                expectedLastUutNumber = 0;
                uut = AssertAddingUutEntity( provider, meterId, sessionLabel, expectedLastUutNumber );
                expectedLastUutNumber = uut.UutNumber;
                _ = AssertAddingUutEntity( provider, meterId, sessionLabel, expectedLastUutNumber );
            }
        }

        #endregion

    }
}

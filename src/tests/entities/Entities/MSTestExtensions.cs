using System;

namespace isr.Dapper.Entities.MSTestExtensions
{
    public static class MSTestExtensionMethods
    {

        /// <summary>   A System.IO.TextWriter extension method that traces an elapsed time message. </summary>
        /// <remarks>   David, 2021-06-24. </remarks>
        /// <param name="writer">       The writer. </param>
        /// <param name="startTime">    The start time. </param>
        /// <param name="activity">     The activity. </param>
        /// <returns>   A DateTimeOffset. </returns>
        public static DateTimeOffset TraceElapsedTime( this System.IO.TextWriter writer, DateTimeOffset startTime, string activity )
        {
            writer.WriteLine( $"{activity} {DateTimeOffset.Now.Subtract( startTime ).TotalMilliseconds}ms" );
            return DateTimeOffset.Now;
        }

    }
}

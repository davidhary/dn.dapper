
namespace isr.Dapper.Entities.MSTest
{

    /// <summary> Test information for the Polynomial Fit Tests. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2/12/2018 </para></remarks>
    [isr.Json.SettingsSection( nameof( EntitiesTestsInfo ) )]
    internal class EntitiesTestsInfo : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public EntitiesTestsInfo() : base( System.Reflection.Assembly.GetAssembly( typeof( EntitiesTestsInfo ) ) )
        { }

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public virtual bool Verbose { get; set; }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public virtual bool Enabled { get; set; }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public virtual bool All { get; set; }

        #endregion

        #region " RESENBROCK CONDITIONS "

        /// <summary> Gets or sets the seed. </summary>
        /// <value> The seed. </value>
        public int Seed { get; set; }

        /// <summary> Gets or sets the number of iterations. </summary>
        /// <value> The number of iterations. </value>
        public int IterationCount { get; set; }

        /// <summary> Gets or sets the objective. </summary>
        /// <value> The objective. </value>
        public double Objective { get; set; }

        /// <summary> Gets or sets the convergence. </summary>
        /// <value> The convergence. </value>
        public double Convergence { get; set; }

        /// <summary> Gets or sets the minimum. </summary>
        /// <value> The minimum value. </value>
        public double Minimum { get; set; }

        /// <summary> Gets or sets the maximum. </summary>
        /// <value> The maximum value. </value>
        public double Maximum { get; set; }

        #endregion

    }
}

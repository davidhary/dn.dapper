Imports System.IO

Imports Dapper.Contrib.Extensions

Imports isr.Core.Constructs
Imports isr.Core.EnumExtensions
Imports isr.CoreTests
Imports isr.Dapper.Entity
Imports isr.Dapper.Entity.ConnectionExtensions

Partial Public NotInheritable Class Auditor

    ''' <summary> Select reading type. </summary>
    ''' <remarks> David, 6/1/2020. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="label"> The label. </param>
    ''' <returns> A ReadingType. </returns>
    Public Shared Function SelectElementReadingType(ByVal label As String) As ReadingType
        Select Case label
            Case "R1"
                Return ReadingType.Resistance
            Case "R2"
                Return ReadingType.Resistance
            Case "D1"
                Return ReadingType.Resistance
            Case "M1"
                Return ReadingType.Equation
            Case Else
                Throw New ArgumentException($"Unhandled [{NameOf(ElementEntity)}].[{NameOf(ElementEntity.Label)}] of {label}")
        End Select
    End Function

    ''' <summary> Select Nut reading. </summary>
    ''' <remarks> David, 6/1/2020. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="nut">          The nut. </param>
    ''' <param name="elementLabel"> The element label. </param>
    ''' <returns> A Double? </returns>
    Public Shared Function SelectNutReading(ByVal nut As NutEntity, ByVal elementLabel As String) As Double?
        Select Case elementLabel
            Case "R1"
                Return nut.Readings.NutReading.Resistance
            Case "R2"
                Return nut.Readings.NutReading.Resistance
            Case "D1"
                Return nut.Readings.NutReading.Resistance
            Case "M1"
                Return nut.Readings.NutReading.Equation
            Case Else
                Throw New ArgumentException($"Unhandled [{NameOf(ElementEntity)}].[{NameOf(ElementEntity.Label)}] of {elementLabel}")
        End Select
    End Function



#Region " NUT "

    ''' <summary> Assert adding Nut entity. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">      The site. </param>
    ''' <param name="provider">  The provider. </param>
    ''' <param name="uutAutoId"> Identifier for the uut automatic. </param>
    ''' <param name="elementId"> Identifier for the element. </param>
    ''' <returns> A <see cref="Dapper.Entities.NutEntity"/>. </returns>
    Public Shared Function AssertAddingNutEntity(ByVal site As isr.Std.FrameworkTests.TestSiteBase, ByVal provider As ProviderBase,
                                                 ByVal uutAutoId As Integer, ByVal elementId As Integer, ByVal nominalTypeId As Integer) As NutEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName, UutNutBuilder.TableName})

        Dim id As Integer = 3
        Dim nut As NutEntity
        Dim UutNut As UutNutEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            UutNut = New UutNutEntity()
            With UutNut.TryObtain(connection, uutAutoId, elementId, nominalTypeId)
                Assert.IsTrue(.Success, $"{NameOf(Entities.UutNutEntity)} with '{NameOf(Entities.UutNutEntity.UutAutoId)}' of {uutAutoId} should have been inserted; { .Details}")
            End With
            With UutNut.ValidateStoredEntity($"Insert {NameOf(Entities.UutNutEntity)} with '{NameOf(Entities.UutNutEntity.UutAutoId)}' of {uutAutoId}")
                Assert.IsTrue(.Success, .Details)
            End With

            Dim actualFetched As Boolean = False
            nut = UutNut.FetchNutEntity(connection)
            With nut.ValidateStoredEntity($"Fetch {NameOf(Entities.NutEntity)} with '{NameOf(Entities.NutEntity.ElementId)}' of {elementId}")
                Assert.IsTrue(.Success, .Details)
            End With
            nut.FetchReadings(connection)
        End Using
        Return nut
    End Function

    ''' <summary> Assert adding Nut readings. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="nutId">    Identifier of the <see cref="Dapper.Entities.NutEntity"/>.. </param>
    Public Shared Function AssertAddingNutReadings(ByVal site As isr.Std.FrameworkTests.TestSiteBase, ByVal provider As ProviderBase, ByVal nutId As Integer) As NutEntity

        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName, NutReadingBuilder.TableName})
        Dim nut As New NutEntity With {.AutoId = nutId}
        Using connection As System.Data.IDbConnection = provider.GetConnection()

            Dim expectedReadingCount As Integer = 0
            Dim resistanceReadingtype As ReadingType = ReadingType.Resistance
            Dim resistance As Double = 1001
            Dim nutReadingEntity As New NutReadingEntity With {.NutAutoId = nutId, .ReadingTypeId = resistanceReadingtype, .Amount = resistance}
            nutReadingEntity.Insert(connection)
            With nutReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutReadingEntity)} with '{NameOf(Entities.NutReadingEntity.ReadingTypeId)}' of {resistanceReadingtype}")
                Assert.IsTrue(.Success, .Details)
            End With
            expectedReadingCount += 1

            Dim voltageReadingtype As ReadingType = ReadingType.Voltage
            Dim Voltage As Double = 0.0101
            nutReadingEntity = New NutReadingEntity With {.NutAutoId = nutId, .ReadingTypeId = voltageReadingtype, .Amount = Voltage}
            nutReadingEntity.Insert(connection)
            With nutReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutReadingEntity)} with '{NameOf(Entities.NutReadingEntity.ReadingTypeId)}' of {voltageReadingtype}")
                Assert.IsTrue(.Success, .Details)
            End With
            expectedReadingCount += 1

            nut.FetchUsingKey(connection)
            With nut.ValidateStoredEntity($"Fetch {NameOf(Entities.NutEntity)} with '{NameOf(Entities.NutEntity.AutoId)}' of {nutId}")
                Assert.IsTrue(.Success, .Details)
            End With
            Assert.AreEqual(expectedReadingCount, nut.FetchReadingEntities(connection), $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} count should match")
            Assert.IsTrue(nut.Readings.NutReading.Resistance.HasValue, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} {NameOf(Entities.NutReading.Resistance)} should have a value")
            Assert.AreEqual(resistance, nut.Readings.NutReading.Resistance, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} {NameOf(Entities.NutReading.Resistance)} should match")
            Assert.IsTrue(nut.Readings.NutReading.Voltage.HasValue, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} {NameOf(Entities.NutReading.Voltage)} should have a value")
            Assert.AreEqual(Voltage, nut.Readings.NutReading.Voltage, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} {NameOf(Entities.NutReading.Voltage)} should match")
        End Using
        Return nut
    End Function

    Public Shared Function AssertAddingNutReadings(ByVal site As isr.Std.FrameworkTests.TestSiteBase, ByVal provider As ProviderBase,
                                                   ByVal nut As NutEntity, ByVal element As Entities.ElementEntity, ByVal bin As ReadingBin) As NutEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName, NutReadingBuilder.TableName})

        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim rand As New Random(DateTime.Now.Millisecond)
            Dim meterModelId As Integer = 1
            Dim expectedReadingCount As Integer = 0
            Dim readingtype As ReadingType = Auditor.SelectElementReadingType(element.Label)
            Dim currentReading As Double = Auditor.SelectElementCurrentSource(element.Label)
            Dim nominalValue As Double
            Dim readingValue As Double
            Dim guardedToleranceBand As RangeR
            Dim overflowRange As RangeR
            With element.NominalTypeNominals(NominalAttribute.SelectDefaultNominalType(CType(element.ElementTypeId, ElementType))).NominalAttribute
                nominalValue = .NominalValue.Value
                guardedToleranceBand = .GuardedSpecificationRange
                overflowRange = .OverflowRange
            End With
            nominalValue = element.NominalTypeNominals(NominalAttribute.SelectDefaultNominalType(CType(element.ElementTypeId, ElementType))).NominalAttribute.NominalValue.Value
            Select Case bin
                Case ReadingBin.Good
                    readingValue = guardedToleranceBand.Min + rand.NextDouble * guardedToleranceBand.Span
                Case ReadingBin.High
                    readingValue = guardedToleranceBand.Max + rand.NextDouble * guardedToleranceBand.Span
                Case ReadingBin.Low
                    readingValue = guardedToleranceBand.Min - rand.NextDouble * guardedToleranceBand.Span
                Case ReadingBin.Overflow
                    readingValue = overflowRange.Min * (1 - 0.1 * rand.NextDouble)
            End Select

            Dim nutReadingEntity As New NutReadingEntity With {.NutAutoId = nut.AutoId, .ReadingTypeId = readingtype, .Amount = readingValue}
            nutReadingEntity.Insert(connection)
            With nutReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutReadingEntity)} with '{NameOf(Entities.NutReadingEntity.ReadingTypeId)}' of {readingtype}")
                Assert.IsTrue(.Success, .Details)
            End With
            If nut.Readings Is Nothing Then nut.FetchReadings(connection)
            nut.Readings.Add(nutReadingEntity)
            expectedReadingCount += 1

            If currentReading > 0 Then
                readingtype = readingtype.Current
                nutReadingEntity = New NutReadingEntity With {.NutAutoId = nut.AutoId, .ReadingTypeId = readingtype, .Amount = currentReading}
                nutReadingEntity.Insert(connection)
                With nutReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutReadingEntity)} with '{NameOf(Entities.NutReadingEntity.ReadingTypeId)}' of {readingtype}")
                    Assert.IsTrue(.Success, .Details)
                End With
                nut.Readings.Add(nutReadingEntity)
                expectedReadingCount += 1

                readingtype = readingtype.Voltage
                nutReadingEntity = New NutReadingEntity With {.NutAutoId = nut.AutoId, .ReadingTypeId = readingtype, .Amount = currentReading * readingValue}
                nutReadingEntity.Insert(connection)
                With nutReadingEntity.ValidateStoredEntity($"Insert {NameOf(Entities.NutReadingEntity)} with '{NameOf(Entities.NutReadingEntity.ReadingTypeId)}' of {readingtype}")
                    Assert.IsTrue(.Success, .Details)
                End With
                nut.Readings.Add(nutReadingEntity)
                expectedReadingCount += 1
            End If

            Assert.AreEqual(expectedReadingCount, nut.FetchReadingEntities(connection), $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} count should match")
            If currentReading > 0 Then
                Assert.AreEqual(readingValue, nut.Readings.NutReading.Resistance, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} {NameOf(Entities.NutReading.Resistance)} should match")
                Assert.IsTrue(nut.Readings.NutReading.Voltage.HasValue, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} {NameOf(Entities.NutReading.Voltage)} should have a value")
                Assert.AreEqual(currentReading * readingValue, nut.Readings.NutReading.Voltage.Value, 0.0001, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} {NameOf(Entities.NutReading.Voltage)} should match")
            Else
                Assert.AreEqual(readingValue, nut.Readings.NutReading.Equation, $"{NameOf(Entities.NutEntity)} {NameOf(Entities.NutEntity.Readings)} {NameOf(Entities.NutReading.Equation)} should match")
            End If
        End Using
        Return nut
    End Function

    ''' <summary> Assert storing Nut bin. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    ''' <param name="nutId">    Identifier of the <see cref="Dapper.Entities.NutEntity"/>. </param>
    ''' <param name="binId">    Identifier for the bin. </param>
    ''' <returns> An NutReadingBinEntity. </returns>
    Public Shared Function AssertStoringNutBin(ByVal site As isr.Std.FrameworkTests.TestSiteBase, ByVal provider As ProviderBase,
                                               ByVal nutId As Integer, ByVal binId As Integer) As NutReadingBinEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName, NutReadingBuilder.TableName})
        Dim nutReadingBin As New NutReadingBinEntity With {.NutAutoId = nutId, .ReadingBinId = binId}
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            With nutReadingBin.TryObtain(connection)
                Assert.IsTrue(.Success, .Details)
            End With
        End Using
        Return nutReadingBin
    End Function

    ''' <summary> Assert adding Nut Bins. </summary>
    ''' <remarks> David, 5/30/2020. </remarks>
    ''' <param name="site">        The site. </param>
    ''' <param name="provider">    The provider. </param>
    ''' <param name="nut">         The <see cref="Dapper.Entities.NutEntity"/>. </param>
    ''' <param name="element">     The element. </param>
    ''' <param name="expectedBin"> The expected bin. </param>
    ''' <returns> A <see cref="Dapper.Entities.NutEntity"/>. </returns>
    Public Shared Function AssertAddingNutBin(ByVal site As isr.Std.FrameworkTests.TestSiteBase, ByVal provider As ProviderBase,
                                              ByVal nut As NutEntity, ByVal element As Entities.ElementEntity, ByVal expectedBin As ReadingBin) As NutReadingBinEntity
        isr.Dapper.Entities.Tests.Auditor.AssertTablesExist(site, provider, New String() {NutBuilder.TableName, NutReadingBuilder.TableName})

        Dim meterId As Integer = 1
        Dim reading As Double? = nut.SelectReading(CType(nut.NominalTypeId, NominalType))
        Dim bin As ReadingBin
        With element.NominalTypeNominals(NominalAttribute.SelectDefaultNominalType(CType(element.ElementTypeId, ElementType))).NominalAttribute
            bin = .DoBin(reading)
        End With
        Assert.AreEqual(expectedBin, bin, $"[{NameOf(NutEntity)}] bin number of {element.Label} should match")
        Return Auditor.AssertStoringNutBin(site, provider, nut.AutoId, bin)
    End Function

    ''' <summary> Assert Nut entity. </summary>
    ''' <remarks> David, 5/12/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertNutEntity(ByVal site As isr.Std.FrameworkTests.TestSiteBase, ByVal provider As ProviderBase)

        Dim nutReadingBin As NutReadingBinEntity
        Dim nut As NutEntity
        Dim element As ElementEntity
        Dim uut As UutEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            connection.DeleteAll(Of PartNub)
            connection.DeleteAll(Of LotNub)
            connection.DeleteAll(Of UutNub)
            connection.DeleteAll(Of LotUutNub)
            connection.DeleteAll(Of UutProductSortNub)
            connection.DeleteAll(Of NutNub)
            connection.DeleteAll(Of ElementNub)

            Dim expectedElementCount As Integer = 4
            Dim productNumber As String = "D1206LF"
            Dim product As ProductEntity = Auditor.AssertAddingProductEntity(site, provider, productNumber)
            Dim productElement As New ProductElementEntity With {.ProductAutoId = product.AutoId}
            productElement.AddElements(connection, productNumber)
            Dim actualCount As Integer = product.FetchElements(connection)
            Assert.AreEqual(expectedElementCount, actualCount, $"inserted {NameOf(Entities.ProductEntity)} {productNumber} {NameOf(Entities.ProductEntity.Elements)} count should match")

            Dim partNumber As String = "PFC-D1206LF-02-1001-3301-FB"
            Dim part As PartEntity = Auditor.AssertAddingPartEntity(site, provider, partNumber)

            Dim lastUutNumber As Integer = 0
            Dim lotNumber As String = "lot1"
            uut = Auditor.AssertAddingUutEntity(site, provider, lotNumber, lastUutNumber)
            lastUutNumber = uut.UutNumber

            For Each elementInfo As ElementInfo In Entities.ProductElementEntity.SelectElementInfo(productNumber)
                element = product.Elements.Element(elementInfo.Label)
                expectedElementCount += 1
                nut = Auditor.AssertAddingNutEntity(site, provider, uut.AutoId, element.AutoId, elementInfo.NominalTypeId)
                nutReadingBin = Auditor.AssertStoringNutBin(site, provider, nut.AutoId, ReadingBin.Good)
                nut = Auditor.AssertAddingNutReadings(site, provider, nut.AutoId)
            Next

#If False Then
' add later
            Dim partAutoId As Integer = part.AutoId
            part = New PartEntity With {.PartNumber = partNumber}
            part.Obtain(connection)
            Assert.AreEqual(partAutoId, part.AutoId, $"Fetched {NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.AutoId)} should match")
            Assert.AreEqual(expectedElementCount, part.FetchElements(connection), $"Fetched {NameOf(Entities.PartEntity)} {NameOf(Entities.PartEntity.Elements)} count should match")
#End If

            lotNumber = "lot2"
            lastUutNumber = 0
            uut = Auditor.AssertAddingUutEntity(site, provider, lotNumber, lastUutNumber)
            lastUutNumber = uut.UutNumber

            For Each elementInfo As ElementInfo In Entities.ProductElementEntity.SelectElementInfo(productNumber)
                element = product.Elements.Element(elementInfo.Label)
                expectedElementCount += 1
                nut = Auditor.AssertAddingNutEntity(site, provider, uut.AutoId, element.AutoId, elementInfo.NominalTypeId)
                nutReadingBin = Auditor.AssertStoringNutBin(site, provider, nut.AutoId, ReadingBin.Good)
                nut = Auditor.AssertAddingNutReadings(site, provider, nut.AutoId)
            Next

            uut = Auditor.AssertAddingUutEntity(site, provider, lotNumber, lastUutNumber)
            lastUutNumber = uut.UutNumber

            For Each elementInfo As ElementInfo In Entities.ProductElementEntity.SelectElementInfo(productNumber)
                element = product.Elements.Element(elementInfo.Label)
                expectedElementCount += 1
                nut = Auditor.AssertAddingNutEntity(site, provider, uut.AutoId, element.AutoId, elementInfo.NominalTypeId)
                nutReadingBin = Auditor.AssertStoringNutBin(site, provider, nut.AutoId, ReadingBin.Good)
                nut = Auditor.AssertAddingNutReadings(site, provider, nut.AutoId)
            Next

        End Using
    End Sub

#End Region

#Region " READING TYPE ENTITY "

    ''' <summary> Asserts <see cref="ReadingTypeEntity"/> test conditions. </summary>
    ''' <remarks> David, 3/26/2020. </remarks>
    ''' <param name="site">     The site. </param>
    ''' <param name="provider"> The provider. </param>
    Public Shared Sub AssertReadingTypeEntity(ByVal site As isr.Std.FrameworkTests.TestSiteBase, ByVal provider As ProviderBase)
        Dim id As Integer = isr.Dapper.Entities.ReadingType.None
        Dim ReadingType As New ReadingTypeEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = ReadingTypeBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = ReadingType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.ReadingTypeEntity)} With {NameOf(Entities.ReadingTypeEntity.Id)} Of {id} should Not exist")
            With ReadingType.ValidateNewEntity($"New {NameOf(Entities.ReadingTypeEntity)} With {NameOf(Entities.ReadingTypeEntity.Id)} Of {id}")
                Assert.IsTrue(.Success, .Details)
            End With
            ReadingType.Clear()
            id = isr.Dapper.Entities.ReadingType.Resistance
            expectedFetched = True
            actualFetched = ReadingType.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.ReadingTypeEntity)} With {NameOf(Entities.ReadingTypeEntity.Id)} Of {id}should exist")
            With ReadingType.ValidateStoredEntity($"Fetched {NameOf(Entities.ReadingTypeEntity)} With {NameOf(Entities.ReadingTypeEntity.Id)} Of {id}")
                Assert.IsTrue(.Success, .Details)
            End With

            ReadingType.Label = isr.Dapper.Entities.ReadingType.Resistance.ToString
            actualFetched = ReadingType.FetchUsingUniqueIndex(connection)
            Assert.AreEqual(expectedFetched, actualFetched, $"{NameOf(Entities.ReadingTypeEntity)} by {NameOf(Entities.ReadingTypeEntity.Label)} Of {isr.Dapper.Entities.ReadingType.Resistance} should exist")
            With ReadingType.ValidateStoredEntity($"Fetched {NameOf(Entities.ReadingTypeEntity)} by {NameOf(Entities.ReadingTypeEntity.Label)} Of {isr.Dapper.Entities.ReadingType.Resistance}")
                Assert.IsTrue(.Success, .Details)
            End With

            Dim expectedCount As Integer = [Enum].GetValues(GetType(isr.Dapper.Entities.ReadingType)).Length - 1
            Dim actualCount As Integer = ReadingType.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"Expected {expectedCount} {NameOf(Entities.ReadingTypeEntity)}s")
        End Using
    End Sub

#End Region


End Class

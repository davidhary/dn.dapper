#If PCM Then
Imports Dapper
Imports Dapper.Contrib.Extensions
Imports isr.Dapper.Entity
Imports isr.Dapper.Entities
#End If

Partial Public NotInheritable Class Auditor

#If PCM Then
#Region " CROSS EDGE "

    ''' <summary> Executes the Cross Edge entity test operation. </summary>
    Friend Shared Sub AssertCrossEdgeEntity(ByVal site As TestSite, ByVal provider As ProviderBase)
        Dim crossEdgeId As Integer = 10
        Dim crossEdge As New CrossEdgeEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            connection.DeleteAll(Of CrossEdgeNub)
            Dim tableName As String = CrossEdgeBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = crossEdge.FetchUsingKey(connection, crossEdgeId)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {crossEdgeId} should not exist")
            Auditor.AssertNewEntity(crossEdge, $"New Cross edge entity with key {crossEdgeId}")
            crossEdge.Clear()

            crossEdgeId = 10
            crossEdge = New CrossEdgeEntity With {.CrossEdgeId = crossEdgeId, .Title = crossEdgeId.ToString, .SenseChannelList = "1", .SenseLeadsPolarity = Polarity.Positive,
                                                      .SourceChannelList = "1", .SourceLeadsPolarity = Polarity.Negative, .SourcePolarity = Polarity.Positive}
            Dim actualInserted As Boolean = crossEdge.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Cross edge {crossEdge.Title} should have been inserted")
            Auditor.AssertStoredEntity(crossEdge, $"Inserted Cross edge entity with key {crossEdgeId}")

            Dim expectedCount As Integer = 1

            crossEdgeId += 1
            Dim ICrossEdge As ICrossEdge = New CrossEdgeNub With {.CrossEdgeId = crossEdgeId, .Title = crossEdgeId.ToString,
                                                                      .SenseChannelList = "1", .SenseLeadsPolarityId = Polarity.Positive,
                                                                      .SourceChannelList = "1", .SourceLeadsPolarityId = Polarity.Negative,
                                                                      .SourcePolarityId = Polarity.Positive}
            Dim unused As Boolean = crossEdge.Store(connection, ICrossEdge)
            Auditor.AssertStoredEntity(crossEdge, $"Stored Cross edge entity with key {crossEdgeId}")

            expectedCount += 1
            Dim actualCount As Integer = crossEdge.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple entities should have been created")

            connection.DeleteAll(Of CrossEdgeNub)()
        End Using
    End Sub

#End Region

#Region " LINE EDGE "

    ''' <summary> Asserts the Line Edge entity. </summary>
    Friend Shared Sub AssertLineEdgeEntity(ByVal site As TestSite, ByVal provider As ProviderBase)
        Dim lineEdgeId As Integer = 10
        Dim lineEdge As New LineEdgeEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            connection.DeleteAll(Of LineEdgeNub)
            Dim tableName As String = LineEdgeBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = lineEdge.FetchUsingKey(connection, lineEdgeId)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {lineEdgeId} should not exist")
            Auditor.AssertNewEntity(lineEdge, $"New line edge with key {lineEdgeId}")
            lineEdge.Clear()

            lineEdgeId = 10
            lineEdge = New LineEdgeEntity With {.LineEdgeId = lineEdgeId, .Title = lineEdgeId.ToString, .MeasureChannelList = "2001,2002", .SourceChannelList = "2003,2004"}
            Dim actualInserted As Boolean = lineEdge.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Line edge {lineEdge.Title} should have been inserted")
            Auditor.AssertStoredEntity(lineEdge, $"Inserted line edge with key {lineEdgeId}")

            Dim expectedCount As Integer = 1
            Dim actualCount As Integer = lineEdge.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple entities should have been created")

            connection.DeleteAll(Of LineEdgeNub)()
        End Using
    End Sub

#End Region

#Region " LINE STRUCTURE NOMINAL "

    ''' <summary> Asserts line structure nominal entity. </summary>
    Friend Shared Sub AssertLineStructureNominalEntity(ByVal site As TestSite, ByVal provider As ProviderBase)
        Dim id As Integer = 10
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = LineStructureNominalBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim lineStructureNominal As New LineStructureNominalEntity
            Dim actualFetched As Boolean = lineStructureNominal.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {id} should not exist")
            Auditor.AssertNewEntity(lineStructureNominal, $"New line Structure Nominal  with key {id}")
            lineStructureNominal.Clear()

            Dim expectedCount As Integer = lineStructureNominal.FetchAllEntities(connection)

            Dim structureType As StructureType = StructureType.GreekCross
            lineStructureNominal = New LineStructureNominalEntity With {.StructureTypeId = structureType, .LineLength = 0.1,
                                                                                  .LineWidth = 0.2, .SplitLineLength = 1, .SplitLineWidth = 1}
            Dim actualInserted As Boolean = lineStructureNominal.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Line structure nominal {lineStructureNominal.StructureTypeId} should have been inserted")
            Assert.AreEqual(CInt(structureType), lineStructureNominal.StructureTypeId, $"Line structure nominal {lineStructureNominal.StructureTypeId} should match")
            Auditor.AssertStoredEntity(lineStructureNominal, $"Inserted line Structure Nominal type {structureType}")

            expectedCount += 1
            Dim actualCount As Integer = lineStructureNominal.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple entities should have been created")

            Dim expectedDeleted As Boolean = lineStructureNominal.Delete(connection)
            Assert.IsTrue(expectedDeleted, $"Line structure nominal {structureType} should have been deleted")
            Auditor.AssertNewEntity(lineStructureNominal, $"deleted line Structure Nominal type {structureType}")

            expectedCount -= 1
            actualCount = connection.DeleteAllEntitiesAsync(Of ILineStructureNominal)().Result
            Assert.AreEqual(expectedCount, actualCount, $"all entities should have been deleted")
        End Using
    End Sub

#End Region

#Region " LINE EDGE RESISTANCE "

    ''' <summary> Asserts line edge resistance entity. </summary>
    Friend Shared Sub AssertLineEdgeResistanceEntity(ByVal site As TestSite, ByVal provider As ProviderBase)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = StructureBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of PartNub)()
            connection.DeleteAll(Of LotNub)()
            connection.DeleteAll(Of SubstrateNub)()
            connection.DeleteAll(Of StructureNub)()
            connection.DeleteAll(Of LineEdgeNub)()
            connection.DeleteAll(Of LineEdgeResistanceNub)()
            Dim part As New PartEntity With {.PartNumber = "Part1"}
            part.Insert(connection)

            Dim lot As New LotEntity With {.LotNumber = "Lot1", .AutoId = part.AutoId}
            lot.Insert(connection)

            Dim substrate As New SubstrateEntity With {.Amount = 1, .SubstrateType = SubstrateType.Wafer}
            substrate.Insert(connection)

            Dim lineEdgeId As Integer = 100
            Dim LineEdge100 As New LineEdgeEntity With {.LineEdgeId = lineEdgeId, .Title = lineEdgeId.ToString, .MeasureChannelList = "2001,2002", .SourceChannelList = "2003,2004"}
            LineEdge100.Insert(connection)

            Dim ordinalNumber As Integer = 1
            Dim structureType As StructureType = StructureType.CrossBridge
            Dim structureEntity As New StructureEntity With {.Amount = ordinalNumber, .TypeId = structureType}
            structureEntity.Insert(connection)

            Dim lineEdgeResistance As New LineEdgeResistanceEntity With {.LineEdgeId = LineEdge100.LineEdgeId, .StructureAutoId = structureEntity.AutoId, .Current = 0.1, .Voltage = 1}
            Dim actualInserted As Boolean = lineEdgeResistance.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Line edge resistance {LineEdge100.LineEdgeId} should have been inserted")
            Assert.AreEqual(structureEntity.AutoId, lineEdgeResistance.StructureAutoId, $"Structure auto id should match")
            Auditor.AssertStoredEntity(lineEdgeResistance, $"Inserted lineEdgeResistance for line edge id {LineEdge100.LineEdgeId}")

            lineEdgeResistance.Current += 0.01
            Auditor.AssertChangedEntity(lineEdgeResistance, $"Changed lineEdgeResistance for line edge id {LineEdge100.LineEdgeId}")

            Dim actualUpdate As Boolean = lineEdgeResistance.UpdateConfirm(connection)
            Dim expectedUpdate As Boolean = True
            Assert.AreEqual(expectedUpdate, actualUpdate, $"Line edge resistance {lineEdgeResistance.LineEdgeId} should be updated")
            Auditor.AssertStoredEntity(lineEdgeResistance, $"Updated lineEdgeResistance for line edge id {LineEdge100.LineEdgeId}")

            actualUpdate = lineEdgeResistance.UpdateConfirm(connection)
            expectedUpdate = False
            Assert.AreEqual(expectedUpdate, actualUpdate, $"Line edge resistance {lineEdgeResistance.LineEdgeId} should not be updated")
            Auditor.AssertStoredEntity(lineEdgeResistance, $"Unchanged lineEdgeResistance for line edge id {LineEdge100.LineEdgeId}")

            lineEdgeResistance.Current += 0.01
            actualUpdate = lineEdgeResistance.UpdateConfirm(connection)
            expectedUpdate = True
            Assert.AreEqual(expectedUpdate, actualUpdate, $"Line edge resistance {lineEdgeResistance.LineEdgeId} should be updated")
            actualUpdate = lineEdgeResistance.UpdateConfirm(connection)
            expectedUpdate = False
            Assert.AreEqual(expectedUpdate, actualUpdate, $"Line edge resistance {lineEdgeResistance.LineEdgeId} should be not be updated; not using update tracking.")

            Dim actualCount As Integer = LineEdgeResistanceEntity.CountEntities(connection, structureEntity.AutoId, lineEdgeId)
            Dim excpectedCount As Integer = 1
            Assert.AreEqual(excpectedCount, actualCount, $"Line edge resistance {structureEntity.AutoId}.{lineEdgeResistance.LineEdgeId} expected count")
            actualCount = LineEdgeResistanceEntity.FetchNubs(connection, structureEntity.AutoId, lineEdgeId).Count
            Assert.AreEqual(excpectedCount, actualCount, $"Line edge resistance {structureEntity.AutoId}.{lineEdgeResistance.LineEdgeId} expected entities count")
            actualCount = LineEdgeResistanceEntity.FetchEntities(connection, structureEntity.AutoId).Count
            Assert.AreEqual(excpectedCount, actualCount, $"Line edge resistance {structureEntity.AutoId} expected entities count")

            lineEdgeId += 1
            Dim LineEdge101 As New LineEdgeEntity With {.LineEdgeId = lineEdgeId, .Title = lineEdgeId.ToString, .MeasureChannelList = "2001,2002", .SourceChannelList = "2003,2004"}
            LineEdge101.Insert(connection)

            lineEdgeResistance = New LineEdgeResistanceEntity With {.LineEdgeId = LineEdge101.LineEdgeId, .StructureAutoId = structureEntity.AutoId,
                                                                        .Current = 0.1, .Voltage = 1}
            actualInserted = LineEdgeResistanceEntity.StoreEntity(connection, lineEdgeResistance)
            excpectedCount += 1
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Line edge resistance {LineEdge101.LineEdgeId} should have been inserted")
            Assert.AreEqual(structureEntity.AutoId, lineEdgeResistance.StructureAutoId, $"Structure auto id should match")

            lineEdgeResistance.Current += 0.01
            Dim expectedCurrent As Double? = lineEdgeResistance.Current
            actualInserted = LineEdgeResistanceEntity.StoreEntity(connection, lineEdgeResistance)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Line edge resistance {LineEdge101.LineEdgeId} should have been updated")
            Assert.AreEqual(expectedCurrent, lineEdgeResistance.Current, $"Line edge resistance {LineEdge101.LineEdgeId} currents should match")

            actualCount = LineEdgeResistanceEntity.FetchEntities(connection, structureEntity.AutoId).Count
            Assert.AreEqual(excpectedCount, actualCount, $"Line edge resistance {structureEntity.AutoId} expected entities count")

            Dim ILineEdgeResistance As ILineEdgeResistance = New LineEdgeResistanceNub
            LineEdgeResistanceEntity.Copy(lineEdgeResistance, ILineEdgeResistance)
            actualInserted = lineEdgeResistance.Store(connection, ILineEdgeResistance)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Line edge resistance {LineEdge101.LineEdgeId} should have been stored")

            connection.DeleteAll(Of LineEdgeResistanceNub)()

        End Using
    End Sub

#End Region

#Region " CROSS EDGE RESISTANCE "

    ''' <summary> Asserts Cross edge resistance entity. </summary>
    Friend Shared Sub AssertCrossEdgeResistanceEntity(ByVal site As TestSite, ByVal provider As ProviderBase)
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = StructureBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of PartNub)()
            connection.DeleteAll(Of LotNub)()
            connection.DeleteAll(Of SubstrateNub)()
            connection.DeleteAll(Of StructureNub)()
            connection.DeleteAll(Of CrossEdgeNub)()
            connection.DeleteAll(Of CrossEdgeResistanceNub)()
            Dim part As New PartEntity With {.PartNumber = "Part1"}
            part.Insert(connection)

            Dim lot As New LotEntity With {.LotNumber = "Lot1", .AutoId = part.AutoId}
            lot.Insert(connection)

            Dim substrate As New SubstrateEntity With {.Amount = 1, .SubstrateType = SubstrateType.Wafer}
            substrate.Insert(connection)

            Dim structureType As StructureType = StructureType.CrossBridge
            Dim ordinalNumber As Integer = 1
            Dim structureEntity As New StructureEntity With {.Amount = ordinalNumber, .TypeId = structureType}
            structureEntity.Insert(connection)

            Dim crossEdgeId As Integer = 100
            Dim CrossEdge100 As New CrossEdgeEntity With {.CrossEdgeId = crossEdgeId, .Title = crossEdgeId.ToString, .SenseChannelList = "1", .SenseLeadsPolarity = Polarity.Positive,
                                                              .SourceChannelList = "1", .SourceLeadsPolarity = Polarity.Negative, .SourcePolarity = Polarity.Positive}
            CrossEdge100.Insert(connection)


            Dim ICrossEdgeResistance As ICrossEdgeResistance = New CrossEdgeResistanceNub With {.CrossEdgeId = CrossEdge100.CrossEdgeId,
                                                                               .StructureAutoId = structureEntity.AutoId,
                                                                               .Current = 0.1, .Voltage = 1}
            Dim crossEdgeResistance As New CrossEdgeResistanceEntity
            Dim id As Integer = 1
            crossEdgeResistance.FetchUsingKey(connection, id)
            Auditor.AssertNewEntity(crossEdgeResistance, $"New Cross Edge resistance With ID {id}")
            crossEdgeResistance.Clear()

            crossEdgeResistance = New CrossEdgeResistanceEntity(ICrossEdgeResistance)
            Dim actualInserted As Boolean = crossEdgeResistance.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Cross edge resistance {CrossEdge100.CrossEdgeId} should have been inserted")
            Assert.AreEqual(structureEntity.AutoId, crossEdgeResistance.StructureAutoId, $"Structure auto id should match")
            Auditor.AssertStoredEntity(crossEdgeResistance, $"Stored Cross Edge resistance With cross edge id {CrossEdge100.CrossEdgeId}")

            crossEdgeResistance.Current += 0.01
            Auditor.AssertChangedEntity(crossEdgeResistance, $"Changed Cross Edge resistance With cross edge id {CrossEdge100.CrossEdgeId}")

            Dim actualUpdate As Boolean = crossEdgeResistance.UpdateConfirm(connection)
            Assert.IsTrue(actualUpdate, $"Cross edge resistance {crossEdgeResistance.CrossEdgeId} should be stored")
            actualUpdate = crossEdgeResistance.UpdateConfirm(connection)
            Auditor.AssertStoredEntity(crossEdgeResistance, $"Unchanged Cross Edge resistance With cross edge id {CrossEdge100.CrossEdgeId}")
            Assert.IsFalse(actualUpdate, $"Unchanged cross edge resistance {crossEdgeResistance.CrossEdgeId} should not be updated")

            Dim actualCount As Integer = CrossEdgeResistanceEntity.CountEntities(connection, structureEntity.AutoId, crossEdgeId)
            Dim excpectedCount As Integer = 1
            Assert.AreEqual(excpectedCount, actualCount, $"Cross edge resistance {structureEntity.AutoId}.{crossEdgeResistance.CrossEdgeId} expected count")
            actualCount = CrossEdgeResistanceEntity.FetchNubs(connection, structureEntity.AutoId, crossEdgeId).Count
            Assert.AreEqual(excpectedCount, actualCount, $"Cross edge resistance {structureEntity.AutoId}.{crossEdgeResistance.CrossEdgeId} expected entities count")
            actualCount = CrossEdgeResistanceEntity.FetchEntities(connection, structureEntity.AutoId).Count
            Assert.AreEqual(excpectedCount, actualCount, $"Cross edge resistance {structureEntity.AutoId} expected entities count")

            crossEdgeId += 1
            Dim CrossEdge101 As New CrossEdgeEntity With {.CrossEdgeId = crossEdgeId, .Title = crossEdgeId.ToString, .SenseChannelList = "1", .SenseLeadsPolarity = Polarity.Positive,
                                                                 .SourceChannelList = "1", .SourceLeadsPolarity = Polarity.Negative, .SourcePolarity = Polarity.Positive}
            CrossEdge101.Insert(connection)

            crossEdgeResistance = New CrossEdgeResistanceEntity With {.CrossEdgeId = CrossEdge101.CrossEdgeId, .StructureAutoId = structureEntity.AutoId,
                                                                          .Current = 0.1, .Voltage = 1}
            actualInserted = CrossEdgeResistanceEntity.StoreEntity(connection, crossEdgeResistance)
            excpectedCount += 1
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Cross edge resistance {CrossEdge101.CrossEdgeId} should have been inserted")
            Assert.AreEqual(structureEntity.AutoId, crossEdgeResistance.StructureAutoId,
                                $"Structure auto id for Cross edge resistance {CrossEdge101.CrossEdgeId} should match")

            crossEdgeResistance.Current += 0.01
            Dim expectedCurrent As Double? = crossEdgeResistance.Current
            actualInserted = CrossEdgeResistanceEntity.StoreEntity(connection, crossEdgeResistance)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Cross edge resistance {CrossEdge101.CrossEdgeId} should have been updated")
            Assert.AreEqual(expectedCurrent, crossEdgeResistance.Current, $"Cross edge resistance {CrossEdge101.CrossEdgeId} currents should match")

            actualCount = CrossEdgeResistanceEntity.FetchEntities(connection, structureEntity.AutoId).Count
            Assert.AreEqual(excpectedCount, actualCount, $"Cross edge resistance {structureEntity.AutoId} expected entities count")

            crossEdgeId += 1
            Dim CrossEdge102 As New CrossEdgeEntity With {.CrossEdgeId = crossEdgeId, .Title = crossEdgeId.ToString, .SenseChannelList = "1", .SenseLeadsPolarity = Polarity.Positive,
                                                                 .SourceChannelList = "1", .SourceLeadsPolarity = Polarity.Negative, .SourcePolarity = Polarity.Positive}
            CrossEdge102.Insert(connection)

            crossEdgeResistance = New CrossEdgeResistanceEntity With {.CrossEdgeId = CrossEdge102.CrossEdgeId,
                                                                         .StructureAutoId = structureEntity.AutoId,
                                                                          .Current = 0.1, .Voltage = 1}
            crossEdgeResistance.Insert(connection)

            ICrossEdgeResistance = New CrossEdgeResistanceNub
            CrossEdgeResistanceEntity.Copy(crossEdgeResistance, ICrossEdgeResistance)
            actualInserted = crossEdgeResistance.Store(connection, ICrossEdgeResistance)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"Cross edge resistance {ICrossEdgeResistance.CrossEdgeId} should have been stored")

            connection.DeleteAll(Of CrossEdgeResistanceNub)()

        End Using
    End Sub

#End Region

#Region " LINE STRUCTURE "

    ''' <summary> Asserts line structure entity. </summary>
    Friend Shared Sub AssertLineStructureEntity(ByVal site As TestSite, ByVal provider As ProviderBase)
        Dim id As Integer = 10
        Dim lineStructure As New LineStructureEntity
        Using connection As System.Data.IDbConnection = provider.GetConnection()
            connection.DeleteAll(Of LineStructureNub)
            Dim tableName As String = LineStructureBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            Dim expectedFetched As Boolean = False
            Dim actualFetched As Boolean = lineStructure.FetchUsingKey(connection, id)
            Assert.AreEqual(expectedFetched, actualFetched, $"key {id} should not exist")
            Auditor.AssertNewEntity(lineStructure, $"New line Structure with key {id}")
            lineStructure.Clear()

            lineStructure = New LineStructureEntity With {.LinePitch = 0.1, .LineWidthDeviation = 0.2, .LineWidth = 0.2,
                                                                    .SheetResistance = 0.001, .SplitLineWidthDeviation = 0.02, .SplitLineWidth = 1, .TestCurrent = 0.004}
            Dim actualInserted As Boolean = lineStructure.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(lineStructure)} should have been inserted")
            Auditor.AssertStoredEntity(lineStructure, $"Inserted line Structure with key")

            Dim expectedCount As Integer = 1
            Dim actualCount As Integer = lineStructure.FetchAllEntities(connection)
            Assert.AreEqual(expectedCount, actualCount, $"multiple {NameOf(lineStructure)} entities should have been created")

            connection.DeleteAll(Of LineStructureNub)()
        End Using
    End Sub

#End Region

#Region " STRUCTURE LINE "

    ''' <summary> Asserts Structure line. </summary>
    Friend Shared Sub AssertStructureLineEntity(ByVal site As TestSite, ByVal provider As ProviderBase)

        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = StructureLineBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of PartNub)
            connection.DeleteAll(Of LotNub)
            connection.DeleteAll(Of SubstrateNub)
            connection.DeleteAll(Of StructureNub)
            connection.DeleteAll(Of LineStructureNub)

            Dim part As New PartEntity With {.PartNumber = "Part1"}
            part.Insert(connection)

            Dim lot As New LotEntity With {.LotNumber = "Lot1", .AutoId = part.AutoId}
            lot.Insert(connection)

            Dim substrate As New SubstrateEntity With {.Amount = 1, .SubstrateType = SubstrateType.Wafer}
            substrate.Insert(connection)

            Dim structureType As StructureType = StructureType.SplitCross
            Dim ordinalNumber As Integer = 1
            Dim structureEntity As New PcmStructureEntity With {.Amount = ordinalNumber, .TypeId = structureType}
            structureEntity.Insert(connection)

            Dim lineStructure As New LineStructureEntity With {.LinePitch = 0.1, .LineWidthDeviation = 0.2, .LineWidth = 0.2,
                                                               .SheetResistance = 0.001, .SplitLineWidthDeviation = 0.02, .SplitLineWidth = 1, .TestCurrent = 0.004}
            lineStructure.Insert(connection)

            Dim StructureLine As New StructureLineEntity With {.StructureAutoId = structureEntity.AutoId,
                                                               .LineStructureAutoId = lineStructure.LineStructureAutoId}
            Dim actualInserted As Boolean = StructureLine.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(LineStructureEntity)} should have been inserted")
            Auditor.AssertStoredEntity(StructureLine, $"Inserted {NameOf(StructureLineEntity)} for Structure #{structureEntity.Amount}")

            structureEntity.FetchLineStructureEntity(connection)
            Assert.AreEqual(lineStructure.LineStructureAutoId, structureEntity.LineStructureEntity.LineStructureAutoId, $"Structure should bring the correct line structure")
            Assert.AreEqual(StructureLine.StructureAutoId, structureEntity.AutoId, $"Line Structure should point to the correct Structure")
            connection.DeleteAll(Of StructureLineNub)()
        End Using
    End Sub

#End Region

#Region " STRUCTURE LINE NOMINAL "

    ''' <summary> Asserts Structure line nominal. </summary>
    Friend Shared Sub AssertStructureLineNominalEntity(ByVal site As TestSite, ByVal provider As ProviderBase)

        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = StructureLineNominalBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of PartNub)
            connection.DeleteAll(Of LotNub)
            connection.DeleteAll(Of SubstrateNub)
            connection.DeleteAll(Of StructureNub)
            connection.DeleteAll(Of LineStructureNominalNub)

            Dim part As New PartEntity With {.PartNumber = "Part1"}
            part.Insert(connection)

            Dim lot As New LotEntity With {.LotNumber = "Lot1", .AutoId = part.AutoId}
            lot.Insert(connection)

            Dim substrate As New SubstrateEntity With {.Amount = 1, .SubstrateType = SubstrateType.Wafer}
            substrate.Insert(connection)

            Dim structureType As StructureType = StructureType.SplitCross
            Dim ordinalNumber As Integer = 1
            Dim structureEntity As New PcmStructureEntity With {.Amount = ordinalNumber, .TypeId = structureType}
            structureEntity.Insert(connection)

            Dim lineStructureNominal As New LineStructureNominalEntity With {.StructureTypeId = StructureType.GreekCross, .LineLength = 0.1,
                                                                                .LineWidth = 0.2, .SplitLineLength = 1, .SplitLineWidth = 1}
            lineStructureNominal.Insert(connection)

            Dim StructureLineNominal As New StructureLineNominalEntity With {.StructureAutoId = structureEntity.AutoId,
                                                                                       .LineStructureNominalAutoId = lineStructureNominal.LineStructureNominalAutoId}
            Dim actualInserted As Boolean = StructureLineNominal.Insert(connection)
            Dim expectedInserted As Boolean = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(LineStructureEntity)} should have been inserted")
            Auditor.AssertStoredEntity(StructureLineNominal, $"Inserted StructureLineNominal with Structure {structureEntity.Amount}")

            structureEntity.FetchLineStructureNominalEntity(connection)
            Assert.AreEqual(StructureLineNominal.LineStructureNominalAutoId, structureEntity.LineStructureNominalEntity.LineStructureNominalAutoId, $"Structure should bring the correct line structure nominal")
            Assert.AreEqual(StructureLineNominal.StructureAutoId, structureEntity.AutoId, $"Line Structure nominal should point to the correct Structure")
            connection.DeleteAll(Of StructureLineNub)()
        End Using
    End Sub

#End Region

#Region " SUBSTRATE LINE NOMINAL "

    ''' <summary> Asserts substrate line nominal. </summary>
    Friend Shared Sub AssertSubstrateLineNominalEntity(ByVal site As TestSite, ByVal provider As ProviderBase)

        Using connection As System.Data.IDbConnection = provider.GetConnection()
            Dim tableName As String = SubstrateLineStructureNominalBuilder.TableName
            site.TraceMessage($"table {tableName} {If(provider.TableExists(connection, tableName), "exists", "not found")}")
            Assert.IsTrue(provider.TableExists(connection, tableName), $"table {tableName} should exist")
            connection.DeleteAll(Of PartNub)
            connection.DeleteAll(Of LotNub)
            connection.DeleteAll(Of SubstrateNub)
            connection.DeleteAll(Of SubstrateLineStructureNominalNub)
            connection.DeleteAll(Of LineStructureNominalNub)

            Dim part As New PartEntity With {.PartNumber = "Part1"}
            part.Insert(connection)

            Dim lot As New LotEntity With {.LotNumber = "Lot1", .AutoId = part.AutoId}
            lot.Insert(connection)

            Dim substrate1 As New SubstrateEntity With {.Amount = 1, .TypeId = SubstrateType.Plate}
            substrate1.Insert(connection)

            Dim substrate2 As New SubstrateEntity With {.Amount = 2, .TypeId = SubstrateType.Plate}
            substrate2.Insert(connection)

            Dim lineStructureNominal As LineStructureNominalEntity = Nothing
            Dim substrateLineStructureNominal As SubstrateLineStructureNominalEntity = Nothing
            Dim actualInserted As Boolean = True
            Dim expectedInserted As Boolean = True
            Dim structuretype As StructureType = StructureType.None

#If False Then
            ' test with no nominal values
            Dim actualCount As Integer = substrate1.FetchSubstrateLineStructureNominals(connection)
            Dim expectedCount As Integer = 0
            Assert.IsNotNull(substrate1.SubstrateLineStructureNominals, $"{NameOf(SubstrateEntity.SubstrateLineStructureNominals)} should exist")
            Assert.AreEqual(expectedCount, actualCount, $"No {NameOf(SubstrateEntity.SubstrateLineStructureNominals)} should exist")

            actualCount = substrate1.FetchLineStructureNominals(connection)
            expectedCount = 0
            Assert.IsNotNull(substrate1.LineStructureNominals, $"{NameOf(SubstrateEntity.SubstrateLineStructureNominals)} should exist")
            Assert.AreEqual(expectedCount, actualCount, $"No {NameOf(SubstrateEntity.LineStructureNominals)} should exist")
#End If

            structuretype = StructureType.CrossBridge
            lineStructureNominal = New LineStructureNominalEntity With {.StructureTypeId = structuretype,
                                                                            .LineLength = 0.0006, .LineWidth = 0.0000135}
            actualInserted = lineStructureNominal.Insert(connection)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(lineStructureNominal)}.{structuretype} should have been inserted")
            Auditor.AssertStoredEntity(lineStructureNominal, $"Inserted Line Structure Nominal for structure {structuretype}")

            substrateLineStructureNominal = New SubstrateLineStructureNominalEntity With {.SubstrateAutoId = substrate1.AutoId,
                                                                                              .LineStructureNominalAutoId = lineStructureNominal.LineStructureNominalAutoId}
            actualInserted = substrateLineStructureNominal.Insert(connection)
            expectedInserted = True
            Assert.IsTrue(actualInserted, $"{NameOf(SubstrateLineStructureNominalEntity)} for {structuretype} should have been inserted")
            Auditor.AssertStoredEntity(substrateLineStructureNominal, $"Inserted substrate Line Structure Nominal for substrate {substrate1.Amount}")

            Dim actualFetched As Boolean = substrateLineStructureNominal.FetchUsingKey(connection)
            Assert.IsTrue(actualFetched, $"{NameOf(SubstrateLineStructureNominalEntity)} for {structuretype} should have been fetched")
            Assert.AreEqual(substrateLineStructureNominal.SubstrateAutoId, substrate1.AutoId,
                                $"{NameOf(SubstrateLineStructureNominalEntity)} {NameOf(substrate1.AutoId)} should match")
            Assert.AreEqual(substrateLineStructureNominal.LineStructureNominalAutoId, lineStructureNominal.LineStructureNominalAutoId,
                                $"{NameOf(SubstrateLineStructureNominalEntity)} {NameOf(lineStructureNominal.LineStructureNominalAutoId)} should match")

            structuretype = StructureType.SplitCross
            lineStructureNominal = New LineStructureNominalEntity With {.StructureTypeId = structuretype,
                                                                            .LineLength = 0.0006, .LineWidth = 0.0000135,
                                                                            .SplitLineLength = 0.0006, .SplitLineWidth = 0.000045}
            actualInserted = lineStructureNominal.Insert(connection)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(lineStructureNominal)}.{structuretype} should have been inserted")

            substrateLineStructureNominal = New SubstrateLineStructureNominalEntity With {.SubstrateAutoId = substrate1.AutoId,
                                                                                              .LineStructureNominalAutoId = lineStructureNominal.LineStructureNominalAutoId}
            actualInserted = substrateLineStructureNominal.Insert(connection)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(SubstrateLineStructureNominalEntity)} for {structuretype} should have been inserted")

            substrateLineStructureNominal = New SubstrateLineStructureNominalEntity With {.SubstrateAutoId = substrate2.AutoId,
                                                                                              .LineStructureNominalAutoId = lineStructureNominal.LineStructureNominalAutoId}
            actualInserted = substrateLineStructureNominal.Insert(connection)
            expectedInserted = True
            Assert.AreEqual(expectedInserted, actualInserted, $"{NameOf(SubstrateLineStructureNominalEntity)} for {structuretype} should have been inserted")

#If False Then
            Dim benchmarkCount As Integer = 1
            Dim stopwatch As Stopwatch = Stopwatch.StartNew
            For i As Integer = 1 To benchmarkCount
                actualCount = substrate1.FetchSubstrateLineStructureNominals(connection)
            Next
            stopwatch.Stop()
            Dim elapsed As TimeSpan = stopwatch.Elapsed
            expectedCount = 2
            Assert.AreEqual(expectedCount, actualCount, $"{NameOf(SubstrateEntity.SubstrateLineStructureNominals)} should exist")
            site.VerboseMessage($"{NameOf(SubstrateEntity.FetchSubstrateLineStructureNominals)} {benchmarkCount} count elapsed: {elapsed}")

            actualCount = substrate1.FetchLineStructureNominals(connection)
            expectedCount = 2
            Assert.IsNotNull(substrate1.LineStructureNominals, $"{NameOf(SubstrateEntity.SubstrateLineStructureNominals)} should exist")
            Assert.AreEqual(expectedCount, actualCount, $"Correct # of {NameOf(SubstrateEntity.LineStructureNominals)} should exist")

            structuretype = StructureType.CrossBridge
            lineStructureNominal = substrate1.SelectLineStructureNominal(connection, structuretype)
            Assert.IsNotNull(lineStructureNominal, $"{NameOf(lineStructureNominal)}.{structuretype} should exist")
            Assert.AreEqual(CInt(structuretype), lineStructureNominal.StructureTypeId, $"{NameOf(lineStructureNominal)}.{structuretype} should match")

            structuretype = StructureType.GreekCross
            lineStructureNominal = substrate1.SelectLineStructureNominal(connection, structuretype)
            Assert.IsNotNull(lineStructureNominal, $"{NameOf(lineStructureNominal)}.{structuretype} should exist")
            Assert.AreEqual(0, lineStructureNominal.StructureTypeId, $"{NameOf(lineStructureNominal)}.{structuretype} should match")

            ' test using multiple fetch
            Stopwatch.StartNew()
            For i As Integer = 1 To benchmarkCount
                actualCount = substrate1.FetchMultipleLineStructureNominals(connection)
            Next
            stopwatch.Stop()
            elapsed = stopwatch.Elapsed
            expectedCount = 2
            Assert.IsNotNull(substrate1.LineStructureNominals, $"{NameOf(SubstrateEntity.SubstrateLineStructureNominals)} should exist")
            Assert.AreEqual(expectedCount, actualCount, $"{NameOf(SubstrateEntity.LineStructureNominals)} should be counted")
            site.VerboseMessage($"{NameOf(SubstrateEntity.FetchMultipleLineStructureNominals)} {benchmarkCount} count elapsed: {elapsed}")
#End If

        End Using
    End Sub

#End Region
#End If

End Class

using System;

namespace isr.Dapper.Skeleton.MSTest
{

    /// <summary>   A test site settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( TestSiteSettings ) )]
    public class TestSiteSettings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public TestSiteSettings() : base( isr.Json.JsonSettingsBase.BuildCallingAssemblySettingsFullFileName() )
        { }

        #region " TEST SITE CONFIGURATION "

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public virtual bool Verbose { get; set; }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public virtual bool Enabled { get; set; }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public virtual bool All { get; set; }

        #endregion

        #region " TEST SITE LOCATION IDENTIFICATION "

        private string _IPv4Prefixes;
        /// <summary>   Gets or sets the candidate IPv4 prefixes for this location. </summary>
        /// <value> The IPv4 prefixes. </value>
        public string IPv4Prefixes
        {
            get => this._IPv4Prefixes;
            set {
                if ( !String.Equals( value, this.IPv4Prefixes ) )
                {
                    this._IPv4Prefixes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _TimeZones;
        /// <summary> Gets or sets the candidate time zones of this location. </summary>
        /// <value> The candidate time zones of the test site. </value>
        public string TimeZones
        {
            get => this._TimeZones;
            set {
                if ( !String.Equals( value, this.TimeZones ) )
                {
                    this._TimeZones = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _TimeZoneOffsets;
        /// <summary> Gets or sets the candidate time zone offsets of this location. </summary>
        /// <value> The time zone offsets. </value>
        public string TimeZoneOffsets
        {
            get => this._TimeZoneOffsets;
            set {
                if ( !String.Equals( value, this.TimeZoneOffsets ) )
                {
                    this._TimeZoneOffsets = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The time zone. </summary>
        private string _TimeZone;

        /// <summary> Gets the time zone of the test site. </summary>
        /// <value> The time zone of the test site. </value>
        public string TimeZone()
        {
            if ( string.IsNullOrWhiteSpace( this._TimeZone ) )
            {
                this._TimeZone = this.TimeZones.Split( '|' )[this.HostInfoIndex()];
            }
            return this._TimeZone;
        }

        /// <summary> The time zone offset. </summary>
        private double _TimeZoneOffset = double.MinValue;

        /// <summary> Gets the time zone offset of the test site. </summary>
        /// <value> The time zone offset of the test site. </value>
        public double TimeZoneOffset()
        {
            if ( this._TimeZoneOffset == double.MinValue )
            {
                this._TimeZoneOffset = double.Parse( this.TimeZoneOffsets.Split( '|' )[this.HostInfoIndex()] );
            }
            return this._TimeZoneOffset;
        }

        /// <summary> Gets the host name of the test site. </summary>
        /// <value> The host name of the test site. </value>
        public static string HostName()
        {
            return System.Net.Dns.GetHostName();
        }

        /// <summary> The host address. </summary>
        private System.Net.IPAddress _HostAddress;

        /// <summary> Gets the IP address of the test site. </summary>
        /// <value> The IP address of the test site. </value>
        public System.Net.IPAddress HostAddress()
        {
            if ( this._HostAddress is null )
            {
                foreach ( System.Net.IPAddress value in System.Net.Dns.GetHostEntry( HostName() ).AddressList )
                {
                    if ( value.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork )
                    {
                        this._HostAddress = value;
                        break;
                    }
                }
            }
            return this._HostAddress;
        }

        /// <summary> Zero-based index of the host information. </summary>
        private int _HostInfoIndex = -1;

        /// <summary> Gets the index into the host information strings. </summary>
        /// <value> The index into the host information strings. </value>
        public int HostInfoIndex()
        {
            if ( this._HostInfoIndex < 0 )
            {
                string ip = this.HostAddress().ToString();
                int i = -1;
                foreach ( string value in this.IPv4Prefixes.Split( '|' ) )
                {
                    i += 1;
                    if ( ip.StartsWith( value, StringComparison.OrdinalIgnoreCase ) )
                    {
                        this._HostInfoIndex = i;
                        break;
                    }
                }
            }

            return this._HostInfoIndex;
        }

        #endregion

    }
}
